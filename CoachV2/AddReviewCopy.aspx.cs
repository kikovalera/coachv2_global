﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Diagnostics;
using System.Web.Security;
namespace CoachV2
{
    public partial class AddReviewCopy : System.Web.UI.Page
    {

        


        string strFilename;
        protected string CIM;
        protected int requestID;
        protected string ccmCIM;
        protected string reviewID;
        protected string reportsTo;
        protected string userIsAgent;

        string toName = "";
        string target = "";
        string subject = "";
        string toEmail = "";
        string htmlBody = "";
        string fromName = "";
        string fromEmail = "";

        public List<OD2> lstOD2
        {
            get
            {
                if (Session["lstOD2"] != null)
                {
                    return (List<OD2>)Session["lstOD2"];
                }
                else
                {
                    return new List<OD2>();
                }
            }
            set
            {
                Session["lstOD2"] = value;
            }
        }



        public List<Employee> lstEmployee
        {
            get
            {
                if (Session["lstEmployee"] != null)
                {
                    return (List<Employee>)Session["lstEmployee"];
                }
                else
                {
                    return new List<Employee>();
                }
            }
            set
            {
                Session["lstEmployee"] = value;
            }
        }
        public List<Nexidia> lstNexidia
        {
            get
            {
                if (Session["lstNexidia"] != null)
                {
                    return (List<Nexidia>)Session["lstNexidia"];
                }
                else
                {
                    return new List<Nexidia>();
                }
            }
            set
            {
                Session["lstNexidia"] = value;
            }
        }

        public List<OD> lstOD
        {
            get
            {
                if (Session["lstOD"] != null)
                {
                    return (List<OD>)Session["lstOD"];
                }
                else
                {
                    return new List<OD>();
                }
            }
            set
            {
                Session["lstOD"] = value;
            }
        }
        public DataTable SearchBlackout
        {
            get
            {
                return ViewState["SearchBlackout"] as DataTable;
            }
            set
            {
                ViewState["SearchBlackout"] = value;
            }
        }
        public DataTable CN
        {
            get
            {
                return ViewState["CN"] as DataTable;
            }
            set
            {
                ViewState["CN"] = value;
            }
        }
        public DataTable PR
        {
            get
            {
                return ViewState["PR"] as DataTable;
            }
            set
            {
                ViewState["PR"] = value;
            }
        }
        public DataTable DataEntry
        {
            get
            {
                return ViewState["DataEntry"] as DataTable;
            }
            set
            {
                ViewState["DataEntry"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (RadCoacheeSignOff.Visible == true)
            {
                this.Form.DefaultButton = RadCoacheeSignOff.UniqueID;
            }


            if (RadCMTSaveSubmit.Visible == true)
            {
                this.Form.DefaultButton = RadCMTSaveSubmit.UniqueID;
            }
            if (!IsPostBack)
            {



                CMTView.Visible = false;
                RadCMTPreview.Visible = false;
                RadCMTSaveSubmit.Visible = false;
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                string value = Session["CIMMyTeam"] as string;
                SomeGlobalVariables sgv = new SomeGlobalVariables();
                if (!String.IsNullOrEmpty(value))
                {
                    //SomeGlobalVariables.CIMMyTeam = value.ToString();
                    sgv.CIMMyTeam = value.ToString();
                }


                //if (Session["CIMMyTeam"].ToString() != null)
                //{
                //    Request.QueryString["CIM"] = Session["CIMMyTeam"].ToString();
                //}

                //GetAllSites();
                //GetAllCampaigns();
                //GetAllDivisions();
                //GetAllLOB();
                GetSubordinates(CIMNumber);

                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CIMNumber, 2);
                if (ds_userselect2.Tables[0].Rows.Count > 0)
                {
                    bool Supervisor;
                    Supervisor = CheckIfHasSubordinates(CIMNumber);

                    if (Supervisor == true)
                    {
                        string CIM = "";
                        string type = Request["Type"];
                        if (Request["CIM"] != null) //Add Review Thru My Team
                        //if (SomeGlobalVariables.CIMMyTeam != null) 
                        {
                            CIM = Request.QueryString["CIM"];
                            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                            Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                            ctrlReviews.Text = " Coaching Dashboard ";
                            ctrlA.Text = "> My Team > ";
                            SetLabels(Convert.ToInt32(CIM));
                            //RadCoacherSignOff.Visible = true;

                            SignOut.Attributes["style"] = "display: block";

                        }
                        //else if (SomeGlobalVariables.CIMMyTeam != null) 
                        else if (sgv.CIMMyTeam != null)
                        {
                            //CIM = SomeGlobalVariables.CIMMyTeam;
                            CIM = sgv.CIMMyTeam;
                            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                            Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                            ctrlReviews.Text = " Coaching Dashboard ";
                            ctrlA.Text = "> My Team > ";
                            SetLabels(Convert.ToInt32(CIM));


                        }
                        else //Add Review Thru My Reviews
                        {

                            Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                            ctrlReviews.Text = " Coaching Dashboard ";
                            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                            ctrlA.Text = " > My Reviews";
                            Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                            ctrlB.Text = " > Add Review";
                            ctrlB.Visible = true;

                            RadAccount.Enabled = true;
                            RadSupervisor.Enabled = true;
                            RadCoacheeName.Enabled = true;
                            GetAccounts(CIMNumber);
                        }


                        GetDropDownSessions();
                        if (DefaultForm.Visible == true)
                        {
                            List<Employee> list = new List<Employee>();
                            lstEmployee = list;
                        }
                        if (NexidiaForm.Visible == true)
                        {
                            List<Nexidia> lst = new List<Nexidia>();
                            lstNexidia = lst;
                        }
                        if (ODForm.Visible == true)
                        {
                            List<OD2> lst = new List<OD2>();
                            lstOD2 = lst;
                        }

                        RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                        RadDocumentationReview.DataSource = string.Empty;
                        RadDocumentationReview.Rebind();
                        RadGridDocumentation.DataSource = string.Empty;
                        RadGridDocumentation.Rebind();
                        RadGrid8.DataSource = string.Empty;
                        RadGrid8.Rebind();
                        RadGrid12.DataSource = string.Empty;
                        RadGrid12.Rebind();

                        RadGrid1.MasterTableView.Rebind();
                        RadGrid4.Rebind();
                        RadGrid4.MasterTableView.Rebind();

                        LoadCommitmentQuestions();
                        string Role;
                        Role = CheckIfHRQA(CIMNumber);

                        SelectSearchBlackout.Visible = false;
                        SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-6";
                        SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-6";

                        if (Role != "HR")
                        {
                            GetDefaultForm(CIMNumber);
                        }
                        else
                        {
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));
                            Session["DefaultUserAcct"] = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                            GetDefaultForm(CIMNumber);
                        }

                        RadGrid1.Rebind();


                        if (Request["CIM"] != null) //Add Review Thru My Team
                        {
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));
                            GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));

                            RadCoacherSignOff.Visible = true;
                        }
                        //else if (SomeGlobalVariables.CIMMyTeam != null)
                        else if (sgv.CIMMyTeam != null)
                        {
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));
                            GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));
                        }

                        VisibleCheckBox();

                        if (Request.QueryString["code"] != null)
                        {
                            googledetails.Visible = true;
                            btngoogle.Visible = false;
                            //GetToken(Request.QueryString["code"].ToString(), SomeGlobalVariables.CIMMyTeam);  
                            GetToken(Request.QueryString["code"].ToString(), sgv.CIMMyTeam);
                        }

                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                LoadCommitment();
            }

        }
        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }
        private bool IsPH(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }
        }
        public void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetDropDownSessions " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void GetAccounts(int CimNumber)
        {
            try
            {
                string Role;
                Role = CheckIfHRQA(CimNumber);

                if (Role == "HR")
                {
                    GetAccount(CimNumber);
                }
                else
                {
                    DataTable dt = null;
                    DataAccess ws = new DataAccess();
                    dt = ws.GetSubordinates2(CimNumber);

                    var distinctRows = (from DataRow dRow in dt.Rows
                                        select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                    RadAccount.Items.Clear();

                    foreach (var row in distinctRows)
                    {
                        RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetAccounts " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected string CheckIfHRQA(int CimNumber)
        {

            DataAccess ws = new DataAccess();
            string Role = ws.CheckIfQAHR(CimNumber);
            if (Role != "")
            {
                return Role;
            }
            else
            {
                return Role;
            }
        }
        public void GetAccount(int CIMNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "Account";
                RadAccount.DataValueField = "AccountID";
                RadAccount.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetAccount " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        {
            try
            {
                string Role;
                Role = CheckIfHRQA(CimNumber);

                if (Role == "HR")
                {
                    GetTLsPerAccount(AccountID);
                }
                else
                {

                    RadSupervisor.Items.Clear();

                    DataSet dsInfo = null;
                    DataAccess wsInfo = new DataAccess();
                    dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
                    if (dsInfo.Tables[0].Rows.Count > 0)
                    {
                        string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                        string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                        string FullName = FirstName + " " + LastName;
                        RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetSubordinatesWithTeam " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        public void GetTLsPerAccount(int AccountID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount(AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetTLsPerAccount " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSupervisors(Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
            RadSupervisor.Text = string.Empty;
            RadSupervisor.ClearSelection();
            RadSupervisor.SelectedIndex = -1;
            RadSessionType.Text = string.Empty;
            RadSessionType.ClearSelection();
            RadSessionType.SelectedIndex = -1;
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;
            GetDropDownSessions();
            GetForm(Convert.ToInt32(Session["DefaultUserAcct"].ToString()));
        }
        public void LoadTTSupervisors(int AccountID)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

                int CIMNumber = Convert.ToInt32(cim_num);

                string Role;
                Role = CheckIfHRQA(CIMNumber);

                if (Role == "HR")
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetTeamLeaderPerAccount(AccountID);

                    RadSupervisor.DataSource = ds;
                    RadSupervisor.DataTextField = "Name";
                    RadSupervisor.DataValueField = "CimNumber";
                    RadSupervisor.DataBind();
                }
                else
                {
                    GetSubordinatesWithTeam(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadAccount.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadTTSupervisors " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
        }



        public void LoadTTSubordinates(int CimNumber, int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinatesWithAccounts2(CimNumber, AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadTTSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadCoacheeName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadCoacheeSignOff.Visible = false;
            try
            {
                DataSet dsInfo = null;
                DataAccess wsInfo = new DataAccess();
                string cim = HttpContext.Current.User.Identity.Name.Split('|')[3].ToString();
                dsInfo = wsInfo.GetSubordinatesSSOv2(Convert.ToInt32(cim), Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()));

                if (dsInfo.Tables[0].Rows.Count > 0)
                {
                    LoadNeededDetails();

                    string AccountID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["AccountID"].ToString() : "";
                    string Account = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["account"].ToString() : "";

                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Account, AccountID));
                    RadAccount.SelectedValue = AccountID;
                    RadAccount.Enabled = false;

                    string SiteID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["CompanySiteID"].ToString() : "";
                    string SiteName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["companysite"].ToString() : "";
                    RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(SiteName, SiteID));
                    RadSite.SelectedValue = SiteID;
                    RadSite.Enabled = false;

                    RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Account, AccountID));
                    RadCampaign.SelectedValue = AccountID;
                    RadCampaign.Enabled = false;

                    string Division = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                    string Division2 = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                    RadDivision.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Division, Division2));
                    RadDivision.SelectedValue = Division2;
                    RadDivision.Enabled = false;

                    string LOBID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOBID"].ToString() : "";
                    string LOBName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOB"].ToString() : "";
                    RadLOB.Items.Add(new Telerik.Web.UI.RadComboBoxItem(LOBName, LOBID));
                    RadLOB.SelectedValue = LOBID;
                    RadLOB.Enabled = false;

                    string FullName = dsInfo.Tables[0].Rows[0]["Name"].ToString();
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString()));
                    RadCoacheeName.SelectedValue = dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString();
                    RadCoacheeName.Enabled = false;
                    SignOut.Attributes["style"] = "";
                    btngoogle.Visible = false;
                }


            }
            catch (Exception ex)
            {
                string ModalLabel = "RadCoacheeName_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void SetLabels(int CIMNumber)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));


                if (ds.Tables[0].Rows.Count > 0)
                {

                    string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();

                    string AccountID = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                    string Account = ds.Tables[0].Rows[0]["Account"].ToString();

                    string SupervisorID = ds.Tables[0].Rows[0]["Reports_To"].ToString();
                    string SupervisorFirstName = ds.Tables[0].Rows[0]["SupFirst Name"].ToString();
                    string SupervisorLastName = ds.Tables[0].Rows[0]["SupLast Name"].ToString();

                    string FullName = FirstName + " " + LastName;
                    string SupFullName = SupervisorFirstName + " " + SupervisorLastName;

                    RadAccount.AllowCustomText = true;
                    RadAccount.Text = Account;
                    RadAccount.SelectedValue = AccountID;
                    RadCoacheeName.AllowCustomText = true;
                    RadCoacheeName.SelectedValue = CIMNumber.ToString();
                    RadCoacheeName.Text = FullName;
                    RadSupervisor.AllowCustomText = true;
                    RadSupervisor.SelectedValue = SupervisorID;
                    RadSupervisor.Text = SupFullName;
                    RadAccount.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeCIM.Text = CIMNumber.ToString();
                    Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                    ctrlA.Text = FullName + " > Add Review";
                    ctrlA.Visible = true;


                    SelectCoacheeGroup.Visible = false;

                }

                //if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue)))
                //{
                //    RadCoacheeCIM.Text = RadCoacheeName.SelectedValue;
                //    RadCoacheeCIM.Enabled = false;
                //    RadSSN.Visible = true;
                //}
                //else
                //{
                //    RadCoacheeCIM.Text = "";
                //    RadSSN.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                string ModalLabel = "SetLabels " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = lstEmployee;
        }
        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllData();
                lstEmployee.Insert(0, new Employee() { UniqueID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid1.Rebind();
            }

            if (e.CommandName == "Delete")
            {
                Guid itemID = (Guid)(((GridDataItem)e.Item).GetDataKeyValue("UniqueID"));
                foreach (var n in lstEmployee.Where(p => p.UniqueID == itemID).ToArray()) lstEmployee.Remove(n);
                RadGrid1.Rebind();


                string ModalLabel = "The selected performance result has been removed";
                string ModalHeader = "Removing performance result";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (lstEmployee.Count == 0)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid1.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid1.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            commandItem.FireCommandEvent("InitInsert", null);
                        }
                    }
                    else
                    {
                        string ModalLabel = "Please do not leave any field blank in Performance Results";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }

        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.Item;
            //Using DataKeyValue to access the Id
            string cname = dataItem.GetDataKeyValue("UniqueID").ToString();
            //OR
            //Accessing the TemplateColumn Label and get Id
            //Label lblId = (Label)dataItem.FindControl("IdLabel");

            string ModalLabel = "Deleting: " + cname;
            string ModalHeader = "Deleting";



            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        }



        protected void saveAllData()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
            {
                Guid UniqueID = new Guid(item.GetDataKeyValue("UniqueID").ToString());
                Employee emp = lstEmployee.Where(i => i.UniqueID == UniqueID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedItem.Value); //.SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
            }
        }
        protected void OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem insertitem = (GridDataItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsTarget = new DataSet();
                            DataAccess wsTarget = new DataAccess();
                            dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                            RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }
                    }
                }
            }
            else
            {
                if (e.Item is GridDataItem)
                {
                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridDataItem insertitem = (GridDataItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsTarget = new DataSet();
                                DataAccess wsTarget = new DataAccess();
                                dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                                RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }
                        }
                    }
                }
            }

        }
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {

            if (lstEmployee.Count == 0)
            {
                if (!IsPostBack)
                {
                    GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    commandItem.FireCommandEvent("InitInsert", null);
                }
            }
            else
            {
                int itemcount = 0;
                foreach (GridDataItem item in RadGrid1.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid1.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            if (!IsPostBack)
                            {
                                GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                                commandItem.FireCommandEvent("InitInsert", null);
                            }
                        }

                    }
                }
            }

        }
        protected void OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();

                    /*if (edititem.ItemIndex == 0)
                    {
                        edititem["DeleteColumn"].Visible = false;
                    }*/

                    if (a != "0")
                    {
                        selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                        combo.Items.Add(selectedItem);
                        selectedItem.DataBind();
                        Session["KPI"] = selectedItem.Value;
                        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIList();
                        comboBrand.DataSource = ds;
                        comboBrand.DataBind();
                        comboBrand.SelectedValue = selectedItem.Value;
                    }
                    if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                    {

                        RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                        DataSet dsTarget = null;
                        DataAccess wsTarget = new DataAccess();
                        dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                        target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                        RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                        RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                        string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                        if (b != "0")
                        {
                            selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                            combodriver.Items.Add(selectedItemdriver);
                            selectedItemdriver.DataBind();
                            Session["Driver"] = selectedItemdriver.Value;
                            RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                            DataSet dsdriver = new DataSet();
                            DataAccess wsdriver = new DataAccess();
                            dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                            comboBranddriver.DataSource = dsdriver;
                            comboBranddriver.DataTextField = "Description";
                            comboBranddriver.DataValueField = "Driver_ID";
                            comboBranddriver.DataBind();
                            comboBranddriver.SelectedValue = selectedItemdriver.Value;
                        }
                    }
                }
            }
        }
        public void RadKPI_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPI_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void RadKPINexidia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPINexidia_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }

        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void RadGrid4_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid4.DataSource = lstNexidia;
        }
        protected void RadGrid4_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllDataNexidia();
                lstNexidia.Insert(0, new Nexidia() { NexidiaID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid4.Rebind();
            }
        }
        protected void saveAllDataNexidia()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid4.MasterTableView.Items)
            {
                Guid NexidiaID = new Guid(item.GetDataKeyValue("NexidiaID").ToString());
                Nexidia emp = lstNexidia.Where(i => i.NexidiaID == NexidiaID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI4") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI4") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadDriver") as RadComboBox).SelectedItem.Text;

            }
        }
        protected void RadGrid4_OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem insertitem = (GridDataItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI4");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsTarget = null;
                            DataAccess wsTarget = new DataAccess();
                            dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                            RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }
                    }

                }
            }
            else
            {
                if (e.Item is GridDataItem)
                {
                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridDataItem insertitem = (GridDataItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI4");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsTarget = null;
                                DataAccess wsTarget = new DataAccess();
                                dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                                RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }
                        }
                    }

                }
            }
        }
        protected void RadGrid4_PreRender(object sender, EventArgs e)
        {
            if (lstNexidia.Count == 0)
            {
                if (!IsPostBack)
                {
                    GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    commandItem.FireCommandEvent("InitInsert", null);
                }
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid4.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid4.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI4") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            if (!IsPostBack)
                            {
                                GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                                commandItem.FireCommandEvent("InitInsert", null);
                            }
                        }

                    }
                }


            }
        }
        protected void RadGrid4_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI4");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    if (a != "0")
                    {
                        selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                        combo.Items.Add(selectedItem);
                        selectedItem.DataBind();
                        Session["KPI"] = selectedItem.Value;
                        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI4");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIList();
                        comboBrand.DataSource = ds;
                        comboBrand.DataBind();
                        comboBrand.SelectedValue = selectedItem.Value;
                    }

                    if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                    {
                        RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                        DataSet dsTarget = null;
                        DataAccess wsTarget = new DataAccess();
                        dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                        target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                        RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                        RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                        string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                        if (b != "0")
                        {
                            selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                            combodriver.Items.Add(selectedItemdriver);
                            selectedItemdriver.DataBind();
                            Session["Driver"] = selectedItemdriver.Value;
                            RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                            DataSet dsdriver = new DataSet();
                            DataAccess wsdriver = new DataAccess();
                            dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                            comboBranddriver.DataSource = dsdriver;
                            comboBranddriver.DataTextField = "Description";
                            comboBranddriver.DataValueField = "Driver_ID";
                            comboBranddriver.DataBind();
                            comboBranddriver.SelectedValue = selectedItemdriver.Value;
                        }
                    }
                }
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            if (lstNexidia.Count == 0)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid4.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid4.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI4") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            commandItem.FireCommandEvent("InitInsert", null);
                        }
                    }
                    else
                    {
                        string ModalLabel = "Please do not leave any field blank in Performance Results";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            GetDropDownTopics(Convert.ToInt32(RadSessionType.SelectedValue), CIMNumber);
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;

            if (Request["CIM"] != null)
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));
                }
            }

        }
        protected void GetDropDownTopics(int SessionID, int CimNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID, CimNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void VisibleCheckBox()
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (RadSessionTopic.SelectedIndex == -1)
                {
                    if (Account == "Default")
                    {

                        CNPR.Visible = false;
                        CheckBox1.Visible = false;
                        CheckBox2.Visible = false;
                        AddReviewDefault.Visible = true;
                        RadSearchBlackout.Visible = false;
                        PerfResult.Visible = true;
                        CMTView.Visible = false;
                        FollowUp.Visible = true;
                        SignOut.Visible = true;
                        RadCMTPreview.Visible = false;
                        RadCMTSaveSubmit.Visible = false;
                        DefaultPane1.Visible = true;
                        DefaultPane3.Visible = true;
                        DefaultPane5.Visible = true;
                    }
                    else if (Account == "TalkTalk")
                    {

                        CNPR.Visible = false;
                        CheckBox1.Visible = false;
                        CheckBox2.Visible = false;
                        AddReviewDefault.Visible = true;
                        RadSearchBlackout.Visible = false;
                        PerfResult.Visible = true;
                        CMTView.Visible = false;
                        FollowUp.Visible = true;
                        SignOut.Visible = true;
                        RadCMTPreview.Visible = false;
                        RadCMTSaveSubmit.Visible = false;
                        NexidiaForm.Visible = true;
                        Div6.Visible = true;
                        Div12.Visible = true;
                        Div15.Visible = true;
                        Panel9.Visible = false;
                        DefaultPane1.Visible = true;
                        DefaultPane3.Visible = true;
                        DefaultPane5.Visible = true;
                        Div7.Visible = false;
                        //Div6.Attributes["style"] = "display: none;";
                    }
                    else
                    {

                        CNPR.Visible = false;
                        CheckBox1.Visible = false;
                        CheckBox2.Visible = false;
                        AddReviewDefault.Visible = true;
                        RadSearchBlackout.Visible = false;
                        PerfResult.Visible = true;
                        CMTView.Visible = false;
                        FollowUp.Visible = true;
                        SignOut.Visible = true;
                        RadCMTPreview.Visible = false;
                        RadCMTSaveSubmit.Visible = false;
                        ODForm.Visible = true;
                        Div23.Visible = true;
                        Div27.Visible = false;
                        Div21.Visible = false;
                        CheckBox2.Visible = false;
                        DefaultPane1.Visible = true;
                        DefaultPane3.Visible = true;
                        DefaultPane5.Visible = true;
                    }



                }


                if (Session["Account"] != null)
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    if (RadSessionTopic.SelectedIndex != -1)
                    {
                        if (RadSessionTopic.SelectedItem.Text != "Awareness")
                        {

                            if (RadSessionTopic.SelectedItem.Text == "Termination")
                            {
                                btngoogle.Visible = false;
                                CNPR.Visible = false;
                                CheckBox1.Visible = true;
                                CheckBox2.Visible = true;
                                AddReviewDefault.Visible = false;
                                RadSearchBlackout.Visible = true;
                                PerfResult.Visible = false;
                                CMTView.Visible = true;
                                LoadCMTDropdowns();
                                FollowUp.Visible = false;
                                SignOut.Visible = false;
                                RadCMTSaveSubmit.Visible = true;
                                RadCMTPreview.Visible = true;
                                RadSessionTopic.Enabled = false;
                                RadSessionType.Enabled = false;
                                NexidiaForm.Visible = false;
                                ODForm.Visible = false;
                                RadHRRep.Text = CIMNumber.ToString();
                                DefaultPane1.Visible = false;
                                DefaultPane3.Visible = false;
                                DefaultPane5.Visible = false;
                                SelectSearchBlackout.Visible = true;
                                SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-5";
                                SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-5";
                                SelectCoacheeGroup.Attributes["style"] = "padding:0px";
                                SelectCoachingSpecifics.Attributes["style"] = "padding:0px";
                                SelectSearchBlackout.Attributes["style"] = "padding:0px";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMT(); });", true);
                            }
                            else
                            {
                                if (Account == "Default")
                                {

                                    CNPR.Visible = true;
                                    CheckBox1.Visible = true;
                                    CheckBox2.Visible = true;
                                    CMTView.Visible = false;
                                    FollowUp.Visible = true;
                                    SignOut.Visible = true;
                                    RadCMTPreview.Visible = false;
                                    RadCMTSaveSubmit.Visible = false;
                                    ODForm.Visible = false;
                                    CheckBox2.Visible = true;
                                }
                                else if (Account == "TalkTalk")
                                {

                                    CNPR.Visible = true;
                                    CheckBox1.Visible = true;
                                    CheckBox2.Visible = true;
                                    CMTView.Visible = false;
                                    FollowUp.Visible = true;
                                    SignOut.Visible = true;
                                    RadCMTPreview.Visible = false;
                                    RadCMTSaveSubmit.Visible = false;
                                    NexidiaForm.Visible = true;
                                    ODForm.Visible = false;
                                    Div6.Visible = true;
                                    Div12.Visible = true;
                                    Div15.Visible = true;
                                    Panel9.Visible = true;
                                    CheckBox2.Visible = true;
                                    //Div6.Attributes["style"] = "";
                                }
                                else
                                {

                                    CNPR.Visible = true;
                                    CheckBox1.Visible = true;
                                    CheckBox2.Visible = true;
                                    CMTView.Visible = false;
                                    FollowUp.Visible = true;
                                    SignOut.Visible = true;
                                    RadCMTPreview.Visible = false;
                                    RadCMTSaveSubmit.Visible = false;
                                    ODForm.Visible = true;
                                    NexidiaForm.Visible = false;
                                    Div23.Visible = true;
                                    Div27.Visible = true;
                                    Div21.Visible = true;
                                    CheckBox2.Visible = false;
                                }




                            }
                        }
                        else
                        {
                            if (Account == "Default")
                            {

                                CNPR.Visible = false;
                                CheckBox1.Visible = false;
                                CheckBox2.Visible = false;
                                AddReviewDefault.Visible = true;
                                RadSearchBlackout.Visible = false;
                                PerfResult.Visible = true;
                                CMTView.Visible = false;
                                FollowUp.Visible = true;
                                SignOut.Visible = true;
                                RadCMTPreview.Visible = false;
                                RadCMTSaveSubmit.Visible = false;
                                DefaultPane1.Visible = true;
                                DefaultPane3.Visible = true;
                                DefaultPane5.Visible = true;
                            }
                            else if (Account == "TalkTalk")
                            {

                                CNPR.Visible = false;
                                CheckBox1.Visible = false;
                                CheckBox2.Visible = false;
                                AddReviewDefault.Visible = true;
                                RadSearchBlackout.Visible = false;
                                PerfResult.Visible = true;
                                CMTView.Visible = false;
                                FollowUp.Visible = true;
                                SignOut.Visible = true;
                                RadCMTPreview.Visible = false;
                                RadCMTSaveSubmit.Visible = false;
                                NexidiaForm.Visible = true;
                                Div6.Visible = true;
                                Div12.Visible = true;
                                Div15.Visible = true;
                                Panel9.Visible = false;
                                DefaultPane1.Visible = true;
                                DefaultPane3.Visible = true;
                                DefaultPane5.Visible = true;
                                Div7.Visible = false;
                            }
                            else
                            {

                                CNPR.Visible = false;
                                CheckBox1.Visible = false;
                                CheckBox2.Visible = false;
                                AddReviewDefault.Visible = true;
                                RadSearchBlackout.Visible = false;
                                PerfResult.Visible = true;
                                CMTView.Visible = false;
                                FollowUp.Visible = true;
                                SignOut.Visible = true;
                                RadCMTPreview.Visible = false;
                                RadCMTSaveSubmit.Visible = false;
                                ODForm.Visible = true;
                                Div23.Visible = true;
                                Div27.Visible = false;
                                Div21.Visible = false;
                                CheckBox2.Visible = false;
                                DefaultPane1.Visible = true;
                                DefaultPane3.Visible = true;
                                DefaultPane5.Visible = true;
                            }
                        }
                    }
                }
                else
                {
                    RadSessionTopic.Text = string.Empty;
                    RadSessionTopic.ClearSelection();
                    RadSessionTopic.SelectedIndex = -1;
                    string myStringVariable = "Please select the account first.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "VisibleCheckBox " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadGrid8_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }

        public void LoadCMTDropdowns()
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetCMTDropdowns(1, 0);

            RadCaseLevel.DataSource = ds;
            RadCaseLevel.DataTextField = "Case_Name";
            RadCaseLevel.DataValueField = "Case_ID";
            RadCaseLevel.DataBind();

            DataSet ds2 = null;
            DataAccess ws2 = new DataAccess();
            ds2 = ws2.GetCMTDropdowns(2, 0);

            RadMajorCategory.DataSource = ds2;
            RadMajorCategory.DataTextField = "MJ_Name";
            RadMajorCategory.DataValueField = "MJ_ID";
            RadMajorCategory.DataBind();

            DataSet ds3 = null;
            DataAccess ws3 = new DataAccess();
            ds3 = ws3.GetCMTDropdowns(4, 0);

            RadStatus.DataSource = ds3;
            RadStatus.DataTextField = "Status";
            RadStatus.DataValueField = "ID";
            RadStatus.DataBind();

            DataSet ds4 = null;
            DataAccess ws4 = new DataAccess();
            ds4 = ws4.GetCMTDropdowns(5, 0);

            RadCaseDecision.DataSource = ds4;
            RadCaseDecision.DataTextField = "Decision_Name";
            RadCaseDecision.DataValueField = "ID";
            RadCaseDecision.DataBind();

            DataSet ds5 = null;
            DataAccess ws5 = new DataAccess();
            ds5 = ws5.GetCMTDropdowns(6, 0);

            RadHoldCaseType.DataSource = ds5;
            RadHoldCaseType.DataTextField = "Hold_Case_Type";
            RadHoldCaseType.DataValueField = "ID";
            RadHoldCaseType.DataBind();

            RadNODHoldCaseType.DataSource = ds5;
            RadNODHoldCaseType.DataTextField = "Hold_Case_Type";
            RadNODHoldCaseType.DataValueField = "ID";
            RadNODHoldCaseType.DataBind();

            DataSet ds6 = null;
            DataAccess ws6 = new DataAccess();
            ds6 = ws6.GetCMTDropdowns(7, 0);


            RadNODNonAcceptance.DataSource = ds6;
            RadNODNonAcceptance.DataTextField = "Non_Acceptance_Reason";
            RadNODNonAcceptance.DataValueField = "ID";
            RadNODNonAcceptance.DataBind();


            RadNonAcceptance.DataSource = ds6;
            RadNonAcceptance.DataTextField = "Non_Acceptance_Reason";
            RadNonAcceptance.DataValueField = "ID";
            RadNonAcceptance.DataBind();

            DataSet ds7 = null;
            DataAccess ws7 = new DataAccess();
            ds7 = ws7.GetCMTDropdowns(8, 0);


            RadEoW.DataSource = ds7;
            RadEoW.DataTextField = "Act_Rqrd";
            RadEoW.DataValueField = "ID";
            RadEoW.DataBind();
        }
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            VisibleCheckBox();
        }
        public void GetForm(int AccountID)
        {
            int Account = 0;
            DataAccess ws = new DataAccess();
            Account = ws.GetAccountType(Convert.ToInt32(AccountID));
            SelectSearchBlackout.Visible = false;
            SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-6";
            SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-6";
            if (Account == 2)
            {
                DefaultForm.Visible = false;
                NexidiaForm.Visible = true;
                ODForm.Visible = false;
                CMTView.Visible = false;
                GetSessionFocus("Nexidia");
                RadCMTPreview.Visible = false;
                RadCMTSaveSubmit.Visible = false;
                CallID.Visible = false;
                CallIDDiv.Visible = false;
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                Session["Account"] = "TalkTalk";
                SessionFocusGroup.Visible = true;
                RadGrid4.Enabled = true;
                RadGrid4.Rebind();
                RadGrid4.MasterTableView.Rebind();
                CoacheeDetails.Visible = false;
                DivAccount.Visible = true;
            }
            else if (Account == 3)
            {
                DefaultForm.Visible = false;
                NexidiaForm.Visible = false;
                ODForm.Visible = true;
                CMTView.Visible = false;
                CallID.Visible = true;
                CallIDDiv.Visible = true;
                GetSessionFocus("OD");
                RadCMTPreview.Visible = false;
                RadCMTSaveSubmit.Visible = false;
                LoadCommitmentQuestions();
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                Session["Account"] = "OD";
                SessionFocusGroup.Visible = true;
                RadGrid9.Enabled = true;
                RadGrid9.Rebind();
                RadGrid9.MasterTableView.Rebind();
                CoacheeDetails.Visible = true;
                DivAccount.Visible = false;
            }
            else
            {
                DefaultForm.Visible = true;
                NexidiaForm.Visible = false;
                ODForm.Visible = false;
                CMTView.Visible = false;
                CallID.Visible = false;
                RadCMTPreview.Visible = false;
                RadCMTSaveSubmit.Visible = false;
                CallIDDiv.Visible = false;
                SessionFocusGroup.Visible = false;
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                Session["Account"] = "Default";
                RadGrid1.Enabled = true;
                RadGrid1.Rebind();
                RadGrid1.MasterTableView.Rebind();
                CoacheeDetails.Visible = false;
                DivAccount.Visible = true;
            }
        }
        protected bool CheckIfOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public void GetSessionFocus(string Account)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionFocus(Account);
                CheckBoxList1.DataSource = ds;
                CheckBoxList1.DataTextField = "SessionFocusName";
                CheckBoxList1.DataValueField = "ID";
                CheckBoxList1.DataBind();
            }

            catch (Exception ex)
            {
                string ModalLabel = "GetSessionFocus " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadGrid12_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["Account"] != null)
            {
                string Account = Session["Account"].ToString();
                if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
                {

                    if (RadSessionType.SelectedIndex != -1 || RadSessionType.AllowCustomText == true)
                    {
                        if (Account == "Default")
                        {
                            if (CheckBox1.Checked == true)
                            {
                                CoachingNotes();
                                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                                DataSet ds = null;
                                DataAccess ws = new DataAccess();
                                int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                                ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    RadGrid2.DataSource = ds;
                                    RadGrid2.DataBind();
                                    RadGrid2.Visible = true;
                                    RadGrid3.Visible = false;
                                    RadPRLink.Visible = false;
                                    RadPRCancel.Visible = false;
                                    RadCNLink.Visible = true;
                                    RadCNCancel.Visible = true;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                    RadGridCN.Visible = true;

                                }
                                else
                                {
                                    string ModalLabel = "No data for previous coaching notes.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }

                            }

                            else
                            {
                                RadGridCN.DataSource = null;
                                RadGridCN.DataBind();
                                RadGridCN.Visible = false;

                            }
                        }
                        else if (Account == "TalkTalk")
                        {

                            if (CheckBox1.Checked == true)
                            {
                                CoachingNotes();
                                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                                DataSet ds = null;
                                DataAccess ws = new DataAccess();
                                int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                                ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    RadGrid2.DataSource = ds;
                                    RadGrid2.DataBind();
                                    RadGrid2.Visible = true;
                                    RadGrid3.Visible = false;
                                    RadPRLink.Visible = false;
                                    RadPRCancel.Visible = false;
                                    RadCNLink.Visible = true;
                                    RadCNCancel.Visible = true;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                    RadGrid6.Visible = true;


                                }
                                else
                                {

                                    string ModalLabel = "No data for previous coaching notes.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }
                            }

                            else
                            {
                                RadGrid6.DataSource = null;
                                RadGrid6.DataBind();
                                RadGrid6.Visible = false;

                            }
                        }
                        else
                        {
                            if (CheckBox1.Checked == true)
                            {
                                CoachingNotes();
                                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                                DataSet ds = null;
                                DataAccess ws = new DataAccess();
                                int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                                ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    RadGrid2.DataSource = ds;
                                    RadGrid2.DataBind();
                                    RadGrid2.Visible = true;
                                    RadGrid3.Visible = false;
                                    RadPRLink.Visible = false;
                                    RadPRCancel.Visible = false;
                                    RadCNLink.Visible = true;
                                    RadCNCancel.Visible = true;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                    RadGrid11.Visible = true;

                                }
                                else
                                {

                                    string ModalLabel = "No data for previous coaching notes.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }
                            }

                            else
                            {
                                RadGrid11.DataSource = null;
                                RadGrid11.DataBind();
                                RadGrid11.Visible = false;

                                Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                                PreviousCommitment.Controls.Remove(Test2);

                            }
                        }
                    }
                    else
                    {
                        CheckBox1.Checked = false;
                        string ModalLabel = "Please select the session type.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                }
                else
                {
                    CheckBox1.Checked = false;
                    string ModalLabel = "Please select the employee first.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }
            }
            else
            {
                CheckBox1.Checked = false;
                string ModalLabel = "Please select the account first.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
            BindListViewCNCMT();
        }
        private void BindListViewCN()
        {
            string Account = Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (Account == "Default")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridCN.DataSource = dt;
                    RadGridCN.DataBind();
                }
                else
                {
                    RadGridCN.DataSource = null;
                    RadGridCN.DataBind();
                }
            }
            else if (Account == "TalkTalk")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid6.DataSource = dt;
                    RadGrid6.DataBind();
                }
                else
                {
                    RadGrid6.DataSource = null;
                    RadGrid6.DataBind();
                }
            }
            else
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid11.DataSource = dt;
                    RadGrid11.DataBind();
                }
                else
                {
                    RadGrid11.DataSource = null;
                    RadGrid11.DataBind();
                }
            }

        }
        private void BindListViewCNCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            Session.Add("CMTCN", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridCNCMT.DataSource = dt;
                RadGridCNCMT.DataBind();
            }
            else
            {
                RadGridCNCMT.DataSource = null;
                RadGridCNCMT.DataBind();
            }
        }
        protected void RadCNLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedIndex != -1)
            {
                if (RadSessionTopic.SelectedItem.Text != "Termination")
                {
                    string Account = Session["Account"].ToString();
                    if (Account == "Default")
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            HyperLink link = (HyperLink)item["ReviewID"].Controls[0];
                            id = link.Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();
                        }
                    }
                    else if (Account == "TalkTalk")
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            HyperLink link = (HyperLink)item["ReviewID"].Controls[0];
                            id = link.Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();
                        }

                    }
                    else
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            HyperLink link = (HyperLink)item["ReviewID"].Controls[0];
                            id = link.Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();

                        }
                        int count = RadGrid2.SelectedItems.Count;
                        LoadPreviousCommitment(count);

                    }
                }

                else
                {

                    string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid2.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        HyperLink link = (HyperLink)item["ReviewID"].Controls[0];
                        id = link.Text;
                        chec = chk.Checked;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        session = item["Session"].Text;
                        topic = item["Topic"].Text;
                        reviewdate = item["ReviewDate"].Text;
                        assignedby = item["AssignedBy"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["CN"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["ReviewID"] = id;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Session"] = session;
                        dr["Topic"] = topic;
                        dr["ReviewDate"] = reviewdate;
                        dr["AssignedBy"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["CN"] = dt;
                        }
                        this.BindListViewCNCMT();
                    }

                }
                ViewState["CN"] = null;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeModal(); });", true);
            }
            else
            {
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadPreviousCommitment(int count)
        {

            DataTable dt = (DataTable)ViewState["CN"];
            foreach (DataRow row in dt.Rows)
            {
                string id = row.Field<string>("ReviewID");
                Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                Test2.ReviewIDSelected = Convert.ToInt32(id);
                PreviousCommitment.Controls.Add(Test2);
            }
        }
        protected void RadCNCancel_Click(object sender, EventArgs e)
        {
            string Account = Session["Account"].ToString();
            CheckBox1.Checked = false;
            if (Account == "Default")
            {
                RadGridCN.DataSource = null;
                RadGridCN.DataBind();
            }
            else if (Account == "TalkTalk")
            {
                RadGrid6.DataSource = null;
                RadGrid6.DataBind();
            }
            else
            {
                RadGrid11.DataSource = null;
                RadGrid11.DataBind();
            }
        }
        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["Account"] != null)
            {
                string Account = Session["Account"].ToString();
                if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
                {
                    if (Account == "Default")
                    {

                        if (CheckBox2.Checked == true)
                        {
                            PerformanceResults();
                            int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetPerformanceResults2(CoachingCIM);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                RadGrid3.DataSource = ds;
                                RadGrid3.DataBind();
                                RadGrid3.Visible = true;
                                RadGrid2.Visible = false;
                                RadPRLink.Visible = true;
                                RadPRCancel.Visible = true;
                                RadCNLink.Visible = false;
                                RadCNCancel.Visible = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                RadGridPR.Visible = true;

                            }
                            else
                            {
                                string ModalLabel = "No data for previous performance results.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                            }
                        }

                        else
                        {
                            RadGridPR.DataSource = null;
                            RadGridPR.DataBind();
                            RadGridPR.Visible = false;

                        }
                    }
                    else if (Account == "TalkTalk")
                    {
                        if (CheckBox2.Checked == true)
                        {
                            PerformanceResults();
                            int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetPerformanceResults2(CoachingCIM);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                RadGrid3.DataSource = ds;
                                RadGrid3.DataBind();
                                RadGrid3.Visible = true;
                                RadGrid2.Visible = false;
                                RadPRLink.Visible = true;
                                RadPRCancel.Visible = true;
                                RadCNLink.Visible = false;
                                RadCNCancel.Visible = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                RadGridPR.Visible = true;
                            }
                            else
                            {
                                string ModalLabel = "No data for previous performance results.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                            }
                        }

                        else
                        {
                            RadGridPR.DataSource = null;
                            RadGridPR.DataBind();
                            RadGridPR.Visible = false;

                        }
                    }

                    else
                    {
                        CheckBox2.Checked = false;
                        string ModalLabel = "Please select the employee first.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                }
                else
                {
                    CheckBox2.Checked = false;
                    string ModalLabel = "Please select the account first.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }

            }
        }
        public void PerformanceResults()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("Review KPI ID", typeof(string));
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Name", typeof(string));
            dt2.Columns.Add("Target", typeof(string));
            dt2.Columns.Add("Current", typeof(string));
            dt2.Columns.Add("Previous", typeof(string));
            dt2.Columns.Add("Driver Name", typeof(string));
            dt2.Columns.Add("Review Date", typeof(string));
            dt2.Columns.Add("Assigned By", typeof(string));
            ViewState["PR"] = dt2;

            BindListViewPR();
            BindListViewPRCMT();
        }
        private void BindListViewPR()
        {
            string Account = Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            if (Account == "Default")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridPR.DataSource = dt;
                    RadGridPR.DataBind();
                }
                else
                {
                    RadGridPR.DataSource = null;
                    RadGridPR.DataBind();
                }
            }
            else if (Account == "TalkTalk")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid7.DataSource = dt;
                    RadGrid7.DataBind();
                }
                else
                {
                    RadGrid7.DataSource = null;
                    RadGrid7.DataBind();
                }
            }

        }
        private void BindListViewPRCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            Session.Add("CMTPR", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridPRCMT.DataSource = dt;
                RadGridPRCMT.DataBind();
            }
            else
            {
                RadGridPRCMT.DataSource = null;
                RadGridPRCMT.DataBind();
            }


        }
        protected void RadPRLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedIndex != -1)
            {

                if (RadSessionTopic.SelectedItem.Text != "Termination")
                {
                    string Account = Session["Account"].ToString();
                    if (Account == "Default")
                    {
                        string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid3.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["Review KPI ID"].Text;
                            chec = chk.Checked;
                            coachingid = item["ID"].Text;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            name = item["Name"].Text;
                            target = item["Target"].Text;
                            current = item["Current"].Text;
                            previous = item["Previous"].Text;
                            drivername = item["Driver Name"].Text;
                            reviewdate = item["Review Date"].Text;
                            assignedby = item["Assigned By"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["PR"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["Review KPI ID"] = id;
                            dr["ReviewID"] = coachingid;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Name"] = name;
                            dr["Target"] = target;
                            dr["Current"] = current;
                            dr["Previous"] = previous;
                            dr["Driver Name"] = drivername;
                            dr["Review Date"] = reviewdate;
                            dr["Assigned By"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["PR"] = dt;
                            }
                            this.BindListViewPR();
                        }
                    }
                    else if (Account == "TalkTalk")
                    {
                        string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid3.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["Review KPI ID"].Text;
                            chec = chk.Checked;
                            coachingid = item["ID"].Text;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            name = item["Name"].Text;
                            target = item["Target"].Text;
                            current = item["Current"].Text;
                            previous = item["Previous"].Text;
                            drivername = item["Driver Name"].Text;
                            reviewdate = item["Review Date"].Text;
                            assignedby = item["Assigned By"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["PR"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["Review KPI ID"] = id;
                            dr["ReviewID"] = coachingid;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Name"] = name;
                            dr["Target"] = target;
                            dr["Current"] = current;
                            dr["Previous"] = previous;
                            dr["Driver Name"] = drivername;
                            dr["Review Date"] = reviewdate;
                            dr["Assigned By"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["PR"] = dt;
                            }
                            this.BindListViewPR();
                        }
                    }

                }
                else
                {
                    string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid3.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        id = item["Review KPI ID"].Text;
                        chec = chk.Checked;
                        coachingid = item["ID"].Text;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        name = item["Name"].Text;
                        target = item["Target"].Text;
                        current = item["Current"].Text;
                        previous = item["Previous"].Text;
                        drivername = item["Driver Name"].Text;
                        reviewdate = item["Review Date"].Text;
                        assignedby = item["Assigned By"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["PR"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["Review KPI ID"] = id;
                        dr["ReviewID"] = coachingid;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Name"] = name;
                        dr["Target"] = target;
                        dr["Current"] = current;
                        dr["Previous"] = previous;
                        dr["Driver Name"] = drivername;
                        dr["Review Date"] = reviewdate;
                        dr["Assigned By"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["PR"] = dt;
                        }
                        this.BindListViewPRCMT();
                    }



                }
                ViewState["PR"] = null;
            }
            else
            {
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadPRCancel_Click(object sender, EventArgs e)
        {
            string Account = Session["Account"].ToString();
            CheckBox2.Checked = false;
            if (Account == "Default")
            {
                RadGridPR.DataSource = null;
                RadGridPR.DataBind();
            }
            else if (Account == "TalkTalk")
            {
                RadGrid7.DataSource = null;
                RadGrid7.DataBind();
            }

        }
        protected void RadSearchBlackout_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMTv2(); });", true);

        }
        protected void RadSearchBlackoutYes_Click(object sender, EventArgs e)
        {
            HiddenSearchBlackout.Value = "1";
            RadSearchBlackout.Enabled = false;
            RadSearchBlackout.ForeColor = Color.Black;
            RadCMTSaveSubmit.Text = "Save";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);

        }
        protected void RadSearchBlackoutNo_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);
        }
        protected void UploadDummy_Click(object sender, EventArgs e)
        {
            UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
        }
        //protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string Account = Session["Account"].ToString();
        //        string emailAddressCCM, emailCoachee;
        //        if (ValidatePage() == true)
        //        {
        //            if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
        //            {
        //                if (RadSSN.Text != "")
        //                {
        //                    if (CheckSSN() == true)
        //                    {
        //                        if (Convert.ToInt32(RadReviewID.Text) == 0)
        //                        {
        //                            if (Account == "Default")
        //                            {
        //                                DataSet ds1 = null;
        //                                DataAccess ws1 = new DataAccess();
        //                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
        //                                emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                emailAddressCCM = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
        //                                int ReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
        //                                RadReviewID.Text = Convert.ToString(ReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ReviewID)); // update status to coacheesigned off 
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(ReviewID);
        //                                UpdateFormType(ReviewID, 1);
        //                                LoadDocumentationsReview();
        //                                CIM = RadCoacheeCIM.Text;
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                //SendEmail(1, ReviewID, emailCoachee);
        //                                //SendEmail(2, ReviewID, emailAddressCCM);
        //                                string ModalLabel = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;

        //                            }
        //                            else if (Account == "TalkTalk")
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionNexidia.Text != null)
        //                                {
        //                                    Description = RadDescriptionNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsNexidia.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesNexidia.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

        //                                string PositiveBehaviour;
        //                                if (RadPositiveBehaviour.Text != null)
        //                                {
        //                                    PositiveBehaviour = RadPositiveBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    PositiveBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string OpportunityBehaviour;
        //                                if (RadOpportunityBehaviour.Text != null)
        //                                {
        //                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    OpportunityBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string BeginBehaviourComments;
        //                                if (RadBeginBehaviourComments.Text != null)
        //                                {
        //                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    BeginBehaviourComments = DBNull.Value.ToString();
        //                                }

        //                                string BehaviourEffects;
        //                                if (RadBehaviourEffect.Text != null)
        //                                {
        //                                    BehaviourEffects = RadBehaviourEffect.Text;
        //                                }
        //                                else
        //                                {
        //                                    BehaviourEffects = DBNull.Value.ToString();
        //                                }

        //                                string RootCause;
        //                                if (RadRootCause.Text != null)
        //                                {
        //                                    RootCause = RadRootCause.Text;
        //                                }
        //                                else
        //                                {
        //                                    RootCause = DBNull.Value.ToString();
        //                                }

        //                                string ReviewResultsComments;
        //                                if (RadReviewResultsComments.Text != null)
        //                                {
        //                                    ReviewResultsComments = RadReviewResultsComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ReviewResultsComments = DBNull.Value.ToString();
        //                                }

        //                                string AddressOpportunities;
        //                                if (RadAddressOpportunities.Text != null)
        //                                {
        //                                    AddressOpportunities = RadAddressOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    AddressOpportunities = DBNull.Value.ToString();
        //                                }

        //                                string ActionPlanComments;
        //                                if (RadActionPlanComments.Text != null)
        //                                {
        //                                    ActionPlanComments = RadActionPlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ActionPlanComments = DBNull.Value.ToString();
        //                                }

        //                                string SmartGoals;
        //                                if (RadSmartGoals.Text != null)
        //                                {
        //                                    SmartGoals = RadSmartGoals.Text;
        //                                }
        //                                else
        //                                {
        //                                    SmartGoals = DBNull.Value.ToString();
        //                                }

        //                                string CreatePlanComments;
        //                                if (RadCreatePlanComments.Text != null)
        //                                {
        //                                    CreatePlanComments = RadCreatePlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    CreatePlanComments = DBNull.Value.ToString();
        //                                }

        //                                string FollowThrough;
        //                                if (RadFollowThrough.Text != null)
        //                                {
        //                                    FollowThrough = RadFollowThrough.Text;
        //                                }
        //                                else
        //                                {
        //                                    FollowThrough = DBNull.Value.ToString();
        //                                }



        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int NexidiaReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
        //                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(NexidiaReviewID)); // update status to coacheesigned off 
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(NexidiaReviewID);
        //                                UpdateFormType(NexidiaReviewID, 2);;
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string ModalLabel = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;
        //                            }
        //                            else
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionOD.Text != null)
        //                                {
        //                                    Description = RadDescriptionOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsOD.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesOD.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                string CoacherFeedback;
        //                                if (RadCoacherFeedback.Text != null)
        //                                {
        //                                    CoacherFeedback = RadCoacherFeedback.Text;
        //                                }
        //                                else
        //                                {
        //                                    CoacherFeedback = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
        //                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ODReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID);
        //                                RadReviewID.Text = Convert.ToString(ODReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ODReviewID)); // update status to coacheesigned off 
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
        //                                InsertReviewHistory(1);
        //                                LoadKPIReview(ODReviewID);
        //                                UpdateFormType(ODReviewID, 3);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string ModalLabel = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;



        //                            }
        //                        }
        //                        else
        //                        {
        //                            DataAccess ws = new DataAccess();
        //                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                            LoadDocumentationsReview();
        //                            RadCoacheeSignOff.Visible = false;
        //                            string ModalLabel = "Review has been successfully saved.";
        //                            string ModalHeader = "Success";
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
        //                            DisableElements();
        //                            RadSSN.Visible = false;

        //                        }
        //                    }
        //                    else
        //                    {

        //                        string script = "Incorrect last 3 digits of SSN.";
        //                        string ModalHeader = "Error Message";
        //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                        DisableElements();

        //                    }
        //                }


        //                else
        //                {

        //                    string script = "SSN is a required field.";
        //                    string ModalHeader = "Error Message";
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                    DisableElements();
        //                }
        //            }
        //            else
        //            {
        //                if (RadCoacheeCIM.Text != "")
        //                {
        //                    if (Convert.ToInt32(RadCoacheeCIM.Text) == Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()))
        //                    {
        //                        if (Convert.ToInt32(RadReviewID.Text) == 0)
        //                        {
        //                            if (Account == "Default")
        //                            {
        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
        //                                RadReviewID.Text = Convert.ToString(ReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ReviewID)); // update status to coacheesigned off 

        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(ReviewID);
        //                                UpdateFormType(ReviewID, 1);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
        //                                RadCoacheeCIM.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                            }
        //                            else if (Account == "TalkTalk")
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionNexidia.Text != null)
        //                                {
        //                                    Description = RadDescriptionNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsNexidia.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesNexidia.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

        //                                string PositiveBehaviour;
        //                                if (RadPositiveBehaviour.Text != null)
        //                                {
        //                                    PositiveBehaviour = RadPositiveBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    PositiveBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string OpportunityBehaviour;
        //                                if (RadOpportunityBehaviour.Text != null)
        //                                {
        //                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    OpportunityBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string BeginBehaviourComments;
        //                                if (RadBeginBehaviourComments.Text != null)
        //                                {
        //                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    BeginBehaviourComments = DBNull.Value.ToString();
        //                                }

        //                                string BehaviourEffects;
        //                                if (RadBehaviourEffect.Text != null)
        //                                {
        //                                    BehaviourEffects = RadBehaviourEffect.Text;
        //                                }
        //                                else
        //                                {
        //                                    BehaviourEffects = DBNull.Value.ToString();
        //                                }

        //                                string RootCause;
        //                                if (RadRootCause.Text != null)
        //                                {
        //                                    RootCause = RadRootCause.Text;
        //                                }
        //                                else
        //                                {
        //                                    RootCause = DBNull.Value.ToString();
        //                                }

        //                                string ReviewResultsComments;
        //                                if (RadReviewResultsComments.Text != null)
        //                                {
        //                                    ReviewResultsComments = RadReviewResultsComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ReviewResultsComments = DBNull.Value.ToString();
        //                                }

        //                                string AddressOpportunities;
        //                                if (RadAddressOpportunities.Text != null)
        //                                {
        //                                    AddressOpportunities = RadAddressOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    AddressOpportunities = DBNull.Value.ToString();
        //                                }

        //                                string ActionPlanComments;
        //                                if (RadActionPlanComments.Text != null)
        //                                {
        //                                    ActionPlanComments = RadActionPlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ActionPlanComments = DBNull.Value.ToString();
        //                                }

        //                                string SmartGoals;
        //                                if (RadSmartGoals.Text != null)
        //                                {
        //                                    SmartGoals = RadSmartGoals.Text;
        //                                }
        //                                else
        //                                {
        //                                    SmartGoals = DBNull.Value.ToString();
        //                                }

        //                                string CreatePlanComments;
        //                                if (RadCreatePlanComments.Text != null)
        //                                {
        //                                    CreatePlanComments = RadCreatePlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    CreatePlanComments = DBNull.Value.ToString();
        //                                }

        //                                string FollowThrough;
        //                                if (RadFollowThrough.Text != null)
        //                                {
        //                                    FollowThrough = RadFollowThrough.Text;
        //                                }
        //                                else
        //                                {
        //                                    FollowThrough = DBNull.Value.ToString();
        //                                }



        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int NexidiaReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
        //                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(NexidiaReviewID)); // update status to coacheesigned off 
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(NexidiaReviewID);
        //                                UpdateFormType(NexidiaReviewID, 2);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;

        //                            }
        //                            else
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescription.Text != null)
        //                                {
        //                                    Description = RadDescription.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengths.Text != null)
        //                                {
        //                                    Strengths = RadStrengths.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunities.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                string CoacherFeedback;
        //                                if (RadCoacherFeedback.Text != null)
        //                                {
        //                                    CoacherFeedback = RadCoacherFeedback.Text;
        //                                }
        //                                else
        //                                {
        //                                    CoacherFeedback = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
        //                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ODReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID);
        //                                RadReviewID.Text = Convert.ToString(ODReviewID);
        //                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ODReviewID)); // update status to coacheesigned off 
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
        //                                InsertReviewHistory(1);
        //                                LoadKPIReview(ODReviewID);
        //                                UpdateFormType(ODReviewID, 3);
        //                                LoadDocumentationsReview();

        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Review has been successfully saved.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
        //                                RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            DataAccess ws = new DataAccess();
        //                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                            LoadDocumentationsReview();
        //                            RadCoacheeSignOff.Visible = false;
        //                            string script = "Review has been successfully saved.";
        //                            string ModalHeader = "Success";
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                            DisableElements();
        //                            RadSSN.Visible = false;

        //                        }
        //                    }
        //                    else
        //                    {
        //                        string script = "Incorrect CIM Number.";
        //                        string ModalHeader = "Error Message";
        //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                        DisableElements();
        //                    }
        //                }
        //                else
        //                {
        //                    string script = "Coachee CIM is a required field.";
        //                    string ModalHeader = "Error Message";
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                    DisableElements();
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        string ModalLabel = "RadCoacheeSignOff_Click " + myStringVariable;
        //        string ModalHeader = "Error Message";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
        //    }
        //}
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                string Account = Session["Account"].ToString();
                string emailAddressCCM, emailCoachee;
                if (ValidatePage() == true)
                {
                    if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                    {
                        if (Convert.ToInt32(RadReviewID.Text) == 0)
                        {
                            if (Account == "Default")
                            {
                                DataSet ds1 = null;
                                DataAccess ws1 = new DataAccess();
                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
                                emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                emailAddressCCM = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                                int ReviewID;
                                DataAccess ws = new DataAccess();
                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
                                RadReviewID.Text = Convert.ToString(ReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

                                InsertReviewKPI();
                                InsertReviewHistory(1);
                                InsertReviewHistory(2);
                                LoadKPIReview(ReviewID);
                                UpdateFormType(ReviewID, 1);
                                LoadDocumentationsReview();
                                CIM = RadCoacheeCIM.Text;
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                //SendEmail(1, ReviewID, emailCoachee);
                                //SendEmail(2, ReviewID, emailAddressCCM);
                                string ModalLabel = "Review has been successfully saved.";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadCoacheeCIM.Visible = false;

                            }
                            else if (Account == "TalkTalk")
                            {
                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescriptionNexidia.Text != null)
                                {
                                    Description = RadDescriptionNexidia.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengthsNexidia.Text != null)
                                {
                                    Strengths = RadStrengthsNexidia.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunitiesNexidia.Text != null)
                                {
                                    Opportunity = RadOpportunitiesNexidia.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CheckBoxList1.SelectedValue) ? 0 : int.Parse(CheckBoxList1.SelectedValue);

                                string PositiveBehaviour;
                                if (RadPositiveBehaviour.Text != null)
                                {
                                    PositiveBehaviour = RadPositiveBehaviour.Text;
                                }
                                else
                                {
                                    PositiveBehaviour = DBNull.Value.ToString();
                                }

                                string OpportunityBehaviour;
                                if (RadOpportunityBehaviour.Text != null)
                                {
                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
                                }
                                else
                                {
                                    OpportunityBehaviour = DBNull.Value.ToString();
                                }

                                string BeginBehaviourComments;
                                if (RadBeginBehaviourComments.Text != null)
                                {
                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
                                }
                                else
                                {
                                    BeginBehaviourComments = DBNull.Value.ToString();
                                }

                                string BehaviourEffects;
                                if (RadBehaviourEffect.Text != null)
                                {
                                    BehaviourEffects = RadBehaviourEffect.Text;
                                }
                                else
                                {
                                    BehaviourEffects = DBNull.Value.ToString();
                                }

                                string RootCause;
                                if (RadRootCause.Text != null)
                                {
                                    RootCause = RadRootCause.Text;
                                }
                                else
                                {
                                    RootCause = DBNull.Value.ToString();
                                }

                                string ReviewResultsComments;
                                if (RadReviewResultsComments.Text != null)
                                {
                                    ReviewResultsComments = RadReviewResultsComments.Text;
                                }
                                else
                                {
                                    ReviewResultsComments = DBNull.Value.ToString();
                                }

                                string AddressOpportunities;
                                if (RadAddressOpportunities.Text != null)
                                {
                                    AddressOpportunities = RadAddressOpportunities.Text;
                                }
                                else
                                {
                                    AddressOpportunities = DBNull.Value.ToString();
                                }

                                string ActionPlanComments;
                                if (RadActionPlanComments.Text != null)
                                {
                                    ActionPlanComments = RadActionPlanComments.Text;
                                }
                                else
                                {
                                    ActionPlanComments = DBNull.Value.ToString();
                                }

                                string SmartGoals;
                                if (RadSmartGoals.Text != null)
                                {
                                    SmartGoals = RadSmartGoals.Text;
                                }
                                else
                                {
                                    SmartGoals = DBNull.Value.ToString();
                                }

                                string CreatePlanComments;
                                if (RadCreatePlanComments.Text != null)
                                {
                                    CreatePlanComments = RadCreatePlanComments.Text;
                                }
                                else
                                {
                                    CreatePlanComments = DBNull.Value.ToString();
                                }

                                string FollowThrough;
                                if (RadFollowThrough.Text != null)
                                {
                                    FollowThrough = RadFollowThrough.Text;
                                }
                                else
                                {
                                    FollowThrough = DBNull.Value.ToString();
                                }

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int NexidiaReviewID;
                                DataAccess ws = new DataAccess();
                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(NexidiaReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

                                InsertReviewKPI();
                                InsertReviewHistory(1);
                                InsertReviewHistory(2);
                                LoadKPIReview(NexidiaReviewID);
                                UpdateFormType(NexidiaReviewID, 2); ;
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                string ModalLabel = "Review has been successfully saved.";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadCoacheeCIM.Visible = false;
                            }
                            else
                            {
                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescriptionOD.Text != null)
                                {
                                    Description = RadDescriptionOD.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengthsOD.Text != null)
                                {
                                    Strengths = RadStrengthsOD.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunitiesOD.Text != null)
                                {
                                    Opportunity = RadOpportunitiesOD.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                string CoacherFeedback;
                                if (RadCoacherFeedback.Text != null)
                                {
                                    CoacherFeedback = RadCoacherFeedback.Text;
                                }
                                else
                                {
                                    CoacherFeedback = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CheckBoxList1.SelectedValue) ? 0 : int.Parse(CheckBoxList1.SelectedValue);
                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int ODReviewID;
                                DataAccess ws = new DataAccess();
                                ODReviewID = ws.InsertReviewODv2(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID, Convert.ToInt32(RadSite.SelectedValue), RadDivision.Text, Convert.ToInt32(RadLOB.SelectedValue));
                                RadReviewID.Text = Convert.ToString(ODReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ODReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


                                InsertReviewKPIOD();
                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
                                InsertReviewHistory(1);
                                LoadKPIReviewOD(ODReviewID);
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                string ModalLabel = "Review has been successfully saved.";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadCoacheeCIM.Visible = false;



                            }
                        }
                        else
                        {
                            DataAccess ws = new DataAccess();
                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


                            LoadDocumentationsReview();
                            RadCoacheeSignOff.Visible = false;
                            string ModalLabel = "Review has been successfully saved.";
                            string ModalHeader = "Success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            DisableElements();
                            RadSSN.Visible = false;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                string ModalLabel = "RadCoacheeSignOff_Click " + myStringVariable;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        public bool CheckSSN()
        {
            int CIMNumber = Convert.ToInt32(RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void InsertReviewKPI()
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (Account == "Default")
                {
                    int ReviewID;
                    RadComboBox KPI, Driver;
                    RadTextBox Target;
                    RadNumericTextBox Current, Previous;

                    foreach (GridDataItem itm in RadGrid1.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        KPI = (RadComboBox)itm.FindControl("RadKPI");
                        Driver = (RadComboBox)itm.FindControl("RadDriver");
                        Target = (RadTextBox)itm.FindControl("RadTarget");
                        Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                        Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");

                        if (Current.Text != "" && Previous.Text != "")
                        {
                            DataAccess ws = new DataAccess();
                            ws.InsertReviewKPI(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue));

                        }
                    }
                }
                else if (Account == "TalkTalk")
                {
                    int ReviewID;
                    RadComboBox KPI, Driver;
                    RadTextBox Target;
                    RadNumericTextBox Current, Previous;

                    foreach (GridDataItem itm in RadGrid4.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        KPI = (RadComboBox)itm.FindControl("RadKPI4");
                        Driver = (RadComboBox)itm.FindControl("RadDriver");
                        Target = (RadTextBox)itm.FindControl("RadTarget");
                        Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                        Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");

                        if (Current.Text != "" && Previous.Text != "")
                        {
                            DataAccess ws = new DataAccess();
                            ws.InsertReviewKPI(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue));

                        }
                    }
                }
                else
                {
                    int ReviewID;
                    RadComboBox KPI, Driver;
                    RadTextBox Target, Behavior, RootCause;
                    RadNumericTextBox Current, Previous, Change;

                    foreach (GridDataItem itm in RadGrid9.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        KPI = (RadComboBox)itm.FindControl("RadKPIOD");
                        Driver = (RadComboBox)itm.FindControl("RadDriver");
                        Target = (RadTextBox)itm.FindControl("RadTarget");
                        Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                        Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");
                        Change = (RadNumericTextBox)itm.FindControl("RadChange");
                        Behavior = (RadTextBox)itm.FindControl("RadBehavior");
                        RootCause = (RadTextBox)itm.FindControl("RadRootCause");


                        if (Current.Text != "" && Previous.Text != "" && Change.Text != "" && Behavior.Text != "" && RootCause.Text != "")
                        {
                            DataAccess ws = new DataAccess();
                            ws.InsertReviewKPIOD(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue), Change.Text, Behavior.Text, RootCause.Text);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "InsertReviewKPI " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        private void InsertReviewHistory(int InclusionType)
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (Account == "Default")
                {
                    if (CNPR.Visible == true && CheckBoxes.Visible == true)
                    {
                        if (InclusionType == 1)
                        {
                            int ReviewID;
                            foreach (GridDataItem itm in RadGridCN.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);
                            }
                        }
                        else
                        {
                            int ReviewID;
                            foreach (GridDataItem itm in RadGridPR.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);
                            }

                        }
                    }
                }
                else if (Account == "TalkTalk")
                {
                    if (Div16.Visible == true && CheckBoxes.Visible == true)
                    {


                        if (InclusionType == 1)
                        {
                            int ReviewID;

                            foreach (GridDataItem itm in RadGrid6.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                            }
                        }
                        else
                        {
                            int ReviewID;

                            foreach (GridDataItem itm in RadGrid7.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                            }

                        }
                    }
                }
                else
                {
                    if (Div21.Visible == true && CheckBoxes.Visible == true)
                    {


                        if (InclusionType == 1)
                        {
                            int ReviewID;

                            foreach (GridDataItem itm in RadGrid11.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "InsertReviewHistory " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);

                if (Account == "Default")
                {
                    RadGridPRR.DataSource = ds;
                    RadGridPRR.DataBind();
                }
                else if (Account == "TalkTalk")
                {
                    RadGrid5.DataSource = ds;
                    RadGrid5.DataBind();
                }
                else
                {
                    DataSet ds2 = null;
                    DataAccess ws2 = new DataAccess();
                    ds2 = ws2.GetKPIReviewOD(ReviewID);

                    RadGrid10.DataSource = ds2;
                    RadGrid10.DataBind();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        public void UploadDocumentationReview(int MassCoachingType, int ReviewID)
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (Account == "Default")
                {
                    foreach (UploadedFile f in RadAsyncUpload2.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    string ModalLabel = "Review has been successfully saved.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //string pageurl = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx";
                    //Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");

                    //if (RadReviewID.Text != "0")
                    ////{
                    //    Process.Start("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //    HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx");
                    //}

                    //string script = "function f(){$find(\"" + Window1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                    //string script = "function f(){$find(\"" + Window1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function (){f.close();}, 10000);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                    string script = "function f(){$find(\"" + Window1.ClientID + "\").show();Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function(){ var oWnd = $find(\"" + Window1.ClientID + "\"); oWnd.close();}, 2000);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);


                }
                else if (Account == "TalkTalk")
                {
                    foreach (UploadedFile f in RadAsyncUpload3.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    string ModalLabel = "Review has been successfully saved.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //if (RadReviewID.Text != "0")
                    //{
                    //Process.Start("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx");

                    //}

                    //string pageurl = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx";
                    //Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");

                    //string script = "function f(){$find(\"" + Window1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                    //string script = "function f(){$find(\"" + Window1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function (){f.close();}, 10000);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openQuickActionRadWindow(); });", true);
                    string script = "function f(){$find(\"" + Window1.ClientID + "\").show();Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function(){ var oWnd = $find(\"" + Window1.ClientID + "\"); oWnd.close();}, 2000);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                }
                else
                {
                    foreach (UploadedFile f in RadAsyncUpload4.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    string ModalLabel = "Review has been successfully saved.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //if (RadReviewID.Text != "0")
                    //{
                    //Process.Start("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx");

                    //}
                    // string pageurl = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                    //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AutoLogout.aspx";
                    // Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");

                    //string script = "function f(){$find(\"" + Window1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function (){f.close();}, 10000);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                    string script = "function f(){$find(\"" + Window1.ClientID + "\").show();Sys.Application.remove_load(f);}Sys.Application.add_load(f);window.setTimeout(function(){ var oWnd = $find(\"" + Window1.ClientID + "\"); oWnd.close();}, 2000);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);

                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "UploadDocumentationReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                if (Account == "Default")
                {
                    RadDocumentationReview.DataSource = ds;
                    RadDocumentationReview.Rebind();
                }
                else if (Account == "TalkTalk")
                {
                    RadGrid8.DataSource = ds;
                    RadGrid8.Rebind();
                }
                else
                {
                    RadGrid12.DataSource = ds;
                    RadGrid12.Rebind();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void DisableElements()
        {
            string Account = Session["Account"].ToString();
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                if (Account == "Default")
                {
                    RadGrid1.DataSource = null;
                    RadGrid1.DataBind();
                    RadGrid1.Visible = false;
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    RadDescription.Enabled = false;
                    RadStrengths.Enabled = false;
                    RadOpportunities.Enabled = false;
                    RadCommitment.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    RadButton1.Visible = false;
                    RadAsyncUpload2.Attributes["style"] = "display: none;";
                }
                else if (Account == "TalkTalk")
                {
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    CheckBoxList1.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    RadDescriptionNexidia.Enabled = false;
                    RadGrid4.Visible = false;
                    RadButton2.Visible = false;
                    RadStrengthsNexidia.Enabled = false;
                    RadOpportunitiesNexidia.Enabled = false;
                    RadPositiveBehaviour.Enabled = false;
                    RadOpportunityBehaviour.Enabled = false;
                    RadBeginBehaviourComments.Enabled = false;
                    RadBehaviourEffect.Enabled = false;
                    RadRootCause.Enabled = false;
                    RadReviewResultsComments.Enabled = false;
                    RadAddressOpportunities.Enabled = false;
                    RadActionPlanComments.Enabled = false;
                    RadSmartGoals.Enabled = false;
                    RadCreatePlanComments.Enabled = false;
                    RadFollowThrough.Enabled = false;
                    RadAsyncUpload3.Attributes["style"] = "display: none;";
                }
                else
                {
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadCallID.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    CheckBoxList1.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    RadDescriptionOD.Enabled = false;
                    RadGrid9.Visible = false;
                    RadButton3.Visible = false;
                    RadStrengthsOD.Enabled = false;
                    RadOpportunitiesOD.Enabled = false;
                    RadCoacherFeedback.Enabled = false;
                    RadTextBox2.Enabled = false;
                    RadTextBox4.Enabled = false;
                    RadTextBox6.Enabled = false;
                    RadTextBox8.Enabled = false;
                    RadAsyncUpload4.Attributes["style"] = "display: none;";
                }
            }

        }

        protected void RadCMTPreview_Click(object sender, EventArgs e)
        {
            Session["CMTCIM"] = RadCoacheeName.SelectedValue;
            Session["CMTSupName"] = RadSupervisor.SelectedItem.Text;
            Session["CMTAccount"] = RadAccount.SelectedItem.Text;
            Session["CMTSessionType"] = RadSessionType.SelectedItem.Text;
            Session["CMTSessionTopic"] = RadSessionTopic.SelectedItem.Text;
            Session["CMTHRComment"] = RadHRComments.Text;
            Session["CMTName"] = RadCoacheeName.Text;
            Session["PreviewType"] = 1;
            LoadDocs();
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "var Mleft = (screen.width/2)-(760/2);var Mtop = (screen.height/2)-(700/2);window.open( 'CMT.aspx', null, 'height=700,width=760,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );", true);

        }
        public void LoadDocs()
        {
            Docspo();
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string Name = dsSAPInfo.Tables[0].Rows[0]["First_Name"].ToString() + " " + dsSAPInfo.Tables[0].Rows[0]["Last_Name"].ToString();
            string DateToday = DateTime.Now.ToShortDateString();

            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string filename = f.GetNameWithoutExtension() + f.GetExtension();

                DataTable dt = new DataTable();
                DataRow dr;
                //assigning ViewState value in Data Table  
                dt = (DataTable)ViewState["Docs"];
                //Adding value in datatable  
                dr = dt.NewRow();
                dr["FileName"] = filename;
                dr["UploadedBy"] = Name;
                dr["DateUploaded"] = DateToday;
                dt.Rows.Add(dr);

                if (dt != null)
                {
                    ViewState["Docs"] = dt;
                }
                this.BindListViewDocs();
            }
        }
        public DataTable Docs
        {
            get
            {
                return ViewState["Docs"] as DataTable;
            }
            set
            {
                ViewState["Docs"] = value;
            }
        }
        public void Docspo()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("FileName", typeof(string));
            dt2.Columns.Add("UploadedBy", typeof(string));
            dt2.Columns.Add("DateUploaded", typeof(string));
            ViewState["Docs"] = dt2;

            BindListViewDocs();
        }
        private void BindListViewDocs()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["Docs"];

            if (dt.Rows.Count > 0 && dt != null)
            {

                Session["Docs"] = dt;
            }
        }
        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataAccess ws = new DataAccess();
                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 1);
                DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                //DataHelper.SetTicketStatus(8, Convert.ToInt32(RadReviewID.Text)); // update status to review signed off 
                RadCoacherSignOff.Visible = false;
                string ModalLabel = "Coach has signed off from this review.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                RadCoacheeCIM.Visible = false;
                RadCoacherSignOff.Visible = false;
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadCoacherSignOff_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                string Account = Session["Account"].ToString();

                if (RadSessionTopic.Text != "Termination")
                {
                    if (Account == "Default")
                    {
                        createreviewtype1();
                    }
                    else if (Account == "TalkTalk")
                    {
                        PrintNexidia();
                    }
                    else
                    {
                        PrintOD();
                    }
                }
                else
                {
                    createreviewtypecmt();
                }
            }
            else
            {
                string ModalLabel = "Please submit the review before creating a pdf.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void createreviewtype1()
        {

            try
            {

                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                FakeURLID.Value = URL;
                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = URL + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                //Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //pdfDoc.NewPage();
                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
                gif.ScaleToFit(10f, 10f);
                //gif.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                //gif.IndentationLeft = 15f;
                //gif.IndentationRight = 15f;
                //gif.SpacingAfter = 15f;
                //gif.SpacingBefore = 15f;
                //gif.Border = 0;



                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                specificstable.AddCell(specificscell);
                //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                //specificscelli00.Border = 0;
                //                   specificscelli00.Rowspan = 7;


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                specificscelli8.Border = 0;

                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                specificscelli10.Border = 0;


                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                specificscelli14.Border = 0;

                PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                specificscelli15.Border = 0;
                PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                specificscelli16.Border = 0;

                PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                specificscelli17.Border = 0;
                PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                specificscelli18.Border = 0;

                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli15);
                specificstable.AddCell(specificscelli16);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);
                specificstable.AddCell(specificscelli17);
                specificstable.AddCell(specificscelli18);

                specificstable.SpacingAfter = 40;

                Paragraph p8 = new Paragraph();
                string line8 = "Description" + Environment.NewLine + Convert.ToString(RadDescription.Text);
                p8.Alignment = Element.ALIGN_LEFT;
                p8.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p8.Add(line8);
                p8.SpacingBefore = 50;
                p8.SpacingAfter = 10;



                Paragraph p15 = new Paragraph();
                string line15 = "Performance Result " + Environment.NewLine;
                p15.Alignment = Element.ALIGN_LEFT;
                p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p15.Add(line15);
                p15.SpacingBefore = 10;
                p15.SpacingAfter = 10;


                PdfPTable perftable = new PdfPTable(5);
                string perfheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell perfcell = new PdfPCell(new Phrase(perfheader, font));
                perfcell.Colspan = 5;
                perfcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                perftable.AddCell(perfcell);
                perftable.SpacingBefore = 30;
                perftable.SpacingAfter = 30;
                perftable.WidthPercentage = 100;

                perftable.HorizontalAlignment = 0;
                perftable.TotalWidth = 500f;
                perftable.LockedWidth = true;
                float[] widths = new float[] { 100f, 50f, 50f, 50f, 250f };
                perftable.SetWidths(widths);

                DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadReviewID.Text));
                if (ds_KPILIST.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_KPILIST.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI", font));
                            perfcellh1.Border = 0;
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target", font));
                            perfcellh2.Border = 0;
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current", font));
                            perfcellh3.Border = 0;
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                            perfcellh4.Border = 0;
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description", font));
                            perfcellh5.Border = 0;
                            perftable.AddCell(perfcellh1);
                            perftable.AddCell(perfcellh2);
                            perftable.AddCell(perfcellh3);
                            perftable.AddCell(perfcellh4);
                            perftable.AddCell(perfcellh5);
                        }
                        PdfPCell perfcellitem1 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["KPI"]), font));
                        perfcellitem1.Border = 0;
                        perftable.AddCell(perfcellitem1);

                        PdfPCell perfcellitem2 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Target"]), font));
                        perfcellitem2.Border = 0;
                        perftable.AddCell(perfcellitem2);

                        PdfPCell perfcellitem3 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Current"]), font));
                        perfcellitem3.Border = 0;
                        perftable.AddCell(perfcellitem3);

                        PdfPCell perfcellitem4 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Previous"]), font));
                        perfcellitem4.Border = 0;
                        perftable.AddCell(perfcellitem4);


                        PdfPCell perfcellitem5 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Description"]), font));
                        perfcellitem5.Border = 0;
                        perftable.AddCell(perfcellitem5);
                    }
                }
                else
                {
                    //perftable.AddCell("Target");
                    //perftable.AddCell("Current");
                    //perftable.AddCell("Previous");
                    //perftable.AddCell("Description");
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI", font));
                    perfcellh1.Border = 0;
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target", font));
                    perfcellh2.Border = 0;
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current", font));
                    perfcellh3.Border = 0;
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh4.Border = 0;
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description", font));
                    perfcellh5.Border = 0;
                    perftable.AddCell(perfcellh1);
                    perftable.AddCell(perfcellh2);
                    perftable.AddCell(perfcellh3);
                    perftable.AddCell(perfcellh4);
                    perftable.AddCell(perfcellh5);
                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Summary", font));
                    perfcelli1.Border = 0;
                    perftable.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase(" ", font));
                    perfcelli2.Border = 0;
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                }

                PdfPTable tablenotes = new PdfPTable(7);
                string headernotes = "";
                PdfPCell cellnotes = new PdfPCell(new Phrase(headernotes, font));
                cellnotes.Colspan = 7;
                cellnotes.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tablenotes.AddCell(cellnotes);
                tablenotes.SpacingBefore = 30;
                tablenotes.SpacingAfter = 30;
                tablenotes.WidthPercentage = 100;

                Paragraph p12 = new Paragraph();
                string line12 = "Coaching Notes " + Environment.NewLine;
                p12.Alignment = Element.ALIGN_LEFT;
                p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p12.Add(line12);
                p12.SpacingBefore = 10;
                p12.SpacingAfter = 10;


                PdfPTable notestable = new PdfPTable(5);
                string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell notescell = new PdfPCell(new Phrase(notesheader, font));
                notescell.Colspan = 5;
                notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                notestable.AddCell(notescell);
                notestable.SpacingBefore = 30;
                notestable.SpacingAfter = 30;
                notestable.WidthPercentage = 100;

                notestable.HorizontalAlignment = 0;
                notestable.TotalWidth = 500f;
                notestable.LockedWidth = true;
                float[] widthsnotestable = new float[] { 100f, 100f, 100f, 100f, 100f };
                notestable.SetWidths(widthsnotestable);


                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            notestable.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                            perfcellh2.Border = 0;
                            notestable.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                            perfcellh3.Border = 0;
                            notestable.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                            perfcellh4.Border = 0;
                            notestable.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh5.Border = 0;
                            notestable.AddCell(perfcellh4);

                        }
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"]), font));
                        perfcelli1.Border = 0;
                        notestable.AddCell(perfcelli1);

                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"]), font));
                        perfcelli2.Border = 0;
                        notestable.AddCell(perfcelli2);

                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"]), font));
                        perfcelli3.Border = 0;
                        notestable.AddCell(perfcelli3);

                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"]), font));
                        perfcelli4.Border = 0;
                        notestable.AddCell(perfcelli4);

                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"]), font));
                        perfcelli5.Border = 0;
                        notestable.AddCell(perfcelli5);


                    }

                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    notestable.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                    perfcellh2.Border = 0;
                    notestable.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                    perfcellh3.Border = 0;
                    notestable.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                    perfcellh4.Border = 0;
                    notestable.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh5.Border = 0;
                    notestable.AddCell(perfcellh4);

                    PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes.", font));
                    notescelli1.Border = 0;
                    notestable.AddCell(notescelli1);
                    PdfPCell notescelli2 = new PdfPCell(new Phrase(" ", font));
                    notescelli2.Border = 0;
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                }

                DataSet ds_perfhistory = null;
                DataAccess ws1 = new DataAccess();
                ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadReviewID.Text)), 2);

                //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                Paragraph p16 = new Paragraph();
                string line16 = "Performance history";
                p16.Alignment = Element.ALIGN_LEFT;
                p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p16.Add(line16);
                p16.SpacingBefore = 10;
                p16.SpacingAfter = 10;

                PdfPTable tableperfhist = new PdfPTable(9);
                string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader, font));
                perfhistcell.Colspan = 9;
                perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableperfhist.AddCell(perfhistcell);
                tableperfhist.SpacingBefore = 30;
                tableperfhist.SpacingAfter = 30;

                tableperfhist.WidthPercentage = 100;

                tableperfhist.HorizontalAlignment = 0;
                tableperfhist.TotalWidth = 500f;
                tableperfhist.LockedWidth = true;
                float[] widthstableperfhist = new float[] { 50f, 50f, 45f, 45f, 45f, 45f, 120f, 50f, 50f };
                tableperfhist.SetWidths(widthstableperfhist);


                if (CheckBox2.Checked == true)
                {
                    if (ds_perfhistory.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                                perfcellh1.Border = 0;
                                tableperfhist.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                                perfcellh2.Border = 0;
                                tableperfhist.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                                perfcellh3.Border = 0;
                                tableperfhist.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                                perfcellh4.Border = 0;
                                tableperfhist.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                                perfcellh5.Border = 0;
                                tableperfhist.AddCell(perfcellh5);
                                PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                                perfcellh6.Border = 0;
                                tableperfhist.AddCell(perfcellh6);
                                PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                                perfcellh7.Border = 0;
                                tableperfhist.AddCell(perfcellh7);
                                PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                                perfcellh8.Border = 0;
                                tableperfhist.AddCell(perfcellh8);
                                PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                                perfcellh9.Border = 0;
                                tableperfhist.AddCell(perfcellh9);

                                PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                                perfcellh10.Border = 0;
                                perfcellh10.NoWrap = false;
                                //perfcellh10.Width = 15;
                                tableperfhist.AddCell(perfcellh10);
                                PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                                perfcellh11.Border = 0;
                                perfcellh11.NoWrap = false;
                                //perfcellh11.Width = 15;
                                tableperfhist.AddCell(perfcellh11);
                                PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                                perfcellh12.Border = 0;
                                perfcellh12.NoWrap = false;
                                //perfcellh12.Width = 15;
                                tableperfhist.AddCell(perfcellh12);
                                PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                                perfcellh13.Border = 0;
                                perfcellh13.NoWrap = false;
                                //perfcellh13.SpaceCharRatio = 10;
                                tableperfhist.AddCell(perfcellh13);
                                PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                                perfcellh14.Border = 0;
                                perfcellh14.NoWrap = false;
                                //perfcellh14.Width = 15;
                                tableperfhist.AddCell(perfcellh14);
                                PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                                perfcellh15.Border = 0;
                                perfcellh15.NoWrap = false;
                                //perfcellh15.Width = 15;
                                tableperfhist.AddCell(perfcellh15);
                                PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                                perfcellh16.Border = 0;
                                perfcellh16.NoWrap = false;
                                //perfcellh16.Width = 15;
                                tableperfhist.AddCell(perfcellh16);
                                PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                                perfcellh17.Border = 0;
                                perfcellh17.NoWrap = false;
                                //perfcellh17.Width = 15;
                                tableperfhist.AddCell(perfcellh17);
                                PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                                perfcellh18.Border = 0;
                                perfcellh18.NoWrap = false;
                                //perfcellh18.Width = 15;
                                tableperfhist.AddCell(perfcellh18);
                            }

                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"]), font));
                            perfcelli1.Border = 0;
                            tableperfhist.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"]), font));
                            perfcelli2.Border = 0;
                            tableperfhist.AddCell(perfcelli2);
                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"]), font));
                            perfcelli3.Border = 0;
                            tableperfhist.AddCell(perfcelli3);
                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"]), font));
                            perfcelli4.Border = 0;
                            tableperfhist.AddCell(perfcelli4);
                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"]), font));
                            perfcelli5.Border = 0;
                            tableperfhist.AddCell(perfcelli5);
                            PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"]), font));
                            perfcelli6.Border = 0;
                            tableperfhist.AddCell(perfcelli6);
                            PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"]), font));
                            perfcelli7.Border = 0;
                            tableperfhist.AddCell(perfcelli7);
                            PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"]), font));
                            perfcelli8.Border = 0;
                            tableperfhist.AddCell(perfcelli8);
                            PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"]), font));
                            perfcelli9.Border = 0;
                            tableperfhist.AddCell(perfcelli9);

                        }
                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                        perfcellh1.Border = 0;
                        tableperfhist.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                        perfcellh2.Border = 0;
                        tableperfhist.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                        perfcellh3.Border = 0;
                        tableperfhist.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                        perfcellh4.Border = 0;
                        tableperfhist.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                        perfcellh5.Border = 0;
                        tableperfhist.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                        perfcellh6.Border = 0;
                        tableperfhist.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                        perfcellh7.Border = 0;
                        tableperfhist.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                        perfcellh8.Border = 0;
                        tableperfhist.AddCell(perfcellh8);
                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                        perfcellh9.Border = 0;
                        tableperfhist.AddCell(perfcellh9);

                        PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                        perfcellh10.Border = 0;
                        perfcellh10.NoWrap = false;
                        //perfcellh10.Width = 15;
                        tableperfhist.AddCell(perfcellh10);
                        PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                        perfcellh11.Border = 0;
                        perfcellh11.NoWrap = false;
                        //perfcellh11.Width = 15;
                        tableperfhist.AddCell(perfcellh11);
                        PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                        perfcellh12.Border = 0;
                        perfcellh12.NoWrap = false;
                        //perfcellh12.Width = 15;
                        tableperfhist.AddCell(perfcellh12);
                        PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                        perfcellh13.Border = 0;
                        perfcellh13.NoWrap = false;
                        //perfcellh13.SpaceCharRatio = 10;
                        tableperfhist.AddCell(perfcellh13);
                        PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                        perfcellh14.Border = 0;
                        perfcellh14.NoWrap = false;
                        //perfcellh14.Width = 15;
                        tableperfhist.AddCell(perfcellh14);
                        PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                        perfcellh15.Border = 0;
                        perfcellh15.NoWrap = false;
                        //perfcellh15.Width = 15;
                        tableperfhist.AddCell(perfcellh15);
                        PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                        perfcellh16.Border = 0;
                        perfcellh16.NoWrap = false;
                        //perfcellh16.Width = 15;
                        tableperfhist.AddCell(perfcellh16);
                        PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                        perfcellh17.Border = 0;
                        perfcellh17.NoWrap = false;
                        //perfcellh17.Width = 15;
                        tableperfhist.AddCell(perfcellh17);
                        PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                        perfcellh18.Border = 0;
                        perfcellh18.NoWrap = false;
                        //perfcellh18.Width = 15;
                        tableperfhist.AddCell(perfcellh18);

                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);

                    }


                }
                else
                {

                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                    perfcellh1.Border = 0;
                    tableperfhist.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                    perfcellh2.Border = 0;
                    tableperfhist.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                    perfcellh3.Border = 0;
                    tableperfhist.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                    perfcellh4.Border = 0;
                    tableperfhist.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                    perfcellh5.Border = 0;
                    tableperfhist.AddCell(perfcellh5);
                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh6.Border = 0;
                    tableperfhist.AddCell(perfcellh6);
                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                    perfcellh7.Border = 0;
                    tableperfhist.AddCell(perfcellh7);
                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                    perfcellh8.Border = 0;
                    tableperfhist.AddCell(perfcellh8);
                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                    perfcellh9.Border = 0;
                    tableperfhist.AddCell(perfcellh9);



                    PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                    perfcellh10.Border = 0;
                    perfcellh10.NoWrap = false;
                    //perfcellh10.Width = 15;
                    tableperfhist.AddCell(perfcellh10);
                    PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                    perfcellh11.Border = 0;
                    perfcellh11.NoWrap = false;
                    //perfcellh11.Width = 15;
                    tableperfhist.AddCell(perfcellh11);
                    PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                    perfcellh12.Border = 0;
                    perfcellh12.NoWrap = false;
                    //perfcellh12.Width = 15;
                    tableperfhist.AddCell(perfcellh12);
                    PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                    perfcellh13.Border = 0;
                    perfcellh13.NoWrap = false;
                    //perfcellh13.SpaceCharRatio = 10;
                    tableperfhist.AddCell(perfcellh13);
                    PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                    perfcellh14.Border = 0;
                    perfcellh14.NoWrap = false;
                    //perfcellh14.Width = 15;
                    tableperfhist.AddCell(perfcellh14);
                    PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                    perfcellh15.Border = 0;
                    perfcellh15.NoWrap = false;
                    //perfcellh15.Width = 15;
                    tableperfhist.AddCell(perfcellh15);
                    PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                    perfcellh16.Border = 0;
                    perfcellh16.NoWrap = false;
                    //perfcellh16.Width = 15;
                    tableperfhist.AddCell(perfcellh16);
                    PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                    perfcellh17.Border = 0;
                    perfcellh17.NoWrap = false;
                    //perfcellh17.Width = 15;
                    tableperfhist.AddCell(perfcellh17);
                    PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                    perfcellh18.Border = 0;
                    perfcellh18.NoWrap = false;
                    //perfcellh18.Width = 15;
                    tableperfhist.AddCell(perfcellh18);

                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("NO History.", font));
                    perfcelli1.Border = 0;
                    tableperfhist.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                    perfcelli2.Border = 0;
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);


                }

                Paragraph p14 = new Paragraph();
                string line14 = "Strengths : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Strengths"]);//RadStrengths.Text);
                p14.Alignment = Element.ALIGN_LEFT;
                p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p14.Add(line14);
                p14.SpacingBefore = 10;
                p14.SpacingAfter = 10;


                Paragraph p11 = new Paragraph();
                string line11 = "Opportunities : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Opportunity"]); //txtOpportunities.Text);
                p11.Alignment = Element.ALIGN_LEFT;
                p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p11.Add(line11);
                p11.SpacingBefore = 10;
                p11.SpacingAfter = 10;

                Paragraph p17 = new Paragraph();
                string line17 = "Commitment : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Commitment"]); //txtOpportunities.Text);RadCommitment.Text);
                p17.Alignment = Element.ALIGN_LEFT;
                p17.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p17.Add(line17);
                p17.SpacingBefore = 10;
                p17.SpacingAfter = 10;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header, font));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;
                table.WidthPercentage = 100;

                table.HorizontalAlignment = 0;
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                float[] widthstable = new float[] { 150f, 150f, 150f, 150f };
                table.SetWidths(widthstable);

                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                            notescellh1.Border = 0;
                            table.AddCell(notescellh1);
                            PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                            notescellh2.Border = 0;
                            table.AddCell(notescellh2);
                            PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                            notescellh3.Border = 0;
                            table.AddCell(notescellh3);
                            PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                            notescellh4.Border = 0;
                            table.AddCell(notescellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                        notescelli1.Border = 0;
                        table.AddCell(notescelli1);


                        PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                        notescelli2.Border = 0;
                        table.AddCell(notescelli2);

                        PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                        notescelli3.Border = 0;
                        table.AddCell(notescelli3);

                        PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                        notescelli4.Border = 0;
                        table.AddCell(notescelli4);

                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));

                    }
                }
                else
                {
                    PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                    notescellh1.Border = 0;
                    table.AddCell(notescellh1);
                    PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                    notescellh2.Border = 0;
                    table.AddCell(notescellh2);
                    PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                    notescellh3.Border = 0;
                    table.AddCell(notescellh3);
                    PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                    notescellh4.Border = 0;
                    table.AddCell(notescellh4);
                    PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded.", font));
                    notescellh5.Border = 0;
                    table.AddCell(notescellh5);
                    PdfPCell notescellh6 = new PdfPCell(new Phrase("", font));
                    notescellh6.Border = 0;
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                }

                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 40;
                p13.SpacingAfter = 40;

                //pdfDoc.Add(new Paragraph(p1)); //specifics
                //pdfDoc.Add(gif); //picture
                //pdfDoc.Add(new Paragraph(p2)); //account
                //pdfDoc.Add(new Paragraph(p3)); //supervisor
                //pdfDoc.Add(new Paragraph(p4)); //name
                //pdfDoc.Add(new Paragraph(p5)); //session type
                //pdfDoc.Add(new Paragraph(p6)); //topic 
                //pdfDoc.Add(new Paragraph(p10)); //follow up date
                //pdfDoc.Add(new Paragraph(p8)); //description
                pdfDoc.Add(specificstable);
                pdfDoc.Add(new Paragraph(p15));
                pdfDoc.Add(perftable);
                pdfDoc.Add(new Paragraph(p12)); //coaching notes
                pdfDoc.Add(notestable); //coaching notes table
                pdfDoc.Add(new Paragraph(p16));//perfhistory
                pdfDoc.Add(tableperfhist);
                pdfDoc.Add(new Paragraph(p14)); //strengths
                pdfDoc.Add(new Paragraph(p11)); //opportunities   
                pdfDoc.Add(new Paragraph(p17)); //commitment   
                pdfDoc.Add(new Paragraph(p13)); //documentation
                pdfDoc.Add(table);//documentation table
                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void createreviewtypecmt()
        {

            try
            {

                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewIDforCMT(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                FakeURLID.Value = URL;
                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = URL + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                //Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //pdfDoc.NewPage();

                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
                gif.ScaleToFit(10f, 10f);
                //gif.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                //gif.IndentationLeft = 15f;
                //gif.IndentationRight = 15f;
                //gif.SpacingAfter = 15f;
                //gif.SpacingBefore = 15f;
                //gif.Border = 0;



                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                specificstable.AddCell(specificscell);


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                specificscelli8.Border = 0;

                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                specificscelli10.Border = 0;


                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                specificscelli14.Border = 0;

                PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                specificscelli15.Border = 0;
                PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                specificscelli16.Border = 0;

                PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                specificscelli17.Border = 0;
                PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                specificscelli18.Border = 0;

                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli15);
                specificstable.AddCell(specificscelli16);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);
                specificstable.AddCell(specificscelli17);
                specificstable.AddCell(specificscelli18);

                specificstable.SpacingAfter = 40;

                //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                Paragraph p1 = new Paragraph();
                string line1 = "HR Comments " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HRComments"]);
                p1.Alignment = Element.ALIGN_LEFT;
                p1.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p1.Add(line1);
                p1.SpacingBefore = 10;
                p1.SpacingAfter = 10;




                PdfPTable tablenotes = new PdfPTable(7);
                string headernotes = "";
                PdfPCell cellnotes = new PdfPCell(new Phrase(headernotes, font));
                cellnotes.Colspan = 7;
                cellnotes.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tablenotes.AddCell(cellnotes);
                tablenotes.SpacingBefore = 30;
                tablenotes.SpacingAfter = 30;
                tablenotes.WidthPercentage = 100;



                Paragraph p12 = new Paragraph();
                string line12 = "Coaching Notes " + Environment.NewLine;
                p12.Alignment = Element.ALIGN_LEFT;
                p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p12.Add(line12);
                p12.SpacingBefore = 10;
                p12.SpacingAfter = 10;


                PdfPTable notestable = new PdfPTable(5);
                string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell notescell = new PdfPCell(new Phrase(notesheader, font));
                notescell.Colspan = 5;
                notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                notestable.AddCell(notescell);
                notestable.SpacingBefore = 30;
                notestable.SpacingAfter = 30;
                notestable.WidthPercentage = 100;

                notestable.HorizontalAlignment = 0;
                notestable.TotalWidth = 500f;
                notestable.LockedWidth = true;
                float[] widths = new float[] { 100f, 100f, 100f, 100f, 100f };
                notestable.SetWidths(widths);

                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            notestable.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                            perfcellh2.Border = 0;
                            notestable.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                            perfcellh3.Border = 0;
                            notestable.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                            perfcellh4.Border = 0;
                            notestable.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh5.Border = 0;
                            notestable.AddCell(perfcellh4);

                        }
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"]), font));
                        perfcelli1.Border = 0;
                        notestable.AddCell(perfcelli1);

                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"]), font));
                        perfcelli2.Border = 0;
                        notestable.AddCell(perfcelli2);

                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"]), font));
                        perfcelli3.Border = 0;
                        notestable.AddCell(perfcelli3);

                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"]), font));
                        perfcelli4.Border = 0;
                        notestable.AddCell(perfcelli4);

                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"]), font));
                        perfcelli5.Border = 0;
                        notestable.AddCell(perfcelli5);


                    }

                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    notestable.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                    perfcellh2.Border = 0;
                    notestable.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                    perfcellh3.Border = 0;
                    notestable.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                    perfcellh4.Border = 0;
                    notestable.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh5.Border = 0;
                    notestable.AddCell(perfcellh4);

                    PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes.", font));
                    notescelli1.Border = 0;
                    notestable.AddCell(notescelli1);
                    PdfPCell notescelli2 = new PdfPCell(new Phrase(" ", font));
                    notescelli2.Border = 0;
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                }



                DataSet ds_perfhistory = null;
                DataAccess ws1 = new DataAccess();
                ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadReviewID.Text)), 2);

                //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                Paragraph p16 = new Paragraph();
                string line16 = "Performance history";
                p16.Alignment = Element.ALIGN_LEFT;
                p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p16.Add(line16);
                p16.SpacingBefore = 10;
                p16.SpacingAfter = 10;

                PdfPTable tableperfhist = new PdfPTable(9);
                string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader, font));
                perfhistcell.Colspan = 9;
                perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableperfhist.AddCell(perfhistcell);
                tableperfhist.SpacingBefore = 30;
                tableperfhist.SpacingAfter = 30;
                tableperfhist.WidthPercentage = 100;

                tableperfhist.HorizontalAlignment = 0;
                tableperfhist.TotalWidth = 500f;
                tableperfhist.LockedWidth = true;
                float[] widthstableperfhist = new float[] { 50f, 50f, 45f, 45f, 45f, 45f, 120f, 50f, 50f };
                tableperfhist.SetWidths(widthstableperfhist);

                if (CheckBox2.Checked == true)
                {
                    if (ds_perfhistory.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                                perfcellh1.Border = 0;
                                tableperfhist.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                                perfcellh2.Border = 0;
                                tableperfhist.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                                perfcellh3.Border = 0;
                                tableperfhist.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                                perfcellh4.Border = 0;
                                tableperfhist.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                                perfcellh5.Border = 0;
                                tableperfhist.AddCell(perfcellh5);
                                PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                                perfcellh6.Border = 0;
                                tableperfhist.AddCell(perfcellh6);
                                PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                                perfcellh7.Border = 0;
                                tableperfhist.AddCell(perfcellh7);
                                PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                                perfcellh8.Border = 0;
                                tableperfhist.AddCell(perfcellh8);
                                PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                                perfcellh9.Border = 0;
                                tableperfhist.AddCell(perfcellh9);


                                PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                                perfcellh10.Border = 0;
                                perfcellh10.NoWrap = false;
                                //perfcellh10.Width = 15;
                                tableperfhist.AddCell(perfcellh10);
                                PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                                perfcellh11.Border = 0;
                                perfcellh11.NoWrap = false;
                                //perfcellh11.Width = 15;
                                tableperfhist.AddCell(perfcellh11);
                                PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                                perfcellh12.Border = 0;
                                perfcellh12.NoWrap = false;
                                //perfcellh12.Width = 15;
                                tableperfhist.AddCell(perfcellh12);
                                PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                                perfcellh13.Border = 0;
                                perfcellh13.NoWrap = false;
                                //perfcellh13.SpaceCharRatio = 10;
                                tableperfhist.AddCell(perfcellh13);
                                PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                                perfcellh14.Border = 0;
                                perfcellh14.NoWrap = false;
                                //perfcellh14.Width = 15;
                                tableperfhist.AddCell(perfcellh14);
                                PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                                perfcellh15.Border = 0;
                                perfcellh15.NoWrap = false;
                                //perfcellh15.Width = 15;
                                tableperfhist.AddCell(perfcellh15);
                                PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                                perfcellh16.Border = 0;
                                perfcellh16.NoWrap = false;
                                //perfcellh16.Width = 15;
                                tableperfhist.AddCell(perfcellh16);
                                PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                                perfcellh17.Border = 0;
                                perfcellh17.NoWrap = false;
                                //perfcellh17.Width = 15;
                                tableperfhist.AddCell(perfcellh17);
                                PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                                perfcellh18.Border = 0;
                                perfcellh18.NoWrap = false;
                                //perfcellh18.Width = 15;
                                tableperfhist.AddCell(perfcellh18);
                            }

                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"]), font));
                            perfcelli1.Border = 0;
                            tableperfhist.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"]), font));
                            perfcelli2.Border = 0;
                            tableperfhist.AddCell(perfcelli2);
                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"]), font));
                            perfcelli3.Border = 0;
                            tableperfhist.AddCell(perfcelli3);
                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"]), font));
                            perfcelli4.Border = 0;
                            tableperfhist.AddCell(perfcelli4);
                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"]), font));
                            perfcelli5.Border = 0;
                            tableperfhist.AddCell(perfcelli5);
                            PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"]), font));
                            perfcelli6.Border = 0;
                            tableperfhist.AddCell(perfcelli6);
                            PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"]), font));
                            perfcelli7.Border = 0;
                            tableperfhist.AddCell(perfcelli7);
                            PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"]), font));
                            perfcelli8.Border = 0;
                            tableperfhist.AddCell(perfcelli8);
                            PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"]), font));
                            perfcelli9.Border = 0;
                            tableperfhist.AddCell(perfcelli9);

                        }
                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                        perfcellh1.Border = 0;
                        tableperfhist.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("FullName", font));
                        perfcellh2.Border = 0;
                        tableperfhist.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number", font));
                        perfcellh3.Border = 0;
                        tableperfhist.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                        perfcellh4.Border = 0;
                        tableperfhist.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                        perfcellh5.Border = 0;
                        tableperfhist.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                        perfcellh6.Border = 0;
                        tableperfhist.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name", font));
                        perfcellh7.Border = 0;
                        tableperfhist.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date", font));
                        perfcellh8.Border = 0;
                        tableperfhist.AddCell(perfcellh8);
                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By", font));
                        perfcellh9.Border = 0;
                        tableperfhist.AddCell(perfcellh9);


                        PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                        perfcellh10.Border = 0;
                        perfcellh10.NoWrap = false;
                        //perfcellh10.Width = 15;
                        tableperfhist.AddCell(perfcellh10);
                        PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                        perfcellh11.Border = 0;
                        perfcellh11.NoWrap = false;
                        //perfcellh11.Width = 15;
                        tableperfhist.AddCell(perfcellh11);
                        PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                        perfcellh12.Border = 0;
                        perfcellh12.NoWrap = false;
                        //perfcellh12.Width = 15;
                        tableperfhist.AddCell(perfcellh12);
                        PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                        perfcellh13.Border = 0;
                        perfcellh13.NoWrap = false;
                        //perfcellh13.SpaceCharRatio = 10;
                        tableperfhist.AddCell(perfcellh13);
                        PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                        perfcellh14.Border = 0;
                        perfcellh14.NoWrap = false;
                        //perfcellh14.Width = 15;
                        tableperfhist.AddCell(perfcellh14);
                        PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                        perfcellh15.Border = 0;
                        perfcellh15.NoWrap = false;
                        //perfcellh15.Width = 15;
                        tableperfhist.AddCell(perfcellh15);
                        PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                        perfcellh16.Border = 0;
                        perfcellh16.NoWrap = false;
                        //perfcellh16.Width = 15;
                        tableperfhist.AddCell(perfcellh16);
                        PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                        perfcellh17.Border = 0;
                        perfcellh17.NoWrap = false;
                        //perfcellh17.Width = 15;
                        tableperfhist.AddCell(perfcellh17);
                        PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                        perfcellh18.Border = 0;
                        perfcellh18.NoWrap = false;
                        //perfcellh18.Width = 15;
                        tableperfhist.AddCell(perfcellh18);

                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);

                    }


                }
                else
                {

                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                    perfcellh1.Border = 0;
                    tableperfhist.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                    perfcellh2.Border = 0;
                    tableperfhist.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                    perfcellh3.Border = 0;
                    tableperfhist.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                    perfcellh4.Border = 0;
                    tableperfhist.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                    perfcellh5.Border = 0;
                    tableperfhist.AddCell(perfcellh5);
                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh6.Border = 0;
                    tableperfhist.AddCell(perfcellh6);
                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                    perfcellh7.Border = 0;
                    tableperfhist.AddCell(perfcellh7);
                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                    perfcellh8.Border = 0;
                    tableperfhist.AddCell(perfcellh8);
                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                    perfcellh9.Border = 0;
                    tableperfhist.AddCell(perfcellh9);


                    PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                    perfcellh10.Border = 0;
                    perfcellh10.NoWrap = false;
                    //perfcellh10.Width = 15;
                    tableperfhist.AddCell(perfcellh10);
                    PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                    perfcellh11.Border = 0;
                    perfcellh11.NoWrap = false;
                    //perfcellh11.Width = 15;
                    tableperfhist.AddCell(perfcellh11);
                    PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                    perfcellh12.Border = 0;
                    perfcellh12.NoWrap = false;
                    //perfcellh12.Width = 15;
                    tableperfhist.AddCell(perfcellh12);
                    PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                    perfcellh13.Border = 0;
                    perfcellh13.NoWrap = false;
                    //perfcellh13.SpaceCharRatio = 10;
                    tableperfhist.AddCell(perfcellh13);
                    PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                    perfcellh14.Border = 0;
                    perfcellh14.NoWrap = false;
                    //perfcellh14.Width = 15;
                    tableperfhist.AddCell(perfcellh14);
                    PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                    perfcellh15.Border = 0;
                    perfcellh15.NoWrap = false;
                    //perfcellh15.Width = 15;
                    tableperfhist.AddCell(perfcellh15);
                    PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                    perfcellh16.Border = 0;
                    perfcellh16.NoWrap = false;
                    //perfcellh16.Width = 15;
                    tableperfhist.AddCell(perfcellh16);
                    PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                    perfcellh17.Border = 0;
                    perfcellh17.NoWrap = false;
                    //perfcellh17.Width = 15;
                    tableperfhist.AddCell(perfcellh17);
                    PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                    perfcellh18.Border = 0;
                    perfcellh18.NoWrap = false;
                    //perfcellh18.Width = 15;
                    tableperfhist.AddCell(perfcellh18);

                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                    perfcelli1.Border = 0;
                    tableperfhist.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                    perfcelli2.Border = 0;
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);


                }



                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 40;
                p13.SpacingAfter = 40;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header, font));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;
                table.WidthPercentage = 100;

                table.HorizontalAlignment = 0;
                table.TotalWidth = 600f;
                table.LockedWidth = true;
                float[] widthstable = new float[] { 150f, 150f, 150f, 150f };
                table.SetWidths(widthstable);


                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                            notescellh1.Border = 0;
                            table.AddCell(notescellh1);
                            PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                            notescellh2.Border = 0;
                            table.AddCell(notescellh2);
                            PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                            notescellh3.Border = 0;
                            table.AddCell(notescellh3);
                            PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                            notescellh4.Border = 0;
                            table.AddCell(notescellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                        notescelli1.Border = 0;
                        table.AddCell(notescelli1);


                        PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                        notescelli2.Border = 0;
                        table.AddCell(notescelli2);

                        PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                        notescelli3.Border = 0;
                        table.AddCell(notescelli3);

                        PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                        notescelli4.Border = 0;
                        table.AddCell(notescelli4);


                    }
                }
                else
                {
                    PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                    notescellh1.Border = 0;
                    table.AddCell(notescellh1);
                    PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                    notescellh2.Border = 0;
                    table.AddCell(notescellh2);
                    PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                    notescellh3.Border = 0;
                    table.AddCell(notescellh3);
                    PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                    notescellh4.Border = 0;
                    table.AddCell(notescellh4);
                    PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded.", font));
                    notescellh5.Border = 0;
                    table.AddCell(notescellh5);
                    PdfPCell notescellh6 = new PdfPCell(new Phrase("", font));
                    notescellh6.Border = 0;
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                }

                Paragraph p2 = new Paragraph();
                string line2 = "NTE Details " + Environment.NewLine;
                p2.Alignment = Element.ALIGN_LEFT;
                p2.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p2.Add(line2);
                p2.SpacingBefore = 10;
                p2.SpacingAfter = 10;


                PdfPTable ntetable = new PdfPTable(4);
                string nteheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell ntecell = new PdfPCell(new Phrase(nteheader, font));
                ntecell.Colspan = 4;
                ntecell.Border = 0;
                ntecell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                ntetable.AddCell(ntecell);
                ntetable.WidthPercentage = 100;

                PdfPCell ntehrreplbl = new PdfPCell(new Phrase("HR Representative: ", font));
                ntehrreplbl.Border = 0;
                ntetable.AddCell(ntehrreplbl);
                PdfPCell ntehrrep = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HR_Cim"]), font));
                ntehrrep.Border = 0;
                ntetable.AddCell(ntehrrep);


                PdfPCell statlbl = new PdfPCell(new Phrase("Status: ", font));
                statlbl.Border = 0;
                ntetable.AddCell(statlbl);
                PdfPCell statval = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["statusname"]), font));
                statval.Border = 0;
                ntetable.AddCell(statval);

                PdfPCell saplbl = new PdfPCell(new Phrase("Tandim/Sap Reference #:	", font));
                saplbl.Border = 0;
                ntetable.AddCell(saplbl);
                PdfPCell sap = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Reference_Number"]), font));
                sap.Border = 0;
                ntetable.AddCell(sap);

                PdfPCell ntedraftlbl = new PdfPCell(new Phrase("NTE draft submitted for HR Approval:	 ", font));
                ntedraftlbl.Border = 0;
                ntetable.AddCell(ntedraftlbl);
                PdfPCell ntedraft = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NTE_Draft_SubmittedDate"]), font));
                ntedraft.Border = 0;
                ntetable.AddCell(ntedraft);

                PdfPCell caselbl = new PdfPCell(new Phrase("Case Level: ", font));
                caselbl.Border = 0;
                ntetable.AddCell(caselbl);
                PdfPCell caseval = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["case_name"]), font));
                caseval.Border = 0;
                ntetable.AddCell(caseval);

                PdfPCell nteapprvlbl = new PdfPCell(new Phrase("NTE draft Approval Date:	 ", font));
                nteapprvlbl.Border = 0;
                ntetable.AddCell(nteapprvlbl);
                PdfPCell nteapprv = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NTE_Draft_ApprovalDate"]), font));
                nteapprv.Border = 0;
                ntetable.AddCell(nteapprv);

                PdfPCell MAJCATlbl = new PdfPCell(new Phrase("Major Category: ", font));
                MAJCATlbl.Border = 0;
                ntetable.AddCell(MAJCATlbl);
                PdfPCell MAJCAT = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["mj_name"]), font));
                MAJCAT.Border = 0;
                ntetable.AddCell(MAJCAT);

                PdfPCell nteisslbl = new PdfPCell(new Phrase("NTE Issue Date:	:	 ", font));
                nteisslbl.Border = 0;
                ntetable.AddCell(nteisslbl);
                PdfPCell nteiss = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NTE_IssueDate"]), font));
                nteiss.Border = 0;
                ntetable.AddCell(nteiss);

                PdfPCell infralbl = new PdfPCell(new Phrase("Infraction Class: ", font));
                infralbl.Border = 0;
                ntetable.AddCell(infralbl);
                PdfPCell infra = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["infraction_name"]), font));
                infra.Border = 0;
                ntetable.AddCell(infra);

                PdfPCell blank = new PdfPCell(new Phrase(" ", font));
                blank.Border = 0;
                ntetable.AddCell(blank);
                ntetable.AddCell(blank);

                PdfPTable ntetable1 = new PdfPTable(4);
                string nteheader1 = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell ntecell1 = new PdfPCell(new Phrase(nteheader1, font));
                ntecell1.Colspan = 4;
                ntecell1.Border = 0;
                ntecell1.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                ntetable1.AddCell(ntecell1);
                ntetable1.WidthPercentage = 100;
                PdfPCell wholdlbl = new PdfPCell(new Phrase("With hold case? ", font));
                wholdlbl.Border = 0;
                string withhold1 = "";
                if (RadWithHoldCase.SelectedItem == null) { withhold1 = ""; }
                else
                {
                    withhold1 = RadWithHoldCase.SelectedItem.Text;
                }
                ntetable1.AddCell(wholdlbl);
                PdfPCell whold = new PdfPCell(new Phrase(Convert.ToString(withhold1), font));
                whold.Border = 0;
                ntetable1.AddCell(whold);

                PdfPCell hcasetypelbl = new PdfPCell(new Phrase("Hold Case Type ", font));
                hcasetypelbl.Border = 0;
                ntetable1.AddCell(hcasetypelbl);
                PdfPCell hcasetype = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["hold_case_type_name"]), font));
                hcasetype.Border = 0;
                ntetable1.AddCell(hcasetype);

                PdfPCell hstartlbl = new PdfPCell(new Phrase("Hold Start Date ", font));
                hstartlbl.Border = 0;
                ntetable1.AddCell(hstartlbl);
                PdfPCell hstart = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["hold_startdate"]), font));
                hstart.Border = 0;
                ntetable1.AddCell(hstart);

                PdfPCell hrsnoteslbl = new PdfPCell(new Phrase("HR Superior Notes ", font));
                hrsnoteslbl.Border = 0;
                ntetable1.AddCell(hrsnoteslbl);
                PdfPCell hrsnotes = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HR_Superior_Notes"]), font));
                hrsnotes.Border = 0;
                //hrsnotes.Rowspan = 2;
                ntetable1.AddCell(hrsnotes);

                PdfPCell hendlbl = new PdfPCell(new Phrase("Hold End Date ", font));
                hendlbl.Border = 0;
                ntetable1.AddCell(hendlbl);
                PdfPCell hend = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["hold_enddate"]), font));
                hend.Border = 0;
                ntetable1.AddCell(hend);
                ntetable1.AddCell(blank);
                ntetable1.AddCell(blank);

                PdfPCell eacceptslbl = new PdfPCell(new Phrase("Employee Accepts ", font));
                eacceptslbl.Border = 0;
                string employeeacc = "";
                if (RadEmployeeAccepts.SelectedItem == null) { employeeacc = ""; }
                else
                {
                    employeeacc = RadEmployeeAccepts.SelectedItem.Text;
                }
                ntetable1.AddCell(eacceptslbl);
                PdfPCell eaccepts = new PdfPCell(new Phrase(Convert.ToString(employeeacc), font));
                eaccepts.Border = 0;
                ntetable1.AddCell(eaccepts);
                PdfPCell nonaccptlbl = new PdfPCell(new Phrase("Non-acceptance reason:", font));
                nonaccptlbl.Border = 0;
                ntetable1.AddCell(nonaccptlbl);
                PdfPCell nonaccpt = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Non_Acceptance_Reason"]), font));
                nonaccpt.Border = 0;
                ntetable1.AddCell(nonaccpt);


                PdfPCell sign1lbl = new PdfPCell(new Phrase("Sign Date:", font));
                sign1lbl.Border = 0;
                ntetable1.AddCell(sign1lbl);
                PdfPCell sign1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Witness_1_SignDate"]), font));
                sign1.Border = 0;
                ntetable1.AddCell(sign1);
                PdfPCell accptnoteslbl = new PdfPCell(new Phrase(" ", font));
                accptnoteslbl.Border = 0;
                ntetable1.AddCell(accptnoteslbl);
                PdfPCell accptnotes = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Acceptance_Notes"]), font));
                accptnotes.Border = 0;
                ntetable1.AddCell(accptnotes);

                PdfPCell wit1lbl = new PdfPCell(new Phrase("Witness:", font));
                wit1lbl.Border = 0;
                ntetable1.AddCell(wit1lbl);
                PdfPCell wit1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Witness_1"]), font));
                wit1.Border = 0;
                ntetable1.AddCell(wit1);
                ntetable1.AddCell(blank);
                ntetable1.AddCell(blank);

                PdfPCell sign2lbl = new PdfPCell(new Phrase("Sign Date:", font));
                sign2lbl.Border = 0;
                ntetable1.AddCell(sign2lbl);
                PdfPCell sign2 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Witness_2_SignDate"]), font));
                sign2.Border = 0;
                ntetable1.AddCell(sign2);
                ntetable1.AddCell(blank);
                ntetable1.AddCell(blank);


                PdfPCell wit2lbl = new PdfPCell(new Phrase("Witness:", font));
                wit2lbl.Border = 0;
                ntetable1.AddCell(wit2lbl);
                PdfPCell wit2 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Witness_2"]), font));
                wit2.Border = 0;
                ntetable1.AddCell(wit2);
                ntetable1.AddCell(blank);
                ntetable1.AddCell(blank);

                PdfPCell RTNTElbl = new PdfPCell(new Phrase("RTNTE Receive Date: ", font));
                RTNTElbl.Border = 0;
                ntetable1.AddCell(RTNTElbl);
                PdfPCell RTNTE = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["RTNTE_ReceiveDate"]), font));
                RTNTE.Border = 0;
                ntetable1.AddCell(RTNTE);
                ntetable1.AddCell(blank);
                ntetable1.AddCell(blank);

                Paragraph p6 = new Paragraph();
                string line6 = "Preventive Suspension Details " + Environment.NewLine;
                p6.Alignment = Element.ALIGN_LEFT;
                p6.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p6.Add(line6);
                p6.SpacingBefore = 10;
                p6.SpacingAfter = 10;

                PdfPTable preventivetable = new PdfPTable(4);
                string preventiveheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell preventivecell = new PdfPCell(new Phrase(preventiveheader, font));
                preventivecell.Border = 0;
                preventivecell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //  preventivetable.AddCell(preventivecell);

                preventivetable.WidthPercentage = 100;

                PdfPCell wsuslbl = new PdfPCell(new Phrase("With Suspension?", font));
                wsuslbl.Border = 0;
                preventivetable.AddCell(wsuslbl);
                string withval = "";
                if (RadWithSuspension.SelectedItem == null) { withval = ""; }
                else { withval = RadWithSuspension.SelectedItem.Text; }
                PdfPCell wsus = new PdfPCell(new Phrase(Convert.ToString(withval), font));
                wsus.Border = 0;
                preventivetable.AddCell(wsus);
                preventivetable.AddCell(blank);
                preventivetable.AddCell(blank);

                PdfPCell addh1lbl = new PdfPCell(new Phrase("Admin Hearing Schedule 1:", font));
                addh1lbl.Border = 0;
                preventivetable.AddCell(addh1lbl);
                PdfPCell addh1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Schedule1"]), font));
                addh1.Border = 0;
                preventivetable.AddCell(addh1);
                PdfPCell addhclbl = new PdfPCell(new Phrase("Admin Hearing Conducted", font));
                addhclbl.Border = 0;
                preventivetable.AddCell(addhclbl);
                PdfPCell addc1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["AdminHearingDate"]), font));
                addc1.Border = 0;
                preventivetable.AddCell(addc1);



                PdfPCell addh2lbl = new PdfPCell(new Phrase("Admin Hearing Schedule 2:", font));
                addh2lbl.Border = 0;
                preventivetable.AddCell(addh2lbl);
                PdfPCell addh2 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Schedule2"]), font));
                addh2.Border = 0;
                preventivetable.AddCell(addh2);
                preventivetable.AddCell(blank);
                preventivetable.AddCell(blank);

                Paragraph p5 = new Paragraph();
                string line5 = "NOD Details " + Environment.NewLine;
                p5.Alignment = Element.ALIGN_LEFT;
                p5.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p5.Add(line5);
                p5.SpacingBefore = 10;
                p5.SpacingAfter = 10;


                PdfPTable nodtable = new PdfPTable(3);
                string nodheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell nodcell = new PdfPCell(new Phrase(nodheader, font));
                nodcell.Colspan = 3;
                nodcell.Border = 0;
                nodcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                nodtable.SpacingBefore = 10;
                nodtable.SpacingAfter = 10;
                PdfPCell hrseclbl = new PdfPCell(new Phrase("HR Section:", font));
                hrseclbl.Border = 0;
                nodtable.AddCell(blank);
                nodtable.WidthPercentage = 100;

                nodtable.AddCell(hrseclbl); ////Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Schedule2"])));
                nodtable.AddCell(blank);
                string cbval = "";
                if (CBList.SelectedItem == null) { cbval = ""; }
                else
                {
                    cbval = CBList.SelectedItem.Text;
                }
                PdfPCell hrsec = new PdfPCell(new Phrase(Convert.ToString(cbval), font));
                hrsec.Border = 0;
                nodtable.AddCell(hrsec);
                nodtable.AddCell(blank);
                nodtable.AddCell(blank);



                PdfPCell NODsublbl = new PdfPCell(new Phrase("NOD submission for approval:", font));
                NODsublbl.Border = 0;
                nodtable.AddCell(NODsublbl); ////Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Schedule2"])));
                PdfPCell NODapprvlbl = new PdfPCell(new Phrase("NOD approval Date: ", font));
                NODapprvlbl.Border = 0;
                nodtable.AddCell(NODapprvlbl);
                PdfPCell NODissuelbl = new PdfPCell(new Phrase("NOD issue date: ", font));
                NODissuelbl.Border = 0;
                nodtable.AddCell(NODissuelbl);

                PdfPCell NODsub = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_SubmissionDate"]), font));
                NODsub.Border = 0;
                nodtable.AddCell(NODsub);
                PdfPCell NODapprv = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_ApprovalDate"]), font));
                NODapprv.Border = 0;
                nodtable.AddCell(NODapprv);
                PdfPCell NODissue = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_IssueDate"]), font));
                NODissue.Border = 0;
                nodtable.AddCell(NODissue);


                PdfPTable nodtable1 = new PdfPTable(4);
                string nod1header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell nod1cell = new PdfPCell(new Phrase(nod1header, font));
                nod1cell.Colspan = 4;
                nod1cell.Border = 0;
                nod1cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                nodtable1.SpacingBefore = 10;
                nodtable1.SpacingAfter = 10;
                //nodtable1.AddCell(blank);
                //nodtable1.AddCell(blank);
                nodtable1.WidthPercentage = 100;
                PdfPCell NODwithholdlbl = new PdfPCell(new Phrase("With hold case?", font));
                NODwithholdlbl.Border = 0;
                nodtable1.AddCell(NODwithholdlbl);
                string radnodwithhold = "";
                if (RadNODWithHoldCase.SelectedItem == null) { }
                else
                {

                    radnodwithhold = RadNODWithHoldCase.SelectedItem.Text;

                }
                PdfPCell NODwithhold = new PdfPCell(new Phrase(Convert.ToString(radnodwithhold), font));
                NODwithhold.Border = 0;
                nodtable1.AddCell(NODwithhold);
                PdfPCell NODholdcasetypelbl = new PdfPCell(new Phrase("Hold Case Type", font));
                NODholdcasetypelbl.Border = 0;
                nodtable1.AddCell(NODholdcasetypelbl);
                PdfPCell NODholdcasetype = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Hold_Case_Type"]), font));
                NODholdcasetype.Border = 0;
                nodtable1.AddCell(NODholdcasetype);



                PdfPCell NODholdstartlbl = new PdfPCell(new Phrase("Hold Start Date", font));
                NODholdstartlbl.Border = 0;
                nodtable1.AddCell(NODholdstartlbl);
                PdfPCell NODholdstart = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Hold_StartDate"]), font));
                NODholdstart.Border = 0;
                nodtable1.AddCell(NODholdstart);
                PdfPCell NODHRnoteslbl = new PdfPCell(new Phrase("HR Immediate Supervisor Notes", font));
                NODHRnoteslbl.Border = 0;
                nodtable1.AddCell(NODHRnoteslbl);
                PdfPCell NODHRnotes = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_HR_Superior_Notes"])));
                NODHRnotes.Border = 0;
                nodtable1.AddCell(NODHRnotes);

                PdfPCell NODholdendlbl = new PdfPCell(new Phrase("Hold end Date", font));
                NODholdendlbl.Border = 0;
                nodtable1.AddCell(NODholdendlbl);
                PdfPCell NODholdend = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Hold_EndDate"]), font));
                NODholdend.Border = 0;
                nodtable1.AddCell(NODholdend);
                nodtable1.AddCell(blank);
                nodtable1.AddCell(blank);

                PdfPTable nodtable2 = new PdfPTable(4);
                string nod2header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell nod2cell = new PdfPCell(new Phrase(nod2header, font));
                nod2cell.Colspan = 4;
                nod2cell.Border = 0;
                nod2cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                nodtable2.SpacingBefore = 10;
                nodtable2.SpacingAfter = 10;
                //nodtable1.AddCell(blank);
                //nodtable1.AddCell(blank);
                nodtable2.WidthPercentage = 100;
                PdfPCell NODempacceptslbl = new PdfPCell(new Phrase("Employee Accepts?	", font));
                NODempacceptslbl.Border = 0;
                nodtable2.AddCell(NODempacceptslbl);
                string nodeaccept = "";
                if (RadNODEmployeeAccept.SelectedItem == null)
                {
                    nodeaccept = "";
                }
                else
                {
                    nodeaccept = RadNODEmployeeAccept.SelectedItem.Text;
                }
                PdfPCell NODempaccepts = new PdfPCell(new Phrase(Convert.ToString(nodeaccept), font));
                NODempaccepts.Border = 0;
                nodtable2.AddCell(NODempaccepts);
                PdfPCell NODnonacceptlbl = new PdfPCell(new Phrase("Non-acceptance reason:	", font));
                NODnonacceptlbl.Border = 0;
                nodtable2.AddCell(NODnonacceptlbl);
                PdfPCell NODnonaccept = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Non_Acceptance_Reason"]), font));
                NODnonaccept.Border = 0;
                nodtable2.AddCell(NODnonaccept);
                PdfPCell NODsign1lbl = new PdfPCell(new Phrase("Sign Date:	", font));
                NODsign1lbl.Border = 0;
                nodtable2.AddCell(NODsign1lbl);
                PdfPCell NODsign1 = new PdfPCell(new Phrase(Convert.ToString(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Witness_1_SignDate"])), font));
                NODsign1.Border = 0;
                nodtable2.AddCell(NODsign1);
                PdfPCell NODnonacceptnotelbl = new PdfPCell(new Phrase("Acceptance Notes", font));
                NODnonacceptnotelbl.Border = 0;
                nodtable2.AddCell(NODnonacceptnotelbl);
                PdfPCell NODnonacceptnote = new PdfPCell(new Phrase(Convert.ToString(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Acceptance_Notes"]))));
                NODnonacceptnote.Border = 0;
                nodtable2.AddCell(NODnonacceptnote);

                PdfPCell NODwitness1lbl = new PdfPCell(new Phrase("Witness", font));
                NODwitness1lbl.Border = 0;
                nodtable2.AddCell(NODwitness1lbl);
                PdfPCell NODwitness1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Witness_1"]), font));
                NODwitness1.Border = 0;
                nodtable2.AddCell(NODwitness1);
                nodtable2.AddCell(blank);
                nodtable2.AddCell(blank);

                PdfPCell NODwitnessd1lbl = new PdfPCell(new Phrase("Sign Date:", font));
                NODwitnessd1lbl.Border = 0;
                nodtable2.AddCell(NODwitnessd1lbl);
                PdfPCell NODwitnessd1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Witness_1_SignDate"]), font));
                NODwitnessd1.Border = 0;
                nodtable2.AddCell(NODwitnessd1);
                nodtable2.AddCell(blank);
                nodtable2.AddCell(blank);

                PdfPCell NODwitness2lbl = new PdfPCell(new Phrase("Witness", font));
                NODwitness2lbl.Border = 0;
                nodtable2.AddCell(NODwitness2lbl);
                PdfPCell NODwitness2 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Witness_2"]), font));
                NODwitness2.Border = 0;
                nodtable2.AddCell(NODwitness2);
                nodtable2.AddCell(blank);
                nodtable2.AddCell(blank);

                PdfPCell NODwitnessd2lbl = new PdfPCell(new Phrase("Sign Date:", font));
                NODwitnessd2lbl.Border = 0;
                nodtable2.AddCell(NODwitnessd2lbl);
                PdfPCell NODwitnessd2 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Witness_2_SignDate"]), font));
                NODwitnessd2.Border = 0;
                nodtable2.AddCell(NODwitnessd2);
                nodtable2.AddCell(blank);
                nodtable2.AddCell(blank);
                //nodtable2.AddCell(blank);
                //nodtable2.AddCell(blank);


                PdfPTable nodtable3 = new PdfPTable(3);
                string nod3header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell nod3cell = new PdfPCell(new Phrase(nod3header, font));
                nod3cell.Colspan = 3;
                nod3cell.Border = 0;
                nod3cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                nodtable3.SpacingBefore = 10;
                nodtable3.SpacingAfter = 10;
                PdfPCell NODretlbl = new PdfPCell(new Phrase("NOD return date:", font));
                NODretlbl.Border = 0;
                nodtable3.AddCell(NODretlbl);
                PdfPCell NODret = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_ReturnDate"]), font));
                NODret.Border = 0;
                nodtable3.AddCell(NODret);
                nodtable3.AddCell(blank);
                nodtable3.WidthPercentage = 100;
                PdfPCell NODesclbl = new PdfPCell(new Phrase("Escalated by:", font));
                NODesclbl.Border = 0;
                nodtable3.AddCell(NODesclbl);
                PdfPCell NODesc = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_EscalatedBy"]), font));
                NODesc.Border = 0;
                nodtable3.AddCell(NODesc);
                nodtable3.AddCell(blank);

                PdfPCell NODCasedeclbl = new PdfPCell(new Phrase("Case Decision:", font));
                NODCasedeclbl.Border = 0;
                nodtable3.AddCell(NODCasedeclbl);
                PdfPCell NODCasedec = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["NOD_Decision"]), font));
                NODCasedec.Border = 0;
                nodtable3.AddCell(NODCasedec);
                nodtable3.AddCell(blank);

                Paragraph p3 = new Paragraph();
                string line3 = "Case Decision Comments " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Case_Decision_Notes"]);
                p3.Alignment = Element.ALIGN_LEFT;
                p3.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p3.Add(line3);
                p3.SpacingBefore = 10;
                p3.SpacingAfter = 10;

                Paragraph p4 = new Paragraph();
                string line4 = "End-of-week action required: " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["EOW_Notes"]);
                p4.Alignment = Element.ALIGN_LEFT;
                p4.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p4.Add(line4);
                p4.SpacingBefore = 10;
                p4.SpacingAfter = 10;



                //nodtable1.AddCell(blank);
                //nodtable1.AddCell(blank);

                pdfDoc.Add(specificstable);
                pdfDoc.Add(new Paragraph(p1)); //HRComments
                pdfDoc.Add(new Paragraph(p12)); //coaching notes
                pdfDoc.Add(notestable); //coaching notes table
                pdfDoc.Add(new Paragraph(p16));//perfhistory
                pdfDoc.Add(tableperfhist);
                pdfDoc.Add(new Paragraph(p13)); //documentation
                pdfDoc.Add(table);//documentation table
                pdfDoc.Add(new Paragraph(p2));
                pdfDoc.Add(ntetable);
                pdfDoc.Add(ntetable1);
                pdfDoc.Add(new Paragraph(p6));//preventive details
                pdfDoc.Add(preventivetable);
                pdfDoc.Add(new Paragraph(p5));//nod details
                pdfDoc.Add(nodtable);
                pdfDoc.Add(nodtable1);
                pdfDoc.Add(nodtable2);
                pdfDoc.Add(nodtable3);
                pdfDoc.Add(new Paragraph(p3)); //case decision comments
                pdfDoc.Add(new Paragraph(p4)); //End-of-week action required:
                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void PrintNexidia()
        {
            try
            {
                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                //FakeURLID.Value = URL;
                //if (!Directory.Exists(directoryPath))
                //{
                //    ImgDefault = URL + "/Content/images/no-photo.jpg";
                //}
                //else
                //{

                //    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                //}

                //Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //pdfDoc.NewPage();

                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();

                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                specificscelli8.Border = 0;


                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                specificscelli10.Border = 0;

                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                specificscelli14.Border = 0;

                PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                specificscelli15.Border = 0;
                PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                specificscelli16.Border = 0;

                PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                specificscelli17.Border = 0;
                PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                specificscelli18.Border = 0;
                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli15);
                specificstable.AddCell(specificscelli16);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);

                specificstable.AddCell(specificscelli17);
                specificstable.AddCell(specificscelli18);
                specificstable.SpacingAfter = 40;

                //                    ds_get_review_forPDF.Tables[0].Rows[0][""]
                Paragraph p9 = new Paragraph(); //Description
                string line9 = "What is the purpose of this coaching? : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]);
                p9.Alignment = Element.ALIGN_LEFT;
                p9.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p9.Add(line9);
                p9.SpacingBefore = 40;

                Paragraph p10 = new Paragraph();
                string line10 = "Begin with Behaviour " + Environment.NewLine + "What positive behaviour did the employee exhibit? " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["PositiveBehaviour"]);
                string line10a = Environment.NewLine + "What opportunity did the employee exhibit in the behaviour? " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["OpportunityBehaviour"]);
                p10.Alignment = Element.ALIGN_LEFT;
                p10.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p10.Add(line10);
                p10.Add(line10a);
                p10.SpacingBefore = 10;
                p10.SpacingAfter = 10;

                Paragraph p11 = new Paragraph();
                string line11 = "Questions(See Opening questions-successes and Areas for development.Prepare 1-2.)." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["BeginBehaviourComments"]);
                p11.Alignment = Element.ALIGN_LEFT;
                p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p11.Add(line11);
                p11.SpacingBefore = 10;
                p11.SpacingAfter = 10;

                Paragraph p12 = new Paragraph();
                string line12 = "Outline Action Plans. Keep it concise." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["AddressOpportunities"]);
                p12.Alignment = Element.ALIGN_LEFT;
                p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p12.Add(line12);
                p12.SpacingBefore = 10;
                p12.SpacingAfter = 10;

                Paragraph p14 = new Paragraph();
                string line14 = "Questions(See general proving & discovery.Prepare 1-2.)." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["ActionPlanComments"]); //(RadActionPlanComments.Text);
                p14.Alignment = Element.ALIGN_LEFT;
                p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p14.Add(line14);
                p14.SpacingBefore = 10;
                p14.SpacingAfter = 10;


                Paragraph p15 = new Paragraph();
                string line15 = "Create Plan" + Environment.NewLine + Convert.ToString(RadSmartGoals.Text) + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatePlanComments"]); //(RadCreatePlanComments.Text);
                p15.Alignment = Element.ALIGN_LEFT;
                p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p15.Add(line15);
                p15.SpacingBefore = 10;
                p15.SpacingAfter = 10;



                Paragraph p18 = new Paragraph();
                string line18 = "Performance Result " + Environment.NewLine;
                p18.Alignment = Element.ALIGN_LEFT;
                p18.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p18.Add(line18);
                p18.SpacingBefore = 10;
                p18.SpacingAfter = 10;


                PdfPTable perftable = new PdfPTable(5);
                string perfheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell perfcell = new PdfPCell(new Phrase(perfheader, font));
                perfcell.Colspan = 5;
                perfcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                perftable.AddCell(perfcell);
                perftable.SpacingBefore = 30;
                perftable.SpacingAfter = 30;
                perftable.WidthPercentage = 100;

                perftable.HorizontalAlignment = 0;
                perftable.TotalWidth = 500f;
                perftable.LockedWidth = true;
                float[] widths = new float[] { 100f, 50f, 50f, 50f, 250f };
                perftable.SetWidths(widths);

                DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadReviewID.Text));
                if (ds_KPILIST.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_KPILIST.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI", font));
                            perfcellh1.Border = 0;
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target", font));
                            perfcellh2.Border = 0;
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current", font));
                            perfcellh3.Border = 0;
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                            perfcellh4.Border = 0;
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description", font));
                            perfcellh5.Border = 0;
                            perftable.AddCell(perfcellh1);
                            perftable.AddCell(perfcellh2);
                            perftable.AddCell(perfcellh3);
                            perftable.AddCell(perfcellh4);
                            perftable.AddCell(perfcellh5);
                        }
                        PdfPCell perfcellitem1 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["KPI"]), font));
                        perfcellitem1.Border = 0;
                        perftable.AddCell(perfcellitem1);

                        PdfPCell perfcellitem2 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Target"]), font));
                        perfcellitem2.Border = 0;
                        perftable.AddCell(perfcellitem2);

                        PdfPCell perfcellitem3 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Current"]), font));
                        perfcellitem3.Border = 0;
                        perftable.AddCell(perfcellitem3);

                        PdfPCell perfcellitem4 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Previous"]), font));
                        perfcellitem4.Border = 0;
                        perftable.AddCell(perfcellitem4);


                        PdfPCell perfcellitem5 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Description"]), font));
                        perfcellitem5.Border = 0;
                        perftable.AddCell(perfcellitem5);
                    }
                }
                else
                {
                    //perftable.AddCell("Target");
                    //perftable.AddCell("Current");
                    //perftable.AddCell("Previous");
                    //perftable.AddCell("Description");
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI", font));
                    perfcellh1.Border = 0;
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target", font));
                    perfcellh2.Border = 0;
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current", font));
                    perfcellh3.Border = 0;
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh4.Border = 0;
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description", font));
                    perfcellh5.Border = 0;
                    perftable.AddCell(perfcellh1);
                    perftable.AddCell(perfcellh2);
                    perftable.AddCell(perfcellh3);
                    perftable.AddCell(perfcellh4);
                    perftable.AddCell(perfcellh5);
                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Summary", font));
                    perfcelli1.Border = 0;
                    perftable.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase(" ", font));
                    perfcelli2.Border = 0;
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                    perftable.AddCell(perfcelli2);
                }

                Paragraph p16 = new Paragraph();
                string line16 = "Coaching Notes " + Environment.NewLine;
                p16.Alignment = Element.ALIGN_LEFT;
                p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p16.Add(line16);
                p16.SpacingBefore = 10;
                p16.SpacingAfter = 10;



                PdfPTable tablenotes = new PdfPTable(5);
                string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell notescell = new PdfPCell(new Phrase(notesheader, font));
                notescell.Colspan = 5;
                notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tablenotes.AddCell(notescell);
                tablenotes.SpacingBefore = 30;
                tablenotes.SpacingAfter = 30;
                tablenotes.WidthPercentage = 100;

                tablenotes.HorizontalAlignment = 0;
                tablenotes.TotalWidth = 500f;
                tablenotes.LockedWidth = true;
                float[] widthstablenotes = new float[] { 50f, 100f, 100f, 150f, 100f };
                tablenotes.SetWidths(widthstablenotes);

                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            tablenotes.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                            perfcellh2.Border = 0;
                            tablenotes.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                            perfcellh3.Border = 0;
                            tablenotes.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                            perfcellh4.Border = 0;
                            tablenotes.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh5.Border = 0;
                            tablenotes.AddCell(perfcellh4);

                        }
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"]), font));
                        perfcelli1.Border = 0;
                        tablenotes.AddCell(perfcelli1);

                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"]), font));
                        perfcelli2.Border = 0;
                        tablenotes.AddCell(perfcelli2);

                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"]), font));
                        perfcelli3.Border = 0;
                        tablenotes.AddCell(perfcelli3);

                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"]), font));
                        perfcelli4.Border = 0;
                        tablenotes.AddCell(perfcelli4);

                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"]), font));
                        perfcelli5.Border = 0;
                        tablenotes.AddCell(perfcelli5);


                    }

                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    tablenotes.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                    perfcellh2.Border = 0;
                    tablenotes.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                    perfcellh3.Border = 0;
                    tablenotes.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                    perfcellh4.Border = 0;
                    tablenotes.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh5.Border = 0;
                    tablenotes.AddCell(perfcellh4);

                    PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes.", font));
                    notescelli1.Border = 0;
                    tablenotes.AddCell(notescelli1);
                    PdfPCell notescelli2 = new PdfPCell(new Phrase(" ", font));
                    notescelli2.Border = 0;
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                }


                DataSet ds_perfhistory = null;
                DataAccess ws1 = new DataAccess();
                ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadReviewID.Text)), 2);

                //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                Paragraph p17 = new Paragraph();
                string line17 = "Performance history";
                p17.Alignment = Element.ALIGN_LEFT;
                p17.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p17.Add(line17);
                p17.SpacingBefore = 10;
                p17.SpacingAfter = 10;

                PdfPTable tableperfhist = new PdfPTable(9);
                string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader, font));
                perfhistcell.Colspan = 9;
                perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableperfhist.AddCell(perfhistcell);
                tableperfhist.SpacingBefore = 30;
                tableperfhist.SpacingAfter = 30;
                tableperfhist.WidthPercentage = 100;

                tableperfhist.HorizontalAlignment = 0;
                tableperfhist.TotalWidth = 500f;
                tableperfhist.LockedWidth = true;
                float[] widthstableperfhist = new float[] { 50f, 50f, 45f, 45f, 45f, 45f, 120f, 50f, 50f };
                tableperfhist.SetWidths(widthstableperfhist);

                if (CheckBox2.Checked == true)
                {
                    if (ds_perfhistory.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                                perfcellh1.Border = 0;
                                tableperfhist.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                                perfcellh2.Border = 0;
                                tableperfhist.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                                perfcellh3.Border = 0;
                                tableperfhist.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                                perfcellh4.Border = 0;
                                tableperfhist.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                                perfcellh5.Border = 0;
                                tableperfhist.AddCell(perfcellh5);
                                PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                                perfcellh6.Border = 0;
                                tableperfhist.AddCell(perfcellh6);
                                PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                                perfcellh7.Border = 0;
                                tableperfhist.AddCell(perfcellh7);
                                PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                                perfcellh8.Border = 0;
                                tableperfhist.AddCell(perfcellh8);
                                PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                                perfcellh9.Border = 0;
                                tableperfhist.AddCell(perfcellh9);


                                PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                                perfcellh10.Border = 0;
                                perfcellh10.NoWrap = false;
                                //perfcellh10.Width = 15;
                                tableperfhist.AddCell(perfcellh10);
                                PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                                perfcellh11.Border = 0;
                                perfcellh11.NoWrap = false;
                                //perfcellh11.Width = 15;
                                tableperfhist.AddCell(perfcellh11);
                                PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                                perfcellh12.Border = 0;
                                perfcellh12.NoWrap = false;
                                //perfcellh12.Width = 15;
                                tableperfhist.AddCell(perfcellh12);
                                PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                                perfcellh13.Border = 0;
                                perfcellh13.NoWrap = false;
                                //perfcellh13.SpaceCharRatio = 10;
                                tableperfhist.AddCell(perfcellh13);
                                PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                                perfcellh14.Border = 0;
                                perfcellh14.NoWrap = false;
                                //perfcellh14.Width = 15;
                                tableperfhist.AddCell(perfcellh14);
                                PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                                perfcellh15.Border = 0;
                                perfcellh15.NoWrap = false;
                                //perfcellh15.Width = 15;
                                tableperfhist.AddCell(perfcellh15);
                                PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                                perfcellh16.Border = 0;
                                perfcellh16.NoWrap = false;
                                //perfcellh16.Width = 15;
                                tableperfhist.AddCell(perfcellh16);
                                PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                                perfcellh17.Border = 0;
                                perfcellh17.NoWrap = false;
                                //perfcellh17.Width = 15;
                                tableperfhist.AddCell(perfcellh17);
                                PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                                perfcellh18.Border = 0;
                                perfcellh18.NoWrap = false;
                                //perfcellh18.Width = 15;
                                tableperfhist.AddCell(perfcellh18);
                            }

                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"]), font));
                            perfcelli1.Border = 0;
                            tableperfhist.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"]), font));
                            perfcelli2.Border = 0;
                            tableperfhist.AddCell(perfcelli2);
                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"]), font));
                            perfcelli3.Border = 0;
                            tableperfhist.AddCell(perfcelli3);
                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"]), font));
                            perfcelli4.Border = 0;
                            tableperfhist.AddCell(perfcelli4);
                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"]), font));
                            perfcelli5.Border = 0;
                            tableperfhist.AddCell(perfcelli5);
                            PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"]), font));
                            perfcelli6.Border = 0;
                            tableperfhist.AddCell(perfcelli6);
                            PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"]), font));
                            perfcelli7.Border = 0;
                            tableperfhist.AddCell(perfcelli7);
                            PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"]), font));
                            perfcelli8.Border = 0;
                            tableperfhist.AddCell(perfcelli8);
                            PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"]), font));
                            perfcelli9.Border = 0;
                            tableperfhist.AddCell(perfcelli9);

                        }
                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                        perfcellh1.Border = 0;
                        tableperfhist.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                        perfcellh2.Border = 0;
                        tableperfhist.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                        perfcellh3.Border = 0;
                        tableperfhist.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                        perfcellh4.Border = 0;
                        tableperfhist.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                        perfcellh5.Border = 0;
                        tableperfhist.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                        perfcellh6.Border = 0;
                        tableperfhist.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                        perfcellh7.Border = 0;
                        tableperfhist.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                        perfcellh8.Border = 0;
                        tableperfhist.AddCell(perfcellh8);
                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                        perfcellh9.Border = 0;
                        tableperfhist.AddCell(perfcellh9);

                        PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                        perfcellh10.Border = 0;
                        perfcellh10.NoWrap = false;
                        //perfcellh10.Width = 15;
                        tableperfhist.AddCell(perfcellh10);
                        PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                        perfcellh11.Border = 0;
                        perfcellh11.NoWrap = false;
                        //perfcellh11.Width = 15;
                        tableperfhist.AddCell(perfcellh11);
                        PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                        perfcellh12.Border = 0;
                        perfcellh12.NoWrap = false;
                        //perfcellh12.Width = 15;
                        tableperfhist.AddCell(perfcellh12);
                        PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                        perfcellh13.Border = 0;
                        perfcellh13.NoWrap = false;
                        //perfcellh13.SpaceCharRatio = 10;
                        tableperfhist.AddCell(perfcellh13);
                        PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                        perfcellh14.Border = 0;
                        perfcellh14.NoWrap = false;
                        //perfcellh14.Width = 15;
                        tableperfhist.AddCell(perfcellh14);
                        PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                        perfcellh15.Border = 0;
                        perfcellh15.NoWrap = false;
                        //perfcellh15.Width = 15;
                        tableperfhist.AddCell(perfcellh15);
                        PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                        perfcellh16.Border = 0;
                        perfcellh16.NoWrap = false;
                        //perfcellh16.Width = 15;
                        tableperfhist.AddCell(perfcellh16);
                        PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                        perfcellh17.Border = 0;
                        perfcellh17.NoWrap = false;
                        //perfcellh17.Width = 15;
                        tableperfhist.AddCell(perfcellh17);
                        PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                        perfcellh18.Border = 0;
                        perfcellh18.NoWrap = false;
                        //perfcellh18.Width = 15;
                        tableperfhist.AddCell(perfcellh18);
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);

                    }


                }
                else
                {

                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching", font));
                    perfcellh1.Border = 0;
                    tableperfhist.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full", font));
                    perfcellh2.Border = 0;
                    tableperfhist.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM", font));
                    perfcellh3.Border = 0;
                    tableperfhist.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                    perfcellh4.Border = 0;
                    tableperfhist.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                    perfcellh5.Border = 0;
                    tableperfhist.AddCell(perfcellh5);
                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh6.Border = 0;
                    tableperfhist.AddCell(perfcellh6);
                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver", font));
                    perfcellh7.Border = 0;
                    tableperfhist.AddCell(perfcellh7);
                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review", font));
                    perfcellh8.Border = 0;
                    tableperfhist.AddCell(perfcellh8);
                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned", font));
                    perfcellh9.Border = 0;
                    tableperfhist.AddCell(perfcellh9);
                    PdfPCell perfcellh10 = new PdfPCell(new Phrase("Ticket", font));
                    perfcellh10.Border = 0;
                    perfcellh10.NoWrap = false;
                    //perfcellh10.Width = 15;;
                    tableperfhist.AddCell(perfcellh10);
                    PdfPCell perfcellh11 = new PdfPCell(new Phrase("Name", font));
                    perfcellh11.Border = 0;
                    perfcellh11.NoWrap = false;
                    //perfcellh11.Width = 15;
                    tableperfhist.AddCell(perfcellh11);
                    PdfPCell perfcellh12 = new PdfPCell(new Phrase("Number", font));
                    perfcellh12.Border = 0;
                    perfcellh12.NoWrap = false;
                    //perfcellh12.Width = 15;
                    tableperfhist.AddCell(perfcellh12);
                    PdfPCell perfcellh13 = new PdfPCell(new Phrase(" ", font));
                    perfcellh13.Border = 0;
                    perfcellh13.NoWrap = false;
                    //perfcellh13.SpaceCharRatio = 10;
                    tableperfhist.AddCell(perfcellh13);
                    PdfPCell perfcellh14 = new PdfPCell(new Phrase(" ", font));
                    perfcellh14.Border = 0;
                    perfcellh14.NoWrap = false;
                    //perfcellh14.Width = 15;
                    tableperfhist.AddCell(perfcellh14);
                    PdfPCell perfcellh15 = new PdfPCell(new Phrase(" ", font));
                    perfcellh15.Border = 0;
                    perfcellh15.NoWrap = false;
                    //perfcellh15.Width = 15;
                    tableperfhist.AddCell(perfcellh15);
                    PdfPCell perfcellh16 = new PdfPCell(new Phrase("Name", font));
                    perfcellh16.Border = 0;
                    perfcellh16.NoWrap = false;
                    //perfcellh16.Width = 15;
                    tableperfhist.AddCell(perfcellh16);
                    PdfPCell perfcellh17 = new PdfPCell(new Phrase("Date", font));
                    perfcellh17.Border = 0;
                    perfcellh17.NoWrap = false;
                    //perfcellh17.Width = 15;
                    tableperfhist.AddCell(perfcellh17);
                    PdfPCell perfcellh18 = new PdfPCell(new Phrase("By", font));
                    perfcellh18.Border = 0;
                    perfcellh18.NoWrap = false;
                    //perfcellh18.Width = 15;
                    tableperfhist.AddCell(perfcellh18);
                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                    perfcelli1.Border = 0;
                    tableperfhist.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                    perfcelli2.Border = 0;
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);


                }
                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 10;
                p13.SpacingAfter = 10;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header, font));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;
                table.WidthPercentage = 100;

                table.HorizontalAlignment = 0;
                table.TotalWidth = 600f;
                table.LockedWidth = true;
                float[] widthstable = new float[] { 150f, 150f, 150f, 150f };
                table.SetWidths(widthstable);

                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {

                        if (ctr == 0)
                        {
                            //table.AddCell("Item Number");
                            //table.AddCell("Document Name");
                            //table.AddCell("Uploaded By");
                            //table.AddCell("Date Uploaded");
                            PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                            cellh1.Border = 0;
                            table.AddCell(cellh1);
                            PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                            cellh2.Border = 0;
                            table.AddCell(cellh2);
                            PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                            cellh3.Border = 0;
                            table.AddCell(cellh3);
                            PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                            cellh4.Border = 0;
                            table.AddCell(cellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));
                        PdfPCell celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                        celli1.Border = 0;
                        table.AddCell(celli1);


                        PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                        celli2.Border = 0;
                        table.AddCell(celli2);

                        PdfPCell celli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                        celli3.Border = 0;
                        table.AddCell(celli3);

                        PdfPCell celli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                        celli4.Border = 0;
                        table.AddCell(celli4);
                    }
                }
                else
                {
                    PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                    cellh1.Border = 0;
                    table.AddCell(cellh1);
                    PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                    cellh2.Border = 0;
                    table.AddCell(cellh2);
                    PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                    cellh3.Border = 0;
                    table.AddCell(cellh3);
                    PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                    cellh4.Border = 0;
                    table.AddCell(cellh4);

                    PdfPCell celli1 = new PdfPCell(new Phrase("No documents uploaded.", font));
                    celli1.Border = 0;
                    table.AddCell(celli1);
                    PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(""), font));
                    celli2.Border = 0;
                    table.AddCell(celli2);
                    table.AddCell(celli2);
                    table.AddCell(celli2);

                }




                pdfDoc.Add(specificstable);
                pdfDoc.Add(new Paragraph(p9)); //description
                pdfDoc.Add(new Paragraph(p10)); //"Begin with Behaviour "
                pdfDoc.Add(new Paragraph(p11)); //"Questions(See Opening questions-successes and Areas for development.Prepare 1-2.)."
                pdfDoc.Add(new Paragraph(p12)); //"Outline Action Plans. Keep it concise."
                pdfDoc.Add(new Paragraph(p14)); //"Questions(See general proving & discovery.Prepare 1-2.)." 
                pdfDoc.Add(new Paragraph(p15)); //create plan
                pdfDoc.Add(new Paragraph(p18)); //perftable
                pdfDoc.Add(perftable);
                pdfDoc.Add(new Paragraph(p16)); //coaching notes
                pdfDoc.Add(tablenotes); //coaching notes
                pdfDoc.Add(new Paragraph(p17));
                pdfDoc.Add(tableperfhist);
                pdfDoc.Add(new Paragraph(p13)); //documentation
                pdfDoc.Add(table);//documentation



                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected void PrintOD()
        {


            try
            {
                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                FakeURLID.Value = URL;
                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = URL + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();




                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                specificscell.Colspan = 2;

                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;

                specificstable.AddCell(specificscell);
                //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                //specificscelli00.Border = 0;
                //                   specificscelli00.Rowspan = 7;


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                specificscelli8.Border = 0;

                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                specificscelli10.Border = 0;

                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                specificscelli14.Border = 0;


                PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                specificscelli15.Border = 0;
                PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                specificscelli16.Border = 0;

                PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                specificscelli17.Border = 0;
                PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                specificscelli18.Border = 0;

                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli15);
                specificstable.AddCell(specificscelli16);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);
                specificstable.AddCell(specificscelli17);
                specificstable.AddCell(specificscelli18);
                specificstable.SpacingAfter = 40;


                Paragraph p5 = new Paragraph();
                DataSet ds_focusname = DataHelper.getsessionfocus(CheckBoxList1.SelectedValue);
                string line5 = "";
                string line5a1 = "Call ID : " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CallID"]);

                string line5a = Environment.NewLine + "Session Type : " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]);//RadSessionType.Text);
                string line5a2 = Environment.NewLine + "Topic: " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]);//RadSessionTopic.Text);

                if (ds_focusname.Tables[0].Rows.Count > 0)
                {
                    line5 = Environment.NewLine + "Session Focus : " + Convert.ToString(ds_focusname.Tables[0].Rows[0]["SessionFocusName"]);
                }
                else
                {

                    line5 = Environment.NewLine + "Session Focus : ";
                }
                p5.Alignment = Element.ALIGN_LEFT;
                p5.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p5.Add(line5a1);
                p5.Add(line5a2);
                p5.Add(line5a);
                p5.Add(line5);


                Paragraph p9 = new Paragraph();
                string line9 = "Description : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]);
                p9.Alignment = Element.ALIGN_LEFT;
                p9.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p9.Add(line9);
                p9.SpacingBefore = 40;

                Paragraph p10 = new Paragraph();
                string line10 = "Performance Result ";
                p10.Alignment = Element.ALIGN_LEFT;
                p10.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p10.Add(line10);
                p10.SpacingBefore = 10;
                p10.SpacingAfter = 10;

                PdfPTable tableperf = new PdfPTable(9);
                string headerperf = "";// "Name    Target      Current     Previous     Change     Driver      Behaviour       Root Cause";
                PdfPCell cellperf = new PdfPCell(new Phrase(headerperf, font));
                cellperf.Colspan = 9;
                cellperf.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableperf.AddCell(cellperf);
                tableperf.SpacingBefore = 30;
                tableperf.SpacingAfter = 30;
                tableperf.WidthPercentage = 100;

                tableperf.HorizontalAlignment = 0;
                tableperf.TotalWidth = 500f;
                tableperf.LockedWidth = true;
                float[] widths = new float[] { 50f, 50f, 45f, 45f, 45f, 120f, 45f, 50f, 50f };
                tableperf.SetWidths(widths);


                DataSet ds_kpiperf = DataHelper.GetKPIPerfODv2(Convert.ToInt32(RadReviewID.Text));
                if (ds_kpiperf.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_kpiperf.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {

                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Name", font));
                            perfcellh1.Border = 0;
                            tableperf.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("DataView", font));
                            perfcellh2.Border = 0;
                            tableperf.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Target", font));
                            perfcellh3.Border = 0;
                            tableperf.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                            perfcellh4.Border = 0;
                            tableperf.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                            perfcellh5.Border = 0;
                            tableperf.AddCell(perfcellh5);
                            PdfPCell perfcellh6 = new PdfPCell(new Phrase("Drivers", font));
                            perfcellh6.Border = 0;
                            tableperf.AddCell(perfcellh6);
                            PdfPCell perfcellh7 = new PdfPCell(new Phrase("Change", font));
                            perfcellh7.Border = 0;
                            tableperf.AddCell(perfcellh7);
                            PdfPCell perfcellh8 = new PdfPCell(new Phrase("Behaviour", font));
                            perfcellh8.Border = 0;
                            tableperf.AddCell(perfcellh8);
                            PdfPCell perfcellh9 = new PdfPCell(new Phrase("Root Cause", font));
                            perfcellh9.Border = 0;
                            tableperf.AddCell(perfcellh9);

                        }

                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Name"]), font));
                        perfcelli1.Border = 0;
                        tableperf.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["DataView"]), font));
                        perfcelli2.Border = 0;
                        tableperf.AddCell(perfcelli2);
                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Target"]), font));
                        perfcelli3.Border = 0;
                        tableperf.AddCell(perfcelli3);
                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Previous"]), font));
                        perfcelli4.Border = 0;
                        tableperf.AddCell(perfcelli4);
                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Current"]), font));
                        perfcelli5.Border = 0;
                        tableperf.AddCell(perfcelli5);
                        PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Drivers"]), font));
                        perfcelli6.Border = 0;
                        tableperf.AddCell(perfcelli6);
                        PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Change"]), font));
                        perfcelli7.Border = 0;
                        tableperf.AddCell(perfcelli7);
                        PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Behaviour"]), font));
                        perfcelli8.Border = 0;
                        tableperf.AddCell(perfcelli8);
                        PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["RootCause"]), font));
                        perfcelli9.Border = 0;
                        tableperf.AddCell(perfcelli9);
                    }
                }
                else
                {


                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Name", font));
                    perfcellh1.Border = 0;
                    tableperf.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("DataView", font));
                    perfcellh2.Border = 0;
                    tableperf.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Target", font));
                    perfcellh3.Border = 0;
                    tableperf.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh4.Border = 0;
                    tableperf.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                    perfcellh5.Border = 0;
                    tableperf.AddCell(perfcellh5);
                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Drivers", font));
                    perfcellh6.Border = 0;
                    tableperf.AddCell(perfcellh6);
                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Change", font));
                    perfcellh7.Border = 0;
                    tableperf.AddCell(perfcellh7);
                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Behaviour", font));
                    perfcellh8.Border = 0;
                    tableperf.AddCell(perfcellh8);
                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Root Cause", font));
                    perfcellh9.Border = 0;
                    tableperf.AddCell(perfcellh9);

                    PdfPCell perfcellh10 = new PdfPCell(new Phrase("No Performance Result.", font));
                    perfcellh10.Border = 0;
                    tableperf.AddCell(perfcellh10);
                    PdfPCell perfcellh11 = new PdfPCell(new Phrase("", font));
                    perfcellh11.Border = 0;
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    tableperf.AddCell(perfcellh11);
                    //  alldocs = "No documents uploaded.";
                }


                Paragraph p11 = new Paragraph();
                string line11 = "Coaching Notes " + Environment.NewLine;
                p11.Alignment = Element.ALIGN_LEFT;
                p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p11.Add(line11);
                p11.SpacingBefore = 10;
                p11.SpacingAfter = 10;


                PdfPTable tablenotes = new PdfPTable(5);
                string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell notescell = new PdfPCell(new Phrase(notesheader, font));
                notescell.Colspan = 5;
                notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                tablenotes.AddCell(notescell);
                tablenotes.SpacingBefore = 30;
                tablenotes.SpacingAfter = 30;
                tablenotes.WidthPercentage = 100;

                tablenotes.HorizontalAlignment = 0;
                tablenotes.TotalWidth = 500f;
                tablenotes.LockedWidth = true;
                float[] widthstablenotes = new float[] { 50f, 100f, 100f, 150f, 100f };
                tablenotes.SetWidths(widthstablenotes);

                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            tablenotes.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                            perfcellh2.Border = 0;
                            tablenotes.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                            perfcellh3.Border = 0;
                            tablenotes.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                            perfcellh4.Border = 0;
                            tablenotes.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh5.Border = 0;
                            tablenotes.AddCell(perfcellh4);

                        }
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"]), font));
                        perfcelli1.Border = 0;
                        tablenotes.AddCell(perfcelli1);

                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"]), font));
                        perfcelli2.Border = 0;
                        tablenotes.AddCell(perfcelli2);

                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"]), font));
                        perfcelli3.Border = 0;
                        tablenotes.AddCell(perfcelli3);

                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"]), font));
                        perfcelli4.Border = 0;
                        tablenotes.AddCell(perfcelli4);

                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"]), font));
                        perfcelli5.Border = 0;
                        tablenotes.AddCell(perfcelli5);

                    }

                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    tablenotes.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                    perfcellh2.Border = 0;
                    tablenotes.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                    perfcellh3.Border = 0;
                    tablenotes.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                    perfcellh4.Border = 0;
                    tablenotes.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh5.Border = 0;
                    tablenotes.AddCell(perfcellh4);

                    PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes.", font));
                    notescelli1.Border = 0;
                    tablenotes.AddCell(notescelli1);
                    PdfPCell notescelli2 = new PdfPCell(new Phrase(" ", font));
                    notescelli2.Border = 0;
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                    tablenotes.AddCell(notescelli2);
                }


                Paragraph p12 = new Paragraph();
                string line12 = "Strengths " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Strengths"]); //RadStrengths.Text);
                p12.Alignment = Element.ALIGN_LEFT;
                p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p12.Add(line12);
                p12.SpacingBefore = 10;
                p12.SpacingAfter = 10;

                Paragraph p14 = new Paragraph();
                string line14 = "Opportunities " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Opportunity"]);//RadOpportunities.Text);
                p14.Alignment = Element.ALIGN_LEFT;
                p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p14.Add(line14);
                p14.SpacingBefore = 10;
                p14.SpacingAfter = 10;


                Paragraph p15 = new Paragraph();
                string line15 = "Commitment";
                p15.Alignment = Element.ALIGN_LEFT;
                p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p15.Add(line15);
                p15.SpacingBefore = 10;
                p15.SpacingAfter = 10;

                PdfPTable commitment = new PdfPTable(4);
                string commheader = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell commcell = new PdfPCell(new Phrase(commheader, font));
                commcell.Colspan = 4;
                commcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                commitment.AddCell(commcell);
                commitment.SpacingBefore = 30;
                commitment.SpacingAfter = 30;
                commitment.WidthPercentage = 100;

                commitment.HorizontalAlignment = 0;
                commitment.TotalWidth = 600f;
                commitment.LockedWidth = true;
                float[] widthscommitment = new float[] { 150f, 150f, 150f, 150f };
                commitment.SetWidths(widthscommitment);

                DataSet ds_scoring = DataHelper.GetCommentsforTriadpdf(Convert.ToInt32(RadReviewID.Text)); //GetCommentsforTriad(Convert.ToInt32(RadReviewID.Text));
                if (ds_scoring.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_scoring.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal", font));
                            commitmentcellh1.Border = 0;
                            commitment.AddCell(commitmentcellh1);
                            PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality", font));
                            commitmentcellh2.Border = 0;
                            commitment.AddCell(commitmentcellh2);
                            PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options", font));
                            commitmentcellh3.Border = 0;
                            commitment.AddCell(commitmentcellh3);
                            PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward", font));
                            commitmentcellh4.Border = 0;
                            commitment.AddCell(commitmentcellh4);


                        }

                        PdfPCell commitmentcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Goal_Text"]), font));
                        commitmentcelli1.Border = 0;
                        commitment.AddCell(commitmentcelli1);
                        PdfPCell commitmentcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Reality_Text"]), font));
                        commitmentcelli2.Border = 0;
                        commitment.AddCell(commitmentcelli2);
                        PdfPCell commitmentcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Options_Text"]), font));
                        commitmentcelli3.Border = 0;
                        commitment.AddCell(commitmentcelli3);
                        PdfPCell commitmentcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["WayForward_Text"]), font));
                        commitmentcelli4.Border = 0;
                        commitment.AddCell(commitmentcelli4);
                    }
                }
                else
                {
                    PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal", font));
                    commitmentcellh1.Border = 0;
                    commitment.AddCell(commitmentcellh1);
                    PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality", font));
                    commitmentcellh2.Border = 0;
                    commitment.AddCell(commitmentcellh2);
                    PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options", font));
                    commitmentcellh3.Border = 0;
                    commitment.AddCell(commitmentcellh3);
                    PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward", font));
                    commitmentcellh4.Border = 0;
                    commitment.AddCell(commitmentcellh4);
                    PdfPCell commitmentcellh5 = new PdfPCell(new Phrase("No GROW Comments", font));
                    commitmentcellh5.Border = 0;
                    commitment.AddCell(commitmentcellh5);
                    PdfPCell commitmentcellh6 = new PdfPCell(new Phrase("", font));
                    commitmentcellh6.Border = 0;
                    commitment.AddCell(commitmentcellh6);
                    commitment.AddCell(commitmentcellh6);
                    commitment.AddCell(commitmentcellh6);


                }

                DataTable dt = null;
                DataAccess ws = new DataAccess();
                DataSet ds_prevcommitment = DataHelper.getpreviouscommitment(Convert.ToInt32(RadReviewID.Text), 1);// .getpreviouscommitment(Convert.ToInt32(RadReviewID.Text, 1));
                Paragraph p16 = new Paragraph();
                string line16 = "Previous Commitment";
                p16.Alignment = Element.ALIGN_LEFT;
                p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p16.Add(line16);
                p16.SpacingBefore = 10;
                p16.SpacingAfter = 10;

                PdfPTable commitment1 = new PdfPTable(4);
                string commheader1 = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell commcell1 = new PdfPCell(new Phrase(commheader1, font));
                commcell1.Colspan = 4;
                commcell1.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                commitment1.AddCell(commcell1);
                commitment1.SpacingBefore = 30;
                commitment1.SpacingAfter = 30;
                commitment1.WidthPercentage = 100;

                commitment1.HorizontalAlignment = 0;
                commitment1.TotalWidth = 600f;
                commitment1.LockedWidth = true;
                float[] widthscommitment1 = new float[] { 150f, 150f, 150f, 150f };
                commitment1.SetWidths(widthscommitment1);

                DataSet ds_scoring1 = DataHelper.getpreviouscommitment(Convert.ToInt32(RadReviewID.Text), 1); //GetCommentsforTriadpdf(Convert.ToInt32(RadReviewID.Text)); //GetCommentsforTriad(Convert.ToInt32(RadReviewID.Text));
                if (ds_scoring1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_scoring1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal", font));
                            commitmentcellh1.Border = 0;
                            commitment1.AddCell(commitmentcellh1);
                            PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality", font));
                            commitmentcellh2.Border = 0;
                            commitment1.AddCell(commitmentcellh2);
                            PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options", font));
                            commitmentcellh3.Border = 0;
                            commitment1.AddCell(commitmentcellh3);
                            PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward", font));
                            commitmentcellh4.Border = 0;
                            commitment1.AddCell(commitmentcellh4);


                        }

                        PdfPCell commitmentcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Goal_Text"]), font));
                        commitmentcelli1.Border = 0;
                        commitment1.AddCell(commitmentcelli1);
                        PdfPCell commitmentcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Reality_Text"]), font));
                        commitmentcelli2.Border = 0;
                        commitment1.AddCell(commitmentcelli2);
                        PdfPCell commitmentcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Options_Text"]), font));
                        commitmentcelli3.Border = 0;
                        commitment1.AddCell(commitmentcelli3);
                        PdfPCell commitmentcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["WayForward_Text"]), font));
                        commitmentcelli4.Border = 0;
                        commitment1.AddCell(commitmentcelli4);
                    }
                }
                else
                {
                    PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal", font));
                    commitmentcellh1.Border = 0;
                    commitment1.AddCell(commitmentcellh1);
                    PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality", font));
                    commitmentcellh2.Border = 0;
                    commitment1.AddCell(commitmentcellh2);
                    PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options", font));
                    commitmentcellh3.Border = 0;
                    commitment1.AddCell(commitmentcellh3);
                    PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward", font));
                    commitmentcellh4.Border = 0;
                    commitment1.AddCell(commitmentcellh4);
                    PdfPCell commitmentcellh5 = new PdfPCell(new Phrase("No GROW Comments", font));
                    commitmentcellh5.Border = 0;
                    commitment1.AddCell(commitmentcellh5);
                    PdfPCell commitmentcellh6 = new PdfPCell(new Phrase("", font));
                    commitmentcellh6.Border = 0;
                    commitment1.AddCell(commitmentcellh6);
                    commitment1.AddCell(commitmentcellh6);
                    commitment1.AddCell(commitmentcellh6);


                }


                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 10;
                p13.SpacingAfter = 10;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header, font));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;
                table.WidthPercentage = 100;


                table.HorizontalAlignment = 0;
                table.TotalWidth = 600f;
                table.LockedWidth = true;
                float[] widthstable = new float[] { 150f, 150f, 150f, 150f };
                table.SetWidths(widthstable);

                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                            cellh1.Border = 0;
                            table.AddCell(cellh1);
                            PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                            cellh2.Border = 0;
                            table.AddCell(cellh2);
                            PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                            cellh3.Border = 0;
                            table.AddCell(cellh3);
                            PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                            cellh4.Border = 0;
                            table.AddCell(cellh4);


                        }
                        PdfPCell celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                        celli1.Border = 0;
                        table.AddCell(celli1);


                        PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                        celli2.Border = 0;
                        table.AddCell(celli2);

                        PdfPCell celli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                        celli3.Border = 0;
                        table.AddCell(celli3);

                        PdfPCell celli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                        celli4.Border = 0;
                        table.AddCell(celli4);
                    }
                }
                else
                {
                    PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                    cellh1.Border = 0;
                    table.AddCell(cellh1);
                    PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                    cellh2.Border = 0;
                    table.AddCell(cellh2);
                    PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                    cellh3.Border = 0;
                    table.AddCell(cellh3);
                    PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                    cellh4.Border = 0;
                    table.AddCell(cellh4);

                    PdfPCell celli1 = new PdfPCell(new Phrase("No documents uploaded.", font));
                    celli1.Border = 0;
                    table.AddCell(celli1);
                    PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(""), font));
                    celli2.Border = 0;
                    table.AddCell(celli2);
                    table.AddCell(celli2);
                    table.AddCell(celli2);
                }





                pdfDoc.Add(specificstable); //coaching specifics
                pdfDoc.Add(new Paragraph(p9)); //description
                pdfDoc.Add(new Paragraph(p10)); //positive description
                pdfDoc.Add(tableperf); //performance table
                pdfDoc.Add(new Paragraph(p11)); //positive description
                pdfDoc.Add(tablenotes);
                pdfDoc.Add(new Paragraph(p12)); // description
                pdfDoc.Add(new Paragraph(p14)); //strenght
                pdfDoc.Add(new Paragraph(p15)); //opportunities
                pdfDoc.Add(commitment); //commitment table 
                pdfDoc.Add(new Paragraph(p16)); //opportunities
                pdfDoc.Add(commitment1); //previous commitment table 
                pdfDoc.Add(new Paragraph(p13)); //documentation
                pdfDoc.Add(table);//documentation



                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadFollowup_Load(object sender, EventArgs e)
        {
            (sender as RadDatePicker).MinDate = DateTime.Today;
        }
        private void LoadCommitmentQuestions()
        {
            RadTextBox1.Text = "What do you want to achieve?";
            RadTextBox3.Text = "Where are you know? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox5.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that?";
            RadTextBox7.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

        }
        private void GetDefaultForm(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo2(Convert.ToInt32(CIMNumber));
            Session["DefaultUserAcct"] = Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString());
            GetForm(Convert.ToInt32(Session["DefaultUserAcct"].ToString()));
        }
        protected void RadCMTSaveSubmit_Click(object sender, EventArgs e)
        {
            if (HiddenSearchBlackout.Value == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMTv3(); });", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { CyborgCMT(); });", true);
            }
        }
        protected void CyborgCMT_Click(object sender, EventArgs e)
        {

            try
            {

                if (RadReviewID.Text == "0")
                {
                    var RadHRRep = FindControl<RadNumericTextBox>(this.Controls, "RadHRRep");
                    var RadCaseLevel = FindControl<RadComboBox>(this.Controls, "RadCaseLevel");
                    var RadMajorCategory = FindControl<RadComboBox>(this.Controls, "RadMajorCategory");
                    var RadInfractionClass = FindControl<RadComboBox>(this.Controls, "RadInfractionClass");
                    var RadStatus = FindControl<RadComboBox>(this.Controls, "RadStatus");
                    var RadWithHoldCase = FindControl<RadComboBox>(this.Controls, "RadWithHoldCase");
                    var RadHoldCaseType = FindControl<RadComboBox>(this.Controls, "RadHoldCaseType");
                    var RadEmployeeAccepts = FindControl<RadComboBox>(this.Controls, "RadEmployeeAccepts");
                    var RadWitness1 = FindControl<RadNumericTextBox>(this.Controls, "RadWitness1");
                    var RadWitness2 = FindControl<RadNumericTextBox>(this.Controls, "RadWitness2");


                    int HRRep = string.IsNullOrEmpty(RadHRRep.Text) ? 0 : int.Parse(RadHRRep.Text);
                    int CaseLvl = string.IsNullOrEmpty(RadCaseLevel.SelectedValue) ? 0 : int.Parse(RadCaseLevel.SelectedValue);
                    int MJCat = string.IsNullOrEmpty(RadMajorCategory.SelectedValue) ? 0 : int.Parse(RadMajorCategory.SelectedValue);
                    int IC = string.IsNullOrEmpty(RadInfractionClass.SelectedValue) ? 0 : int.Parse(RadInfractionClass.SelectedValue);
                    int Stat = string.IsNullOrEmpty(RadStatus.SelectedValue) ? 0 : int.Parse(RadStatus.SelectedValue);
                    int WHCase = string.IsNullOrEmpty(RadWithHoldCase.SelectedValue) ? 0 : int.Parse(RadWithHoldCase.SelectedValue);
                    int HCType = string.IsNullOrEmpty(RadHoldCaseType.SelectedValue) ? 0 : int.Parse(RadHoldCaseType.SelectedValue);
                    int EmpAc = string.IsNullOrEmpty(RadEmployeeAccepts.SelectedValue) ? 0 : int.Parse(RadEmployeeAccepts.SelectedValue);
                    int Wit1 = string.IsNullOrEmpty(RadWitness1.Text) ? 0 : int.Parse(RadWitness1.Text);
                    int Wit2 = string.IsNullOrEmpty(RadWitness2.Text) ? 0 : int.Parse(RadWitness2.Text);
                    int WithSuspension = string.IsNullOrEmpty(RadWithSuspension.SelectedValue) ? 0 : int.Parse(RadWithSuspension.SelectedValue);
                    int CheckC = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
                    int NODWithHold = string.IsNullOrEmpty(RadNODWithHoldCase.SelectedValue) ? 0 : int.Parse(RadNODWithHoldCase.SelectedValue);
                    int NODHoldCaseType = string.IsNullOrEmpty(RadNODHoldCaseType.SelectedValue) ? 0 : int.Parse(RadNODHoldCaseType.SelectedValue);
                    int NODEmployeeAccept = string.IsNullOrEmpty(RadNODEmployeeAccept.SelectedValue) ? 0 : int.Parse(RadNODEmployeeAccept.SelectedValue);
                    int NODWitness1 = string.IsNullOrEmpty(RadNODWitness1.Text) ? 0 : int.Parse(RadNODWitness1.Text);
                    int NODWitness2 = string.IsNullOrEmpty(RadNODWitness2.Text) ? 0 : int.Parse(RadNODWitness2.Text);
                    int NODEscalatedBy = string.IsNullOrEmpty(RadEscalatedBy.Text) ? 0 : int.Parse(RadEscalatedBy.Text);
                    int NODCaseDecision = string.IsNullOrEmpty(RadCaseDecision.SelectedValue) ? 0 : int.Parse(RadCaseDecision.SelectedValue);
                    int NODNonAcceptance = string.IsNullOrEmpty(RadNODNonAcceptance.SelectedValue) ? 0 : int.Parse(RadNODNonAcceptance.SelectedValue);
                    int NonAcceptance = string.IsNullOrEmpty(RadNonAcceptance.SelectedValue) ? 0 : int.Parse(RadNonAcceptance.SelectedValue);
                    int NODEoWAction = string.IsNullOrEmpty(RadEoW.SelectedValue) ? 0 : int.Parse(RadEoW.SelectedValue);


                    string a = HiddenSearchBlackout.Value;
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    int CMTReviewID;
                    DataAccess ws = new DataAccess();
                    CMTReviewID = ws.InsertCMTReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, Convert.ToInt32(HiddenSearchBlackout.Value),
                    RadHRComments.Text, HRRep, RadSapReference.Text, CaseLvl, MJCat, IC, Stat, RadNTEDraft.SelectedDate, RadNTEApproval.SelectedDate, RadNTEIssueDate.SelectedDate, WHCase, RadHoldStartDate.SelectedDate, RadHoldEndDate.SelectedDate, HCType, RadHRImmediateComments.Text,
                    EmpAc, RadEmployeeAcceptDate.SelectedDate, Wit1, RadSignDate1.SelectedDate, Wit2, RadSignDate2.SelectedDate, NonAcceptance, RadNonAcceptanceNotes.Text, RadRTNTEReceiveDate.SelectedDate, WithSuspension, RadAdminHearingDate1.SelectedDate, RadAdminHearingDate2.SelectedDate, RadAdminHearingConduct.SelectedDate
                    , CheckC, RadNODSubmittedDate.SelectedDate, RadNODApproval.SelectedDate, RadNODIssueDate.SelectedDate, NODWithHold, RadNODHoldStartDate.SelectedDate, RadNODHoldEndDate.SelectedDate, NODHoldCaseType, RadNODHrSuperiorNotes.Text, NODEmployeeAccept, RadNODEmployeeSignDate.SelectedDate, NODWitness1, RadNODWitness1SignDate.SelectedDate,
                    NODWitness2, RadNODWitness2SignDate.SelectedDate, NODNonAcceptance, RadNODNonAcceptanceNotes.Text, RadNODReturnDate.SelectedDate, NODEscalatedBy, NODCaseDecision, RadNODCaseDecisionNotes.Text, RadNODEOWNotes.Text, NODEoWAction);
                    RadReviewID.Text = CMTReviewID.ToString();
                    DataHelper.SetTicketStatus(7, Convert.ToInt32(CMTReviewID)); // update status to coachersigned off 
                    InsertReviewHistoryCMT(1);
                    InsertReviewHistoryCMT(2);
                    UploadDocumentation(0, Convert.ToInt32(RadReviewID.Text));
                    LoadDocumentations();
                    if (RadAccount.Visible == true)
                    {
                        RadAccount.Enabled = false;
                    }
                    if (RadSupervisor.Visible == true)
                    {
                        RadSupervisor.Enabled = false;
                    }
                    if (RadCoacheeName.Visible == true)
                    {
                        RadCoacheeName.Enabled = false;
                    }
                    if (RadSessionType.Visible == true)
                    {
                        RadSessionType.Enabled = false;
                    }
                    if (RadSessionTopic.Visible == true)
                    {
                        RadSessionTopic.Enabled = false;
                    }
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    string ModalLabel = "Review has been submitted.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                }
                else
                {
                    int HRRep = string.IsNullOrEmpty(RadHRRep.Text) ? 0 : int.Parse(RadHRRep.Text);
                    int CaseLvl = string.IsNullOrEmpty(RadCaseLevel.SelectedValue) ? 0 : int.Parse(RadCaseLevel.SelectedValue);
                    int MJCat = string.IsNullOrEmpty(RadMajorCategory.SelectedValue) ? 0 : int.Parse(RadMajorCategory.SelectedValue);
                    int IC = string.IsNullOrEmpty(RadInfractionClass.SelectedValue) ? 0 : int.Parse(RadInfractionClass.SelectedValue);
                    int Stat = string.IsNullOrEmpty(RadStatus.SelectedValue) ? 0 : int.Parse(RadStatus.SelectedValue);
                    int WHCase = string.IsNullOrEmpty(RadWithHoldCase.SelectedValue) ? 0 : int.Parse(RadWithHoldCase.SelectedValue);
                    int HCType = string.IsNullOrEmpty(RadHoldCaseType.SelectedValue) ? 0 : int.Parse(RadHoldCaseType.SelectedValue);
                    int EmpAc = string.IsNullOrEmpty(RadEmployeeAccepts.SelectedValue) ? 0 : int.Parse(RadEmployeeAccepts.SelectedValue);
                    int Wit1 = string.IsNullOrEmpty(RadWitness1.Text) ? 0 : int.Parse(RadWitness1.Text);
                    int Wit2 = string.IsNullOrEmpty(RadWitness2.Text) ? 0 : int.Parse(RadWitness2.Text);
                    int WithSuspension = string.IsNullOrEmpty(RadWithSuspension.SelectedValue) ? 0 : int.Parse(RadWithSuspension.SelectedValue);
                    int CheckC = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
                    int NODWithHold = string.IsNullOrEmpty(RadNODWithHoldCase.SelectedValue) ? 0 : int.Parse(RadNODWithHoldCase.SelectedValue);
                    int NODHoldCaseType = string.IsNullOrEmpty(RadNODHoldCaseType.SelectedValue) ? 0 : int.Parse(RadNODHoldCaseType.SelectedValue);
                    int NODEmployeeAccept = string.IsNullOrEmpty(RadNODEmployeeAccept.SelectedValue) ? 0 : int.Parse(RadNODEmployeeAccept.SelectedValue);
                    int NODWitness1 = string.IsNullOrEmpty(RadNODWitness1.Text) ? 0 : int.Parse(RadNODWitness1.Text);
                    int NODWitness2 = string.IsNullOrEmpty(RadNODWitness2.Text) ? 0 : int.Parse(RadNODWitness2.Text);
                    int NODEscalatedBy = string.IsNullOrEmpty(RadEscalatedBy.Text) ? 0 : int.Parse(RadEscalatedBy.Text);
                    int NODCaseDecision = string.IsNullOrEmpty(RadCaseDecision.SelectedValue) ? 0 : int.Parse(RadCaseDecision.SelectedValue);
                    int NODNonAcceptance = string.IsNullOrEmpty(RadNODNonAcceptance.SelectedValue) ? 0 : int.Parse(RadNODNonAcceptance.SelectedValue);
                    int NODEoWAction = string.IsNullOrEmpty(RadEoW.SelectedValue) ? 0 : int.Parse(RadEoW.SelectedValue);


                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    ws.UpdateCMTReview(Convert.ToInt32(RadReviewID.Text), Convert.ToInt32(HiddenSearchBlackout.Value), RadHRComments.Text, HRRep, RadSapReference.Text, CaseLvl, MJCat, IC, Stat, RadNTEDraft.SelectedDate, RadNTEApproval.SelectedDate, RadNTEIssueDate.SelectedDate, WHCase, RadHoldStartDate.SelectedDate, RadHoldEndDate.SelectedDate, HCType, RadHRImmediateComments.Text,
                    EmpAc, RadEmployeeAcceptDate.SelectedDate, Wit1, RadSignDate1.SelectedDate, Wit2, RadSignDate2.SelectedDate, RadNonAcceptance.SelectedValue, RadNonAcceptanceNotes.Text, RadRTNTEReceiveDate.SelectedDate, WithSuspension, RadAdminHearingDate1.SelectedDate, RadAdminHearingDate2.SelectedDate, RadAdminHearingConduct.SelectedDate
                    , CheckC, RadNODSubmittedDate.SelectedDate, RadNODApproval.SelectedDate, RadNODIssueDate.SelectedDate, NODWithHold, RadNODHoldStartDate.SelectedDate, RadNODHoldEndDate.SelectedDate, NODHoldCaseType, RadNODHrSuperiorNotes.Text, NODEmployeeAccept, RadNODEmployeeSignDate.SelectedDate, NODWitness1, RadNODWitness1SignDate.SelectedDate,
                    NODWitness2, RadNODWitness2SignDate.SelectedDate, RadNODNonAcceptance.SelectedValue, RadNODNonAcceptanceNotes.Text, RadNODReturnDate.SelectedDate, NODEscalatedBy, NODCaseDecision, RadNODCaseDecisionNotes.Text, RadNODEOWNotes.Text, NODEoWAction);



                    DataHelper.SetTicketStatus(7, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                    UploadDocumentation(0, Convert.ToInt32(RadReviewID.Text));
                    LoadDocumentations();
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }

                    string ModalLabel = "Review has been updated.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }

            }
            catch (Exception ex)
            {
                string ModalLabel = "RadCMTSaveSubmit_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        private static T FindControl<T>(ControlCollection controls, string controlId)
        {
            T ctrl = default(T);
            foreach (Control ctl in controls)
            {
                if (ctl.GetType() != typeof(Telerik.Web.UI.GridTableRow))
                {
                    if (ctl.ClientID.Length <= controlId.Length)
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                    else
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                }


                if (ctl.Controls.Count > 0 && ctrl == null)
                    ctrl = FindControl<T>(ctl.Controls, controlId);
            }
            return ctrl;
        }
        private void InsertReviewHistoryCMT(int InclusionType)
        {
            try
            {

                if (InclusionType == 1)
                {
                    int ReviewID;

                    foreach (GridDataItem itm in RadGridCNCMT.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");
                        DataAccess ws = new DataAccess();
                        ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                    }
                }
                else
                {
                    int ReviewID;

                    foreach (GridDataItem itm in RadGridPRCMT.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                        DataAccess ws = new DataAccess();
                        ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "InsertReviewHistoryCMT " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }
        }
        protected void RadMajorCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetCMTDropdowns(3, Convert.ToInt32(RadMajorCategory.SelectedValue));

            RadInfractionClass.AllowCustomText = true;
            RadInfractionClass.Text = "";
            RadInfractionClass.ClearSelection();
            RadInfractionClass.AllowCustomText = false;
            RadInfractionClass.DataSource = ds;
            RadInfractionClass.DataTextField = "Infraction_Name";
            RadInfractionClass.DataValueField = "id";
            RadInfractionClass.DataBind();
        }

        //private void SendEmail(int CoachType,int ReviewID,string emailAddressCCM)
        //{

        //    if (CoachType == 1)//Coachee
        //    {
        //        toName = "To Approve";
        //        fromName = "Request Approval System";
        //        fromEmail = "ras@nucomm.net";

        //        //string CCMEmail = emailAddressCCM;
        //        //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
        //        target = "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
        //        htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + CIM + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\".</font></p>";
        //        htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
        //        subject = "Coach Request with ReviewID" + ReviewID.ToString() + " ";
        //        toEmail = emailAddressCCM;
        //        EmailNotifs ws = new EmailNotifs();
        //        ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
        //    }
        //    else {

        //        toName = "To Approve";
        //        fromName = "Request Approval System";
        //        fromEmail = "ras@nucomm.net";

        //        //string CCMEmail = emailAddressCCM;
        //        //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
        //        target = "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
        //        htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + CIM + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\". The review has been signed off by the agent.</font></p>";
        //        htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
        //        subject = "Coach Request Sign Off with ReviewID" + ReviewID.ToString() + " ";
        //        toEmail = emailAddressCCM;
        //        EmailNotifs ws = new EmailNotifs();
        //        ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
        //    }//Coacher
        //}
        private bool ValidatePage()
        {
            bool stat = true;
            if (RadAccount.Visible == true)
            {
                if (RadAccount.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select an account.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSupervisor.Visible == true)
            {
                if (RadSupervisor.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select supervisor.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadCoacheeName.Visible == true)
            {
                if (RadCoacheeName.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select coachee.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSessionType.Visible == true)
            {
                if (RadSessionType.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session type.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSessionTopic.Visible == true)
            {
                if (RadSessionTopic.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session topic.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadCallID.Visible == true)
            {
                if (RadCallID.Text == "")
                {
                    stat = false;
                    string ModalLabel = "Please enter call id.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (CheckBoxList1.Visible == true)
            {
                if (CheckBoxList1.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session focus.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadFollowup.Visible == true)
            {
                if (RadFollowup.SelectedDate.HasValue == false)
                {
                    stat = false;
                    string ModalLabel = "Please select enter follow up date.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSSN.Visible == true)
            {
                if (RadSSN.Text == "")
                {
                    stat = false;
                    string ModalLabel = "Please select enter sss id.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadCoacheeCIM.Visible == true)
            {
                if (RadCoacheeCIM.Text == "")
                {
                    stat = false;
                    string ModalLabel = "Please select enter cim.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            return stat;
        }
        protected void RadSearchBlackoutYesv2_Click(object sender, EventArgs e)
        {
            HiddenSearchBlackout.Value = "1";
            RadSearchBlackout.Enabled = false;
            RadSearchBlackout.ForeColor = Color.Black;
            RadCMTSaveSubmit.Text = "Save";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv2(); });", true);
        }
        protected void RadSearchBlackoutNov2_Click(object sender, EventArgs e)
        {
            //Window3.VisibleOnPageLoad = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv2(); });", true);
        }
        protected void RadCMTShareYes_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { CyborgCMT(); });", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv3(); });", true);
        }
        protected void RadCMTShareNo_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv3(); });", true);
        }

        protected void RadGrid2_OnItemDatabound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["ReviewID"].Controls[0];
                string val1 = hLink.Text;

                string hlinknavigate = "";
                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;


                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");

                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                }

                else if (Convert.ToInt32(val2) == 4)
                {
                    hLink.NavigateUrl = "AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2; ;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");//"~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");
                }
            }
        }

        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void UpdateFormType(int ReviewID, int FormType)
        {
            DataAccess ws = new DataAccess();
            ws.UpdateFormType(ReviewID, FormType);
        }
        protected void RadGrid9_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid9.DataSource = lstOD2;
        }
        protected void RadGrid9_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllDataOD();
                lstOD2.Insert(0, new OD2() { ODID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid9.Rebind();
            }

            if (e.CommandName == "Delete")
            {
                Guid itemID = (Guid)(((GridDataItem)e.Item).GetDataKeyValue("ODID"));
                foreach (var n in lstOD2.Where(p => p.ODID == itemID).ToArray()) lstOD2.Remove(n);
                RadGrid9.Rebind();


                string ModalLabel = "The selected performance result has been removed";
                string ModalHeader = "Removing performance result";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected void saveAllDataOD()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid9.MasterTableView.Items)
            {
                Guid ODID = new Guid(item.GetDataKeyValue("ODID").ToString());
                OD2 emp = lstOD2.Where(i => i.ODID == ODID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPIOD") as RadComboBox).SelectedItem.Value); //.SelectedValue);
                emp.Name = (item.FindControl("RadKPIOD") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Change = (item.FindControl("RadChange") as RadNumericTextBox).Text;
                emp.DataViewValue = (item.FindControl("RadDataView") as RadComboBox).SelectedValue;
                emp.Behavior = (item.FindControl("RadBehavior") as RadTextBox).Text;
                emp.RootCause = (item.FindControl("RadRootCause") as RadTextBox).Text;

                emp.Driver = null;
                RadComboBox RadDriver = item.FindControl("RadDriver") as RadComboBox;
                foreach (RadComboBoxItem checkeditem in RadDriver.CheckedItems)
                {
                    if (emp.Driver == null)
                    {
                        emp.Driver = checkeditem.Value;
                    }
                    else
                    {
                        emp.Driver = emp.Driver + "," + checkeditem.Value + ",";
                    }
                }
                emp.Driver = emp.Driver.TrimEnd(',');
            }
        }
        protected void RadGrid9_OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem insertitem = (GridDataItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPIOD");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }

                        if (RadCoacheeName.SelectedIndex != -1 && combo.SelectedIndex != -1)
                        {

                            DataSet dsFirst = null;
                            DataAccess wsFirst = new DataAccess();
                            dsFirst = wsFirst.GetLastDate(Convert.ToInt32(RadCoacheeName.SelectedValue), combo.SelectedItem.Text);

                            Label LabelDate = (Label)insertitem.FindControl("LabelAsOf");
                            if (dsFirst.Tables[0].Rows.Count > 0)
                            {
                                LabelDate.Text = "* Data Available as of " + dsFirst.Tables[0].Rows[0]["AsOfDate"].ToString();
                            }
                            else
                            {
                                LabelDate.Text = "No Data Available";
                            }
                        }
                    }
                }
            }
            else
            {
                if (e.Item is GridDataItem)
                {
                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridDataItem insertitem = (GridDataItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPIOD");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }

                            if (RadCoacheeName.SelectedIndex != -1 && combo.SelectedIndex != -1)
                            {

                                DataSet dsFirst = null;
                                DataAccess wsFirst = new DataAccess();
                                dsFirst = wsFirst.GetLastDate(Convert.ToInt32(RadCoacheeName.SelectedValue), combo.SelectedItem.Text);

                                Label LabelDate = (Label)insertitem.FindControl("LabelAsOf");
                                if (dsFirst.Tables[0].Rows.Count > 0)
                                {
                                    LabelDate.Text = "* Data Available as of " + dsFirst.Tables[0].Rows[0]["AsOfDate"].ToString();
                                }
                                else
                                {
                                    LabelDate.Text = "No Data Available";
                                }
                            }
                        }
                    }
                }
            }

        }
        protected void RadGrid9_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPIOD");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    if (a != "0")
                    {
                        selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                        combo.Items.Add(selectedItem);
                        selectedItem.DataBind();
                        Session["KPI"] = selectedItem.Value;
                        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPIOD");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIList();
                        comboBrand.DataSource = ds;
                        comboBrand.DataBind();
                        comboBrand.SelectedValue = selectedItem.Value;
                    }

                    RadComboBox combodv = (RadComboBox)edititem.FindControl("RadDataView");
                    RadComboBoxItem selectedItemdv = new RadComboBoxItem();
                    string adv = Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "DataViewValue")) ? "" : (String)DataBinder.Eval(e.Item.DataItem, "DataViewValue");
                    if (adv != null)
                    {
                        selectedItemdv.Value = (string)DataBinder.Eval(e.Item.DataItem, "DataViewValue").ToString();
                        combodv.Items.Add(selectedItemdv);
                        selectedItemdv.DataBind();
                        Session["DataViewValue"] = selectedItemdv.Value;
                        RadComboBox combodvBrand = (RadComboBox)edititem.FindControl("RadDataView");
                        combodvBrand.SelectedValue = selectedItemdv.Value;
                    }

                    RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                    RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                    string dr = Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "Driver")) ? "" : (String)DataBinder.Eval(e.Item.DataItem, "Driver");
                    if (dr != null)
                    {
                        //selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                        //combodriver.Items.Add(selectedItemdriver);
                        //selectedItemdriver.DataBind();
                        //Session["Driver"] = selectedItemdriver.Value;
                        RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                        DataSet dsdriver = new DataSet();
                        DataAccess wsdriver = new DataAccess();
                        dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                        comboBranddriver.DataSource = dsdriver;
                        comboBranddriver.DataTextField = "Description";
                        comboBranddriver.DataValueField = "Driver_ID";
                        comboBranddriver.DataBind();

                        string s = dr;
                        string[] words = s.Split(',');
                        foreach (string word in words)
                        {
                            foreach (RadComboBoxItem checkeditem in comboBranddriver.Items)
                            {
                                if (checkeditem.Value == word)
                                {
                                    checkeditem.Checked = true;
                                    break;
                                }
                            }
                        }
                    }

                }
            }

        }
        protected void RadGrid9_PreRender(object sender, EventArgs e)
        {

            if (lstOD2.Count == 0)
            {
                if (!IsPostBack)
                {
                    GridCommandItem commandItem = (GridCommandItem)RadGrid9.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    commandItem.FireCommandEvent("InitInsert", null);

                }
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid9.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid9.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string change = (dataItem.FindControl("RadChange") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPIOD") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string dataview = (dataItem.FindControl("RadDataView") as RadComboBox).SelectedValue;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    string behavior = (dataItem.FindControl("RadBehavior") as RadTextBox).Text;
                    string rootcause = (dataItem.FindControl("RadRootCause") as RadTextBox).Text;

                    if (current != "" && previous != "" && change != "" && behavior != "" && rootcause != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            if (!IsPostBack)
                            {
                                GridCommandItem commandItem = (GridCommandItem)RadGrid9.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                                commandItem.FireCommandEvent("InitInsert", null);

                            }
                        }

                    }
                }


            }

        }
        public void RadKPIOD_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");
                RadComboBox RadDataView = (RadComboBox)item.FindControl("RadDataView");

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();

                RadNumericTextBox RadCurrent = (RadNumericTextBox)item.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)item.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)item.FindControl("RadChange");

                if (RadDataView.SelectedIndex != -1)
                {

                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetPerformanceDetails(Convert.ToInt32(RadCoacheeName.SelectedValue), combo.SelectedItem.Text, RadDataView.SelectedItem.Text, Convert.ToInt32(RadAccount.SelectedValue));
                    RadTarget.Text = ds.Tables[0].Rows[0]["Target"].ToString();
                    RadCurrent.Text = ds.Tables[0].Rows[0]["Current"].ToString();
                    RadPrevious.Text = ds.Tables[0].Rows[0]["Previous"].ToString();
                    RadChange.Text = ds.Tables[0].Rows[0]["Change"].ToString();
                    RadTarget.Enabled = false;
                    RadCurrent.Enabled = false;
                    RadPrevious.Enabled = false;
                    RadChange.Enabled = false;
                }

                if (RadCoacheeName.SelectedIndex != -1 && combo.SelectedIndex != -1)
                {

                    DataSet dsFirst = null;
                    DataAccess wsFirst = new DataAccess();
                    dsFirst = wsFirst.GetLastDate(Convert.ToInt32(RadCoacheeName.SelectedValue), combo.SelectedItem.Text);

                    Label LabelDate = (Label)item.FindControl("LabelAsOf");
                    if (dsFirst.Tables[0].Rows.Count > 0)
                    {
                        LabelDate.Text = "* Data Available as of " + dsFirst.Tables[0].Rows[0]["AsOfDate"].ToString();
                    }
                    else
                    {
                        LabelDate.Text = "No Data Available";
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPIOD_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void RadDataViewOD_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {


                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadKPI = (RadComboBox)item.FindControl("RadKPIOD");
                RadNumericTextBox RadCurrent = (RadNumericTextBox)item.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)item.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)item.FindControl("RadChange");
                RadComboBox RadDataView = (RadComboBox)item.FindControl("RadDataView");

                if (RadCoacheeName.SelectedIndex != -1 && combo.SelectedIndex != -1 && RadKPI.SelectedIndex != -1)
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetPerformanceDetails(Convert.ToInt32(RadCoacheeName.SelectedValue), RadKPI.SelectedItem.Text, combo.SelectedItem.Text, Convert.ToInt32(RadAccount.SelectedValue));
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        RadTarget.Text = ds.Tables[0].Rows[0]["Target"].ToString();
                        RadCurrent.Text = ds.Tables[0].Rows[0]["Current"].ToString();
                        RadPrevious.Text = ds.Tables[0].Rows[0]["Previous"].ToString();
                        RadChange.Text = ds.Tables[0].Rows[0]["Change"].ToString();
                    }
                    else
                    {
                        RadTarget.Text = "";
                        RadCurrent.Text = "";
                        RadPrevious.Text = "";
                        RadChange.Text = "";
                    }
                    RadTarget.Enabled = false;
                    RadCurrent.Enabled = false;
                    RadPrevious.Enabled = false;
                    RadChange.Enabled = false;
                }
                else if (RadCoacheeName.SelectedIndex == -1 && Request.QueryString["CIM"] == null)
                {
                    RadDataView.SelectedIndex = -1;
                    RadDataView.ClearSelection();
                    RadDataView.Text = string.Empty;
                    string ModalLabel = "Please select coachee.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
                else if (combo.SelectedIndex == -1)
                {
                    RadDataView.SelectedIndex = -1;
                    RadDataView.ClearSelection();
                    RadDataView.Text = string.Empty;
                    string ModalLabel = "No KPI Available for this account.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadDataViewOD_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }

        public void LoadCommitment()
        {
            string Account = Session["Account"].ToString();

            if (Account != "Default" && Account != "TalkTalk")
            {
                if (CheckBox1.Visible == true && CheckBox1.Checked == true)
                {
                    foreach (GridDataItem itm in RadGrid11.Items)
                    {
                        GridDataItem dataItem = itm as GridDataItem;
                        string id = (dataItem.FindControl("LabelCT") as Label).Text;
                        Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                        Test2.ReviewIDSelected = Convert.ToInt32(id);
                        PreviousCommitment.Controls.Add(Test2);
                    }
                }
            }

        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            if (lstOD2.Count == 0)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid9.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid9.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid9.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string change = (dataItem.FindControl("RadChange") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPIOD") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    string behavior = (dataItem.FindControl("RadBehavior") as RadTextBox).Text;
                    string rootcause = (dataItem.FindControl("RadRootCause") as RadTextBox).Text;

                    if (current != "" && previous != "" && change != "" && behavior != "" && rootcause != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            GridCommandItem commandItem = (GridCommandItem)RadGrid9.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            commandItem.FireCommandEvent("InitInsert", null);
                        }
                    }
                    else
                    {
                        string ModalLabel = "Please do not leave any field blank in Performance Results";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }
        }

        private void InsertReviewKPIOD()
        {
            try
            {
                string Account = Session["Account"].ToString();

                int ReviewID;
                RadComboBox KPI, DataView, Driver;
                RadTextBox Target, Behavior, RootCause;
                RadNumericTextBox Current, Previous, Change;


                foreach (GridDataItem itm in RadGrid9.Items)
                {
                    ReviewID = Convert.ToInt32(RadReviewID.Text);
                    KPI = (RadComboBox)itm.FindControl("RadKPIOD");
                    Driver = (RadComboBox)itm.FindControl("RadDriver");
                    Target = (RadTextBox)itm.FindControl("RadTarget");
                    DataView = (RadComboBox)itm.FindControl("RadDataView");
                    Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                    Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");
                    Change = (RadNumericTextBox)itm.FindControl("RadChange");
                    Behavior = (RadTextBox)itm.FindControl("RadBehavior");
                    RootCause = (RadTextBox)itm.FindControl("RadRootCause");


                    if (Current.Text != "" && Previous.Text != "" && Change.Text != "" && Behavior.Text != "" && RootCause.Text != "")
                    {
                        string DriverID = null;
                        string DriverNames = null;
                        foreach (RadComboBoxItem checkeditem in Driver.CheckedItems)
                        {
                            if (DriverID == null)
                            {
                                DriverID = checkeditem.Value + ",";
                                DriverNames = checkeditem.Text + ",";
                            }
                            else
                            {
                                DriverID = DriverID + "" + checkeditem.Value + ",";
                                DriverNames = DriverNames + "" + checkeditem.Text + ",";
                                DriverID.TrimEnd(',');
                                DriverNames.TrimEnd(',');
                            }
                        }
                        DriverID = DriverID.TrimEnd(',');
                        DriverNames = DriverNames.TrimEnd(',');


                        DataAccess ws = new DataAccess();
                        ws.InsertReviewKPIODv2(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, 0, Change.Text, Behavior.Text, RootCause.Text, DriverID, DriverNames, DataView.SelectedValue.ToString());

                    }


                }

            }
            catch (Exception ex)
            {
                string ModalLabel = "InsertReviewKPI " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }

        public void LoadKPIReviewOD(int ReviewID)
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);

                if (Account == "Default")
                {
                    RadGridPRR.DataSource = ds;
                    RadGridPRR.DataBind();
                }
                else if (Account == "TalkTalk")
                {
                    RadGrid5.DataSource = ds;
                    RadGrid5.DataBind();
                }
                else
                {
                    DataSet ds2 = null;
                    DataAccess ws2 = new DataAccess();
                    ds2 = ws2.GetKPIReviewODv2(ReviewID);

                    RadGrid10.DataSource = ds2;
                    RadGrid10.DataBind();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }


        #region SignOFF

        protected void btnRequestLogin_Click(object sender, EventArgs e)
        {
            //your client id  
            //string clientid = "321886997770-j3fbsi2fof2u5p6f3hgq203ddcpc3u3l.apps.googleusercontent.com";
            string clientid = "462887256364-42m4lgl7cpt0t5dthdthj2hoq1ukhrt9.apps.googleusercontent.com";

            string redirection_url;
            if (Request["CIM"] != null)
            {
                Session["CIMMyTeam"] = Request["CIM"].ToString();

                redirection_url = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AddReview.aspx";
            }
            else
            {
                redirection_url = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AddReview.aspx";
            }
            string url = "https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/analytics.readonly+https://www.googleapis.com/auth/userinfo.email&prompt=select_account+consent&access_type=online&include_granted_scopes=true&redirect_uri=" + redirection_url + "&response_type=code&client_id=" + clientid + "";
            Response.Redirect(url);

        }
        public class Tokenclass
        {
            public string access_token
            {
                get;
                set;
            }
            public string token_type
            {
                get;
                set;
            }
            public int expires_in
            {
                get;
                set;
            }
            public string refresh_token
            {
                get;
                set;
            }
        }
        public class Userclass
        {
            public string id
            {
                get;
                set;
            }
            public string email
            {
                get;
                set;
            }
            public string name
            {
                get;
                set;
            }
            public string given_name
            {
                get;
                set;
            }
            public string family_name
            {
                get;
                set;
            }
            public string link
            {
                get;
                set;
            }
            public string picture
            {
                get;
                set;
            }
            public string gender
            {
                get;
                set;
            }
            public string locale
            {
                get;
                set;
            }
            public string hd
            {
                get;
                set;
            }
        }


        public void GetToken(string code, string CIM)
        {


            //your client id  
            //string clientid = "321886997770-j3fbsi2fof2u5p6f3hgq203ddcpc3u3l.apps.googleusercontent.com";
            string clientid = "462887256364-42m4lgl7cpt0t5dthdthj2hoq1ukhrt9.apps.googleusercontent.com";
            //your client secret  
            //string clientsecret = "HuomqvoDqzYvvHXlT4CIpa0D";
            string clientsecret = "E_-tgOVNxHK0nHcevPjKn4Xp";
            string redirection_url = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AddReview.aspx";
            string url = "https://accounts.google.com/o/oauth2/token";

            if (CIM != null) //Add Review Thru My Team
            {
                redirection_url = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/AddReview.aspx";
            }

            string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(poststring);
            Stream outputstream = null;
            try
            {
                request.ContentLength = bytes.Length;
                outputstream = request.GetRequestStream();
                outputstream.Write(bytes, 0, bytes.Length);
            }
            catch { }
            var response = (HttpWebResponse)request.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream());
            string responseFromServer = streamReader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
            GetuserProfile(obj.access_token, CIM);
        }
        public void GetuserProfile(string accesstoken, string CIM)
        {

            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accesstoken + "";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Userclass userinfo = js.Deserialize<Userclass>(responseFromServer);
            imgprofile.ImageUrl = userinfo.picture;
            lblemail.Text = userinfo.email;
            lblname.Text = userinfo.name;
            lblhdDomain.Text = userinfo.hd;
            if (lblhdDomain.Text != null && lblhdDomain.Text == "transcom.com")
            {
                DataSet ds2 = DataHelper.GetUserInfoForSSO(Convert.ToString(lblemail.Text));

                if (ds2.Tables[0].Rows.Count > 0)
                {

                    txtReceiverID.Text = ds2.Tables[0].Rows[0]["CimNo"].ToString();
                    string a = (HttpContext.Current.User.Identity.Name.Split('|')[0]);


                    DataSet dsInfo = null;
                    DataAccess wsInfo = new DataAccess();
                    string cim = HttpContext.Current.User.Identity.Name.Split('|')[3].ToString();
                    dsInfo = wsInfo.GetSubordinatesSSOv2(Convert.ToInt32(cim), Convert.ToInt32(ds2.Tables[0].Rows[0]["CimNo"].ToString()));

                    if (dsInfo.Tables[0].Rows.Count > 0)
                    {
                        LoadNeededDetails();

                        string AccountID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["AccountID"].ToString() : "";
                        string Account = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["account"].ToString() : "";

                        RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Account, AccountID));
                        RadAccount.SelectedValue = AccountID;
                        RadAccount.Enabled = false;

                        string SiteID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["CompanySiteID"].ToString() : "";
                        string SiteName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["companysite"].ToString() : "";
                        RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(SiteName, SiteID));
                        RadSite.SelectedValue = SiteID;
                        RadSite.Enabled = false;

                        RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Account, AccountID));
                        RadCampaign.SelectedValue = AccountID;
                        RadCampaign.Enabled = false;

                        string Division = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                        string Division2 = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                        RadDivision.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Division, Division2));
                        RadDivision.SelectedValue = Division2;
                        RadDivision.Enabled = false;

                        string LOBID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOBID"].ToString() : "";
                        string LOBName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOB"].ToString() : "";
                        RadLOB.Items.Add(new Telerik.Web.UI.RadComboBoxItem(LOBName, LOBID));
                        RadLOB.SelectedValue = LOBID;
                        RadLOB.Enabled = false;

                        string FullName = dsInfo.Tables[0].Rows[0]["Name"].ToString();
                        RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString()));
                        RadCoacheeName.SelectedValue = dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString();
                        RadCoacheeName.Enabled = false;
                        SignOut.Attributes["style"] = "";
                        btngoogle.Visible = false;
                    }
                    else
                    {
                        SignOut.Attributes["style"] = "display:none";
                        btngoogle.Visible = true;

                        string ModalLabel = "Logged in email is not owned by the any of your subordinate.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }


                    if (!String.IsNullOrEmpty(CIM))
                    {
                        if (CIM != ds2.Tables[0].Rows[0]["CimNo"].ToString())
                        {
                            LoadNeededDetails();
                            SignOut.Attributes["style"] = "";
                            btngoogle.Visible = true;
                            string ModalLabel = "Logged in email is not owned by the selected coachee.";
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        }
                    }
                }
                else
                {
                    LoadNeededDetails();
                    SignOut.Attributes["style"] = "display:none";
                    btngoogle.Visible = true;
                    string ModalLabel = "Logged in email is not registered.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            else
            {
                btngoogle.Visible = true;
                SignOut.Attributes["style"] = "display:none";
                FormsAuthentication.SignOut();
                Session.Abandon();
                string ModalLabel = "Your domain is not allowed.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                "window.location='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/Default.aspx';", true);
            }
        }

        public void LoadNeededDetails()
        {
            RadSupervisor.Items.Clear();

            DataSet dsInfo = null;
            DataAccess wsInfo = new DataAccess();
            string cim = HttpContext.Current.User.Identity.Name.Split('|')[3];
            dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(cim));
            if (dsInfo.Tables[0].Rows.Count > 0)
            {
                string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                string FullName = FirstName + " " + LastName;
                RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), cim));
                RadSupervisor.SelectedValue = cim;
                RadSupervisor.Enabled = false;
            }
        }
        #endregion



        public void GetAllSites()
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetAllSites();

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CompanySiteID"], col2 = dRow["CompanySite"] }).Distinct();

                RadSite.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "Site " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }


        public void GetAllCampaigns()
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetAllCampaigns();

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CampaignID"], col2 = dRow["Campaign"] }).Distinct();

                RadCampaign.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "Campaign " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        public void GetAllDivisions()
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetAllDivs();

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["Department"], col2 = dRow["Department"] }).Distinct();

                RadDivision.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadDivision.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "Division " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }


        public void GetAllLOB()
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetAllLOB();

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["LOBID"], col2 = dRow["LOB"] }).Distinct();

                RadLOB.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadLOB.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LOB " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }


        protected void GetSubordinates(int CimNumber)
        {
            try
            {

                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates2(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSubordinates " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }



        protected void RadSaveBtn_Click(object sender, EventArgs e)
        {


            try
            {
                RadSaveBtn.Enabled = false;
                string Account = Session["Account"].ToString();
                string emailAddressCCM, emailCoachee;
                if (ValidatePage() == true)
                {
                    if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                    {
                        if (Convert.ToInt32(RadReviewID.Text) == 0)
                        {
                            if (Request["CIM"] != null)
                            {
                                DataSet dsInfo = null;
                                DataAccess wsInfo = new DataAccess();
                                string cim = HttpContext.Current.User.Identity.Name.Split('|')[3].ToString();
                                dsInfo = wsInfo.GetSubordinatesSSOv2(Convert.ToInt32(cim), Convert.ToInt32(Request["CIM"].ToString()));

                                if (dsInfo.Tables[0].Rows.Count > 0)
                                {
                                    LoadNeededDetails();

                                    string AccountID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["AccountID"].ToString() : "";
                                    string Accountx = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["account"].ToString() : "";

                                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Accountx, AccountID));
                                    RadAccount.SelectedValue = AccountID;
                                    RadAccount.Enabled = false;

                                    string SiteID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["CompanySiteID"].ToString() : "";
                                    string SiteName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["companysite"].ToString() : "";
                                    RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(SiteName, SiteID));
                                    RadSite.SelectedValue = SiteID;
                                    RadSite.Enabled = false;

                                    RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Accountx, AccountID));
                                    RadCampaign.SelectedValue = AccountID;
                                    RadCampaign.Enabled = false;

                                    string Division = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                                    string Division2 = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["Division"].ToString() : "";
                                    RadDivision.Items.Add(new Telerik.Web.UI.RadComboBoxItem(Division, Division2));
                                    RadDivision.SelectedValue = Division2;
                                    RadDivision.Enabled = false;

                                    string LOBID = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOBID"].ToString() : "";
                                    string LOBName = dsInfo.Tables[0].Rows.Count > 0 ? dsInfo.Tables[0].Rows[0]["LOB"].ToString() : "";
                                    RadLOB.Items.Add(new Telerik.Web.UI.RadComboBoxItem(LOBName, LOBID));
                                    RadLOB.SelectedValue = LOBID;
                                    RadLOB.Enabled = false;

                                    string FullName = dsInfo.Tables[0].Rows[0]["Name"].ToString();
                                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString()));
                                    RadCoacheeName.SelectedValue = dsInfo.Tables[0].Rows[0]["CIMNumber"].ToString();
                                    RadCoacheeName.Enabled = false;
                                    SignOut.Attributes["style"] = "";
                                    btngoogle.Visible = false;
                                }
                            }


                            if (Account == "Default")
                            {
                                DataSet ds1 = null;
                                DataAccess ws1 = new DataAccess();
                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
                                emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                emailAddressCCM = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                                int ReviewID;
                                DataAccess ws = new DataAccess();
                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 0, CIMNumber, 1, 2);
                                RadReviewID.Text = Convert.ToString(ReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

                                InsertReviewKPI();
                                InsertReviewHistory(1);
                                InsertReviewHistory(2);
                                LoadKPIReview(ReviewID);
                                UpdateFormType(ReviewID, 1);
                                LoadDocumentationsReview();
                                CIM = RadCoacheeCIM.Text;
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                //SendEmail(1, ReviewID, emailCoachee);
                                //SendEmail(2, ReviewID, emailAddressCCM);
                                string ModalLabel = "This review has been saved";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadCoacheeCIM.Visible = false;

                                RadSaveBtn.Visible = false;

                            }
                            else if (Account == "TalkTalk")
                            {
                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescriptionNexidia.Text != null)
                                {
                                    Description = RadDescriptionNexidia.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengthsNexidia.Text != null)
                                {
                                    Strengths = RadStrengthsNexidia.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunitiesNexidia.Text != null)
                                {
                                    Opportunity = RadOpportunitiesNexidia.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CheckBoxList1.SelectedValue) ? 0 : int.Parse(CheckBoxList1.SelectedValue);

                                string PositiveBehaviour;
                                if (RadPositiveBehaviour.Text != null)
                                {
                                    PositiveBehaviour = RadPositiveBehaviour.Text;
                                }
                                else
                                {
                                    PositiveBehaviour = DBNull.Value.ToString();
                                }

                                string OpportunityBehaviour;
                                if (RadOpportunityBehaviour.Text != null)
                                {
                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
                                }
                                else
                                {
                                    OpportunityBehaviour = DBNull.Value.ToString();
                                }

                                string BeginBehaviourComments;
                                if (RadBeginBehaviourComments.Text != null)
                                {
                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
                                }
                                else
                                {
                                    BeginBehaviourComments = DBNull.Value.ToString();
                                }

                                string BehaviourEffects;
                                if (RadBehaviourEffect.Text != null)
                                {
                                    BehaviourEffects = RadBehaviourEffect.Text;
                                }
                                else
                                {
                                    BehaviourEffects = DBNull.Value.ToString();
                                }

                                string RootCause;
                                if (RadRootCause.Text != null)
                                {
                                    RootCause = RadRootCause.Text;
                                }
                                else
                                {
                                    RootCause = DBNull.Value.ToString();
                                }

                                string ReviewResultsComments;
                                if (RadReviewResultsComments.Text != null)
                                {
                                    ReviewResultsComments = RadReviewResultsComments.Text;
                                }
                                else
                                {
                                    ReviewResultsComments = DBNull.Value.ToString();
                                }

                                string AddressOpportunities;
                                if (RadAddressOpportunities.Text != null)
                                {
                                    AddressOpportunities = RadAddressOpportunities.Text;
                                }
                                else
                                {
                                    AddressOpportunities = DBNull.Value.ToString();
                                }

                                string ActionPlanComments;
                                if (RadActionPlanComments.Text != null)
                                {
                                    ActionPlanComments = RadActionPlanComments.Text;
                                }
                                else
                                {
                                    ActionPlanComments = DBNull.Value.ToString();
                                }

                                string SmartGoals;
                                if (RadSmartGoals.Text != null)
                                {
                                    SmartGoals = RadSmartGoals.Text;
                                }
                                else
                                {
                                    SmartGoals = DBNull.Value.ToString();
                                }

                                string CreatePlanComments;
                                if (RadCreatePlanComments.Text != null)
                                {
                                    CreatePlanComments = RadCreatePlanComments.Text;
                                }
                                else
                                {
                                    CreatePlanComments = DBNull.Value.ToString();
                                }

                                string FollowThrough;
                                if (RadFollowThrough.Text != null)
                                {
                                    FollowThrough = RadFollowThrough.Text;
                                }
                                else
                                {
                                    FollowThrough = DBNull.Value.ToString();
                                }

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int NexidiaReviewID;
                                DataAccess ws = new DataAccess();
                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(NexidiaReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

                                InsertReviewKPI();
                                InsertReviewHistory(1);
                                InsertReviewHistory(2);
                                LoadKPIReview(NexidiaReviewID);
                                UpdateFormType(NexidiaReviewID, 2); ;
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                string ModalLabel = "This review has been saved";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                //RadCoacherSignOff.Visible = true;
                                RadCoacheeCIM.Visible = false;
                                RadSaveBtn.Visible = false;
                            }
                            else
                            {

                                

                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescriptionOD.Text != null)
                                {
                                    Description = RadDescriptionOD.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengthsOD.Text != null)
                                {
                                    Strengths = RadStrengthsOD.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunitiesOD.Text != null)
                                {
                                    Opportunity = RadOpportunitiesOD.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                string CoacherFeedback;
                                if (RadCoacherFeedback.Text != null)
                                {
                                    CoacherFeedback = RadCoacherFeedback.Text;
                                }
                                else
                                {
                                    CoacherFeedback = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CheckBoxList1.SelectedValue) ? 0 : int.Parse(CheckBoxList1.SelectedValue);
                                int CallID = string.IsNullOrEmpty(RadCallID.Text) || RadCallID.Text == "000" ? 0 : int.Parse(RadCallID.Text);

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int ODReviewID;
                                DataAccess ws = new DataAccess();
                                ODReviewID = ws.InsertReviewODv2(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID, Convert.ToInt32(RadSite.SelectedValue), RadDivision.Text, Convert.ToInt32(RadLOB.SelectedValue));
                                RadReviewID.Text = Convert.ToString(ODReviewID);
                                DataHelper.SetTicketStatus(7, Convert.ToInt32(ODReviewID)); // update status to coacheesigned off 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


                                InsertReviewKPIOD();
                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
                                InsertReviewHistory(1);
                                LoadKPIReviewOD(ODReviewID);
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                string ModalLabel = "This review has been saved";
                                string ModalHeader = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = false;
                                RadCoacheeCIM.Visible = false;

                                RadSaveBtn.Visible = false;

                            }
                        }
                        else
                        {
                            DataAccess ws = new DataAccess();
                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


                            LoadDocumentationsReview();
                            RadCoacheeSignOff.Visible = false;
                            string ModalLabel = "This review has been saved";
                            string ModalHeader = "Success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            DisableElements();
                            RadSSN.Visible = false;
                            RadSaveBtn.Visible = false;
                        }
                    }


                }

                
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                string ModalLabel = "RadCoacheeSignOff_Click " + myStringVariable;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

       


    }
}