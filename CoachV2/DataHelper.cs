﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using CoachV2.Entities; 
using System.Security.Cryptography;
using System.IO;
using System.Text;
using CoachV2.AppCode;
namespace CoachV2
{
    public class DataHelper
    {
        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "cn_CoachV2";


        public static DataSet GetAllKPIs()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"SELECT KPIid, [Name] KPIName FROM tbl_Coach_KPI";

                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet GetAllKPIs2()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"SELECT * FROM tbl_Coach_SchoolType";

                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet GetEmployeeInfo(string EmailAddress)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetEmployeeInfo";

                //                                SELECT  
                //	                                TOP 1 *,
                //	                                (SELECT b.First_Name + ' ' + b.Last_Name FROM SAP.dbo.tbl_SAP_Hld_Export b WHERE b.CIM_Number = a.Reports_To) ReportsTo
                //	
                //                                FROM SAP.dbo.tbl_SAP_Hld_Export a WITH(NOLOCK)
                //                                WHERE LOWER(a.Email) = @Email";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Email", EmailAddress);

                cmd.CommandType = CommandType.StoredProcedure;//.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetUserInfo(string EmailAddress)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserInfo1";
                //SELECT * FROM tbl_Coach_UserInfo WHERE LOWER(TranscomEmail) = @Email

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Email", EmailAddress);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetUserInfoViaCIMNo(string CIMNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                //                string query = @"SELECT  
                //	                                TOP 1 *,
                //                                         CASE 
                //                                        WHEN Country = 'GB' THEN
                //                                            'Great Britain'
                //                                        WHEN Country = 'PH' THEN
                //                                            'Philippines'
                //                                        WHEN Country = 'SE' THEN
                //                                            'Sweden'
                //                                        WHEN Country = 'CA' THEN
                //                                            'Canada'
                //                                        WHEN Country = 'US' THEN
                //                                            'United States'
                //                                        WHEN Country = 'SG' THEN
                //                                            'Singapore'
                //                                        ELSE
                //                                            'N/A'
                //                                    END CountryName,
                //	                                (SELECT b.First_Name + ' ' + b.Last_Name FROM SAP.dbo.tbl_SAP_Hld_Export b WHERE b.CIM_Number = a.Reports_To) ReportsTo
                //	                            ,(select  f.companysite from 
                //								SUSL3PSQLDB02.CIMEnterprise.dbo.tbl_Personnel_Cor_Employee E  
                //								JOIN SUSL3PSQLDB02.CIMEnterprise.dbo.tbl_Personnel_Cor_EmployeeCompany EC  
                //								ON E.EmployeeID = EC.EmployeeID  
                //								join susl3psqldb02.cimenterprise.dbo.tbl_Personnel_Lkp_CompanySite f  
                //								on ec.companysiteid = f.companysiteid
                //								where a.cim_number = e.cimnumber ) sitename
                //                                FROM SAP.dbo.tbl_SAP_Hld_Export a WITH(NOLOCK)
                //                                WHERE CIM_Number = @CIMNo";

                string query = @"pr_Coach_GetUserInfoViaCIMNo";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("CIMNo", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetUserInfoLocalViaCIMNo(string CIMNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserInfoLocalViaCIMNo";// @"SELECT * FROM tbl_Coach_UserInfo WHERE CIMNo = @CIMNo";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("CIMNo", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetEduc(int UserID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetEduc";
                //                                SELECT a.*, b.[Description] FROM tbl_Coach_UserEduc a
                //                                JOIN tbl_Coach_SchoolType b
                //                                ON a.SchoolTypeID = b.SchoolTypeID
                //                                WHERE a.UserInfoID = @UserInfoID AND a.Inactive <> 1";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("UserInfoID", UserID);

                cmd.CommandType = CommandType.StoredProcedure; // .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSkill(int UserID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSkill";
                //                                SELECT a.* FROM tbl_Coach_UserSkill a WHERE a.UserInfoID = @UserInfoID AND a.Inactive <> 1";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("UserInfoID", UserID);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetExp(int UserID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetExp";
                //                                SELECT a.* FROM tbl_Coach_UserExperience a WHERE a.UserInfoID = @UserInfoID AND a.Inactive <> 1";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("UserInfoID", UserID);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCert(int UserID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCert";
                //                SELECT a.* FROM tbl_Coach_UserCert a WHERE a.UserInfoID = @UserInfoID AND a.Inactive <> 1";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("UserInfoID", UserID);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSchoolTypes()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"SELECT * FROM tbl_Coach_SchoolType";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static void InsertUserInfo(string UserID, string CIMNo, string FirstName, string LastName, string MiddleName, string ReportingTo, int Gender, DateTime DateOfBirth, DateTime DateStartTranscom, string PrimaryLanguage, string MobileNo, string LandLineNo, string TranscomEmail, string PersonalEmail, string Role, string Site, string Region, string Country, string Department, string Client, string Campaign, string Photo, DateTime Created, DateTime Updated, bool Inactive)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_AddUserInfo";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@UserID", UserID);
                cmd.Parameters.Add("@CIMNo", CIMNo);
                cmd.Parameters.Add("@FirstName", FirstName);
                cmd.Parameters.Add("@LastName", LastName);
                cmd.Parameters.Add("@MiddleName", "");
                cmd.Parameters.Add("@ReportingTo", ReportingTo);
                cmd.Parameters.Add("@Gender", Gender);
                cmd.Parameters.Add("@DateOfBirth", DateOfBirth);
                cmd.Parameters.Add("@DateStartTranscom", DateStartTranscom);
                cmd.Parameters.Add("@PrimaryLanguage", PrimaryLanguage);
                cmd.Parameters.Add("@MobileNo", MobileNo);
                cmd.Parameters.Add("@LandLineNo", LandLineNo);
                cmd.Parameters.Add("@TranscomEmail", TranscomEmail);
                cmd.Parameters.Add("@PersonalEmail", PersonalEmail);
                cmd.Parameters.Add("@Role", Role);
                cmd.Parameters.Add("@Site", Site);
                cmd.Parameters.Add("@Region", Region);
                cmd.Parameters.Add("@Country", Country);
                cmd.Parameters.Add("@Department", Department);
                cmd.Parameters.Add("@Client", Client);
                cmd.Parameters.Add("@Campaign", Campaign);
                cmd.Parameters.Add("@Photo", Photo);
                cmd.Parameters.Add("@Created", Created);
                cmd.Parameters.Add("@Updated", DateTime.Now);
                cmd.Parameters.Add("@Inactive", false);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUserInfo(string UserID, string CIMNo, string FirstName, string LastName, string MiddleName, string ReportingTo, int Gender, DateTime DateOfBirth, DateTime DateStartTranscom, string PrimaryLanguage, string MobileNo, string LandLineNo, string TranscomEmail, string PersonalEmail, string Role, string Site, string Region, string Country, string Department, string Client, string Campaign, DateTime Updated, bool Inactive)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserInfo";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@UserID", UserID);
                cmd.Parameters.Add("@CIMNo", CIMNo);
                cmd.Parameters.Add("@FirstName", FirstName);
                cmd.Parameters.Add("@LastName", LastName);
                cmd.Parameters.Add("@MiddleName", "");
                cmd.Parameters.Add("@ReportingTo", ReportingTo);
                cmd.Parameters.Add("@Gender", Gender);
                cmd.Parameters.Add("@DateOfBirth", DateOfBirth);
                cmd.Parameters.Add("@DateStartTranscom", DateStartTranscom);
                cmd.Parameters.Add("@PrimaryLanguage", PrimaryLanguage);
                cmd.Parameters.Add("@MobileNo", MobileNo);
                cmd.Parameters.Add("@LandLineNo", LandLineNo);
                cmd.Parameters.Add("@TranscomEmail", TranscomEmail);
                cmd.Parameters.Add("@PersonalEmail", PersonalEmail);
                cmd.Parameters.Add("@Role", Role);
                cmd.Parameters.Add("@Site", Site);
                cmd.Parameters.Add("@Region", Region);
                cmd.Parameters.Add("@Country", Country);
                cmd.Parameters.Add("@Department", Department);
                cmd.Parameters.Add("@Client", Client);
                cmd.Parameters.Add("@Campaign", Campaign);
                cmd.Parameters.Add("@Updated", Updated);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertUserEduc(int UserInfoID, string School, string Course, int SchoolTypeID, DateTime StartDate, DateTime EndDate, string Honors, bool Graduated, DateTime Created, DateTime Updated, bool Inactive)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserEduc";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@UserInfoID", UserInfoID);
                cmd.Parameters.Add("@School", School);
                cmd.Parameters.Add("@Course", Course);
                cmd.Parameters.Add("@SchoolTypeID", SchoolTypeID);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@EndDate", EndDate);
                cmd.Parameters.Add("@Honors", Honors);
                cmd.Parameters.Add("@Graduated", Graduated);
                cmd.Parameters.Add("@Created", Created);
                cmd.Parameters.Add("@Updated", Updated);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUserEduc(int EducBgID, string School, string Course, int SchoolTypeID, DateTime StartDate, DateTime EndDate, string Honors, bool Graduated, bool Inactive)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserEduc";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@EducBgID", EducBgID);
                cmd.Parameters.Add("@SchoolName", School);
                cmd.Parameters.Add("@Course", Course);
                cmd.Parameters.Add("@SchoolTypeID", SchoolTypeID);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@EndDate", EndDate);
                cmd.Parameters.Add("@Honors", Honors);
                cmd.Parameters.Add("@Graduated", Graduated);
                cmd.Parameters.Add("@Updated", DateTime.Now);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertUserSkill(int UserInfoID, string Description, int Rating, DateTime Created, DateTime Updated, bool Inactive)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserSkill";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@UserInfoID", UserInfoID);
                cmd.Parameters.Add("@Description", Description);
                cmd.Parameters.Add("@Rating", Rating);
                cmd.Parameters.Add("@Created", Created);
                cmd.Parameters.Add("@Updated", Updated);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUserSkill(int SkillID, string Desc, int Rating)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserSkill";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@SkillID", SkillID);
                cmd.Parameters.Add("@Description", Desc);
                cmd.Parameters.Add("@Rating", Rating);
                cmd.Parameters.Add("@Updated", DateTime.Now);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertUserCert(int UserInfoID, string Description, DateTime DateObtained, string WhereObtained, DateTime Created, DateTime Updated, bool Inactive)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserCert";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@UserInfoID", UserInfoID);
                cmd.Parameters.Add("@Description", Description);
                cmd.Parameters.Add("@DateObtained", DateObtained);
                cmd.Parameters.Add("@WhereObtained", WhereObtained);
                cmd.Parameters.Add("@Created", Created);
                cmd.Parameters.Add("@Updated", Updated);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUserCert(int CertID, string Desc, DateTime WhenObtained, string WhereObtained)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserCert";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CertID", CertID);
                cmd.Parameters.Add("@Description", Desc);
                cmd.Parameters.Add("@DateObtained", WhenObtained);
                cmd.Parameters.Add("@WhereObtained", WhereObtained);
                cmd.Parameters.Add("@Updated", DateTime.Now);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertExp(int UserInfoID, string CompanyName, string Role, string Industry, DateTime StartDate, DateTime EndDate, string ReasonForLeaving, DateTime Created, DateTime Updated, bool Inactive)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserExp";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@UserInfoID", UserInfoID);
                cmd.Parameters.Add("@Company", CompanyName);
                cmd.Parameters.Add("@Role", Role);
                cmd.Parameters.Add("@Industry", Industry);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@EndDate", EndDate);
                cmd.Parameters.Add("@ReasonForLeaving", ReasonForLeaving);
                cmd.Parameters.Add("@Created", Created);
                cmd.Parameters.Add("@Updated", Updated);
                cmd.Parameters.Add("@Inactive", Inactive);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUserExp(int ExpID, string Company, string Role, string Industry, DateTime StartDate, DateTime EndDate, string ReasonForLeaving)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserExp";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ExpID", ExpID);
                cmd.Parameters.Add("@Company", Company);
                cmd.Parameters.Add("@Role", Role);
                cmd.Parameters.Add("@Industry", Industry);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@EndDate", EndDate);
                cmd.Parameters.Add("@ReasonForLeaving", ReasonForLeaving);
                cmd.Parameters.Add("@Updated", DateTime.Now);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteEduc(int EducBgID)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_DeleteEduc"; //UPDATE tbl_Coach_UserEduc SET Inactive = 1 WHERE EducBgID = @EducBgID
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@EducBgID", EducBgID);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteCert(int CertID)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_DeleteCert"; //UPDATE tbl_Coach_UserCert SET Inactive = 1 WHERE CertID = @CertID
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@CertID", CertID);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteSkill(int SkillID)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_DeleteSkill"; //UPDATE tbl_Coach_UserSkill SET Inactive = 1 WHERE SkillID = @SkillID
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@SkillID", SkillID);

                cmd.CommandType = CommandType.StoredProcedure;//Text;
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteExp(int ExpID)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_DeleteExp"; //UPDATE tbl_Coach_UserExperience SET Inactive = 1 WHERE ExpID = @ExpID
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@ExpID", ExpID);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertUserRole(int cim, int roleid, string createdby, DateTime createdon)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserRole";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@userid", cim);
                cmd.Parameters.Add("@roleid", roleid);
                cmd.Parameters.Add("@Createdby", createdby);
                cmd.Parameters.Add("@Createdon", createdon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataSet GetMyReviewsByCIM(string cim)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewsByCIM";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CIMNumber", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetMyReviewsByCompletedCIM(string cim)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewsCompletedByCIM";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CIMNumber", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }


        public static DataSet GetGraphValues(DateTime StartDate, DateTime EndDate, int KPIid, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyVOC_Charlie";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@FinishDate", EndDate);
                if (KPIid == 0)
                    cmd.Parameters.Add("@KPIID", DBNull.Value);
                else
                    cmd.Parameters.Add("@KPIID", KPIid);
                cmd.Parameters.Add("@Cimno", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetMyAcccountDetails(int CIMNumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCimNumberDetails2";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNum", CIMNumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }

            return ds;
        }

        public static DataSet GetAccountsForSearch(int AccountID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getAccountLookupByAccountID";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@AccountID", AccountID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }

            return ds;
        }

        public static DataSet GetSupervisorListing(int CIMNumber)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Get_SupervisorByCIM";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CIMNumber", CIMNumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }



        public static DataSet GetAllTopics(int SessionID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetAllTopics"; //SELECT * FROM tbl_Coach_Topic WHERE SessionId = @SessionID


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@SessionID", SessionID);

                cmd.CommandType = CommandType.StoredProcedure;//.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        // public static DataSet GetSearchCoachingTickets(int AccountID, int SupervisorID, int CoacheeID, int TopicID, DateTime ReviewDateFrom, DateTime ReviewDateTo, DateTime? FollowDateFrom, DateTime? FollowDateTo)
        // convert followup dates to optional parameters for prod0721 (francis.valera/07232018)
        public static DataSet GetSearchCoachingTickets(int AccountID, int SupervisorID, int CoacheeID, int TopicID, DateTime? ReviewDateFrom, DateTime? ReviewDateTo, DateTime? FollowDateFrom, DateTime? FollowDateTo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;
                string query = @"pr_Coach_SearchCoachingTickets";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@AccountID", AccountID);
                cmd.Parameters.Add("@SupervisorID", SupervisorID);
                cmd.Parameters.Add("@CoacheeID", CoacheeID);
                cmd.Parameters.Add("@TopicID", TopicID);
                cmd.Parameters.Add("@ReviewDateFrom", ReviewDateFrom);
                cmd.Parameters.Add("@ReviewDateTo", ReviewDateTo);
                cmd.Parameters.Add("@FollowUpDateFrom", FollowDateFrom);
                cmd.Parameters.Add("@FollowUpDateTo", FollowDateTo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetSearchQuery(string Q, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;
                string query = @"pr_Coach_SearchReviewViaWildCard";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@SearchString", Q);
                cmd.Parameters.Add("@MyCIM", CIMNo);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }


        public static DataSet GetAccountsLookupSearch(string Q)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getAccountlookupSearch";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@SearchQuery", Q);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetMyReviewsTL(int CIMNo, bool coacheesigned, bool coachersigned)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetMyReviewsTL";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);
                cmd.Parameters.Add("@CoacheeSigned", coachersigned);
                cmd.Parameters.Add("@CoacherSigned", coacheesigned);
                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }


        public static DataSet GetGraphValuesVOC(DateTime StartDate, DateTime EndDate, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_VOC_Charlie";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@FinishDate", EndDate);
                cmd.Parameters.Add("@Cimno", CIMNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        //Added by CPP 31.01.2019
        public static DataSet GetCoachingByTix(string CoachingTicket)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewById";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CoachingTicket", CoachingTicket);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        //Added by Charlie 31.01.2019
        public static DataSet UpdateSignOffRemoteCoaching(string CoachingTicket)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_UpdateSignOffsInRemoteCoaching";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ReviewID", CoachingTicket);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetGraphValuesFCR(DateTime StartDate, DateTime EndDate, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_FCR_Charlie";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@FinishDate", EndDate);
                cmd.Parameters.Add("@Cimno", CIMNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetGraphValuesAHT(DateTime StartDate, DateTime EndDate, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_AHT_Charlie";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@StartDate", StartDate);
                cmd.Parameters.Add("@FinishDate", EndDate);
                cmd.Parameters.Add("@Cimno", CIMNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetMyDistinction(int cim)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSubordinatesByReportsTo";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CIMNo", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetMyDistinction2(int cim)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSubordinatesByReportsTo2";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CIMNo", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetEmployeeRoleID(int cimno)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GeTRoleofUser";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimno", cimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        // use to test if user/OM has DIR view (francis.valera/07252018)
        public static Boolean UserHasSpecialAccess(int cimno)
        {
            Boolean _userhasdirview = false;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;
                string query = @"pr_Coach_SpecialAccess";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("CIMNo", cimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    _userhasdirview = true;
                }
            }
            return _userhasdirview;
        }

        public static string MyRoleInSAP(int CIMNo)
        {
            bool hasLevel1 = false;
            bool hasLevel2 = false;
            bool hasLevel3 = false;
            bool hasLevel4 = false;

            string myRole = "";

            DataSet dsDistinction = DataHelper.GetMyDistinction(CIMNo);

            List<int> fakeListing = new List<int>();

            foreach (DataTable table in dsDistinction.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    if (Convert.ToInt32(dr["Level"]) == 1)
                        fakeListing.Add(1);

                    if (Convert.ToInt32(dr["Level"]) == 2)
                        fakeListing.Add(2);

                    if (Convert.ToInt32(dr["Level"]) == 3)
                        fakeListing.Add(3);

                    if (Convert.ToInt32(dr["Level"]) == 4)
                        fakeListing.Add(4);

                }
            }

            IEnumerable<int> distinctRoles = fakeListing.Distinct();

            foreach (int roles in distinctRoles)
            {
                if (roles == 1)
                {
                    hasLevel1 = true;
                }

                if (roles == 2)
                {
                    hasLevel2 = true;
                }

                if (roles == 3)
                {
                    hasLevel3 = true;
                }

                if (roles == 4)
                {
                    hasLevel4 = true;
                }
            }

            if (hasLevel2 == true)
            {
                myRole = "TL";
            }

            if (hasLevel3 == true)
            {
                myRole = "OM";

                //// test if user has DIR role/view (francis.valera/07252018)
                //if (DataHelper.UserHasDirView(CIMNo))
                //{
                //    myRole = "Dir";
                //}
            }

            if (hasLevel4 == true)
            {
                myRole = "Dir";
            }
            return myRole;
        }

        public static string ReturnLabels(string Role)
        {
            string RoleLabel = "";
            if (Role == "TL")
            {
                RoleLabel = "Agents";
            }

            if (Role == "OM")
            {
                RoleLabel = "Team Leaders";
            }

            if (Role == "Dir")
            {
                RoleLabel = "Operations Managers";
            }

            if (Role.ToUpper().Contains("VP") == true)
            {
                RoleLabel = "Directors";
            }



            if (Role == "QA")
            {
                RoleLabel = "";
            }



            return RoleLabel;

        }

        public static DataSet Get_TL_ForTermination2(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTLForTermination";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetForFinalWarningAndTermination(int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetQAForFinalWarning";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }


        /*End Charlie Region*/



        #region admin
        //administrator datahelpers
        public static DataSet GetAllUserRoles(int cim)
        {
            DataSet ds_allroles = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserRolesforAdmin";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds_allroles);
            }
            return ds_allroles;

        }

        public static DataSet GetUserRolesAssigned(int cim, int roleid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserRolesofEmployee";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.Parameters.Add("roleid", roleid);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet RemoveUserRolesAssigned(int cim, int roleid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_RemoveUserRole";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("userid", cim);
                cmd.Parameters.Add("roleid", roleid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet get_adminusername(string gmail)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserInfo";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("gmail", gmail);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetSubordinates(int CIMNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;

                cmd = new SqlCommand("pr_Coach_GetSubordinates", cn);
                cmd.Parameters.AddWithValue("@CimNumber", CIMNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        // newly created for OM items in DDSupervisor (francis.valera/07162018)
        public static DataSet GetSubordinatesAndMe(int CIMNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;

                cmd = new SqlCommand("pr_Coach_GetSubordinatesAndMe", cn);
                cmd.Parameters.AddWithValue("@CimNumber", CIMNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetDriversLookup()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetDriversforKPIs";//"pr_Coach_GetDriversforKPI";
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSessionsLookup()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSessionLookup";
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAccountsLookup()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getActiveAccount"; // "pr_Coach_getAccountlookup";
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSuperAdminAccessAll(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_coach_getsuperalladminaccess";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSuperUserAccess(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_coachget_useraccess";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSupervisorLookup()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSupervisorList";


                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetEmployeeRoleFromSapDetails(string gmail)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserInfoforAdmin";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("email", gmail);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet UpdateUserToInactive(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_SetUserToInactive";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet UpdateUserToActive(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_SetUserToActive";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }


        public static DataSet getuserrolefromsap(int cimno)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GeTSAPRole";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimno", cimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }
        //administrator datahelpers

        //Charts datahelper
        //Charts datahelper
        #endregion admin

        #region //Charts agents
        public static DataSet GetDatesForChart1(string startdate, string enddate, int cim, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet getAHTFrequency_agent(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getVOCFrequency_agent(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_VOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getFCRFrequency_agent(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetDatesForChartVOC(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyVOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetDatesForChartVOC2(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyVOC2";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetDatesForCharttest()        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyTEST";


                cmd = new SqlCommand(query, cn);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehavior(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("kpiid", kpiid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIList(int acct)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_CoachGetKPIforuser";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("acct", acct);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCore(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetScore";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCorevsTarget(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTarget";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetFCRFrequency(string startdate, string enddate, int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyFCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetFCRScore(string startdate, string enddate, int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetFCRSCore";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAHTFrequency(string startdate, string enddate, int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyAHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAHTScore(string startdate, string enddate, int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetAHTSCore";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet getGlidepath_agentvoc(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTarget_VOC_Agent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);



                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_agentaht(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTarget_AHT_Agent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_agentaht_pivot(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_Agent_AHT_pivot";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_agentfcr(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTarget_FCR_Agent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        #endregion //charts

        #region charts tl
        public static DataSet GetFrequencyForTL(string startdate, string enddate, int cim, int subcimno, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_ForTL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("subcimno", subcimno);
                cmd.Parameters.Add("DataView", DataView);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorforTL(string startdate, string enddate, int cim, int kpiid, int subcimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_forTL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("subcimno", subcimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI_TL(string startdate, string enddate, int cim, int kpiid, int subcimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_forTL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("Subcimno", subcimno);



                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCoreforTL(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetScore_forTL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCorevsTargetforTl(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTargetforTL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_TL_AHT(string startdate, string enddate, int cim, int subcimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_TL_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("SubCimno", subcimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_TL_VOC(string startdate, string enddate, int cim, int subcimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_TL_VOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("SubCimno", subcimno);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_TL_FCR(string startdate, string enddate, int cim, int subcimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_TL_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("SubCimno", subcimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        #endregion charts tl
        #region charts OM / Director
        public static DataSet GetFrequencyForOM_Directors(string startdate, string enddate, int cim, int OMCimno, int TLCimNo, int AgentCimNo, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_Dir";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetFrequencyForOM_Directors1(string startdate, string enddate, int cim, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int Siteid, int Account, int LobId)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_DirCha";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("DataView", DataView);
                cmd.Parameters.Add("Siteid", Siteid);
                cmd.Parameters.Add("Accountid", Account);
                cmd.Parameters.Add("LOBID", LobId);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorforOMDIR(string startdate, string enddate, int cim, int kpiid, int OMCimno, int TLCimNo, int AgentCimNo)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_OMDIR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI_VP(string startdate, string enddate, int cim, int kpiid, int DirCimNo, int OMCimno, int TLCimNo, int AgentCimNo, int LobID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_VP";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("DirCimNo", DirCimNo);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("LobID", LobID);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI_OMDIR(string startdate, string enddate, int cim, int kpiid, int OMCimno, int TLCimNo, int AgentCimNo)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_OMDIR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCoreforOMDIR(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetScore_OMDIR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCorevsTargetforOMDIR(string startdate, string enddate, int cim, int kpiid, int OMCimNo, int TLCimNo, int AgentCimNo)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTargetforOMDIR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_DIR_AHT(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_DIR_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimNo);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);



                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_DIR_VOC(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = "pr_Coach_GetCoachingFrequency_DIR_VOC";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimNo);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_DIR_FCR(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_DIR_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimNo);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        #endregion charts OM/Director
        #region //QA chartwithout subordinates
        public static DataSet GetQAdepartment(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getAccountsforQA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetQAFrequency(string startdate, string enddate, int deptno)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_QADept";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("deptno", deptno);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorforQA(string startdate, string enddate, int deptno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_QADept";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("deptno", deptno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPIQA(string startdate, string enddate, int deptno, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_QA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("deptno", deptno);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCoreforQA(string startdate, string enddate, int deptno, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetScore_QA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("deptno", deptno);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPISCoreVsTargetQA(string startdate, string enddate, int deptno, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTargetforQA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("deptno", deptno);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        #endregion //QA chartwithout subordinates
        #region //agent dashboard datahelpers



        public static DataSet Get_AD_Recentescalation(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetRecentEscalated_AgentDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_AD_OverdueFollowUps(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueFollowUps_AgentDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_AD_OverdueSignedoff(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueSignOffs_AgentDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_Subordinates(int cim)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSubordinates";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("CimNumber", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewID(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Agent";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewIDKPI(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewKPILIST_Agent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewIDCoachingNotes(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Coachingnotes";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewIDPerfNotes(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Perfnotes";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_Agent_SSS(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Display_SSN";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimnumber", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion  //agent dashboard datahelpers
        #region Agent My reviews

        public static DataSet GetUserDetails(int cim)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCimNumberDetails";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("CimNum", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_Agent_MyReviews(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetAgentReview";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        // called from review dashboard, review list for non agents (francis.valera/07172018)
        public static DataSet Get_NonAgentReviewList(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewListForNonAgent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion Agent My reviews

        #region TLDashboard

        public static DataSet Get_TL_ForTermination(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_ForTermination_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_ForFinalWarning(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_ForFinalWarning_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_RecentlyEscalated(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetRecentEscalated_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_RecentlyReviewed(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetRecentReviewed_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_OverdueFollowups(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueFollowUps_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_OverdueSignOffs(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueSignOffs_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_WeeklyReview(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetWeeklyReview_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_WRecentlyAuditedbyOM(int cim, int CimMngr)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_RecentlyAuditedReviewsbyOM_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.Parameters.Add("CimMngr", CimMngr);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TL_RecentlyAssigned(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_RecentlyAssignedReviews_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetReviewsOpenForMe(int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewsOpenForMeTL";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        #endregion TLDashboard

        #region update coacheesignedoff

        public static DataSet Get_CoacheeSignOff(int Review)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Update_CoacheeSignoff";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewID", Review);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_CoacherSignOff(int Review)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Update_CoacherSignoff";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewID", Review);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion update coacheesignedoff

        #region omdashboard
        public static DataSet Get_RecentlyAuditedby(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_RecentlyAuditedReviews_OMDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_OverduefollowupsOMDash(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueFollowUps_OMDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_OverdueSignOffsOMDash(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueSignOffs_OMDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_OM_Weekly_Review(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetWeeklyReview_OMDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetFrequencyForOM(string startdate, string enddate, int cim, int TLCimno, int AgentCimno, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_ForOM";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);
                cmd.Parameters.Add("DataView", DataView);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorForOM(string startdate, string enddate, int cim, int kpiid, int TLCimno, int AgentCimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_forOM";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetBehaviorVSKPI_forOM(string startdate, string enddate, int cim, int kpiid, int TLCimno, int AgentCimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_forOM";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetKPISCorevsTargetForOM(string startdate, string enddate, int cim, int kpiid, int TLCimno, int AgentCimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTargetforOM";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_OM_AHT(string startdate, string enddate, int cim, int TLCimno, int AgentCimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_OM_VOC(string startdate, string enddate, int cim, int TLCimno, int AgentCimNo)           //coaching frequency
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_VOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_OM_FCR(string startdate, string enddate, int cim, int TLCimno, int AgentCimno)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion omdashboard



        #region triad coaching

        public static DataSet GetReviewsforTriad(int cim)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewToAudit";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCommentsforTriad(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadcomments";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetNotesforTriad(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadnotes";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetScoreforTriad(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadscore";//@"pr_Coach_gettriadscore";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetScoreforTriad1(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadscore1";//@"pr_Coach_gettriadscore";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet SetReviewforAudit(int Cimno, int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewForAudit";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", Cimno);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet SaveReviewforAudit(int Cimno, int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_SaveAuditedReview";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", Cimno);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static void InsertCommitmenthere(int ReviewID, string Goal_Text, string Reality_Text, string Options_Text, string WayForward_Text)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddCommitment";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("ReviewID", ReviewID);
                cmd.Parameters.Add("Goal_Text", Goal_Text);
                cmd.Parameters.Add("Reality_Text", Reality_Text);
                cmd.Parameters.Add("Options_Text", Options_Text);
                cmd.Parameters.Add("WayForward_Text", WayForward_Text);
                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertTriadNotes(int reviewid, string didwell, string dodifferent)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddNotes";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("ReviewID", reviewid);
                cmd.Parameters.Add("didwell", didwell);
                cmd.Parameters.Add("dodifferent", dodifferent);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        //        pr_Coach_addCoachingEvaluation
        public static void InsertCoachingEvaluation(int reviewid, int c_cmpnt_id, int scoring, string prepstep, string properstep, string poststep, int totalscore)
        {

            DataSet dset_userrole = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("c_cmpnt_id", c_cmpnt_id);
                cmd.Parameters.Add("scoring", scoring);
                cmd.Parameters.Add("prepstep", prepstep);
                cmd.Parameters.Add("properstep", properstep);
                cmd.Parameters.Add("poststep", poststep);
                cmd.Parameters.Add("totalscore", totalscore);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataSet GetDocumentation(int reviewid)  //coaching frequency
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetDocumentation";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_CoacherFeedback(string feedback, int Review)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_updatecoacherfeedback";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("feedback", feedback);
                cmd.Parameters.Add("ReviewID", Review);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TriadCoachingChart(string startdate, string enddate, int cim, int TLCimno, int AgentCimno, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getTriadCoachingFrequency";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);
                cmd.Parameters.Add("DataView", DataView);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TriadCoachingChart_HR_QA(string startdate, string enddate, int cim)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getTriadCoachingFrequency_HR_QA";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_TriadCoachingChartDir(string startdate, string enddate, int cim, int OMCimno, int TLCimno, int AgentCimno, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getTriadCoachingFrequencyDir";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);
                cmd.Parameters.Add("DataView", DataView);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        #endregion triad coaching

        #region addkpi
        public static DataSet GetKPIFromScorecard()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIFromScorecard";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIFromScorecardList()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIFromScorecardList";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIFromScorecardListv2()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIFromLookUpv2";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPILinkTo()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPILinkTo"; //SELECT [id] ,[linktype]  FROM [CoachV2].[dbo].[tbl_Coach_LinkType] where hidefromlist = 0


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;//.Text;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetValueto()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetValueto"; //SELECT  [valueid],[valuetoname] FROM [CoachV2].[dbo].[tbl_Coach_Valueto] where [hidefromlist] = 0


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIIDfromScorecardv2(int kpiid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIFromLookUp";
                //select top 1  id,REPLACE(KPI, '[Measures].', '') as 'KPItrim' from ScoreCard.dbo.tbl_ScoreCard_Lkp_KPI where id = @kpiid ";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAccountsforKPI()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getActiveAccount";


                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet InsertNewKPI(int kpiid, string kpi_name, string kpi_desc,
            int valuetype, int linkto, string createdby, int hidefromlist, int accountid, DateTime createdon, int driverid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_addNewKPI";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("kpi_name", kpi_name);
                cmd.Parameters.Add("kpi_desc", kpi_desc);
                cmd.Parameters.Add("valuetype", valuetype);
                cmd.Parameters.Add("linkto", linkto);
                cmd.Parameters.Add("createdby", createdby);
                cmd.Parameters.Add("hidefromlist", hidefromlist);
                cmd.Parameters.Add("accountid", accountid);
                cmd.Parameters.Add("createdon", createdon);
                cmd.Parameters.Add("driverid", driverid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }



        public static DataSet GetKPIIDAddedKPI(int kpiid, int accountid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckifKPIassigned";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("accountid", accountid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }



        public static DataSet GetKPIIList_admin()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_get_KPIList_Admin";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet UpdateKPIAdmin(int id, string kpi_name, string kpi_desc,
            int valuetype, int linkto, string createdby, int accountid, DateTime createdon)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_updateKPI";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("id", id);
                cmd.Parameters.Add("kpi_name", kpi_name);
                cmd.Parameters.Add("kpi_desc", kpi_desc);
                cmd.Parameters.Add("valuetype", valuetype);
                cmd.Parameters.Add("linkto", linkto);
                cmd.Parameters.Add("createdby", createdby);
                //cmd.Parameters.Add("hidefromlist", hidefromlist);
                cmd.Parameters.Add("accountid", accountid);
                cmd.Parameters.Add("createdon", createdon);
                //cmd.Parameters.Add("driverid", driverid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static void DeleteKPIAdmin(int id, string createdby, DateTime createdon)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_DeleteKPI";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("id", id);
                cmd.Parameters.Add("createdby", createdby);
                cmd.Parameters.Add("createdon", createdon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }


        #endregion addkpi


        #region hr dashboard
        public static DataSet GetHRForTermination(int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetHRForTermination";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetHRForFinalWarning(int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetHRForFinalWarning";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }


        public static DataSet Get_HR_OverdueFollowups(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueFollowUps_HRDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_HR_OverdueSignOffs(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueSignOffs_HRDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetFrequencyForHR(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_ForHR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorforHR(string startdate, string enddate, int cim, int kpiid)       //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_forHR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("kpiid", kpiid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI_HR(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_forHR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_HR_VOC(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_HR_VOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_HR_AHT(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_HR_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_HR_FCR(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_HR_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion dashboard

        #region qa dashboard
        public static DataSet Get_QA_WeeklyReview(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetWeeklyReview_QADash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_QA_WRecentlyAuditedbyOM(int cim, int CimMngr)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_RecentlyAuditedReviewsbyOM_TLDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.Parameters.Add("CimMngr", CimMngr);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetFrequencyForQA(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_ForQA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorforQA(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_forQA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("kpiid", kpiid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetBehaviorVsKPI_QA(string startdate, string enddate, int cim, int kpiid)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorVSKPI_forQA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIID", kpiid);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_QA_VOC(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_QA_VOC";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_QA_AHT(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_QA_AHT";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_QA_FCR(string startdate, string enddate, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_QA_FCR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_QA_OverdueFollowups(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueFollowUps_QADash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet Get_QA_OverdueSignOffs(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetOverdueSignOffs_QADash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion qa dashboard

        #region pdf remote coaching
        public static DataSet GetRemoteCoachFeedbackpdf(int ReviewId, int FeedbackRole)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetRemoteCoachFeedback";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewId", ReviewId);
                cmd.Parameters.Add("FeedbackRole", FeedbackRole);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetRemoteCoacherFeedbackpdf(int ReviewId, int FeedbackRole)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetRemoteCoachFeedback";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewId", ReviewId);
                cmd.Parameters.Add("FeedbackRole", FeedbackRole);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetRemoteCoachDocumentationpdf(int ReviewId, int FeedbackRole)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"GetUploadedDocuments_forPDF";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewId", ReviewId);
                cmd.Parameters.Add("Type", FeedbackRole);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetDocumentationpdf(int ReviewId, int FeedbackRole)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"GetUploadedDocuments_forPDF";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("ReviewId", ReviewId);
                cmd.Parameters.Add("Type", FeedbackRole);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }



        public static DataSet getsessionfocus(string id)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = "pr_Coach_getsessionfocusbyid";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("id", id);
                cmd.CommandType = CommandType.StoredProcedure; //Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingnotesOD(int CimNumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Get_CoachingNotesPerCim";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"CimNumber", CimNumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIPerfOD(int Id)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_get_review_kpi_OD";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"Id", Id);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetKPIPerfHistory(int CimNumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_Get_PerformanceResultsPerCim";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"CimNumber", CimNumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCimnumbersforMassReview(int masscoachingid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"GetCimnumbersforMassReview";

                //select * from tbl_Coach_ReviewTable where masscoachingid = @masscoachingid";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"masscoachingid", masscoachingid);
                cmd.CommandType = CommandType.StoredProcedure;//.Text;  
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetReviewIDforMassReview(int masscoachingid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Agent_masscoaching";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"reviewID", masscoachingid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetReviewcommitmentforpdf(int reviewid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadscore_forpdf";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCommentsforTriadpdf(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadcomments_forpdf";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetNotesforTriadforPDF(int reviewid)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_gettriadnotes";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewIDforCMT(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Agent_cmtdetails";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        #endregion

        public static DataSet GetAssignedReviews(int CIMNo, int AssignmentType)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetQARecentlyEscalated";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);
                cmd.Parameters.Add("@AssignmentType", AssignmentType);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        //06082017
        public static void InsertCoachingEvaluation1(int reviewid, int score1, string prepstep1, string properstep1, string poststep1)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation1";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("score1", score1);
                cmd.Parameters.Add("prepstep1", prepstep1);
                cmd.Parameters.Add("properstep1", properstep1);
                cmd.Parameters.Add("poststep1", poststep1);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertCoachingEvaluation2(int reviewid, int score2, string prepstep2, string properstep2, string poststep2)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation2";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("score2", score2);
                cmd.Parameters.Add("prepstep2", prepstep2);
                cmd.Parameters.Add("properstep2", properstep2);
                cmd.Parameters.Add("poststep2", poststep2);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertCoachingEvaluation3(int reviewid, int score3, string prepstep3, string properstep3, string poststep3)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation3";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("score3", score3);
                cmd.Parameters.Add("prepstep3", prepstep3);
                cmd.Parameters.Add("properstep3", properstep3);
                cmd.Parameters.Add("poststep3", poststep3);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertCoachingEvaluation4(int reviewid, int score4, string prepstep4, string properstep4, string poststep4)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation4";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("score4", score4);
                cmd.Parameters.Add("prepstep4", prepstep4);
                cmd.Parameters.Add("properstep4", properstep4);
                cmd.Parameters.Add("poststep4", poststep4);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static void InsertCoachingEvaluation5(int reviewid, int score5, string prepstep5, string properstep5, string poststep5, double totalscore, int hidefromlist)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_addCoachingEvaluation5";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("reviewid", reviewid);
                cmd.Parameters.Add("score5", score5);
                cmd.Parameters.Add("prepstep5", prepstep5);
                cmd.Parameters.Add("properstep5", properstep5);
                cmd.Parameters.Add("poststep5", poststep5);

                cmd.Parameters.Add("totalscore", totalscore);
                cmd.Parameters.Add("hidefromlist", hidefromlist);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }


        public static DataSet GetSearchedCoachingticket(int reviewid, int coacheeid)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_SearchforCoachTicket";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@reviewid", reviewid);
                cmd.Parameters.Add("@coacheeid", coacheeid);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetSearchQuery_forAgent(string Q, int CIMNo)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;
                string query = @"pr_Coach_SearchReviewViaWildCard_forAgent";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@SearchString", Q);
                cmd.Parameters.Add("@MyCIM", CIMNo);
                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }

        public static DataSet GetAssignedKPI(int kpiid, int accountid, int valuetype, int linkto)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckifKPIAdded";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("accountid", accountid);
                cmd.Parameters.Add("valuetype", valuetype);
                cmd.Parameters.Add("linkto", linkto);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAssignedKPIv2(int kpiid, int accountid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckifKPIAddedv2";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("accountid", accountid);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet CheckforSubs(int cim)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckforSubs";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Insertuserhierarchy(int cim, int deptid, int role_id, int hierarchy)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_AddUserHierarchy";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@cim", cim);
                cmd.Parameters.Add("@deptid", deptid);
                cmd.Parameters.Add("@role_id", role_id);
                cmd.Parameters.Add("@hierarchy", hierarchy);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            } return ds;
        }

        public static DataSet Edituserhierarchy(int cim, int deptid, int role_id, int hierarchy)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_EditUserHierarchy";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@cim", cim);
                cmd.Parameters.Add("@deptid", deptid);
                cmd.Parameters.Add("@role_id", role_id);
                cmd.Parameters.Add("@hierarchy", hierarchy);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            } return ds;
        }

        public static DataSet checkuserhierarchy(int cim)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;
                string query_add = @"pr_Coach_CheckUserHierarchy";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.Add("@cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            } return ds;
        }

        public static string checkifhaskpimaintenanceaccess(int cimno)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                SqlCommand cmd = null;
                cmd = new SqlCommand("pr_Coach_GeTRoleofUserForKPIMaintenance", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cimno", cimno);
                cn.Open();
                cmd.ExecuteNonQuery();
                string access = (string)cmd.ExecuteScalar();
                cn.Close();
                cn.Dispose();
                cmd.Dispose();
                return access;
            }
        }


        //additional dropdown for Tl's agents on charts

        public static DataSet GetTLAgents(int CimNumber, int AccountID)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd = null;
                cmd = new SqlCommand("pr_Coach_GetSubordinatesofUser", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                return ds;

            }
        }

        public static DataSet CheckifHr(int CimNumber)
        {

            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd = null;
                cmd = new SqlCommand("pr_Coach_CheckifHR", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                return ds;

            }
        }


        public static void UpdateHRReviewNexidia(int CoacheeID, int AccountID, int SupervisorID, int TopicId, string FollowDate, string Description, string Strengths, string Opportunity,
        int SessionFocus, string PositiveBehaviour, string OpportunityBehaviour, string BeginBehaviourComments, string BehaviourEffects, string RootCause, string ReviewResultsComments,
        string AddressOpportunities, string ActionPlanComments, string SmartGoals, string CreatePlanComments, string FollowThrough, int CreatedBy, string HRComments, string SupervisorComments, int Reviewid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_Coach_update_review_nexidia";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
                cmd.Parameters.AddWithValue("@TopicID", TopicId);
                if (FollowDate != null)
                {
                    cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@Description", Description);

                cmd.Parameters.AddWithValue("@Strengths", Strengths);
                cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
                if (SessionFocus == 0)
                {
                    cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
                }
                cmd.Parameters.AddWithValue("@PositiveBehaviour", PositiveBehaviour);
                cmd.Parameters.AddWithValue("@OpportunityBehaviour", OpportunityBehaviour);
                cmd.Parameters.AddWithValue("@BeginBehaviourComments", BeginBehaviourComments);

                cmd.Parameters.AddWithValue("@BehaviourEffects", BehaviourEffects);
                cmd.Parameters.AddWithValue("@RootCause", RootCause);
                cmd.Parameters.AddWithValue("@ReviewResultsComments", ReviewResultsComments);
                cmd.Parameters.AddWithValue("@AddressOpportunities", AddressOpportunities);
                cmd.Parameters.AddWithValue("@ActionPlanComments", ActionPlanComments);
                cmd.Parameters.AddWithValue("@SmartGoals", SmartGoals);

                cmd.Parameters.AddWithValue("@CreatePlanComments", CreatePlanComments);
                cmd.Parameters.AddWithValue("@FollowThrough", FollowThrough);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                cmd.Parameters.AddWithValue("@HRComments", HRComments);
                cmd.Parameters.AddWithValue("@SupervisorComments", SupervisorComments);
                cmd.Parameters.AddWithValue("@Reviewid", Reviewid);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataSet checkifhascoachingnotes(int ReviewID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd = null;
                cmd = new SqlCommand("pr_Coach_check_for_inc", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                return ds;

            }
        }



        public static void UpdateHRReview(int CoacheeID, int AccountID, int SupervisorID, int TopicId, DateTime? FollowDate, string Strengths,
                                    string Opportunity, string CoacherFeedback, string HRComments, string SupervisorComments, int Reviewid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query_add = @"pr_get_hr_review";
                cmd = new SqlCommand(query_add, cn);
                cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
                cmd.Parameters.AddWithValue("@TopicID", TopicId);
                if (FollowDate != null)
                {
                    cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@Strengths", Strengths);
                cmd.Parameters.AddWithValue("@Opportunity", Opportunity);

                cmd.Parameters.AddWithValue("@CoacherFeedback", @CoacherFeedback);
                cmd.Parameters.AddWithValue("@HRComments", HRComments);
                cmd.Parameters.AddWithValue("@SupervisorComments", SupervisorComments);
                cmd.Parameters.AddWithValue("@Reviewid", Reviewid);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }




        public static DataSet GetAllTicketTypes()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetAllTicketTypes";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAllDepartments()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetAllDepartments";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;// .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAllLOB(int AccountID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetLOB";


                cmd = new SqlCommand(query, cn);

                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        /// <summary>
        /// pdf sample for reports
        /// </summary>
        /// <returns></returns>
        public static DataSet getuserstable_pdf()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd = null;
                cmd = new SqlCommand("select top 3 * from [UsersTable]", cn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                return ds;

            }
        }

        public static DataSet SetTicketStatus(int stat, int Reviewid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_SetReviewStatus";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@statusid", stat);
                cmd.Parameters.AddWithValue("@reviewid", Reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getBuiltInReportDefault(string startdate, string enddate)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_BuiltInReportDefault";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@StartDate", startdate);
                cmd.Parameters.AddWithValue("@FinishDate", enddate);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet CreateBuiltInReport(int ReportID, int TicketStatusID, int SessionType, int SessionTopic, int Users, int DeptId, int AccountID, int LOBId, int HideFromlist, DateTime startdate, DateTime enddate)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Insert_BuiltIn_Columns";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@ReportID", ReportID);
                cmd.Parameters.AddWithValue("@TicketStatusID", TicketStatusID);
                cmd.Parameters.AddWithValue("@SessionType", SessionType);
                cmd.Parameters.AddWithValue("@SessionTopic", SessionTopic);
                cmd.Parameters.AddWithValue("@Users", Users);
                cmd.Parameters.AddWithValue("@DeptId", DeptId);
                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                cmd.Parameters.AddWithValue("@LOBId", LOBId);
                cmd.Parameters.AddWithValue("@HideFromlist", HideFromlist);
                cmd.Parameters.AddWithValue("@StartDate", startdate);
                cmd.Parameters.AddWithValue("@FinishDate", enddate);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getBuiltInReportFilters(int reportid, int cimno)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_BuiltInReportFilter";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@reportid", reportid);
                cmd.Parameters.AddWithValue("@cimno", cimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getReportType(int reportid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getReportType"; //select ReportID, ReportName, ReportType , CreatedBy from ReportTable where reportid = @Reportid
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@Reportid", reportid);
                cmd.CommandType = CommandType.StoredProcedure; // .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet SaveBuiltIn(int reportid, int Status, string Name)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_UpdateReportStatus_BuiltIn";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@Reportid", reportid);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getreportbuiltinusers()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"getreportbuiltinusers"; //select * from dbo.tbl_Coach_BuiltInReportUsers
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure; // .Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getreportusers(int cimnumber)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getreportusers"; //select * from dbo.tbl_Coach_Report_Users where hidefromlist = 0 and cimnumber = @cimnumber
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@cimnumber", cimnumber);
                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet get_users_voc_kpi(int cimnumber)
        {
            //pr_Coach_GetUserVOCKPI
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetUserVOCKPI";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@Cimno", cimnumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetTopKPIIList_admin()
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_get_KPIList_Adminv2";


                cmd = new SqlCommand(query, cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIIDfromScorecard(int kpiid)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIFromLookUp";
                //select top 1  id,REPLACE(KPI, '[Measures].', '') as 'KPItrim' from ScoreCard.dbo.tbl_ScoreCard_Lkp_KPI where id = @kpiid ";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@kpiid", kpiid);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet InsertNewKPIv2(int kpiid, string kpi_name, string kpi_desc,
           int valuetype, int linkto, string createdby, int hidefromlist, int accountid, DateTime createdon, int hierarchy)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_addNewKPIv2";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("kpiid", kpiid);
                cmd.Parameters.AddWithValue("kpi_name", kpi_name);
                cmd.Parameters.AddWithValue("kpi_desc", kpi_desc);
                cmd.Parameters.AddWithValue("valuetype", valuetype);
                cmd.Parameters.AddWithValue("linkto", linkto);
                cmd.Parameters.AddWithValue("createdby", createdby);
                cmd.Parameters.AddWithValue("hidefromlist", hidefromlist);
                cmd.Parameters.AddWithValue("accountid", accountid);
                cmd.Parameters.AddWithValue("createdon", createdon);
                cmd.Parameters.AddWithValue("hierarchy", hierarchy);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        #region from bi
        public static DataSet getGlidepath_agent_bi(string startdate, string enddate, int cim, int KPI, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCurrScorevsTarget_AgentfromBIData";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet getGlidepath_TL_bi(string startdate, string enddate, int cim, int subcimno, int KPI)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_TL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("subcimno", subcimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_OM_bi(string startdate, string enddate, int cim, int TLCimno, int AgentCimno, int KPI)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_OM";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLCimno", TLCimno);
                cmd.Parameters.Add("AgentCimno", AgentCimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_Dir_bi(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_Dir";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimNo", OMCimNo);
                cmd.Parameters.Add("TLCimno", TLCimNo);
                cmd.Parameters.Add("AgentCimno", AgentCimNo);
                cmd.Parameters.Add("KPI", KPI);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_QA_bi(string startdate, string enddate, int cim, int KPI)        //coaching frequency
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_QA";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getGlidepath_HR_bi(string startdate, string enddate, int cim, int KPI)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_HR";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetSubordinatesforcharts(int CIMNo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;

                cmd = new SqlCommand("pr_Coach_GetSubordinatesforcharts", cn);
                cmd.Parameters.AddWithValue("@CimNumber", CIMNo);

                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSubordinatesforchartsVP(int CIMNo, int LobID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd;

                cmd = new SqlCommand("pr_Coach_GetSubordinatesforchartsVP", cn);
                cmd.Parameters.AddWithValue("@CimNumber", CIMNo);
                cmd.Parameters.AddWithValue("@LOBID", LobID);
                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet gettopkpis(int cimno)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTOPKPIs";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        //Changes by Charlie 08/08/2018 <--- lucky daaaaaaay
        public static DataSet GetKPIByLOB(int LOBID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetKPIsByLOB";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("LOBID", LOBID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet gettopkpiscores_aget(string startdate, string enddate, int cim, int KPIhierarchy, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyScore_Agent";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIhierarchy", KPIhierarchy);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet gettopkpiscores_for_TL(string startdate, string enddate, int cim, int subcimno, int KPI, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_KPI_TL";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("subcimno", subcimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_VS_ScoresTL(string startdate, string enddate, int cim, int subcimno, int KPI, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_TL_FORKPIS";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("SubCimno", subcimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet gettopkpiscores_for_OM(string startdate, string enddate, int cim, int TLcimno, int Agentcimno, int KPI, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_OM_FORKPIS";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLcimno", TLcimno);
                cmd.Parameters.Add("Agentcimno", Agentcimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getactualglide_for_OM(string startdate, string enddate, int cim, int TLcimno, int Agentcimno, int KPI, int DataView)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_OM_fromBI";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("TLcimno", TLcimno);
                cmd.Parameters.Add("Agentcimno", Agentcimno);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_DIR_Scores(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI, int DataView, int SiteID, int LobID, int AccountID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_DIR_FORScoreCha";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimNo);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("KPI", KPI);
                cmd.Parameters.Add("DataView", DataView);
                cmd.Parameters.Add("SiteID", SiteID);
                cmd.Parameters.Add("LOBID", LobID);
                //cmd.Parameters.Add("AccountID", AccountID);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_DIR_Glides(string startdate, string enddate, int cim, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI, int DataView)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTargetVSScore_Dir_fromBI";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("OMCimno", OMCimNo);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("KPI", KPI);

                cmd.Parameters.Add("DataView", DataView);
                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingFrequency_QA_Scores(string startdate, string enddate, int cim, int KPI)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_QA_OMDir";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetCoachingFrequency_HR_Scores(string startdate, string enddate, int cim, int KPI)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_HR_Score";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);


                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        //[pr_Coach_GetTargetVSScore_Dir_fromBI]
        #endregion from bi



        public static DataSet GetCoachingFrequency_ForMyTeam(string startdate, string enddate, int cim, int KPI)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_ForMyTeam";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPI", KPI);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetCoachingOverallFrequency_ForMyTeam(string startdate, string enddate, int KPIid, int cim)        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequencyoverall";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cim);
                cmd.Parameters.Add("KPIid", KPIid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetDataView()        //coaching frequency
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"select * from [CoachV2].[dbo].tbl_Coach_DataView where hidefromlist = 0 order by sortorder";


                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static void UpdateAdditionalInfo(string UserID, string AdditionalInfo)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_Coach_UpdateUserAdditionalInfo";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@UserID", UserID);
                cmd.Parameters.Add("@AdditionalInfo", AdditionalInfo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataSet GetSubordinates2(int CimNumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSubordinatesForReviews";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNumber", CimNumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }


        public static DataSet GetSapDept(int cimno)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_DisplayDept";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimno", cimno);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet UpdateDeptUserinfo(int cimno, string deptname, string rolename)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_UpdateDepartment";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimno", cimno);
                cmd.Parameters.Add("deptname", deptname);
                cmd.Parameters.Add("rolename", rolename);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet UpdateDeptUserinfodetails(int cimno, string deptname, string rolename, string reportingto)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_UpdateUserInfoDetails";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cimno", cimno);
                cmd.Parameters.Add("deptname", deptname);
                cmd.Parameters.Add("rolename", rolename);
                cmd.Parameters.Add("reportingto", reportingto);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 2000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;

        }

        public static DataSet Get_Dir_Weekly_Review(int cim)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetWeeklyReview_DirDash";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("cim", cim);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet Checkiftalktalk(int acct)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getTalktalkAccounts";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("acct", acct);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet CheckifOps(int acct)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getTalktalkAccounts";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("acct", acct);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getpreviouscommitment(int ReviewID, int Type)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_getpreviouscommitment_forpdf";//@"pr_Coach_Get_INC_History";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
                //cmd.Parameters.AddWithValue("@Type", Type);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        
        public static DataSet checkroleifQA(int roleid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckifQARole";//@"pr_Coach_Get_INC_History";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.AddWithValue("@roleid", roleid);
                //cmd.Parameters.AddWithValue("@Type", Type);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetUserInfoForSSO(string EmailAddress)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCIMViaEmail";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@Gmail", EmailAddress);

                cmd.CommandType = CommandType.StoredProcedure; //.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetKPIPerfODv2(int Id)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_get_review_kpi_ODv2";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(@"Id", Id);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet Get_ReviewIDv2(int reviewid)
        {


            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetReviewID_Agentv2";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("reviewID", reviewid);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }





        public static DataSet GetUserIfSpecial(int cimnumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_CheckIfSpecialRole";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@Cimnumber", cimnumber);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetDirectorList(int cimnumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetDirectorList";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@Cimnumber", cimnumber);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSiteList()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSite";
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetSiteList1(int cimnumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetSite1";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@Cimnumber", cimnumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetLOBList()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetLOBList";
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetLOBList1(int cimnumber)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetLOBList1";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@Cimnumber", cimnumber);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        } 

        public static DataSet GetOverAllCoachingFrequencyVP(string startdate, string enddate, int cimno, int DirCimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int siteid, int accountid, int LobID, int KPIID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetCoachingFrequency_VPCha";
                cmd = new SqlCommand(query, cn); 
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.Parameters.Add("Dircimno", DirCimno);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("DataView", DataView);
                cmd.Parameters.Add("SiteId", siteid);
                cmd.Parameters.Add("AccountId", LobID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }
        public static DataSet GetOverAllBehaviorVP(string startdate, string enddate, int cimno, int DirCimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int siteid, int accountid, int kpiid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorChart_forVP";
                cmd = new SqlCommand(query, cn); 
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.Parameters.Add("Dircimno", DirCimno);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("SiteId", siteid);
                cmd.Parameters.Add("kpiid", kpiid);
                cmd.Parameters.Add("LOBID", accountid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetOverBehaviorVSKPIVP(string startdate, string enddate, int cimno, int DirCimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int siteid, int accountid, int kpiid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetBehaviorsVSKPI_VP";
                cmd = new SqlCommand(query, cn);
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.Parameters.Add("Dircimno", DirCimno);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                cmd.Parameters.Add("DataView", DataView);
                cmd.Parameters.Add("SiteId", siteid);
                cmd.Parameters.Add("AccountId", accountid);
                cmd.Parameters.Add("kpiid", kpiid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }


        public static DataSet GetFrequencyVSKPI(string startdate, string enddate, int cimno, int DirCimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int siteid, int accountid, int kpiid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetFrequencyVSKPI_VP";
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.Parameters.Add("Dircimno", DirCimno);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);

                cmd.Parameters.Add("kpi", kpiid);
                cmd.Parameters.Add("DataView", DataView);
                cmd.Parameters.Add("LOBID", accountid);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetAVSKPI_VP(string startdate, string enddate, int cimno, int DirCimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int siteid, int accountid, int kpiid)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"Pr_coach_gettargetvsscore_vp_frombi"; //pr_Coach_GetActualVSTarget_VP original sp
                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("StartDate", startdate);
                cmd.Parameters.Add("FinishDate", enddate);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.Parameters.Add("Dircimno", DirCimno);
                cmd.Parameters.Add("OMCimno", OMCimno);
                cmd.Parameters.Add("TLCimNo", TLCimNo);
                cmd.Parameters.Add("AgentCimNo", AgentCimNo);
                
                cmd.Parameters.Add("kpi", kpiid);
                cmd.Parameters.Add("DataView", DataView);
                //cmd.Parameters.Add("SiteId", siteid);
                cmd.Parameters.Add("LOBID", accountid);
                

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 20000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet getkpisforVP(int cimno)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetTOPVPKPIs";


                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("Cimno", cimno);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            return ds;
        }



        public static string Encrypt(int text)
        {
            string EncryptionKey = "TRNSCMV32017111";
            string clearText = text.ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public static string encrypt1(string text)
        {
            string EncryptionKey = "TRNSCMV32017111";
            string clearText = text.ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "TRNSCMV32017111";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        /*VP from Charlie*/
        public static DataSet GetMyReviewsVP(int CIMNo, bool coacheesigned, bool coachersigned)
        {
            DataSet dsReviews = new DataSet();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;
                string query = @"pr_Coach_GetMyReviewsVP";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CimNo", CIMNo);
                cmd.Parameters.Add("@CoacheeSigned", coachersigned);
                cmd.Parameters.Add("@CoacherSigned", coacheesigned);
                cmd.CommandTimeout = 2000;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dsReviews);
            }
            return dsReviews;
        }
    }

}