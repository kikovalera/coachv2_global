﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Charts.aspx.cs" Inherits="CoachV2.Charts"  MasterPageFile="~/Site.Master" %>
 <%@ Register src="UserControl/Ops_ChartAgents.ascx" tagname="UserControlAgent1" tagprefix="ucdashagent1"  %>
 <%@ Register src="UserControl/Ops_ChartTL.ascx" tagname="UserControlAgent2" tagprefix="ucdashtl1"  %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
 

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderCharts" runat="server">
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>

    <div class="menu-content bg-alt">
        <div id="ChartDiv" runat="server">
        </div>
    
    </div>
</asp:Content>
