﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportDefault.aspx.cs" Inherits="CoachV2.ReportDefault" %>

<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/DashboardMyReviewsReport.ascx" TagName="DashboardMyReviewsUserControl" TagPrefix="ucdash5" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<script type="text/javascript" src="libs/js/JScript1.js"></script>
       <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>           
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server"></telerik:RadAjaxPanel>
    <asp:Panel ID="pnlMain" runat="server">
        <div class="menu-content bg-alt">
            <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
              <div class="panel-group" id="accordion">
                <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Recently Used Reports <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                            </h4>
                        </div>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" GroupPanelPosition="Top"
                                        AutoGenerateColumns="false" AllowFilteringByColumn="false"  OnItemDataBound="RadGrid1_ItemDataBound" OnNeedDataSource="RadGrid1_NeedDataSource">
                                        <MasterTableView DataKeyNames="ReportID">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Date Created" >
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" Height="25px" Width="25px" />
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("IsExported") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("CreatedOn") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="ReportID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ReportID" runat="server" Text='<%# Eval("ReportID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
<%--                                                <telerik:GridHyperLinkColumn DataTextField="ReportName" HeaderText="Report Name" UniqueName="ReportName"
                                                    FilterControlToolTip="ReportName" DataNavigateUrlFields="ReportName">
                                                </telerik:GridHyperLinkColumn>--%>
                                                <telerik:GridTemplateColumn HeaderText="Report Name" UniqueName="ReportName">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnlnk" runat="server" Text='<%# Eval("ReportName") %>' Target="_blank" OnClick="btnlnk_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ReportType" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                  <telerik:GridTemplateColumn HeaderText="Date Used">
                                                    <ItemTemplate>
                                                        <asp:Label ID="DateUsed" runat="server" Text='<%# Eval("LastUsedPo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
