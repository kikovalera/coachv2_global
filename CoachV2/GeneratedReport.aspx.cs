﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using Telerik.Web.UI.GridExcelBuilder;
using System.Web.Configuration;
using System.Data.SqlClient;
using CoachV2.AppCode;


namespace CoachV2
{
    public partial class GeneratedReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
                if (Page.Request.QueryString["ReportID"] != null && Page.Request.QueryString["UserID"] != null)
                {
                    DataSet ds_reportype = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));

                    if (ds_reportype.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToString(ds_reportype.Tables[0].Rows[0]["ReportType"].ToString()) != "2")
                        {

                            string ReportID = Page.Request.QueryString["ReportID"].ToString();
                            string UserID = Page.Request.QueryString["UserID"].ToString();
                            HiddenReportID.Value = ReportID.ToString();
                            HiddenUserID.Value = UserID.ToString();
                            Report rt = new Report();
                            DataSet ds = null;
                            ds = rt.GenerateSavedReportNew(ReportID, UserID);

                            Report rt2 = new Report();
                            DataSet ds2 = null;
                            ds2 = rt2.GetReportName(ReportID);
                            if (ds2.Tables[0].Rows.Count > 0)
                            {
                                Label1.Text = ds2.Tables[0].Rows[0]["ReportName"].ToString();
                            }


                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                RadGrid1.DataSource = null;
                                RadGrid1.DataBind();
                                RadGrid1.DataSource = ds;
                                RadGrid1.DataBind();
                            }
                        }
                        else
                        {
                            DataSet ds_reportype1 = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));
                            if (ds_reportype1.Tables[0].Rows.Count > 0)
                            {
                                Label1.Text = ds_reportype1.Tables[0].Rows[0]["ReportName"].ToString();
                            }
                            string ReportID = Page.Request.QueryString["ReportID"].ToString();
                            string UserID = Page.Request.QueryString["UserID"].ToString();
                            HiddenReportID.Value = ReportID.ToString();
                            HiddenUserID.Value = UserID.ToString();
                            RadGrid1.DataSource = null;
                            RadGrid1.DataBind();
                            Report rt = new Report();
                            DataSet ds_default_builtin = DataHelper.getBuiltInReportFilters(Convert.ToInt32(Page.Request.QueryString["ReportID"]), Convert.ToInt32(Page.Request.QueryString["UserID"])); 
                            RadGrid1.DataSource = ds_default_builtin;
                            RadGrid1.DataBind();
                             

                        }
                    }

                //}

            }

        }
        //protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    string ReportID = Page.Request.QueryString["ReportID"].ToString();
        //    string UserID = Page.Request.QueryString["UserID"].ToString();

        //    if (e.CommandName == "ExportToExcel" || e.CommandName == "ExportToCsv")
        //    {
        //        var ds = new DataSet();

        //        ds.Tables.Add(LoadData(HiddenReportID.Value, HiddenUserID.Value));


        //        if (e.CommandName == "ExportToExcel")
        //        {
        //            ExcelHelperUpdated.ToExcel(ds, "Report " + DateTime.Now + ".xls", Page.Response);
        //        }
        //        else
        //        {
        //            if (ds.Tables.Count > 1)
        //            {
        //                CSVHelper.ConvertToCSV(ds, "Report " + DateTime.Now + ".csv", Page.Response);
        //            }
        //            else
        //            {
        //                CSVHelper.ConvertToCSV(ds.Tables[0], "Report " + DateTime.Now + ".csv", Page.Response);
        //            }

        //        }
        //    }
        //}
        protected void RadGrid1_ExcelMLWorkBookCreated(object sender, GridExcelMLWorkBookCreatedEventArgs e)
        {

            foreach (RowElement row in e.WorkBook.Worksheets[0].Table.Rows)
            {
                row.Cells[0].StyleValue = "Style1";
            }

            StyleElement style = new StyleElement("Style1");
            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
            style.InteriorStyle.Color = System.Drawing.Color.LightGray;
            e.WorkBook.Styles.Add(style);

        }
        private DataTable LoadData(string reportID, string User)
        {
            DataTable dt = new DataTable();
            try
            {
                //low level sql datatable 
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
                {
                    cn.Open();

                    string sql = @"pr_Coach_GenReportNew";
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cmd.CommandTimeout = 20000;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportID", reportID);
                    cmd.Parameters.AddWithValue("@UserID", User);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                }
                return dt;
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadData " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                return dt;
            }

        }
        protected void RadMenu1_ItemClick(object sender, Telerik.Web.UI.RadMenuEventArgs e)
        {
            //Label1.Text = "The MENU was clicked!";
            //Label1.Text = e.Item.Text;
            string ReportID = Page.Request.QueryString["ReportID"].ToString();
            string UserID = Page.Request.QueryString["UserID"].ToString();

            DataSet ds_reportype = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));

            if (e.Item.Text== "CSV")
            {
                       if (ds_reportype.Tables[0].Rows.Count > 0)
                       {
                           if (Convert.ToString(ds_reportype.Tables[0].Rows[0]["ReportType"].ToString()) != "2")
                           {
                               var ds = new DataSet();

                               ds.Tables.Add(LoadData(ReportID, UserID));

                               if (ds.Tables.Count > 0)
                               {
                                   Report rt = new Report();
                                   rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 2);

                                   CSVHelper.ConvertToCSV(ds, "Report " + DateTime.Now + ".csv", Page.Response);


                               }      

                           }

                           else
                           { 
                              DataSet  ds_reportvalues = DataHelper.getBuiltInReportFilters(Convert.ToInt32(Page.Request.QueryString["ReportID"]), Convert.ToInt32(Page.Request.QueryString["UserID"])); //DataHelper.getBuiltInReportDefault(dp_startdate, dp_enddate);
                                
                               if (ds_reportvalues.Tables.Count > 0)  
                               {

                                   Report rt = new Report();
                                   rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 2);

                                   CSVHelper.ConvertToCSV(ds_reportvalues, "Report " + DateTime.Now + ".csv", Page.Response);

                               }      
                           
                           
                           }
                       }

                
            }
            else if (e.Item.Text == "EXCEL")
             {

                 if (ds_reportype.Tables[0].Rows.Count > 0)
                 {
                     if (Convert.ToString(ds_reportype.Tables[0].Rows[0]["ReportType"].ToString()) != "2")
                     {
                         var ds = new DataSet();

                         ds.Tables.Add(LoadData(ReportID, UserID));

                         if (ds.Tables.Count > 0)
                         {
                             Report rt = new Report();
                             rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 2);

                             ExcelHelperUpdated.ToExcel(ds, "Report " + DateTime.Now + ".xls", Page.Response);

                         }
                     }
                     else 
                     {
                         DataSet ds_reportvalues = DataHelper.getBuiltInReportFilters(Convert.ToInt32(Page.Request.QueryString["ReportID"]), Convert.ToInt32(Page.Request.QueryString["UserID"])); //DataHelper.getBuiltInReportDefault(dp_startdate, dp_enddate);
 
                         if (ds_reportvalues.Tables.Count > 0)
                         {
                             Report rt = new Report();
                             rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 2);

                             ExcelHelperUpdated.ToExcel(ds_reportvalues, "Report " + DateTime.Now + ".xls", Page.Response);

                         }
                     }
                 
                 
                 }
               
            }

        }
        protected void RadButton1_Click(object sender, EventArgs e)
        {
            Label1.Text = "The Button was clicked!";
        }

        protected void RadBack_Click(object sender, EventArgs e)
        {
            string jScript = "<script>window.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
        }

        protected void RadSave_Click(object sender, EventArgs e)
        {
            DataSet ds_reportype = DataHelper.getReportType(Convert.ToInt32(HiddenReportID.Value));

            if (ds_reportype.Tables[0].Rows.Count > 0)
            {
                if (ds_reportype.Tables[0].Rows[0]["ReportType"].ToString() != "2")
                {
                    Report rt = new Report();
                    rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 1);

                    string ModalLabel = "Successfully saved report.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }
                else
                {
                    string ReportID = Page.Request.QueryString["ReportID"].ToString();
                    string UserID = Page.Request.QueryString["UserID"].ToString();
           
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal1(); });", true);
                    RadSave.CssClass = "btn btn-info btn-small";
                                          
                }
            }
        }

        protected void RadBackMod_Click(object sender, EventArgs e)
        {
            string jScript = "<script>window.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
        }

        protected void RadSaveMod_Click(object sender, EventArgs e)
        {
            DataSet ds_reportype = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));
            if (RadReportName.Text != string.Empty)
            {
                Report rp = new Report();
                DataSet dsr = null;
                dsr = rp.CheckIfReportNameExists(RadReportName.Text, HiddenUserID.Value, 2);

                if (dsr.Tables[0].Rows.Count > 0)
                {
                    string ModalLabel = "Report already exists. Please use another report name";
                    string ModalHeader = "Error";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    RadReportName.Focus();
                }
                else
                {
                    if (ds_reportype.Tables[0].Rows.Count > 0)
                    {
                        DataHelper.SaveBuiltIn(Convert.ToInt32(Page.Request.QueryString["ReportID"]), 1, Convert.ToString(RadReportName.Text));
                        string ModalLabel = "Successfully saved report.";
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        DataSet ds_reportype1 = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));
                        if (ds_reportype1.Tables[0].Rows.Count > 0)
                        {
                            Label1.Text = ds_reportype1.Tables[0].Rows[0]["ReportName"].ToString();
                        }
                    }
                }
            }
            else
            {
                string ModalLabel = "Fill up Report Name";
                string ModalHeader = "Error";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                RadReportName.Focus();
            }
        }
    }
}