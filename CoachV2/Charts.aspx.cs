﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;


namespace CoachV2
{
    public partial class Charts : System.Web.UI.Page
    {
        private int cim_num;
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            string cim_num = ds.Tables[0].Rows[0][0].ToString();
            int intcim = Convert.ToInt32(cim_num); //27093;//Convert.ToInt32(cim_num); //98461; //95759;//27093;// Convert.ToInt32(cim_num);  


            DataSet ds_special_role = DataHelper.GetUserIfSpecial(intcim);
            if (ds_special_role.Tables[0].Rows.Count > 0)
            {
                ChartDiv.Controls.Clear();
                Ops_VP Ops_VP = (Ops_VP)LoadControl("UserControl/Ops_VP.ascx");
                ChartDiv.Controls.Add(Ops_VP);
            }
            else
            {
                get_hierarchy();
            }
         
        }

        protected void get_hierarchy()
        {
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string emp_email =   ds.Tables[0].Rows[0]["Email"].ToString();
            string cim_num =  ds.Tables[0].Rows[0][0].ToString();
            //int intcim = Convert.ToInt32(cim_num);
            int intcim = Convert.ToInt32(cim_num); //98461;//27093;// Convert.ToInt32(cim_num);  
            //////testing purposes for sir bart's access 
            //string emp_email = "ROMEL.BARATETA@TRANSCOM.COM";//"TRACEY.RUSTIA@TRANSCOM.COM";//"ROMEL.BARATETA@TRANSCOM.COM";//ds.Tables[0].Rows[0]["Email"].ToString(); //"MARIA.CASTANEDA@TRANSCOM.COM";//"MARVIN.REGALADO@TRANSCOM.COM"; //
            //string cim_num = "10113944";//"95759"; //"10113944";  // ds.Tables[0].Rows[0][0].ToString();
            //int intcim = Convert.ToInt32(cim_num);//95759;//10113944;  //Convert.ToInt32(cim_num);
            ////testing purposes for sir bart's access 

            DataSet ds_hassubs = DataHelper.CheckforSubs(Convert.ToInt32(intcim));
            DataSet ds1 = null;
            DataAccess ws1 = new DataAccess();
            int userhierarchy;
            userhierarchy = ws1.getroleofloggedin_user(intcim);

            DataAccess ws2 = new DataAccess();
            string department = "";
            department = ws2.getuserdept(intcim);

            int deptid, roleid;
            DataSet ds_userdept = DataHelper.get_adminusername(emp_email);
            if (ds_userdept.Tables[0].Rows.Count > 0)
            {
                deptid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["accountid"].ToString());
                roleid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["roleid"].ToString());
            }
            else
            {
                deptid = 0;
                roleid = 0;
            }
            DataSet ds_checkhierarchy = DataHelper.checkuserhierarchy(intcim);
            if (ds_checkhierarchy.Tables[0].Rows.Count > 0)
            {
                if (deptid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["dept_id"].ToString()) && (roleid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["role_id"].ToString())))
                { }
                else
                {
                    //update user hierarchy
                    DataHelper.Edituserhierarchy(intcim, deptid, roleid, userhierarchy);
                }
            }

            else
            {
                DataHelper.Insertuserhierarchy(intcim, deptid, roleid, userhierarchy);

            }
            if (department == "HR")
            {
                if (userhierarchy == 1)
                {
                    //agent         
                    ChartDiv.Controls.Clear();
                    Ops_ChartAgents Ops_ChartAgents = (Ops_ChartAgents)LoadControl("UserControl/Ops_ChartAgents.ascx");
                    ChartDiv.Controls.Add(Ops_ChartAgents);
                }
                else if (userhierarchy == 2)
                {
                    //tl
                    ChartDiv.Controls.Clear();
                    Ops_ChartTL Ops_ChartTL = (Ops_ChartTL)LoadControl("UserControl/Ops_ChartTL.ascx");
                    ChartDiv.Controls.Add(Ops_ChartTL);
                }
                else if (userhierarchy == 3)
                {//om
                    ChartDiv.Controls.Clear();
                    Ops_OMCharts Ops_OMCharts = (Ops_OMCharts)LoadControl("UserControl/Ops_OMCharts.ascx");
                    ChartDiv.Controls.Add(Ops_OMCharts);
                }

                else if (userhierarchy == 4)
                { //dir
                    ChartDiv.Controls.Clear();
                    Ops_ChartDir Ops_ChartDir = (Ops_ChartDir)LoadControl("UserControl/Ops_ChartDir.ascx");
                    ChartDiv.Controls.Add(Ops_ChartDir);
                }
            }

            else if (department == "QA")
            {
                if (userhierarchy == 1)
                {
                    //agent         
                    ChartDiv.Controls.Clear();
                    Ops_ChartAgents Ops_ChartAgents = (Ops_ChartAgents)LoadControl("UserControl/Ops_ChartAgents.ascx");
                    ChartDiv.Controls.Add(Ops_ChartAgents);
                }
                else if (userhierarchy == 2)
                {
                    //tl
                    ChartDiv.Controls.Clear();
                    Ops_ChartTL Ops_ChartTL = (Ops_ChartTL)LoadControl("UserControl/Ops_ChartTL.ascx");
                    ChartDiv.Controls.Add(Ops_ChartTL);
                }
                else if (userhierarchy == 3)
                {//om
                    ChartDiv.Controls.Clear();
                    Ops_OMCharts Ops_OMCharts = (Ops_OMCharts)LoadControl("UserControl/Ops_OMCharts.ascx");
                    ChartDiv.Controls.Add(Ops_OMCharts);
                }
                else if (userhierarchy == 4)
                { //dir
                    ChartDiv.Controls.Clear();
                    Ops_ChartDir Ops_ChartDir = (Ops_ChartDir)LoadControl("UserControl/Ops_ChartDir.ascx");
                    ChartDiv.Controls.Add(Ops_ChartDir);
                }
            }
            else
            {
                if (userhierarchy == 1)
                {
                    //agent         
                    ChartDiv.Controls.Clear();
                    Ops_ChartAgents Ops_ChartAgents = (Ops_ChartAgents)LoadControl("UserControl/Ops_ChartAgents.ascx");
                    ChartDiv.Controls.Add(Ops_ChartAgents);
                }
                else if (userhierarchy == 2)
                {
                    //tl
                    ChartDiv.Controls.Clear();
                    Ops_ChartTL Ops_ChartTL = (Ops_ChartTL)LoadControl("UserControl/Ops_ChartTL.ascx");
                    ChartDiv.Controls.Add(Ops_ChartTL);
                }
                else if (userhierarchy == 3)
                {
                    //om
                    // added for special access (francis.valera/08022018)
                    ChartDiv.Controls.Clear();
                    if (DataHelper.UserHasSpecialAccess(intcim))
                    {
                        //dir
                        Ops_ChartDir Ops_ChartDir = (Ops_ChartDir)LoadControl("UserControl/Ops_ChartDir.ascx");
                        ChartDiv.Controls.Add(Ops_ChartDir);
                    }
                    else
                    {
                        Ops_OMCharts Ops_OMCharts = (Ops_OMCharts)LoadControl("UserControl/Ops_OMCharts.ascx");
                        ChartDiv.Controls.Add(Ops_OMCharts);
                    }
                    //ChartDiv.Controls.Clear();
                    //Ops_OMCharts Ops_OMCharts = (Ops_OMCharts)LoadControl("UserControl/Ops_OMCharts.ascx");
                    //ChartDiv.Controls.Add(Ops_OMCharts);
                }
                else if (userhierarchy == 4)
                {   //dir
                    ChartDiv.Controls.Clear();
                    Ops_ChartDir Ops_ChartDir = (Ops_ChartDir)LoadControl("UserControl/Ops_ChartDir.ascx");
                    ChartDiv.Controls.Add(Ops_ChartDir);
                }

            }

            //testing purposes
            //if (intcim == 10107032)
            //{
            //    DashboardDiv.Controls.Clear();
            //    QADashboard QADashboard = (QADashboard)LoadControl("UserControl/QADashboard.ascx");
            //    DashboardDiv.Controls.Add(QADashboard);
            //}


        }
    }
}