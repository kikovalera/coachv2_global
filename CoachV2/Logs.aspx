﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Logs.aspx.cs" Inherits="CoachV2.Logs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dpLogDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="gridLogs" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <div class="row">
        <div class="col-sm-8">
            <label style="padding-left:14px;font-size:20px;line-height:35px;">CoachV2 Error Logs</label>
        </div>
        <div class="col-sm-2">
            <label style="float:right;line-height:35px;">Log Date:</label>
        </div>
        <div class="col-sm-2" style="float:right;text-align:right;">
            <telerik:RadDatePicker ID="dpLogDate" runat="server" placeholder="Date" class="RadCalendarPopupShadows" EnableTyping="false"
                TabIndex="6" Width="100%" OnSelectedDateChanged="dpLogDate_OnSelectedDateChanged" AutoPostBack="true" style="padding-right:10px;">
                <DatePopupButton ToolTip="Click to select log date" />
                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True" runat="server"
                    FastNavigationNextText="&amp;lt;&amp;lt;">
                </Calendar>
                <DateInput ID="DateInput1" DisplayDateFormat="d MMMM yyyy" DateFormat="yyyy/MM/dd" LabelWidth="40%" TabIndex="6" runat="server">
                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                    <FocusedStyle Resize="None"></FocusedStyle>
                    <DisabledStyle Resize="None"></DisabledStyle>
                    <InvalidStyle Resize="None"></InvalidStyle>
                    <HoveredStyle Resize="None"></HoveredStyle>
                    <EnabledStyle Resize="None"></EnabledStyle>
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
            </telerik:RadDatePicker>
        </div>
        <div class="col-sm-12" style="padding-bottom:30px;">
            <telerik:RadGrid ID="gridLogs" runat="server" ResolvedRenderMode="Classic" RenderMode="Lightweight" 
                AutoGenerateColumns="false" OnNeedDataSource="gridLogs_NeedDataSource">
                <ClientSettings><Scrolling UseStaticHeaders="true"/></ClientSettings>
                <MasterTableView CssClass="RadGrid" GridLines="None" AllowPaging="True" PageSize="10" DataKeyNames="LogUserCIM" AllowSorting="true">
                    <Columns>
                        <%--<telerik:GridBoundColumn DataField="LogDateTime" DataFormatString="{0:t}" ItemStyle-Wrap="false" HeaderStyle-Width="170px" HeaderText="Time" UniqueName="LogDateTime" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn UniqueName="LogDateTime" HeaderText="Time" DataField="LogDateTime" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="logtime" runat="server" Text='<%#System.Convert.ToDateTime(Eval("LogDateTime")).ToString("HH:mm:ss.fff") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--<telerik:GridBoundColumn DataField="LogID" HeaderText="ID" UniqueName="LogID" HeaderStyle-Font-Bold="true" Display="false" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="LogUserCIM" HeaderText="CIM No" DataField="LogUserCIM" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="cim" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "LogUserCIM")%>' Width="100px"></asp:Label>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="cim"
                                    RelativeTo="Element" Position="TopCenter" ShowCallout="false" RenderInPageRoot="true" ShowEvent="OnMouseOver" HideEvent="LeaveTargetAndToolTip" >
                                    <%# DataBinder.Eval(Container.DataItem, "LogUserEmail")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                        <telerik:GridTemplateColumn UniqueName="LogUser" HeaderText="User" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="logcimno" runat="server" Text='<%#Eval("LogUserCIM")%>'></asp:Label><br />
                                <asp:Label ID="logemail" runat="server" Text='<%#Eval("LogUserEmail")%>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="LogErrorCode" HeaderText="Code" UniqueName="LogErrorCode" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LogErrorUrl" HeaderText="URL" UniqueName="LogErrorUrl" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="LogErrorLine" HeaderText="Method/Line No" UniqueName="LogErrorLine" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LogErrorMessage" HeaderStyle-Width="500px" HeaderText="Error Message" UniqueName="LogErrorMessage" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn UniqueName="LogUser" HeaderText="User" HeaderStyle-Font-Bold="true" HeaderStyle-Width="600px" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="logerrormsg" runat="server" Text='<%#Eval("LogErrorMessage")%>'></asp:Label>
                                <br /><asp:Label ID="logerrorlineno" runat="server" Text='<%#Eval("LogErrorLine")%>'></asp:Label>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip2" runat="server" TargetControlID="logerrorlineno"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true"
                                    Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                <%# DataBinder.Eval(Container.DataItem, "LogStackTrace")%>
                            </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
    </div>
</asp:Content>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dpLogDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="gridLogs" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <div class="row">
        <div class="col-sm-8">
            <label style="padding-left:14px;font-size:20px;line-height:35px;">CoachV2 Error Logs</label>
        </div>
        <div class="col-sm-2">
            <label style="float:right;line-height:35px;">Log Date:</label>
        </div>
        <div class="col-sm-2" style="float:right;text-align:right;">
            <telerik:RadDatePicker ID="dpLogDate" runat="server" placeholder="Date" class="RadCalendarPopupShadows" EnableTyping="false"
                TabIndex="6" Width="100%" OnSelectedDateChanged="dpLogDate_OnSelectedDateChanged" AutoPostBack="true" style="padding-right:10px;">
                <DatePopupButton ToolTip="Click to select log date" />
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True" runat="server"
                    FastNavigationNextText="&amp;lt;&amp;lt;">
                </Calendar>
                <DateInput DisplayDateFormat="d MMMM yyyy" DateFormat="yyyy/MM/dd" LabelWidth="40%" TabIndex="6" runat="server">
                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                    <FocusedStyle Resize="None"></FocusedStyle>
                    <DisabledStyle Resize="None"></DisabledStyle>
                    <InvalidStyle Resize="None"></InvalidStyle>
                    <HoveredStyle Resize="None"></HoveredStyle>
                    <EnabledStyle Resize="None"></EnabledStyle>
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
            </telerik:RadDatePicker>
        </div>
        <div class="col-sm-12" style="padding-bottom:30px;">
            <telerik:RadGrid ID="gridLogs" runat="server" ResolvedRenderMode="Classic" RenderMode="Lightweight" 
                AutoGenerateColumns="false" OnNeedDataSource="gridLogs_NeedDataSource">
                <ClientSettings><Scrolling UseStaticHeaders="true"/></ClientSettings>
                <MasterTableView CssClass="RadGrid" GridLines="None" AllowPaging="True" PageSize="10" DataKeyNames="LogUserCIM" AllowSorting="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="LogDateTime" ItemStyle-Wrap="false" HeaderStyle-Width="170px" HeaderText="Date" UniqueName="LogDateTime" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LogID" HeaderText="ID" UniqueName="LogID" HeaderStyle-Font-Bold="true" Display="false" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="LogUserCIM" HeaderText="CIM No" DataField="LogUserCIM" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="cim" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "LogUserCIM")%>' Width="100px"></asp:Label>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="cim"
                                    RelativeTo="Element" Position="TopCenter" ShowCallout="false" RenderInPageRoot="true" ShowEvent="OnMouseOver" HideEvent="LeaveTargetAndToolTip" >
                                    <%# DataBinder.Eval(Container.DataItem, "LogUserEmail")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="LogParentPage" HeaderText="Page" UniqueName="LogParentPage" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LogErrorLine" HeaderText="Method/Line No" UniqueName="LogErrorLine" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LogErrorMessage" HeaderStyle-Width="500px" HeaderText="Error Message" UniqueName="LogErrorMessage" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
    </div>
</asp:Content>--%>
