﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;


namespace CoachV2.UserControl
{
    public partial class Ops_ChartTL : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num =  ds.Tables[0].Rows[0][2].ToString();
            string gmail =   ds.Tables[0].Rows[0][13].ToString();
            int intcim = Convert.ToInt32(cim_num);

            string URL = (HttpContext.Current.Request.Url.Host == "localhost" ? "http://" : "http://") + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);

            FakeURLID.Value = URL;

            MyCIM.Value = Convert.ToString(cim_num);
            DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
            int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
            DataAccess ws = new DataAccess();
            DataSet ds_KPIforAgent = DataHelper.gettopkpis(intcim); //ws.GetKPIListPerAcct(Convert.ToString(acctno));
            StartDate.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            EndDate.SelectedDate = DateTime.Now;
           

            //overall behaviordropdown
            dp_behavior_start.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behavior_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior1.DataSource = ds_KPIforAgent;
            cb_kpi_behavior1.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior1.DataBind();
            }

            //behavior vs kpi
            dp_behaviorkpi_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_behaviorkpi_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior_kpi.DataSource = ds_KPIforAgent;
            cb_kpi_behavior_kpi.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior_kpi.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior_kpi.DataBind();
            }

            //aht dates
            dp_aht_start1.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_aht_end1.SelectedDate = DateTime.Now;
            dp_ahtglide_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_ahtglide_end.SelectedDate = DateTime.Now;

            

            //agent drop down
            DataSet ds_agentlist = DataHelper.GetSubordinatesforcharts(intcim);//.GetTLAgents(intcim, acctno);
            cb_overallagents.DataSource = ds_agentlist;
            cb_overallagents.DataBind();

            cb_agentlist2.DataSource = ds_agentlist;
            cb_agentlist2.DataBind();

            cb_agentlist3.DataSource = ds_agentlist;
            cb_agentlist3.DataBind();

            cb_agentlist4.DataSource = ds_agentlist;
            cb_agentlist4.DataBind();

            cb_agentlist5.DataSource = ds_agentlist;
            cb_agentlist5.DataBind();

            DataSet ds_DVlist = DataHelper.GetDataView();
            cb_overalldataview.DataSource = ds_DVlist;
            cb_overalldataview.DataBind();

            cb_top1scoredataview.DataSource = ds_DVlist;
            cb_top1scoredataview.DataBind();
            cb_top1avtdataview.DataSource = ds_DVlist;
            cb_top1avtdataview.DataBind();


            dlKPI.DataSource = DataHelper.gettopkpis(intcim);
            dlKPI.DataBind();

        }

        
    }
}