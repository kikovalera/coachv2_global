﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CoachV2.UserControl
{
    public partial class PerfGraphUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            MyCIM.Value = Request.QueryString["CIM"];

            ddKPIList.Items.Clear();
            ddKPIList.AppendDataBoundItems = true;
            ddKPIList.DataSource = DataHelper.GetAllKPIs();
            ddKPIList.DataTextField = "Name";
            ddKPIList.DataValueField = "KPIid";
            ddKPIList.DataBind();

        }
    }
}