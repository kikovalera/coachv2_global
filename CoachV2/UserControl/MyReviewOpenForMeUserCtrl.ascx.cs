﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class MyReviewOpenForMeUserCtrl : System.Web.UI.UserControl
    {
        private int cim_num;
        private string roleval;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            cim_num = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);

            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(cim_num);

            roleval = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());


            ReviewsGrid2.DataSource = String.Empty;
            DataSet ds2 = roleval.ToUpper().Contains("VP") == true ? DataHelper.GetMyReviewsVP(cim_num, false, false)  : DataHelper.GetMyReviewsTL(cim_num, false, false);

            DataTable dt;
            if (ds2.Tables[0].Rows.Count > 0)
            {
                var k = ds2.Tables[0].AsEnumerable()
                      .GroupBy(x => x.Field<System.Int32>("Id"))
                      .Select(g => g.First());
                if (k.Any())
                {
                    dt = k.CopyToDataTable();
                }
                else
                {
                    dt = ds2.Tables[0].Clone();
                }
                lblReviewsopenforme.Text = Convert.ToString(dt.Rows.Count);
            }

            else { lblReviewsopenforme.Text = "0"; }
            //}
        }

        protected void ReviewsGrid_PreRender(object sender, EventArgs e)
        {
            DataSet ds2 = roleval.ToUpper().Contains("VP") == true ? DataHelper.GetMyReviewsVP(cim_num, false, false) : DataHelper.GetMyReviewsTL(cim_num, false, false);
            DataTable dt;
            if (ds2.Tables[0].Rows.Count > 0)
            {

                var k = ds2.Tables[0].AsEnumerable()
                       .GroupBy(x => x.Field<System.Int32>("Id"))
                       .Select(g => g.First());
                if (k.Any())
                {
                    dt = k.CopyToDataTable();
                }
                else
                {
                    dt = ds2.Tables[0].Clone();
                }
                ReviewsGrid2.DataSource = dt;
                ReviewsGrid2.DataBind();

            }
            else
            {
                //                ReviewsGrid2.Controls.Add(new LiteralControl(""));
                ReviewsGrid2.DataSource = String.Empty;
            }
        }

        protected void ReviewsGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                cim_num = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);
                DataTable dt;
                try
                {
                    DataSet ds2 = roleval.ToUpper().Contains("VP") == true ? DataHelper.GetMyReviewsVP(cim_num, false, false) : DataHelper.GetMyReviewsTL(cim_num, false, false);

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        var k = ds2.Tables[0].AsEnumerable()
                      .GroupBy(x => x.Field<System.Int32>("Id"))
                      .Select(g => g.First());
                        if (k.Any())
                        {
                            dt = k.CopyToDataTable();
                        }
                        else
                        {
                            dt = ds2.Tables[0].Clone();
                        }
                        ReviewsGrid2.DataSource = dt;
                        ReviewsGrid2.DataBind();
                    }

                }
                catch (Exception ex)
                {
                    ReviewsGrid2.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void ReviewsGrid_onitemdatabound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is Telerik.Web.UI.GridDataItem)
            {
                Telerik.Web.UI.GridDataItem item = (Telerik.Web.UI.GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["CoacheeName"].Controls[0];
                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        //hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    DataSet ds = DataHelper.GetCoachingByTix(val1.ToString());
                    int? ReqSignOff = Convert.ToInt32(ds.Tables[0].Rows[0]["RequireSignOff"]);

                    if (ReqSignOff == 1)
                    {
                        hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/NSOAddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/NSOAddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

            }
        }
        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}