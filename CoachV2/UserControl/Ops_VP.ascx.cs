﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class Ops_VP : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            string URL = (HttpContext.Current.Request.Url.Host == "localhost" ? "http://" : "http://") + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
            FakeURLID.Value = URL;
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            string gmail = ds.Tables[0].Rows[0][13].ToString();
            string cim_num = ds.Tables[0].Rows[0][2].ToString(); //
            int intcim = Convert.ToInt32(cim_num);

            ////testing purposes for sir bart's access 
            //string emp_email = "ROMEL.BARATETA@TRANSCOM.COM";//"TRACEY.RUSTIA@TRANSCOM.COM";//"ROMEL.BARATETA@TRANSCOM.COM";//ds.Tables[0].Rows[0]["Email"].ToString(); //"MARIA.CASTANEDA@TRANSCOM.COM";//"MARVIN.REGALADO@TRANSCOM.COM"; //
            //string cim_num = "10113944";//"95759"; //"10113944";  // ds.Tables[0].Rows[0][0].ToString();
            //int intcim = Convert.ToInt32(cim_num);//95759;//10113944;  //Convert.ToInt32(cim_num);
            //string gmail = emp_email;// ds.Tables[0].Rows[0][13].ToString();
            //testing purposes for sir bart's access 


            //DataSet ds_kpi_hierarchy = DataHelper.gettopkpis(intcim);
            //if (ds_kpi_hierarchy.Tables[0].Rows.Count > 0)
            //{
            //    for (int ctr = 0; ctr < ds_kpi_hierarchy.Tables[0].Rows.Count; ctr++)
            //    {
            //if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "1")
            //   {
            //       lbl_1stkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       btn_top1kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       lbl_1stkpi_coaching_frequency.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //   }
            //   else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "2")
            //   {
            //       lbl_2ndkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       lbl_2ndkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //       btn_top2kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //   }

            //   else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "3")
            //   {
            //       lbl_3rdkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       lbl_3rdkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //       btn_top3kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //   }
            //   else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "4")
            //   {
            //       lbl_4thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       lbl_4thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //       btn_top4kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //   }
            //   else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "5")
            //   {
            //       lbl_5thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //       lbl_5thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //       btn_top5kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //   }
            //    }
            //}

            //DataSet ds_kpi_hierarchy = DataHelper.gettopkpis(intcim);
            //if (ds_kpi_hierarchy.Tables[0].Rows.Count > 0)
            //{
            //    for (int ctr = 0; ctr < ds_kpi_hierarchy.Tables[0].Rows.Count; ctr++)
            //    {
            //        lbl_1stkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        btn_top1kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        lbl_1stkpi_coaching_frequency.Text = "Average " + "Score"; // Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " +
            //    }
            //}


            //lbl_2ndkpi_actualvstarget.Text = "Top 2 ";
            //btn_top2kpi.Text = "Top 2 "; 
            //lbl_2ndkpi_averagecoachingscore.Text = "Average " + "Top 2 " + " " + "Score";

            //lbl_3rdkpi_actualvstarget.Text = "Top 3 ";
            //lbl_3rdkpi_averagecoachingscore.Text = "Average " + "Top 3 "  + "Score";
            //btn_top3kpi.Text = "Top 3 ";

            //lbl_4thkpi_actualvstarget.Text = "Top 4 ";
            //lbl_4thkpi_averagecoachingscore.Text = "Average " + "Top 4 " + " " + "Score";
            //btn_top4kpi.Text = "Top 4 ";

            //lbl_5thkpi_actualvstarget.Text = "Top 5 ";
            //lbl_5thkpi_averagecoachingscore.Text = "Average " + "Top 5 " +   "Score";
            //btn_top5kpi.Text = "Top 5 ";

            //Charts
            MyCIM.Value = Convert.ToString(intcim);
            DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
            int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
            DataAccess ws = new DataAccess();
            DataSet ds_KPIforAgent = DataHelper.getkpisforVP(intcim);//ws.GetKPIListPerAcct(Convert.ToString(acctno));

            StartDate.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            EndDate.SelectedDate = DateTime.Now;
           

            //overall behaviordropdown
            dp_behavior_start.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behavior_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior1.DataSource = ds_KPIforAgent;
            cb_kpi_behavior1.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior1.DataBind();
            }

            //behavior vs kpi
            dp_behaviorkpi_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_behaviorkpi_end.SelectedDate = DateTime.Now;
            
            dp_ahtglide_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_ahtglide_end.SelectedDate = DateTime.Now;

            



            DataSet ds_Dirlist = DataHelper.GetSubordinatesforchartsVP(intcim, 0); //.GetTLAgents(intcim, acctno);
            cb_overallDir.DataSource = ds_Dirlist;
            cb_overallDir.DataBind();

            cb_overallbDir.DataSource = ds_Dirlist;
            cb_overallbDir.DataBind();

            //cb_overallbOM.DataSource = ds_Dirlist;
            //cb_overallbOM.DataBind();

            //cb_BvsKPIDir.DataSource = ds_Dirlist;
            //cb_BvsKPIDir.DataBind();
          
            cb_ahtavtfOM.DataSource = ds_Dirlist;
            cb_ahtavtfOM.DataBind();
            DataSet ds_DVlist = DataHelper.GetDataView();
            cb_overalldataview.DataSource = ds_DVlist;
            cb_overalldataview.DataBind();

            cb_overallbdataview.DataSource = ds_DVlist;
            cb_overallbdataview.DataBind();

            
            //cb_top1scoredataview.DataSource = ds_DVlist;
            //cb_top1scoredataview.DataBind();
            cb_top1avtdataview.DataSource = ds_DVlist;
            cb_top1avtdataview.DataBind();


            DataSet ds_sitelist = DataHelper.GetSiteList1(intcim);
            cb_overallSite.DataSource = ds_sitelist;
            cb_overallSite.DataBind();


            cb_overallbehaviorsite.DataSource = ds_sitelist;
            cb_overallbehaviorsite.DataBind();

            cb_overallbvsksite.DataSource = ds_sitelist;
            cb_overallbvsksite.DataBind();

            cb_top1avsttop1_site.DataSource = ds_sitelist;
            cb_top1avsttop1_site.DataBind();


            DataSet ds_overall_lob = DataHelper.GetLOBList1(Convert.ToInt32(intcim));
            cb_selectLOB.DataSource = ds_overall_lob;
            cb_selectLOB.DataBind();


            DataSet ds_kpilist = DataHelper.getkpisforVP(Convert.ToInt32(intcim));

            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    var oldParent = "";
            //    var ParentId = dr["accountid"].ToString();

            //    oldParent = ParentId;

            //    if (oldParent == ParentId)
            //    {

            //    }

            //}
            
            //cb_kpioverall.DataSource = ds_kpilist;
            //cb_kpioverall.DataValueField = "KPIID";
            //cb_kpioverall.DataTextField = "KPIName";
            //cb_kpioverall.DataBind();

            cb_overallbvskpi.DataSource = ds_kpilist;
            cb_overallbvskpi.DataBind();

            cb_top1avsttop1_kpi.DataSource = ds_kpilist;
            cb_top1avsttop1_kpi.DataBind();


            cb_ahtavtfDir.DataSource = ds_Dirlist;
            cb_ahtavtfDir.DataBind();

        }

    }
}