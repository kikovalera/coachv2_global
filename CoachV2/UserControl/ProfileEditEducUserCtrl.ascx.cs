﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using Telerik.Web;
using System.Collections;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class ProfileEditEducUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEduc();

                //LoadSchoolType();
            }
        }

      

        private void LoadEduc()
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            EducGrid.DataSource = DataHelper.GetEduc(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));
            EducGrid.DataBind();
        }


        /*private void LoadSchoolType()
        {
            DDSchoolTypeID.Items.Clear();
            DDSchoolTypeID.DataSource = DataHelper.GetSchoolTypes();
            DDSchoolTypeID.DataTextField = "Description";
            DDSchoolTypeID.DataValueField = "SchoolTypeId";
            DDSchoolTypeID.DataBind();
        }*/

        

        protected void EditLink_Click(object sender, EventArgs e)
        {
            
        }

        /*protected void BtnSaveEduc_Click(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            bool IsGraduated = RadioYes.Checked == true ? true : false;

            DataHelper.InsertUserEduc(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"])
                , TxtSchoolName.Text
                , TxtCourse.Text
                , Convert.ToInt32(DDSchoolTypeID.SelectedValue)
                , TxtFrom.Text
                , TxtTo.Text
                , TxtHonors.Text
                , IsGraduated
                , DateTime.Now
                , DateTime.Now
                , true);

            //LoadEduc();
        }*/


        protected void EducGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    EducGrid.DataSource = DataHelper.GetEduc(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));
                    
                }
                catch (Exception ex)
                {
                    EducGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void EducGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditableItem editItem = e.Item as GridEditableItem;
                RadComboBox cboST = (RadComboBox)editItem.FindControl("cboST");
                DropDownList cboGraduated = (DropDownList)editItem.FindControl("DDGraduated");
                RadMonthYearPicker rd = (RadMonthYearPicker)editItem.FindControl("TxtStart");
                RadMonthYearPicker rd2 = (RadMonthYearPicker)editItem.FindControl("TxtEnd");

                rd.MaxDate = DateTime.Today;
                rd2.MaxDate = DateTime.Today;

                cboST.DataSource = DataHelper.GetSchoolTypes();
               

                Label lblName = editItem.FindControl("SchoolTypeIDVal") as Label;
                cboST.SelectedValue = lblName.Text;
                cboST.DataBind();

                Label lblStart = editItem.FindControl("SchoolStartDate") as Label;

                rd.SelectedDate = lblStart.Text != null && lblStart.Text != "" ? Convert.ToDateTime(lblStart.Text) : DateTime.Now;

                Label lblEnd = editItem.FindControl("SchoolEndDate") as Label;

                rd2.SelectedDate = lblEnd.Text != null && lblEnd.Text != "" ? Convert.ToDateTime(lblEnd.Text) : DateTime.Now;

                Label lblGraduated = editItem.FindControl("LblGraduated") as Label;

                string sel = "";

                if (lblGraduated.Text != "" && Convert.ToBoolean(lblGraduated.Text) == true)
                    sel = "1";

                if (lblGraduated.Text != "" && Convert.ToBoolean(lblGraduated.Text) == false)
                    sel = "0";

                cboGraduated.SelectedValue = sel;
            
            }
        }



        protected void EducGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            GridEditableItem editableItem = ((GridEditableItem)e.Item);
            Hashtable newValues = new Hashtable();
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

            //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
            try
            {
                DateTime? selectedDateStart = ((RadMonthYearPicker)editableItem["StartDate"].FindControl("TxtStart")).SelectedDate;
                DateTime? selectedDateEnd = ((RadMonthYearPicker)editableItem["EndDate"].FindControl("TxtEnd")).SelectedDate;


                DataHelper.InsertUserEduc(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"])
                , ((TextBox)editableItem["School"].FindControl("TxtSchoolName")).Text
                , ((TextBox)editableItem["Course"].FindControl("TxtCourse")).Text
                , Convert.ToInt32(((RadComboBox)editableItem["SchoolTypeID"].FindControl("cboST")).SelectedValue)
                , Convert.ToDateTime(selectedDateStart.Value.ToString("yyyy-MM-dd"))
                , Convert.ToDateTime(selectedDateEnd.Value.ToString("yyyy-MM-dd"))
                , ((TextBox)editableItem["Honors"].FindControl("TxtHonors")).Text
                , Convert.ToInt32(((DropDownList)editableItem["Graduated"].FindControl("DDGraduated")).SelectedValue) == 1 ? true : false
                , DateTime.Now
                , DateTime.Now
                , false);

                EducGrid.Controls.Add(new LiteralControl("Record has been inserted"));

            }
            catch (Exception ex)
            {
                EducGrid.Controls.Add(new LiteralControl(string.Format("Error Inserting Record: {0}", ex.Message)));
            }
        }

        protected void EducGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                DataHelper.DeleteEduc(
                    Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("EducBgID"))
                    );
                EducGrid.Controls.Add(new LiteralControl("Record has been deleted"));
            }
            catch (Exception ex)
            {
                EducGrid.Controls.Add(new LiteralControl(string.Format("Error deleting record: {0}", ex.Message)));
            }
        }

        protected void EducGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {

                GridEditableItem editableItem = ((GridEditableItem)e.Item);
                GridEditManager editMan = editableItem.EditManager;
                int EducBgID = Convert.ToInt32(editableItem.GetDataKeyValue("EducBgID"));
                Hashtable newValues = new Hashtable();

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                try
                {
                    DateTime? selectedDateStart = ((RadMonthYearPicker)editableItem.FindControl("TxtStart")).SelectedDate;
                    DateTime? selectedDateEnd = ((RadMonthYearPicker)editableItem.FindControl("TxtEnd")).SelectedDate;

                    DataHelper.UpdateUserEduc(EducBgID
                    , ((TextBox)editableItem.FindControl("TxtSchoolName")).Text
                    , ((TextBox)editableItem.FindControl("TxtCourse")).Text
                    , Convert.ToInt32(((RadComboBox)editableItem.FindControl("cboST")).SelectedValue)
                    , Convert.ToDateTime(selectedDateStart.Value.ToString("yyyy-MM-dd"))
                    , Convert.ToDateTime(selectedDateEnd.Value.ToString("yyyy-MM-dd"))
                    , ((TextBox)editableItem.FindControl("TxtHonors")).Text
                    , Convert.ToInt32(((DropDownList)editableItem.FindControl("DDGraduated")).SelectedValue) == 1 ? true : false
                    , false);

                    EducGrid.Controls.Add(new LiteralControl("Record has been updated"));

                }
                catch (Exception ex)
                {
                    EducGrid.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
                }
            }

        }

        protected void EducGrid_ItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            GridEditableItem editableItem = ((GridEditableItem)e.Item);
            Hashtable newValues = new Hashtable();
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

            //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
            try
            {
                DateTime? selectedDateStart = ((RadMonthYearPicker)editableItem["StartDate"].FindControl("TxtStart")).SelectedDate;
                DateTime? selectedDateEnd = ((RadMonthYearPicker)editableItem["EndDate"].FindControl("TxtEnd")).SelectedDate;
                int EducBgID = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("EducBgID"));

                DataHelper.UpdateUserEduc(EducBgID
                , ((TextBox)editableItem["School"].FindControl("TxtSchoolName")).Text
                , ((TextBox)editableItem["Course"].FindControl("TxtCourse")).Text
                , Convert.ToInt32(((RadComboBox)editableItem["SchoolTypeID"].FindControl("cboST")).SelectedValue)
                , Convert.ToDateTime(selectedDateStart.Value.ToString("yyyy-MM-dd"))
                , Convert.ToDateTime(selectedDateEnd.Value.ToString("yyyy-MM-dd"))
                , ((TextBox)editableItem["Honors"].FindControl("TxtHonors")).Text
                , Convert.ToInt32(((DropDownList)editableItem["Graduated"].FindControl("DDGraduated")).SelectedValue) == 1 ? true : false
                , false);

                EducGrid.Controls.Add(new LiteralControl("Record has been updated"));

            }
            catch (Exception ex)
            {
                EducGrid.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
            }
        }

        protected void EducGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (!e.Item.OwnerTableView.IsItemInserted)
                {
                    Button updateButton = (Button)e.Item.FindControl("UpdateButton");
                    updateButton.Text = "Update";
                }
                else
                {
                    Button insertButton = (Button)e.Item.FindControl("PerformInsertButton");
                    insertButton.Text = "Save";
                }
                Button cancelButton = (Button)e.Item.FindControl("CancelButton");
                cancelButton.Text = "Cancel";
            }
        }

        protected void EducGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ExportToExcel" || e.CommandName == "ExportToCsv")
                {
                    DataSet ds1 = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    var ds = new DataSet();

                    DataAccess ws = new DataAccess();
                    ds = ws.GetProfileDetails(Convert.ToInt32(ds1.Tables[0].Rows[0]["UserInfoId"]), "EDUC");


                    if (e.CommandName == "ExportToExcel")
                    {
                        ExcelHelperUpdated.ToExcel(ds, "Education " + DateTime.Now + ".xls", Page.Response);

                    }
                    else
                    {
                        if (ds.Tables.Count > 1)
                        {
                            CSVHelper.ConvertToCSV(ds, "Education " + DateTime.Now + ".csv", Page.Response);
                        }
                        else
                        {
                            CSVHelper.ConvertToCSV(ds.Tables[0], "Education " + DateTime.Now + ".csv", Page.Response);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "EducGrid_ItemCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);      

            }
        }        
        
    }
}