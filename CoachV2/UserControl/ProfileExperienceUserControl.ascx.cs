﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

namespace CoachV2.UserControl
{
    public partial class ProfileExperienceUserControl : System.Web.UI.UserControl
    {
        string cim_num;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["tab"] != "" && Request.QueryString["tab"] == "profile")
                fake_title.Visible = false;


            if (Page.Request.QueryString["CIM"] != null)
            {
                cim_num = Page.Request.QueryString["CIM"];
                RadLinkButton1.Visible = false;
            }

            else
            {
                DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

                DataSet ds = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

                if (ds.Tables[0].Rows.Count == 0)
                    RadLinkButton1.Visible = false;
            }
            
        }

        protected void ExpGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    DataSet ds2 = DataHelper.GetUserInfoLocalViaCIMNo(cim_num);

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        ExpGrid.DataSource = DataHelper.GetExp(Convert.ToInt32(ds2.Tables[0].Rows[0]["UserInfoId"]));
                        ExpGrid.DataBind();
                    }

                }
                catch (Exception ex)
                {
                    ExpGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void ExpGrid_PreRender(object sender, EventArgs e)
        {
            DataSet ds2 = DataHelper.GetUserInfoLocalViaCIMNo(cim_num);

            if (ds2.Tables[0].Rows.Count > 0)
            {
                ExpGrid.DataSource = DataHelper.GetExp(Convert.ToInt32(ds2.Tables[0].Rows[0]["UserInfoId"]));
                ExpGrid.DataBind();
            }
            else
            {
                ExpGrid.Controls.Add(new LiteralControl("No record found"));
            }
        }


        
    }
}