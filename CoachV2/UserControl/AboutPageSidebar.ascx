﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutPageSidebar.ascx.cs" Inherits="CoachV2.UserControl.AboutPageSidebar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div class="menu-content">
    <div class="panel panel-default">
        <div class="panel-body">
           <span class="fa fa-question-circle" style="font-size:30px;"> </span> &nbsp;About</div>
    </div>
    
    <asp:LinkButton runat="server" CssClass="list-group-item" ID="btn_cv2" OnClick="btn_coachv2_OnClick">  <span class="fa fa-comments" style="font-size:20px;"></span>  The Coach v2 </asp:LinkButton>
    <asp:LinkButton runat="server" CssClass="list-group-item" ID="btn_ug2" Visible="false" OnClick="btn_userguide_OnClick">  <span class="material-icons" style="font-size:20px;">directions</span>  Userguide </asp:LinkButton>
    <asp:LinkButton runat="server" CssClass="list-group-item" ID="btn_ref2" Visible="false" OnClick="btn_ref2_OnClick">  <span class="glyphicon glyphicon-tasks" style="font-size:20px;"></span>  References </asp:LinkButton>
</div>
