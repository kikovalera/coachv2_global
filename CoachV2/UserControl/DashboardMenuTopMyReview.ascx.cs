﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CoachV2.UserControl
{
    public partial class DashboardMenuTop : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var mark1 = 0;
            var mark2 = 0;
            if (!IsPostBack)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString(); //
                int intcim = Convert.ToInt32(cim_num); 
                DataSet ds_userselect = DataHelper.GetAllUserRoles(intcim);
                if (ds_userselect.Tables[0].Rows.Count > 0)
                {
                    DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(intcim, 1);
                    mark1 = 1;

                    DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(intcim, 2);
                    mark2 = 1;
                }



                if ((mark1 == 1) && (mark2 == 1))
                {  //if has subordinates
                    DataSet DS_GETSUB = DataHelper.Get_Subordinates(intcim);
                    if (DS_GETSUB.Tables[0].Rows.Count > 0)
                    {
                        btn_AddNewReview.Visible = true;
                    }
                }
                else if ((mark1 == 0) && (mark2 == 1)) {

                    btn_AddNewReview.Visible = true;
                }
                else if ((mark1 == 1) && (mark2 == 0))
                {

                    btn_AddNewReview.Visible = false;
                }
                
            }

        }

        protected void btn_AddNewReview_onClick(object sender, EventArgs e)
        {
            Response.Redirect("~/AddReview.aspx");
        }
    }
}