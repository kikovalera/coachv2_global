﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
namespace CoachV2.UserControl
{
    public partial class ProfileEditMainUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                DataSet ds4 = DataHelper.GetUserDetails(Convert.ToInt32(ds.Tables[0].Rows[0]["CIMNo"].ToString()));
                TxtCIM.Text = ds.Tables[0].Rows[0]["CIMNo"].ToString();
                TxtFirstName.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                TxtLastName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                TxtDepartment.Text = ds.Tables[0].Rows[0]["Department"].ToString();
                TxtCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString();
                TxtReportingManager.Text = ds.Tables[0].Rows[0]["ReportingTo"].ToString();

                TxtRegion.Text = ds4.Tables[0].Rows[0]["RegionName"].ToString();// ds.Tables[0].Rows[0]["Region"].ToString();
                //TxtSite.Text = ds.Tables[0].Rows[0]["Site"].ToString();
                TxtSite.Text = ds4.Tables[0].Rows[0]["Site"].ToString();
                TxtClient.Text = ds.Tables[0].Rows[0]["Client"].ToString();
                TxtRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
                TxtCampaign.Text = ds.Tables[0].Rows[0]["Campaign"].ToString();

                RadioMale.Checked = Convert.ToInt32(ds.Tables[0].Rows[0]["Gender"]) == 1 ? true : false;
                RadioFemale.Checked = Convert.ToInt32(ds.Tables[0].Rows[0]["Gender"]) == 2 ? true : false;
                DPBday.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateOfBirth"]);
                DPTranscomStartDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateStartTranscom"]);

                TxtMobile.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
                TxtLandline.Text = ds.Tables[0].Rows[0]["LandLineNo"].ToString();
                TxtTranscomEmail.Text = ds.Tables[0].Rows[0]["TranscomEmail"].ToString().ToLower();
                TxtPersonalEmail.Text = ds.Tables[0].Rows[0]["PersonalEmail"].ToString().ToLower();
                TxtPrimaryLanguage.Text = ds.Tables[0].Rows[0]["PrimaryLanguage"].ToString();

                RadioYes.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Inactive"]) == true ? true : false;
                RadioNo.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Inactive"]) == false ? true : false;

                

                
            }
             
        }

        protected void BtnSaveInfo_Click(object sender, EventArgs e)
        {
            int Gender = RadioMale.Checked == true ? 1 : 2;
            bool IsInactive = RadioYes.Checked == true ? true : false;

            MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string userId = mu.ProviderUserKey.ToString();

            DataHelper.UpdateUserInfo(userId, TxtCIM.Text, TxtFirstName.Text, TxtLastName.Text,
                "", TxtReportingManager.Text, Gender, Convert.ToDateTime(DPBday.SelectedDate), Convert.ToDateTime(DPTranscomStartDate.SelectedDate),
                TxtPrimaryLanguage.Text, TxtMobile.Text, TxtLandline.Text, TxtTranscomEmail.Text,
                TxtPersonalEmail.Text, TxtRole.Text, TxtSite.Text, TxtRegion.Text, TxtCountry.Text,
                TxtDepartment.Text, TxtClient.Text, TxtCampaign.Text, DateTime.Now, IsInactive);
        }
    }
}