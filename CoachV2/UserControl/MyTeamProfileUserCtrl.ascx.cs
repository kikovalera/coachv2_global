﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;

namespace CoachV2.UserControl
{
    public partial class MyTeamProfileUserCtrl : System.Web.UI.UserControl
    {
        //DataSet ds2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string CIM = Convert.ToString(HttpContext.Current.Request.QueryString["CIM"]);

                DataSet ds = DataHelper.GetUserInfoViaCIMNo(CIM);
               
                LblFullName2.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
            }
        }

    }
}