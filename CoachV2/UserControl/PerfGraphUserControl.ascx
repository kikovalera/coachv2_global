﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PerfGraphUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.PerfGraphUserControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="form-horizontal col-md-12" role="form">
    <button type="button" class="btn btn-custom" id="check_graph">
        Overall</button>
    <button type="button" class="btn btn-custom" id="check_graph_aht">
        <%-- AHT--%>
        <asp:Label ID="btn_top1kpi" runat="server"> </asp:Label>
    </button>
    <button type="button" class="btn btn-custom" id="check_graph_voc">
        <%--VOC--%>
        <asp:Label ID="btn_top2kpi" runat="server"> </asp:Label>
    </button>
    <button type="button" class="btn btn-custom" id="check_graph_fcr">
        <%--FCR--%>
        <asp:Label ID="btn_top3kpi" runat="server"> </asp:Label>
    </button>
    <button type="button" class="btn btn-custom" id="check_graph_top4">
        <asp:Label ID="btn_top4kpi" runat="server"> </asp:Label>
    </button>
    <button type="button" class="btn btn-custom" id="check_graph_top5">
        <asp:Label ID="btn_top5kpi" runat="server"> </asp:Label>
    </button>
    <br /> <br />
        <%--<span class="graph-label">Agent Coaching Frequency</span> <hr />--%>
        <span class="graph-label">Coaching Frequency</span> <hr />
        <%--removed AGENT in label to eliminate confusion in display (francis.valera/07132018--%>
    <div class="form-group">
        <!--<label class="col-sm-12">Phone number</label>-->
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Check the following field(s):" ValidationGroup="SearchCriteria" ShowMessageBox="true" ShowSummary="false" />
        
        <div class="col-sm-3">
            <div class="help">
                From</div>
            <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                DateInput-DateFormat="yyyy-MM-dd" Calendar-ClientEvents-OnDateSelected="OnDateSelected">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Start date is required"
                            ControlToValidate="StartDate" ValidationGroup="SearchCriteria" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
            <asp:HiddenField ID="FakeURLID" runat="server" />
        </div>
        <div class="col-sm-3">
            <div class="help">
                To</div>
            <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"
                Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="reqDPEndDate" runat="server" ErrorMessage="End date is required"
                            ControlToValidate="EndDate" ValidationGroup="SearchCriteria" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="dateCompareValidator2"
            runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
            Type="Date" ErrorMessage="The end date must be after the first one. " Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
        </asp:CompareValidator>
        </div>
        <div class="col-sm-3" style="display: none;">
            <div class="help">
                KPI</div>
            <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                <asp:ListItem Value="0" Text="All" />
            </asp:DropDownList>
        </div>
        <asp:HiddenField ID="MyCIM" runat="server" />
        <div class="col-sm-1">
            <div class="help">
                &nbsp;&nbsp;&nbsp;</div>
        </div>
    </div>
</div>
<asp:Label runat="server" ID="fakerr"></asp:Label>

<div id="placeholder" class="col-xs-12 demo-placeholder" style="width: 100%; height: 400px;">
</div>
<%--test for local--%>
<%--<script type="text/javascript" src="<%# "http://"+ HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) %>/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<%# "http://"+ HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) %>/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<%# "http://"+ HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) %>/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="<%# "http://"+ HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) %>/libs/highcharts/js/modules/exporting.js"></script>
--%>
<%--test for local--%>

<%--test for test site  comment this if for local only --%>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/exporting.js"></script>
<%--test for test site --%>


<script type="text/javascript">



    function OnDateSelected(sender, args) {
        // $("#check_graph").removeAttr("disabled");
    }

    function OnDateSelected2(sender, args) {
        //$("#check_graph").removeAttr("disabled");
    }
    //test local 
    //var URL = $("#<%= FakeURLID.ClientID %>").val();
    //test local

    //test site // comment this if for local only
    // var URL = "http://q9vmdevapp06.nucomm.net/Coach2";
    //test site // comment this if for local only



    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML
    if (txtValue1 == "") {

        $('#check_graph_aht').hide(); //button
    }
    else {
        $('#check_graph_aht').show(); //button
    }


    var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML;
    if (txtValue2 == "") {
        $('#check_graph_voc').hide(); //button
    } else {
        $('#check_graph_voc').show(); //button
    }



    var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
    if (txtValue3 == "") {
        $('#check_graph_fcr').hide(); //button

    } else {
        $('#check_graph_fcr').show(); //button
    }


    var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML
    if (txtValue4 == "") { 
        $('#check_graph_top4').hide(); //button

    } else { 
        $('#check_graph_top4').show(); //button
    }

    var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
    if (txtValue5 == "") { 
        $('#check_graph_top5').hide(); //button

    } else { 
        $('#check_graph_top5').show(); //button
    }

    var fplot = function (e, data, options) {
        var jqParent, jqHidden;
        if (e.offsetWidth <= 0 || e.offetHeight <= 0) {
            // lets attempt to compensate for an ancestor with display:none
            jqParent = $(e).parent();
            jqHidden = $("<div style='visibility:hidden'></div>");
            $('body').append(jqHidden);
            jqHidden.append(e);
        }

        var plot = $.plot(e, data, options);

        // if we moved it above, lets put it back
        if (jqParent) {
            jqParent.append(e);
            jqHidden.remove();
        }

        return plot;
    };

    //var data = [];

    $("#err-msg").hide();


    $("#check_graph").on("click", function () {
        //alert(URL);
        var data = [];
        var data2 = [];
        //$(".graph-label").html("Agent Coaching Frequency");
        $(".graph-label").html("Coaching Frequency");
        //removed AGENT in label to eliminate confusion in display (francis.valera/07132018)

        $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
        $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

        var obj;
        var c = [];

        var i = 0;

        var xxxx;
        var dataString = {};
        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        //var KPIid = $("#<%= ddKPIList2.ClientID %>").val();
        var KPIid = "0";
        var URL = $("#<%= FakeURLID.ClientID %>").val();

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getoverallDataformyteam",
            //getData",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
//                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);
                    //data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('placeholder', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: null,
                    /*title: {
                    text: 'Overall Coaching Frequency',
                    align: 'left',
                    style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                    }
                    },*/
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: ''
                        },
                        color: '#FF00FF'
                    },
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 5,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            },
                            fillOpacity: 1,
                            fillColor: '#289CCC'
                        }
                    },
                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'Coaching Frequency',
                        data: data,
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });
    });

    $("#check_graph_voc").on("click", function () {
        var data = [];
        var data2 = [];
        var data3 = [];
        var data4 = [];

        var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML
        var txtheader = "Coaching Frequency Vs. " + txtValue3
        $(".graph-label").html(txtheader);
        //        $(".graph-label").html("Coaching Frequency Vs. VOC Score");

        $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
        $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

        var obj;
        var c = [];

        var i = 0;

        var xxxx;
        var dataString = {};
        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date

        var URL = $("#<%= FakeURLID.ClientID %>").val();
        var KPIid = "2";
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getoverallDataformyteam",
            //getData",
            //getDataAHT",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);
                    data2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('placeholder', {
                    chart: {
                        zoomType: 'xy'
                    },
                    title: null,
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: txtValue1 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'TOTAL ' + txtValue1 + ' Coaching',
                    data: data,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText);
        }
    });
});

$("#check_graph_fcr").on("click", function () {
    var data = [];
    var data2 = [];
    var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML
    var txtheader = "Coaching Frequency Vs. " + txtValue3
    //$(".graph-label").html("Coaching Frequency Vs. FCR Score");
    $(".graph-label").html(txtheader);
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    var obj;
    var c = [];

    var i = 0;

    var xxxx;
    var dataString = {};
    var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
    var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
    var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date

    var URL = $("#<%= FakeURLID.ClientID %>").val();
    var KPIid = "3";
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getoverallDataformyteam",
        //getData",
        //getDataAHT",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {

                c = f.TargetCount;
                data.push([f.ADate, f.CoachCount]);
                data2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('placeholder', {
                chart: {
                    zoomType: 'xy'
                },
                title: null,
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue3 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue3 + ' Coaching',
                data: data,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue3,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(XMLHttpRequest.responseText);
    }
});
});

//4th

$("#check_graph_top4").on("click", function () {
    var data = [];
    var data2 = [];
    var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML
    var txtheader = "Coaching Frequency Vs. " + txtValue3
    //$(".graph-label").html("Coaching Frequency Vs. FCR Score");
    $(".graph-label").html(txtheader);
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    var obj;
    var c = [];

    var i = 0;

    var xxxx;
    var dataString = {};
    var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
    var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
    var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date

    var URL = $("#<%= FakeURLID.ClientID %>").val();
    var KPIid = "4";
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getoverallDataformyteam",
        //getData",
        //getDataAHT",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {

                c = f.TargetCount;
                data.push([f.ADate, f.CoachCount]);
                data2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('placeholder', {
                chart: {
                    zoomType: 'xy'
                },
                title: null,
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue4 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue4 + ' Coaching',
                data: data,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue4,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(XMLHttpRequest.responseText);
    }
});
});

//4th

// 5th

$("#check_graph_top5").on("click", function () {
    var data = [];
    var data2 = [];
    var txtValue3 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
    var txtheader = "Coaching Frequency Vs. " + txtValue3
    //$(".graph-label").html("Coaching Frequency Vs. FCR Score");
    $(".graph-label").html(txtheader);
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    var obj;
    var c = [];

    var i = 0;

    var xxxx;
    var dataString = {};
    var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
    var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
    var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date

    var URL = $("#<%= FakeURLID.ClientID %>").val();
    var KPIid = "5";
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getoverallDataformyteam",
        //getData",
        //getDataAHT",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {

                c = f.TargetCount;
                data.push([f.ADate, f.CoachCount]);
                data2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('placeholder', {
                chart: {
                    zoomType: 'xy'
                },
                title: null,
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue1 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue1 + ' Coaching',
                data: data,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(XMLHttpRequest.responseText);
    }
});
});
// 5th
$("#check_graph_aht").on("click", function () {
    var data = [];
    var data2 = [];

    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML
    var txtheader = "Coaching Frequency Vs. " + txtValue1
    $(".graph-label").html(txtheader);
    //$(".graph-label").html("Coaching Frequency Vs. AHT");

    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top4").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_top5").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    var obj;
    var c = [];

    var i = 0;

    var xxxx;
    var dataString = {};
    var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
    var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
    var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
    
    var URL = $("#<%= FakeURLID.ClientID %>").val();
    var KPIid = "1";
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getoverallDataformyteam",
        //getData",
        //getDataAHT",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {

                c = f.TargetCount;
                data.push([f.ADate, f.CoachCount]);
                data2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('placeholder', {
                chart: {
                    zoomType: 'xy'
                },
                title: null,
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue1 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue1 + ' Coaching',
                data: data,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(XMLHttpRequest.responseText);
    }
});
});

$(document).ready(function () {
    $("#check_graph").trigger('click');
});
</script>
