﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;

namespace CoachV2.UserControl
{
    public partial class MyTeamPerfUserCtrl : System.Web.UI.UserControl
    {
        string cim_num;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Page.Request.QueryString["CIM"] != null)
                {
                    cim_num = Page.Request.QueryString["CIM"];
                }
                else
                {
                    DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

                    DataSet ds = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

                }


                DataSet ds2 = DataHelper.GetUserInfoViaCIMNo(cim_num);                

                DataSet ds3 = DataHelper.GetUserInfo(ds2.Tables[0].Rows[0]["Email"].ToString());



                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + cim_num));
                string ImgDefault;

                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = "../Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = "../Content/uploads/" + ds2.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds3.Tables[0].Rows[0]["Photo"].ToString();
                }


                ProfImage.ImageUrl = ImgDefault;
                LblFullName2.Text = ds2.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds2.Tables[0].Rows[0]["Last_Name"].ToString();
                LblFullName.Text = ds2.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds2.Tables[0].Rows[0]["Last_Name"].ToString();
                LblDept.Text = ds2.Tables[0].Rows[0]["Department"].ToString();
                LblCountry.Text = ds2.Tables[0].Rows[0]["CountryName"].ToString();
                LblMySupervisor.Text = ds2.Tables[0].Rows[0]["ReportsTo"].ToString();
                LblMyRole.Text = ds2.Tables[0].Rows[0]["Role"].ToString();
                LblSite.Text = ds2.Tables[0].Rows[0]["Sitename"].ToString();
                LblRegion.Text = ds2.Tables[0].Rows[0]["RegionName"].ToString();

                LblClient.Text = ds3.Tables[0].Rows.Count > 0 ? ds3.Tables[0].Rows[0]["Client"].ToString() : "";
                LblCampaign.Text = ds3.Tables[0].Rows.Count > 0 ? ds3.Tables[0].Rows[0]["Campaign"].ToString() : "";

                DateTime DateOfBirth = DateTime.ParseExact(ds2.Tables[0].Rows[0]["Birthday"].ToString(),
                                            "yyyyMMdd",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None);
                DateTime DateStartTranscom = DateTime.ParseExact(ds2.Tables[0].Rows[0]["Start_Date"].ToString(),
                                            "yyyyMMdd",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None);

                

            }
        }
    }
}