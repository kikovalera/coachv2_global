﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HR_DashboardwithTriad.ascx.cs" Inherits="CoachV2.UserControl.HR_DashboardwithTriad" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="grd_HR_ForTermination">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_HR_ForTermination"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_HR_ForFinalWarning">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_HR_ForFinalWarning"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueFollowups">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueFollowups"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueSignOffs">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueSignOffs"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_WeeklyReviewTL">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_WeeklyReviewTL"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

<style type="text/css">
    div.RemoveBorders .rgHeader, div.RemoveBorders th.rgResizeCol, div.RemoveBorders .rgFilterRow td
    {
        border-width: 0 0 1px 0; /*top right bottom left*/
    }
    
    div.RemoveBorders .rgRow td, div.RemoveBorders .rgAltRow td, div.RemoveBorders .rgEditRow td, div.RemoveBorders .rgFooter td
    {
        border-width: 0;
        padding-left: 7px; /*needed for row hovering and selection*/
    }
    
    div.RemoveBorders .rgGroupHeader td, div.RemoveBorders .rgFooter td
    {
        padding-left: 7px;
    }
</style>
<div class="panel menuheadercustom">
    <div>
        &nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard<span class="breadcrumb2ndlevel"><asp:Label
            ID="Label4" runat="server" Text=" "></asp:Label></span></div>
</div>
 <div class="menu-content bg-alt">
<div class="panel-group" id="Div_Termination">
        <div class="row row-custom">
            <div class="col-sm-5">
                <form>
                <div class="input-group">
                </div>
                </form>
            </div>
        </div>
        <div  id="pn_fortermination"  runat="server" visible="false" >
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseForTermination" >
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Terminations<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblForTermination" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseForTermination" class="panel-collapse collapse in" > 
            <div class="panel-body">
            <telerik:RadGrid ID="grd_HR_ForTermination" runat="server" AllowPaging="True" GroupPanelPosition="Top" RenderMode="Auto"
                ResolvedRenderMode="Classic" AutoGenerateColumns="false" AllowFilteringByColumn="false" OnItemDataBound="grd_HR_ForTermination_onItemDatabound"
                CssClass="RemoveBorders" BorderStyle="None"  Visible="true">

                <MasterTableView DataKeyNames="Id" TableLayout="Auto">
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="Id" HeaderText="Coaching Ticket"
                            UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                            Visible="false" Display="false">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="CoacheeName" HeaderText="Name" UniqueName="NameField"
                            FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield"
                            ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn DataField="CoacheeCim" HeaderText="CIM" UniqueName="CimField"
                            FilterControlToolTip="Cimtip" ShowSortIcon="true"  HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="Escalation Date" DataField="CreatedOn"
                            DataType="System.DateTime" DataFormatString="{0:d}" HeaderStyle-Font-Bold="true" />
                        <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                            DataField="TopicName" ShowSortIcon="true" HeaderStyle-Font-Bold="true"  ItemStyle-Width="160px" >
                            <ItemTemplate>
                                <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "TopicName")%>'  Width="100px"  > </asp:Label>
                                <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true" Skin="Office2010Silver">
                                    <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" UniqueName="StatusName"
                            FilterControlToolTip="Datectip" ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CoacherName" HeaderText="Assigned To" UniqueName="AssignedTo"
                            FilterControlToolTip="Datectip" ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"  >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    <Resizing AllowResizeToFit="True" />
                </ClientSettings>
            </telerik:RadGrid>
                </div>
        </div>
        </div>
  <div class="panel-group" id="Div_forfinalwarning">
        <div class="row row-custom">
            <div class="col-sm-5">
                <form>
                <div class="input-group">
                </div>
                </form>
            </div>
        </div>
        <div id="pn_forfinalwarning" runat="server" visible="false" >
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseForFinalWarning">
                <div class="panel-heading">
                    <h4 class="panel-title">
                          For Final Warning<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblForFinalWarning" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
  
        <div id="collapseForFinalWarning" class="panel-collapse collapse in"  >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_HR_ForFinalWarning" runat="server" AllowPaging="True" GroupPanelPosition="Top"
                ResolvedRenderMode="Classic" AutoGenerateColumns="false" AllowFilteringByColumn="false"  RenderMode="Auto"
                CssClass="RemoveBorders" BorderStyle="None" OnItemDataBound="grd_HR_ForFinalWarning_onItemDatabound" >               
                <MasterTableView DataKeyNames="Id" TableLayout="Auto">
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="Id" HeaderText="Coaching Ticket"
                            UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                            Visible="false" Display="false">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="CoacheeName" HeaderText="Name" UniqueName="NameField"
                            FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield"
                            ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn DataField="CoacheeCim" HeaderText="CIM" UniqueName="CimField"
                            FilterControlToolTip="Cimtip" ShowSortIcon="true"  HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="Escalation Date" DataField="CreatedOn"
                            DataType="System.DateTime" DataFormatString="{0:d}" HeaderStyle-Font-Bold="true" />
                        <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                            DataField="TopicName" ShowSortIcon="true" HeaderStyle-Font-Bold="true"  ItemStyle-Width="160px" >
                            <ItemTemplate>
                                <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "TopicName")%>'  Width="100px"  > </asp:Label>
                                <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true" 
                                    Skin="Office2010Silver"   ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                    <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                     </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" UniqueName="StatusName"
                            FilterControlToolTip="Datectip" ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CoacherName" HeaderText="Assigned To" UniqueName="AssignedTo"
                            FilterControlToolTip="Datectip" ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"  >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    <Resizing AllowResizeToFit="True" />
                </ClientSettings>
            </telerik:RadGrid>

      </div>
    </div>
    </div>

        <div class="panel-group" id="DIV_OVERDUEFOLLOWUPS">
            <div class="row row-custom">
                <div class="col-sm-5">
                    <form>
                    <div class="input-group">
                        </span>
                    </div>
                    </form>
                </div>
            </div>
            <div id="pn_followsup" runat="server" visible="false" > 
            <div class="panel panel-custom" >
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Overdue Follow Ups <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>&nbsp <span class="label label-default">
                                <asp:Label ID="lblNotifOverdueFollowups" runat="server" Visible="true"></asp:Label>
                            </span>
                        </h4>
                    </div>
                </a>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in" >
                <div class="panel-body">
                  <telerik:RadGrid ID="grd_OverdueFollowups" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                          GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"  RenderMode="Auto"
                        OnItemDataBound="grd_OverdueFollowups_onItemDatabound" CssClass="RemoveBorders" BorderStyle="None">
                        <MasterTableView DataKeyNames="Topic"  >
                            <Columns>
                                <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                    UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                    Visible="false" Display="false">
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                    FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield"  
                                    ShowSortIcon="true">
                                </telerik:GridHyperLinkColumn>
                 
                                <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"
                                     >
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                      FilterControlToolTip="Escalationctip">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"  
                                    SortExpression="Topic" DataField="Topic" ShowSortIcon="true"  ItemStyle-Width="160px" >
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                        <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                        <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                            RelativeTo="Element" Position="BottomCenter" ShowCallout="false" 
                                            CssClass="tooltip-inner" RenderInPageRoot="true" Skin="Office2010Silver"
                                              ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                            <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                            <br />
                                        </telerik:RadToolTip>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                     FilterControlToolTip="Reviewtip">
                                </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"  >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            <Resizing AllowResizeToFit="True" />
                        </ClientSettings>
                    </telerik:RadGrid> 
                </div>
      </div>
      </div>

  <div class="panel-group" id="Div_overduesignoffs">
    <div class="row row-custom">
        <div class="col-sm-5">
            <form>
            <div class="input-group">
            </div>
            </form>
        </div>
    </div>
    <div  id="pn_signoffs" runat="server" visible="false" >
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Overdue Sign offs <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                    </span>&nbsp;<span class="label label-default">
                        <asp:Label ID="lblNotifOverdueSignOffs" runat="server" Visible="true"></asp:Label>
                    </span>
                </h4>
            </div>
        </a>
    </div>
    <div id="collapseThree" class="panel-collapse collapse in"  >
        <div class="panel-body">
            <telerik:RadGrid ID="grd_OverdueSignOffs" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                  GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"  RenderMode="Auto"
                OnItemDataBound="grd_OverdueSignOffs_onitemdatabound" CssClass="RemoveBorders" BorderStyle="None" >
                <MasterTableView DataKeyNames="Topic" >
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                            UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                            Visible="false" Display="false">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                            FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield" 
                            ShowSortIcon="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"
                            >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                              FilterControlToolTip="Escalationctip">
                        </telerik:GridBoundColumn>
              <%--          <telerik:GridBoundColumn DataField="Topic" HeaderText="Topic" UniqueName="TopicField"
                              SortExpression="Topic" FilterControlToolTip="Topictip">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                            DataField="Topic" ShowSortIcon="true"  ItemStyle-Width="160px" >
                            <ItemTemplate>
                                <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true" Skin="Office2010Silver"
                                      ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                    <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    <br />
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                              FilterControlToolTip="ReviewFtip">
                        </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"  >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    <Resizing AllowResizeToFit="True" />
                </ClientSettings>
            </telerik:RadGrid> 
        </div>
     </div>
     </div>
<div class="panel-group" id="Div3">
  <div class="row row-custom">
            <div class="col-sm-5">
                <form>
                <div class="input-group">
                    </span>
                </div>
                </form>
            </div>
        </div>
        <div id="pn_weekly" runat="server" visible="false" >
    <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                <div class="panel-heading">
                    <h4 class="panel-title">
                       Weekly Review List<span class="label label-default"></span>
                        <span class="glyphicon glyphicon-triangle-top triangletop">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblWeekly" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
    <div id="collapseFour" class="panel-collapse collapse in" >
        <div class="panel-body">
            <telerik:RadGrid ID="grd_WeeklyReviewTL" runat="server"  RenderMode="Auto" AllowPaging="True" AllowFilteringByColumn="false"
                GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="true"
                CssClass="RemoveBorders" BorderStyle="None">
            </telerik:RadGrid>
        </div>
    </div>
    </div>



    

<%-- charts area --%>
<div class="panel-group" id="Div_TLCHARTS" runat="server" visible="false">     
 <asp:HiddenField ID="FakeURLID" runat="server" /> 
   <div class="row row-custom">
            <div class="col-sm-5">
                <form>
                <div class="input-group">
                    </span>
                </div>
                </form>
            </div>
        </div> 
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTL">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Charts<span class="glyphicon glyphicon-triangle-top triangletop"> </span>&nbsp;
                </h4>
            </div>
        </a>
    </div>
    <div id="collapseTL" class="panel-collapse collapse in">
    <br />
        <div class="form-horizontal col-md-12" role="form">
            <button type="button" class="btn btn-custom" id="check_graph">
                Overall</button>
             <button type="button" class="btn btn-custom" id="check_graph_aht">
                 <asp:Label ID="btn_top1kpi" runat="server"></asp:Label></button>
            <button type="button" class="btn btn-custom" id="check_graph_voc">
                 <asp:Label ID="btn_top2kpi" runat="server"></asp:Label></button>
            <button type="button" class="btn btn-custom" id="check_graph_fcr">
                 <asp:Label ID="btn_top3kpi" runat="server"></asp:Label></button>         
            <button type="button" class="btn btn-custom" id="check_graph_4th">
                <asp:Label ID="btn_top4kpi" runat="server"> </asp:Label></button>
            <button type="button" class="btn btn-custom" id="check_graph_5th">
                <asp:Label ID="btn_top5kpi" runat="server"> </asp:Label></button> 
            <button type="button" class="btn btn-custom" id="check_graph_triad">
                Triad Coaching</button>

        </div>

<div   id="overalldate"  style="width: 100% ">        
        <br />
        <br />
        <br />
<table class="table table-condensed">
            <thead>
                <tr>
                    <th>
                        <font size="4">Overall Coaching Frequency</font>
                    </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="overall" />
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="overall" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3" style="display: none;">
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                        AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                        <asp:ListItem Value="0" Text="All" />
                    </asp:DropDownList>
                </div>
 </div>
<div id="overallfrequency" style="width: 100%; height: 400px;" > 
</div>

<div   id="overall_behaviordates">
        <br />
        <br />
        <br />
        <br />
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>
                        <font size="4">Overall Behavior</font>
                    </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behavior_start" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="overallbehavior"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span> </div>
                    <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="overallbehavior"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3" >
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                     <telerik:RadComboBox  ID="cb_kpi_behavior1" runat="server" DataValueField="KPIID" DataTextField="Name" ResolvedRenderMode="Classic" 
                      OnClientSelectedIndexChanged="overallbehavior" AppendDataBoundItems="true"  >
                     </telerik:RadComboBox>
                </div>
            </div>
<div id="behaviorvsKPI" style="width: 100%; height: 400px; ">
</div>
<div  id="overall_kpivsb" >
        <br />
        <br />
        <br />
        <br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Behavior vs KPI</font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents  OnDateSelected="overallbehaviorvskpi"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="overallbehaviorvskpi"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3" >
                    <div class="help">
                        KPI <span class="glyphicon glyphicon-triangle-bottom"></span></div>
                     <telerik:RadComboBox  ID="cb_kpi_behavior_kpi" runat="server" DataValueField="KPIID" DataTextField="Name" ResolvedRenderMode="Classic"
                      OnClientSelectedIndexChanged="overallbehaviorvskpi"  AppendDataBoundItems="true" >
                     </telerik:RadComboBox>
                </div>
            </div>
<div id="DIV2_kpivsscore" style="width: 100%; height: 400px; ">
</div>

<div  id="aht_dates" >
<br />
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                   <font size="4">Coaching Frequency Vs. <asp:Label ID="lbl_1stkpi_coaching_frequency" runat="server"> </asp:Label>  </font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_aht_start1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="ahtfvss"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_aht_end1" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents  OnDateSelected="ahtfvss"/>
                    </telerik:RadDatePicker>&nbsp; 
                </div>
            </div>
<div id="aht_score" style="width: 100%; height: 400px;  ">
</div>
<div id = "ahtglidedates">
<br />
<br />
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                        <font size="4">   <asp:Label ID="lbl_1stkpi_actualvstarget" runat="server"> </asp:Label>  Glide Path Targets Vs. Actual</font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_ahtglide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents OnDateSelected="ahtavst" />
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_ahtglide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents OnDateSelected="ahtavst" />
                    </telerik:RadDatePicker>&nbsp; 
                </div>
            </div>
<div id="AHTGlide"  style="width: 100%; height: 400px; " >
</div>

<div   id="fcr_dates1" >
<br /> 
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                   <font size="4">Coaching Frequency Vs.   <asp:Label ID="lbl_3rdkpi_averagecoachingscore" runat="server"> </asp:Label>  </font>
                               </th>
                </tr>
            </thead>
        </table>

                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcr_start1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents OnDateSelected="fcrfvss" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcr_end1" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents OnDateSelected="fcrfvss" />
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="DIV2_FCRScore" style="width: 100%; height: 400px; ">
</div>
<div   id="Div2_FCRGlidedates" >
<br /> 
<br />
<br />
    <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                   <font size="4"> <asp:Label ID="lbl_3rdkpi_actualvstarget" runat="server"> </asp:Label> Glide Path Targets Vs. Actual</font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcrglide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="fcravst"/>
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcrglide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"   >
                    <ClientEvents  OnDateSelected="fcravst"/>
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="DIV2_FCRGlide" style="width: 100%; height: 400px; " >
</div>
<div   id="overallvocdate" >
<br /><br /> <br />        
   <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                   <font size="4">Coaching Frequency Vs.   <asp:Label ID="lbl_2ndkpi_averagecoachingscore" runat="server"> </asp:Label>  </font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    <telerik:RadDatePicker ID="dp_start_voc" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents  OnDateSelected="voc_score" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_end_voc" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="voc_score" />
                    </telerik:RadDatePicker> 
                </div>
</div>

<div id="DIV2_VOCScore" style="width: 100%; height: 400px;">
</div>


<div   id="Div3_vocGlidedates" >
<br /><br /> <br />        
   <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                   <asp:Label ID="lbl_2ndkpi_actualvstarget" runat="server"></asp:Label> Glide Path Targets Vs. Actual</font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    <telerik:RadDatePicker ID="RadDatePicker3" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                        <ClientEvents OnDateSelected="voc_glide" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="RadDatePicker4" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                        <ClientEvents OnDateSelected="voc_glide" />
                    </telerik:RadDatePicker> 
                </div>
</div>

<div id="Div3_vocGlidecharts" style="width: 100%; height: 400px;">
</div>
<div   id="triaddates" >
<br /><br /> <br />        
   <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                    <font size="4">Triad Coaching VS Score</font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents  OnDateSelected="triadcoaching"/>
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents  OnDateSelected="triadcoaching"/>
                    </telerik:RadDatePicker> 
                </div>
</div>
<div id="div_triadcoaching" style="width: 100%; height: 400px;">
</div>


<%--top 4 --%>

<div   id="div_top4_fvssdates" >
<br /> 
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4"> <asp:Label ID="lbl_4thkpi_averagecoachingscore" runat="server"></asp:Label> </font>
                               </th>
                </tr>
            </thead>
        </table>

                <div class="col-sm-4">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top4_score_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents OnDateSelected="overalltop4" /> 
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-4">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top4_score_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                   <ClientEvents OnDateSelected="overalltop4" /> 
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="div_top4_fvsschart" style="width: 100%; height: 400px; ">
</div>
 
<br /> 
<br />
<br /> 

<div   id="div_top4_avstdate" >
<br /> 
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">  <asp:Label ID="lbl_4thkpi_actualvstarget" runat="server"></asp:Label> Glide Path Targets Vs. Actual</font>
                               </th>
                </tr>
            </thead>
        </table>
          <div class="col-sm-4">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top4_glide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                    <ClientEvents OnDateSelected="top4glideactual" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-4">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top4_glide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                    <ClientEvents OnDateSelected="top4glideactual" />
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="div_top4_avstchart" style="width: 100%; height: 400px; ">
</div>
 
 
<%--top 4 --%>

<%--top 5--%>


<div   id="div_top5_fvssdates" >
 
 
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">LOB <asp:Label ID="lbl_5thkpi_averagecoachingscore" runat="server"></asp:Label> </font>
                               </th>
                </tr>
            </thead>
        </table>
 
                <div class="col-sm-4">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top5_score_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                   <ClientEvents OnDateSelected="overalltop5" />  
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-4">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top5_score_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                 <ClientEvents OnDateSelected="overalltop5" /> 
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="div_top5_fvsschart" style="width: 100%; height: 400px; ">
</div>
 

<div   id="div_top5_avstdate" >
<br /> 
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">  <asp:Label ID="lbl_5thkpi_actualvstarget" runat="server"></asp:Label> Glide Path Targets Vs. Actual</font>
                               </th>
                </tr>
            </thead>
        </table>
 
                <div class="col-sm-4">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top5_glide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  >
                   <ClientEvents OnDateSelected="top5glideactual" /> 
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-4">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_top5_glide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" >
                     <ClientEvents OnDateSelected="top5glideactual" /> 
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="div_top5_avstchart" style="width: 100%; height: 400px; ">
</div>  
 



<%--top 5--%>
<br />
<div class="form-group">           
            <asp:HiddenField ID="MyCIM" runat="server" />
            <div >
                <div > 
                    <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                        padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>No data found</span>
                    <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                        runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
                        Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                        Style="padding-top: 10px; padding-bottom: 10px;">
                    </asp:CompareValidator>
              </div>
            </div>

       </div>
    </div>
    </div>
         </div>
      </div>
     </div>
   </div>
 </div>
 </div>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/exporting.js"></script>

<script type='text/javascript'>



    function OnDateSelected(sender, args) {
        // $("#check_graph").removeAttr("disabled");
    }

    function OnDateSelected2(sender, args) {
        //$("#check_graph").removeAttr("disabled");
    }
    
     var URL = $("#<%= FakeURLID.ClientID %>").val();
  
  

    var fplot = function (e, data, options) {
        var jqParent, jqHidden;
        if (e.offsetWidth <= 0 || e.offetHeight <= 0) {
            // lets attempt to compensate for an ancestor with display:none
            jqParent = $(e).parent();
            jqHidden = $("<div style='visibility:hidden'></div>");
            $('body').append(jqHidden);
            jqHidden.append(e);
        }

        var plot = $.plot(e, data, options);

        // if we moved it above, lets put it back
        if (jqParent) {
            jqParent.append(e);
            jqHidden.remove();
        }

        return plot;
    };

    $("#err-msg").hide();

    function overall(sender, args) {
        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var data3 = [];
        var data_kpivsbehavior1 = [];
        var data_kpivsbehavior2 = [];
        var data_bvsk1 = [];
        var data_bvsk2 = [];


        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();  
        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()
 

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForHR",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    //c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);
                    //                    data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                });

                Highcharts.chart('overallfrequency', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: ''
                        },
                        color: '#FF00FF'
                    },
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 5,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            },
                            fillOpacity: 1,
                            fillColor: '#289CCC'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Coaching Frequency',
                        data: data,
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                });
            }
        });
    }

    function overallbehavior(sender, args) {

        var data3 = [];

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();  

        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");

        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehavior_ForHR",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data3.push([f.description, f.CoachCount]);
                
                    $("#err-msg").hide();

                });

                Highcharts.chart('behaviorvsKPI', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                    // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                },
                plotOptions: {
                    series: {
                        colorByPoint: true
                    }
                },
                credits: {
                    enabled: false
                },


                series: [{
                    name: 'Overall Behavior',
                    data: data3,
                    type: 'column',
                    yaxis: 0,
                    color: '#2F9473',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                }]
            });
        }
    });
}

function overallbehaviorvskpi(sender, args) {

    var RadDatePickerbkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>");
    var selectedDatebkpi1 = RadDatePickerbkpi1.get_selectedDate().format("yyyy-MM-dd");
    var RadDatePickerbkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>");
    var selectedDatebkpi2 = RadDatePickerbkpi2.get_selectedDate().format("yyyy-MM-dd");
    var kpiidbkpi = $find('<%=cb_kpi_behavior_kpi.ClientID %>');
    var kpiidbkpi1 = kpiidbkpi.get_selectedItem().get_value()
    var data_bvsk1 = [];
    var data_bvsk2 = [];
    var CIMNo = $("#<%= MyCIM.ClientID %>").val();

    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForHR",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {

                data_bvsk1.push([f.descriptions, f.coachedkpis]);
                data_bvsk2.push([f.descriptions, f.AvgCurr]);

                $("#err-msg").hide();

            });
            Highcharts.chart('DIV2_kpivsscore', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: true,
                    title: {
                        text: '',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: true,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },


        series: [{
            name: 'Behavior',
            data: data_bvsk1,
            type: 'column',
            yaxis: 0,
            color: '#27A6D9',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                    }
                }]
            });
        }
    });
}


function ahtfvss(sender, args) {
    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML;
   
    var data = [];
    var data_aht1 = [];
    var data_aht2 = [];
    var data_aht3 = [];
    var data_aht4 = [];
    var RadDatePickeraht1 = $find("<%= dp_aht_start1.ClientID %>");
    var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickeraht2 = $find("<%= dp_aht_end1.ClientID %>");
    var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val();
    var kpiid = 4
    var KPI = 1;
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_AHTScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_aht1.push([f.ADate, f.CoachCount]);
                data_aht2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('aht_score', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue1 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue1 +  ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue1  +' Coaching',
                data: data_aht1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                    }
                }]
            });
        }
    });
    }

function ahtavst(sender, args) {
    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML;
   
    var data_aht3 = [];
    var data_aht4 = [];
    var RadDatePickerahtglide1 = $find("<%= dp_ahtglide_start.ClientID %>");
    var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickerahtglide2 = $find("<%= dp_ahtglide_end.ClientID %>");
    var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val();
    var KPI = 1; //AHT
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
        //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_aht3.push([f.ADate, f.TargetCount]);
                data_aht4.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('AHTGlide', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Target ' + txtValue1,
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue1 ,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue1 + ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'Target ' + txtValue1,
                data: data_aht3,
                type: 'spline',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                    }
                }]
            });
        }
    });
    }

    function fcrfvss(sender, args) {
        var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
   
        var data_fcr1 = [];
        var data_fcr2 = [];
        

        var RadDatePickerfcr1 = $find("<%= dp_fcr_start1.ClientID %>");
        var selectedDatefcr1 = RadDatePickerfcr1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerfcr2 = $find("<%= dp_fcr_end1.ClientID %>");
        var selectedDatefcr2 = RadDatePickerfcr2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var KPI = 3; 

	$.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatefcr1 + "', EndDate: '" + selectedDatefcr2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_FCRScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_fcr1.push([f.ADate, f.CoachCount]);
                    data_fcr2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                 
                Highcharts.chart('DIV2_FCRScore', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },  
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: txtValue3 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: txtValue3 +  ' : <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'TOTAL ' + txtValue3 + ' Coaching',
                        data: data_fcr1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: txtValue3,
                        type: 'spline',
                        yAxis: 1,
                        data: data_fcr2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
    
    
                    }
                }]
            });
        }
    });
    }

      function fcravst(sender, args) {
          var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
   
          var data_fcr3 = [];
          var data_fcr4 = [];

          var RadDatePickerfcrg1 = $find("<%= dp_fcrglide_start.ClientID %>");
          var selectedDatefcrg1 = RadDatePickerfcrg1.get_selectedDate().format("yyyy-MM-dd");

          var RadDatePickerfcrg2 = $find("<%= dp_fcrglide_end.ClientID %>");
          var selectedDatefcrg2 = RadDatePickerfcrg2.get_selectedDate().format("yyyy-MM-dd");

          var CIMNo = $("#<%= MyCIM.ClientID %>").val();

          var KPI = 3; // fcr
          //fcr actual glide
          $.ajax({
              type: "POST",
              data: "{ StartDate: '" + selectedDatefcrg1 + "', EndDate: '" + selectedDatefcrg2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
              contentType: "application/json; charset=utf-8",
              url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
              //url: URL + "/FakeApi.asmx/getDataHR_FCRScore",
              dataType: 'json',
              success: function (msg) {

                  if (msg != null && msg.d == null) {
                      $("#err-msg").show();
                      //fplot($("#placeholder"), [[]], opts);
                  }
                  else {
                      $("#err-msg").hide();
                  }

                  $.each(msg, function (e, f) {


                      data_fcr3.push([f.ADate, f.TargetCount]);
                      data_fcr4.push([f.ADate, f.CurrentCount]);

                      $("#err-msg").hide();

                  });

                  Highcharts.chart('DIV2_FCRGlide', {
                      chart: {
                          type: 'xy',
                          spacingBottom: 30
                      },
                      title: {
                          text: '',
                          align: 'left',
                          style: {
                              // margin: '50px', // does not work for some reasons, see workaround below
                              color: '#707070',
                              fontSize: '20px',
                              fontWeight: 'bold',
                              textTransform: 'none'
                          }
                      },
                      xAxis: {
                          type: "category",
                          color: '#FF00FF'
                      },
                      yAxis: [{
                          allowDecimals: false,
                          title: {
                              text: 'Target ' + txtValue3,
                              style: {
                                  // margin: '50px', // does not work for some reasons, see workaround below
                                  color: '#707070',
                                  fontWeight: 'bold',
                                  textTransform: 'none'
                              }
                          },
                          color: '#FF00FF'

                      },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue3,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                      tooltip: {
                          pointFormat:  txtValue3 + ' : <b>{point.y:,.0f}</b>'
                      },
                      plotOptions: {

                  },
                  credits: {
                      enabled: false
                  },

                  series: [{
                      name: 'Target ' + txtValue3 ,
                      data: data_fcr3,
                      type: 'spline',
                      yaxis: 0,
                      color: '#27A6D9',
                      marker: {
                          fillColor: '#fff',
                          lineWidth: 2,
                          lineColor: '#ccc'
                      }
                  },
                    {
                        name: 'Actual ' + txtValue3,
                        type: 'spline',
                        yAxis: 0,
                        data: data_fcr4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
    
    
                    }
                }]
            });
        }
    });
    }

     function voc_score(sender, args) {
         var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML;
   
         var data_voc1 = [];
         var data_voc2 = [];
         var RadDatePickervoc1 = $find("<%= dp_start_voc.ClientID %>");
         var selectedDatevoc1 = RadDatePickervoc1.get_selectedDate().format("yyyy-MM-dd");

         var RadDatePickervoc2 = $find("<%= dp_end_voc.ClientID %>");
         var selectedDatevoc2 = RadDatePickervoc2.get_selectedDate().format("yyyy-MM-dd");

         var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
         var KPI = 2;
         
         $.ajax({
             type: "POST",
             data: "{ StartDate: '" + selectedDatevoc1 + "', EndDate: '" + selectedDatevoc2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
             contentType: "application/json; charset=utf-8",
             url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_VOCScore",
             // getDataTL_VOCScore
             dataType: 'json',
             success: function (msg) {

                 if (msg != null && msg.d == null) {
                     $("#err-msg").show();
                     //fplot($("#placeholder"), [[]], opts);
                 }
                 else {
                     $("#err-msg").hide();
                 }

                 $.each(msg, function (e, f) {


                     data_voc1.push([f.ADate, f.CoachCount]);
                     data_voc2.push([f.ADate, f.CurrentCount]);

                     $("#err-msg").hide();

                 });
                 Highcharts.chart('DIV2_VOCScore', {
                     chart: {
                         type: 'xy',
                         spacingBottom: 30
                     },
                     title: {
                         text: '',
                         align: 'left',
                         style: {
                             // margin: '50px', // does not work for some reasons, see workaround below
                             color: '#707070',
                             fontSize: '20px',
                             fontWeight: 'bold',
                             textTransform: 'none'
                         }
                     },
                     xAxis: {
                         type: "category",
                         color: '#FF00FF'
                     },
                     yAxis: [{
                         allowDecimals: false,
                         title: {
                             text: 'Total Coaching',
                             style: {
                                 // margin: '50px', // does not work for some reasons, see workaround below
                                 color: '#707070',
                                 fontWeight: 'bold',
                                 textTransform: 'none'
                             }
                         },
                         color: '#FF00FF'

                     },
                    { allowDecimals: false,
                        title: {
                            text:  txtValue2  + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                     tooltip: {
                         pointFormat: txtValue2 + ' : <b>{point.y:,.0f}</b>'
                     },
                     plotOptions: {

                 },
                 credits: {
                     enabled: false
                 },

                 series: [{
                     name: 'TOTAL '  + txtValue2 + ' Coaching',
                     data: data_voc1,
                     type: 'column',
                     yaxis: 0,
                     color: '#2F9473',
                     marker: {
                         fillColor: '#fff',
                         lineWidth: 2,
                         lineColor: '#ccc'
                     }
                 },
                    {
                        name: txtValue2,
                        type: 'spline',
                        yAxis: 1,
                        data: data_voc2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                    }
                }]
            });
        }
     });
    }


    function voc_glide(sender, args) {


        var txtValue1 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML

        var data_aht3 = [];
        var data_aht4 = [];
        var RadDatePickerahtglide1 = $find("<%= RadDatePicker3.ClientID %>");
        var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerahtglide2 = $find("<%= RadDatePicker4.ClientID %>");
        var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var KPI = 2; //AHT
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getGlidepath_QA_bi",
            // url: URL + "/FakeApi.asmx/getDataQA_AHTScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht3.push([f.ADate, f.TargetCount]);
                    data_aht4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                Highcharts.chart('Div3_vocGlidecharts', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Target ' + txtValue1,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    {
                        allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue1,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: txtValue1 + ' : <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Target ' + txtValue1,
                    data: data_aht3,
                    type: 'spline',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'

                        }
                    }]
            });
        }
    });

}
    function triadcoaching(sender, args) { 
    
        var data_triad1 = [];
        var data_triad2 = [];             
           
        var RadDatePickertriad1 = $find("<%= RadDatePicker1.ClientID %>");
        var selectedDatetriad1 = RadDatePickertriad1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickertriad2 = $find("<%= RadDatePicker2.ClientID %>");
        var selectedDatetriad2 = RadDatePickertriad2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatetriad1 + "', EndDate: '" + selectedDatetriad2 + "', CIMNo: '" + CIMNo  + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_TriadCoaching_HR_QA",
            //getData_TriadCoaching",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_triad1.push([f.ADate, f.CoachCount]);
                    data_triad2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('div_triadcoaching', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Triad Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'Triad Coaching Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Total Triad Coaching',
                    data: data_triad1,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: 'Triad Coaching',
                        type: 'spline',
                        yAxis: 1,
                        data: data_triad2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                });
            }
        });
    }

    function overalltop4(sender, args) {
        var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML

        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var RadDatePickeraht1 = $find("<%= dp_top4_score_start.ClientID %>");
        var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickeraht2 = $find("<%= dp_top4_score_end.ClientID %>");
        var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid = 4;
        var KPI = 4;
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht1.push([f.ADate, f.CoachCount]);
                    data_aht2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('div_top4_fvsschart', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: txtValue4 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: txtValue4 + ' : <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'TOTAL ' + txtValue4 + ' Coaching',
                    data: data_aht1,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: txtValue4,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //aht
        }
    });

    }

    function top4glideactual(sender, args) {
        var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML

        var data_aht3 = [];
        var data_aht4 = [];
        var RadDatePickerahtglide1 = $find("<%= dp_top4_glide_start.ClientID %>");
        var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerahtglide2 = $find("<%= dp_top4_glide_end.ClientID %>");
        var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var KPI = 4; // AHT
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
            //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht3.push([f.ADate, f.TargetCount]);
                    data_aht4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('div_top4_avstchart', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Target ' + txtValue4,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue4,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: txtValue4 + ' : <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Target ' + txtValue4,
                    data: data_aht3,
                    type: 'spline',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: txtValue4,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //aht
        }
    });

    
    }

    function overalltop5(sender, args) {

        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var RadDatePickeraht1 = $find("<%= dp_top5_score_start.ClientID %>");
        var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickeraht2 = $find("<%= dp_top5_score_end.ClientID %>");
        var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid = 5;

        var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
        var KPI = 5;
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_AHTScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht1.push([f.ADate, f.CoachCount]);
                    data_aht2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('div_top5_fvsschart', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: txtValue5 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: txtValue5 + ' : <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'TOTAL ' + txtValue5 + ' Coaching',
                    data: data_aht1,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: txtValue5,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //aht
        }
    });
     }


     function top5glideactual(sender, args) {
         var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
         var data_aht3 = [];
         var data_aht4 = [];
         var RadDatePickerahtglide1 = $find("<%= dp_top5_glide_start.ClientID %>");
         var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

         var RadDatePickerahtglide2 = $find("<%= dp_top5_glide_end.ClientID %>");
         var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

         var CIMNo = $("#<%= MyCIM.ClientID %>").val();
         var KPI = 5; // AHT
         $.ajax({
             type: "POST",
             data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
             contentType: "application/json; charset=utf-8",
             url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
             //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
             dataType: 'json',
             success: function (msg) {

                 if (msg != null && msg.d == null) {
                     $("#err-msg").show();
                     //fplot($("#placeholder"), [[]], opts);
                 }
                 else {
                     $("#err-msg").hide();
                 }

                 $.each(msg, function (e, f) {


                     data_aht3.push([f.ADate, f.TargetCount]);
                     data_aht4.push([f.ADate, f.CurrentCount]);

                     $("#err-msg").hide();

                 });
                 Highcharts.chart('div_top5_avstchart', {
                     chart: {
                         type: 'xy',
                         spacingBottom: 30
                     },
                     title: {
                         text: '',
                         align: 'left',
                         style: {
                             // margin: '50px', // does not work for some reasons, see workaround below
                             color: '#707070',
                             fontSize: '20px',
                             fontWeight: 'bold',
                             textTransform: 'none'
                         }
                     },
                     xAxis: {
                         type: "category",
                         color: '#FF00FF'
                     },
                     yAxis: [{
                         allowDecimals: false,
                         title: {
                             text: 'Target ' + txtValue5,
                             style: {
                                 // margin: '50px', // does not work for some reasons, see workaround below
                                 color: '#707070',
                                 fontWeight: 'bold',
                                 textTransform: 'none'
                             }
                         },
                         color: '#FF00FF'

                     },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue5,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                     tooltip: {
                         pointFormat: txtValue5 + ' : <b>{point.y:,.0f}</b>'
                     },
                     plotOptions: {

                 },
                 credits: {
                     enabled: false
                 },

                 series: [{
                     name: 'Target ' + txtValue5,
                     data: data_aht3,
                     type: 'spline',
                     yaxis: 0,
                     color: '#DAA455',
                     marker: {
                         fillColor: '#fff',
                         lineWidth: 2,
                         lineColor: '#ccc'
                     }
                 },
                    {
                        name: txtValue5,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
             }); //aht
         }
     });

     
     }
    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML
    if (txtValue1 == "") {
        $('#OAHT_dates').hide();  //coaching frequency 
        $('#Overall_AHT_Agent').hide();
        $('#AHTGlide').hide(); //actual glide vs score
        $('#ahtglidedates').hide();
        $('#check_graph_aht').hide(); //button
    }
    else {
        $('#check_graph_aht').show(); //button
    }


    var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML;
    if (txtValue2 == "") {
        $('#overallvocdate').hide();
        $('#DIV2_VOCScore').hide();
        $('#OVOC_dates').hide();
        $('#overall_voc_agent').hide();
        $('#check_graph_voc').hide(); //button
    } else {
        $('#check_graph_voc').show(); //button
    }



    var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
    if (txtValue3 == "") {
        $('#Div2_FCRGlidedates').hide();
        $('#DIV2_FCRGlide').hide();
        $('#OFCR_dates').hide();
        $('#overall_fcr_agent').hide();
        $('#check_graph_fcr').hide(); //button

    } else {
        $('#check_graph_fcr').show(); //button
    }


    var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML
    if (txtValue4 == "") {
        $('#div_top4_fvssdates').hide(); //frequency vs score
        $('#div_top4_fvsschart').hide();
        //        $('#div_top4_avstdate').hide(); //actual glide vs score
        //        $('#div_top4_avstchart').hide();
        $('#check_graph_4th').hide(); //button

    } else {
        $('#check_graph_4th').show(); //button
    }

    var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
    if (txtValue5 == "") {
        //        $('#div_top5_fvssdates').hide(); //frequency vs score
        //        $('#div_top5_fvsschart').hide();
        //        $('#div_top5_avstdate').hide(); //actual glide vs score
        //        $('#div_top5_avstchart').hide();
        $('#check_graph_5th').hide(); //button

    } else {
        $('#check_graph_5th').show(); //button
    }



    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();
 
    $('#aht_score').hide();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRGlide').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#AHTGlide').hide();
    $('#ahtglidedates').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();

    $('#Div3_vocGlidedates').hide();
    $('#Div3_vocGlidecharts').hide();

    $('#overalldate').show();
    $('#overallfrequency').show();
    $('#overall_behaviordates').show();
    $('#aht_dates').hide();
    $('#behaviorvsKPI').show();
    $('#overall_kpivsb').show();
    $('#DIV2_kpivsscore').show(); 

    $("#check_graph").on("click", function () {
    
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overalldate').show();
    $('#overallfrequency').show();
    $('#overall_behaviordates').show();
    $('#aht_dates').hide();
    $('#behaviorvsKPI').show(); 
    $('#overall_kpivsb').show();
    $('#DIV2_kpivsscore').show(); 
    $('#aht_score').hide(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();   
    $('#Div2_FCRGlidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#AHTGlide').hide(); 
    $('#ahtglidedates').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();


    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();

    var txtValue1 = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML
    if (txtValue1 == "") {
        $('#OAHT_dates').hide();  //coaching frequency 
        $('#Overall_AHT_Agent').hide();
        $('#AHTGlide').hide(); //actual glide vs score
        $('#ahtglidedates').hide();
        $('#check_graph_aht').hide(); //button
    }
    else {
        $('#check_graph_aht').show(); //button
    }


    var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML;
    if (txtValue2 == "") {
        $('#overallvocdate').hide();
        $('#DIV2_VOCScore').hide();
        $('#OVOC_dates').hide();
        $('#overall_voc_agent').hide();
        $('#check_graph_voc').hide(); //button
    } else {
        $('#check_graph_voc').show(); //button
    }



    var txtValue3 = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
    if (txtValue3 == "") {
        $('#Div2_FCRGlidedates').hide();
        $('#DIV2_FCRGlide').hide();
        $('#OFCR_dates').hide();
        $('#overall_fcr_agent').hide();
        $('#check_graph_fcr').hide(); //button

    } else {
        $('#check_graph_fcr').show(); //button
    }


    var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML
    if (txtValue4 == "") {
        $('#div_top4_fvssdates').hide(); //frequency vs score
        $('#div_top4_fvsschart').hide();
        //        $('#div_top4_avstdate').hide(); //actual glide vs score
        //        $('#div_top4_avstchart').hide();
        $('#check_graph_4th').hide(); //button

    } else {
        $('#check_graph_4th').show(); //button
    }

    var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
    if (txtValue5 == "") {
        //        $('#div_top5_fvssdates').hide(); //frequency vs score
        //        $('#div_top5_fvsschart').hide();
        //        $('#div_top5_avstdate').hide(); //actual glide vs score
        //        $('#div_top5_avstchart').hide();
        $('#check_graph_5th').hide(); //button

    } else {
        $('#check_graph_5th').show(); //button
    }

    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();

    $('#aht_score').hide();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRGlide').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#AHTGlide').hide();
    $('#ahtglidedates').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();

    $('#Div3_vocGlidedates').hide();
    $('#Div3_vocGlidecharts').hide();


        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var data3 = [];
        var data_kpivsbehavior1 = [];
        var data_kpivsbehavior2 = [];
        var data_bvsk1 = [];
        var data_bvsk2 = [];
               

        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()

                $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: URL+ "/FakeApi.asmx/getData_overall_ForHR",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            //c = f.TargetCount;
                            data.push([f.ADate, f.CoachCount]);
                            //                    data2.push([f.ADate, f.TargetCount]);

                            $("#err-msg").hide();

                        });
                         
                               Highcharts.chart('overallfrequency', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                           title: {
                                text: '',
                                align: 'left',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontSize: '20px',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            xAxis: {
                                type: "category",
                                color: '#FF00FF'
                            },
                            yAxis: {
                                allowDecimals: false,
                                title: {
                                    text: ''
                                },
                                color: '#FF00FF'
                            },
                            tooltip: {
                                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                            },
                            plotOptions: {
                                area: {
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle',
                                        radius: 5,
                                        states: {
                                            hover: {
                                                enabled: true
                                            }
                                        }
                                    },
                                    fillOpacity: 1,
                                    fillColor: '#289CCC'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Coaching Frequency',
                                data: data,
                                marker: {
                                    fillColor: '#fff',
                                    lineWidth: 2,
                                    lineColor: '#ccc'
                                }
                            }]
                        }); //OVERALL FREQUENCY DIV1
                    } //SUCCESSFUNCTION OA FRE
                }); //AJAX OVERALL FREQUENCY
                 
        var data3 = []; 
     
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();  
      
        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");

        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()
                $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: URL+ "/FakeApi.asmx/getData_overallbehavior_ForHR",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            //c = f.TargetCount;
                            data3.push([f.description, f.CoachCount]);
                            //                    data2.push([f.ADate, f.TargetCount]);

                            $("#err-msg").hide();

                        });
                         
                      Highcharts.chart('behaviorvsKPI', {
                                        chart: {
                                            type: 'area',
                                            spacingBottom: 30
                                        },
                        title: {
                            text: '',
                            align: 'left',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontSize: '20px',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        xAxis: {
                            type: "category",
                            color: '#FF00FF'
                        },
                        yAxis: [{
                            allowDecimals: false,
                            title: {
                                text: '',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            color: '#FF00FF'

                        },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                        tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                                                series: {
                                colorByPoint: true
                            }
                },
                credits: {
                    enabled: false
                },


                    series: [{
                        name: 'Overall Behavior',
                        data: data3,
                        type: 'column',
                        yaxis: 0,
                        color: '#2F9473',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL Behavior
            } //SUCCESS BEHAVIOR
        }); //BEHAVIOR AJAX

        var RadDatePickerbkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>");
        var selectedDatebkpi1 = RadDatePickerbkpi1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerbkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>");
        var selectedDatebkpi2 = RadDatePickerbkpi2.get_selectedDate().format("yyyy-MM-dd");

        var kpiidbkpi = $find('<%=cb_kpi_behavior_kpi.ClientID %>');
        var kpiidbkpi1 = kpiidbkpi.get_selectedItem().get_value()

        var data_bvsk1 = [];
        var data_bvsk2 = [];
               

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 
      
        $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: URL+ "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForHR",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            data_bvsk1.push([f.descriptions, f.coachedkpis]);
                            data_bvsk2.push([f.descriptions, f.AvgCurr]);

                            $("#err-msg").hide();

                        }); 
                        Highcharts.chart('DIV2_kpivsscore', {
                            chart: {
                                type: 'xy',
                                spacingBottom: 30
                            },
                        title: {
                            text: '',
                            align: 'left',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontSize: '20px',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        xAxis: {
                            type: "category",
                            color: '#FF00FF'
                        },
                        yAxis: [{
                            allowDecimals: true,
                            title: {
                                text: '',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            color: '#FF00FF'

                        },
                    { allowDecimals: true,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                        tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },


                    series: [{
                        name: 'Behavior',
                        data: data_bvsk1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL BEHAVIORvsKPI
            } //SUCCESS BEHAVIORvsKPI
        }); //BEHAVIORvsKPI AJAX       		
}); //READY FUNCTION



$("#check_graph_voc").on("click", function () {
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overalldate').hide();
    $('#overallfrequency').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide();
    $('#overall_kpivsb').hide();
    $('#DIV2_kpivsscore').hide();
    $('#aht_dates').hide();
    $('#aht_score').hide();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#aht_dates').hide();
    $('#overallvocdate').show();
    $('#DIV2_VOCScore').show();
    $('#AHTGlide').hide();
    $('#ahtglidedates').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();
    $('#Div3_vocGlidedates').show();
    $('#Div3_vocGlidecharts').show();


    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();

    var txtValue2 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML;

    var data_voc1 = [];
    var data_voc2 = [];
    var RadDatePickervoc1 = $find("<%= dp_start_voc.ClientID %>");
    var selectedDatevoc1 = RadDatePickervoc1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickervoc2 = $find("<%= dp_end_voc.ClientID %>");
    var selectedDatevoc2 = RadDatePickervoc2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
    var KPI = 2;

    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDatevoc1 + "', EndDate: '" + selectedDatevoc2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_VOCScore",
        // getDataTL_VOCScore
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_voc1.push([f.ADate, f.CoachCount]);
                data_voc2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('DIV2_VOCScore', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue2 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue2 + ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue2 + ' Coaching',
                data: data_voc1,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue2,
                        type: 'spline',
                        yAxis: 1,
                        data: data_voc2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        }); //voc
    }
});


var txtValue1 = document.getElementById("<%=btn_top2kpi.ClientID %>").innerHTML

var data_aht3 = [];
var data_aht4 = [];
var RadDatePickerahtglide1 = $find("<%= RadDatePicker3.ClientID %>");
var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

var RadDatePickerahtglide2 = $find("<%= RadDatePicker4.ClientID %>");
var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");
var CIMNo = $("#<%= MyCIM.ClientID %>").val();

var KPI = 2; //AHT
$.ajax({
    type: "POST",
    data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
    contentType: "application/json; charset=utf-8",
    url: URL + "/FakeApi.asmx/getGlidepath_QA_bi",
    // url: URL + "/FakeApi.asmx/getDataQA_AHTScore",
    dataType: 'json',
    success: function (msg) {

        if (msg != null && msg.d == null) {
            $("#err-msg").show();
            //fplot($("#placeholder"), [[]], opts);
        }
        else {
            $("#err-msg").hide();
        }

        $.each(msg, function (e, f) {


            data_aht3.push([f.ADate, f.TargetCount]);
            data_aht4.push([f.ADate, f.CurrentCount]);

            $("#err-msg").hide();

        });

        Highcharts.chart('Div3_vocGlidecharts', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + txtValue1,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue1,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                pointFormat: txtValue1 + ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Target ' + txtValue1,
            data: data_aht3,
            type: 'spline',
            yaxis: 0,
            color: '#DAA455',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: txtValue1,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'

                        }
                    }]
    });
}
});

});

$("#check_graph_fcr").on("click", function () {
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overallfrequency').hide();
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide();
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide();
    $('#aht_score').hide();
    $('#fcr_dates1').show();
    $('#DIV2_FCRScore').show();
    $('#DIV2_FCRGlide').show();
    $('#Div2_FCRGlidedates').show();
    $('#aht_dates').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#AHTGlide').hide();
    $('#ahtglidedates').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();


    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();

    var txtValue = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
    var data = [];

    var data_fcr1 = [];
    var data_fcr2 = [];


    var RadDatePickerfcr1 = $find("<%= dp_fcr_start1.ClientID %>");
    var selectedDatefcr1 = RadDatePickerfcr1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickerfcr2 = $find("<%= dp_fcr_end1.ClientID %>");
    var selectedDatefcr2 = RadDatePickerfcr2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val();
    var KPI = 3; 
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDatefcr1 + "', EndDate: '" + selectedDatefcr2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_FCRScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_fcr1.push([f.ADate, f.CoachCount]);
                data_fcr2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });

            Highcharts.chart('DIV2_FCRScore', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue + ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue +' Coaching',
                data: data_fcr1,
                type: 'column',
                yaxis: 0,
                color: '#27A6D9',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue ,
                        type: 'spline',
                        yAxis: 1,
                        data: data_fcr2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        }); //fcr
    }
});
        var txtValue = document.getElementById("<%=btn_top3kpi.ClientID %>").innerHTML;
   
var data_fcr3 = [];
var data_fcr4 = [];

var RadDatePickerfcrg1 = $find("<%= dp_fcrglide_start.ClientID %>");
var selectedDatefcrg1 = RadDatePickerfcrg1.get_selectedDate().format("yyyy-MM-dd");

var RadDatePickerfcrg2 = $find("<%= dp_fcrglide_end.ClientID %>");
var selectedDatefcrg2 = RadDatePickerfcrg2.get_selectedDate().format("yyyy-MM-dd");

var CIMNo = $("#<%= MyCIM.ClientID %>").val();
var KPI = 3; //FCR
//fcr actual glide
$.ajax({
    type: "POST",
    data: "{ StartDate: '" + selectedDatefcrg1 + "', EndDate: '" + selectedDatefcrg2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
    contentType: "application/json; charset=utf-8",
    url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
    //url: URL + "/FakeApi.asmx/getDataHR_FCRScore",
    dataType: 'json',
    success: function (msg) {

        if (msg != null && msg.d == null) {
            $("#err-msg").show();
            //fplot($("#placeholder"), [[]], opts);
        }
        else {
            $("#err-msg").hide();
        }

        $.each(msg, function (e, f) {


            data_fcr3.push([f.ADate, f.TargetCount]);
            data_fcr4.push([f.ADate, f.CurrentCount]);

            $("#err-msg").hide();

        });

        Highcharts.chart('DIV2_FCRGlide', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + txtValue,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                pointFormat: txtValue + ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Target ' + txtValue,
            data: data_fcr3,
            type: 'spline',
            yaxis: 0,
            color: '#27A6D9',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: 'Actual ' + txtValue ,
                        type: 'spline',
                        yAxis: 0,
                        data: data_fcr4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
    }); //fcr
}
});



//fcr actual glide





});


$("#check_graph_aht").on("click", function () {

    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overallfrequency').hide();
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide();
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide();
    $('#aht_score').show();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').show();
    $('#AHTGlide').show();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#ahtglidedates').show();
    $('#DIV2_FCRGlide').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();


    $('#div_top4_fvssdates').hide(); //frequency vs score
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide(); //actual glide vs score
    $('#div_top4_avstchart').hide();
    $('#div_top5_fvssdates').hide(); //frequency vs score
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide(); //actual glide vs score
    $('#div_top5_avstchart').hide();

    var txtValue = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML;
   
    var data = [];
    var data_aht1 = [];
    var data_aht2 = [];
    var RadDatePickeraht1 = $find("<%= dp_aht_start1.ClientID %>");
    var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickeraht2 = $find("<%= dp_aht_end1.ClientID %>");
    var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
    var kpiid = 4;
    var KPI = 1;
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_AHTScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_aht1.push([f.ADate, f.CoachCount]);
                data_aht2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('aht_score', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue + ': <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue + ' Coaching',
                data: data_aht1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        }); //aht
    }
});



//glide actual
        var txtValue = document.getElementById("<%=btn_top1kpi.ClientID %>").innerHTML;
   
var data_aht3 = [];
var data_aht4 = [];
var RadDatePickerahtglide1 = $find("<%= dp_ahtglide_start.ClientID %>");
var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

var RadDatePickerahtglide2 = $find("<%= dp_ahtglide_end.ClientID %>");
var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

var CIMNo = $("#<%= MyCIM.ClientID %>").val();
var KPI = 1; // AHT
$.ajax({
    type: "POST",
    data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
    contentType: "application/json; charset=utf-8",
    url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
    //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
    dataType: 'json',
    success: function (msg) {

        if (msg != null && msg.d == null) {
            $("#err-msg").show();
            //fplot($("#placeholder"), [[]], opts);
        }
        else {
            $("#err-msg").hide();
        }

        $.each(msg, function (e, f) {


            data_aht3.push([f.ADate, f.TargetCount]);
            data_aht4.push([f.ADate, f.CurrentCount]);

            $("#err-msg").hide();

        });
        Highcharts.chart('AHTGlide', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + txtValue,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: { 
                pointFormat: txtValue +  ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Target ' + txtValue,
            data: data_aht3,
            type: 'spline',
            yaxis: 0,
            color: '#DAA455',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: txtValue,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
    }); //aht
}
});


//glide actual






});

    // top 4


$("#check_graph_4th").on("click", function () {

    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overallfrequency').hide();
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide();
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide();
    $('#aht_score').hide();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#AHTGlide').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#ahtglidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();


    $('#div_top4_fvssdates').show();
    $('#div_top4_fvsschart').show();
    $('#div_top4_avstdate').show();
    $('#div_top4_avstchart').show();


    $('#div_top5_fvssdates').hide();
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide();
    $('#div_top5_avstchart').hide();
    var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML

    var data = [];
    var data_aht1 = [];
    var data_aht2 = [];
    var RadDatePickeraht1 = $find("<%= dp_top4_glide_start.ClientID %>");
    var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickeraht2 = $find("<%= dp_top4_glide_end.ClientID %>");
    var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
    var kpiid = 4;
    var KPI = 4;
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_AHTScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_aht1.push([f.ADate, f.CoachCount]);
                data_aht2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('div_top4_fvsschart', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: 'AHT Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue4 + ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue4 + ' Coaching',
                data: data_aht1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue4,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        }); //aht
    }
});



//glide actual
var txtValue4 = document.getElementById("<%=btn_top4kpi.ClientID %>").innerHTML

var data_aht3 = [];
var data_aht4 = [];
var RadDatePickerahtglide1 = $find("<%= dp_top4_glide_start.ClientID %>");
var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

var RadDatePickerahtglide2 = $find("<%= dp_top4_glide_end.ClientID %>");
var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

var CIMNo = $("#<%= MyCIM.ClientID %>").val();
var KPI = 4; // AHT
$.ajax({
    type: "POST",
    data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
    contentType: "application/json; charset=utf-8",
    url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
    //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
    dataType: 'json',
    success: function (msg) {

        if (msg != null && msg.d == null) {
            $("#err-msg").show();
            //fplot($("#placeholder"), [[]], opts);
        }
        else {
            $("#err-msg").hide();
        }

        $.each(msg, function (e, f) {


            data_aht3.push([f.ADate, f.TargetCount]);
            data_aht4.push([f.ADate, f.CurrentCount]);

            $("#err-msg").hide();

        });
        Highcharts.chart('div_top4_avstchart', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + txtValue4,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue4,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                pointFormat: txtValue4 + ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Target ' + txtValue4,
            data: data_aht3,
            type: 'spline',
            yaxis: 0,
            color: '#DAA455',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: txtValue4,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
    }); //aht
}
});


//glide actual






});
    // top 4

    // top 5


$("#check_graph_5th").on("click", function () {

    $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
    $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
    $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

    $('#overallfrequency').hide();
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide();
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide();
    $('#aht_score').hide();
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#AHTGlide').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide();
    $('#ahtglidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#triaddates').hide();
    $('#div_triadcoaching').hide();


    $('#div_top4_fvssdates').hide();
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide();
    $('#div_top4_avstchart').hide();


    $('#div_top5_fvssdates').show();
    $('#div_top5_fvsschart').show();
    $('#div_top5_avstdate').show();
    $('#div_top5_avstchart').show();

    var data = [];
    var data_aht1 = [];
    var data_aht2 = [];
    var RadDatePickeraht1 = $find("<%= dp_top5_score_start.ClientID %>");
    var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

    var RadDatePickeraht2 = $find("<%= dp_top5_score_end.ClientID %>");
    var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

    var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
    var kpiid = 5;

    var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML;
    var KPI = 5;
    $.ajax({
        type: "POST",
        data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
        contentType: "application/json; charset=utf-8",
        url: URL + "/FakeApi.asmx/GetCoachingFrequency_HR_Scores", //getDataHR_AHTScore",
        dataType: 'json',
        success: function (msg) {

            if (msg != null && msg.d == null) {
                $("#err-msg").show();
                //fplot($("#placeholder"), [[]], opts);
            }
            else {
                $("#err-msg").hide();
            }

            $.each(msg, function (e, f) {


                data_aht1.push([f.ADate, f.CoachCount]);
                data_aht2.push([f.ADate, f.CurrentCount]);

                $("#err-msg").hide();

            });
            Highcharts.chart('div_top5_fvsschart', {
                chart: {
                    type: 'xy',
                    spacingBottom: 30
                },
                title: {
                    text: '',
                    align: 'left',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                xAxis: {
                    type: "category",
                    color: '#FF00FF'
                },
                yAxis: [{
                    allowDecimals: false,
                    title: {
                        text: 'Total Coaching',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF'

                },
                    { allowDecimals: false,
                        title: {
                            text: txtValue5 + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                tooltip: {
                    pointFormat: txtValue5 + ' : <b>{point.y:,.0f}</b>'
                },
                plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'TOTAL ' + txtValue5 + ' Coaching',
                data: data_aht1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: txtValue5,
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
        }); //aht
    }
});



//glide actual
var txtValue5 = document.getElementById("<%=btn_top5kpi.ClientID %>").innerHTML
var data_aht3 = [];
var data_aht4 = [];
var RadDatePickerahtglide1 = $find("<%= dp_top5_glide_start.ClientID %>");
var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

var RadDatePickerahtglide2 = $find("<%= dp_top5_glide_end.ClientID %>");
var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

var CIMNo = $("#<%= MyCIM.ClientID %>").val();
var KPI = 5; // AHT
$.ajax({
    type: "POST",
    data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', KPI: '" + KPI + "'}",
    contentType: "application/json; charset=utf-8",
    url: URL + "/FakeApi.asmx/getGlidepath_HR_bi",
    //url: URL + "/FakeApi.asmx/getDataHR_AHTScore",
    dataType: 'json',
    success: function (msg) {

        if (msg != null && msg.d == null) {
            $("#err-msg").show();
            //fplot($("#placeholder"), [[]], opts);
        }
        else {
            $("#err-msg").hide();
        }

        $.each(msg, function (e, f) {


            data_aht3.push([f.ADate, f.TargetCount]);
            data_aht4.push([f.ADate, f.CurrentCount]);

            $("#err-msg").hide();

        });
        Highcharts.chart('div_top5_avstchart', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + txtValue5,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual ' + txtValue5,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                pointFormat: txtValue5 + ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Target ' + txtValue5,
            data: data_aht3,
            type: 'spline',
            yaxis: 0,
            color: '#DAA455',
            marker: {
                fillColor: '#fff',
                lineWidth: 2,
                lineColor: '#ccc'
            }
        },
                    {
                        name: txtValue5,
                        type: 'spline',
                        yAxis: 0,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
    }); //aht
}
});


//glide actual






});
    // top 5
		
			//Triad Coaching
		
		
   $("#check_graph_triad").on("click", function () {
        $("#check_graph_aht").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_voc").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_fcr").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F5D27C" });
        $("#check_graph_4th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        $("#check_graph_5th").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });


    $('#overallfrequency').hide(); 
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide(); 
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide(); 
    $('#aht_score').hide(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide(); 
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#AHTGlide').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#ahtglidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#Div2_VOCActual').hide();
    $('#Div2_VOCGlidedate').hide();
    $('#triaddates').show();
    $('#div_triadcoaching').show();
    $('#triaddates').show();
    $('#div_triadcoaching').show();

    $('#div_top4_fvssdates').hide();
    $('#div_top4_fvsschart').hide();
    $('#div_top4_avstdate').hide();
    $('#div_top4_avstchart').hide();


    $('#div_top5_fvssdates').hide();
    $('#div_top5_fvsschart').hide();
    $('#div_top5_avstdate').hide();
    $('#div_top5_avstchart').hide();


    $('#Div3_vocGlidedates').hide();
    $('#Div3_vocGlidecharts').hide();

        var data_triad1 = [];
        var data_triad2 = [];             
           
        var RadDatePickertriad1 = $find("<%= RadDatePicker1.ClientID %>");
        var selectedDatetriad1 = RadDatePickertriad1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickertriad2 = $find("<%= RadDatePicker2.ClientID %>");
        var selectedDatetriad2 = RadDatePickertriad2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatetriad1 + "', EndDate: '" + selectedDatetriad2 + "', CIMNo: '" + CIMNo  + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_TriadCoaching_HR_QA",
            //getData_TriadCoaching",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_triad1.push([f.ADate, f.CoachCount]);
                    data_triad2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                Highcharts.chart('div_triadcoaching', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Triad Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'Triad Coaching Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Total Triad Coaching',
                    data: data_triad1,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: 'Triad Coaching',
                        type: 'spline',
                        yAxis: 1,
                        data: data_triad2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //Triad Coaching
		
        }
        });
		});
    
$(document).ready(function () {
    $("#check_graph").trigger('click');
});
</script>




 