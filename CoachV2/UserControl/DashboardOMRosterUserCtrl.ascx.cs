﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;


namespace CoachV2.UserControl
{
    public partial class DashboardOMRosterUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Session["PagerMode"] != null && !IsPostBack)
            {
                GridPagerMode mode = (GridPagerMode)Session["PagerMode"];

            }
        }

        protected void AgentGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            //{
                try
                {

                    DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    int CIMNo = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);

                    AgentGrid.DataSource = DataHelper.GetSubordinates2(CIMNo);
                    AgentGrid.DataBind();

                }
                catch (Exception ex)
                {
                    AgentGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            //}
        }

        protected void AgentGrid_PreRender(object sender, EventArgs e)
        {
            DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            int CIMNo = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);

            AgentGrid.DataSource = DataHelper.GetSubordinates2(CIMNo);
            AgentGrid.DataBind();
        }

        protected string encryp_cim_url(object cim)
        {
            return DataHelper.Encrypt(Convert.ToInt32(cim.ToString()));
        }

    }
}