﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditEducUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileEditEducUserCtrl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
        
         <a href="javascript:history.go(-1)" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>
        <div>
            &nbsp;<span class="glyphicon glyphicon-education"></span> My Education
        </div> <%-- ExportSettings-ExportOnlyData="true"--%>
    </div>
    <telerik:RadGrid ID="EducGrid" runat="server" AutoGenerateColumns="false" AllowSorting="true"
        OnNeedDataSource="EducGrid_NeedDataSource" OnItemDataBound="EducGrid_ItemDataBound"
        OnInsertCommand="EducGrid_InsertCommand" OnDeleteCommand="EducGrid_DeleteCommand"
        OnUpdateCommand="EducGrid_UpdateCommand" OnItemCreated="EducGrid_ItemCreated"  RenderMode="Auto" OnItemCommand="EducGrid_ItemCommand" >
        <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" DataKeyNames="EducBgID"   
            EditMode="EditForms">
            <EditFormSettings>
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add Education"
                ShowExportToCsvButton="true"   />
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Edit" ID="EditLink" runat="server"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="School" SortExpression="School" DataField="School"
                    ColumnEditorID="txtSchoolName21" HeaderText="School Name" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "School") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtSchoolName" runat="server" EmptyMessage="Please enter school name"
                            Text='<%# DataBinder.Eval(Container.DataItem, "School") %>' />
                        <asp:RequiredFieldValidator ID="reqSchool" runat="server" Text="Enter school name"
                            ControlToValidate="TxtSchoolName"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="SchoolTypeID" SortExpression="Description"
                    DataField="Description" ColumnEditorID="cboSTName" HeaderText="Level" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Description") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="SchoolTypeIDVal" Text='<%# DataBinder.Eval(Container.DataItem, "SchoolTypeID") %>'
                            Visible="false" />
                        <telerik:RadComboBox ID="cboST" runat="server" EmptyMessage="Choose level" DataTextField="Description"
                            DataValueField="SchoolTypeID" AppendDataBoundItems="true" Sort="Ascending" DropDownAutoWidth="Enabled"
                            AllowCustomText="true" MarkFirstMatch="true">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="reqST" runat="server" Text="*" ControlToValidate="cboST"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Course" SortExpression="Course" DataField="Course"
                    ColumnEditorID="txtCourset" HeaderText="Course" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Course") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtCourse" runat="server" EmptyMessage="Please enter course" Text='<%# DataBinder.Eval(Container.DataItem, "Course") %>' />
                        <asp:RequiredFieldValidator ID="reqCourse" runat="server" Text="Enter course. If Not Applicable please input N/A " ControlToValidate="TxtCourse"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Honors" SortExpression="Honors" DataField="Honors"
                    ColumnEditorID="Honors" HeaderText="Honors" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Honors") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtHonors" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Honors") %>' />
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="StartDate" SortExpression="From" DataField="StartDate"
                    ColumnEditorID="txtFrom2" HeaderText="From" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%#  DataBinder.Eval(Container.DataItem, "StartDate", "{0:y}")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="SchoolStartDate" Text='<%# DataBinder.Eval(Container.DataItem, "StartDate") %>'
                            Visible="false" />
                        <telerik:RadMonthYearPicker ID="TxtStart" runat="server" EmptyMessage="Please enter year start" />
                        <asp:RequiredFieldValidator ID="reqFrom" runat="server" Text="Enter year start" ControlToValidate="TxtStart"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="EndDate" SortExpression="To" DataField="EndDate"
                    ColumnEditorID="txtTo21" HeaderText="To" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "EndDate", "{0:y}")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="SchoolEndDate" Text='<%# DataBinder.Eval(Container.DataItem, "EndDate") %>'
                            Visible="false" />
                        <telerik:RadMonthYearPicker ID="TxtEnd" runat="server" EmptyMessage="Please enter year end" />
                        <asp:RequiredFieldValidator ID="reqTo" runat="server" Text="Enter year end" ControlToValidate="TxtEnd"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Graduated" SortExpression="Graduated" DataField="Graduated"
                    ColumnEditorID="txtGRadE" HeaderText="Graduated" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Graduated")) == true ? "Yes" : "No" %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="LblGraduated" Text='<%# DataBinder.Eval(Container.DataItem, "Graduated") %>'
                            Visible="false" />
                        <asp:DropDownList ID="DDGraduated" runat="server" CssClass="form-control" Width="150">
                            <asp:ListItem Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="DDGraduated"
                            Text="Required">
                        </asp:RequiredFieldValidator>
                        <br />
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Delete" ID="DeleteLink" runat="server" OnClientClick="return confirm('Are you sure to delete this record?');"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings >
        <Scrolling AllowScroll="true" />
        </ClientSettings>

    </telerik:RadGrid>
    <br />
    <br />
    <!--  <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" UpdateText="Toxic">
                </telerik:GridEditCommandColumn>-->
</div>
