﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MyTeamLogsUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.MyTeamLogsUserCtrl" %>
<%@ Register Src="ReviewsProfUserCtrl.ascx" TagName="ReviewsProfUserCtrl" TagPrefix="uc1" %>
<%@ Register Src="ReviewsProfWeeklyUserCtrl.ascx" TagName="ReviewsProfWeeklyUserCtrl"
    TagPrefix="uc2" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
        <div>
            &nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard > MY TEAM > <%--Agent Review--%>
                        <asp:Label runat="server" ID="LblFullName2" CssClass="breadcrumb2ndlevel" Text="GURL" /> 
                        >
              <asp:Label runat="server" ID="lbl_sublevel"   Visible="true"  /> Review
            Logs 
<%--            <asp:Label runat="server" ID="LblFullName2" CssClass="breadcrumb2ndlevel" Text="GURL" />--%>
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div id="profile-summary">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Profile Summary <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                   <div class="panel-body">
                        <div class="container-fluid" id="Div6" runat="server">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div id="profileimageholder">
                                        <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                    </div>
                                </div>                               
                                <div class="form-group col-sm-8" style="padding: 0px">
                                    <form>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span class="namefont">
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span>
                                                <asp:Label ID="LblMyRole" runat="server" Text="My role" /></span>
                                        </div>
                                    </div>
                                     <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                         <div class="col-sm-12">
                                             <span>
                                                 <asp:Label ID="LblSite" runat="server" Text="Manila.Ortigas" /></span>
                                         </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Reporting Manager:</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="LblMySupervisor" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Region:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblRegion" runat="server" Text="My region" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Country:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblCountry" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px; display:none;">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Department:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblDept" runat="server" Text="My Dept" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Client:</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="LblClient" runat="server" Text="My client" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Organizational Unit:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblCampaign" runat="server" Text="My campaign" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group" id="Div1">
        <div id="Div2">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Review List <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <uc1:ReviewsProfUserCtrl ID="ReviewsProfUserCtrl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group" id="Div4">
        <div id="Div5">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            This week's completed reviews <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapse3" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <uc2:ReviewsProfWeeklyUserCtrl ID="ReviewsProfWeeklyUserCtrl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
