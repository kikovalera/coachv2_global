﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditMainUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileEditMainUserCtrl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
<%--        <a href="javascript:history.go(-1)" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>
--%>      
  <a href="MyProfile.aspx" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>
  <div>
            &nbsp;<span class="glyphicon glyphicon-user"></span> MY PROFILE
        </div>
    </div>
    <br />
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            CIM No.</label>
        <div class="col-sm-5"><%-- <div class="col-sm-10">--%>
            <asp:TextBox ID="TxtCIM" ReadOnly="true" runat="server" CssClass="form-control"  Width="100%" Enabled="false"  /><%-- Width="300px"--%>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            First Name</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtFirstName" runat="server" CssClass="form-control" Width="100%" Enabled="false" /> <%-- Width="300px" --%>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Last Name</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtLastName" runat="server" CssClass="form-control"  Width="100%" Enabled="false" /> <%--Width="300px" --%>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Reporting Manager</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtReportingManager" runat="server" CssClass="form-control" Width="100%"  Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Role</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtRole" runat="server" CssClass="form-control"  Width="100%"  Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Region</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtRegion" runat="server" CssClass="form-control"  Width="100%"  Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Country</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtCountry" runat="server" CssClass="form-control"  Width="100%"  Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Site</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtSite" runat="server" CssClass="form-control" Width="100%"   Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row" style="display: none;">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Department</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtDepartment" runat="server" CssClass="form-control" Width="100%"  Enabled="false" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Client</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtClient" runat="server" CssClass="form-control"  Width="100%"  />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Organizational Unit</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtCampaign" runat="server" CssClass="form-control"  Width="100%"  />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Gender</label>
        <div class="col-sm-5">
            <telerik:RadButton ID="RadioMale" runat="server" ToggleType="Radio" GroupName="StandardButton"
                Text="Male" ButtonType="ToggleButton" AutoPostBack="false" Value="1" Enabled="false" RenderMode="Auto">
            </telerik:RadButton>
            <telerik:RadButton ID="RadioFemale" runat="server" ToggleType="Radio" GroupName="StandardButton"
                Text="Female" ButtonType="ToggleButton" AutoPostBack="false" Value="2"  Enabled="false" RenderMode="Auto">
            </telerik:RadButton>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Date of Birth</label>
        <div class="col-sm-5">
            <telerik:RadDatePicker ID="DPBday" runat="server" RenderMode="Auto"  MaxDate="3000-01-01" MinDate="1900-01-01" Enabled="false" >
            </telerik:RadDatePicker>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Start date with Transcom</label>
        <div class="col-sm-5">
            <telerik:RadDatePicker ID="DPTranscomStartDate" runat="server" RenderMode="Auto" MaxDate="3000-01-01" MinDate="1900-01-01"  Enabled="false" > 
            </telerik:RadDatePicker>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Mobile No</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtMobile" runat="server" CssClass="form-control" Width="100%" /> <%--Width="300px" --%>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Landline No</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtLandline" runat="server" CssClass="form-control" Width="100%" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Transcom Email</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtTranscomEmail" runat="server" CssClass="form-control" Width="100%" ReadOnly="true" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Personal Email</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtPersonalEmail" runat="server" CssClass="form-control" Width="100%" />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Primary Language</label>
        <div class="col-sm-5">
            <asp:TextBox ID="TxtPrimaryLanguage" runat="server" CssClass="form-control" Width="100%"  />
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            Inactive</label>
        <div class="col-sm-5">
            <telerik:RadButton ID="RadioYes" runat="server" ToggleType="Radio" GroupName="RadioInactive" Enabled="false"
                Text="Yes" ButtonType="ToggleButton" AutoPostBack="false" Value="1" >
            </telerik:RadButton>
            <telerik:RadButton ID="RadioNo" runat="server" ToggleType="Radio" GroupName="RadioInactive"
                Text="No" ButtonType="ToggleButton" AutoPostBack="false" Value="0" Enabled="false"  >
            </telerik:RadButton>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">
            </label>
         <div class="col-sm-10"> 
            <asp:Button ID="BtnSaveInfo" Text="Save Info" runat="server" CssClass="btn btn-primary"
                OnClick="BtnSaveInfo_Click" AutoPostBack="true"></asp:Button>
        </div>
    </div>
</div>
