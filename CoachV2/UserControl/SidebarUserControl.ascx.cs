﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class SidebarUserControl : System.Web.UI.UserControl
    {

        

        ProfileMainUserControl ProfileMainUserControl1;
        ProfileExperienceUserControl ProfEx;
        ProfileEducUserControl ProfEduc;
        ProfileSkillUserControl ProfSkill;
        ProfileCertUserControl ProfCert;
        ProfileExtraInfoUserCtrl ProfExtra;
        AgentSearch AgentUc;
        protected void Page_Load(object sender, EventArgs e)
        {
            ProfileMainUserControl1 = (ProfileMainUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileMainUserControl1");
            ProfEx = (ProfileExperienceUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileExperienceUserControl1");
            ProfEduc = (ProfileEducUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEducUserControl1");
            ProfSkill = (ProfileSkillUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileSkillUserControl1");
            ProfCert = (ProfileCertUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileCertUserControl1");
            ProfExtra = (ProfileExtraInfoUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileExtraInfoUserCtrl1");
            AgentUc = (AgentSearch)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("AgentSearchCtrl1");

             ProfileMainUserControl1.Visible = Request.QueryString["tab"] == null ? true : false;
             ProfEx.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "experience" ? true : false;
             ProfEduc.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "education" ? true : false;
             ProfSkill.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "skill" ? true : false;
             ProfCert.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "cert" ? true : false;
            ProfExtra.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "extra" ? true : false;

            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            int int_cim = Convert.ToInt32(cim_num);

            DataSet ds_getrolefromsap1 = DataHelper.getuserrolefromsap(int_cim);
                      
            string rolevalue1 = Convert.ToString(ds_getrolefromsap1.Tables[0].Rows[0]["Role"].ToString());

            string saprolefordashboard1 = DataHelper.MyRoleInSAP(Convert.ToInt32(int_cim));

            if (rolevalue1 == "QA")
                saprolefordashboard1 = "QA";
            else if (rolevalue1 == "HR")
                saprolefordashboard1 = "HR";
            else
                saprolefordashboard1 = DataHelper.MyRoleInSAP(Convert.ToInt32(int_cim));


            if (saprolefordashboard1 == "")
            {
                TxtSearch.Visible = true;
                BtnSearchQuery.Visible = true;
                div_search.Visible = true;
                //pn_agentsearch.Visible = true;
            }
            else
            {
                TxtSearch.Visible = false;
                BtnSearchQuery.Visible = false;
                div_search.Visible = false;
                //pn_agentsearch.Visible = false;
            }
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            ProfileMainUserControl1.Visible = true;

            
            ProfEduc.Visible = false;
            ProfSkill.Visible = false;
            ProfCert.Visible = false;

        }

        protected void Unnamed2_Click(object sender, EventArgs e)
        {
            ProfileMainUserControl1.Visible = false;
            ProfEx.Visible = true;
            ProfEduc.Visible = false;
            ProfSkill.Visible = false;
            ProfCert.Visible = false;
        }

        protected void Unnamed3_Click(object sender, EventArgs e)
        {
            ProfileMainUserControl1.Visible = false;
            ProfEx.Visible = false;
            ProfEduc.Visible = true;
            ProfSkill.Visible = false;
            ProfCert.Visible = false;
        }

        protected void Unnamed4_Click(object sender, EventArgs e)
        {
            ProfileMainUserControl1.Visible = false;
            ProfEx.Visible = false;
            ProfEduc.Visible = false;
            ProfSkill.Visible = true;
            ProfCert.Visible = false;
        }

        protected void Unnamed5_Click(object sender, EventArgs e)
        {
            ProfileMainUserControl1.Visible = false;
            ProfEx.Visible = false;
            ProfEduc.Visible = false;
            ProfSkill.Visible = false;
            ProfCert.Visible = true;
        }


        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

             protected void BtnSearchQuery_Click(object sender, EventArgs e)
        {

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            int cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);
          
            RadGrid SearchGrid = (RadGrid)AgentUc.FindControl("SearchGrid");
            if (TxtSearch.Text != "")
            {
                ProfileMainUserControl1.Visible = false;
                ProfEx.Visible = false;
                ProfEduc.Visible = false;
                ProfSkill.Visible = false;
                ProfCert.Visible = false;
                ProfExtra.Visible = false;
                try
                {
                    AgentUc.Visible = true;
                    SearchGrid.Visible = true;
                    SearchGrid.DataSource = DataHelper.GetSearchQuery_forAgent(TxtSearch.Text, cimNo);
                    SearchGrid.DataBind();

                    TextBox textsearch = (TextBox)FindControl("TxtSearch");
                    //GridDataItem item = (GridDataItem)e.Item;

                    foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {


                        string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        string txtdesctolow = txtdesc.Text.ToLower();
                        string txtseatolow = textsearch.Text.ToLower();
                        string searchterm = Convert.ToString(textsearch.Text);

                        //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                        }
                        //added for talk talk 
                        string Account = null;


                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                        DataSet ds = null;
                        DataAccess ws = new DataAccess();
                        ds = ws.GetEmployeeInfo(Convert.ToInt32(cimNo));
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["Client"].ToString() == "TalkTalk")
                            {
                                Account = "TalkTalk";
                            }
                            else
                            {
                                if (CheckOperations(Convert.ToInt32(cimNo)) == true)
                                {
                                    Account = "Operations";
                                }
                                else
                                {
                                    Account = "NT";
                                }
                            }
                        }
                        else
                        {
                            Account = "NT";
                        }
                        //added for talkt talk
                        if (Convert.ToInt32(val2) == 4)
                        {

                            hlink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;

                        }
                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;


                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            hlink.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        }
                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        }
                        else
                        {
                            //added for talkt talk
                            if (Account == "TalkTalk")
                            {
                                hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 + "&Account=" + Account;
                            }
                            else if (Account == "Operations")
                            {
                                hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 + "&Account=" + Account;
                            }
                            else
                            {
                                hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            }
                            //added for talkt talk
                        }

                        //hyperlink

                        int itmindx = Item.ItemIndex + 1;
                        if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                        {



                            //description
                            string str = null;
                            string[] strArr = null;
                            int count = 0;
                            str = txtdesc.Text;
                            char[] splitchar = { ' ' };
                            strArr = str.Split(splitchar);
                            string desc = "";

                            string str_searchval = null;
                            string[] str_searchArr = null;
                            str_searchval = textsearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            for (count = 0; count <= strArr.Length - 1; count++)
                            {
                                string val1 = strArr[count].ToLower();
                                if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                {
                                    desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                }
                                else
                                {
                                    desc = desc + " " + strArr[count].ToString();

                                }

                            }
                            txtdesc.Text = desc;
                            //description

                            //Strengths 
                            string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtstrengths = (Label)Item.FindControl("Label6");
                            string txtstrengthslow = txtstrengths.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrStrengths = null;
                                int count1 = 0;
                                str = txtstrengths.Text;
                                strArrStrengths = str.Split(splitchar);
                                string strengths = "";
                                str_searchval = textsearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                {
                                    string val1 = strArrStrengths[count1].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        strengths = strengths + " " + strArrStrengths[count1].ToString();

                                    }

                                }
                                txtstrengths.Text = strengths;


                            }
                            //Strengths 

                            //Opportunity 
                            string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtOpportunity = (Label)Item.FindControl("Label7");
                            string txtOpportunitylow = txtOpportunity.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrOpportunity = null;
                                int count2 = 0;
                                str = txtOpportunity.Text;
                                strArrOpportunity = str.Split(splitchar);
                                string Opportunity = "";
                                str_searchval = textsearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                {
                                    string val1 = strArrOpportunity[count2].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                    }

                                }
                                txtOpportunity.Text = Opportunity;


                            }
                            //Opportunity 
                            //session name  
                            string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtSessionname = (Label)Item.FindControl("Label3");
                            string txtSessionnamelow = txtSessionname.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrSessionname = null;
                                int count3 = 0;
                                str = txtSessionname.Text;
                                strArrSessionname = str.Split(splitchar);
                                string Sessionname = "";
                                str_searchval = textsearch.Text;


                                for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                {
                                    string val1 = strArrSessionname[count3].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                    }

                                }
                                txtSessionname.Text = Sessionname;


                            }
                            //session name 


                        }
                        else
                        {


                            string str_searchval = null;
                            string str = "";
                            string[] str_searchArr = null;
                            char[] splitchar = { ' ' };
                            str_searchval = textsearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            //created on 
                            string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtCreatedon = (Label)Item.FindControl("Label4");
                            string txtCreatedonlow = txtCreatedon.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrCreatedon = null;
                                int count4 = 0;
                                str = txtCreatedon.Text;
                                strArrCreatedon = str.Split(splitchar);
                                string Createdon = "";
                                str_searchval = textsearch.Text;

                                for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                {
                                    string val1 = strArrCreatedon[count4].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                    }
                                    else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                }
                                txtCreatedon.Text = Createdon;


                            }
                            //createdon

                            //followdate 
                            string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtfollowdate = (Label)Item.FindControl("Label5");
                            string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrfollowdate = null;
                                int count5 = 0;
                                str = txtfollowdate.Text;
                                strArrfollowdate = str.Split(splitchar);
                                string followdate = "";
                                str_searchval = textsearch.Text;

                                for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                {
                                    string val1 = strArrfollowdate[count5].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                    }
                                    else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                }
                                txtfollowdate.Text = followdate;


                            }
                            //followdate
                        }
                    }
                }
                catch (Exception ex) {

                    string ModalLabel = ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                }

            }
            else if (TxtSearch.Text == "")
            {
                ProfileMainUserControl1.Visible = false;
                ProfEx.Visible = false;
                ProfEduc.Visible = false;
                ProfSkill.Visible = false;
                ProfCert.Visible = false;
                ProfExtra.Visible = false;

                AgentUc.Visible = true;
                SearchGrid.Visible = true;
                SearchGrid.DataSource = null;//DataHelper.GetSearchQuery_forAgent("0", cimNo);
                SearchGrid.DataBind();

            }

        }
    }
}