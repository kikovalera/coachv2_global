﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ops_ChartDir.ascx.cs"
    Inherits="CoachV2.UserControl.Ops_ChartDir" %>
<div class="panel-group" id="Div_TLCHARTS">
    <asp:HiddenField ID="FakeURLID" runat="server" />
    <div class="row row-custom">
    </div>
</div>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTL">
        <div class="panel-heading">
            <h4 class="panel-title">
                Charts<span class="glyphicon glyphicon-triangle-top triangletop"> </span>&nbsp;
            </h4>
        </div>
    </a>
</div>
<div id="collapseTL" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="form-horizontal col-md-12" role="form">
            <table>
                <tr>
                    <td>
                        <button type="button" class="btn btn-custom" id="check_graph" style="margin-right: 10px;">
                            Overall</button>
                    </td>
                    <td>
                        <asp:DataList ID="dlKPI" runat="server" RepeatColumns="10" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <button class="btn btn-custom click-me-kpi" kpi-id='<%#Eval("hierarchy").ToString() %>'>
                                    <%# Eval("kpi").ToString()%></button>
                                &nbsp;
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server" Visible="false"
                                    CssClass="blacktext"> 
                                </asp:Label>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                    <td>
                        <button type="button" class="btn btn-custom" id="check_graph_triad">
                            Triad Coaching</button>
                    </td>
                </tr>
            </table>
        </div>
        <%--Overall Account Coaching Frequency--%>
        <div id="overall-holder">
            <div id="overalldate" style="width: 100%; background-color: white">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4"><br />Overall Account Coaching Frequency</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallOM" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <asp:HiddenField ID="hiddentl1" runat="server" />
                    <telerik:RadComboBox ID="cb_overallTL" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <asp:HiddenField ID="HiddenAgent1" runat="server" />
                    <telerik:RadComboBox ID="cb_overallAgent" runat="server" AppendDataBoundItems="true"
                        DataValueField="CimNumber" DataTextField="Name">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_overalldataview" runat="server" AppendDataBoundItems="true"
                        DataValueField="DataViewID" DataTextField="DataView">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallSite" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallLOB" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnOverall" class="btn btn-info">
                            Run</button>
                    </div>
                <div class="col-sm-4" style="display: none;">
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                        AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                        <asp:ListItem Value="0" Text="All" />
                    </asp:DropDownList>
                </div>
            </div>
            <div id="overallfrequency" style="width: 100%; height: 100%;">
            </div>
            <%--Top Behavioral Drivers --%>
            <div id="overall_behaviordates">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4"><br />Top Behavioral Drivers</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbOM" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbTL" runat="server" DataValueField="CimNumber"
                         DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_kpi_behavior1" runat="server" AppendDataBoundItems="true"
                        DataValueField="KPIID" DataTextField="KPIName" ResolvedRenderMode="Classic">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallBehaviorChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallBehaviorChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbehaviorSite" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbehaviorLOB" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnBehavior" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="behaviorvsKPI" style="width: 100%; height: 100%;">
            </div>
            <%--Behavioral Drivers Vs. KPI--%>
            <div id="overall_kpivsb">
                <br />
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4"><br />Behavioral Drivers Vs. KPI</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_BvsKPIOM" runat="server" DataValueField="CimNumber" DataTextField="Name"
                        AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_BvsKPITL" runat="server" DataValueField="CimNumber" DataTextField="Name"
                        AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_BvsKPIAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_kpi_behavior_kpi" runat="server" DataValueField="KPIID" DataTextField="KPIName" ResolvedRenderMode="Classic"
                        AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallBehaviorVsKPIChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="overallBehaviorVsKPIChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_behaviorkpi_site" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_behaviorkpi_lob" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnKPIvBehavior" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="DIV2_kpivsscore" style="width: 100%; height: 100%;">
            </div>
        </div>
        <!--Audit Frequency vs KPI-->
        <div id="chart-holder">
            <div id="div_top5_fvssdates">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="padding-top: 20px;">
                                <font size="4" id="freqvsscore"><br />Average AHT Score</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_omtop5_fvsscore" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_tltop5_fvsscore" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_agenttop5_fvsscore" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_top5_score_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="freqvsscoreChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_top5_score_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="freqvsscoreChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_top5scoredataview" runat="server" AppendDataBoundItems="true"
                        DataValueField="DataViewID" DataTextField="DataView" >
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top5fvs_site" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top5fvs_lob" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnScoreVKPI" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="div_top5_fvsschart" style="width: 100%; height: 100%;">
            </div>
            <br />
            <div id="div_top5_avstdate">
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                 <font size="4" id="tarvsactual"><br />Targets Vs. Actual</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_omtop5_avst" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_tltop5_avst" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_agenttop5_avst" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" >
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_top5_glide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="tarvsactualChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_top5_glide_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="tarvsactualChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_top5avtdataview" runat="server" AppendDataBoundItems="true"
                        DataValueField="DataViewID" DataTextField="DataView">
                        <%--   OnClientSelectedIndexChanged="OnClientSelectedIndexChangedAgentOverall"  OnClientSelectedIndexChanged="OVERALL"--%>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top5avst_site" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top5avst_lob" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnTarVAct" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="div_top5_avstchart" style="width: 100%; height: 100%;">
            </div>
        </div>
        <!-- End of Audit frequency vs KPI -->
        <!-- Start Triad-->
        <div id="triad-holder">
            <div id="triaddates">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4"><br />Triad Coaching VS Score</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triadcoachingom" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexOM">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triadcoachingtl" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexTL">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triadcoachingagent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                         <ClientEvents OnDateSelected="triadChartChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                         <ClientEvents OnDateSelected="triadChartChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_triaddataview" runat="server" AppendDataBoundItems="true"
                        DataValueField="DataViewID" DataTextField="DataView">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triad_site" runat="server" DataValueField="companysiteid"
                        DataTextField="companysite" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        LOB <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triad_lob" runat="server" DataValueField="accountid"
                        DataTextField="LOB" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnTriadCoaching" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="div_triadcoaching" style="width: 100%; height: 100%;">
            </div>
        </div>
        <!-- End triad Coaching -->
        <br />
        <br />
        <div class="form-group">
            <asp:HiddenField ID="MyCIM" runat="server" />
            <div>
                <div>
                    <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                        padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>No data found</span>
                    <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                        runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
                        Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                        Style="padding-top: 10px; padding-bottom: 10px;">
                    </asp:CompareValidator>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- charts area --%>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/exporting.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
<script type='text/javascript'>
    var URL = $("#<%= FakeURLID.ClientID %>").val();

    var kpiid;
    var kpilabel;

    function overallBehaviorVsKPIChanger(sender, args) {
        //overallbehaviorvskpi();
        var startDate = $find('<%=dp_behaviorkpi_start.ClientID%>');
        var endDate = $find('<%=dp_behaviorkpi_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnKPIvBehavior').prop('disabled', true);
        } else {
            $('#BtnKPIvBehavior').prop('disabled', false);
        }
    }

    function overallBehaviorChanger(sender, args) {
        var startDate = $find('<%=dp_behavior_start.ClientID%>');
        var endDate = $find('<%=dp_behavior_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnBehavior').prop('disabled', true);
        } else {
            $('#BtnBehavior').prop('disabled', false);
        }
    }

    function overallChanger(sender, args) {
        var startDate = $find('<%=StartDate.ClientID%>');
        var endDate = $find('<%=EndDate.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnOverall').prop('disabled', true);
        } else {
            $('#BtnOverall').prop('disabled', false);
        }
    }

    function overall() {

        var data = [];
        var highC = Highcharts.chart('overallfrequency', {
            chart: { //backgroundColor: '#ffffff',
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: {
                tickInterval: 5,
                min: 0,
                title: {
                    text: ''
                },
                color: '#FF00FF'
            },
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    },
                    fillOpacity: 1,
                    fillColor: '#289CCC'
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },
            series: [{
                name: 'Coaching Frequency',
                data: data,
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        }); //OVERALL FREQUENCY DIV1

        var selectedDate1 = $find("<%= StartDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDate2 = $find("<%= EndDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 
        var kpiid1 = $find('<%=cb_kpi_behavior1.ClientID %>').get_selectedItem().get_value();
        var OMCimNo = $find('<%=cb_overallOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallAgent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value();
        var Siteid = $find('<%=cb_overallSite.ClientID %>').get_selectedItem().get_value();
        var Account = $find('<%=cb_overallLOB.ClientID %>').get_selectedItem().get_value();


        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "',  KPIID: '" + kpiid1 + "',  OMCimNo: '" + OMCimNo + "',  TLCimNo: '" + TLCimNo + "',  AgentCimNo: '" + AgentCimNo + "',  DataView: '" + DataView + "',  Siteid: '" + Siteid + "',  Account: '" + Account + "',  LObID: '" + Account + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForDir1",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data.push([f.ADate, f.CoachCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data);
                highC.hideLoading();

            } //SUCCESSFUNCTION OA FRE
        });   //AJAX OVERALL FREQUENCY

        highC.showLoading();
    }

    function overallbehavior() {

        var data3 = [];
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 
        var selectedDateb1 = $find("<%= dp_behavior_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateb2 = $find("<%= dp_behavior_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var OMCimNo = $find('<%=cb_overallbOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallbTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallbAgent.ClientID %>').get_selectedItem().get_value();
        var kpiid1 = $find('<%=cb_kpi_behavior1.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('behaviorvsKPI', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                series: {
                    colorByPoint: true
                }
            },
            credits: {
                enabled: false
            }, exporting: {
                showTable: true
            },


            series: [{
                name: 'Top Behavioral Drivers',
                data: data3,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "', OMCimNo: '" + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_GetBehaviorForDir",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data3.push([f.description, f.CoachCount]);
                    $("#err-msg").hide();
                });
                highC.series[0].setData(data3);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }


    function overallbehaviorvskpi() {

        var data_bvsk1 = [];
        var data_bvsk2 = [];
        
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); 
        var selectedDatebkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatebkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiidbkpi1 = $find('<%=cb_kpi_behavior_kpi.ClientID %>').get_selectedItem().get_value();
        var OMCimNo = $find('<%=cb_BvsKPIOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_BvsKPITL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_BvsKPIAgent.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('DIV2_kpivsscore', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        tickInterval: 20,
                        max: 100,
                        labels: {
                            format: '{value}%'
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            }, exporting: {
                showTable: true
            },


            series: [{
                name: 'Behavior',
                data: data_bvsk1,
                type: 'column',
                yaxis: 0,
                color: '#27A6D9',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
        }); //OVERALL BEHAVIORvsKPI

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "', OMCimNo: '" + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForDIR",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_bvsk1);
                highC.series[1].setData(data_bvsk2);

                highC.hideLoading();
            } //SUCCESS BEHAVIORvsKPI
        });   //BEHAVIORvsKPI AJAX

        highC.showLoading(); 	
    }

    function getRandomColor(v) {
        var color = ['#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4', '#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4'];

        return color[v];
    }

    function freqvsscoreChanger(sender, args) {
        //chart1loader(kpiid, kpilabel);
        var startDate = $find('<%=dp_top5_score_start.ClientID%>');
        var endDate = $find('<%=dp_top5_score_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnScoreVKPI').prop('disabled', true);
        } else {
            $('#BtnScoreVKPI').prop('disabled', false);
        }
    }

    function tarvsactualChanger(sender, args) {
        var startDate = $find('<%=dp_top5_glide_start.ClientID%>');
        var endDate = $find('<%=dp_top5_glide_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTarVAct').prop('disabled', true);
        } else {
            $('#BtnTarVAct').prop('disabled', false);
        }
    }

    function triadChartChanger(sender, args) {
        var startDate = $find('<%=RadDatePicker1.ClientID%>');
        var endDate = $find('<%=RadDatePicker2.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTriadCoaching').prop('disabled', true);
        } else {
            $('#BtnTriadCoaching').prop('disabled', false);
        }
    }

    function chart1loader(v, label) {

        $('.highcharts-data-table').remove();

        var colorx = getRandomColor(v);
        var data1 = [];
        var data2 = [];
        var selectedDateaht1 = $find("<%= dp_top5_score_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateaht2 = $find("<%= dp_top5_score_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date

        var OMCimNo = $find('<%=cb_omtop5_fvsscore.ClientID %>').get_selectedItem().get_value()
        var TLCimNo = $find('<%=cb_tltop5_fvsscore.ClientID %>').get_selectedItem().get_value()
        var AgentCimNo = $find('<%=cb_agenttop5_fvsscore.ClientID %>').get_selectedItem().get_value()
        var KPI = v;
        var DataView = $find('<%=cb_top5scoredataview.ClientID %>').get_selectedItem().get_value()
        var Siteid = $find('<%=cb_top5fvs_site.ClientID %>').get_selectedItem().get_value()
        var Account = $find('<%=cb_top5fvs_lob.ClientID %>').get_selectedItem().get_value()

        $("#freqvsscore").html("Audit Frequency VS. " + label.toUpperCase() + " Score");

        var highC = Highcharts.chart('div_top5_fvsschart', {
            chart: {
                type: 'xy', // 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: 'Total Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: label + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        allowDecimals: false,
                        color: '#FF00FF',
                        opposite: true,
                        //min: 0,
                        max: (label.toString().indexOf('AHT') >= 0 ? 2000 : 100),
                        tickInterval: 20,
                        labels: {
                            format: ((label.toString().indexOf('AHT') >= 0 || label.toString().indexOf('OCR') >= 0) >= 0 ? '{value}' : '{value}%') 
                        }
                    }],
            tooltip: {
                pointFormat: label + ': <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },

            exporting: {
                showTable: true
            },
            series: [{
                name: 'TOTAL ' + label + ' Coaching',
                data: data1,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: label + " Score",
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: colorx,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return label + ' <b>' + this.y.toFixed(2) + ((label.toString().indexOf('AHT') >= 0 || label.toString().indexOf('OCR') >= 0) ? '' : '%') + ' </b>';
                            }
                        }
                    }]
        }); //voc

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', OMCimNo: '" + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', KPI: '" + KPI + "',  DataView: '" + DataView + "',  SiteID: '" + Siteid + "',  LobID: '" + Account + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_Score_Dir", //getData_AHTScore_Dir",
            // url: URL + "/FakeApi.asmx/getDataTL_VOCScore", // URL + "/FakeApi.asmx/getData",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data1.push([f.ADate, f.CoachCount]);
                    data2.push([f.ADate, f.CurrentCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data1);
                highC.series[1].setData(data2);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }

    function chart2loader(v, label) {

        var selectedDateavst41 =  $find("<%= dp_top5_glide_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateavst42 = $find("<%= dp_top5_glide_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var OMCimNo = $find('<%=cb_omtop5_avst.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_tltop5_avst.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_agenttop5_avst.ClientID %>').get_selectedItem().get_value();
        var KPI = v;
        var DataView = $find('<%=cb_top5avtdataview.ClientID %>').get_selectedItem().get_value();
        var Siteid = $find('<%=cb_top5avst_site.ClientID %>').get_selectedItem().get_value();
        var Account = $find('<%=cb_top5avst_lob.ClientID %>').get_selectedItem().get_value();

        $('.highcharts-data-table').remove();

        var colorx = getRandomColor(v);

        $("#tarvsactual").html("Target " + label.toUpperCase() + " Vs. Actual " + label.toUpperCase());

        var data_voc1 = [];
        var data_voc2 = [];

        var highC = Highcharts.chart('div_top5_avstchart', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{

                title: {
                    text: 'Target ' + label,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { // allowDecimals: false,
                        title: {
                            text: 'Actual ' + label,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {


                //                formatter: function () {
                //                    if (this.y == 0) {
                //                        return label + ':<b>No Score</b>';
                //                    } else {
                //                        return label + ':<b>' + this.y.toFixed(2) + '%</b>';
                //                    }
                //                }
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'Target ' + label,
                data: data_voc1,
                type: 'spline',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }, tooltip: {
                    pointFormatter: function () {
                        if (data_voc1[this.category].toString().indexOf('*') >= 0) {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ' <b>' + this.y.toFixed(2) + ((label.toString().indexOf('AHT') >= 0 || label.toString().indexOf('OCR') >= 0) ? '' : '%') + ' </b> fiscal month target change';
                            }
                        } else {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ' <b>' + this.y.toFixed(2) + ((label.toString().indexOf('AHT') >= 0 || label.toString().indexOf('OCR') >= 0) ? '' : '%') + '</b>';
                            }
                        }
                    }
                }
            },
                    {
                        name: 'Actual ' + label,
                        type: 'spline',
                        yAxis: 0,
                        data: data_voc2,
                        color: colorx,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'

                        },
                        tooltip: {
                            pointFormatter: function () {
                                if (this.y == 0 && parseInt(data_voc1[this.index][1]) == 0) {
                                    return label + '<b> No data available</b>';
                                } else {
                                    return label + ' <b>' + this.y.toFixed(2) + ((label.toString().indexOf('AHT') >= 0 || label.toString().indexOf('OCR') >= 0) ? '' : '%') + '</b>';
                                }
                            }
                        }
                    }]
        });


        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateavst41 + "', EndDate: '" + selectedDateavst42 + "', CIMNo: '" + CIMNo + "', OMCimNo: '"
    + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', KPI: '" + KPI + "',  DataView: '" + DataView + "',  Siteid: '" + Siteid + "',  Account: '" + Account + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetCoachingFrequency_DIR_Glides1", //getData_AHTScore_Dir", // url: URL + "/FakeApi.asmx/getDataTL_VOCScore", // URL + "/FakeApi.asmx/getData",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data_voc1.push([f.ADate, f.TargetCount]);
                    data_voc2.push([f.ADate, f.CurrentCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_voc1);
                highC.series[1].setData(data_voc2);

                highC.hideLoading();

            }
        });

        highC.showLoading();
    }

    function loadtriad() {
        $('.highcharts-data-table').remove();

        var data_triad1 = [];
        var data_triad2 = [];

        var selectedDatetriad1 = $find("<%= RadDatePicker1.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatetriad2 = $find("<%= RadDatePicker2.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var OMCimNo = $find('<%=cb_triadcoachingom.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_triadcoachingtl.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_triadcoachingagent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_triaddataview.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('div_triadcoaching', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                allowDecimals: false,
                title: {
                    text: 'Triad Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: 'Triad Coaching Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        allowDecimals: false,
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        max: 100,
                        tickInterval: 20,
                        labels: {
                            format: '{value}%'
                        }
                    }],
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },

            exporting: {
                showTable: true
            },
            series: [{
                name: 'Total Triad Coaching',
                data: data_triad1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: 'Triad Coaching Score',
                        type: 'spline',
                        yAxis: 1,
                        data: data_triad2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' Triad Coaching Score: <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
        }); //Triad Coaching


        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatetriad1 + "', EndDate: '" + selectedDatetriad2 + "', CIMNo: '" + CIMNo + "', OMCimNo: '" + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "',  DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_TriadCoachingDir",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data_triad1.push([f.ADate, f.CoachCount]);
                    data_triad2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_triad1);
                highC.series[1].setData(data_triad2);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }


    $(document).ready(function () {
        

        $("#check_graph").on("click", function () {
            $("#triad-holder").hide();
            $("#chart-holder").hide();

            $(this).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            $('.click-me-kpi').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $('#check_graph_triad').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

            $('.highcharts-data-table').remove();
            $("#overall-holder").show();

            overall();
            overallbehavior();
            overallbehaviorvskpi();
        });

        $("#check_graph").trigger('click');

        $(".click-me-kpi").on("click", function () {
            var v = $(this).attr("kpi-id");
            var label = $(this).text();

            kpiid = v;
            kpilabel = label;
            resetColor(v);

            $("#chart-holder").show();
            $("#overall-holder").hide();
            $("#triad-holder").hide();

            chart1loader(v, label);
            chart2loader(v, label);

            return false;
        });

        $("#check_graph_triad").on("click", function () {
            $("#chart-holder").hide();
            $("#overall-holder").hide();
            $("#triad-holder").show();

            $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $(".click-me-kpi").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            loadtriad();

            return false;
        });

        $("#BtnOverall").on("click", function () {
            overall();
            return false;
        });

        $("#BtnBehavior").on("click", function () {
            overallbehavior();
            return false;
        });

        $("#BtnKPIvBehavior").on("click", function () {
            overallbehaviorvskpi();
            return false;
        });

        $("#BtnScoreVKPI").on("click", function () {
            chart1loader(kpiid, kpilabel);
            return false;
        });

        $("#BtnTarVAct").on("click", function () {
            chart2loader(kpiid, kpilabel);
            return false;
        });

        $("#BtnTriadCoaching").on("click", function () {
            loadtriad();
            return false;
        });


    });


    function OnClientSelectedIndexOM(sender, eventArgs) {

        var id = sender.get_id();
        var TLCimNo;
        var combo;
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();

        if (id.toString() == "cb_overallOM") {
            TLCimNo = $find('<%=cb_overallOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallTL.ClientID %>");
        }

        if (id.toString() == "cb_overallbOM") {
            TLCimNo = $find('<%=cb_overallbOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbTL.ClientID %>");
        } 

        if (id.toString() == "cb_BvsKPIOM") {
            TLCimNo = $find('<%=cb_BvsKPIOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_BvsKPITL.ClientID %>");
        } 

        if (id.toString() == "cb_omtop5_fvsscore") {
            TLCimNo = $find('<%=cb_omtop5_fvsscore.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_tltop5_fvsscore.ClientID %>");
        }

        if (id.toString() == "cb_omtop5_avst") {
            TLCimNo = $find('<%=cb_omtop5_avst.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_tltop5_avst.ClientID %>");
        }

        if (id.toString() == "cb_triadcoachingom") {
            TLCimNo = $find('<%=cb_triadcoachingom.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_triadcoachingtl.ClientID %>");
        } 

        combo.get_items().clear();
        comboItem.set_text("All");
        comboItem.set_value('0');
        combo.trackChanges();
        combo.get_items().add(comboItem);
        comboItem.select();
        combo.commitChanges();

        $.ajax({
            type: 'GET',
            url: 'ChartHandlers/AgentListOM.ashx',
            data: { cim: TLCimNo },
            contentType: "application/json; charset=utf-8",
            dataType: 'JSON',
            success: function (data) {
                for (i = 0; i < data.Table.length; i++) {
                    var combo2 = combo;
                    var comboItem2 = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem2.set_text(data.Table[i].Name);
                    comboItem2.set_value(data.Table[i].CimNumber);
                    combo2.trackChanges();
                    combo2.get_items().add(comboItem2);
                    combo2.commitChanges();
                }
            },
            error: function (e) {
                console.log(e);
            }
        });

    }


    function OnClientSelectedIndexTL(sender, eventArgs) {

        var id = sender.get_id();

        var TLCimNo, combo;

        var comboItem = new Telerik.Web.UI.RadComboBoxItem();

        if (id.toString() == "cb_overallTL") {
            TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallAgent.ClientID %>");
        }

        if (id.toString() == "cb_overallbTL") {
            TLCimNo = $find('<%=cb_overallbTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbAgent.ClientID %>");
        }

        if (id.toString() == "cb_BvsKPITL") {
            TLCimNo = $find('<%=cb_BvsKPITL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_BvsKPIAgent.ClientID %>");
        }

        if (id.toString() == "cb_tltop5_fvsscore") {
            TLCimNo = $find('<%=cb_tltop5_fvsscore.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_agenttop5_fvsscore.ClientID %>");
        }

        if (id.toString() == "cb_tltop5_avst") {
            TLCimNo = $find('<%=cb_tltop5_avst.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_agenttop5_avst.ClientID %>");
        }

        if (id.toString() == "cb_triadcoachingtl") {
            TLCimNo = $find('<%=cb_triadcoachingtl.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_triadcoachingagent.ClientID %>");
        }
        
        combo.get_items().remove(comboItem);
        combo.get_items().clear();
        comboItem.set_text("All");
        comboItem.set_value('0');
        combo.trackChanges();
        combo.get_items().add(comboItem);
        comboItem.select();
        combo.commitChanges();

        $.ajax({
            type: 'GET',
            url: 'ChartHandlers/AgentListOM.ashx',
            data: { cim: TLCimNo },
            contentType: "application/json; charset=utf-8",
            dataType: 'JSON',
            success: function (data) {
                for (i = 0; i < data.Table.length; i++) {
                    var combox = combo;
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(data.Table[i].Name);
                    comboItem.set_value(data.Table[i].CimNumber);
                    combox.trackChanges();
                    combox.get_items().add(comboItem);
                    comboItem.select();
                    combox.commitChanges();
                }

            },
            error: function (e) {
                console.log(e);
            }


        });

    }

    function resetColor(v) {
        $.each($('.click-me-kpi'), function (key, xx) {
            var kpiid = $(xx).attr('kpi-id');

            if (kpiid == v) {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            } else {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            }

            $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

        });
    }

</script>
