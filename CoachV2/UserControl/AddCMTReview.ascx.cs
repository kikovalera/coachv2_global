﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class AddCMTReview : System.Web.UI.UserControl
    {
        public DataTable CN
        {
            get
            {
                return ViewState["CN"] as DataTable;
            }
            set
            {
                ViewState["CN"] = value;
            }
        }

        public DataTable PR
        {
            get
            {
                return ViewState["PR"] as DataTable;
            }
            set
            {
                ViewState["PR"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                GetAccount(CIMNumber);
                GetTLsPerAccount(Convert.ToInt32(RadAccount.SelectedValue));
                GetSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue));
                GetDropDownSessions();
                PopulateTopics();
            }
        }


        public void GetAccount(int CIMNumber)
        {
            try
            {
                
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "Account";
                RadAccount.DataValueField = "AccountID";
                RadAccount.DataBind();

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }

        }
        public void GetTLsPerAccount(int AccountID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount(AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
     
            }
        }

        protected void GetSubordinates(int CimNumber)
        {
            try
            {

                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
     
            }

        }
        protected void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }

        protected void PopulateTopics()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }

        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                PopulateTopics();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }

        protected void GetDropDownTopics(int SessionID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics(SessionID);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }

        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                GetTLsPerAccount(Convert.ToInt32(RadAccount.SelectedValue));
                GetSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue));

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                GetSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue));
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            VisibleCheckBox();
        }
        private void VisibleCheckBox()
        {
            if (RadSessionTopic.SelectedItem.Text != "Awareness" && RadSessionTopic.SelectedItem.Text != "1st Warning")
            {
                //CNPR.Visible = true;
                CheckBox1.Visible = true;
                CheckBox2.Visible = true;
            }
            else
            {
                //CNPR.Visible = false;
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
            }
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox1.Checked == true)
            {
                CoachingNotes();
                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetCoachingNotes(CoachingCIM);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RadGrid2.DataSource = ds;
                    RadGrid2.DataBind();
                    Window1.VisibleOnPageLoad = true;
                    //RadGridCN.Visible = true;
                }
                else
                {
                    string myStringVariable = "No data for previous coaching notes.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "script", myStringVariable, true);

                }
            }

            else
            {
                ViewState["CN"] = null;
                //RadGridCN.DataSource = null;
                //RadGridCN.DataBind();


            }
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox2.Checked == true)
            {
                PerformanceResults();
                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetPerformanceResults(CoachingCIM);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RadGrid3.DataSource = ds;
                    RadGrid3.DataBind();
                    Window2.VisibleOnPageLoad = true;
                    //RadGridPR.Visible = true;
                }
                else
                {
                    string myStringVariable = "No data for previous performance results.";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "script", myStringVariable, true);
                }
            }

            else
            {
                //RadGridPR.DataSource = null;
                //RadGridPR.DataBind();
                //RadGridPR.Visible = false;

            }
        }
        public void PerformanceResults()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewKPIID", typeof(string));
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Name", typeof(string));
            dt2.Columns.Add("Target", typeof(string));
            dt2.Columns.Add("Current", typeof(string));
            dt2.Columns.Add("Previous", typeof(string));
            dt2.Columns.Add("Driver_Name", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["PR"] = dt2;

            BindListViewPR();
        }
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
        }

        private void BindListViewCN()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                //RadGridCN.DataSource = dt;
                //RadGridCN.DataBind();
            }
            else
            {
                //RadGridCN.DataSource = null;
                //RadGridCN.DataBind();
            }


        }

        private void BindListViewPR()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                //RadGridPR.DataSource = dt;
                //RadGridPR.DataBind();
            }
            else
            {
                //RadGridPR.DataSource = null;
                //RadGridPR.DataBind();
            }


        }
    }
}