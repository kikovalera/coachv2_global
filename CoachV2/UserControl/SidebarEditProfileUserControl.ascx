﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarEditProfileUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.SidebarEditProfileUserControl" %>
<!--<div class="input-group">
    <input type="text" class="form-control" placeholder="Search&hellip;">
    <span class="input-group-btn">
        <button type="button" class="btn btn-default">
            Go</button>
    </span>
</div>-->
<br />
<div class="menu-content">
    <div class="panel panel-default">
        <div class="panel-body">
            <span class="glyphicon glyphicon-user"></span>&nbsp;My Profile</div>
    </div>
    <div class="list-group">
        <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/EditProfile.aspx" CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-user"></span> Profile Summary
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/EditProfile.aspx?tab=experience"
            CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-saved"></span> Experience
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/EditProfile.aspx?tab=education"
            CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-education"></span> Education
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/EditProfile.aspx?tab=skill"
            CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-saved"></span> Skills
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/EditProfile.aspx?tab=cert"
            CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-certificate"></span> Certifications
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/EditProfile.aspx?tab=extra" CssClass="list-group-item for-link-btn">
        <span class="glyphicon glyphicon-plus"></span> Additional Information
        </asp:HyperLink>
    </div>
</div>
