﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Charting;
//using Telerik.Windows;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class Ops_ChartAgents : System.Web.UI.UserControl
    {
        int intcim;
        protected void Page_Load(object sender, EventArgs e)
        {
            string URL = (HttpContext.Current.Request.Url.Host == "localhost" ? "http://" : "http://") + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
            
            FakeURLID.Value = URL;
            var mark1 = 0;
            var mark2 = 0;
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString(); //


            //cim_num = "10120001";
            string gmail = ds.Tables[0].Rows[0][13].ToString(); //
            intcim = Convert.ToInt32(cim_num);
            //dpstartdate.MinDate = DateTime.Today;
            //dpenddate.MinDate = DateTime.Today;
            if (!IsPostBack)
            {
                DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
                DataAccess ws = new DataAccess();
                int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
                //DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
                //DataSet ds_KPIforAgent = ws.GetKPIListPerAcct(Convert.ToString(acctno));
                
                DataSet ds_KPIforAgent = DataHelper.gettopkpis(intcim);
                DataSet ds_userselect = DataHelper.GetAllUserRoles(intcim);
                if (ds_userselect.Tables[0].Rows.Count > 0)
                {
                    DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(intcim, 1);
                    mark1 = 1;

                    DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(intcim, 2);
                    mark2 = 1;

                }

                if ((mark1 == 1) && (mark2 == 1))
                {

                    DataSet DS_GETSUB = DataHelper.Get_Subordinates(intcim);
                    if (DS_GETSUB.Tables[0].Rows.Count > 0)
                    {

                    }
                }

                MyCIM.Value = Convert.ToString(cim_num);
                StartDate.SelectedDate = DateTime.Today.AddMonths(-1); //Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01"); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
                EndDate.SelectedDate = DateTime.Now;
                ddKPIList2.AppendDataBoundItems = true;
                ddKPIList2.DataTextField = "KPIName";
                ddKPIList2.DataValueField = "KPIId";
                ddKPIList2.DataSource = DataHelper.GetAllKPIs();
                ddKPIList2.DataBind();

                //overall behaviordropdown
                dp_behavior_start.SelectedDate = DateTime.Today.AddMonths(-1); //Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01"); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
                dp_behavior_end.SelectedDate = DateTime.Now;
                cb_kpi_behavior1.DataSource = ds_KPIforAgent;
                cb_kpi_behavior1.DataBind();

                if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
                {
                    cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                    cb_kpi_behavior1.DataBind();
                }

                dp_behaviorkpi_start.SelectedDate = DateTime.Today.AddMonths(-1); //Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
                dp_behaviorkpi_end.SelectedDate = DateTime.Now;
                cb_kpi_behavior_kpi.DataSource = ds_KPIforAgent;
                cb_kpi_behavior_kpi.DataBind();

                if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
                {
                    cb_kpi_behavior_kpi.Items.Add(new RadComboBoxItem("N/A", "0"));
                    cb_kpi_behavior_kpi.DataBind();
                }


                dp_fcrglide_start.SelectedDate = DateTime.Today.AddMonths(-1);
                dp_fcrglide_end.SelectedDate = DateTime.Now;

                cb_FCR_Scoredt.DataSource = DataHelper.GetDataView();
                cb_FCR_Scoredt.DataBind();


                RadDatePicker1.SelectedDate = DateTime.Today.AddMonths(-1);
                RadDatePicker2.SelectedDate = DateTime.Now;

                RadComboBox1.DataSource = DataHelper.GetDataView();
                RadComboBox1.DataBind();


                DataSet ds_DVlist = DataHelper.GetDataView();
                cb_overalldataview.DataSource = ds_DVlist;
                cb_overalldataview.DataBind();


                dlKPI.DataSource = DataHelper.gettopkpis(intcim);
                dlKPI.DataBind();

                
                  
            }
        }


    }
}