﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Charting;
//using Telerik.Windows;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class PerfGraphUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MyCIM.Value = Page.Request.QueryString["CIM"];

                //ddKPIList.Items.Clear();
                // try
                //{
                ddKPIList2.AppendDataBoundItems = true;
                ddKPIList2.DataTextField = "KPIName";
                ddKPIList2.DataValueField = "KPIId";
                ddKPIList2.DataSource = DataHelper.GetAllKPIs();

                ddKPIList2.DataBind();

                StartDate.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
                EndDate.SelectedDate = DateTime.Now;

                //}
                //catch (Exception ex)
                //{
                //   fakerr.Text = "Message " + ex.Message;
                //}

                string URL = "http://"+ HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);

                FakeURLID.Value = URL;

                int intcim = Convert.ToInt32(MyCIM.Value);
                DataSet ds_kpi_hierarchy = DataHelper.gettopkpis(intcim);
                if (ds_kpi_hierarchy.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_kpi_hierarchy.Tables[0].Rows.Count; ctr++)
                    {
                        if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "1")
                        {
                            
                            btn_top1kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                            
                        }
                        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "2")
                        {
                            btn_top2kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        }

                        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "3")
                        {  
                            btn_top3kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        }
                        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "4")
                        {
                            //lbl_4thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                            //lbl_4thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                            btn_top4kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        }
                        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "5")
                        {
                            //lbl_5thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                            //lbl_5thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                            btn_top5kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        }
                    }
                }
            }
        }
    }
}