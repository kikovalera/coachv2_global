﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMain.ascx.cs" Inherits="CoachV2.UserControl.AboutMain" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 <div class="menu-content bg-alt">
<div class="panel menuheadercustom">
    
        &nbsp;<span class="fa fa-question-circle" style="font-size:30px;"> </span> About <span class="breadcrumb2ndlevel">
        <asp:Label ID="Label1" runat="server" Text=" "></asp:Label></span>
</div>
 
     <asp:LinkButton runat="server" ID="btn_cv1" OnClick="btn_coachv2_OnClick">   <span class="fa fa-comments" style="font-size:20px;"></span>  The Coach v2 </asp:LinkButton>
     &nbsp;
     <asp:LinkButton runat="server" ID="btn_userguide" Visible="false" OnClick="btn_userguide_OnClick"><i class="material-icons" style="font-size:20px;">directions</i>   User Guide </asp:LinkButton>
     &nbsp;
     <asp:LinkButton runat="server" ID="btn_references" Visible="false" OnClick="btn_references_OnClick"><i class="glyphicon glyphicon-tasks" style="font-size:20px;"></i>   References </asp:LinkButton>
  
     <asp:Panel runat="server" ID="pn_coachv2" Visible="true">
         <div class="panel-group" id="pn_what" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="pn_what1" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>What is the Coach Tool? <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseOne" class="panel-collapse collapse in">
                     <div class="panel-body">
                         <p  style="font-size:12px"  >   
                             One of the primary uses of this online tool is documentation of a coaching or review
                             session between an immediate head and his/her direct reports.
                         </p>
                         <p  style="font-size:12px"  >
                             Another use of this online tool is the generation and tracking of all coaching instances
                             via the ''Performance Dashboard'' or Trends Visualization. At a glance, Immediate Heads and Managers
                             are able to view coaching frequencies and per employee per KPI and per department.
                         </p>
                         <p  style="font-size:12px" >
                             The Coach also provides Operations Managers the ability to record and rate any observed
                             coaching session (Triad Coaching) between a Team Leader and an Agent; Immediate
                             Head and direct reports.
                         </p>
                         <p  style="font-size:12px"  >
                             The tool serves as a document repository for all uploaded documents pertaining to
                             a coaching session (and ECN instances). Where, these documents may be used as a
                             reference during an actual coaching session or as an additional reference in cases
                             of Disciplinary Action progression.
                         </p>
                     </div>
                 </div>
             </div>
         </div>
         <div class="panel-group" id="pn_who" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="pn_who1" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Who uses the tool? <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseTwo" class="panel-collapse collapse in">
                     <div class="panel-body">
                         <p  style="font-size:12px"  >
                             Any active employee within Transcom's network may access and use the tool. This
                             is regardless of the person's organizational affinity and role/position and/or job
                             function.
                         </p>
                         <p  style="font-size:12px"  >
                             The access and view to the features and functionalities of the tool will depend
                             on the level of access granted to a user. The access granted is based on a person's
                             role/position and/or job function (ex. Agent's view/navigation vs. HR's).&nbsp;
                         </p>
                         <p  style="font-size:12px"  >
                             Employee view and navigation are categorized into 6 roles, namely, Agent/Direct
                             Reports, Team Leader/Immediate Head, Operations Manager, Director, HR, and Administrator.
                             During a coaching session, there would be 2 characters that will come into play,
                             the ''Coachee'' (Agent/Direct Reports) and the ''Coacher'' (any one from the Supervisor
                             level up and w/ Direct Reports).
                         </p>
                         <p  style="font-size:12px"  >
                             Immediate Heads and/or Team Leaders are required to record/document and attach pertinent
                             documents on every instance of a coaching session, regardless of the Session Type
                             and Topic (ex. Attendance>Awareness or Attendance>For Final Warning)
                         </p>
                     </div>
                 </div>
             </div>
         </div>
         <div class="panel-group" id="pn_Features" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Pn_Features1" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Features the tool? <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseThree" class="panel-collapse collapse in">
                     <div class="panel-body">
                         <p  style="font-size:12px"  >
                             Since the first version of the tool (The Coach v1), there have been additional features
                             and functionalities incorporated to the v2 of the tool. Some of the existing features
                             (of the v1) have been enhanced to cater to the ever changing need of the business
                             and of course, to provide a better User Experience.
                         </p>
                         <p  style="font-size:12px"  > 
                             One of the most noticeable enhancements made on the tool is the User Interface.
                             Moving away from the usual white and text-heavy web tool design to a more aesthetically
                             pleasing pages.&nbsp;
                         </p>
                         <p  style="font-size:12px"  >
                             Another enhancement added is the "Smart" feature of the tool, where, the forms and
                             buttons presented to a user dynamically adapts to a user's profile and role/job
                             function (ex. a Team Leader under Comcast will see an entirely different coaching
                             form form those who are under Talk Talk).
                         </p>
                         <p  style="font-size:12px"  >
                             Users can now see the different statuses of their coaching tickets (and ECNs) on
                             the My Coaching page. Users are alerted if they have new reviews or if they have overdue
                             sign-offs.&nbsp;
                         </p>
                         <p  style="font-size:12px"  >
                             Click this button to view the Functional Requirements&nbsp;
                         </p>
                     </div>
                 </div>
             </div>
         </div>
         <div class="panel-group" id="pn_Difference" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="pn_Difference1" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Difference between The Coach v1 and The Coach v2<span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseFour" class="panel-collapse collapse in">
                     <div class="panel-body"  style="font-size:12px" >
                         Below is a quick reference to the features and functionalities of The Coach v1 and
                         The Coach v2:
                     </div>
                     <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>
                                     <p  style="font-size:12px"  >
                                         Features and Functionalites</p>
                                 </th>
                                 <th>
                                     <p  style="font-size:12px"  >
                                         The Coach v1</p>
                                 </th>
                                 <th>
                                     <p  style="font-size:12px"  >
                                         The Coach v2</p>
                                 </th>
                             </tr>
                         </thead>
                         <tbody>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; TL-Agent Feedback - gather and store TL-Agent feedback</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Session Type - register, store and display coaching Session Types</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  > 
                                         &nbsp; Follow-up Date - register, store and display coaching Follow-up Dates</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Target Input - register and store Target Input</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Remote Coaching - ability to remote coach agents/employees from other regions</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; 2nd Level Supervisor Feedback - record, gather, store and display</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Previous Logs - ability to review Previous Logs</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; KPI-based Review - ability to conduct KPI-based reviews</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px" >
                                         &nbsp; Documentation Upload - ability to upload, store and view documents</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p style="font-size:12px" >
                                         &nbsp; Mass Reviews - ability to select individuals, groups or all</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px" >
                                         &nbsp; Previous Commitment View - ability to view previous commitments</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Previous Performance Results View - ability to view previous performance
                                         results</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Include Previous Coaching Notes - ability to view, select and include previous
                                         coaching notes</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Include Previous Performance Results - ability to view, select and include
                                         previous performance results</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Driver / Behavior Data - ability to gather, record and view Driver/Behavior
                                         Data</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"   > 
                                         &nbsp; User Notifications - ability to send alerts on coaching ticket statuses</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                             <tr>
                                 <th scope="row">
                                     <p  style="font-size:12px"  >
                                         &nbsp; Coach Assignment - ability to assign coaching to TL/Immediate Heads (from
                                         QA/Trainer or HR)</p>
                                 </th>
                                 <td>
                                     <span class="glyphicon glyphicon-remove"></span>
                                 </td>
                                 <td>
                                     <span class="glyphicon glyphicon-ok"></span>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
     </asp:Panel>

     <asp:Panel runat="server" ID="pn_userguide"  Visible="false">
        <div class="panel-group" id="Div1" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div2" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseOneUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Getting Started <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseOneUG" class="panel-collapse collapse in">
                     <div class="panel-body">   
                         <p style="font-size: 12px">
                             To access the tool, follow the steps below:
                         </p>
                         <br />
                         <p style="font-size: 12px">
                             STEP 1: Type the tool's address in the address bar</p>
                            <img id="Img1" src="../Content/Userguide/1.PNG" class="img-responsive" runat="server"  alt="gs1"/>
                         <p align="center" style="font-size: 12px">
                             <i>Figure 1: A replica image of Google's Address Bar</i></p>
                         <%--picture--%>
                         <p style="font-size: 12px">
                             STEP 2: Hit this button
                         </p>
                           <img id="Img2" src="../Content/Userguide/Enterbtn.PNG" class="img-responsive" runat="server"  alt="gs1"/>
 
                         <br />
                         <p style="font-size: 12px">
                             If the address is correct, then you should be looking at a page that looks like
                             this:
                         </p>
                        <img id="Img3" src="../Content/Userguide/2.PNG" class="img-responsive" runat="server"  alt="gs1"/>
                         <p align="center" style="font-size: 12px">
                             <i>Figure 2.1: The Coach v2's Homepage (logged out state)</i></p>
                         <p style="font-size: 12px">
                             This is the new face of The Coach, it's The Coach v2</p>
                         <p style="font-size: 12px">
                             STEP 3: Click the icon/link that says ''Login'' to start accessing your account.
                             See marking below for the link's location.
                         </p>
                         <img id="Img4" src="../Content/Userguide/3.PNG" class="img-responsive" runat="server"  alt="gs1"/>
                         <p align="center" style="font-size: 12px">
                             <i>Figure 2.2: The Coach v2's Homepage (logged out state) highlighting the ''Login''
                                 link</i></p>
                         <br />
                         <p style="font-size: 12px">
                             Once logged in, you will notice a few things added to the previous page view.</p>
                         <img id="Img5" src="../Content/Userguide/4.PNG" class="img-responsive" runat="server"  alt="gs1"/>
                         <p align="center" style="font-size: 12px">
                             <i>Figure 2.3: User's Homepage (logged in state) labelled parts</i></p>
                         <p style="font-size: 12px">
                             This is the User's Homepage. From here, you will notice that the tool greets you
                             (1), Navigation Links for navigating through the pages (2), a Logout link (3), links
                             to re-direct you to other systems integrated with The Coach v2 (4), and Quick links
                             at the Footer section for further references and readings (5).
                         </p>
                         <p style="font-size: 12px">
                             From here on, the navigation will depend solely on what view or task you wish to
                             accomplish.</p>
                         <p style="font-size: 12px">
                             Refer to the other sections (accordion menu) of this page for the step-by-step processes
                             on how to access/navigate the features and functionalities of The Coach v2.</p>
                     </div>
                </div>
                </div>
                </div>
             
          <div class="panel-group" id="Div6" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div7" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwoUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Accessing My Profile Page <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseTwoUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     <p    style="font-size:12px">To access your profile page, follow the one-click step described below:</p>
                     <br />
                     <p   style="font-size:12px">STEP: From your Homepage (logged in), click on the ''My Profile'' link on the Navigation bar.</p>
                        <img id="Img6" src="../Content/Userguide/5.PNG" class="img-responsive" runat="server"  alt="gs1"/> 
                       <p  align="center" style="font-size:12px"><i>Figure 2.4: User's Homepage (logged in state) highlighting ''My Profile'' Navigation link</i></p>
                     <br />
                     <p  style="font-size:12px">You will immediately see your profile page.</p>
                     <img id="Img7" src="../Content/Userguide/6.PNG" class="img-responsive" runat="server"  alt="gs1"/> 
                     <p  align="center" style="font-size:12px"><i>Figure 3.1: User's My Profile Page </i> </p>
                     <br />
                     <p  style="font-size:12px" >There are three (3) sections on this page: (1) Left Panel Information, (2) Profile picture/avatar and the (3) View Section or the Profile Summary</p>
                     <br />
                     <p  style="font-size:12px" > The default view of this page has all the sections (accordion menu) of the View Section expanded. But, for presentation purposes of this User Guide, the Personal Information and Contact Information sections are collapsed. </p>
                     <br />
                     <p  style="font-size:12px" >You can view the contents of the collapsed sections simply by clicking on the the section header (A) or the down-arrows (B). </p>
                     <br />  
                     <p style="font-size:12px" >From the Left Panel Information, you can also navigate and view the other sections of your profile, which are not displayed on the View Section as it is the summary of your profile.</p>
                     <br />
                     <p style="font-size:12px" >
                     To view those sections, simply click on a menu from the Left Panel Information.
                     </p>
                     <%--pic--%><%-- on the righside add this --%>
                     <table>
                       
                         <tbody>
                                <tr>
                                 <th>
                                    <img id="Img9" src="../Content/Userguide/7.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                                 </th>
                                 <th>
                                    <p style="font-size:12px" align="right"    > As an example, if you click on the ''Experience'' menu of the Left Panel Information...</p>
                                 </th>
                                 
                             </tr>
                             </tbody>
                             </table>
 
                     <p style="font-size:12px; text-indent:80px"  align="left"  > <i>  Figure 3.2: Left Panel Information </i></p> 
                     <br />
                     <p style="font-size:12px">...this is how it will look like on the View Section</p>
                      <img id="Img8" src="../Content/Userguide/8.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p style="font-size:12px"  align="center" > <i> Figure 3.3: User's My Profile Page, 'displaying 'My Experience'' View Section </i> </p>
                     <p style="font-size:12px" >
                     To edit information on your profile page, just click on the ''Edit'' icon. See marking below:
                     </p>
                      <img id="Img10" src="../Content/Userguide/9.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p style="font-size:12px"  align="center" > <i>  Figure 3.4: User's My Profile Page, ''Edit'' icon higlighted</i> </p>
                     <br />
                     <p  style="font-size:12px"> (Note: There are ''editable'' and ''non-editable'' fields on your profile page. You may notice that some of the fields are ''grayed-out'' and some are open for editing.)  
                     </p>
                     <p  style="font-size:12px" > Update information on the editable fields as necessary.
                     </p>
                     <div class="col-md-12 text-right">                     
                      
                        <img id="myImg" src="../Content/Userguide/processmap.PNG" alt="Process1"   />

                        <!-- The Modal -->
                        <div id="myModal" class="modal">
                          <span class="close">&times;</span>
                          <img class="modal-content" id="img12" src="../Content/Userguide/10.png">
                          <div id="caption"></div>
                        </div>
                        </div>
                      
                     </div>
                </div>
                </div>
                </div>

           <div class="panel-group" id="Div9" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div10" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseThreeUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Accessing the My Coaching Page <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseThreeUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     <p    style="font-size:12px">
                     To access your My Coaching page, follow the one-click step described below:
                     </p>
                     <br />
                     <p style="font-size:12px">STEP 1 : From your Homepage (logged in), click on the ''My Coaching'' link on the Navigation bar.
                     </p>
                     <img id="Img12" src="../Content/Userguide/11.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p  style="font-size:12px">
                     You will immediately see your My Coaching page.
                     </p>
                     <br /> 
                     <img id="Img13" src="../Content/Userguide/12.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p style="font-size:12px" >There are two (2) sections on this page: (1) Left Panel Information, (2) Coaching Dashboard Section.
                     </p>
                     <p style=" color:Red; font-size:12px  " >The Coaching Dashboard Section serves as an ''organizer'' of the different Coaching Ticket statuses (ex. For Termination, For Final Warning, Overdue Follow-Ups, Overdue Sign-Offs, etc...) and as an alert/notification page showing the count of Coaching Ticket #'s per status.&nbsp;</p>
                     <p style="font-size:12px  ">The Coaching Dashboard may contain multiple Coaching Ticket statuses, and within each status or section, there could be multiple lines of Coaching Ticket #'s. Thus, the default view of the Coaching Dashboard Page is always in collapsed accordion view.</p>
                     <p  style=" color:Red; font-size:12px  ">To expand and view the contents of each section, you can simply click on the section headers (A) or the drop-down arrows (B). See example below when the ''Overdue Follow-Ups'' section is clicked:</p>
                     <img id="Img14" src="../Content/Userguide/13.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p  style=" color:Red; font-size:12px  ">Figure 4.3: Coaching Dashboard Section showing contents of the ''Overdue Follow-Ups'' section</p>
                     <p style=" font-size:12px  " >From the Left Panel Information, you can go to your ''My Reviews'' page by clicking on the ''My Reviews'' menu</p>
                     <p style="  font-size:12px  " > To view those sections, simply click on a menu from the Left Panel Information.</p> 
                     <img id="Img15" src="../Content/Userguide/14.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p style="font-size:12px "  align="left"  > <i>Figure 4.4: Left Panel Information, Coaching Dashboard Page</i></p> 
                     <p style="font-size:12px"  > From here on, the navigation will depend solely on what view or task you wish to accomplish.</p> 
                     <p style="font-size:12px"   > Refer to the next section (accordion menu), ''Accessing My reviews Page'' for the step-by-step processes on how to access/navigate your reviews page.</p> 
                      <div class="col-md-12 text-right">                     
                      
                        <img id="myImg1" src="../Content/Userguide/processmap.PNG" alt="Process2"   />

                        <!-- The Modal -->
                        <div id="Div15" class="modal">
                          <span class="close">&times;</span>
                          <img class="modal-content" id="img18" src="../Content/Userguide/24.png">
                          <div id="Div16"></div>
                        </div>
                        </div>

                      </div>
                     </div>
                     </div>
                </div>
             
      
      <div class="panel-group" id="Div12" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div13" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseFourUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span> Accessing My Reviews Page<span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseFourUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     <p style="font-size:12px"  >On the Left Panel Information (1) of the Coaching Dashboard page, click the "My Reviews" (A) menu.</p>
                     <img id="Img17" src="../Content/Userguide/16.PNG" class="img-responsive" runat="server"  alt="gs1"   /> 
                     <p style="font-size:12px "  align="center"  > <i>Figure 4.3: User's Coaching Dashboard Page w/ the Left Panel Information zoomed out</i></p>
                     <p style="font-size:12px "     >  On My Reviews Page, you will see a list of all the reviews created and/or assigned to you, regardless of the status. </p>
                     <img id="Img18" src="../Content/Userguide/17.PNG" class="img-responsive" runat="server"  alt="gs1" /> 
                     <p style="font-size:12px " align="center" > <i> 4.3: User's Coaching Dashboard Page w/ the Left Panel Information zoomed out </i></p>
                     <p style="font-size:12px"> Scroll, select and click on your name to open a review session you wish to view. See markings above.</p>
                     <p style="font-size:12px"> The default view of this page has all the sections (accordion menu) of the View Section expanded. But, for presentation purposes of this User Guide, only the Career Profile section is expanded while the other sections such as the Performance Results, Strengths, Opportunities, Commitment, and Documentation sections are collapsed.</p>
                     <img id="Img19" src="../Content/Userguide/18.PNG" class="img-responsive" runat="server"  alt="gs1" />                      
                     <p style="font-size:12px " align="center"><i> Figure 4.6: A copy of a review yet to be signed off. Coachee Sign Off button zoomed out.</i></p>
                     <p style="font-size:12px"> Expanded Performance Results section of the same review form.</p>
                     <img id="Img20" src="../Content/Userguide/19.PNG" class="img-responsive" runat="server"  alt="gs1" />                      
                     <p style="font-size:12px "  align="center"  > <i>Figure 4.6.1: Performance Results section expanded.</i> </p>
                     <p style="font-size:12px"> Expanded Strengths section of the same review form.</p>
                     <img id="Img21" src="../Content/Userguide/20.PNG" class="img-responsive" runat="server"  alt="gs1" />                   
                     <p style="font-size:12px " align="center"><i>Figure 4.6.2: Strengths section expanded.</i></p>
                     <p style="font-size:12px"> Expanded Opportunities section of the same review form.</p>
                     <img id="Img22" src="../Content/Userguide/21.PNG" class="img-responsive" runat="server"  alt="gs1" />                   
                     <p style="font-size:12px " align="center"><i>Figure 4.6.3: Opportunities section expanded.</i></p>
                     <p style="font-size:12px"> Expanded Commitment section of the same review form.</p>
                     <img id="Img23" src="../Content/Userguide/22.PNG" class="img-responsive" runat="server"  alt="gs1" />
                     <p style="font-size:12px " align="center"><i>Figure 4.6.4: Commitment section expanded.</i></p>             
                     <p style="font-size:12px"> Expanded Documentation section of the same review form.</p>
                     <img id="Img24" src="../Content/Userguide/23.PNG" class="img-responsive" runat="server"  alt="gs1" />                            
                     <p style="font-size:12px " align="center"><i>Figure 4.6.5: Documentation section expanded.</i></p>             
                     <p style="font-size:12px"> Refer to the next section (accordion menu), ''Signing Off a Review Session'' for the step-by-step processes on how to access and sign off a review session. </p>
                    <div class="col-md-12 text-right">                     
                      
                        <img id="myImg1" src="../Content/Userguide/processmap.PNG" alt="Process3"   />

                        <!-- The Modal -->
                        <div id="Div20" class="modal">
                          <span class="close">&times;</span>
                          <img class="modal-content" id="img16"  >
                          <div id="Div21"></div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>

       <div class="panel-group" id="Div17" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div18" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseFiveUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>   Signing Off a Review Session <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseFiveUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     
                     </div>
                </div>
                </div>
                </div>
        <div class="panel-group" id="Div22" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div23" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseSixUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>   Accessing My ECNs Page* <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseSixUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     
                     </div>
                </div>
                </div>
                </div>
                <div class="panel-group" id="Div25" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div26" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseSevenUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>    Accomplishing an ECN Form*<span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseSevenUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     
                     </div>
                </div>
                </div>
                </div>
                         <div class="panel-group" id="Div28" runat="server">
             <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
             <div id="Div29" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseEightUG">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>    Accessing the linked pages<span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseEightUG" class="panel-collapse collapse in">
                     <div class="panel-body">
                     
                     </div>
                </div>
                </div>
                </div>
     </asp:Panel>
    
     <asp:Panel ID="pn_references" runat="server" Visible = "false">
        <div class="panel-group" id="pn_incidentmngt" runat="server">
            <div class="row row-custom">
                 <div class="col-sm-5">
                 </div>
             </div>
              <div id="DivGoogleForm" runat="server">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseReferences">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-top triangletop">
                                 </span>Reference Links <span class="label label-default"></span>
                             </h4>
                         </div>
                     </a>
                 </div>
                 <div id="collapseReferences" class="panel-collapse collapse in">
                     <div class="panel-body">
                         <asp:HyperLink ID="GoogleFormsLink" runat="server" Target="_blank"></asp:HyperLink>
                         <br /><br />
                         <asp:HyperLink ID="SurveyLink" runat="server" Target="_blank"></asp:HyperLink>
                         <br /><br />
                         <asp:HyperLink ID="GuidelinesLink" Visible = "false" runat="server" Target="_blank"></asp:HyperLink>
                     </div>
                 </div>
             </div>
        </div>
     </asp:Panel>
</div>

<style type="text/css" >
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>


<style type="text/css" >
#myImg1 {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg1:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<style type="text/css" >
#myImg2 {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg2:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>

<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg1');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg2');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>

