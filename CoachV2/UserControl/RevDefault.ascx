﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevDefault.ascx.cs" Inherits="CoachV2.UserControl.RevDefault" %>
<div class="panel-group" id="accordion">
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseDescription">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Description <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                </h4>
            </div>
        </a>
        <div id="collapseDescription" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Description"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false"
                    Visible="true">
                </telerik:RadTextBox>
            </div>
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Performance Result <span class="glyphicon glyphicon-triangle-top triangletop">
                    </span>
                </h4>
            </div>
        </a>
        <div id="collapseThree" class="panel-collapse collapse in">
            <div class="panel-body">
                <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                    Perfomance Results</label><br />
                <div style="height: 5px">
                </div>
                <div>
                    &nbsp;</div>
                <div>
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGrid1" RenderMode="Lightweight" runat="server" CssClass="RemoveBorders"
                            AutoGenerateColumns="true">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
                <div>
                    &nbsp;</div>
                <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                    Previous Coaching Notes</label><br />
                <div style="height: 5px">
                </div>
                <div>
                    &nbsp;</div>
                <div>
                    <div class="panel-body">
                        <telerik:RadGrid ID="grd_Coaching_Notes" CssClass="RemoveBorders" RenderMode="Lightweight"
                            runat="server" AutoGenerateColumns="true">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
                <div>
                    &nbsp;</div>
                <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                    Previous Performance Results</label><br />
                <div style="height: 5px">
                </div>
                <div>
                    &nbsp;</div>
                <div>
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                            <MasterTableView>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="CIM Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Session">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Target">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Current">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Previous">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Driver">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Assigned By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                </Scrolling>
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Strengths <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                </h4>
            </div>
        </a>
        <div id="collapseFour" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strengths"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ValidationGroup="AddReview"
                    ControlToValidate="RadStrengths" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                    CssClass="validator" Visible="false"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <%--Strenghts--%>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Opportunities <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                </h4>
            </div>
        </a>
        <div id="collapseFive" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunities"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                    ControlToValidate="RadOpportunities" Display="Dynamic" ErrorMessage="*This is a required field."
                    ForeColor="Red" CssClass="validator" Visible="false"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <%--Opportunities--%>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Commitment <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                </h4>
            </div>
        </a>
        <div id="collapseSix" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadCommitment" runat="server" class="form-control" placeholder="Commitment"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false"
                    Visible="true">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                    ControlToValidate="RadCommitment" Display="Dynamic" ErrorMessage="*This is a required field."
                    ForeColor="Red" CssClass="validator" Visible="false"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <%-- Commitment--%>
    <asp:Panel ID="pn_mass" runat='server' Visible="false">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAgenda">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Agenda <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseAgenda" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadTextBox1" runat="server" class="form-control" placeholder="Description"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false"
                        Visible="false">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                        ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                        CssClass="validator" Visible="false"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentation">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                </h4>
            </div>
        </a>
        <div id="collapseDocumentation" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadGrid ID="grd_Documentation" runat="server" Visible="true">
                </telerik:RadGrid>
                <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                    AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                    OnItemDataBound="RadGridDocumentation_onItemDatabound" CssClass="RemoveBorders"
                    BorderStyle="None">
                    <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Item Number">
                                <ItemTemplate>
                                    <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                AllowSorting="true" Target="_blank" ShowSortIcon="true">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                <ItemTemplate>
                                    <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                <ItemTemplate>
                                    <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowResizeToFit="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</div>
