﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class KPIMaintenance : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            int int_cim = Convert.ToInt32(cim_num);
            
            //DataSet ds_KPIList = DataHelper.GetKPIIList_admin();
            //grd_KPILIst.DataSource = ds_KPIList;

            DataSet ds_KPIList2 = DataHelper.GetKPIIList_admin();
            RadGrid1.DataSource = ds_KPIList2;
            lblForKpiCreated.Text = "KPIs Created by Admin : " + Convert.ToString(ds_KPIList2.Tables[0].Rows.Count);
        }

        //protected void radgrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        //{
        
        //    if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
        //    {
        //        try
        //        {
        //            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //            string cim_num = ds.Tables[0].Rows[0][2].ToString();

        //            //GridEditFormItem updateItem = (GridEditFormItem)e.Item;
        //            //int rowindex = updateItem.ItemIndex;
        //            //Label kpiid = (Label)updateItem.FindControl("txtRow");
        //            //RadComboBox RadKPI = (RadComboBox)updateItem.FindControl("cb_kpiname");
        //            //RadComboBox Radvalue = (RadComboBox)updateItem.FindControl("cb_valuetype");
        //            //RadComboBox Radlinkto = (RadComboBox)updateItem.FindControl("cb_linkto");
        //            //RadComboBox RadAccount = (RadComboBox)updateItem.FindControl("cb_account");
        //            //string kpiname_text = RadKPI.SelectedItem.Text;

        //            //DataSet ds_ifexist = DataHelper.GetAssignedKPI(Convert.ToInt32(cb_kpiname.SelectedValue), Convert.ToInt32(cb_account.SelectedValue), Convert.ToInt32(cb_valuetype.SelectedValue), Convert.ToInt32(cb_linkto.SelectedValue));
        //            //if (ds_ifexist.Tables[0].Rows.Count > 0)
        //            //{ // cb_kpiname.Text
        //            //    string ModalLabel = "KPI : " + "KPINAME" + " already Assigned to : " + cb_account.Text;
        //            //    string ModalHeader = "Error Message";
        //            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
        //            //}
        //            //else
        //            //{

        //            //    DataHelper.UpdateKPIAdmin(Convert.ToInt32(kpiid.Text), Convert.ToString(RadKPI.SelectedValue), Convert.ToString(kpiname_text), Convert.ToInt32(Radvalue.SelectedValue), Convert.ToInt32(Radlinkto.SelectedValue), Convert.ToString(cim_num), Convert.ToInt32(RadAccount.SelectedValue), DateTime.Now);
        //            //    string ModalLabel = "Update on KPI : " + kpiname_text + " Sucessful!";
        //            //    string ModalHeader = "Success Message";
        //            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //            //    //string myStringVariable = "Update on KPI : " + kpiname_text + " Sucessful!";
        //            //    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
        //            //    //       "alert('" + myStringVariable + "');", true);
        //            //    RadGrid1.Focus();
        //            //}

        //        }
        //        catch (Exception ex)
        //        {
                   
        //            string ModalLabel = ex.ToString();
        //            string ModalHeader = "Error Message";
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //        }
        //        RadGrid1.MasterTableView.ClearEditItems();
        //        Response.AppendHeader("Refresh", "0.1");
        //        RadGrid1.Focus();
        //    }

        //}

        //protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        //{
        //    try
        //    {
        //        //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //        //string cim_num = ds.Tables[0].Rows[0][2].ToString();


        //        //DataHelper.DeleteKPIAdmin(Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Row")), Convert.ToString(cim_num), DateTime.Now);
        //        ////string myStringVariable = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!"; 
        //        ////ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
        //        ////           "alert('" + myStringVariable + "');", true);

        //        //string ModalLabel = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!";
        //        //string ModalHeader = "Success Message";
        //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //        //Response.AppendHeader("Refresh", "0.1");
        //        //RadGrid1.Focus();

        //    }
        //    catch (Exception ex)
        //    {
        //        string ModalLabel = ex.ToString();
        //        string ModalHeader = "Error Message";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //        RadGrid1.Focus();
        //    }
        //}

        //protected void radgrid1_databound(object sender, GridItemEventArgs e)
        //{

        //    if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
        //    {
        //        GridEditableItem edititem = (GridEditableItem)e.Item;
                
        //        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("cb_kpiname");
        //        DataSet ds = new DataSet();
        //        //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
        //        ds = DataHelper.GetKPIFromScorecardList(); //GetKPIFromScorecard();
        //        comboBrand.DataSource = ds;
        //        comboBrand.DataBind();
        //        //comboBrand.SelectedValue = selectedItem.Value;
        //        RadComboBox combo = (RadComboBox)edititem.FindControl("cb_kpiname");
        //        RadComboBoxItem selectedItem = new RadComboBoxItem();
        //        selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPI Name"].ToString();
        //        selectedItem.Value = ((DataRowView)e.Item.DataItem)["KPI_ID"].ToString();
        //        combo.Items.Add(selectedItem);
        //        selectedItem.DataBind();
        //        Session["KPI Name"] = selectedItem.Value;
        //        combo.SelectedValue = selectedItem.Value;

        //        RadComboBox combo1 = (RadComboBox)edititem.FindControl("cb_valuetype");
        //        RadComboBoxItem selectedItem1 = new RadComboBoxItem();
        //        selectedItem1.Text = ((DataRowView)e.Item.DataItem)["Value Type"].ToString();
        //        selectedItem1.Value = ((DataRowView)e.Item.DataItem)["valuetype"].ToString();
        //        combo1.Items.Add(selectedItem1);
        //        selectedItem1.DataBind();
        //        Session["Value Type"] = selectedItem.Value;
        //        RadComboBox comboBrand1 = (RadComboBox)edititem.FindControl("cb_valuetype");
        //        DataSet ds1 = new DataSet();
        //        //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
        //        ds1 = DataHelper.GetValueto();
        //        comboBrand1.DataSource = ds1;
        //        comboBrand1.DataBind();
        //        comboBrand1.SelectedValue = selectedItem1.Value;

        //        RadComboBox combo2 = (RadComboBox)edititem.FindControl("cb_linkto");
        //        RadComboBoxItem selectedItem2 = new RadComboBoxItem();
        //        selectedItem2.Text = ((DataRowView)e.Item.DataItem)["Assign Type"].ToString();
        //        selectedItem2.Value = ((DataRowView)e.Item.DataItem)["linkto"].ToString();
        //        combo2.Items.Add(selectedItem2);
        //        selectedItem2.DataBind();
        //        Session["Assign Type"] = selectedItem2.Value;
        //        RadComboBox comboBrand2 = (RadComboBox)edititem.FindControl("cb_linkto");
        //        DataSet ds2 = new DataSet();
        //        ds2 = DataHelper.GetKPILinkTo();
        //        comboBrand2.DataSource = ds2;
        //        comboBrand2.DataBind();
        //        comboBrand2.SelectedValue = selectedItem2.Value;

        //        RadComboBox combo3 = (RadComboBox)edititem.FindControl("cb_account");
        //        RadComboBoxItem selectedItem3 = new RadComboBoxItem();
        //        selectedItem3.Text = ((DataRowView)e.Item.DataItem)["Assigned to"].ToString();
        //        selectedItem3.Value = ((DataRowView)e.Item.DataItem)["ACCOUNTID"].ToString();
        //        combo3.Items.Add(selectedItem3);
        //        selectedItem3.DataBind();
        //        Session["Assigned to"] = selectedItem.Value;
        //        RadComboBox comboBrand3 = (RadComboBox)edititem.FindControl("cb_account");
        //        DataSet ds3 = new DataSet();
        //        ds3 = DataHelper.GetAccountsforKPI();
        //        comboBrand3.DataSource = ds3;
        //        comboBrand3.DataBind();
        //        comboBrand3.SelectedValue = selectedItem3.Value;



        //    }

        //}

    }
}