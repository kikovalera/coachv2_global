﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditCertUserControl.ascx.cs" Inherits="CoachV2.UserControl.ProfileEditCertUserControl" %>
   <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
        <%--<a href="javascript:history.go(-1)" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>--%>
        <a href="MyProfile.aspx?tab=cert" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a> 
        <div>
            &nbsp;<span class="glyphicon glyphicon-certificate"></span> My Certificate
        </div>
    </div>
    <telerik:radgrid id="ExpGrid" runat="server" autogeneratecolumns="false" allowsorting="true"         
        onneeddatasource="ExpGrid_NeedDataSource" 
        oninsertcommand="ExpGrid_InsertCommand" 
        ondeletecommand="ExpGrid_DeleteCommand" 
        onitemdatabound="ExpGrid_ItemDataBound" 
        onupdatecommand="ExpGrid_UpdateCommand" OnItemCommand="ExpGrid_ItemCommand" RenderMode="Auto">
        <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" DataKeyNames="CertID"
            EditMode="EditForms">
            <EditFormSettings>
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add certificate"
                ShowExportToCsvButton="true" />
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Edit" ID="EditLink" runat="server"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="CertificateName" SortExpression="Description" DataField="Description"
                    ColumnEditorID="CompanyName" HeaderText="Certificate" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Description")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtDescription" runat="server" EmptyMessage="Please enter certificate title" Text='<%# DataBinder.Eval(Container.DataItem, "Description")%>' />
                        <asp:RequiredFieldValidator ID="reqDescription" runat="server" Text="Enter certificate title"
                            ControlToValidate="TxtDescription"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="DateObtained" SortExpression="DateObtained" DataField="DateObtained"
                    ColumnEditorID="DateObtained" HeaderText="Date Obtained" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "DateObtained", "{0:D}") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="CertDate" Text='<%# DataBinder.Eval(Container.DataItem, "DateObtained") %>'
                            Visible="false" />
                        <telerik:RadDatePicker ID="DPDateObtained" runat="server"></telerik:RadDatePicker>
                        <asp:RequiredFieldValidator ID="reqDate" runat="server" Text="Enter date obtained" ControlToValidate="DPDateObtained"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="WhereObtained" SortExpression="WhereObtained" DataField="WhereObtained"
                    ColumnEditorID="WhereObtained" HeaderText="Obtained At" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "WhereObtained")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtWhereObtained" runat="server" EmptyMessage="Please enter the place" Text='<%# DataBinder.Eval(Container.DataItem, "WhereObtained")%>' />
                        <asp:RequiredFieldValidator ID="reqTxtWhereObtained" runat="server" Text="Enter where did you obtain the certificate"
                            ControlToValidate="TxtDescription"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Delete" ID="DeleteLink" runat="server" OnClientClick="return confirm('Are you sure to delete this record?');"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:radgrid>
    <br />
    <br />
</div>
