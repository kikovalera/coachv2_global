﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using CoachV2.AppCode;
 
namespace CoachV2.UserControl
{
    public partial class Ops_ChartDir : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string URL = (HttpContext.Current.Request.Url.Host == "localhost" ? "http://" : "http://") + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);

            FakeURLID.Value = URL;
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            string gmail =   ds.Tables[0].Rows[0][13].ToString();
            string cim_num =  ds.Tables[0].Rows[0][2].ToString(); //
            int intcim =  Convert.ToInt32(cim_num);
            //Charts
            MyCIM.Value = Convert.ToString(cim_num);
            DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
            int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
            DataAccess ws = new DataAccess();
            DataSet ds_KPIforAgent = DataHelper.gettopkpis(intcim);//ws.GetKPIListPerAcct(Convert.ToString(acctno));

            StartDate.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            EndDate.SelectedDate = DateTime.Now;
            ddKPIList2.AppendDataBoundItems = true;
            ddKPIList2.DataTextField = "KPIName";
            ddKPIList2.DataValueField = "KPIId";
            ddKPIList2.DataSource = DataHelper.GetAllKPIs();
            ddKPIList2.DataBind();

            //overall behaviordropdown
            dp_behavior_start.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behavior_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior1.DataSource = ds_KPIforAgent;
            cb_kpi_behavior1.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior1.DataBind();
            }

            //behavior vs kpi
            dp_behaviorkpi_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_behaviorkpi_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior_kpi.DataSource = ds_KPIforAgent;
            cb_kpi_behavior_kpi.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior_kpi.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior_kpi.DataBind();
            }
            //dates for triad coaching chart
            RadDatePicker1.SelectedDate = DateTime.Today.AddMonths(-1);
            RadDatePicker2.SelectedDate = DateTime.Now;

            

            dp_top5_score_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_top5_score_end.SelectedDate = DateTime.Now;

            dp_top5_glide_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_top5_glide_end.SelectedDate = DateTime.Now;

            // added for special access francis.valera/08022018
            DataSet ds_OMlist = DataHelper.GetSubordinatesforcharts(intcim);
            DataAccess ws1 = new DataAccess();
            int userhierarchy;
            userhierarchy = ws1.getroleofloggedin_user(intcim);
            if (DataHelper.UserHasSpecialAccess(intcim) && userhierarchy == 3)
            {
                ds_OMlist = DataHelper.GetMyDistinction2(intcim);
            }
            cb_overallOM.DataSource = ds_OMlist;
            cb_overallOM.DataBind(); 
            
            //DataSet ds_OMlist = DataHelper.GetSubordinatesforcharts(intcim); //.GetTLAgents(intcim, acctno);
            //cb_overallOM.DataSource = ds_OMlist;
            //cb_overallOM.DataBind();

            cb_overallbOM.DataSource = ds_OMlist;
            cb_overallbOM.DataBind();
            cb_BvsKPIOM.DataSource = ds_OMlist;
            cb_BvsKPIOM.DataBind();

            cb_triadcoachingom.DataSource = ds_OMlist;
            cb_triadcoachingom.DataBind();

            cb_omtop5_fvsscore.DataSource = ds_OMlist;
            cb_omtop5_fvsscore.DataBind();
            cb_omtop5_avst.DataSource = ds_OMlist;
            cb_omtop5_avst.DataBind();

            DataSet ds_DVlist = DataHelper.GetDataView();
            cb_overalldataview.DataSource = ds_DVlist;
            cb_overalldataview.DataBind();


            cb_top5scoredataview.DataSource = ds_DVlist;
            cb_top5scoredataview.DataBind();
            cb_top5avtdataview.DataSource = ds_DVlist;
            cb_top5avtdataview.DataBind();

            cb_triaddataview.DataSource = ds_DVlist;
            cb_triaddataview.DataBind();


            DataSet ds_overall_lob = DataHelper.GetLOBList1(Convert.ToInt32(intcim));
            cb_overallLOB.DataSource = ds_overall_lob;
            cb_overallLOB.DataBind();

            cb_overallbehaviorLOB.DataSource = ds_overall_lob;
            cb_overallbehaviorLOB.DataBind();

            cb_behaviorkpi_lob.DataSource = ds_overall_lob;
            cb_behaviorkpi_lob.DataBind();

            cb_top5fvs_lob.DataSource = ds_overall_lob;
            cb_top5fvs_lob.DataBind();

            cb_top5avst_lob.DataSource = ds_overall_lob;
            cb_top5avst_lob.DataBind();

            cb_triad_lob.DataSource = ds_overall_lob;
            cb_triad_lob.DataBind();

            DataSet ds_sitelist = DataHelper.GetSiteList1(intcim);

            cb_triad_site.DataSource = ds_sitelist;
            cb_triad_site.DataBind();

            cb_top5avst_site.DataSource = ds_sitelist;
            cb_top5avst_site.DataBind();

            cb_top5fvs_site.DataSource = ds_sitelist;
            cb_top5fvs_site.DataBind();

            cb_overallSite.DataSource = ds_sitelist;
            cb_overallSite.DataBind();

            cb_overallbehaviorSite.DataSource = ds_sitelist;
            cb_overallbehaviorSite.DataBind();

            cb_behaviorkpi_site.DataSource = ds_sitelist;
            cb_behaviorkpi_site.DataBind();

            dlKPI.DataSource = DataHelper.gettopkpis(intcim);
            dlKPI.DataBind();
        
        }

    }
}