﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ops_VP.ascx.cs" Inherits="CoachV2.UserControl.Ops_VP" %>
<style type="text/css">
    .bgimg
    {
        background-image: url('/content/images/whitebgp.png');
    }
</style>
<div class="panel-group" id="Div5">
    <div class="row row-custom">
    </div>
    <div style="background-color: White">
        <div class="panel-group" id="Div_TLCHARTS">
            <asp:HiddenField ID="FakeURLID" runat="server" />
            <div class="row row-custom">
                <div class="col-sm-5">
                    <form>
                    <div class="input-group">
                        <span></span>
                    </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTL">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Charts<span class="glyphicon glyphicon-triangle-top triangletop"> </span>&nbsp;
                        </h4>
                    </div>
                </a>
            </div>
            <div id="collapseTL" class="panel-collapse collapse in" style="background-color: White">
                <br />
                <div class="form-horizontal col-md-12" role="form">
                    <button type="button" class="btn btn-custom" id="check_graph">
                        Overall</button>
                    <telerik:RadComboBox ID="cb_selectLOB" runat="server" DataValueField="AccountID"
                        DataTextField="LOB" AppendDataBoundItems="true" OnClientSelectedIndexChanged="selectlob">
                        <%-- OnClientSelectedIndexChanged="selectlob"--%>
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Select LOB" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <%--Overall Account Coaching Frequency--%>
                <div id="overalldate">
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4" id="lbl-changer">Overall Coaching Frequency</font>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Director <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallDir" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallSite" runat="server" DataValueField="companysiteid"
                            DataTextField="companysite" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_kpioverall" runat="server" ResolvedRenderMode="Classic"
                            AppendDataBoundItems="true" OnClientSelectedIndexChanged="kpichanger" FeatureGroupID="accountid">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="-" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="dateChangerScoreVsKPI" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                            DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="dateChangerScoreVsKPI" />
                        </telerik:RadDatePicker>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_overalldataview" runat="server" AppendDataBoundItems="true"
                            DataValueField="DataViewID" DataTextField="DataView">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallOM" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallTL" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <asp:HiddenField ID="hiddentl1" runat="server" />
                        <asp:HiddenField ID="HiddenAgent1" runat="server" />
                        <telerik:RadComboBox ID="cb_overallAgent" runat="server" AppendDataBoundItems="true"
                            DataValueField="CimNumber" DataTextField="Name">
                            <%--  OnClientSelectedIndexChanged="OVERALL"--%>
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnOverall" class="btn btn-info">
                            Run</button>
                    </div>
                </div>
                <div id="overallfrequency" style="width: 100%; height: 100%; background-color: #fff transparent">
                </div>
                <%--Top Behavioral Drivers--%>
                <div id="behavior-chart-section">
                    <div id="overall_behaviordates">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4">Overall Behavior</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-2">
                            <div class="help">
                                Director <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_overallbDir" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <%-- <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>--%>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_overallbOM" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_overallbTL" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_overallbAgent" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_overallbehaviorsite" runat="server" DataValueField="companysiteid"
                                DataTextField="companysite" AppendDataBoundItems="true">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="cb_kpi_behavior1" runat="server" AppendDataBoundItems="true"
                                DataValueField="KPIID" DataTextField="KPIName" ResolvedRenderMode="Classic" ClientIDMode="Static">
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="overallchanger" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="overallchanger" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                               &nbsp;
                            </div>
                            <button id="BtnBehavior" class="btn btn-info">
                                Run</button>
                        </div>
                        <div class="col-sm-2">
                            <div class="help" style="display: none;">
                                Data View<span class="glyphicon glyphicon-triangle-bottom"></span>
                                <telerik:RadComboBox ID="cb_overallbdataview" runat="server" AppendDataBoundItems="true"
                                    DataValueField="DataViewID" DataTextField="DataView">
                                </telerik:RadComboBox>
                            </div>
                        </div>
                        
                    </div>
                    <div id="behaviorvsKPI" style="width: 100%; height: 100%; background-color: White">
                    </div>
                </div>
                <%--Behavioral Drivers Vs. KPI--%>
                <div id="overall_kpivsb">
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Behavioral vs. KPI</font>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Director <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_BvsKPIDir" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_BvsKPIOM" runat="server" DataValueField="CimNumber" DataTextField="Name"
                            AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_BvsKPITL" runat="server" DataValueField="CimNumber" DataTextField="Name"
                            AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_BvsKPIAgent" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallbvsksite" runat="server" DataValueField="companysiteid"
                            DataTextField="companysite" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_overallbvsklob" runat="server" DataValueField="accountid"
                            DataTextField="LOB" AppendDataBoundItems="true" Visible="false">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadComboBox ID="cb_overallbvskpi" runat="server" DataValueField="KPIID"
                            DataTextField="KPIName" ResolvedRenderMode="Classic" AppendDataBoundItems="true"
                            ClientIDMode="Static">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="kpivsbehaviorchanger" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="kpivsbehaviorchanger" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                            <div class="help">
                               &nbsp;
                            </div>
                            <button id="BtnKPIvBehavior" class="btn btn-info">
                                Run</button>
                        </div>
                    <div id="DIV2_kpivsscore" style="width: 100%; height: 100%; background-color: White">
                    </div>
                </div>
                <%-- top 1 kpi --%>
                <div id="kpi-chart-changer">
                    <%--<div id="aht_dates">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4">Audit Frequency Vs. Average Account
                                            <asp:Label ID="lbl_1stkpi_coaching_frequency" runat="server"></asp:Label>
                                        </font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>--%>
                    <div id="aht_score" style="width: 100%; height: 100%;">
                    </div>
                </div>
                <div id="glide-holder">
                    <div id="ahtglidedates">
                        <br />
                        <br />
                        <br />
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4" id="tar-act-label">Targets Vs. Actual</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-2">
                            <div class="help">
                                Director <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_ahtavtfDir" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                Site <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_top1avsttop1_site" runat="server" DataValueField="companysiteid"
                                DataTextField="companysite" AppendDataBoundItems="true">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_top1avsttop1_kpi" runat="server" DataValueField="KPIID"
                                DataTextField="KPIName" ResolvedRenderMode="Classic" AppendDataBoundItems="true"
                                ClientIDMode="Static">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="---" Value="-1" Enabled="false" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_ahtglide_start" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="testcal" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_ahtglide_end" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="testcal" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="cb_top1avtdataview" runat="server" AppendDataBoundItems="true"
                                DataValueField="DataViewID" DataTextField="DataView" ClientIDMode="Static">
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                OM <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_ahtavtfOM" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_ahtavtfTL" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="dirChanger">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                                Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_ahtavtfAgent" runat="server" DataValueField="CimNumber"
                                DataTextField="Name" AppendDataBoundItems="true">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-2">
                            <div class="help">
                               &nbsp;
                            </div>
                            <button id="BtnTarVAct" class="btn btn-info">
                                Run</button>
                        </div>
                    </div>
                    <div id="AHTGlide" style="width: 100%; height: 100%;">
                    </div>
                </div>
                <%-- top 2 kpi --%>
                <br />
                <br />
                <div class="form-group">
                    <asp:HiddenField ID="MyCIM" runat="server" />
                    <div>
                        <div>
                            <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                                padding-bottom: 10px; display: none;"><i class="glyphicon glyphicon-warning-sign">
                                </i>No data found</span>
                            <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                                runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
                                Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                                Style="padding-top: 10px; padding-bottom: 10px;">
                            </asp:CompareValidator>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<%-- white bg 
--%>
<%-- charts area --%>
</div>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/exporting.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
<script type='text/javascript'>
    var URL = $("#<%= FakeURLID.ClientID %>").val();


    function dateChangerScoreVsKPI(sender, args) {
        //var KPI = $find('<%= cb_kpioverall.ClientID %>').get_selectedItem().get_value();

        var startDate = $find('<%=StartDate.ClientID%>');
        var endDate = $find('<%=EndDate.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnOverall').prop('disabled', true);
        } else {
            $('#BtnOverall').prop('disabled', false);
        }

//        if (KPI > 0) {
//            var KPI = $find('<%= cb_kpioverall.ClientID %>').get_selectedItem().get_value();
//            var KPIText = $find('<%= cb_kpioverall.ClientID %>').get_selectedItem().get_text();
//            ScoreVsKPI(KPI, KPIText);
//        } else {
//            $("#check_graph").trigger("click");
//        }
    }

    function testcal(sender, args) {
        //var KPI = $find('<%= cb_overallbvskpi.ClientID %>').get_selectedItem().get_value();
//        var KPI = $find('<%= cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_value();
//        var KPIText = $find('<%= cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_text();
        //        KPIGlide(KPI, KPIText); 

        var startDate = $find('<%=dp_ahtglide_start.ClientID%>');
        var endDate = $find('<%=dp_ahtglide_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTarVAct').prop('disabled', true);
        } else {
            $('#BtnTarVAct').prop('disabled', false);
        }
    }

    function getRandomColor(v) {

        var color = ['#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4', '#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4'];
        return color[v];
    }

    function overallchanger(sender, args) {
//        var KPI = $find('<%= cb_kpi_behavior1.ClientID %>').get_selectedItem().get_value();
        //        overallbehavior(KPI);

        var startDate = $find('<%=dp_behavior_start.ClientID%>');
        var endDate = $find('<%=dp_behavior_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnBehavior').prop('disabled', true);
        } else {
            $('#BtnBehavior').prop('disabled', false);
        }
    }

    function kpivsbehaviorchanger(sender, args) {
//        var KPI = $find('<%= cb_overallbvskpi.ClientID %>').get_selectedItem().get_value();
        //        kpivsbehavior(KPI); dp_behaviorkpi_end

        var startDate = $find('<%=dp_behaviorkpi_start.ClientID%>');
        var endDate = $find('<%=dp_behaviorkpi_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnKPIvBehavior').prop('disabled', true);
        } else {
            $('#BtnKPIvBehavior').prop('disabled', false);
        }
    }


    function ahtvglide(sender, args) {

        var idx = sender.get_id();
        if (idx.toString() == "cb_top1avsttop1_kpi" || idx.toString() == "cb_top1avtdataview") {
            var KPI = $find('<%= cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_value();
            var KPIText = $find('<%= cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_text();
            KPIGlide(KPI, KPIText);
        }

    }


    function kpivsscorechanger(sender, args) {
        var KPI = $find('<%= cb_kpioverall.ClientID %>').get_selectedItem().get_value();
        var KPIText = $find('<%= cb_kpioverall.ClientID %>').get_selectedItem().get_text();

        if (parseInt(KPI) > 0) {
            ScoreVsKPI(KPI, KPIText);
            KPIGlide(KPI, KPIText);
        } else {
            $("#check_graph").trigger("click");
        }
    }

    function selectlob(sender, args) {

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();


        var LOB = $find('<%= cb_selectLOB.ClientID %>').get_selectedItem().get_value();

        var comboxxx = $find("<%= cb_overallDir.ClientID %>")
        var comboItemxxx = new Telerik.Web.UI.RadComboBoxItem();
        comboxxx.get_items().remove(comboItemxxx);
        comboxxx.get_items().clear();

        comboItemxxx.set_text("All");
        comboItemxxx.set_value('0');
        comboxxx.trackChanges();
        comboxxx.get_items().add(comboItemxxx);
        comboItemxxx.select();
        comboxxx.commitChanges();


        var combo2xxx = $find("<%= cb_overallbDir.ClientID %>")
        var comboItem2xxx = new Telerik.Web.UI.RadComboBoxItem();
        combo2xxx.get_items().remove(comboItem2xxx);
        combo2xxx.get_items().clear();

        comboItem2xxx.set_text("All");
        comboItem2xxx.set_value('0');
        combo2xxx.trackChanges();
        combo2xxx.get_items().add(comboItem2xxx);
        comboItem2xxx.select();
        combo2xxx.commitChanges();

        var combo3xxx = $find("<%= cb_BvsKPIDir.ClientID %>")
        var comboItem3xxx = new Telerik.Web.UI.RadComboBoxItem();
        combo3xxx.get_items().remove(comboItem3xxx);
        combo3xxx.get_items().clear();

        comboItem3xxx.set_text("All");
        comboItem3xxx.set_value("0");
        combo3xxx.trackChanges();
        combo3xxx.get_items().add(comboItem3xxx);
        comboItem3xxx.select();
        combo3xxx.commitChanges();

        var combo4xxx = $find("<%= cb_ahtavtfDir.ClientID %>")
        var comboItem4xxx = new Telerik.Web.UI.RadComboBoxItem();
        combo4xxx.get_items().remove(comboItem4xxx);
        combo4xxx.get_items().clear();

        comboItem4xxx.set_text("All");
        comboItem4xxx.set_value("0");
        combo4xxx.trackChanges();
        combo4xxx.get_items().add(comboItem4xxx);
        comboItem4xxx.select();
        combo4xxx.commitChanges();




        $.ajax({
            type: 'GET',
            url: 'ChartHandlers/DirByLOB.ashx',
            data: { cim: CIMNo, lobid: LOB },
            contentType: "application/json; charset=utf-8",
            dataType: 'JSON',
            success: function (data) {


                for (i = 0; i < data.Table.length; i++) {
                    var combo = comboxxx;
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(data.Table[i].Name);
                    comboItem.set_value(data.Table[i].CimNumber);
                    combo.trackChanges();
                    combo.get_items().add(comboItem);
                    combo.get_items().getItem(0).select();
                    combo.updateClientState();
                    combo.commitChanges();


                    var combo2 = combo2xxx;
                    var comboItem2 = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem2.set_text(data.Table[i].Name);
                    comboItem2.set_value(data.Table[i].CimNumber);
                    combo2.trackChanges();
                    combo2.get_items().add(comboItem2);
                    combo2.get_items().getItem(0).select();
                    combo2.updateClientState();
                    combo2.commitChanges();


                    var combo3 = combo3xxx;
                    var comboItem3 = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem3.set_text(data.Table[i].Name);
                    comboItem3.set_value(data.Table[i].CimNumber);
                    combo3.trackChanges();
                    combo3.get_items().add(comboItem3);
                    combo3.get_items().getItem(0).select();
                    combo3.updateClientState();
                    combo3.commitChanges();

                    var combo4 = combo4xxx;
                    var comboItem4 = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem4.set_text(data.Table[i].Name);
                    comboItem4.set_value(data.Table[i].CimNumber);
                    combo4.trackChanges();
                    combo4.get_items().add(comboItem4);
                    combo4.get_items().getItem(0).select();
                    combo4.updateClientState();
                    combo4.commitChanges();
                }

                //                if ($find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_index() == 0) {
                //                    $("#check_graph").trigger('click');
                //                }


                //                if (KPI > 0) {
                //                    $('#overallfrequency').hide();

                //                    //ahtglidechart(KPI, KPIName);
                //                }
            },
            error: function (e) {
                console.log(e);
            }
        });


        //        if (parseInt(LOB) > 0) {

        //                        $("#overall_behaviordates").show();
        //                        $("#overall_kpivsb").show();

        //            //            overallbehavior();
        //            //            kpivsbehavior();

        //            $("#check_graph").trigger("click");
        //        }

        var combo3x1 = $find("<%= cb_kpioverall.ClientID %>");
        var comboItem3x1 = new Telerik.Web.UI.RadComboBoxItem();
        combo3x1.get_items().remove(comboItem3x1);
        combo3x1.get_items().clear();

        comboItem3x1.set_text("None");
        comboItem3x1.set_value("0");
        combo3x1.trackChanges();
        combo3x1.get_items().add(comboItem3x1);
        comboItem3x1.select();
        combo3x1.commitChanges();

        var combo3x2 = $find("<%= cb_top1avsttop1_kpi.ClientID %>");
        var comboItem3x2 = new Telerik.Web.UI.RadComboBoxItem();
        combo3x2.get_items().remove(comboItem3x2);
        combo3x2.get_items().clear();

        comboItem3x2.set_text("-");
        comboItem3x2.set_value("0");
        comboItem3x2.set_enabled(false);
        combo3x2.trackChanges();
        combo3x2.get_items().add(comboItem3x2);
        comboItem3x2.select();
        combo3x2.commitChanges();


        var combo3x4 = $find("<%= cb_kpi_behavior1.ClientID %>");
        var comboItem3x4 = new Telerik.Web.UI.RadComboBoxItem();

        //        comboItem3x4.set_text("All");
        //        comboItem3x4.set_value("0");
        //        combo3x4.trackChanges();
        //        combo3x4.get_items().add(comboItem3x4);
        //        comboItem3x4.select();
        //        combo3x4.commitChanges();

        //        comboItem3x4.set_text("-");
        //        comboItem3x4.set_value("0");
        //        comboItem3x4.set_enabled(false);
        //        combo3x4.trackChanges();
        //        combo3x4.get_items().add(comboItem3x4);
        //        comboItem3x4.select();
        //        combo3x4.commitChanges();

        var combo3x6 = $find("<%= cb_overallbvskpi.ClientID %>");
        var comboItem3x6 = new Telerik.Web.UI.RadComboBoxItem();
        combo3x6.get_items().remove(comboItem3x6);
        combo3x6.get_items().clear();


        //        comboItem3x6.set_text("-");
        //        comboItem3x6.set_value("0");
        //        comboItem3x6.set_enabled(false);
        //        combo3x6.trackChanges();
        //        combo3x6.get_items().add(comboItem3x6);
        //        comboItem3x6.select();
        //        combo3x6.commitChanges();


        $.ajax({
            type: 'GET',
            url: 'ChartHandlers/KPIChanger.ashx',
            data: { lobid: LOB },
            contentType: "application/json; charset=utf-8",
            dataType: 'JSON',
            success: function (data) {
                combo3x4.get_items().remove(comboItem3x4);
                combo3x4.get_items().clear();
                combo3x4.commitChanges();

                for (i = 0; i < data.Table.length; i++) {
                    var combo3x = combo3x1;
                    var comboItem3x = new Telerik.Web.UI.RadComboBoxItem();

                    comboItem3x.set_text(data.Table[i].kpiname);
                    comboItem3x.set_value(data.Table[i].hierarchy);
                    combo3x.trackChanges();
                    combo3x.get_items().add(comboItem3x);
                    combo3x.get_items().getItem(0).select();
                    combo3x.updateClientState();
                    combo3x.commitChanges();

                    var combo3x3 = combo3x2;
                    var comboItem3x3 = new Telerik.Web.UI.RadComboBoxItem();

                    comboItem3x3.set_text(data.Table[i].kpiname);
                    comboItem3x3.set_value(data.Table[i].hierarchy);
                    combo3x3.trackChanges();
                    combo3x3.get_items().add(comboItem3x3);
                    combo3x3.updateClientState();
                    combo3x3.commitChanges();

                    var combo3x5 = combo3x4;
                    var comboItem3x5 = new Telerik.Web.UI.RadComboBoxItem();

                    comboItem3x5.set_text(data.Table[i].kpiname);
                    comboItem3x5.set_value(data.Table[i].hierarchy);
                    combo3x5.trackChanges();
                    combo3x5.get_items().add(comboItem3x5);
                    combo3x.get_items().getItem(0).select();
                    combo3x.updateClientState();
                    combo3x5.commitChanges();

                    var combo3x7 = combo3x6;
                    var comboItem3x7 = new Telerik.Web.UI.RadComboBoxItem();

                    comboItem3x7.set_text(data.Table[i].kpiname);
                    comboItem3x7.set_value(data.Table[i].hierarchy);
                    combo3x7.trackChanges();
                    combo3x7.get_items().add(comboItem3x7);
                    combo3x7.updateClientState();
                    combo3x7.commitChanges();
                }
            },
            error: function (e) {
                console.log(e);
            }
        });

        //        $("#overall_behaviordates").show();
        //                                $("#overall_kpivsb").show();

        //        overallbehavior();
        //                                kpivsbehavior();


        $("#check_graph").click();

        
    }


    function dirChanger(sender, args) {
        var id = sender.get_id();
        var MyCIMNo;
        var combo;
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();

        var combokpi;

        if (id.toString() == "cb_overallDir") {
            MyCIMNo = $find('<%=cb_overallDir.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallOM.ClientID %>");
            combokpi = $find("<%= cb_kpioverall.ClientID %>");
        }

        if (id.toString() == "cb_overallOM") {
            MyCIMNo = $find('<%=cb_overallOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallTL.ClientID %>");
            combokpi = $find("<%= cb_kpioverall.ClientID %>");
        }


        if (id.toString() == "cb_overallTL") {
            MyCIMNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallAgent.ClientID %>");
            combokpi = $find("<%= cb_kpioverall.ClientID %>");
        }

        if (id.toString() == "cb_overallbDir") {
            MyCIMNo = $find('<%=cb_overallbDir.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbOM.ClientID %>");
            combokpi = $find("<%= cb_kpi_behavior1.ClientID %>");
        }

        if (id.toString() == "cb_overallbOM") {
            MyCIMNo = $find('<%=cb_overallbOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbTL.ClientID %>");
            combokpi = $find("<%= cb_kpi_behavior1.ClientID %>");
        }

        if (id.toString() == "cb_overallbTL") {
            MyCIMNo = $find('<%=cb_overallbTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbAgent.ClientID %>");
            combokpi = $find("<%= cb_kpi_behavior1.ClientID %>");
        }

        if (id.toString() == "cb_BvsKPIDir") {
            MyCIMNo = $find('<%=cb_BvsKPIDir.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_BvsKPIOM.ClientID %>");
            combokpi = $find("<%= cb_overallbvskpi.ClientID %>");
        }

        if (id.toString() == "cb_BvsKPIOM") {
            MyCIMNo = $find('<%=cb_BvsKPIOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_BvsKPITL.ClientID %>");
            combokpi = $find("<%= cb_overallbvskpi.ClientID %>");
        }

        if (id.toString() == "cb_BvsKPITL") {
            MyCIMNo = $find('<%=cb_BvsKPITL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_BvsKPIAgent.ClientID %>");
            combokpi = $find("<%= cb_overallbvskpi.ClientID %>");
        }

        if (id.toString() == "cb_ahtavtfDir") {
            MyCIMNo = $find('<%=cb_ahtavtfDir.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_ahtavtfOM.ClientID %>");
            combokpi = $find("<%= cb_top1avsttop1_kpi.ClientID %>");
        }

        if (id.toString() == "cb_ahtavtfOM") {
            MyCIMNo = $find('<%=cb_ahtavtfOM.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_ahtavtfTL.ClientID %>");
            combokpi = $find("<%= cb_top1avsttop1_kpi.ClientID %>");
        }

        if (id.toString() == "cb_ahtavtfTL") {
            MyCIMNo = $find('<%=cb_ahtavtfTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_ahtavtfAgent.ClientID %>");
            combokpi = $find("<%= cb_top1avsttop1_kpi.ClientID %>");
        }

        //kpiid = $find('<%=cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_value();


        combo.get_items().clear();
        comboItem.set_text("All");
        comboItem.set_value('0');
        combo.trackChanges();
        combo.get_items().add(comboItem);
        comboItem.select();
        combo.commitChanges();

        if (MyCIMNo != "0") {

            $.ajax({
                type: 'GET',
                url: 'ChartHandlers/AgentListDir.ashx',
                data: { cim: MyCIMNo },
                contentType: "application/json; charset=utf-8",
                dataType: 'JSON',
                success: function (data) {
                    for (i = 0; i < data.Table.length; i++) {
                        var combo2 = combo;
                        var comboItem2 = new Telerik.Web.UI.RadComboBoxItem();
                        comboItem2.set_text(data.Table[i].Name);
                        comboItem2.set_value(data.Table[i].CimNumber);
                        combo2.trackChanges();
                        combo2.get_items().add(comboItem2);
                        combo2.commitChanges();
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });

            //            var combo2;
            //            var comboItem2 = new Telerik.Web.UI.RadComboBoxItem();

            //            combo2 = combokpi;
            //            combo2.get_items().clear();


            //            if (combo2.get_id().toString() == "cb_kpi_behavior1" || combo2.get_id().toString() == "cb_overallbvskpi" || combo2.get_id().toString() == "cb_top1avsttop1_kpi") {
            ////                comboItem2.set_text("-");
            ////                comboItem2.set_value('-1');
            ////                comboItem2.set_enabled(false);
            //            } else {
            //                comboItem2.set_text("-");
            //                comboItem2.set_value('-1');
            //                comboItem2.set_enabled(true);
            //                combo2.trackChanges();
            //                combo2.get_items().add(comboItem2);
            //                comboItem2.select();
            //                combo2.commitChanges();
            //            }
            //            

            //            $.ajax({
            //                type: 'GET',
            //                url: 'ChartHandlers/KPIByCIM.ashx',
            //                data: { cim: MyCIMNo },
            //                contentType: "application/json; charset=utf-8",
            //                dataType: 'JSON',
            //                success: function (data) {
            //                    for (i = 0; i < data.Table.length; i++) {
            //                        var combo3 = combo2;
            //                        var comboItem3 = new Telerik.Web.UI.RadComboBoxItem();
            //                        comboItem3.set_text(data.Table[i].kpiname);
            //                        comboItem3.set_value(data.Table[i].hierarchy);
            //                        combo3.trackChanges();

            //                        combo3.get_items().add(comboItem3);
            //                        combo3.get_items().getItem(0).select();
            //                        combo3.updateClientState();
            //                        combo3.commitChanges();
            //                    }
            //                },
            //                error: function (e) {
            //                    console.log(e);
            //                }
            //            });
        }

    }

    function overallfreq() {
        $('.highcharts-data-table').remove();
        var data1 = [];
        var selectedDate1 = $find("<%= StartDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDate2 = $find("<%= EndDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid1 = $find('<%=cb_kpioverall.ClientID %>').get_selectedItem().get_value();
        var Dircimno = $find('<%=cb_overallDir.ClientID %>').get_selectedItem().get_value();
        var OMCimNo = $find('<%=cb_overallOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();

        var AgentCimNo = $find('<%=cb_overallAgent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value();
        var SiteId = $find('<%=cb_overallSite.ClientID %>').get_selectedItem().get_value();
        var LOBXx = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();


        var highC = Highcharts.chart('overallfrequency', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: ''
                },
                color: '#FF00FF'
            },
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    },
                    fillOpacity: 1,
                    fillColor: '#289CCC'
                }
            },
            credits: {
                enabled: false
            },

            exporting: {
                showTable: true
            },
            series: [{
                name: 'Coaching Frequency',
                data: data1,
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',CIMNo: '" + CIMNo + "', Dircimno: '" + Dircimno + "',  OMCimno: '" + OMCimNo + "' ,TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "' ,  DataView: '" + DataView + "' , SiteId: '" + SiteId + "', accountId: '" + "0" + "', LobID: '" + LOBXx + "', KPIID: '" + "0" + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForVP",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    //$("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data1.push([f.ADate, f.CoachCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data1);
                highC.hideLoading();


            }
        });

        highC.showLoading();
    }

    function overallbehavior(kpiid) {
        var data3 = [];

        var Dircimno = $find('<%=cb_overallbDir.ClientID %>').get_selectedItem().get_value();
        var OMCimNo = $find('<%=cb_overallbOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallbTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallbAgent.ClientID %>').get_selectedItem().get_value();
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var selectedDateb1 = $find("<%= dp_behavior_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateb2 = $find("<%= dp_behavior_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiid1 = kpiid;
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value();
        var SiteId = $find('<%=cb_overallbehaviorsite.ClientID %>').get_selectedItem().get_value();
        var accountId = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();


        var highC = Highcharts.chart('behaviorvsKPI', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                series: {
                    colorByPoint: true
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },


            series: [{
                name: 'Top Behavioral Drivers',
                data: data3,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        });

        $.ajax({
            type: "POST",
            data: "{  StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "', CIMNo: '" + CIMNo + "', Dircimno: '" + Dircimno + "', OMCimno: '" + OMCimNo + "',  TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', DataView: '" + DataView + "', SiteId: '" + SiteId + "', LobID: '" + accountId + "', kpiid: '" + kpiid1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehavior_ForVP",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    //$("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data3.push([f.description, f.CoachCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data3);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }

    function kpivsbehavior(kpiid) {
        var data_bvsk1 = [];
        var data_bvsk2 = [];
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

//        var xxxkpi;
//        if ($find('<%=cb_overallbvskpi.ClientID %>').get_selectedItem().get_value() == null) {
//            xxxkpi = 1;
//        } else {
//            xxxkpi = $find('<%=cb_overallbvskpi.ClientID %>').get_selectedItem().get_value();
//        }

        var selectedDatebkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatebkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiidbkpi1 = kpiid;
        var DirCimNo = $find('<%=cb_BvsKPIDir.ClientID %>').get_selectedItem().get_value();
        var OMCimNo = $find('<%=cb_BvsKPIOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_BvsKPITL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_BvsKPIAgent.ClientID %>').get_selectedItem().get_value();
        var LobID = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('DIV2_kpivsscore', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: true,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        tickInterval: 20,
                        max: 100,
                        labels: {
                            format: '{value}%'
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'Behavior',
                data: data_bvsk1,
                type: 'column',
                yaxis: 0,
                color: '#27A6D9',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "', DirCimNo: '" + DirCimNo + "', OMCimNo: '" + OMCimNo + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', LobID: '" + LobID + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForVP",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    //$("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);

                    $("#err-msg").hide();


                });

                highC.series[0].setData(data_bvsk1);
                highC.series[1].setData(data_bvsk2);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }

    function ScoreVsKPI(kpiidx, str) {

        var kpitextx = str.substring(str.indexOf(":") + 1);

        $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

        var xxx1 = $find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_index();
        var colorA, colorB, chartHead1, chartHead2;

        chartHead1 = "Coaching Frequency vs " + kpitextx + " Score";


        $("#tar-act-label").html(chartHead2);

        $("#lbl-changer").html(chartHead1);

        colorA = getRandomColor(kpiidx);


        $('.highcharts-data-table').remove();
        var data1 = [];
        var data2 = [];


        var selectedDateaht1 = $find("<%= StartDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateaht2 = $find("<%= EndDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var kpiid = kpiidx;

        var DirCimNo = $find('<%=cb_overallDir.ClientID %>').get_selectedItem().get_value();
        var OMCimno = $find('<%=cb_overallOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallAgent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value();
        var Siteid = $find('<%=cb_overallSite.ClientID %>').get_selectedItem().get_value();
        var Account = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('aht_score', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 1,
                title: {
                    text: 'Total Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { //allowDecimals: false,
                        title: {
                            text: kpitextx + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        allowDecimals: false,
                        color: '#FF00FF',
                        opposite: true,
                        //min: (kpitextx.toString() == "tnPS" ? -100 : 0),
                        max: (kpitextx.toString() == "AHT" ? 2000 : 100),
                        tickInterval: (kpitextx.toString() == "AHT" ? 100 : 20),
                        labels: {
                            format: (kpitextx.toString() == "AHT" ? '{value}' : '{value}%')
                        }
                    }],
            tooltip: {
                pointFormat: kpitextx + ': <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },
            series: [{
                name: 'TOTAL ' + kpitextx + ' Coaching',
                data: [],
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: kpitextx + ' Score',
                        type: 'spline',
                        yAxis: 1,
                        data: [],
                        color: colorA,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return (kpitextx.toString() == "AHT" ? ' <b>' + this.y.toFixed(2) + '</b>' : ' <b>' + this.y.toFixed(2) + ' %</b>'); 
                            }
                        }
                    }]
        });  //aht

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', Dircimno: '" + DirCimNo + "', OMCimno: '" + OMCimno + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', kpiid: '" + kpiid + "', DataView: '" + DataView + "',  SiteId: '" + Siteid + "',  accountId: '" + Account + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetFVSKPIVP", //getData_AHTScore_Dir",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    //$("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data1.push([f.ADate, f.CoachCount]);
                    data2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data1);
                highC.series[1].setData(data2);

                highC.hideLoading();


            }
        });

        highC.showLoading();


        $('.highcharts-data-table').remove();

    }

    function KPIGlide(kpiidx, str) {

        $("#glide-holder").show();
        var colorA, colorB, chartHead1, chartHead2;

        var colorA = getRandomColor(kpiidx);

        var kpitextx = str.substring(str.indexOf(":") + 1);

        chartHead1 = "Coaching Frequency vs " + kpitextx + " Score";
        chartHead2 = "Target " + kpitextx + " vs Actual " + kpitextx;

        $("#tar-act-label").html(chartHead2);


        var xxx1 = $find("<%= cb_top1avsttop1_kpi.ClientID%>").get_selectedItem().get_index();

        var data_aht3 = [];
        var data_aht4 = [];


        var selectedDateaht1 = $find("<%= dp_ahtglide_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateaht2 = $find("<%= dp_ahtglide_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var kpiid;

        if (kpiidx != null)
            kpiid = kpiidx;
        else
            kpiid = $find('<%=cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_value();


        var kpiidxxxx = $find('<%=cb_top1avsttop1_kpi.ClientID %>').get_selectedItem().get_text();

        var DirCimNo = $find('<%=cb_ahtavtfDir.ClientID %>').get_selectedItem().get_value();
        var OMCimno = $find('<%=cb_ahtavtfOM.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_ahtavtfTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_ahtavtfAgent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_top1avtdataview.ClientID %>').get_selectedItem().get_value();
        var Siteid = $find('<%=cb_top1avsttop1_site.ClientID %>').get_selectedItem().get_value();
        var Account = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();



        var highC1 =
                Highcharts.chart('AHTGlide', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        tickVal: 5,
                        //min: 0,
                        title: {
                            text: 'Target ' + kpitextx,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    {
                        tickVal: 10,
                        title: {
                            text: 'Actual ' + kpitextx,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        //min: (kpitextx.toString() == "tnPS" ? -100 : 0),
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        //pointFormat: kpitextx + (this.y == 0 && this.point.y != null ? "No Score" : ' <b>{point.y:,.2f}</b>')

                        //                        formatter: function () {
                        //                            if (this.y == 0) {
                        //                                return kpitextx + ':<b>No Score</b>';
                        //                            } else {
                        //                                return kpitextx + ':<b>' + this.y.toFixed(2) + (kpiidxxxx.toString() == "AHT" ? '' : '%') + '</b>';
                        //                            }
                        //                        }
                    },
                    plotOptions: {

                    },
                    credits: {
                        enabled: false
                    }, exporting: {
                        showTable: true
                    },

                    series: [{
                        name: 'Target ' + kpitextx,
                        data: [],
                        type: 'spline',
                        yaxis: 0,
                        color: '#2F9473',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        pointFormatter: function () {
                            if (data_aht3[this.category].toString().indexOf('*') >= 0) {
                                if (this.y == 0) {
                                    return "No target value for this date";
                                } else {
                                    return kpitextx + ':<b>' + this.y.toFixed(2) + (kpitextx.toString().indexOf('AHT') >= 0 ? '' : '%') + ' </b> fiscal month target change';
                                }
                            } else {
                                if (this.y == 0) {
                                    return "No target value for this date";
                                } else {
                                    return kpitextx + ' <b>' + this.y.toFixed(2) + (kpitextx.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                                }
                            }
                        }
                    },
                    {
                        name: 'Actual ' + kpitextx,
                        type: 'spline',
                        yAxis: 0,
                        data: [],
                        color: colorA,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                if (this.y == 0 && parseInt(data_aht3[this.index][1]) == 0) {
                                    return kpitextx + ' <b>No data available</b>';
                                } else {
                                    return kpitextx + ' <b>' + this.y.toFixed(2) + (kpitextx.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                                }
                            }
                        }
                    }]
                });   //aht


        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo + "', Dircimno: '" + DirCimNo + "', OMCimno: '" + OMCimno + "', TLCimNo: '" + TLCimNo + "', AgentCimNo: '" + AgentCimNo + "', kpiid: '" + kpiid + "', DataView: '" + DataView + "',  SiteId: '" + Siteid + "',  accountId: '" + Account + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetAVST_VP",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    //$("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht3.push([f.ADate, f.TargetCount]);
                    data_aht4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC1.series[0].setData(data_aht3);
                highC1.series[1].setData(data_aht4);

                highC1.hideLoading();
            }
        });

        highC1.showLoading();

    }

    function kpichanger(sender, args) {
        var kpiid = $find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_value();
        var kpitext = $find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_text();

        if (kpiid > 0) {
            //$('#overallfrequency').hide();

            var combo = $find("<%= cb_top1avsttop1_kpi.ClientID %>");
            var itm = combo.findItemByValue(kpiid);
            itm.select();

            $("#overallfrequency").hide();
           
            $("#behavior-chart-section").hide();
            $("#kpi-chart-changer").show();
            $("#overall_kpivsb").hide();


            ScoreVsKPI(kpiid, kpitext);
            $("#glide-holder").show();
            KPIGlide(kpiid, kpitext);
            //$("#BtnTarVActual").trigger("click");
        } else {
            $("#overallfrequency").show();
            $("#kpi-chart-changer").hide();
            $("#glide-holder").hide();
            $("#behavior-chart-section").show();
            $("#overall_kpivsb").show();
        }
    }

    function setSelectedIndex() {
        var combo = $find("<%= cb_kpioverall.ClientID %>");
        combo.trackChanges();
        combo.get_items().getItem(0).select();
        combo.updateClientState();
        combo.commitChanges();

        //        var combox = $find("<%= cb_selectLOB.ClientID %>");
        //        combox.trackChanges();
        //        combox.get_items().getItem(0).select();
        //        combox.updateClientState();
        //        combox.commitChanges();
    }


    $(document).ready(function () {
        $("#kpi-chart-changer").hide();
        $("#glide-holder").hide();


        $("#check_graph").on("click", function () {
            setSelectedIndex();

            $("#overallchanger").show();
            $("#kpi-chart-changer").hide();
            $("#glide-holder").hide();

            var xxx = $find('<%=cb_selectLOB.ClientID %>').get_selectedItem().get_value();

            overallfreq();

            if (parseInt(xxx) > 0) {

                $("#overall_behaviordates").show();
                $("#overall_kpivsb").show();
                $("#behavior-chart-section").show();

                overallbehavior(1);
                kpivsbehavior(1);

            } else {
                $("#overall_behaviordates").hide();
                $("#overall_kpivsb").hide();
                $("#behavior-chart-section").hide();
            }
        });

        $("#BtnOverall").on("click", function () {

            var kpiid = $find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_value();
            var kpitext = $find("<%= cb_kpioverall.ClientID%>").get_selectedItem().get_text();

            if (parseInt(kpiid) == 0) {
                //$("#check_graph").trigger("click");
                overallfreq();
            } else {
                ScoreVsKPI(kpiid, kpitext);
                //KPIGlide(kpiid, kpitext);
            }

            return false;
        });

        $("#BtnBehavior").on("click", function () {
            var KPI = $find('<%= cb_kpi_behavior1.ClientID %>').get_selectedItem().get_value();
            overallbehavior(KPI);
            return false;
        });

        $("#BtnKPIvBehavior").on("click", function () {
            var KPI = $find('<%= cb_overallbvskpi.ClientID %>').get_selectedItem().get_value();
            kpivsbehavior(KPI);
            return false;
        });

        $("#BtnTarVAct").on("click", function () {
            //var KPI = $find('<%= cb_overallbvskpi.ClientID %>').get_selectedItem().get_value();
            var xxx1 = $find("<%= cb_top1avsttop1_kpi.ClientID%>").get_selectedItem().get_value();
            var xxx2 = $find("<%= cb_top1avsttop1_kpi.ClientID%>").get_selectedItem().get_text();

            KPIGlide(xxx1, xxx2);
            return false;
        });



        $("#check_graph").trigger("click");
    });

</script>
