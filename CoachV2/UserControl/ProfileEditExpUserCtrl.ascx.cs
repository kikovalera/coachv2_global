﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Collections;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class ProfileEditExpUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEduc();
            }
        }

        private void LoadEduc()
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            ExpGrid.DataSource = DataHelper.GetExp(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));
            ExpGrid.DataBind();
        }

        protected void ExpGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    ExpGrid.DataSource = DataHelper.GetExp(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));

                }
                catch (Exception ex)
                {
                    ExpGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void ExpGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            GridEditableItem editableItem = ((GridEditableItem)e.Item);
            Hashtable newValues = new Hashtable();
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

            //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
            try
            {

                DateTime? selectedDateStart = ((RadMonthYearPicker)editableItem["StartDate"].FindControl("TxtStart")).SelectedDate;
                DateTime? selectedDateEnd = ((RadMonthYearPicker)editableItem["EndDate"].FindControl("TxtEnd")).SelectedDate;

                DataHelper.InsertExp(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"])
                , ((TextBox)editableItem["Company"].FindControl("TxtCompanyName")).Text
                , ((TextBox)editableItem["Role"].FindControl("TxtRole")).Text
                , ((TextBox)editableItem["Industry"].FindControl("TxtIndustry")).Text
                , Convert.ToDateTime(selectedDateStart.Value.ToString("yyyy-MM-dd"))
                , Convert.ToDateTime(selectedDateEnd.Value.ToString("yyyy-MM-dd"))
                , ((TextBox)editableItem["ReasonForLeaving"].FindControl("TxtRFL")).Text
                , DateTime.Now
                , DateTime.Now
                , false);

                ExpGrid.Controls.Add(new LiteralControl("Record has been inserted"));

            }
            catch (Exception ex)
            {
                ExpGrid.Controls.Add(new LiteralControl(string.Format("Error Inserting Record: {0}", ex.Message)));
            }
        }

        protected void ExpGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                DataHelper.DeleteExp(
                    Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("ExpID"))
                    );
                ExpGrid.Controls.Add(new LiteralControl("Record has been deleted"));
            }
            catch (Exception ex)
            {
                ExpGrid.Controls.Add(new LiteralControl(string.Format("Error deleting record: {0}", ex.Message)));
            }
        }

        protected void ExpGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditableItem editItem = e.Item as GridEditableItem;
                RadMonthYearPicker rd = (RadMonthYearPicker)editItem.FindControl("TxtStart");
                RadMonthYearPicker rd2 = (RadMonthYearPicker)editItem.FindControl("TxtEnd");

                rd.MaxDate = DateTime.Now;
                rd2.MaxDate = DateTime.Now;
          

                Label lblStart = editItem.FindControl("WorkStartDate") as Label;
                rd.SelectedDate = lblStart.Text != null && lblStart.Text != "" ? Convert.ToDateTime(lblStart.Text) : DateTime.Now;

                Label lblEnd = editItem.FindControl("WorkEndDate") as Label;
                rd2.SelectedDate = lblEnd.Text != null && lblEnd.Text != "" ? Convert.ToDateTime(lblEnd.Text) : DateTime.Now;

            }
        }

        protected void ExpGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {

                GridEditableItem editableItem = ((GridEditableItem)e.Item);
                GridEditManager editMan = editableItem.EditManager;
                int ExpID = Convert.ToInt32(editableItem.GetDataKeyValue("ExpID"));
                Hashtable newValues = new Hashtable();

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                try
                {
                    DateTime? selectedDateStart = ((RadMonthYearPicker)editableItem.FindControl("TxtStart")).SelectedDate;
                    DateTime? selectedDateEnd = ((RadMonthYearPicker)editableItem.FindControl("TxtEnd")).SelectedDate;

                    DataHelper.UpdateUserExp(ExpID
                        , ((TextBox)editableItem.FindControl("TxtCompanyName")).Text
                        , ((TextBox)editableItem.FindControl("TxtRole")).Text
                        , ((TextBox)editableItem.FindControl("TxtIndustry")).Text
                        , Convert.ToDateTime(selectedDateStart.Value.ToString("yyyy-MM-dd"))
                        , Convert.ToDateTime(selectedDateEnd.Value.ToString("yyyy-MM-dd"))
                        , ((TextBox)editableItem.FindControl("TxtRFL")).Text
                    );

                    ExpGrid.Controls.Add(new LiteralControl("Record has been updated"));

                }
                catch (Exception ex)
                {
                    ExpGrid.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
                }
            }
        }

        protected void ExpGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ExportToExcel" || e.CommandName == "ExportToCsv")
                {
                    DataSet ds1 = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    var ds = new DataSet();

                    DataAccess ws = new DataAccess();
                    ds = ws.GetProfileDetails(Convert.ToInt32(ds1.Tables[0].Rows[0]["UserInfoId"]), "EXP");


                    if (e.CommandName == "ExportToExcel")
                    {
                        ExcelHelperUpdated.ToExcel(ds, "Experiences " + DateTime.Now + ".xls", Page.Response);

                    }
                    else
                    {
                        if (ds.Tables.Count > 1)
                        {
                            CSVHelper.ConvertToCSV(ds, "Experiences " + DateTime.Now + ".csv", Page.Response);
                        }
                        else
                        {
                            CSVHelper.ConvertToCSV(ds.Tables[0], "Experiences " + DateTime.Now + ".csv", Page.Response);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "ExpGrid_ItemCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);      

            }
        }
    }
}