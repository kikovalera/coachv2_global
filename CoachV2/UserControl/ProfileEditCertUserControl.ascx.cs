﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Configuration;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class ProfileEditCertUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadEduc();
            }
        }

        private void LoadEduc()
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            ExpGrid.DataSource = DataHelper.GetCert(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));
            ExpGrid.DataBind();
        }

        protected void ExpGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    ExpGrid.DataSource = DataHelper.GetCert(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"]));

                }
                catch (Exception ex)
                {
                    ExpGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void ExpGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            GridEditableItem editableItem = ((GridEditableItem)e.Item);
            Hashtable newValues = new Hashtable();
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

            //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
            try
            {


                DataHelper.InsertUserCert(Convert.ToInt32(ds.Tables[0].Rows[0]["UserInfoId"])
                , ((TextBox)editableItem["CertificateName"].FindControl("TxtDescription")).Text
                , Convert.ToDateTime(((RadDatePicker)editableItem["DateObtained"].FindControl("DPDateObtained")).SelectedDate)
                , ((TextBox)editableItem["WhereObtained"].FindControl("TxtWhereObtained")).Text
                , DateTime.Now
                , DateTime.Now
                , false);

                ExpGrid.Controls.Add(new LiteralControl("Record has been inserted"));

            }
            catch (Exception ex)
            {
                ExpGrid.Controls.Add(new LiteralControl(string.Format("Error Inserting Record: {0}", ex.Message)));
            }
        }

        protected void ExpGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                DataHelper.DeleteCert(
                    Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("CertID"))
                    );
                ExpGrid.Controls.Add(new LiteralControl("Record has been deleted"));
            }
            catch (Exception ex)
            {
                ExpGrid.Controls.Add(new LiteralControl(string.Format("Error deleting record: {0}", ex.Message)));
            }
        }

        protected void ExpGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                GridEditableItem editItem = e.Item as GridEditableItem;

                RadDatePicker rd = (RadDatePicker)editItem.FindControl("DPDateObtained");

                rd.MaxDate = DateTime.Today;

                Label LblCertDate = editItem.FindControl("CertDate") as Label;

                rd.SelectedDate = LblCertDate.Text != null && LblCertDate.Text != "" ? Convert.ToDateTime(LblCertDate.Text) : DateTime.Now;

            }
        }

        protected void ExpGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {

                GridEditableItem editableItem = ((GridEditableItem)e.Item);
                GridEditManager editMan = editableItem.EditManager;
                int CertID = Convert.ToInt32(editableItem.GetDataKeyValue("CertID"));
                Hashtable newValues = new Hashtable();

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                try
                {


                    DataHelper.UpdateUserCert(CertID
                        , ((TextBox)editableItem.FindControl("TxtDescription")).Text
                        , Convert.ToDateTime(((RadDatePicker)editableItem.FindControl("DPDateObtained")).SelectedDate)
                        , ((TextBox)editableItem.FindControl("TxtWhereObtained")).Text
                    );

                    ExpGrid.Controls.Add(new LiteralControl("Record has been updated"));

                }
                catch (Exception ex)
                {
                    ExpGrid.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
                }
            }
        }

        protected void ExpGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ExportToExcel" || e.CommandName == "ExportToCsv")
                {
                    DataSet ds1 = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    var ds = new DataSet();

                    DataAccess ws = new DataAccess();
                    ds = ws.GetProfileDetails(Convert.ToInt32(ds1.Tables[0].Rows[0]["UserInfoId"]), "CERT");


                    if (e.CommandName == "ExportToExcel")
                    {
                        ExcelHelperUpdated.ToExcel(ds, "Certifications " + DateTime.Now + ".xls", Page.Response);

                    }
                    else
                    {
                        if (ds.Tables.Count > 1)
                        {
                            CSVHelper.ConvertToCSV(ds, "Certifications " + DateTime.Now + ".csv", Page.Response);
                        }
                        else
                        {
                            CSVHelper.ConvertToCSV(ds.Tables[0], "Certifications " + DateTime.Now + ".csv", Page.Response);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "ExpGrid_ItemCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);      

            }
        }
    }
}