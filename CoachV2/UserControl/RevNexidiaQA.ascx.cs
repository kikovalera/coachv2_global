﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class RevNexidiaQA : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] != null)
                {
                    int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 1260;
                    GetMassCoachingDetails(CoachingTicket);
                    LoadDocumentationsReview();
                    LoadKPIReview(CoachingTicket);
                    LoadPreviousCoachingNotes(CoachingTicket);
                    LoadPreviousPerformanceResults(CoachingTicket);
                    DisableElements();

                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                //int CoachingTicket = 755;
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(CoachingTicket, 3);
                //ds = ws.GetUploadedDocuments(79);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    RadDocumentationReview.DataSource = ds;
                    RadDocumentationReview.Rebind();
                }
                else
                {
                    RadDocumentationReview.DataSource = string.Empty;
                    RadDocumentationReview.Rebind();
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string CoacherCIM = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                    RadTextBox RadReviewID = this.Parent.FindControl("RadReviewID") as RadTextBox;
                    RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                    RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblCimNumber = this.Parent.FindControl("LblCimNumber") as Label;
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                    Label LblFullName = this.Parent.FindControl("LblFullName") as Label;
                    LblFullName.Text = ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    //RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    //RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblSessionType = this.Parent.FindControl("LblSessionType") as Label;
                    LblSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Label LblSessionTopic = this.Parent.FindControl("LblSessionTopic") as Label;
                    LblSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                    //RadAgenda.Text = ds.Tables[0].Rows[0]["Agenda"].ToString();
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    //RadAgenda.ReadOnly = true;
                    //RadDescription.ReadOnly = true;

                    RadStrengths.Text = ds.Tables[0].Rows[0]["Strengths"].ToString();
                    RadOpportunities.Text = ds.Tables[0].Rows[0]["Opportunity"].ToString();
                    RadPositiveBehaviour.Text = ds.Tables[0].Rows[0]["PositiveBehaviour"].ToString();
                    RadOpportunityBehaviour.Text = ds.Tables[0].Rows[0]["OpportunityBehaviour"].ToString();
                    RadBeginBehaviourComments.Text = ds.Tables[0].Rows[0]["BeginBehaviourComments"].ToString();
                    RadBehaviourEffect.Text = ds.Tables[0].Rows[0]["BehaviourEffects"].ToString();
                    RadRootCause.Text = ds.Tables[0].Rows[0]["RootCause"].ToString();
                    RadReviewResultsComments.Text = ds.Tables[0].Rows[0]["ReviewResultsComments"].ToString();
                    RadAddressOpportunities.Text = ds.Tables[0].Rows[0]["AddressOpportunities"].ToString();
                    RadActionPlanComments.Text = ds.Tables[0].Rows[0]["ActionPlanComments"].ToString();
                    RadSmartGoals.Text = ds.Tables[0].Rows[0]["SmartGoals"].ToString();
                    RadCreatePlanComments.Text = ds.Tables[0].Rows[0]["CreatePlanComments"].ToString();
                    RadFollowThrough.Text = ds.Tables[0].Rows[0]["FollowThrough"].ToString();
                    string Supervisor = ds.Tables[0].Rows[0]["SupervisorID"].ToString();

                    Label Label3 = this.Parent.FindControl("Label3") as Label;
                    Label3.Text = "My Reviews";
                    Label Label1 = this.Parent.FindControl("Label1") as Label;
                    Label1.Text = " > QA Review ";
                    Label Label2 = this.Parent.FindControl("Label2") as Label;
                    Label2.Text = " > " + ds.Tables[0].Rows[0]["CoacheeName"].ToString();

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacheeSignedDate"])))
                    {
                        if (Convert.ToInt32(Supervisor) == CIMNumber)
                        {
                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = true;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = true;


                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                        else
                        {

                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = false;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = false;


                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                    }
                    else
                    {

                        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                        RadCommitment.Enabled = false;
                        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                        RadSave.Visible = false;


                        Label lab = this.Parent.FindControl("lblstatus") as Label;
                        lab.Visible = true;

                        string stats = "*Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString() + " " + ds.Tables[0].Rows[0]["SupervisorSignedOffDate"].ToString();
                        lab.Text = stats;
                    }

                    //if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacherSignedDate"])))
                    //{
                    //    if (Convert.ToInt32(CoacherCIM) == CIMNumber)
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //    else
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //    RadCommitment.Enabled = false;
                    //    RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //    RadSave.Visible = false;
                    //    RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);
                RadGridPRR.DataSource = ds;
                RadGridPRR.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        private bool IsPH(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        public void LoadPreviousCoachingNotes(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                RadGridCN.DataSource = ds;
                RadGridCN.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadPreviousPerformanceResults(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPR.DataSource = ds;
                RadGridPR.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void DisableElements()
        {
            RadDescription.Enabled = false;
            RadPositiveBehaviour.Enabled = false;
            RadOpportunityBehaviour.Enabled = false;
            RadBeginBehaviourComments.Enabled = false;
            RadAddressOpportunities.Enabled = false;
            RadActionPlanComments.Enabled = false;
            RadSmartGoals.Enabled = false;
            RadCreatePlanComments.Enabled = false;
            if (RadStrengths.Visible)
            {
                RadStrengths.Enabled = false;
            }
            if (RadOpportunities.Visible)
            {
                RadOpportunities.Enabled = false;
            }
            if (RadBehaviourEffect.Visible)
            {
                RadBehaviourEffect.Enabled = false;
            }
            if (RadRootCause.Visible)
            {
                RadRootCause.Enabled = false;
            }
            if (RadReviewResultsComments.Visible)
            {
                RadReviewResultsComments.Enabled = false;
            }
            if (RadFollowThrough.Visible)
            {
                RadFollowThrough.Enabled = false;
            }
        }
    }
}