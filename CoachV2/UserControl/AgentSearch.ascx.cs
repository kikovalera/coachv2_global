﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class AgentSearch : System.Web.UI.UserControl
    {
        SidebarUserControl SidebarUsercontrol1;
        ProfileMainUserControl ProfileMainUserControl1;
        ProfileExperienceUserControl ProfEx;
        ProfileEducUserControl ProfEduc;
        ProfileSkillUserControl ProfSkill;
        ProfileCertUserControl ProfCert;
        ProfileExtraInfoUserCtrl ProfExtra;
        AgentSearch AgentUcontrol;

        protected void Page_Load(object sender, EventArgs e)
        {
            SidebarUsercontrol1 = (SidebarUserControl)this.Page.Master.FindControl("ContentPlaceHolderSidebar").FindControl("SidebarUsercontrol1");
            ProfileMainUserControl1 = (ProfileMainUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileMainUserControl1");
            ProfEx = (ProfileExperienceUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileExperienceUserControl1");
            ProfEduc = (ProfileEducUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEducUserControl1");
            ProfSkill = (ProfileSkillUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileSkillUserControl1");
            ProfCert = (ProfileCertUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileCertUserControl1");
            ProfExtra = (ProfileExtraInfoUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileExtraInfoUserCtrl1");           
            AgentUcontrol = (AgentSearch)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("AgentSearchCtrl1");


            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            int cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);
            TextBox TxtSearch = (TextBox)SidebarUsercontrol1.FindControl("TxtSearch");

            if (TxtSearch.Text != "")
            {
                ProfileMainUserControl1.Visible = false;
                ProfEx.Visible = false;
                ProfEduc.Visible = false;
                ProfSkill.Visible = false;
                ProfCert.Visible = false;
                ProfExtra.Visible = false;

                SearchGrid.Visible = true;
                SearchGrid.DataSource = DataHelper.GetSearchQuery_forAgent(TxtSearch.Text, cimNo);
                SearchGrid.DataBind();
                lblrelsearch.Text = "   " + "<b>" + " ' " + Convert.ToString(TxtSearch.Text) + " ' " + "</b>";
            }
        }

        protected void SearchGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //cimNo = TxtSearchCIM.Text == "" ? Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]) : Convert.ToInt32(TxtSearchCIM.Text);

            int cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);

            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    TextBox TxtSearch = (TextBox)SidebarUsercontrol1.FindControl("TxtSearch");
                    if (TxtSearch.Text != "")
                    {
                        SearchGrid.Visible = true;
                        SearchGrid.DataSource = DataHelper.GetSearchQuery(TxtSearch.Text, cimNo);
                        SearchGrid.DataBind();

                        TextBox textsearch = (TextBox)SidebarUsercontrol1.FindControl("TxtSearch");

                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {


                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = textsearch.Text.ToLower();
                            string searchterm = Convert.ToString(textsearch.Text);

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            string Account = null;

                             
                             
                          
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
             
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            int itmindx = Item.ItemIndex + 1;
                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {



                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = textsearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = textsearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = textsearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = textsearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = textsearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = textsearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = textsearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }
                    }
                    //else
                    //{
                    //}

                }
                catch (Exception ex)
                {
                    SearchGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }


        protected void SearchGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //cimNo = TxtSearchCIM.Text == "" ? Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]) : Convert.ToInt32(TxtSearchCIM.Text);

            int cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);
            if (e.Item is GridDataItem)
            {
                try
                {

                    TextBox textsearch = (TextBox)SidebarUsercontrol1.FindControl("TxtSearch");
                    //GridDataItem item = (GridDataItem)e.Item;

                    foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {
                      

                        string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        string txtdesctolow = txtdesc.Text.ToLower();
                        string txtseatolow = textsearch.Text.ToLower();
                        string searchterm = Convert.ToString(textsearch.Text);

                        //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                        }
                        //added for talk talk 
                        //string Account = null;

                         
                        string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                        if ((formtype == null) || (formtype == ""))
                        {

                            formtype = "0";
                        }

                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                        if (Convert.ToInt32(val2) == 1)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                        }

                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 4)
                        {

                            hlink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 5)
                        {
                            hlink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 6)
                        {
                            hlink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                        }
                        else
                        {
                            hlink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }
                        //hyperlink
                    
                        int itmindx = Item.ItemIndex + 1;
                        if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                        {

                            

                            //description
                            string str = null;
                            string[] strArr = null;
                            int count = 0;
                            str = txtdesc.Text;
                            char[] splitchar = { ' ' };
                            strArr = str.Split(splitchar);
                            string desc = "";

                            string str_searchval = null;
                            string[] str_searchArr = null;
                            str_searchval = textsearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            for (count = 0; count <= strArr.Length - 1; count++)
                            {
                                string val1 = strArr[count].ToLower();
                                if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                {
                                    desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                }
                                else
                                {
                                    desc = desc + " " + strArr[count].ToString();

                                }

                            }
                            txtdesc.Text = desc;
                            //description

                            //Strengths 
                            string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtstrengths = (Label)Item.FindControl("Label6");
                            string txtstrengthslow = txtstrengths.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrStrengths = null;
                                int count1 = 0;
                                str = txtstrengths.Text;
                                strArrStrengths = str.Split(splitchar);
                                string strengths = "";
                                str_searchval = textsearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                {
                                    string val1 = strArrStrengths[count1].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        strengths = strengths + " " + strArrStrengths[count1].ToString();

                                    }

                                }
                                txtstrengths.Text = strengths;


                            }
                            //Strengths 

                            //Opportunity 
                            string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtOpportunity = (Label)Item.FindControl("Label7");
                            string txtOpportunitylow = txtOpportunity.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrOpportunity = null;
                                int count2 = 0;
                                str = txtOpportunity.Text;
                                strArrOpportunity = str.Split(splitchar);
                                string Opportunity = "";
                                str_searchval = textsearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                {
                                    string val1 = strArrOpportunity[count2].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                    }

                                }
                                txtOpportunity.Text = Opportunity;


                            }
                            //Opportunity 
                            //session name  
                            string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtSessionname = (Label)Item.FindControl("Label3");
                            string txtSessionnamelow = txtSessionname.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrSessionname = null;
                                int count3 = 0;
                                str = txtSessionname.Text;
                                strArrSessionname = str.Split(splitchar);
                                string Sessionname = "";
                                str_searchval = textsearch.Text;


                                for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                {
                                    string val1 = strArrSessionname[count3].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                    }

                                }
                                txtSessionname.Text = Sessionname;


                            }
                            //session name 


                        }
                        else
                        {


                            string str_searchval = null;
                            string str = "";
                            string[] str_searchArr = null;
                            char[] splitchar = { ' ' };
                            str_searchval = textsearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            //created on 
                            string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtCreatedon = (Label)Item.FindControl("Label4");
                            string txtCreatedonlow = txtCreatedon.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrCreatedon = null;
                                int count4 = 0;
                                str = txtCreatedon.Text;
                                strArrCreatedon = str.Split(splitchar);
                                string Createdon = "";
                                str_searchval = textsearch.Text;

                                for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                {
                                    string val1 = strArrCreatedon[count4].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                    }
                                    else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                }
                                txtCreatedon.Text = Createdon;


                            }
                            //createdon

                            //followdate 
                            string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtfollowdate = (Label)Item.FindControl("Label5");
                            string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrfollowdate = null;
                                int count5 = 0;
                                str = txtfollowdate.Text;
                                strArrfollowdate = str.Split(splitchar);
                                string followdate = "";
                                str_searchval = textsearch.Text;

                                for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                {
                                    string val1 = strArrfollowdate[count5].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                    }
                                    else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                }
                                txtfollowdate.Text = followdate;


                            }
                            //followdate
                        }
                    }

                }
                catch (Exception ex) {

                    string myStringVariable = ex.ToString();
                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                
                }
            
                
            }
        }
        protected void btn_clear_Click(object sender, EventArgs e)
        {

            TextBox SearchText = (TextBox)SidebarUsercontrol1.FindControl("TxtSearch");
            SearchText.Text = "";
            SearchGrid.DataSource = null;
            SearchGrid.DataBind();
            ProfileMainUserControl1.Visible = false;
            ProfEx.Visible = false;
            ProfEduc.Visible = false;
            ProfSkill.Visible = false;
            ProfCert.Visible = false;
            ProfExtra.Visible = false;
            lblrelsearch.Text = "";
        }

        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    
    }
}