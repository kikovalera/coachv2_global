﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TLChartDashboard.ascx.cs"
    Inherits="CoachV2.UserControl.TLDashboard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<div class="panel-group" id="Div_TLCHARTS">      
   <div class="row row-custom">
            <div class="col-sm-5">
                <form>
                <div class="input-group">
                    </span>
                </div>
                </form>
            </div>
        </div> 
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTL">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Charts<span class="glyphicon glyphicon-triangle-top triangletop"> </span>&nbsp;
                </h4>
            </div>
        </a>
    </div>
    <div id="collapseTL" class="panel-collapse collapse in">
    <br />
        <div class="form-horizontal col-md-12" role="form">
            <button type="button" class="btn btn-custom" id="check_graph">
                Overall</button>
            <button type="button" class="btn btn-custom" id="check_graph_voc">
                VOC</button>
            <button type="button" class="btn btn-custom" id="check_graph_fcr">
                FCR</button>
            <button type="button" class="btn btn-custom" id="check_graph_aht">
                AHT</button>
        </div>

<div   id="overalldate"  style="width: 100% ">        
        <br />
        <br />
        <br />
<table class="table table-condensed">
            <thead>
                <tr>
                    <th>
                        <font size="4">Overall Coaching Frequency</font>
                    </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3" style="display: none;">
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                        AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                        <asp:ListItem Value="0" Text="All" />
                    </asp:DropDownList>
                </div>
 </div>
<div id="overallfrequency" style="width: 100%; height: 400px;" > 
</div>

<div   id="overall_behaviordates">
        <br />
        <br />
        <br />
        <br />
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>
                        <font size="4">Overall Behavior</font>
                    </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span> </div>
                    <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3" >
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                     <telerik:RadComboBox  ID="cb_kpi_behavior1" runat="server" DataValueField="KPIID" DataTextField="Name" ResolvedRenderMode="Classic"  >
                     </telerik:RadComboBox>
                </div>
            </div>
<div id="behaviorvsKPI" style="width: 100%; height: 400px; ">
</div>
<div  id="overall_kpivsb" >
        <br />
        <br />
        <br />
        <br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Behavior vs KPI</font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3" >
                    <div class="help">
                        KPI <span class="glyphicon glyphicon-triangle-bottom"></span></div>
                     <telerik:RadComboBox  ID="cb_kpi_behavior_kpi" runat="server" DataValueField="KPIID" DataTextField="Name" ResolvedRenderMode="Classic"  >
                     </telerik:RadComboBox>
                </div>
            </div>
<div id="DIV2_kpivsscore" style="width: 100%; height: 400px; ">
</div>

<div  id="aht_dates" >
<br />
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Coaching Frequency Vs. AHT Score</font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_aht_start1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_aht_end1" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
            </div>
<div id="aht_score" style="width: 100%; height: 400px;  ">
</div>
<div id = "ahtglidedates">
<br />
<br />
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">AHT Glide Path Targets Vs. Actual</font>
                               </th>
                </tr>
            </thead>
        </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_ahtglide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_ahtglide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>&nbsp; 
                </div>
            </div>
<div id="AHTGlide"  style="width: 100%; height: 400px; " >
</div>

<div   id="fcr_dates1" >
<br /> 
<br />
<br />
        <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Coaching Frequency Vs. FCR Score</font>
                               </th>
                </tr>
            </thead>
        </table>

                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcr_start1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcr_end1" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="DIV2_FCRScore" style="width: 100%; height: 400px; ">
</div>
<div   id="Div2_FCRGlidedates" >
<br /> 
<br />
<br />
    <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                    <font size="4">FCR Glide Path Targets Vs. Actual</font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcrglide_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_fcrglide_end" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker>
                </div>
</div>
<div id="DIV2_FCRGlide" style="width: 100%; height: 400px; " >
</div>
<div   id="overallvocdate" >
<br /><br /> <br />        
   <table class="table table-condensed">
        <thead>
            <tr>
                <th>
                    <font size="4">Coaching Frequency Vs. VOC Score</font>
                </th>
            </tr>
        </thead>
    </table>
                <div class="col-sm-3">
                    <div class="help">
                        From  <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    <telerik:RadDatePicker ID="dp_start_voc" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-3">
                    <div class="help">
                        To   <span class="glyphicon glyphicon-calendar"></span></div>
                    <telerik:RadDatePicker ID="dp_end_voc" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd"  Calendar-ClientEvents-OnDateSelected="OnDateSelected2">
                    </telerik:RadDatePicker> 
                </div>
</div>

<div id="DIV2_VOCScore" style="width: 100%; height: 400px;">
</div>
<br />
<div class="form-group">           
            <asp:HiddenField ID="MyCIM" runat="server" />
            <div >
                <div > 
                    <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                        padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>No data found</span>
                    <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                        runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
                        Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                        Style="padding-top: 10px; padding-bottom: 10px;">
                    </asp:CompareValidator>
                </div>
            </div>
        </div>
</div>
</div>--%>
<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../libs/jquery/dist/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<%--<script type='text/javascript'>

    function OnDateSelected(sender, args) {
        // $("#check_graph").removeAttr("disabled");
    }

    function OnDateSelected2(sender, args) {
        //$("#check_graph").removeAttr("disabled");
    }
    
    //var data = [];

    $("#err-msg").hide();

    $('#overallfrequency').hide(); 
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide(); 
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide(); 
    $('#aht_score').hide(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRGlide').hide();
    $('#DIV2_FCRScore').hide(); 
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#AHTGlide').hide(); 
    $('#ahtglidedates').hide();

    $("#check_graph").on("click", function () {

    $('#overalldate').show();
    $('#overallfrequency').show();
    $('#overall_behaviordates').show();
    $('#aht_dates').hide();
    $('#behaviorvsKPI').show(); 
    $('#overall_kpivsb').show();
    $('#DIV2_kpivsscore').show(); 
    $('#aht_score').hide(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();   
    $('#Div2_FCRGlidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#AHTGlide').hide(); 
    $('#ahtglidedates').hide();

                var data = [];
                var data_aht1 = [];
                var data_aht2 = [];
                var data3 = [];
                var data_kpivsbehavior1 = [];
                var data_kpivsbehavior2 = [];
                var  data_bvsk1 = [];
                var  data_bvsk2 = [];
               

        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

               var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
               var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
               var kpiid1 = kpiid.get_selectedItem().get_value()
               //alert(kpiid1);

                $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: "../FakeApi.asmx/getData_overall_ForTL",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            //c = f.TargetCount;
                            data.push([f.ADate, f.CoachCount]);
                            //                    data2.push([f.ADate, f.TargetCount]);

                            $("#err-msg").hide();

                        });


                        var overallfrequency = new Highcharts.Chart({
                            chart: {
                                renderTo: 'overallfrequency',
                                type: 'area',
                                spacingBottom: 30
                            },
                           title: {
                                text: '',
                                align: 'left',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontSize: '20px',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            xAxis: {
                                type: "category",
                                color: '#FF00FF'
                            },
                            yAxis: {
                                allowDecimals: false,
                                title: {
                                    text: ''
                                },
                                color: '#FF00FF'
                            },
                            tooltip: {
                                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                            },
                            plotOptions: {
                                area: {
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle',
                                        radius: 5,
                                        states: {
                                            hover: {
                                                enabled: true
                                            }
                                        }
                                    },
                                    fillOpacity: 1,
                                    fillColor: '#289CCC'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Coaching Frequency',
                                data: data,
                                marker: {
                                    fillColor: '#fff',
                                    lineWidth: 2,
                                    lineColor: '#ccc'
                                }
                            }]
                        }); //OVERALL FREQUENCY DIV1
                    } //SUCCESSFUNCTION OA FRE
                }); //AJAX OVERALL FREQUENCY


        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");

               var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
               var kpiid1 = kpiid.get_selectedItem().get_value()
                $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: "../FakeApi.asmx/getData_overallbehavior_ForTL",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            //c = f.TargetCount;
                            data3.push([f.description, f.CoachCount]);
                            //                    data2.push([f.ADate, f.TargetCount]);

                            $("#err-msg").hide();

                        });

                        var behaviorvsKPI = new Highcharts.Chart({
                            chart: {
                                renderTo: 'behaviorvsKPI',
                                  zoomType: 'xy',
                                  },
                        title: {
                            text: '',
                            align: 'left',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontSize: '20px',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        xAxis: {
                            type: "category",
                            color: '#FF00FF'
                        },
                        yAxis: [{
                            allowDecimals: false,
                            title: {
                                text: '',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            color: '#FF00FF'

                        },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                        tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },


                    series: [{
                        name: 'Overall Behavior',
                        data: data3,
                        type: 'column',
                        yaxis: 0,
                        color: '#2F9473',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL Behavior
            } //SUCCESS BEHAVIOR
        }); //BEHAVIOR AJAX

        var RadDatePickerbkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>");
        var selectedDatebkpi1 = RadDatePickerbkpi1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerbkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>");
        var selectedDatebkpi2 = RadDatePickerbkpi2.get_selectedDate().format("yyyy-MM-dd");

        var kpiidbkpi = $find('<%=cb_kpi_behavior_kpi.ClientID %>');
        var kpiidbkpi1 = kpiidbkpi.get_selectedItem().get_value()

        $.ajax({
                    type: "POST",
                    data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: "../FakeApi.asmx/getData_overallbehaviorvsKPI_ForTL",
                    dataType: 'json',
                    success: function (msg) {

                        if (msg != null && msg.d == null) {
                            $("#err-msg").show();
                            //fplot($("#placeholder"), [[]], opts);
                        }
                        else {
                            $("#err-msg").hide();
                        }

                        $.each(msg, function (e, f) {

                            data_bvsk1.push([f.descriptions, f.coachedkpis]);
                            data_bvsk2.push([f.descriptions, f.AvgCurr]);

                            $("#err-msg").hide();

                        });

                        var DIV2_kpivsscore = new Highcharts.Chart({
                            chart: {
                                renderTo: 'DIV2_kpivsscore',
                                  zoomType: 'xy',
                                  },
                        title: {
                            text: '',
                            align: 'left',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontSize: '20px',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        xAxis: {
                            type: "category",
                            color: '#FF00FF'
                        },
                        yAxis: [{
                            allowDecimals: true,
                            title: {
                                text: '',
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            color: '#FF00FF'

                        },
                    { allowDecimals: true,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                        tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },


                    series: [{
                        name: 'Behavior',
                        data: data_bvsk1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL BEHAVIORvsKPI
            } //SUCCESS BEHAVIORvsKPI
        }); //BEHAVIORvsKPI AJAX       		
}); //READY FUNCTION



 $("#check_graph_voc").on("click", function () {
    $('#overalldate').hide();
    $('#overallfrequency').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide(); 
    $('#overall_kpivsb').hide();
    $('#DIV2_kpivsscore').hide(); 
    $('#aht_dates').hide();
    $('#aht_score').hide(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide();
    $('#Div2_FCRGlidedates').hide();
    $('#DIV2_FCRGlide').hide();
    $('#aht_dates').hide();
    $('#overallvocdate').show();
    $('#DIV2_VOCScore').show();   
    $('#AHTGlide').hide(); 
    $('#ahtglidedates').hide();

        var data_voc1 = [];
        var data_voc2 = [];
        var data_bvsk1 = [];
        var data_bvsk2 = [];
        var RadDatePickervoc1 = $find("<%= dp_start_voc.ClientID %>");
        var selectedDatevoc1 = RadDatePickervoc1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickervoc2 = $find("<%= dp_end_voc.ClientID %>");
        var selectedDatevoc2 = RadDatePickervoc2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
                
//                var selectedDate1 = "2017-01-01";
//                var selectedDate2 = "2017-02-28";
//                var CIMNo = "10115015";
                var kpiid = "4"



	$.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatevoc1 + "', EndDate: '" + selectedDatevoc2 + "', CIMNo: '" + CIMNo  + "'}",
            contentType: "application/json; charset=utf-8",
            url: "../FakeApi.asmx/getDataTL_VOCScore",
            // getDataTL_VOCScore
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

   
                    data_voc1.push([f.ADate, f.CoachCount]);
                    data_voc2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

          var DIV2_VOCScore = new Highcharts.Chart({
                    chart: {
                        renderTo: 'DIV2_VOCScore' ,
                        zoomType: 'xy'
                    },
                      
                    title: {
                        text: '',
                         align: 'left',
                            style: {
                              // margin: '50px', // does not work for some reasons, see workaround below
                              color: '#707070',
                              fontSize: '20px',
                              fontWeight: 'bold',
                              textTransform: 'none'
                            }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'VOC Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'TOTAL VOC Coaching',
                        data: data_voc1,
                        type: 'column',
                        yaxis: 0,
                        color: '#2F9473',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: 'VOC',
                        type: 'spline',
                        yAxis: 1,
                        data: data_voc2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                  }
                    }]
            }); //voc
        }
        });   
    
});
                 
    $("#check_graph_fcr").on("click", function () {
    $('#overallfrequency').hide(); 
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide(); 
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide(); 
    $('#aht_score').hide(); 
    $('#fcr_dates1').show();
    $('#DIV2_FCRScore').show(); 
    $('#DIV2_FCRGlide').show();
    $('#Div2_FCRGlidedates').show();
    $('#aht_dates').hide();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#AHTGlide').hide(); 
    $('#ahtglidedates').hide();

                var data = [];
 
                var data_fcr1 = [];
                var data_fcr2 = [];
                var data_fcr3 = [];
                var data_fcr4 = [];
  
                //var selectedDate1 = "2017-01-01";
                //var selectedDate2 = "2017-02-28";
                //var CIMNo = "10115015";
                //var kpiid = "4"

        var RadDatePickerfcr1 = $find("<%= dp_fcr_start1.ClientID %>");
        var selectedDatefcr1 = RadDatePickerfcr1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerfcr2 = $find("<%= dp_fcr_end1.ClientID %>");
        var selectedDatefcr2 = RadDatePickerfcr2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid = 5

	$.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatefcr1 + "', EndDate: '" + selectedDatefcr2 + "', CIMNo: '" + CIMNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: "../FakeApi.asmx/getDataTL_FCRScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_fcr1.push([f.ADate, f.CoachCount]);
                    data_fcr2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

          var DIV2_FCRScore = new Highcharts.Chart({
                    chart: {
                        renderTo: 'DIV2_FCRScore' ,
                        zoomType: 'xy'
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'FCR Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'TOTAL FCR Coaching',
                        data: data_fcr1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: 'FCR',
                        type: 'spline',
                        yAxis: 1,
                        data: data_fcr2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //fcr
        }
        });

		
		//fcr actual glide
		$.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatefcr1 + "', EndDate: '" + selectedDatefcr2 + "', CIMNo: '" + CIMNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: "../FakeApi.asmx/getDataTL_FCRScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_fcr3.push([f.ADate, f.TargetCount]);
                    data_fcr4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

          var DIV2_FCRGlide = new Highcharts.Chart({
                    chart: {
                        renderTo: 'DIV2_FCRGlide' ,
                        zoomType: 'xy'
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Target FCR',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'ACtual FCR',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching Score: <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'Target FCR',
                        data: data_fcr3,
                        type: 'spline',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: 'Actual FCR',
                        type: 'spline',
                        yAxis: 1,
                        data: data_fcr4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //fcr
        }
        });

		
		
		//fcr actual glide
		
		
		
		
		
		});
	

    $("#check_graph_aht").on("click", function () {



    $('#overallfrequency').hide(); 
    $('#overalldate').hide();
    $('#overall_kpivsb').hide();
    $('#overall_behaviordates').hide();
    $('#behaviorvsKPI').hide(); 
    $('#overall_behaviordates').hide();
    $('#DIV2_kpivsscore').hide(); 
    $('#aht_score').show(); 
    $('#fcr_dates1').hide();
    $('#DIV2_FCRScore').hide(); 
    $('#Div2_FCRGlidedates').hide();
    $('#aht_dates').show();
    $('#AHTGlide').show();
    $('#overallvocdate').hide();
    $('#DIV2_VOCScore').hide(); 
    $('#ahtglidedates').show();
    $('#DIV2_FCRGlide').hide();

   var data = [];
                var data_aht1 = [];
                var data_aht2 = [];                
                var data_aht3 = [];                
                var data_aht4 = [];
              //  var selectedDate1 = "2017-01-01";
              //  var selectedDate2 = "2017-02-28";
              //  var CIMNo = "10115015";


        var RadDatePickeraht1 = $find("<%= dp_aht_start1.ClientID %>");
        var selectedDateaht1 = RadDatePickeraht1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickeraht2 = $find("<%= dp_aht_end1.ClientID %>");
        var selectedDateaht2 = RadDatePickeraht2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var kpiid = 4     
        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateaht1 + "', EndDate: '" + selectedDateaht2 + "', CIMNo: '" + CIMNo  + "'}",
            contentType: "application/json; charset=utf-8",
            url: "../FakeApi.asmx/getDataTL_AHTScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht1.push([f.ADate, f.CoachCount]);
                    data_aht2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                var aht_score = new Highcharts.Chart({
                    chart: {
                        renderTo: 'aht_score',
                        zoomType: 'xy'
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Total Coaching',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'AHT Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'TOTAL AHT Coaching',
                    data: data_aht1,
                    type: 'column',
                    yaxis: 0,
                    color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: 'AHT',
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //aht
        }
        });
		
		
		
		//glide actual
		
        var RadDatePickerahtglide1 = $find("<%= dp_ahtglide_start.ClientID %>");
        var selectedDateahtglide1 = RadDatePickerahtglide1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerahtglide2 = $find("<%= dp_ahtglide_end.ClientID %>");
        var selectedDateahtglide2 = RadDatePickerahtglide2.get_selectedDate().format("yyyy-MM-dd");

		$.ajax({
          type: "POST",
            data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo  + "'}",
            contentType: "application/json; charset=utf-8",
            url: "../FakeApi.asmx/getDataTL_AHTScore",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht3.push([f.ADate, f.TargetCount]);
                    data_aht4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });
                var AHTGlide = new Highcharts.Chart({
                    chart: {
                        renderTo: 'AHTGlide',
                        zoomType: 'xy'
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        title: {
                            text: 'Target AHT',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: 'Actual AHT',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching Score: <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                },
                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Target AHT',
                    data: data_aht3,
                    type: 'spline',
                    yaxis: 0,
                     color: '#DAA455',
                    marker: {
                        fillColor: '#fff',
                        lineWidth: 2,
                        lineColor: '#ccc'
                    }
                },
                    {
                        name: 'AHT',
                        type: 'spline',
                        yAxis: 1,
                        data: data_aht4,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
            }); //aht
        }
        });
		
		
		//glide actual
		
		
		
		});
    
$(document).ready(function () {
    $("#check_graph").trigger('click');
});
</script>

--%>