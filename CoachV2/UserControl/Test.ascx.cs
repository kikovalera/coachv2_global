﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class Test : System.Web.UI.UserControl
    {
        public int ReviewIDSelected { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
          
                int ReviewID = ReviewIDSelected;
                RadFReviewID.Text = Convert.ToString(ReviewID);
                RadFReviewID.Enabled = false;
                //DataSet ds_scoring = DataHelper.GetCommentsforTriad(ReviewID);
                //grdPrevCom.DataSource = ds_scoring;
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(ReviewID);
                RadFCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                RadFCoachingDate.Enabled = false;
                LoadCommitmentQuestions(ReviewID);
            //}
        }
        private void LoadCommitmentQuestions(int ReviewID)
        {
            RadTextBox1.Text = "What do you want to achieve?";
            RadTextBox3.Text = "Where are you now? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox5.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that? (SMART)";
            RadTextBox7.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

            DataSet ds_scoring = DataHelper.GetCommentsforTriad(ReviewID);
            if (ds_scoring.Tables[0].Rows.Count > 0)
            {
                RadTextBox2.Text = ds_scoring.Tables[0].Rows[0]["Goal_Text"].ToString();
                RadTextBox4.Text = ds_scoring.Tables[0].Rows[0]["Reality_Text"].ToString();
                RadTextBox6.Text = ds_scoring.Tables[0].Rows[0]["Options_Text"].ToString();
                RadTextBox8.Text = ds_scoring.Tables[0].Rows[0]["WayForward_Text"].ToString();
            
            }
        }
    }
}