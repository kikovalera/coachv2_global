﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class RevODQA : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] != null)
                {
                    int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 1260;
                    GetMassCoachingDetails(CoachingTicket);
                    LoadDocumentationsReview();
                    LoadKPIReview(CoachingTicket);
                    LoadPreviousCoachingNotes(CoachingTicket);
                    LoadPreviousCommitment(CoachingTicket);
                    GetCommitment(CoachingTicket);
                    DisableElements();


                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                //int CoachingTicket = 755;
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(CoachingTicket, 3);
                //ds = ws.GetUploadedDocuments(79);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    RadDocumentationReview.DataSource = ds;
                    RadDocumentationReview.Rebind();
                }
                else
                {
                    RadDocumentationReview.DataSource = string.Empty;
                    RadDocumentationReview.Rebind();
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string CoacherCIM = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                    RadTextBox RadReviewID = this.Parent.FindControl("RadReviewID") as RadTextBox;
                    RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                    RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblCimNumber = this.Parent.FindControl("LblCimNumber") as Label;
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                    Label LblFullName = this.Parent.FindControl("LblFullName") as Label;
                    LblFullName.Text = ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    //RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    //RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblSessionType = this.Parent.FindControl("LblSessionType") as Label;
                    LblSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Label LblSessionTopic = this.Parent.FindControl("LblSessionTopic") as Label;
                    LblSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                    //RadAgenda.Text = ds.Tables[0].Rows[0]["Agenda"].ToString();
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    //RadAgenda.ReadOnly = true;
                    //RadDescription.ReadOnly = true;
                    string Supervisor = ds.Tables[0].Rows[0]["SupervisorID"].ToString();

                    RadStrengths.Text = ds.Tables[0].Rows[0]["Strengths"].ToString();
                    RadOpportunities.Text = ds.Tables[0].Rows[0]["Opportunity"].ToString();


                    Label Label3 = this.Parent.FindControl("Label3") as Label;
                    Label3.Text = "My Reviews";
                    Label Label1 = this.Parent.FindControl("Label1") as Label;
                    Label1.Text = " > QA Review ";
                    Label Label2 = this.Parent.FindControl("Label2") as Label;
                    Label2.Text = " > " + ds.Tables[0].Rows[0]["CoacheeName"].ToString();

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacheeSignedDate"])))
                    {
                        if (Convert.ToInt32(Supervisor) == CIMNumber)
                        {                            
                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = true;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = true;


                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                        else
                        {                         

                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = false;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = false;

                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                    }
                    else
                    {
                        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                        RadCommitment.Enabled = false;
                        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                        RadSave.Visible = false;
                        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();


                        Label lab = this.Parent.FindControl("lblstatus") as Label;
                        lab.Visible = true;

                        string stats = "*Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString() + " " + ds.Tables[0].Rows[0]["SupervisorSignedOffDate"].ToString();
                        lab.Text = stats;
                    }

                    //if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacherSignedDate"])))
                    //{
                    //    if (Convert.ToInt32(CoacherCIM) == CIMNumber)
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //    else
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //    RadCommitment.Enabled = false;
                    //    RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //    RadSave.Visible = false;
                    //    RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //}
                }
            }
            catch (Exception ex)
            {               
                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReviewOD(ReviewID);
                RadGridPRR.DataSource = ds;
                RadGridPRR.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }       
        public void LoadPreviousCoachingNotes(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                RadGridCN.DataSource = ds;
                RadGridCN.DataBind();
            }
            catch (Exception ex)
            {              
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void DisableElements()
        {

            RadDescription.Enabled = false;
            if (RadStrengths.Visible)
            {
                RadStrengths.Enabled = false;
            }
            if (RadOpportunities.Visible)
            {
                RadOpportunities.Enabled = false;
            }
            if (RadCoacherFeedback.Visible)
            {
                RadCoacherFeedback.Enabled = false;
            }
        }
        public void LoadPreviousCommitment(int CoachingTicket)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetIncHistoryDataTable(CoachingTicket, 1);

            foreach (DataRow row in dt.Rows)
            {
                int id = row.Field<Int32>("ReviewID");
                Test Test2 = (Test)LoadControl("Test.ascx");
                Test2.ReviewIDSelected = Convert.ToInt32(id);
                PreviousCommitment.Controls.Add(Test2);
            }
        }
        public void GetCommitment(int CoachingTicket)
        {            
            RadTextBox1.Text = "What do you want to achieve?";
            RadTextBox3.Text = "Where are you now? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox5.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that? (SMART)";
            RadTextBox7.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

            DataSet ds_scoring = DataHelper.GetCommentsforTriad(CoachingTicket);
            if (ds_scoring.Tables[0].Rows.Count > 0)
            {
                RadTextBox2.Text = ds_scoring.Tables[0].Rows[0]["Goal_Text"].ToString();
                RadTextBox4.Text = ds_scoring.Tables[0].Rows[0]["Reality_Text"].ToString();
                RadTextBox6.Text = ds_scoring.Tables[0].Rows[0]["Options_Text"].ToString();
                RadTextBox8.Text = ds_scoring.Tables[0].Rows[0]["WayForward_Text"].ToString();

            }
        }
    }
}