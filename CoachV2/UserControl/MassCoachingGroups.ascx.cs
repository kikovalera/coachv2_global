﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class MassCoachingGroups : System.Web.UI.UserControl
    {
        int CompanyGroup;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                CompanyGroup = GetCompanyGroup(CIMNumber);
                //PopulateSites(CompanyGroup);
                //PopulateClients();
                //PopulateCampaigns(CompanyGroup, CIMNumber, Convert.ToInt32(RadAccount.SelectedValue));
                GetAccounts(CIMNumber);
                GetDropDownSessions();              
                //PopulateTopics();
               
            }
        }

        protected int GetCompanyGroup(int CimNumber)
        {
            int retval = 0;
            DataAccess ws = new DataAccess();
            retval = ws.GetCompanyGroup(CimNumber, 0);
            return retval;
        }

        protected void PopulateSites(int CompanyGroup)
        {
            try
            {
                DataSet ds = null;
                DataAccessGenericWeb ws = new DataAccessGenericWeb();
                ds = ws.GetCompanySites(CompanyGroup);

                RadSite.DataSource = ds;
                RadSite.DataTextField = "CompanySite";
                RadSite.DataValueField = "CompanySiteID";
                RadSite.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void PopulateClients()
        {
            try
            {
                DataSet ds = null;
                DataAccessGenericWeb ws = new DataAccessGenericWeb();
                ds = ws.GetClients();

                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "client";
                RadAccount.DataValueField = "clientid";
                RadAccount.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        //protected void PopulateCampaigns(int CompanyGroup, int CimNumber, int ClientID)
        //{
        //    try
        //    {
        //        DataSet ds = null;
        //        DataAccessGenericWeb ws = new DataAccessGenericWeb();
        //        ds = ws.GetCampaigns(CompanyGroup, CimNumber, ClientID);

        //        RadCampaign.DataSource = ds;
        //        RadCampaign.DataTextField = "Account";
        //        RadCampaign.DataValueField = "AccountID";
        //        RadCampaign.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }
        //}

        //protected void RadAccount_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //    int CIMNumber = Convert.ToInt32(cim_num);
        //    CompanyGroup = GetCompanyGroup(CIMNumber);
        //    //PopulateCampaigns(CompanyGroup, CIMNumber, Convert.ToInt32(RadAccount.SelectedValue));

        //}
        protected void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }
        protected void PopulateTopics()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void GetDropDownTopics(int SessionID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics(SessionID);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void RadSessionType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }


        protected void GetAccounts(int CimNumber)
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                RadCampaign.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }

                var distinctRowsSites = (from DataRow dRowSites in dt.Rows
                                         select new { col1Site = dRowSites["CompanySiteID"], col2Site = dRowSites["companysite"] }).Distinct();

                RadSite.Items.Clear();

                foreach (var row in distinctRowsSites)
                {
                    RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2Site.ToString(), row.col1Site.ToString()));
                }

                var distinctRowsClient = (from DataRow dRowClient in dt.Rows
                                          select new { col1Client = dRowClient["clientID"], col2Client = dRowClient["client"] }).Distinct();

                RadAccount.Items.Clear();

                foreach (var row in distinctRowsClient)
                {
                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2Client.ToString(), row.col1Client.ToString()));
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }

        }

        protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        {
            try
            {
                RadTeamLeader.Items.Clear();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSubordinatesWithTeam(CimNumber, AccountID);
                RadTeamLeader.DataSource = ds;
                RadTeamLeader.DataTextField = "Name";
                RadTeamLeader.DataValueField = "CimNumber";
                RadTeamLeader.DataBind();

                DataSet dsInfo = null;
                DataAccess wsInfo = new DataAccess();
                dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
                if (dsInfo.Tables[0].Rows.Count > 0)
                {
                    string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                    string FullName = FirstName + " " + LastName;
                    RadTeamLeader.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void RadCampaign_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            GetSubordinatesWithTeam(CIMNumber, Convert.ToInt32(RadCampaign.SelectedValue));
        }

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();

                int Site;
                if (RadSite.SelectedValue == "")
                {
                    Site = 0;
                }
                else
                {
                    Site = Convert.ToInt32(RadSite.SelectedValue);
                }

                int Account;
                if (RadAccount.SelectedValue == "")
                {
                    Account = 0;
                }
                else
                {
                    Account = Convert.ToInt32(RadAccount.SelectedValue);
                }

                int Campaign;
                if (RadCampaign.SelectedValue == "")
                {
                    Campaign=0;
                }
                else
                {
                    Campaign = Convert.ToInt32(RadCampaign.SelectedValue);
                }

                int TeamLeader;
                if (RadTeamLeader.SelectedValue == "")
                {
                    TeamLeader = 0;
                }
                else
                {
                    TeamLeader = Convert.ToInt32(RadTeamLeader.SelectedValue);
                }

                ws.InsertMassCoachingGroups(2, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber,Site,Account,Campaign, false); //,TeamLeader
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "alert('Successfully saved review.')", true);
                DisableItems();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);

            }
        }

        public void DisableItems()
        {
            RadSite.Enabled = false;
            RadAccount.Enabled = false;
            RadCampaign.Enabled = false;
            RadSessionType.Enabled = false;
            RadSessionTopic.Enabled = false;
            RadTeamLeader.Enabled = false;
            RadAgenda.Enabled = false;
            RadDescription.Enabled = false;
            RadCoacherSignOff.Visible = false;
        }

        public void refresh()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            RadDescription.Enabled = true;
            RadAgenda.Enabled = true;
            RadDescription.Text = "";
            RadAgenda.Text = "";
            CompanyGroup = GetCompanyGroup(CIMNumber);
            GetAccounts(CIMNumber);
            RadSessionTopic.DataSource = null;
            RadSessionTopic.DataBind();
            RadSessionType.Enabled = true;
            RadSessionTopic.Enabled = true;
            RadSessionType.Items.Clear();
            RadSessionType.DataSource = null;
            RadSessionType.DataBind();
            RadSessionType.Text = String.Empty;
            RadSessionType.ClearSelection();
            GetDropDownSessions();
            RadCoacherSignOff.Visible = true;
            RadSessionTopic.Items.Clear();
            RadSessionTopic.DataSource = null;
            RadSessionTopic.DataBind();
            RadSessionTopic.Text = String.Empty;
            RadSessionTopic.ClearSelection();
     
        }
    }
}