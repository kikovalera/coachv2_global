﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Charting;
using Telerik.Web.UI;
using CoachV2.AppCode;
using System.Web.Services;


namespace CoachV2.UserControl
{
    public partial class Ops_OMCharts : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string URL = (HttpContext.Current.Request.Url.Host == "localhost" ? "http://" : "http://") + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);

            FakeURLID.Value = URL;
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num =   ds.Tables[0].Rows[0][2].ToString();
            string gmail =   ds.Tables[0].Rows[0][13].ToString();
            int intcim = Convert.ToInt32(cim_num);





            //Charts
            MyCIM.Value = Convert.ToString(cim_num);
            DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
            int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
            //DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
            DataAccess ws = new DataAccess();
            //DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
            DataSet ds_KPIforAgent = DataHelper.gettopkpis(intcim); //ws.GetKPIListPerAcct(Convert.ToString(acctno));
            ovrall_StartDate.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            ovrall_EndDate.SelectedDate = DateTime.Now;
            ddKPIList2.AppendDataBoundItems = true;
            ddKPIList2.DataTextField = "KPIName";
            ddKPIList2.DataValueField = "KPIId";
            ddKPIList2.DataSource = DataHelper.gettopkpis(intcim); //DataHelper.GetAllKPIs();
            ddKPIList2.DataBind();

            //overall behaviordropdown
            dp_behavior_start.SelectedDate = DateTime.Today.AddMonths(-1); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behavior_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior1.DataSource = ds_KPIforAgent;
            cb_kpi_behavior1.DataBind();

            dlKPI.DataSource = DataHelper.gettopkpis(intcim);
            dlKPI.DataBind();




            //DataSet ds_kpi_hierarchy = DataHelper.gettopkpis(intcim);
            //if (ds_kpi_hierarchy.Tables[0].Rows.Count > 0)
            //{
            //    for (int ctr = 0; ctr < ds_kpi_hierarchy.Tables[0].Rows.Count; ctr++)
            //    {
            //        if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "1")
            //        {
            //            lbl_1stkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            btn_top1kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            lbl_1stkpi_coaching_frequency.Text = " " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //        }
            //        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "2")
            //        {
            //            lbl_2ndkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            lbl_2ndkpi_averagecoachingscore.Text = " " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //            btn_top2kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        }

            //        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "3")
            //        {
            //            lbl_3rdkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            lbl_3rdkpi_averagecoachingscore.Text = " " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //            btn_top3kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        }
            //        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "4")
            //        {
            //            lbl_4thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            lbl_4thkpi_averagecoachingscore.Text = " " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //            btn_top4kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        }
            //        else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "5")
            //        {
            //            lbl_5thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //            lbl_5thkpi_averagecoachingscore.Text = " " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
            //            btn_top5kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
            //        }
            //    }


            //}

            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior1.DataBind();
            }

            //behavior vs kpi
            dp_behaviorkpi_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_behaviorkpi_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior_kpi.DataSource = ds_KPIforAgent;
            cb_kpi_behavior_kpi.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior_kpi.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior_kpi.DataBind();
            }

            //voc dates 
            dp_start_voc.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_end_voc.SelectedDate = DateTime.Now;
            dp_glide_voc_start.SelectedDate = DateTime.Today.AddMonths(-1);
            dp_glide_voc_end.SelectedDate = DateTime.Now;

            ////aht dates
            //dp_aht_start1.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_aht_end1.SelectedDate = DateTime.Now;
            //dp_ahtglide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_ahtglide_end.SelectedDate = DateTime.Now;

            ////fcr dates
            //dp_fcrglide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_fcrglide_end.SelectedDate = DateTime.Now;
            //dp_fcr_start1.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_fcr_end1.SelectedDate = DateTime.Now;

            RadDatePicker1.SelectedDate = DateTime.Today.AddMonths(-1);
            RadDatePicker2.SelectedDate = DateTime.Now;

            //dp_top4_score_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_top4_score_end.SelectedDate = DateTime.Now;

            //dp_top4_glide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_top4_glide_end.SelectedDate = DateTime.Now;

            //dp_top5_score_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_top5_score_end.SelectedDate = DateTime.Now;

            //dp_top5_glide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            //dp_top5_glide_end.SelectedDate = DateTime.Now;


            //tl drop down
            DataSet ds_TLlist = DataHelper.GetTLAgents(intcim, acctno);
            cb_overallTL.DataSource = ds_TLlist;
            cb_overallTL.DataBind();

            //overallbehavior
            cb_overallbehavioTL.DataSource = ds_TLlist;
            cb_overallbehavioTL.DataBind();

            //behavior vs kpi
            cb_overallbvkTL.DataSource = ds_TLlist;
            cb_overallbvkTL.DataBind();

            //aht f vs s
            //cb_AHTFvS.DataSource = ds_TLlist;
            //cb_AHTFvS.DataBind();

            ////aht t vs a
            //cb_AHTAvT_TL.DataSource = ds_TLlist;
            //cb_AHTAvT_TL.DataBind();

            ////fcr f vs s
            //cb_FCRFvS_TL.DataSource = ds_TLlist;
            //cb_FCRFvS_TL.DataBind();


            ////fcr a vs t 
            //cb_FCRAvT_TL.DataSource = ds_TLlist;
            //cb_FCRAvT_TL.DataBind();
            HF_ACCTNO.Value = Convert.ToString(acctno);
            //TL_AHTScore.Value = 
            //ds_GETSUBAHTFvSAgent.SelectParameters["CimNumber"].DefaultValue = HF_ACCTNO.Value;

            //voc F vs S 
            cb_VOCFvS_TL.DataSource = ds_TLlist;
            cb_VOCFvS_TL.DataBind();

            //voc a vs t 
            cb_VOCAvT_TL.DataSource = ds_TLlist;
            cb_VOCAvT_TL.DataBind();

            cb_triadTl.DataSource = ds_TLlist;
            cb_triadTl.DataBind();

            //cb_tllist10.DataSource = ds_TLlist;
            //cb_tllist10.DataBind();

            //cb_tllist11.DataSource = ds_TLlist;
            //cb_tllist11.DataBind();

            //cb_tllist12.DataSource = ds_TLlist;
            //cb_tllist12.DataBind();

            //cb_tllist13.DataSource = ds_TLlist;
            //cb_tllist13.DataBind();


            DataSet ds_DVlist = DataHelper.GetDataView();

            cb_overalldataview.DataSource = ds_DVlist;
            cb_overalldataview.DataBind();

            //cb_top1scoredataview.DataSource = ds_DVlist;
            //cb_top1scoredataview.DataBind();
            //cb_top1avtdataview.DataSource = ds_DVlist;
            //cb_top1avtdataview.DataBind();

            //cb_top2scoredataview.DataSource = ds_DVlist;
            //cb_top2scoredataview.DataBind();
            //cb_top2avtdataview.DataSource = ds_DVlist;
            //cb_top2avtdataview.DataBind();

            cb_top3scoredataview.DataSource = ds_DVlist;
            cb_top3scoredataview.DataBind();
            cb_top3avtdataview.DataSource = ds_DVlist;
            cb_top3avtdataview.DataBind();

            //cb_top4scoredataview.DataSource = ds_DVlist;
            //cb_top4scoredataview.DataBind();
            //cb_top4avtdataview.DataSource = ds_DVlist;
            //cb_top4avtdataview.DataBind();

            //cb_top5scoredataview.DataSource = ds_DVlist;
            //cb_top5scoredataview.DataBind();
            //cb_top5avtdataview.DataSource = ds_DVlist;
            //cb_top5avtdataview.DataBind();

            cb_triaddataview.DataSource = ds_DVlist;
            cb_triaddataview.DataBind();
        }
    }
}