﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

namespace CoachV2.UserControl
{
    public partial class ProfileEditExtraUserCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                RadAdditionalInfo.Text = ds.Tables[0].Rows[0]["AdditionalInfo"].ToString();
            }

        }
        protected void RadSave_Click(object sender, EventArgs e)
        {
            try
            {
                MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string userId = mu.ProviderUserKey.ToString();
                DataHelper.UpdateAdditionalInfo(userId, RadAdditionalInfo.Text);
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadSave_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            
            }
        }
    }
}