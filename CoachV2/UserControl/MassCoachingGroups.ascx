﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MassCoachingGroups.ascx.cs" Inherits="CoachV2.UserControl.MassCoachingGroups" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadSessionType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadSessionTopic" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadCampaign">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadTeamLeader" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAccount">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCampaign" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadSite">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadAccount" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<asp:Panel ID="CoachingAllPanel" runat="server">
<div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Select Coachee
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <form class="form-horizontal">
                            <div class="form-group">
                                <label for="Site" class="control-label col-xs-4">
                                    Site</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadSite" runat="server" class="form-control" RenderMode="Lightweight"
                                        DropDownAutoWidth="Enabled">
                                    </telerik:RadComboBox>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="Account" class="control-label col-xs-4">
                                    Account</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadAccount" runat="server" class="form-control" RenderMode="Lightweight"
                                        DropDownAutoWidth="Enabled" CausesValidation="true" AutoPostBack="true" EmptyMessage="Select Account">
                                    </telerik:RadComboBox>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="Campaign" class="control-label col-xs-4">
                                    Campaign</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadCampaign" runat="server" class="form-control" RenderMode="Lightweight"
                                        DropDownAutoWidth="Enabled" EmptyMessage="Select Campaign" AutoPostBack="true"
                                        onselectedindexchanged="RadCampaign_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </div>
                            </div>
                             <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="SessionType" class="control-label col-xs-4">
                                    Session Type</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" RenderMode="Lightweight"
                                        DropDownAutoWidth="Enabled" AutoPostBack="true" OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged" EmptyMessage="Select Session Type">
                                    </telerik:RadComboBox>
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadSessionType" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="Topic" class="control-label col-xs-4">
                                    Topic</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" RenderMode="Lightweight" EmptyMessage="Select Session Topic"
                                        DropDownAutoWidth="Enabled">
                                    </telerik:RadComboBox>
                                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadSessionTopic" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="Topic" class="control-label col-xs-4">
                                    Team Leader:</label>
                                <div class="col-xs-8">
                                    <telerik:RadComboBox ID="RadTeamLeader" runat="server" class="form-control" RenderMode="Lightweight"
                                        DropDownAutoWidth="Enabled" EmptyMessage="Select Team Leader">
                                    </telerik:RadComboBox>
                                </div>
                            </div>
                            </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</asp:Panel>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        <div class="panel-heading">
            <h4 class="panel-title">
                Description <span class="glyphicon glyphicon-triangle-top triangletop"></span>
            </h4>
        </div>
    </a>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body">
            <telerik:RadTextBox ID="RadDescription" Width="100%" class="form-control" placeholder="Description"
                runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="5" ValidationGroup="MassCoaching">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
        </div>
    </div>
</div>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <div class="panel-heading">
            <h4 class="panel-title">
                Agenda <span class="glyphicon glyphicon-triangle-top triangletop"></span>
            </h4>
        </div>
    </a>
    <div id="collapseThree" class="panel-collapse collapse in">
        <div class="panel-body">
            <telerik:RadTextBox ID="RadAgenda" Width="100%" class="form-control" placeholder="Agenda"
                runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="15">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadAgenda" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
        </div>
    </div>
</div>
<div class="row">
    <br>
    <div class="col-sm-12">
        <span class="addreviewbuttonholder">
            <telerik:RadButton ID="RadCoacherSignOff" CssClass="btn btn-primary" runat="server"
                Text="Coach Sign Off" ValidationGroup="MassCoaching" 
            ForeColor="White" onclick="RadCoacherSignOff_Click">
            </telerik:RadButton>
        </span>
    </div>
</div>
