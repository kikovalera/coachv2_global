﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ops_OMCharts.ascx.cs"
    Inherits="CoachV2.UserControl.Ops_OMCharts" %>
<%-- charts area --%>
<div class="panel-group" id="Div_TLCHARTS">
    <asp:HiddenField ID="FakeURLID" runat="server" />
    <div class="row row-custom">
    </div>
</div>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTL">
        <div class="panel-heading">
            <h4 class="panel-title">
                Charts<span class="glyphicon glyphicon-triangle-top triangletop"> </span>&nbsp;
            </h4>
        </div>
    </a>
</div>
<div id="collapseTL" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="form-horizontal col-md-12" role="form">
            <table>
                <tr>
                    <td>
                        <button type="button" class="btn btn-custom" id="check_graph" style="margin-right: 10px;">
                            Overall</button>
                    </td>
                    <td>
                        <asp:DataList ID="dlKPI" runat="server" RepeatColumns="10" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <button class="btn btn-custom click-me-kpi" kpi-id='<%#Eval("hierarchy").ToString() %>'>
                                    <%# Eval("kpi").ToString()%></button>
                                &nbsp;
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server" Visible="false"
                                    CssClass="blacktext"> 
                                </asp:Label>
                            </FooterTemplate>
                        </asp:DataList>
                    </td>
                    <td>
                        <button type="button" class="btn btn-custom" id="check_graph_triad">
                            Triad Coaching</button>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="HF_ACCTNO" runat="server" />
        <asp:HiddenField ID="TL_AHTScore" runat="server" />
        <div id="overall-holder">
            <div id="overalldate" style="width: 100%">
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th> <br />
                                <font size="4">Overall Team Leaders Coaching Frequency</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallTL" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="ovrall_StartDate" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="ovrall_EndDate" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallChanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overalldataview" runat="server" DataValueField="DataViewID"
                        DataTextField="DataView" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnOverall" class="btn btn-info">
                            Run</button>
                    </div>
                <div class="col-sm-3" style="display: none;">
                    <div class="help">
                        <telerik:RadComboBox ID="cb1" runat="server" AppendDataBoundItems="true" Visible="false">
                        </telerik:RadComboBox>
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                        AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                        <asp:ListItem Value="0" Text="All" />
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <div id="overallfrequency" style="width: 100%; height: 100%;">
            </div>
            <div id="overall_behaviordates">
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4">Top Behavioral Drivers</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbehavioTL" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbehavioAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallBehaviorChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallBehaviorChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                    <telerik:RadComboBox ID="cb_kpi_behavior1" runat="server" DataValueField="KPIID"
                        DataTextField="KPIName" ResolvedRenderMode="Classic" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnBehavior" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="behaviorvsKPI" style="width: 100%; height: 100%;">
            </div>
            <div id="overall_kpivsb">
                <br />
                <br />
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4">Behavioral Drivers Vs. KPI</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbvkTL" runat="server" DataValueField="CimNumber" DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static"
                        OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_overallbvkAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallBehaviorVsKPIChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="OverallBehaviorVsKPIChanger" />
                    </telerik:RadDatePicker>
                    &nbsp;
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_kpi_behavior_kpi" runat="server" DataValueField="KPIID"
                        AppendDataBoundItems="true" DataTextField="KPIName" ResolvedRenderMode="Classic">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnKPIvBehavior" class="btn btn-info">
                            Run</button>
                    </div>
                <%--<div class="col-sm-9">
                </div>--%>
            </div>
            <div id="DIV2_kpivsscore" style="width: 100%; height: 100%;">
            </div>
        </div>
        <div id="chart-holder">
            <div id="overallvocdate">
                <br />
                <br />
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4" id="freqvsscore">Average AHT Score</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_VOCFvS_TL" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_VOCFvS_Agent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_start_voc" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="freqkpichanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_end_voc" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                        DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="freqkpichanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top3scoredataview" runat="server" DataValueField="DataViewID"
                        DataTextField="DataView" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnScoreVKPI" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="DIV2_VOCScore" style="width: 100%; height: 100%;">
            </div>
            <br />
            <div id="Div2_VOCGlidedate">
                <br />
                <br />
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <font size="4" id="tarvsactual">Targets Vs. Actual</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_VOCAvT_TL" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <%-- OnSelectedIndexChanged="cb_VOCAvT_TL_SelectedIndexChanged"--%>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_VOCAvT_Agent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_glide_voc_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="actvstarchanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="dp_glide_voc_end" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="actvstarchanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Data View <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_top3avtdataview" runat="server" DataValueField="DataViewID"
                        DataTextField="DataView" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnTarVAct" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="Div2_VOCActual" style="width: 100%; height: 100%;">
            </div>
        </div>
        <div id="triad-holder">
            <div id="triaddates">
                <br />
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                <br /><font size="4">Triad Coaching</font>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="col-sm-2">
                    <div class="help">
                        TL <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triadTl" runat="server" DataValueField="CimNumber" DataTextField="Name"
                        AppendDataBoundItems="true" ClientIDMode="Static" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged1">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        Agent <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triadAgent" runat="server" DataValueField="CimNumber"
                        DataTextField="Name" AppendDataBoundItems="true">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        From <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="triadchanger" />
                    </telerik:RadDatePicker>
                </div>
                <div class="col-sm-2">
                    <div class="help">
                        To <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                    <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Empty="Enter end date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd">
                        <ClientEvents OnDateSelected="triadchanger" />
                    </telerik:RadDatePicker>
                </div>
             <%--   <div class="col-sm-9">
                </div>--%>
                <div class="col-sm-2">
                    <div class="help">
                        Data View <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                    <telerik:RadComboBox ID="cb_triaddataview" runat="server" DataValueField="DataViewID"
                        DataTextField="DataView" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </div>
                <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnTriadCoaching" class="btn btn-info">
                            Run</button>
                    </div>
            </div>
            <div id="div_triadcoaching" style="width: 100%; height: 100%;">
            </div>
        </div>
        <br />
        <div class="form-group">
            <asp:HiddenField ID="MyCIM" runat="server" />
            <div>
                <div>
                    <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                        padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>No data found</span>
                    <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                        runat="server" ControlToValidate="ovrall_EndDate" ControlToCompare="ovrall_StartDate"
                        Operator="GreaterThan" Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                        Style="padding-top: 10px; padding-bottom: 10px;">
                    </asp:CompareValidator>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- charts area --%>
<%--test for local--%>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="https://university.transcom.com/TheCoachItaly/libs/highcharts/js/modules/exporting.js"></script>
<%--test for test site --%>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
<script type='text/javascript'>
    var URL = $("#<%= FakeURLID.ClientID %>").val();

    var kpiid;
    var kpilabel;

    function OverallBehaviorVsKPIChanger(sender, args) {

        var startDate = $find('<%=dp_behaviorkpi_start.ClientID%>');
        var endDate = $find('<%=dp_behaviorkpi_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnKPIvBehavior').prop('disabled', true);
        } else {
            $('#BtnKPIvBehavior').prop('disabled', false);
        }

    }

    function OverallBehaviorChanger(sender, args) {

        var startDate = $find('<%=dp_behavior_start.ClientID%>');
        var endDate = $find('<%=dp_behavior_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnBehavior').prop('disabled', true);
        } else {
            $('#BtnBehavior').prop('disabled', false);
        }
    }

    function OverallChanger(sender, args) {
        //overall();
        var startDate = $find('<%=ovrall_StartDate.ClientID%>');
        var endDate = $find('<%=ovrall_EndDate.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnOverall').prop('disabled', true);
        } else {
            $('#BtnOverall').prop('disabled', false);
        }
    }

    function triadchanger(sender, args) {
        var startDate = $find('<%=RadDatePicker1.ClientID%>');
        var endDate = $find('<%=RadDatePicker2.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTriadCoaching').prop('disabled', true);
        } else {
            $('#BtnTriadCoaching').prop('disabled', false);
        }
                
    }

    function freqkpichanger(sender, args) {
        //chart1loader(kpiid, kpilabel);
        var startDate = $find('<%=dp_start_voc.ClientID%>');
        var endDate = $find('<%=dp_end_voc.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnScoreVKPI').prop('disabled', true);
        } else {
            $('#BtnScoreVKPI').prop('disabled', false);
        }
    }

    function actvstarchanger(sender, args) {

        var startDate = $find('<%=dp_glide_voc_start.ClientID%>');
        var endDate = $find('<%=dp_glide_voc_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTarVAct').prop('disabled', true);
        } else {
            $('#BtnTarVAct').prop('disabled', false);
        }
    }

    function getRandomColor(v) {
        //        var letters = '0123456789ABCDEF';
        //        var color = '#';
        //        for (var i = 0; i < 6; i++) {
        //            color += letters[Math.floor(Math.random() * 16)];
        //        }
        //        return color;

        var color = ['#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4', '#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4'];

        return color[v];
    }

    function overall() {

        var data = [];

        var highC = Highcharts.chart('overallfrequency', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: {
                tickInterval: 5,
                min: 0,
                title: {
                    text: ''
                },
                color: '#FF00FF'
            },
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    },
                    fillOpacity: 1,
                    fillColor: '#289CCC'
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },
            series: [{
                name: 'Coaching Frequency',
                data: data,
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        }); //OVERALL FREQUENCY DIV1

        var selectedDate1 = $find("<%= ovrall_StartDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDate2 = $find("<%= ovrall_EndDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value()
        var AgentCimNo = $find('<%=cb_overallAgent.ClientID %>').get_selectedItem().get_value()
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "', TLCimNo: '" + TLCimNo + "',  AgentCimNo: '" + AgentCimNo + "',  DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForOM",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data.push([f.ADate, f.CoachCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data);
                highC.hideLoading();


            } //SUCCESSFUNCTION OA FRE
        });   //AJAX OVERALL FREQUENCY

        highC.showLoading();
    }

    function overallbehavior() {
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var data3 = [];

        var selectedDateb1 = $find("<%= dp_behavior_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateb2 = $find("<%= dp_behavior_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiid1 = $find('<%=cb_kpi_behavior1.ClientID %>').get_selectedItem().get_value();
        var TLCimNo = $find('<%=cb_overallbehavioTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallbehavioAgent.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('behaviorvsKPI', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                min: 0,
                tickInterval: 5,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                series: {
                    colorByPoint: true
                }
            },
            credits: {
                enabled: false
            },


            exporting: {
                showTable: true
            },
            series: [{
                name: 'Overall Behavior',
                data: data3,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        }); //OVERALL FREQUENCY DIV1

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "', TLCimNo: '" + TLCimNo + "', AgentCimno: '" + AgentCimNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_GetBehaviorForOM",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data3.push([f.description, f.CoachCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data3);
                highC.hideLoading();

            } //SUCCESSFUNCTION OA FRE
        });   //AJAX OVERALL FREQUENCY

        highC.showLoading();
    }

    function overallbehaviorvskpi() {

        var selectedDatebkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatebkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiidbkpi1 = $find('<%=cb_kpi_behavior_kpi.ClientID %>').get_selectedItem().get_value();
        var TLCimno = $find('<%=cb_overallbvkTL.ClientID %>').get_selectedItem().get_value();
        var AgentCimNo = $find('<%=cb_overallbvkAgent.ClientID %>').get_selectedItem().get_value();

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var data_bvsk1 = [];
        var data_bvsk2 = [];

        var highC = Highcharts.chart('DIV2_kpivsscore', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        allowDecimals: false,
                        tickInterval: 20,
                        max: 100,
                        labels: {
                            format: '{value}%'
                        }
                    }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },


            series: [{
                name: 'Behavior',
                data: data_bvsk1,
                type: 'column',
                yaxis: 0,
                color: '#27A6D9',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "', TLCimno: '" + TLCimno + "', AgentCimno: '" + AgentCimNo + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForOM",
            dataType: 'json',
            success: function (msg) {


                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_bvsk1);
                highC.series[1].setData(data_bvsk2);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }

    function chart1loader(v, labelx) {
        $('.highcharts-data-table').remove();
        var data1 = [];
        var data2 = [];

        var colorx = getRandomColor(v);

        $("#freqvsscore").html("Coaching Frequency VS. " + labelx.toUpperCase() + " Score");

        var highC = Highcharts.chart('DIV2_VOCScore', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: 'Total Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: labelx + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        allowDecimals: false,
                        color: '#FF00FF',
                        opposite: true,
                        //min: 0,
                        max: ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0) ? 2000 : 100),
                        tickInterval: 20,
                        labels: {
                            format: ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0) ? '{value}' : '{value}%') 
                        }
                    }],
            tooltip: {
                pointFormat: labelx + ': <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'TOTAL ' + labelx + ' Coaching',
                data: data1,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: labelx,
                        type: 'spline',
                        yAxis: 1,
                        data: data2,
                        color: colorx,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return labelx + ' <b>' + this.y.toFixed(2) + ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0) ? '' : '%') + ' </b>';
                            }
                        }
                    }]
        });

        var selectedDatevoc1 = $find("<%= dp_start_voc.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatevoc2 = $find("<%= dp_end_voc.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var TLCimno = $find('<%=cb_VOCFvS_TL.ClientID %>').get_selectedItem().get_value();
        var AgentCimno = $find('<%=cb_VOCFvS_Agent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_top3scoredataview.ClientID %>').get_selectedItem().get_value();

        var KPI = v;

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatevoc1 + "', EndDate: '" + selectedDatevoc2 + "', CIMNo: '" + CIMNo + "', TLCimno: '" + TLCimno + "', AgentCimno: '" + AgentCimno + "', KPI: '" + KPI + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getGlidepath_OM_coachingfrequency", 
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data1.push([f.ADate, f.CoachCount]);
                    data2.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data1);
                highC.series[1].setData(data2);

                highC.hideLoading();

            }
        });

        highC.showLoading();
    }

    function chart2loader(v, labelx) {


        var data_voc3 = [];
        var data_voc4 = [];

        var colorx = getRandomColor(v);

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var selectedDatevocglide1 = $find("<%= dp_glide_voc_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatervocglide2 = $find("<%= dp_glide_voc_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var TLCimno = $find('<%=cb_VOCAvT_TL.ClientID %>').get_selectedItem().get_value()
        var AgentCimno = $find('<%=cb_VOCAvT_Agent.ClientID %>').get_selectedItem().get_value()
        var DataView = $find('<%=cb_top3avtdataview.ClientID %>').get_selectedItem().get_value()

        var KPI = v;

        $("#tarvsactual").html("Target " + labelx.toUpperCase() + " Vs. Actual" + labelx);

        var highC = Highcharts.chart('Div2_VOCActual', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                //   
                title: {
                    text: 'Target ' + labelx,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: 'Actual ' + labelx,
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },

            exporting: {
                showTable: true
            },
            series: [{
                name: 'Target ' + labelx,
                data: data_voc3,
                type: 'spline',
                yAxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                },
                tooltip: {
                    pointFormatter: function () {
                        if (data_voc3[this.category].toString().indexOf('*') >= 0) {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return labelx + ' <b>' + this.y.toFixed(2) + ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0)  ? '' : '%') + ' </b> fiscal month target change';
                            }
                        } else {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return labelx + ' <b>' + this.y.toFixed(2) + ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0)  ? '' : '%') + '</b>';
                            }
                        }
                    }
                }
            },
                    {
                        name: 'Actual ' + labelx,
                        type: 'spline',
                        yAxis: 0,
                        data: data_voc4,
                        color: colorx,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                if (this.y == 0 && parseInt(data_voc3[this.index][1]) == 0) {
                                    return labelx + '<b> No data available</b>';
                                } else {
                                    return labelx + ' <b>' + this.y.toFixed(2) + ((labelx.toString().indexOf('AHT') >= 0 || labelx.toString().indexOf('OCR') >= 0)  ? '' : '%') + '</b>';
                                }
                            }
                        }
                    }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatevocglide1 + "', EndDate: '" + selectedDatervocglide2 + "', CIMNo: '" + CIMNo + "', TLCimno: '" + TLCimno + "', AgentCimno: '" + AgentCimno + "', KPI: '" + KPI + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getactualglide_for_OM",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data_voc3.push([f.ADate, f.TargetCount]);
                    data_voc4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_voc3);
                highC.series[1].setData(data_voc4);

                highC.hideLoading();
            }
        });

        highC.showLoading();
    }


    function loadtriad() {
        
        $('.highcharts-data-table').remove();
        var data = [];
        var data_triad1 = [];
        var data_triad2 = [];

        var highC = Highcharts.chart('div_triadcoaching', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: 'Triad Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    {
                        title: {
                            text: 'Triad Coaching Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        allowDecimals: false,
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        max: 100,
                        tickInterval: 20,
                        labels: {
                            format: '{value}%'
                        }
                    }],
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },

            exporting: {
                showTable: true
            },
            series: [{
                name: 'Total Triad Coaching',
                data: data_triad1,
                type: 'column',
                yaxis: 0,
                color: '#DAA455',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: 'Triad Coaching Score',
                        type: 'spline',
                        yAxis: 1,
                        data: data_triad2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' Triad Coaching Score: <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
        }); //Triad Coaching

        var selectedDatetriad1 =  $find("<%= RadDatePicker1.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatetriad2 = $find("<%= RadDatePicker2.ClientID %>").get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var TLCimno = $find('<%=cb_triadTl.ClientID %>').get_selectedItem().get_value();
        var AgentCimno = $find('<%=cb_triadAgent.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_triaddataview.ClientID %>').get_selectedItem().get_value();

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatetriad1 + "', EndDate: '" + selectedDatetriad2 + "', CIMNo: '" + CIMNo + "', TLCimno: '" + TLCimno + "', AgentCimno: '" + AgentCimno + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_TriadCoaching",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data_triad1.push([f.ADate, f.CoachCount]);
                    data_triad2.push([f.ADate, f.CurrentCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_triad1);
                highC.series[1].setData(data_triad2);

                highC.hideLoading();
            }
        });

        highC.showLoading();

    }

    function OnClientSelectedIndexChanged1(sender, eventArgs) {

        var id = sender.get_id();
        var TLCimNo;
        var combo;
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();

        if (id.toString() == "cb_overallTL") { 
            TLCimNo = $find('<%=cb_overallTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallAgent.ClientID %>");
        }

        if (id.toString() == "cb_overallbehavioTL") {
            TLCimNo = $find('<%=cb_overallbehavioTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbehavioAgent.ClientID %>");
        }

        if (id.toString() == "cb_overallbvkTL") {
            TLCimNo = $find('<%=cb_overallbvkTL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_overallbvkAgent.ClientID %>");
        }

        if (id.toString() == "cb_VOCFvS_TL") {
            TLCimNo = $find('<%=cb_VOCFvS_TL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_VOCFvS_Agent.ClientID %>");
        } 

        if (id.toString() == "cb_VOCAvT_TL") {
            TLCimNo = $find('<%=cb_VOCAvT_TL.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_VOCAvT_Agent.ClientID %>");
        }

        if (id.toString() == "cb_triadTl") {
            TLCimNo = $find('<%=cb_triadTl.ClientID %>').get_selectedItem().get_value();
            combo = $find("<%= cb_triadAgent.ClientID %>");
        } 


        combo.get_items().remove(comboItem);
        combo.get_items().clear();
        comboItem.set_text("All");
        comboItem.set_value('0');
        combo.trackChanges();
        combo.get_items().add(comboItem);
        comboItem.select();
        combo.commitChanges();

        $.ajax({
            type: 'GET',
            url: 'ChartHandlers/AgentListOM.ashx',
            data: { cim: TLCimNo },
            contentType: "application/json; charset=utf-8",
            dataType: 'JSON',
            success: function (data) {
                for (i = 0; i < data.Table.length; i++) {
                    var combox = combo;
                    var comboItemx = new Telerik.Web.UI.RadComboBoxItem();
                    comboItemx.set_text(data.Table[i].Name);
                    comboItemx.set_value(data.Table[i].CimNumber);
                    combox.trackChanges();
                    combox.get_items().add(comboItemx);
                    comboItemx.select();
                    combox.commitChanges();
                }
            },
            error: function (e) {
                console.log(e);
            }
        });

    }

    $(document).ready(function () {

        $("#check_graph").on("click", function () {
            $("#triad-holder").hide();
            $("#chart-holder").hide();

            $(this).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            $('.click-me-kpi').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $('#check_graph_triad').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

            $('.highcharts-data-table').remove();
            $("#overall-holder").show();

            overall();


            overallbehavior();
            overallbehaviorvskpi();
        });




        $("#check_graph").trigger('click');

        $(".click-me-kpi").on("click", function () {
            var v = $(this).attr("kpi-id");
            var label = $(this).text();

            kpiid = v;
            kpilabel = label;
            resetColor(v);

            $("#chart-holder").show();
            $("#overall-holder").hide();
            $("#triad-holder").hide();

            chart1loader(v, label);
            chart2loader(v, label);

            return false;
        });


        $("#BtnOverall").on("click", function () {
            overall();
            return false;
        });

        $("#BtnBehavior").on("click", function () {
            overallbehavior();
            return false;
        });

        $("#BtnKPIvBehavior").on("click", function () {
            overallbehaviorvskpi();
            return false;
        });

        $("#BtnScoreVKPI").on("click", function () {
            chart1loader(kpiid, kpilabel);
            return false;
        });

        $("#BtnTarVAct").on("click", function () {
            chart2loader(kpiid, kpilabel);
            return false;
        });

        $("#BtnTriadCoaching").on("click", function () {
            loadtriad();
            return false;
        });



        $("#check_graph_triad").on("click", function () {
            $("#chart-holder").hide();
            $("#overall-holder").hide();
            $("#triad-holder").show();

            $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $(".click-me-kpi").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            loadtriad();

            return false;
        });

        function resetColor(v) {
            $.each($('.click-me-kpi'), function (key, xx) {
                var kpiid = $(xx).attr('kpi-id');

                if (kpiid == v) {
                    $(xx).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

                } else {
                    $(xx).css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
                }

                $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
                $("#check_graph_triad").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

            });
        }
    });
</script>
