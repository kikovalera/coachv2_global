﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CoachV2.UserControl
{
    public partial class SidebarEditProfileUserControl : System.Web.UI.UserControl
    {

        ProfileEditMainUserCtrl ProfileEditMainUserCtrl1;
        ProfileEditExpUserCtrl ProfileEditExpUserCtrl1;
        ProfileEditEducUserCtrl ProfileEditEducUserCtrl1;
        ProfileEditSkillUserControl ProfileEditSkillUserControl1;
        ProfileEditCertUserControl ProfileEditCertCtrl;
        ProfileEditExtraUserCtrl ProfileEditExtraCtrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1 = (ProfileEditMainUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditMainUserCtrl1");
            ProfileEditExpUserCtrl1 = (ProfileEditExpUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditExpUserCtrl1");
            ProfileEditEducUserCtrl1 = (ProfileEditEducUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditEducUserCtrl1");
            ProfileEditSkillUserControl1 = (ProfileEditSkillUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditSkillUserControl1");
            ProfileEditCertCtrl = (ProfileEditCertUserControl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditCertUserControl1");
            ProfileEditExtraCtrl = (ProfileEditExtraUserCtrl)this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("ProfileEditExtraUserCtrl1");
            //

            ProfileEditMainUserCtrl1.Visible = Request.QueryString["tab"] == null ? true : false;
            ProfileEditExpUserCtrl1.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "experience" ? true : false;
            ProfileEditEducUserCtrl1.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "education" ? true : false;
            ProfileEditSkillUserControl1.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "skill" ? true : false;
            ProfileEditCertCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "cert" ? true : false;
            ProfileEditExtraCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "extra" ? true : false;
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1.Visible = true;

            ProfileEditExpUserCtrl1.Visible = false;
            ProfileEditEducUserCtrl1.Visible = false;
            ProfileEditSkillUserControl1.Visible = false;
            ProfileEditCertCtrl.Visible = false;

        }

        protected void Unnamed2_Click(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1.Visible = false;
            ProfileEditExpUserCtrl1.Visible = true;
            ProfileEditEducUserCtrl1.Visible = false;
            ProfileEditSkillUserControl1.Visible = false;
            ProfileEditCertCtrl.Visible = false;
        }

        protected void Unnamed3_Click(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1.Visible = false;
            ProfileEditExpUserCtrl1.Visible = false;
            ProfileEditEducUserCtrl1.Visible = true;
            ProfileEditSkillUserControl1.Visible = false;
            ProfileEditCertCtrl.Visible = false;
        }
        protected void Unnamed4_Click(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1.Visible = false;
            ProfileEditExpUserCtrl1.Visible = false;
            ProfileEditEducUserCtrl1.Visible = false;
            ProfileEditSkillUserControl1.Visible = true;
            ProfileEditCertCtrl.Visible = false;
        }

        protected void Unnamed5_Click(object sender, EventArgs e)
        {
            ProfileEditMainUserCtrl1.Visible = false;
            ProfileEditExpUserCtrl1.Visible = false;
            ProfileEditEducUserCtrl1.Visible = false;
            ProfileEditSkillUserControl1.Visible = false;
            ProfileEditCertCtrl.Visible = true;
        }
        
    }
}