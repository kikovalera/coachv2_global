﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileExperienceUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileExperienceUserControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom" id="fake_title" runat="server">
        <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx?tab=experience" ID="RadLinkButton1"
            ForeColor="White" ToolTip="Edit profile" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit trianglebottom"></i>
        </asp:HyperLink>
        <div>
            &nbsp;<span class="glyphicon glyphicon-user"></span> MY Experience
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                       Working Experience <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <telerik:RadGrid ID="ExpGrid" runat="server" OnNeedDataSource="ExpGrid_NeedDataSource"
                        OnPreRender="ExpGrid_PreRender" AllowAutomaticInserts="False"  CssClass="RemoveBorders" GridLines="None" 
                          BorderStyle="None"     >
                        
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>

                        <MasterTableView CommandItemDisplay="Top" AllowPaging="True"
                            PageSize="20" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ExpID"       >
                            <CommandItemSettings ShowAddNewRecordButton="false"  />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Company" HeaderText="Company" DataField="Company"
                                    HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Role" HeaderText="Role" DataField="Role" HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Industy" HeaderText="Industry" DataField="Industry"
                                    HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn UniqueName="StartDate" HeaderText="From" DataField="StartDate"  
                                    DataType="System.DateTime"  DataFormatString="{0:y}" HeaderStyle-Font-Bold="true"   >
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn UniqueName="EndDate" HeaderText="To" DataField="EndDate"
                                    DataType="System.DateTime" DataFormatString="{0:y}" HeaderStyle-Font-Bold="true"  >
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn UniqueName="ReasonForLeaving" HeaderText="Reason For Leaving"
                                    DataField="ReasonForLeaving" HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>

                            </Columns>
                        </MasterTableView>
                          <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
</div>
