﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;

namespace CoachV2.UserControl
{
    public partial class DashboardMyReviews : System.Web.UI.UserControl
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            //SearchReview searchuc;
            //searchuc = this.Page.Master.FindControl("SearchGrid");
            RadGrid sgrid = (RadGrid)FindControl("SearchGrid");
            Page.Form.DefaultButton = BtnSearchQuery.UniqueID;

            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString(); //"10113944";//
               
                int CIMNumber = Convert.ToInt32(cim_num);
                HasQARole(CIMNumber);
                HasSupervisorRole(CIMNumber);
                HasHRRole(CIMNumber);
                //if TalkTalk
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["Client"].ToString() == "TalkTalk")
                    {
                        HyperLinkRev.NavigateUrl = string.Format("~/AddReview.aspx");
                        Session.Remove("CIMMyTeam");
                        //SomeGlobalVariables.CIMMyTeam = null;
                        SomeGlobalVariables sgv = new SomeGlobalVariables();
                        sgv.CIMMyTeam = null;

                    }
                    else
                    {
                        if (CheckIfOperations(CIMNumber) == true)
                        {
                            HyperLinkRev.NavigateUrl = string.Format("~/AddReview.aspx");
                            Session.Remove("CIMMyTeam");
                            //SomeGlobalVariables.CIMMyTeam = null;
                            SomeGlobalVariables sgv = new SomeGlobalVariables();
                            sgv.CIMMyTeam = null;
                        }
                        else
                        {
                            HyperLinkRev.NavigateUrl = string.Format("~/AddReview.aspx");
                            Session.Remove("CIMMyTeam");
                            //SomeGlobalVariables.CIMMyTeam = null;
                            SomeGlobalVariables sgv = new SomeGlobalVariables();
                            sgv.CIMMyTeam = null;
                        }
                    }
                    
                    //if hr department
                    
                }
                else
                {
                    HyperLinkRev.NavigateUrl = string.Format("~/AddReview.aspx");
                }
            }


        }

        public bool IsQA(int CimNumber)
        {
            //DataAccess ws2 = new DataAccess();
            //string department = "";
            //department = ws2.getuserdept(CimNumber);

            //if (department == "QA")
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CimNumber);


            if (ds_getrolefromsap.Tables[0].Rows.Count > 0)
            {
                if (ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString().Contains("QA"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void HasQARole(int CimNumber)
        {

            try
            {
                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CimNumber, 18);
                HtmlGenericControl li = (HtmlGenericControl)SearchPane.FindControl("listlinkqa");

                if (ds_userselect2.Tables[0].Rows.Count > 0)
                {

                    if (IsQA(CimNumber))
                    {
                       
                        li.Visible = true;

                        HyperLinkQA.Visible = true;
                        
                        Search.Visible = false;
                        SearchPane.Attributes["class"] = "col-sm-12";
                    }
                    else
                    {
                        li.Visible = false;

                        HyperLinkQA.Visible = false;
                        Search.Visible = true;
                        SearchPane.Attributes["class"] = "col-sm-9";
                    }
                }
                else
                {
                    li.Visible = false;

                    HyperLinkQA.Visible = false;
                    //Search.Visible = false; //false; //
                    //SearchPane.Attributes["class"] = "col-sm-9";
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
            }
        }

        public void HasHRRole(int CimNumber)
        {
            try
            {
                DataSet ds_hr = DataHelper.CheckifHr(CimNumber);

                if (ds_hr.Tables[0].Rows.Count > 0)
                {
                    HyperLinkHR.Visible = true;
                    Search.Visible = false;
                    SearchPane.Attributes["class"] = "col-sm-12";
                  
                }
                else
                {
                    HyperLinkHR.Visible = false;
                    //Search.Visible = false; //false; //
                    //SearchPane.Attributes["class"] = "col-sm-9";
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
            }
        }

        public void HasSupervisorRole(int CimNumber)
        {
            try
            {

                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CimNumber, 2);

                if (ds_userselect2.Tables[0].Rows.Count > 0)
                {

                    bool Supervisor;
                    Supervisor = CheckIfHasSubordinates(CimNumber);

                    if (Supervisor == true)
                    {
                        HyperLinkRev.Visible = true;
                    }
                    else
                    {
                        HyperLinkRev.Visible = false;
                    }
                }
                else
                {
                    HyperLinkRev.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }

        protected bool CheckIfOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        protected void BtnSearchQuery_Click(object sender, EventArgs e)
        {

            try
            {
                if ( HttpContext.Current.Request.Url.PathAndQuery != "~/SearchReview.aspx")
                {
                    if ((TxtSearch.Text != "") && (Request.QueryString["searchkey"] != null))
                    {
                        string urlsearch1 =  Convert.ToString(TxtSearch.Text);// HttpContext.Current.Request.Url.PathAndQuery + "?searchkey=" +  Convert.ToString(TxtSearch.Text);  //"/SearchReview.aspx?searchkey=" +  Convert.ToString(TxtSearch.Text);
                        //encrypt
                        string enctxt = DataHelper.encrypt1(urlsearch1);
                        string linkpage = "~/SearchReview.aspx?searchkey=" + enctxt;
                        Response.Redirect(linkpage);
                        //Response.Redirect(urlsearch);
                    }
                    else if ((TxtSearch.Text == "") && (Request.QueryString["searchkey"] != null))
                    {
                        string ModalLabel = "Searching Failed, input value first before searching";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                    }
                    else if ((TxtSearch.Text == "") && (Request.QueryString["searchkey"] == null))
                    {
                        string ModalLabel = "Searching Failed, input value first before searching";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                    if ((TxtSearch.Text != "") && (Request.QueryString["searchkey"] == null))
                    {
                        string urlsearch1 = Convert.ToString(TxtSearch.Text);// HttpContext.Current.Request.Url.PathAndQuery + "?searchkey=" +  Convert.ToString(TxtSearch.Text);  //"/SearchReview.aspx?searchkey=" +  Convert.ToString(TxtSearch.Text);
                        //encrypt
                        string enctxt = DataHelper.encrypt1(urlsearch1);
                        string linkpage = "~/SearchReview.aspx?searchkey=" + enctxt;
                        Response.Redirect(linkpage);
                        //Response.Redirect(urlsearch);
                    }
                    //                    SearchViaMyReviewSearchBox();
                //}
                //else
                //{
                    else  if ( HttpContext.Current.Request.Url.PathAndQuery == "~/SearchReview.aspx")
                    {
                        if (Request.QueryString["searchkey"] == null)
                        {
                            ////decrypt
                            //string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["searchkey"]));
                            //SearchViaMyReviewSearchBox(decrypt);
                            SearchViaMyReviewSearchBox(Convert.ToString(TxtSearch.Text));
                        }
                    }
                    else {
                        //decrypt
                        string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["searchkey"]));
                        SearchViaMyReviewSearchBox(Convert.ToString(decrypt));
                        //SearchViaMyReviewSearchBox(Convert.ToString(Request.QueryString["searchkey"]));
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "Searching Failed" + ex.Message;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void SearchViaMyReviewSearchBox(string txtvalue ) {

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            int cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);
            //if (TxtSearch.Text != "")
            //{

                // RadGrid SearchGrid = (RadGrid)this.Page.FindControl("SearchGrid");

                RadGrid SearchGrid = (RadGrid)Parent.FindControl("SearchGrid");
                SearchGrid.Visible = true;
                SearchGrid.DataSource = DataHelper.GetSearchQuery(TxtSearch.Text, cimNo);
                SearchGrid.DataBind();
                DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
                string cim_num = ds.Tables[0].Rows[0][0].ToString();
                int intcim = Convert.ToInt32(cim_num);

                DataSet ds1 = null;
                DataAccess ws1 = new DataAccess();
                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(intcim));
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
                string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                if (rolevalue == "QA")
                    saprolefordashboard = "QA";
                else if (rolevalue == "HR")
                    saprolefordashboard = "HR";
                else
                    saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                string retlabels = DataHelper.ReturnLabels(saprolefordashboard);

                if ((saprolefordashboard == "TL") || (saprolefordashboard == ""))
                {
                    foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {
                        string txt1 = (Item.FindControl("Label1") as Label).Text;
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        string txtdesctolow = txtdesc.Text.ToLower();
                        string txtseatolow = TxtSearch.Text.ToLower();
                        string searchterm = Convert.ToString(TxtSearch.Text);


                        Label lbltriad = (Label)Item.FindControl("Label8");
                        lbltriad.Visible = false;
                        HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                        hlink.Visible = false;

                        //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();                        
                        }
                        //added for talk talk 
                        //string Account = null;


                      
                        string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                        if ((formtype == null) || (formtype == ""))
                        {

                            formtype = "0";
                        }

                        if (Convert.ToInt32(val2) == 1)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {

                                //hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                        }

                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 4)
                        {

                            hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 5)
                        {
                            hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 6)
                        {
                            hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                        }
                        else
                        {
                            hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }
                        //hyperlink

                        if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                        {
                            //description
                            string str = null;
                            string[] strArr = null;
                            int count = 0;
                            str = txtdesc.Text;
                            char[] splitchar = { ' ' };
                            strArr = str.Split(splitchar);
                            string desc = "";

                            string str_searchval = null;
                            string[] str_searchArr = null;
                            str_searchval = TxtSearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            for (count = 0; count <= strArr.Length - 1; count++)
                            {
                                string val1 = strArr[count].ToLower();
                                if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                {
                                    desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                }
                                else
                                {
                                    desc = desc + " " + strArr[count].ToString();

                                }

                            }
                            txtdesc.Text = desc;
                            //description

                            //Strengths 
                            string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtstrengths = (Label)Item.FindControl("Label6");
                            string txtstrengthslow = txtstrengths.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrStrengths = null;
                                int count1 = 0;
                                str = txtstrengths.Text;
                                strArrStrengths = str.Split(splitchar);
                                string strengths = "";
                                str_searchval = TxtSearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                {
                                    string val1 = strArrStrengths[count1].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        strengths = strengths + " " + strArrStrengths[count1].ToString();

                                    }

                                }
                                txtstrengths.Text = strengths;


                            }
                            //Strengths 

                            //Opportunity 
                            string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtOpportunity = (Label)Item.FindControl("Label7");
                            string txtOpportunitylow = txtOpportunity.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrOpportunity = null;
                                int count2 = 0;
                                str = txtOpportunity.Text;
                                strArrOpportunity = str.Split(splitchar);
                                string Opportunity = "";
                                str_searchval = TxtSearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                {
                                    string val1 = strArrOpportunity[count2].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                    }

                                }
                                txtOpportunity.Text = Opportunity;


                            }
                            //Opportunity 
                            //session name  
                            string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtSessionname = (Label)Item.FindControl("Label3");
                            string txtSessionnamelow = txtSessionname.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrSessionname = null;
                                int count3 = 0;
                                str = txtSessionname.Text;
                                strArrSessionname = str.Split(splitchar);
                                string Sessionname = "";
                                str_searchval = TxtSearch.Text;


                                for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                {
                                    string val1 = strArrSessionname[count3].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                    }

                                }
                                txtSessionname.Text = Sessionname;


                            }
                            //session name 


                        }
                        else
                        {


                            string str_searchval = null;
                            string str = "";
                            string[] str_searchArr = null;
                            char[] splitchar = { ' ' };
                            str_searchval = TxtSearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            //created on 
                            string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtCreatedon = (Label)Item.FindControl("Label4");
                            string txtCreatedonlow = txtCreatedon.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrCreatedon = null;
                                int count4 = 0;
                                str = txtCreatedon.Text;
                                strArrCreatedon = str.Split(splitchar);
                                string Createdon = "";
                                str_searchval = TxtSearch.Text;

                                for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                {
                                    string val1 = strArrCreatedon[count4].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                    }
                                    else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                }
                                txtCreatedon.Text = Createdon;


                            }
                            //createdon

                            //followdate 
                            string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtfollowdate = (Label)Item.FindControl("Label5");
                            string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrfollowdate = null;
                                int count5 = 0;
                                str = txtfollowdate.Text;
                                strArrfollowdate = str.Split(splitchar);
                                string followdate = "";
                                str_searchval = TxtSearch.Text;

                                for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                {
                                    string val1 = strArrfollowdate[count5].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                    }
                                    else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                }
                                txtfollowdate.Text = followdate;


                            }
                            //followdate
                        }
                    }

                }
                else if (saprolefordashboard == "HR") //hr
                {
                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                    if ((saprole1 == "TL") || (saprole1 == ""))
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {
                            string txt1 = (Item.FindControl("Label1") as Label).Text;
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);


                            Label lbltriad = (Label)Item.FindControl("Label8");
                            lbltriad.Visible = false;
                            HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                            hlink.Visible = false;

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                           
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    //hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;//hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }
                    }
                    else
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {   //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            string Account = null;

                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);

                            //check if coachee and coacher signed off already
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                            {
                                if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                {
                                    Label lbltriad = (Label)Item.FindControl("Label8");
                                    lbltriad.Visible = true;
                                    HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                    hlink.Visible = true;
                                }
                            }

                            //check if coachee and coacher signed off already

 
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }
                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {



                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }
                    }

                }
                else if (saprolefordashboard == "QA") //QA
                {
                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                    if ((saprole1 == "TL") || (saprole1 == ""))
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {
                            string txt1 = (Item.FindControl("Label1") as Label).Text;
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);


                            Label lbltriad = (Label)Item.FindControl("Label8");
                            lbltriad.Visible = false;
                            HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                            hlink.Visible = false;

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                             
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + txt2 + "&ReviewType=" + val2;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }
                    }
                    else
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {   //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            string Account = null;

                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);

                            //check if coachee and coacher signed off already
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                            {
                                if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                {
                                    Label lbltriad = (Label)Item.FindControl("Label8");
                                    lbltriad.Visible = true;
                                    HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                    hlink.Visible = true;
                                }
                            }

                         
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {



                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }
                    }
                }
                else //if OM and Dir
                {

                     foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {   //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                        }
                        //added for talk talk 
                        string Account = null;

                        string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        string txtdesctolow = txtdesc.Text.ToLower();
                        string txtseatolow = TxtSearch.Text.ToLower();
                        string searchterm = Convert.ToString(TxtSearch.Text);

                        
                        //check if coachee and coacher signed off already
                        if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                        {
                            if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                            { 
                                    Label lbltriad = (Label)Item.FindControl("Label8");
                                    lbltriad.Visible = true;
                                    HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                    hlink.Visible = true;
                               
                            }
                        }

                        string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                        if ((formtype == null) || (formtype == ""))
                        {

                            formtype = "0";
                        }
                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                  
                        if (Convert.ToInt32(val2) == 1)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {

                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1 ;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                        }

                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 4)
                        {

                            hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt;
                            //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 5)
                        {
                            hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 6)
                        {
                            hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                        }
                        else
                        {
                            hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }
                        //hyperlink

                        if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                        {



                            //description
                            string str = null;
                            string[] strArr = null;
                            int count = 0;
                            str = txtdesc.Text;
                            char[] splitchar = { ' ' };
                            strArr = str.Split(splitchar);
                            string desc = "";

                            string str_searchval = null;
                            string[] str_searchArr = null;
                            str_searchval = TxtSearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            for (count = 0; count <= strArr.Length - 1; count++)
                            {
                                string val1 = strArr[count].ToLower();
                                if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                {
                                    desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                }
                                else
                                {
                                    desc = desc + " " + strArr[count].ToString();

                                }

                            }
                            txtdesc.Text = desc;
                            //description

                            //Strengths 
                            string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtstrengths = (Label)Item.FindControl("Label6");
                            string txtstrengthslow = txtstrengths.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrStrengths = null;
                                int count1 = 0;
                                str = txtstrengths.Text;
                                strArrStrengths = str.Split(splitchar);
                                string strengths = "";
                                str_searchval = TxtSearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                {
                                    string val1 = strArrStrengths[count1].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        strengths = strengths + " " + strArrStrengths[count1].ToString();

                                    }

                                }
                                txtstrengths.Text = strengths;


                            }
                            //Strengths 

                            //Opportunity 
                            string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtOpportunity = (Label)Item.FindControl("Label7");
                            string txtOpportunitylow = txtOpportunity.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrOpportunity = null;
                                int count2 = 0;
                                str = txtOpportunity.Text;
                                strArrOpportunity = str.Split(splitchar);
                                string Opportunity = "";
                                str_searchval = TxtSearch.Text;
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                {
                                    string val1 = strArrOpportunity[count2].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                    }

                                }
                                txtOpportunity.Text = Opportunity;


                            }
                            //Opportunity 
                            //session name  
                            string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtSessionname = (Label)Item.FindControl("Label3");
                            string txtSessionnamelow = txtSessionname.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrSessionname = null;
                                int count3 = 0;
                                str = txtSessionname.Text;
                                strArrSessionname = str.Split(splitchar);
                                string Sessionname = "";
                                str_searchval = TxtSearch.Text;


                                for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                {
                                    string val1 = strArrSessionname[count3].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                    }

                                }
                                txtSessionname.Text = Sessionname;


                            }
                            //session name 


                        }
                        else
                        {


                            string str_searchval = null;
                            string str = "";
                            string[] str_searchArr = null;
                            char[] splitchar = { ' ' };
                            str_searchval = TxtSearch.Text;
                            string toup_textsearch = str_searchval.ToLower();
                            str_searchArr = toup_textsearch.Split(splitchar);


                            //created on 
                            string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtCreatedon = (Label)Item.FindControl("Label4");
                            string txtCreatedonlow = txtCreatedon.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrCreatedon = null;
                                int count4 = 0;
                                str = txtCreatedon.Text;
                                strArrCreatedon = str.Split(splitchar);
                                string Createdon = "";
                                str_searchval = TxtSearch.Text;

                                for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                {
                                    string val1 = strArrCreatedon[count4].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                    }
                                    else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                }
                                txtCreatedon.Text = Createdon;


                            }
                            //createdon

                            //followdate 
                            string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtfollowdate = (Label)Item.FindControl("Label5");
                            string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                            //                    string txtseatolow = textsearch.Text.ToLower();
                            if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                string[] strArrfollowdate = null;
                                int count5 = 0;
                                str = txtfollowdate.Text;
                                strArrfollowdate = str.Split(splitchar);
                                string followdate = "";
                                str_searchval = TxtSearch.Text;

                                for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                {
                                    string val1 = strArrfollowdate[count5].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                    }
                                    else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                }
                                txtfollowdate.Text = followdate;


                            }
                            //followdate
                        }
                    }

                }
            //}
            //else
            //{
            //    string ModalLabel = "Searching Failed, input value first before searching";
            //    string ModalHeader = "Error Message";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


            //}
        }
        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}