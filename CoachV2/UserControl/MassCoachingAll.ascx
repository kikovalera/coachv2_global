﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MassCoachingAll.ascx.cs" Inherits="CoachV2.UserControl.MassCoachingAll" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadSessionTypeAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadSessionTopicAll" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<asp:Panel ID="CoachingAllPanel" runat="server">
<div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Coaching Details
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="SessionType" class="control-label col-xs-5">
                                            Session Type</label>
                                        <div class="col-xs-6">
                                             <telerik:RadComboBox ID="RadSessionTypeAll" runat="server" class="form-control" 
                                                 RenderMode="Lightweight" DropDownAutoWidth="Enabled" AutoPostBack="true" 
                                                 onselectedindexchanged="RadSessionTypeAll_SelectedIndexChanged" EmptyMessage="Select Session Type"
                                                 >
                                            </telerik:RadComboBox>
                                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="MassCoaching"
                                        ForeColor="Red" ControlToValidate="RadSessionTypeAll" Display="Dynamic" ErrorMessage="*This is a required field."
                                        CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div>&nbsp;</div>
                                    <div class="form-group">
                                        <label for="Topic" class="control-label col-xs-5">
                                            Topic</label>
                                        <div class="col-xs-6">
                                            <telerik:RadComboBox ID="RadSessionTopicAll" runat="server" class="form-control" RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="Select Session Topic">
                                            </telerik:RadComboBox>
                                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="MassCoaching"
                                        ForeColor="Red" ControlToValidate="RadSessionTopicAll" Display="Dynamic" ErrorMessage="*This is a required field."
                                        CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</asp:Panel>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Description <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                 <telerik:RadTextBox ID="RadDescription" Width="100%" class="form-control" placeholder="Description" runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="5" ValidationGroup="MassCoaching"></telerik:RadTextBox>
                       <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="MassCoaching"
                    ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                    CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Agenda <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseThree" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadAgenda" Width="100%" class="form-control" placeholder="Agenda" runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="15"></telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="MassCoaching"
                    ForeColor="Red" ControlToValidate="RadAgenda" Display="Dynamic" ErrorMessage="*This is a required field."
                    CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <br>
            <div class="col-sm-12">
                <span class="addreviewbuttonholder">
                    <telerik:RadButton ID="RadCoacherSignOff" CssClass="btn btn-primary" 
                    runat="server" Text="Coach Sign Off" ValidationGroup="MassCoaching" 
                    ForeColor="White" onclick="RadCoacherSignOff_Click"></telerik:RadButton>
                </span>
            </div>
        </div>