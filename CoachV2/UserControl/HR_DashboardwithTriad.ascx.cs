﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using CoachV2.AppCode;


namespace CoachV2.UserControl
{
    public partial class HR_DashboardwithTriad : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            string gmail = ds.Tables[0].Rows[0][13].ToString();
            int intcim = Convert.ToInt32(cim_num);
            string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);

            FakeURLID.Value = URL;
            //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //string cim_num = ds.Tables[0].Rows[0][2].ToString();
            //string gmail = ds.Tables[0].Rows[0][13].ToString();
            //int intcim = Convert.ToInt32(cim_num);
            MyCIM.Value = Convert.ToString(cim_num);
            DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
            int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
//            DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
            DataAccess ws = new DataAccess();
            //DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
            DataSet ds_KPIforAgent = ws.GetKPIListPerAcct(Convert.ToString(acctno));

            StartDate.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01"); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            EndDate.SelectedDate = DateTime.Now;
            ddKPIList2.AppendDataBoundItems = true;
            ddKPIList2.DataTextField = "KPIName";
            ddKPIList2.DataValueField = "KPIId";
            ddKPIList2.DataSource = DataHelper.GetAllKPIs();
            ddKPIList2.DataBind();
            //overall behaviordropdown
            dp_behavior_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01"); //DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behavior_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior1.DataSource = ds_KPIforAgent;
            cb_kpi_behavior1.DataBind();

            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior1.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior1.DataBind();
            }
            DataSet ds_kpi_hierarchy = DataHelper.gettopkpis(intcim);
            if (ds_kpi_hierarchy.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < ds_kpi_hierarchy.Tables[0].Rows.Count; ctr++)
                {
                    if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "1")
                    {
                        lbl_1stkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        btn_top1kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        lbl_1stkpi_coaching_frequency.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                    }
                    else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "2")
                    {
                        lbl_2ndkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        lbl_2ndkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                        btn_top2kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                    }

                    else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "3")
                    {
                        lbl_3rdkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        lbl_3rdkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                        btn_top3kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                    }
                    else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "4")
                    {
                        lbl_4thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        lbl_4thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                        btn_top4kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                    }
                    else if (ds_kpi_hierarchy.Tables[0].Rows[ctr]["hierarchy"].ToString() == "5")
                    {
                        lbl_5thkpi_actualvstarget.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                        lbl_5thkpi_averagecoachingscore.Text = "Average " + Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]) + " " + "Score";
                        btn_top5kpi.Text = Convert.ToString(ds_kpi_hierarchy.Tables[0].Rows[ctr]["kpi"]);
                    }
                }


            }
            //behavior vs kpi
            dp_behaviorkpi_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_behaviorkpi_end.SelectedDate = DateTime.Now;
            cb_kpi_behavior_kpi.DataSource = ds_KPIforAgent;
            cb_kpi_behavior_kpi.DataBind();
            if (ds_KPIforAgent.Tables[0].Rows.Count == 0)
            {
                cb_kpi_behavior_kpi.Items.Add(new RadComboBoxItem("N/A", "0"));
                cb_kpi_behavior_kpi.DataBind();
            }

            //voc dates 
            dp_start_voc.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_end_voc.SelectedDate = DateTime.Now;

            //aht dates
            dp_aht_start1.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_aht_end1.SelectedDate = DateTime.Now;
            dp_ahtglide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_ahtglide_end.SelectedDate = DateTime.Now;

            //fcr dates
            dp_fcrglide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_fcrglide_end.SelectedDate = DateTime.Now;
            dp_fcr_start1.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_fcr_end1.SelectedDate = DateTime.Now;

            //triad dates
            RadDatePicker1.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            RadDatePicker2.SelectedDate = DateTime.Now;

            RadDatePicker3.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            RadDatePicker4.SelectedDate = DateTime.Now;

            dp_top4_score_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_top4_score_end.SelectedDate = DateTime.Now;

            dp_top4_glide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_top4_glide_end.SelectedDate = DateTime.Now;

            dp_top5_score_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_top5_score_end.SelectedDate = DateTime.Now;

            dp_top5_glide_start.SelectedDate = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + "01" + "-" + "01");
            dp_top5_glide_end.SelectedDate = DateTime.Now;

            loadtldashboards(intcim);
        }

        protected void loadtldashboards(int cimnum)
        {   //for termination
            DataSet ds_forTermination_HR = DataHelper.GetHRForTermination(cimnum);
            grd_HR_ForTermination.DataSource = ds_forTermination_HR;
            lblForTermination.Visible = true;
            lblForTermination.Text = Convert.ToString(ds_forTermination_HR.Tables[0].Rows.Count);
            if (ds_forTermination_HR.Tables[0].Rows.Count > 0) {
                pn_fortermination.Visible = true;
                //collapseForTermination.Visible = true;
            }

            //for final warning
            DataSet ds_ForFinalWarning_HR = DataHelper.GetHRForFinalWarning(cimnum);
            grd_HR_ForFinalWarning.DataSource = ds_ForFinalWarning_HR;
            lblForFinalWarning.Visible = true;
            lblForFinalWarning.Text = Convert.ToString(ds_ForFinalWarning_HR.Tables[0].Rows.Count);
            if (ds_ForFinalWarning_HR.Tables[0].Rows.Count > 0) {
                pn_forfinalwarning.Visible = true;
                //collapseForFinalWarning.Visible = true;
            }

            //for overdue follow ups
            DataSet ds_OverdueFollowupHR = DataHelper.Get_HR_OverdueFollowups(cimnum);
            grd_OverdueFollowups.DataSource = ds_OverdueFollowupHR;
            lblNotifOverdueFollowups.Text = Convert.ToString(ds_OverdueFollowupHR.Tables[0].Rows.Count);
            if (ds_OverdueFollowupHR.Tables[0].Rows.Count > 0) {
                pn_followsup.Visible = true;
                //collapseTwo.Visible = true;
            }

            //for overdue sign off
            DataSet ds_OverdueSignoffHR = DataHelper.Get_HR_OverdueSignOffs(cimnum);
            grd_OverdueSignOffs.DataSource = ds_OverdueSignoffHR;
            lblNotifOverdueSignOffs.Text = Convert.ToString(ds_OverdueSignoffHR.Tables[0].Rows.Count);
            if (ds_OverdueSignoffHR.Tables[0].Rows.Count > 0)
            {
                pn_signoffs.Visible = true;
                //collapseThree.Visible = true;
            }

            //WeeklyReview
            DataSet ds_WeeklyReviewTL = DataHelper.Get_TL_WeeklyReview(cimnum);
            grd_WeeklyReviewTL.DataSource = ds_WeeklyReviewTL;
            lblWeekly.Text = Convert.ToString(ds_WeeklyReviewTL.Tables[0].Rows.Count);
            if (ds_WeeklyReviewTL.Tables[0].Rows.Count > 0) 
            {
                pn_weekly.Visible = true;
                //collapseFour.Visible = true;
            }
        }

        protected void grd_HR_ForFinalWarning_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              
         
                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }
        }


        protected void grd_HR_ForTermination_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];
                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
 
            }
        }

        protected void grd_OverdueFollowups_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];
                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }
        }

        protected void grd_OverdueSignOffs_onitemdatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              

                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }
        }

        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}