﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyTeamProfileUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.MyTeamProfileUserCtrl" %>
<%@ Register Src="ProfileMainUserControl.ascx" TagName="ProfileMainUserControl" TagPrefix="uc1" %>
<%@ Register Src="ProfileEducUserControl.ascx" TagName="ProfileEducUserControl" TagPrefix="uc2" %>
<%@ Register Src="ProfileExperienceUserControl.ascx" TagName="ProfileExperienceUserControl"
    TagPrefix="uc3" %>
<%@ Register src="ProfileSkillUserControl.ascx" tagname="ProfileSkillUserControl" tagprefix="uc4" %>
<%@ Register src="ProfileCertUserControl.ascx" tagname="ProfileCertUserControl" tagprefix="uc5" %>
<%@ Register src="ProfileExtraInfoUserCtrl.ascx" tagname="ProfileExtraInfoUserCtrl" tagprefix="uc6" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
        <div>
            &nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard > MY TEAM > <asp:Label runat="server" ID="LblFullName2" CssClass="breadcrumb2ndlevel" Text="GURL" />
            > Profile 
            
        </div>
    </div>
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1" MultiPageID="RadMultiPage1"
        SelectedIndex="0">
        <Tabs>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-file'></span> Profile Summary">
            </telerik:RadTab>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-briefcase'></span> Experience">
            </telerik:RadTab>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-education'></span> Education">
            </telerik:RadTab>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-list-alt'></span> Skills">
            </telerik:RadTab>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-certificate'></span> Certifications">
            </telerik:RadTab>
            <telerik:RadTab Text="<span class='glyphicon glyphicon-plus'></span> Additional Info">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <uc1:ProfileMainUserControl ID="ProfileMainUserControl1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <uc3:ProfileExperienceUserControl ID="ProfileExperienceUserControl1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3" CssClass="RadPageView3">
            <uc2:ProfileEducUserControl ID="ProfileEducUserControl1" runat="server" />
        </telerik:RadPageView>
        
        <telerik:RadPageView runat="server" ID="RadPageView4" CssClass="RadPageView3">
            <uc4:ProfileSkillUserControl ID="ProfileSkillUserControl1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView5" CssClass="RadPageView3">
            <uc5:ProfileCertUserControl ID="ProfileCertUserControl1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView6" CssClass="RadPageView3">
            <uc6:ProfileExtraInfoUserCtrl ID="ProfileExtraInfoUserCtrl1" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</div>
