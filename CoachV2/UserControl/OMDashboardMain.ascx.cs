﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Charting;
using Telerik.Web.UI;
using CoachV2.AppCode;
using System.Web.Services;


namespace CoachV2.UserControl
{
    public partial class OMDashboardMain : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            int intcim = Convert.ToInt32(cim_num);

            //recently audited reviews
            DataSet ds_recentlyaudited = DataHelper.Get_RecentlyAuditedby(intcim);
            grd_recentlyAudited.DataSource = ds_recentlyaudited;
            lblNotifRecentReviews.Text = Convert.ToString(ds_recentlyaudited.Tables[0].Rows.Count);
            if (ds_recentlyaudited.Tables[0].Rows.Count > 0) {
                pn_audited.Visible = true;
              //  collapseRecentlyAudited.Visible = true;
            }

            //overdue follow ups
            DataSet ds_overduefollowups = DataHelper.Get_OverduefollowupsOMDash(intcim);
            grd_OverdueFollowups.DataSource = ds_overduefollowups;
            lblNotifOverdueFollowups.Text = Convert.ToString(ds_overduefollowups.Tables[0].Rows.Count);
            if (ds_overduefollowups.Tables[0].Rows.Count > 0) {
                pn_followups.Visible = true;
               // collapseTwo.Visible = true;
            }

            ////overdue signs off
            DataSet ds_overduesignoff = DataHelper.Get_OverdueSignOffsOMDash(intcim);
            grd_OverdueSignOffs.DataSource = ds_overduesignoff;
            lblNotifOverdueSignOffs.Text = Convert.ToString(ds_overduesignoff.Tables[0].Rows.Count);
            if (ds_overduesignoff.Tables[0].Rows.Count > 0)
            {
                pn_signoffs.Visible = true;
               // collapseThree.Visible = true;
            }

           //weekly review list
            DataSet ds_weeklyreview = DataHelper.Get_OM_Weekly_Review(intcim);
            grd_WeeklyReviewOM.DataSource = ds_weeklyreview;
            lblWeekly.Text = Convert.ToString(ds_weeklyreview.Tables[0].Rows.Count);
            if (ds_weeklyreview.Tables[0].Rows.Count > 0) {
                pn_weekly.Visible = true;
                //collapseFour.Visible = true;
            }

            //////for recently escalated agents - Hide based on UAT 01232019 - janelle.velasquez
            //DataSet ds_RecentlyEscalated_TL = DataHelper.Get_TL_RecentlyEscalated(intcim);
            //grd_TL_RecentEscalated.DataSource = ds_RecentlyEscalated_TL;
            //lblRecentlyEscalated.Text = Convert.ToString(ds_RecentlyEscalated_TL.Tables[0].Rows.Count);
            //if (ds_RecentlyEscalated_TL.Tables[0].Rows.Count > 0)
            //{
            //    pn_escalated.Visible = true;
            //    //  collapseRecentEscalated.Visible = true;
            //}

            //// for recently assigned
            DataSet ds_recentlyassigned = DataHelper.Get_TL_RecentlyAssigned(intcim);
            grd_RecentlyAssignedReviews.DataSource = ds_recentlyassigned;
            lblRecentlyAssigned.Text = Convert.ToString(ds_recentlyassigned.Tables[0].Rows.Count);
            if (ds_recentlyassigned.Tables[0].Rows.Count > 0)
            {
                pn_recentlyassigned.Visible = true;
            }
        }

        protected void grd_recentlyAudited_itemdatabound( object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];
                //string val1 = hLink.Text;
                //int reviewid = Convert.ToInt32(val1);
                //DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;
                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              
               // string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
             
            }
        }

        protected void grd_OverdueFollowups_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                 
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                //string val1 = hLink.Text;
                //int reviewid = Convert.ToInt32(val1);
                //DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                //string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString();

                //string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                //if ((formtype == null) || (formtype == ""))
                //{

                //    formtype = "0";
                //}

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              
                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }

        }
        
        protected void grd_OverdueSignOffs_onitemdatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              
                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }

        }

        protected void grd_recentlyAssigned_itemdatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              

                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }
        }

        protected void grd_TL_RecentEscalated_databound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;
                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
              

                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
            }
        }
    }


}