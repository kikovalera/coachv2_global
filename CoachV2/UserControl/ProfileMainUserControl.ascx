﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileMainUserControl.ascx.cs" Inherits="CoachV2.UserControl.ProfileMainUserControl" %>
<div class="menu-content bg-alt">
        <div class="panel menuheadercustom" id="fake_title" runat="server">
            <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx" ID="RadLinkButton1" ForeColor="White" ToolTip="Edit profile" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit trianglebottom"></i>
            </asp:HyperLink>
            <div>
                &nbsp;<span class="glyphicon glyphicon-user"></span> My Profile
            </div>
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Profile Summary <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="container-fluid" id="Div6" runat="server">
                            <div class="row">
                                <div class="col-sm-4">
                                    <asp:HiddenField ID="FakeURLID" runat="server" />
                                    <div id="profileimageholder">
                                        <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                    </div>
                                </div>                               
                                <div class="form-group col-sm-8" style="padding: 0px">
                                    <form>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span class="namefont">
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span>
                                               <asp:Label ID="LblRole" runat="server" Text="My Role" /></span>
                                        </div>
                                    </div>
                                     <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                         <div class="col-sm-12">
                                             <span>
                                                 <asp:Label ID="LblSite" runat="server" Text="Manila.Ortigas" /></span>
                                         </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Reporting Manager:</label>
                                        <div class="col-sm-8">
                                           <asp:Label ID="LblMySupervisor" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Region:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblRegion" runat="server" Text="My region" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Country:</label>
                                        <div class="col-sm-8">
                                               <asp:Label ID="LblCountry" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px; display: none;">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Department:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblDept" runat="server" Text="My Dept" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Client:</label>
                                        <div class="col-sm-8">
                                              <asp:Label ID="LblClient" runat="server" Text="My client" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Organizational Unit:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblCampaign" runat="server" Text="My campaign" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Personal Information <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group col-sm-12" style="padding:0px">
                            <form>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label class="control-label col-sm-3" for="email">
                                    CIM Number:</label>
                                <div class="col-sm-9">
                                  <asp:Label ID="LblCIMNo" runat="server" Text="My Dept" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label class="control-label col-sm-3" for="Supervisor">
                                    Gender:</label>
                                <div class="col-sm-9">
                                   <asp:Label ID="LblGender" runat="server" Text="My Gender" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label for="CoacheeName" class="control-label col-sm-3">
                                    Date of Birth:</label>
                                <div class="col-sm-9">
                                    <asp:Label ID="LblBday" runat="server" Text="My Bday" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label for="CoacheeName" class="control-label col-sm-3">
                                    Start Date with Transcom:</label>
                                <div class="col-sm-9">
                                   <asp:Label ID="LblTranscomDay" runat="server" Text="My start at Transcom" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label for="CoacheeName" class="control-label col-sm-3">
                                    Primary Language:</label>
                                <div class="col-sm-9">
                                   <asp:Label ID="LblPrimaryLang" runat="server" Text="My primary lang" />
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Contact Information <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group col-sm-12" style="padding:0px">
                            <form>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label class="control-label col-sm-3" for="email">
                                    Mobile Number:</label>
                                <div class="col-sm-9">
                                  <asp:Label ID="LblMobileNo" runat="server" Text="My mobile #" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label class="control-label col-sm-3" for="Supervisor">
                                    Landline:</label>
                                <div class="col-sm-9">
                                    <asp:Label ID="LblLandLineNo" runat="server" Text="My landline #" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label for="CoacheeName" class="control-label col-sm-3">
                                    Transcom Email:</label>
                                <div class="col-sm-9">
                                    <asp:Label ID="LblTranscomEmail" runat="server" Text="My Transcom email" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12" style="padding:0px;margin-bottom:0px">
                                <label for="CoacheeName" class="control-label col-sm-3">
                                    Personal Email:</label>
                                <div class="col-sm-9">
                                    <asp:Label ID="LblPersonalEmail" runat="server" Text="My personal email" />
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>