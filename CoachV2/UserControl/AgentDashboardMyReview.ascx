﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentDashboardMyReview.ascx.cs" Inherits="CoachV2.UserControl.AgentDashboardMyReview" %>
<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="grd_ReviewList_agent">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_ReviewList_agent"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

/* added for static header alignment (francis.valera/08092018) */
.rgDataDiv
   {
        overflow-x: hidden !important;
   }
   
div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="panel menuheadercustom">
    <%--replaced span label with proper case (francis.valera/07182018)--%>
    <div>
        &nbsp;<span class="glyphicon glyphicon-dashboard"></span> My Reviews
        <span class="breadcrumb2ndlevel">
            <asp:Label ID="Label1" runat="server" Text=" "></asp:Label>
        </span>
    </div>
</div>
<br />

<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="collapsed">
        <div class="panel-heading">
            <h4 class="panel-title">Review List
                <span class="label label-default"></span>
                <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>&nbsp;
                <span class="label label-default">
                    <asp:Label ID="label2" runat="server" Visible="true"></asp:Label>
                </span>
            </h4>
        </div>
    </a>
</div>
<div id="collapseOne" class="panel-collapse collapse">
    <div class="panel-body">
        <telerik:RadGrid ID="grd_ReviewList_agent" runat="server" AllowFilteringByColumn="false" GroupPanelPosition="Top" ResolvedRenderMode="Classic" RenderMode="Lightweight" 
                AutoGenerateColumns="false" OnItemDataBound="grd_ReviewList_agent_databound" CssClass="RemoveBorders" BorderStyle="None">

                <%--added for static header alignment (francis.valera/08092018)--%>
                <ClientSettings>
                    <Scrolling UseStaticHeaders="true"/>
                </ClientSettings>

                <MasterTableView CssClass="RadGrid" GridLines="None" AllowPaging="True" PageSize="10" DataKeyNames="Topic" AllowSorting="true">
                <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                            UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket" Display="false" Visible="false" >
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                            FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Name"
                                ShowSortIcon="true" HeaderStyle-Font-Bold="true" SortExpression="Name">
                        </telerik:GridHyperLinkColumn> <%--added SortExpression for Sorting 11/06/18 janelle.velasquez--%>
                    <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                        FilterControlToolTip="Escalationctip" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" DataField="Topic" SortExpression="Topic" ItemStyle-Width="160px" HeaderStyle-Font-Bold="true">
                        <ItemTemplate>
                            <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                            <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                            <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true"
                                    Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                            </telerik:RadToolTip>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                        FilterControlToolTip="Reviewtip" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="StatusField"
                        FilterControlToolTip="Statustip" HeaderStyle-Font-Bold="true">
                    </telerik:GridBoundColumn>    
                        <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching Type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype"  Display="false"   >
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                        </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreatedBy" HeaderText="Created By" UniqueName="CreatedBy"  
                            FilterControlToolTip="CreatedBy" Display="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <%--added for static header alignment (francis.valera/08092018)--%>
                    <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                        <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="True"  />
            </ClientSettings>
        </telerik:RadGrid>
    </div>
</div>