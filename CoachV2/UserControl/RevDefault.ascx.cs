﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using CoachV2.AppCode;
using System.IO;

namespace CoachV2.UserControl
{
    public partial class RevDefault : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Page.ClientQueryString == string.Empty)
            //{
            //    get_review();
            //    LoadDocumentations();
            //}
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] != null)
                {
                    int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 2037;
                    GetMassCoachingDetails(CoachingTicket);
                    LoadDocumentations(CoachingTicket);
                    LoadKPIReview(CoachingTicket);
                    LoadPreviousCoachingNotes(CoachingTicket);
                    LoadPreviousPerformanceResults(CoachingTicket);
                    //DisableElements();


                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        public void LoadPreviousCoachingNotes(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                grd_Coaching_Notes.DataSource = ds;
                grd_Coaching_Notes.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }      
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);
                RadGrid1.DataSource = ds;
                RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        //protected void get_review()
        //{   
        //    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //    int CIMNumber_userlogged = Convert.ToInt32(cim_num);

        //    string coachid = "";
        //    int reviewid;
        //    coachid = Page.Request.QueryString["Coachingticket"];
        //    reviewid = Convert.ToInt32(coachid);
        //    DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

        //    int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
        //    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
        //    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
        //    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));


        //    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
        //    string ImgDefault;

        //    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
        //    FakeURLID.Value = URL;

        //    if (!Directory.Exists(directoryPath))
        //    {
        //        ImgDefault = URL + "/Content/images/no-photo.jpg";
        //    }
        //    else
        //    {

        //        ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
        //    }

        //    RadReviewID.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["ID"]);

        //    if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
        //    else
        //    {
        //        RadCoachingDate.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
        //    }
        //    ProfImage.ImageUrl = ImgDefault;
        //    LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
        //    LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
        //    lblSite.Text = ds.Tables[0].Rows[0]["Sitename"].ToString();
        //    LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
        //    LblCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString();
        //    LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
        //    LblRegion.Text = ds.Tables[0].Rows[0]["Province_State"].ToString();

        //    LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
        //    LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";
        //    if (ds_get_review.Tables[0].Rows[0]["Topicname"] is DBNull) { }
        //    else
        //    {
        //        SessionTopic.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Topicname"]);
        //    }

        //    if (ds_get_review.Tables[0].Rows[0]["SessionName"] is DBNull) { }
        //    else
        //    {
        //        SessionType.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["SessionName"]);
        //    }

        //    RadReviewID.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Id"].ToString());


        //    if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
        //    else
        //    {
        //        RadCoachingDate.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
        //    }

        //    if (ds_get_review.Tables[0].Rows[0]["Description"] is DBNull) { }
        //    else
        //    {
        //        RadDescription.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Description"].ToString());
        //    }

        //    if (ds_get_review.Tables[0].Rows[0]["Strengths"] is DBNull) { }
        //    else
        //    {
        //        RadStrengths.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Strengths"].ToString());
        //    }

        //    if (ds_get_review.Tables[0].Rows[0]["Opportunity"] is DBNull) { }
        //    else { RadOpportunities.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Opportunity"].ToString()); }

        //    if (ds_get_review.Tables[0].Rows[0]["Commitment"] is DBNull) { }
        //    else
        //    {
        //        RadCommitment.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Commitment"].ToString());
        //    }

        //    DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadReviewID.Text));
        //    RadGrid1.DataSource = ds_KPILIST;
        //    RadGrid1.DataBind(); //performance 

        //    DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
        //    grd_Coaching_Notes.DataSource = ds_coachingnotes;
        //    grd_Coaching_Notes.DataBind();

        //    LoadPreviousPerformanceResults(Convert.ToInt32(RadReviewID.Text));

        //    int coacheesigned, coachersigned;
        //    if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
        //    {
        //        coachersigned = 0;
        //    }
        //    else
        //    {
        //        coachersigned = 1;
        //    }
        //    if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
        //    {
        //        coacheesigned = 0;
        //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
        //        RadSave.Visible = true;
        //    }
        //    else
        //    {
        //        coacheesigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
        //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
        //        RadSave.Visible = false;
        //    }

        //    string coachstatus;

        //    string coachersigneddate = ds_get_review.Tables[0].Rows[0]["CoacherSigneddate"].ToString();
        //    string coacheesigneddate = ds_get_review.Tables[0].Rows[0]["CoacheeSigneddate"].ToString();


        //    string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
        //    DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CIMNumber_userlogged);
        //    string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

        //    if (rolevalue == "QA")
        //        saprolefordashboard = "QA";
        //    else if (rolevalue == "HR")
        //        saprolefordashboard = "HR";
        //    else
        //        saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

        //    string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
        //    string retrole = "";
        //    if (saprolefordashboard == "TL")
        //    {
        //        retrole = "TL";
        //    }
        //    else if (saprolefordashboard == "OM")
        //    {
        //        retrole = "OM";
        //    }
        //    else if (saprolefordashboard == "Dir")
        //    {
        //        retrole = "OM";
        //    }
        //    else if (saprolefordashboard == "QA")
        //    {


        //        string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
        //        if ((saprole1 == "OM") || (saprole1 == "Dir"))
        //        {
        //            retrole = "OM";
        //        }
        //        else
        //        {

        //            retrole = "";
        //        }
        //    }
        //    else if (saprolefordashboard == "HR")
        //    {


        //        string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

        //        if ((saprole1 == "OM") || (saprole1 == "Dir"))
        //        {
        //            retrole = "OM";
        //        }
        //        else
        //        {
        //            retrole = "";
        //        }
        //    }
        //    else
        //    {
        //        retrole = "";
        //    }

        //    //if ((coacheesigned == 1) && (coachersigned == 1) && (retrole == "OM"))
        //    //{

        //    //    btn_Audit1.Visible = true;
        //    //}

        //}
        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string CoacherCIM = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                    RadTextBox RadReviewID = this.Parent.FindControl("RadReviewID") as RadTextBox;
                    RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                    RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblCimNumber = this.Parent.FindControl("LblCimNumber") as Label;
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                    Label LblFullName = this.Parent.FindControl("LblFullName") as Label;
                    LblFullName.Text = ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    //RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    //RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblSessionType = this.Parent.FindControl("LblSessionType") as Label;
                    LblSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Label LblSessionTopic = this.Parent.FindControl("LblSessionTopic") as Label;
                    LblSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                    //RadAgenda.Text = ds.Tables[0].Rows[0]["Agenda"].ToString();
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    //RadAgenda.ReadOnly = true;
                    //RadDescription.ReadOnly = true;
                    string Supervisor = ds.Tables[0].Rows[0]["SupervisorID"].ToString();


                    RadStrengths.Text = ds.Tables[0].Rows[0]["Strengths"].ToString();
                    RadOpportunities.Text = ds.Tables[0].Rows[0]["Opportunity"].ToString();

                    if (ds.Tables[0].Rows[0]["ReviewTypeID"].ToString() == "2")
                    {

                        Label Label3 = this.Parent.FindControl("Label3") as Label;
                        Label Label1 = this.Parent.FindControl("Label1") as Label;
                        Label Label2 = this.Parent.FindControl("Label2") as Label;
                        Label3.Text = "My Reviews";
                        Label1.Text = " > QA Reviews ";
                        Label2.Text = " > " + ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    }

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacheeSignedDate"])))
                    {
                        if (Convert.ToInt32(Supervisor) == CIMNumber)
                        {
                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = true;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = true;

                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                        else
                        {

                            RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                            RadCommitment.Enabled = false;
                            RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                            RadSave.Visible = false;

                            Label lab = this.Parent.FindControl("lblstatus") as Label;
                            lab.Visible = true;

                            string stats = "*Needs Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                            lab.Text = stats;
                        }
                    }
                    else
                    {
                        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                        RadCommitment.Enabled = false;
                        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                        RadSave.Visible = false;
                        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();


                        Label lab = this.Parent.FindControl("lblstatus") as Label;
                        lab.Visible = true;

                        string stats = "*Supervisor Sign Off: " + ds.Tables[0].Rows[0]["SupervisorName"].ToString() + " " + ds.Tables[0].Rows[0]["SupervisorSignedOffDate"].ToString();
                        lab.Text = stats;
                    }

                    //if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacherSignedDate"])))
                    //{
                    //    if (Convert.ToInt32(CoacherCIM) == CIMNumber)
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //    else
                    //    {
                    //        RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //        RadCommitment.Enabled = false;
                    //        RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //        RadSave.Visible = false;
                    //        RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    RadTextBox RadCommitment = this.Parent.FindControl("RadCommitment") as RadTextBox;
                    //    RadCommitment.Enabled = false;
                    //    RadButton RadSave = this.Parent.FindControl("RadSave") as RadButton;
                    //    RadSave.Visible = false;
                    //    RadCommitment.Text = ds.Tables[0].Rows[0]["SupervisorComments"].ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        public void LoadDocumentations(int CoachingTicket)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(CoachingTicket), 3);
                //ds = ws.GetUploadedDocuments(79);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    RadGridDocumentation.DataSource = ds;
                    RadGridDocumentation.Rebind();
                }
                else
                {
                    RadGridDocumentation.DataSource = string.Empty;
                    RadGridDocumentation.Rebind();
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        public void LoadPreviousPerformanceResults(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPR.DataSource = ds;
                RadGridPR.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
    }
}