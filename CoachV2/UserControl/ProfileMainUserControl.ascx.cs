﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.IO;

namespace CoachV2.UserControl
{
    public partial class ProfileMainUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cim_num;
            if (!IsPostBack)
            {
                if (Page.Request.QueryString["CIM"] != null)
                {
                    cim_num = Page.Request.QueryString["CIM"];
                    RadLinkButton1.Visible = false;
                }
                else
                {
                    DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();
                }
                

                DataSet ds = DataHelper.GetUserInfoViaCIMNo(cim_num);

                if (Request.QueryString["tab"] != "" && Request.QueryString["tab"] == "profile")
                    fake_title.Visible = false;

                //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);


                DataSet ds2 = DataHelper.GetUserInfo(ds.Tables[0].Rows[0]["Email"].ToString());
                DataSet ds4 = DataHelper.GetUserDetails(Convert.ToInt32(cim_num));
                

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + cim_num.ToString()));
                string ImgDefault;

                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = "../Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = "../Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                ProfImage.ImageUrl = ImgDefault;
                LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
                LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();

                LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
                LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
                LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
                LblCIMNo.Text = cim_num;
                LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();

                LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
                LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";

                DateTime DateOfBirth = DateTime.ParseExact(ds.Tables[0].Rows[0]["Birthday"].ToString(),
                                           "yyyyMMdd",
                                           CultureInfo.InvariantCulture,
                                           DateTimeStyles.None);
                DateTime DateStartTranscom = DateTime.ParseExact(ds.Tables[0].Rows[0]["Start_Date"].ToString(),
                                            "yyyyMMdd",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None);

                LblGender.Text = Convert.ToInt32(ds.Tables[0].Rows[0]["Gender"]) == 1 ? "Male" : "Female";
                LblBday.Text = String.Format("{0:MMMM dd, yyyy}", Convert.ToDateTime(DateOfBirth));
                LblTranscomDay.Text = String.Format("{0:MMMM dd, yyyy}", Convert.ToDateTime(DateStartTranscom));

                LblMobileNo.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["MobileNo"].ToString() : ds.Tables[0].Rows[0]["Mobile_Number"].ToString();
                LblLandLineNo.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["LandLineNo"].ToString() :  ds.Tables[0].Rows[0]["Phone_Number"].ToString();
                LblTranscomEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString().ToLower();
                LblPersonalEmail.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["PersonalEmail"].ToString().ToLower() : ""; ;

                LblPrimaryLang.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["PrimaryLanguage"].ToString() : "";

                LblSite.Text = ds4.Tables[0].Rows[0]["Site"].ToString();

                //DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                //string cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

                DataSet ds3 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

                if (ds3.Tables[0].Rows.Count == 0)
                    RadLinkButton1.Visible = false;
            }
        }
    }
}