﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Data.OleDb;
using System.IO;

namespace CoachV2.UserControl
{
    public partial class AdminMain : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {           
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            int int_cim = Convert.ToInt32(cim_num);
            
            DataSet ds_drivers = DataHelper.GetDriversLookup();
            grd_Drivers.DataSource = ds_drivers;
            btnKPI.BackColor = System.Drawing.Color.DarkGray;

            DataSet ds_session = DataHelper.GetSessionsLookup();
            grd_session.DataSource = ds_session;
            grd_session.DataBind();
            
            DataSet ds_account = DataHelper.GetAccountsLookup();
            grd_Account.DataSource = ds_account;
            grd_Account.DataBind();

            DataSet ds_supervisor = DataHelper.GetSupervisorLookup();
            grd_Supervisor.DataSource = ds_supervisor;
            grd_Supervisor.DataBind();

            DataSet ds_KPIList = DataHelper.GetKPIIList_admin();
            grd_KPILIst.DataSource = ds_KPIList;
            grd_KPILIst.DataBind();

            DataSet ds_KPIList2 = DataHelper.GetKPIIList_admin();
            RadGrid1.DataSource = ds_KPIList2;
            lblForKpiCreated.Text = "KPIs Created by Admin : " + Convert.ToString(ds_KPIList2.Tables[0].Rows.Count);

            if (!IsPostBack)
            { 
                DataSet ds_userselect = DataHelper.GetAllUserRoles(int_cim);
                var mark_13 = "0";
                var mark_14 = "0";
                var mark_15 = "0";
                //for kpi addition

                RadGridUpload.DataSource = string.Empty;
                RadGridUpload.Rebind();
                LoadUploadedItems();

                if (ds_userselect.Tables[0].Rows.Count > 0) //13,14,15
                {
                    for (int ctr = 0; ctr < ds_userselect.Tables[0].Rows.Count; ctr++)
                    {


                        DataSet ds_userselect13 = DataHelper.GetUserRolesAssigned(int_cim, 13);
                        if (ds_userselect13.Tables[0].Rows.Count > 0)
                        {
                            mark_13 = "1";
                        }
                        DataSet ds_userselect14 = DataHelper.GetUserRolesAssigned(int_cim, 14);
                        if (ds_userselect14.Tables[0].Rows.Count > 0)
                        {
                            mark_14 = "1";
                        }
                        DataSet ds_userselect15 = DataHelper.GetUserRolesAssigned(int_cim, 15);
                        if (ds_userselect15.Tables[0].Rows.Count > 0)
                        {
                            mark_15 = "1";
                        }

                    }

                    if ((mark_13 == "1") && (mark_14 == "0") && (mark_15 == "0")) //13 only
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;

                        Pn_KPI.Visible = false;
                        ic_kpi.Visible = true;

                        Pn_Lookup.Visible = true;

                    }
                    else if ((mark_13 == "0") && (mark_14 == "1") && (mark_15 == "0")) //14only
                    {
                        Pn_Admin.Visible = false;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = false;
                    }
                    else if ((mark_13 == "0") && (mark_14 == "0") && (mark_15 == "1")) //15only
                    {
                        Pn_Admin.Visible = true;
                        Pn_KPI.Visible = false;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "1") && (mark_15 == "1")) //13,14,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;

                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "1") && (mark_15 == "0")) //13,14
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "0") && (mark_15 == "1")) //13,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = false;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "0") && (mark_14 == "1") && (mark_15 == "1")) //14,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }

                }
                else
                {


                }

            }
        }

        protected void display_access()
        {


            int role_id = 0;
            int cim_val = Convert.ToInt32(txtCimnumber.Text);

            DataSet ds_userselect = DataHelper.GetAllUserRoles(Convert.ToInt32(txtCimnumber.Text));
            try
            {

                if (ds_userselect.Tables[0].Rows.Count > 0) //ds_userroles.Tables[0].Rows.Count > 0)
                {

                    //cb_Login : 1
                    //cb_AddReview : 2
                    //cb_perfsummary : 3
                    //cb_reviewLogs : 4
                    //cb_profile : 5
                    //cb_addnewreview : 6
                    //cb_addMassReview : 7
                    //cb_remotecoach : 8
                    //cb_searchReviews : 9
                    //cb_ReportBuilder : 10
                    //cb_BuiltInReports : 11
                    //cb_SavedReports : 12
                    //cb_Users : 13
                    //cb_KPI : 14
                    //cb_SearchUsers : 15
                    //cb_EditProfile : 16
                    //cb_qareview : 18 
                    for (int ctr = 0; ctr < ds_userselect.Tables[0].Rows.Count; ctr++)
                    {
                        string user_roleid = ds_userselect.Tables[0].Rows[ctr][1].ToString();

                        if (user_roleid == "1")
                        {
                            cb_Login.Checked = true;
                        }

                        if (user_roleid == "2")
                        {
                            cb_AddReview.Checked = true;
                        }

                        if (user_roleid == "3")
                        {
                            cb_perfsummary.Checked = true;
                        }

                        if (user_roleid == "4")
                        {
                            cb_reviewLogs.Checked = true;
                        }

                        if (user_roleid == "5")
                        {
                            cb_profile.Checked = true;
                        }

                        if (user_roleid == "6")
                        {
                            cb_addnewreview.Checked = true;
                        }

                        if (user_roleid == "7")
                        {
                            cb_addMassReview.Checked = true;
                        }

                        if (user_roleid == "8")
                        {
                            cb_remotecoach.Checked = true;
                        }

                        if (user_roleid == "9")
                        {
                            cb_searchReviews.Checked = true;
                        }

                        if (user_roleid == "10")
                        {
                            cb_ReportBuilder.Checked = true;
                        }

                        if (user_roleid == "11")
                        {
                            cb_BuiltInReports.Checked = true;
                        }


                        if (user_roleid == "12")
                        {
                            cb_SavedReports.Checked = true;
                        }

                        if (user_roleid == "13")
                        {
                            cb_Users.Checked = true;
                        }

                        if (user_roleid == "14")
                        {
                            cb_KPI.Checked = true;
                        }

                        if (user_roleid == "15")
                        {
                            cb_SearchUsers.Checked = true;
                        }

                        if (user_roleid == "16")
                        {
                            cb_EditProfile.Checked = true;
                        }

                        if (user_roleid == "17")
                        {
                            cb_notifications.Checked = true;
                        }

                        if (user_roleid == "18")
                        {
                            cb_qareview.Checked = true;
                        }
                    }
                }
                else
                {
                    string ModalLabel = "Please enter valid Cim!";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);



                }
            }
            catch(Exception ex)
            {
                string ModalLabel = ex.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            if (txtCimnumber.Text != string.Empty)
            {
                display_access();

                string ModalLabel = "User Role for " + txtCimnumber.Text + " displayed!"; 
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                //ClientScript.RegisterStartupScript("LaunchServerSide", "<script> $(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });<script> ");
                //string message = "User Role for " + txtCimnumber.Text + " displayed!";
               // ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
               
            }

            else
            {
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('Please enter Valid Cim Number.');", true);

                string ModalLabel = "Please enter Valid Cim Number.";
                string ModalHeader = "Error Message";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        protected void btn_submit_OnClick(object sender, EventArgs e)
        {

            if (txtCimnumber.Text != string.Empty)
            {

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string gmailaccount = ds.Tables[0].Rows[0][13].ToString().ToLower();


                int role_id = 0;
                int cim_val = Convert.ToInt32(txtCimnumber.Text);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                DataSet ds_userselect = DataHelper.GetAllUserRoles(cim_val);

                //cannot edit own profile,
                if (Convert.ToString(txtCimnumber.Text) == cim_num)
                {
                    string ModalLabel = "Editting own access is not allowed!";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                    //string message = "Editting own access is not allowed!";
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    //    "alert(\"" + message + "\");", true);
                }
                else if (Convert.ToString(txtCimnumber.Text) != cim_num)
                {
                    try
                    {
                        if (ds_userselect.Tables[0].Rows.Count > 0)
                        {

                            if (cb_Login.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);

                                DataHelper.UpdateUserToInactive(Convert.ToInt32(txtCimnumber.Text)); //set user to inactive
                                DataHelper.RemoveUserRolesAssigned(cim_val, 1); //remove for log in
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 1);

                                try
                                {

                                    if (ds_userselect1.Tables[0].Rows.Count == 0)
                                    {
                                        role_id = 1;
                                        int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                        DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                                        DataHelper.UpdateUserToActive(Convert.ToInt32(txtCimnumber.Text)); //set user to active


                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ModalLabel = ex.ToString();
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }



                            }


                            if (cb_AddReview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 2);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 2);

                                try
                                {

                                    if (ds_userselect1.Tables[0].Rows.Count == 0)
                                    {
                                        role_id = 2;
                                        int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                        DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ModalLabel = ex.ToString();
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }

                            }

                            if (cb_perfsummary.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 3);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 3);

                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 3;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);

                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                                }


                            }

                            if (cb_reviewLogs.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 4);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 4);

                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 4;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_profile.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 5);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 5);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 5;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_addnewreview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 6);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 6);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 6;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_addMassReview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 7);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 7);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 7;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }


                            if (cb_remotecoach.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 8);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 8);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 8;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }


                            if (cb_searchReviews.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 9);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 9);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 9;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_ReportBuilder.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 10);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 10);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 10;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_BuiltInReports.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 11);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 11);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 11;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }


                            if (cb_SavedReports.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 12);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 12);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 12;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }

                            if (cb_Users.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 13);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 13);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 13;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_KPI.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 14);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 14);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 14;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_SearchUsers.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 15);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 15);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 15;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_EditProfile.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 16);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 16);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 16;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }
                            //for notifications
                            if (cb_notifications.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 17);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 17);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 17;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_qareview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 18);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 18);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 18;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                        }
                        else //for first time role assigning
                        {
                            if (cb_Login.Checked == true)
                            {
                                role_id = 1;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);


                            }

                            if (cb_AddReview.Checked == true)
                            {
                                role_id = 2;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_perfsummary.Checked == true)
                            {
                                role_id = 3;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_reviewLogs.Checked == true)
                            {
                                role_id = 4;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_profile.Checked == true)
                            {
                                role_id = 5;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_addnewreview.Checked == true)
                            {
                                role_id = 6;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_addMassReview.Checked == true)
                            {
                                role_id = 7;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_remotecoach.Checked == true)
                            {
                                role_id = 8;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_searchReviews.Checked == true)
                            {
                                role_id = 9;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_ReportBuilder.Checked == true)
                            {
                                role_id = 10;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_BuiltInReports.Checked == true)
                            {
                                role_id = 11;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }


                            if (cb_SavedReports.Checked == true)
                            {
                                role_id = 12;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_Users.Checked == true)
                            {
                                role_id = 13;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_KPI.Checked == true)
                            {
                                role_id = 14;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_SearchUsers.Checked == true)
                            {
                                role_id = 15;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_EditProfile.Checked == true)
                            {
                                role_id = 16;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_notifications.Checked == true)
                            {
                                role_id = 17;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_qareview.Checked == true)
                            {
                                role_id = 18;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }
                        }
                        display_access();
                        string ModalLabel1 = "User Role for " + cim_val + " saved!";
                        string ModalHeader1 = "Success Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader1 + "','" + ModalLabel1 + "'); });", true);

                        //string message = "User Role for " + cim_val + " saved!";
                        //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                        //    "alert(\"" + message + "\");", true);
                    }
                    catch (Exception ex)
                    {
                        string ModalLabel = ex.ToString();
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }
            else
            {

                string ModalLabel = "Please enter Valid Cim Number.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('Please enter Valid Cim Number.');", true);
            }
        }

        protected void btn_reset_OnClick(object sender, EventArgs e)
        {
            txtCimnumber.Text = "";
            cb_Login.Checked = false;
            cb_AddReview.Checked = false;
            cb_perfsummary.Checked = false;
            cb_reviewLogs.Checked = false;
            cb_profile.Checked = false;
            cb_addnewreview.Checked = false;
            cb_addMassReview.Checked = false;
            cb_remotecoach.Checked = false;
            cb_searchReviews.Checked = false;
            cb_ReportBuilder.Checked = false;
            cb_BuiltInReports.Checked = false;
            cb_SavedReports.Checked = false;
            cb_Users.Checked = false;
            cb_KPI.Checked = false;
            cb_SearchUsers.Checked = false;
            cb_EditProfile.Checked = false;
            cb_qareview.Checked = false;

        }

        protected void btn_Button1(object sender, EventArgs e)
        { 

            if ((cb_kpiname.SelectedValue != "") && (cb_account.SelectedValue != ""))
            {
                try
                {
                    DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = ds.Tables[0].Rows[0][2].ToString();
                    int int_cim = Convert.ToInt32(cim_num);
                    
                    DataSet ds_kpiidlast = DataHelper.GetKPIIDfromScorecard(Convert.ToInt32(cb_kpiname.SelectedValue));
                    //GetKPIFromScorecardList
                    if (ds_kpiidlast.Tables[0].Rows.Count > 0)
                    {

                        
                        int kpiid = Convert.ToInt32(ds_kpiidlast.Tables[0].Rows[0][0]);
                        string KPItrim1 = Convert.ToString(ds_kpiidlast.Tables[0].Rows[0]["KPIName"]);

                        //DataSet ds_ifexist = DataHelper.GetKPIIDAddedKPI(Convert.ToInt32(cb_kpiname.SelectedValue), Convert.ToInt32(cb_account.SelectedValue));
                        DataSet ds_ifexist = DataHelper.GetAssignedKPI(Convert.ToInt32(cb_kpiname.SelectedValue), Convert.ToInt32(cb_account.SelectedValue), Convert.ToInt32(cb_valuetype.SelectedValue), Convert.ToInt32(cb_linkto.SelectedValue));
                        if (ds_ifexist.Tables[0].Rows.Count > 0)
                        {
                            //string myStringVariable = "KPI : " + cb_kpiname.Text + " already Assigned to : " + cb_account.Text;
                            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            //    "alert('" + myStringVariable + "');", true);

                            string ModalLabel = "KPI : " + cb_kpiname.Text + " already Assigned to : " + cb_account.Text;
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          

                            RadGrid1.Focus();
                        }
                        else {
                            //                kpiid = kpiid + 1;
                            //int kpiid,   string kpi_name,                        string kpi_desc,                        int valuetype,                                  int linkto,                       string createdby, int hidefromlist, int accountid,                                    DateTime createdon, int driverid
                            DataHelper.InsertNewKPI(Convert.ToInt32(kpiid), Convert.ToString(KPItrim1), Convert.ToString(cb_kpiname.Text), Convert.ToInt32(cb_valuetype.SelectedValue), Convert.ToInt32(cb_linkto.SelectedValue), cim_num, 0, Convert.ToInt32(cb_account.SelectedValue), DateTime.Now, 1);
                            Response.AppendHeader("Refresh", "0.1");
                            //string myStringVariable = "KPI : " + cb_kpiname.Text + " Assigned to : " + cb_account.Text;
                            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                            //    "alert('" + myStringVariable + "');", true);

                            string ModalLabel = "KPI : " + cb_kpiname.Text + " Assigned to : " + cb_account.Text;
                            string ModalHeader = "Success Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          
                            RadGrid1.Focus();
                        }
                    
                    }

                }
                catch (Exception ex)
                {
                    string ModalLabel = ex.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //string myStringVariable = ex.ToString();
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    //    "alert('" + myStringVariable + "');", true);
                }
            }
            else
            {
                //string myStringVariable = "Please select KPI Name / Account!";
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('" + myStringVariable + "');", true);
                string ModalLabel = "Please select KPI Name / Account!";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          

            }
        }
        
        protected void GetAccount(int CIMNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                cb_account.DataSource = ds;
                cb_account.DataTextField = "Account";
                cb_account.DataValueField = "AccountID";
                cb_account.DataBind();

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //// ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('" + myStringVariable + "');", true);
                string ModalLabel = ex.ToString(); 
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          
            }

        }

        protected void loadkpidetails()
        {

            DataSet ds_kpifromscorecard = DataHelper.GetKPIFromScorecardList();//.GetKPIFromScorecard();
            cb_kpiname.DataSource = ds_kpifromscorecard;
            cb_kpiname.DataBind();

            DataSet ds_link_to = DataHelper.GetKPILinkTo();
            cb_linkto.DataSource = ds_link_to;
            cb_linkto.DataBind();

            DataSet ds_valueto = DataHelper.GetValueto();
            cb_valuetype.DataSource = ds_valueto;
            cb_valuetype.DataBind();
        }

        //OnUpdateCommand
        protected void radgrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            //if ((e.Item.ItemType == GridItemType.Item) || (e.Item is GridEditableItem))
            //{
            //    try
            //    {
             if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
                {
                 try
                 {
                     DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                     string cim_num = ds.Tables[0].Rows[0][2].ToString();
                    
                    GridEditFormItem updateItem = (GridEditFormItem)e.Item;
                    int rowindex = updateItem.ItemIndex;
                    Label kpiid = (Label)updateItem.FindControl("txtRow");
                    RadComboBox RadKPI = (RadComboBox)updateItem.FindControl("cb_kpiname");
                    RadComboBox Radvalue = (RadComboBox)updateItem.FindControl("cb_valuetype");
                    RadComboBox Radlinkto = (RadComboBox)updateItem.FindControl("cb_linkto");
                    RadComboBox RadAccount = (RadComboBox)updateItem.FindControl("cb_account");
                    string kpiname_text = RadKPI.SelectedItem.Text;

                    DataSet ds_ifexist = DataHelper.GetAssignedKPI(Convert.ToInt32(cb_kpiname.SelectedValue), Convert.ToInt32(cb_account.SelectedValue), Convert.ToInt32(cb_valuetype.SelectedValue), Convert.ToInt32(cb_linkto.SelectedValue));
                    if (ds_ifexist.Tables[0].Rows.Count > 0)
                    {
                        string ModalLabel = "KPI : " + cb_kpiname.Text + " already Assigned to : " + cb_account.Text;
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                    else
                    { 

                    DataHelper.UpdateKPIAdmin(Convert.ToInt32(kpiid.Text), Convert.ToString(RadKPI.SelectedValue), Convert.ToString(kpiname_text), Convert.ToInt32(Radvalue.SelectedValue), Convert.ToInt32(Radlinkto.SelectedValue), Convert.ToString(cim_num), Convert.ToInt32(RadAccount.SelectedValue), DateTime.Now);
                    string ModalLabel = "Update on KPI : " + kpiname_text + " Sucessful!";
                    string ModalHeader = "Success Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //string myStringVariable = "Update on KPI : " + kpiname_text + " Sucessful!";
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    //       "alert('" + myStringVariable + "');", true);
                     RadGrid1.Focus();
                    }
                }
                catch (Exception ex)
                {
                    //string myStringVariable = ex.ToString();
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    //       "alert('" + myStringVariable + "');", true);

                    string ModalLabel = ex.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          
                }
                 RadGrid1.MasterTableView.ClearEditItems();
                 Response.AppendHeader("Refresh", "0.1");
                 RadGrid1.Focus();
            }
             
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
              

                DataHelper.DeleteKPIAdmin(Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Row")), Convert.ToString(cim_num), DateTime.Now);
                //string myStringVariable = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!"; 
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //           "alert('" + myStringVariable + "');", true);

                string ModalLabel = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!"; 
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          
                Response.AppendHeader("Refresh", "0.1");
                RadGrid1.Focus();
            
            }
            catch (Exception ex)
            {
                string ModalLabel = ex.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
          
                RadGrid1.Focus();
            }
        }
     
        protected void radgrid1_databound(object sender, GridItemEventArgs e)
        {
          
                if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;

  

                    //RadComboBox combo = (RadComboBox)edititem.FindControl("cb_kpiname");
                    //RadComboBoxItem selectedItem = new RadComboBoxItem();
                    //selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPI Name"].ToString();
                    //selectedItem.Value = ((DataRowView)e.Item.DataItem)["KPI_ID"].ToString();
                    //combo.Items.Add(selectedItem);
                    //selectedItem.DataBind();
                    //Session["KPI Name"] = selectedItem.Value;

                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("cb_kpiname");
                    DataSet ds = new DataSet();
                    //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
                    ds = DataHelper.GetKPIFromScorecardList(); //GetKPIFromScorecard();
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    //comboBrand.SelectedValue = selectedItem.Value;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("cb_kpiname");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPI Name"].ToString();
                    selectedItem.Value = ((DataRowView)e.Item.DataItem)["KPI_ID"].ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI Name"] = selectedItem.Value;
                    combo.SelectedValue = selectedItem.Value;

                    RadComboBox combo1 = (RadComboBox)edititem.FindControl("cb_valuetype");
                    RadComboBoxItem selectedItem1 = new RadComboBoxItem();
                    selectedItem1.Text = ((DataRowView)e.Item.DataItem)["Value Type"].ToString();
                    selectedItem1.Value = ((DataRowView)e.Item.DataItem)["valuetype"].ToString();
                    combo1.Items.Add(selectedItem1);
                    selectedItem1.DataBind();
                    Session["Value Type"] = selectedItem.Value;
                    RadComboBox comboBrand1 = (RadComboBox)edititem.FindControl("cb_valuetype");
                    DataSet ds1 = new DataSet();
                    //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
                    ds1 = DataHelper.GetValueto();
                    comboBrand1.DataSource = ds1;
                    comboBrand1.DataBind();
                    comboBrand1.SelectedValue = selectedItem1.Value;

                    RadComboBox combo2 = (RadComboBox)edititem.FindControl("cb_linkto");
                    RadComboBoxItem selectedItem2 = new RadComboBoxItem();
                    selectedItem2.Text = ((DataRowView)e.Item.DataItem)["Assign Type"].ToString();
                    selectedItem2.Value = ((DataRowView)e.Item.DataItem)["linkto"].ToString();
                    combo2.Items.Add(selectedItem2);
                    selectedItem2.DataBind();
                    Session["Assign Type"] = selectedItem2.Value;
                    RadComboBox comboBrand2 = (RadComboBox)edititem.FindControl("cb_linkto");
                    DataSet ds2 = new DataSet();
                    ds2 = DataHelper.GetKPILinkTo();
                    comboBrand2.DataSource = ds2;
                    comboBrand2.DataBind();
                    comboBrand2.SelectedValue = selectedItem2.Value;

                    RadComboBox combo3 = (RadComboBox)edititem.FindControl("cb_account");
                    RadComboBoxItem selectedItem3 = new RadComboBoxItem();
                    selectedItem3.Text = ((DataRowView)e.Item.DataItem)["Assigned to"].ToString();
                    selectedItem3.Value = ((DataRowView)e.Item.DataItem)["ACCOUNTID"].ToString();
                    combo3.Items.Add(selectedItem3);
                    selectedItem3.DataBind();
                    Session["Assigned to"] = selectedItem.Value;
                    RadComboBox comboBrand3 = (RadComboBox)edititem.FindControl("cb_account");
                    DataSet ds3 = new DataSet();
                    ds3 = DataHelper.GetAccountsforKPI();
                    comboBrand3.DataSource = ds3;
                    comboBrand3.DataBind();
                    comboBrand3.SelectedValue = selectedItem3.Value;



                }
               
        }

        protected void btnKPI_onclick(object sender, EventArgs e) {
            btnKPI.BackColor = System.Drawing.Color.DarkGray;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSession.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.Empty;
            grd_Account.Visible = false;
            grd_Supervisor.Visible = false;
            grd_session.Visible = false;
            grd_Drivers.Visible = true;    
            grd_Drivers.Focus();
           
        }

        protected void btnSession_onclick(object sender, EventArgs e)
        {   btnSession.BackColor = System.Drawing.Color.DarkGray;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.Empty;

            grd_Account.Visible = false;
            grd_Supervisor.Visible = false;
            grd_session.Visible = true;
            grd_Drivers.Visible = false;    
            grd_session.Focus();
        }

        protected void btnAcct_onclick(object sender, EventArgs e)
        {   btnSession.BackColor = System.Drawing.Color.Empty;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.DarkGray;
            btnSup.BackColor = System.Drawing.Color.Empty;
            grd_session.Visible = false;
            grd_Supervisor.Visible = false;
            grd_Account.Visible = true;
            grd_Drivers.Visible = false;
            grd_Account.Focus();
        }

        protected void btnSup_onclick(object sender, EventArgs e)
        {
            btnSession.BackColor = System.Drawing.Color.Empty;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.DarkGray;
            grd_Account.Visible = false;
            grd_Supervisor.Visible = true;
            grd_session.Visible = false;
            grd_Drivers.Visible = false;
            grd_Supervisor.Focus();
        }

        protected void RadGridUpload_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }

        protected void btnDummy_Click(object sender, EventArgs e)
        {
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            foreach (UploadedFile f in RadUpload.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = @"C:\Documentation\";
                string path = f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + " - " + cim_num + f.GetExtension();
                LabelPath.Text = f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + " - " + cim_num + f.GetExtension();
                //f.SaveAs(targetFolder + path);
                f.SaveAs(Path.Combine(targetFolder, path));
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                LabelHost.Text = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                //ws.InsertUploadedAgentPerformance(f.GetName(), Convert.ToInt32(cim_num), host + "/Documentation/" + path);
                HiddenFileName.Value = f.GetName();
                SaveFileToDatabase(targetFolder + LabelPath.Text);

                LoadUploadedItems();

                string ModalLabel = "Successfully uploaded file.";
                string ModalHeader = "Success.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

             }
           

        }

        public void LoadUploadedItems()
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetUploadedDocuments(0, 4);

            RadGridUpload.DataSource = ds;
            RadGridUpload.Rebind();
        }

        private void SaveFileToDatabase(string fileName)
        {
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", fileName);
            DataSet data = new DataSet();
            foreach (var sheetName in GetExcelSheetNames(excelConnString))
            {
                using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                {
                    using (OleDbCommand cmd = new OleDbCommand("Select [CIM],[NAME],[LOB],[DATE],[KPI],[TARGET],[SCORE],[SUPERVISOR CIM] from [" + sheetName + "]", excelConnection))
                    {
                        excelConnection.Open();
                        if (CheckIfColumnsExists(excelConnection, sheetName) == true)
                        {
                            if (HiddenUploadID.Value == "0")
                            {
                                DataAccess ws = new DataAccess();
                                int UploadID = ws.InsertUploadedAgentPerformance(HiddenFileName.Value, Convert.ToInt32(cim_num), LabelHost.Text + "/Documentation/" + LabelPath.Text);
                                HiddenUploadID.Value = UploadID.ToString();
                            }

                            using (OleDbDataReader dReader = cmd.ExecuteReader())
                            {
                                while (dReader.Read())
                                {
                                    string cimno = dReader["CIM"].ToString();
                                    string name = dReader["NAME"].ToString();
                                    string lob = dReader["LOB"].ToString();
                                    string date = dReader["DATE"].ToString();
                                    string kpi = dReader["KPI"].ToString();
                                    string target = dReader["TARGET"].ToString();
                                    string score = dReader["SCORE"].ToString();
                                    string scim = dReader["SUPERVISOR CIM"].ToString();

                                    DateTime dDate;
                                    if (DateTime.TryParse(date, out dDate))
                                    {
                                        String.Format("{0:d/MM/yyyy}", dDate);
                                        DataAccess ws = new DataAccess();
                                        ws.UploadAgentPerformance(cimno, name, lob, date, kpi, target, score, scim);
                                    }
                                    else
                                    {
                                        string ModalLabel = "Date format is invalid.";
                                        string ModalHeader = "Error.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                                        break;
                                    }
                                }

                            }
                        }
                        else
                        {
                            string ModalLabel = "Please check the excel columns.";
                            string ModalHeader = "Error.";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            break;
                        }
                    }
                }
            }
        }

        public void ExcelColumns()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ColumnName", typeof(string));
            dt2.Columns.Add("Exists", typeof(int));
            ViewState["ExcelColumns"] = dt2;
        }

        protected bool CheckIfColumnsExists(OleDbConnection excelConnection, string SheetName)
        {
            //ExcelColumns();
            BIColumns();
            bool a = false;
            DataTable dt = (DataTable)ViewState["BIColumns"];
            DataTable myColumns = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, SheetName, null });
            foreach (DataRow dtRow in myColumns.Rows)
            {
                if (dtRow["COLUMN_NAME"].ToString() == "CIM")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "CIM")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "Name")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "NAME")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "LOB")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "LOB")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "Date")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "DATE")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "KPI")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "KPI")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "Target")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "TARGET")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "Score")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "SCORE")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString() == "Supervisor CIM")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString() == "SUPERVISOR CIM")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
            }
            int c = 0;
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                
                DataRow dr = dt.Rows[i];
                if (dr["Exists"].ToString() == DBNull.Value.ToString())
                {
                    a = false;
                }
                else
                {
                    c = c+1;
                }

            }
            int count = dt.Rows.Count;
            if (c == count)
            {
                a = true;
            }
            return a;

        }

        public void BIColumns()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns["ID"].AutoIncrement = true;
            dt.Columns["ID"].AutoIncrementSeed = 1;
            dt.Columns["ID"].AutoIncrementStep = 1;
            dt.Columns.Add("ColumnName", typeof(string));
            dt.Columns.Add("Exists", typeof(int));
            ViewState["BIColumns"] = dt;

            DataTable dt2;
            dt2 = (DataTable)ViewState["BIColumns"];
            DataRow dr;
            dr = dt.NewRow();
            dr["ColumnName"] = "CIM";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "NAME";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "LOB";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "DATE";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "KPI";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "TARGET";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "SCORE";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "SUPERVISOR CIM";
            dt2.Rows.Add(dr);
        }

        static string[] GetExcelSheetNames(string connectionString)
        {
            OleDbConnection con = null;
            DataTable dt = null;
            con = new OleDbConnection(connectionString);
            con.Open();
            dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (dt == null)
            {
                return null;
            }

            String[] excelSheetNames = new String[dt.Rows.Count];
            int i = 0;

            foreach (DataRow row in dt.Rows)
            {
                excelSheetNames[i] = row["TABLE_NAME"].ToString();
                i++;
            }

            return excelSheetNames;
        }
    }
}