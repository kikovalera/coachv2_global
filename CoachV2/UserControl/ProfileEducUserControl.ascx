﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEducUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileEducUserControl" %>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom" id="fake_title" runat="server">
        <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx?tab=education" ID="RadLinkButton1"
            ForeColor="White" ToolTip="Edit education" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit"></i>
        </asp:HyperLink>
        <div>
            &nbsp;<span class="glyphicon glyphicon-education"></span> MY Education
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Educational Background <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <telerik:RadGrid ID="EducGrid" runat="server" OnPreRender="EducGrid_PreRender" AllowAutomaticInserts="False"
                        OnNeedDataSource="EducGrid_NeedDataSource" CssClass="RemoveBorders" GridLines="None" BorderStyle="None" ExportSettings-ExportOnlyData="true"  > 
                        
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>

                        <MasterTableView CommandItemDisplay="Top" AllowPaging="True"
                            PageSize="20" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="EducBgId">
                            <CommandItemSettings ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="School" HeaderText="School" DataField="School"
                                    HeaderStyle-Font-Bold="true" />
                                <telerik:GridBoundColumn UniqueName="Level" HeaderText="Level" DataField="Description"
                                    HeaderStyle-Font-Bold="true" />
                                <telerik:GridBoundColumn UniqueName="Course" HeaderText="Course" DataField="Course"
                                    HeaderStyle-Font-Bold="true" />
                                <telerik:GridDateTimeColumn UniqueName="From" HeaderText="From" DataField="StartDate"
                                    DataType="System.DateTime" DataFormatString="{0:y}" HeaderStyle-Font-Bold="true" />
                                <telerik:GridDateTimeColumn UniqueName="End" HeaderText="To" DataField="EndDate"
                                    DataType="System.DateTime" DataFormatString="{0:y}" HeaderStyle-Font-Bold="true" />
                                <telerik:GridBoundColumn UniqueName="Honors" HeaderText="Honors" DataField="Honors"
                                    HeaderStyle-Font-Bold="true" />
                                <telerik:GridBoundColumn UniqueName="Graduated" HeaderText="Graduated" DataField="Graduated"
                                    HeaderStyle-Font-Bold="true" />
                                    
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>

                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
</div>
