﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class AboutPageSidebar : System.Web.UI.UserControl
    {
        AboutMain AboutUC;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckReferencesLink();
        }

        protected void btn_userguide_OnClick(object sender, EventArgs e)
        {
            var about = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_coachv2");
            about.Visible = false;

            var guide = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_userguide");
            guide.Visible = true;

            var references = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_references");
            references.Visible = false;
        }

        protected void btn_coachv2_OnClick(object sender, EventArgs e)
        {
            var about = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_coachv2"); 
            about.Visible = true;

            var guide = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_userguide");
            guide.Visible = false;

            var references = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_references");
            references.Visible = false;
        }
        
        private void CheckReferencesLink()
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                btn_ref2.Visible = true;
            }
        }

        protected void btn_ref2_OnClick(object sender, EventArgs e)
        {
            var about = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_coachv2");
            about.Visible = false;

            var guide = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_userguide");
            guide.Visible = false;

            var references = this.Page.Master.FindControl("ContentPlaceHolderMain").FindControl("Main").FindControl("pn_references");
            references.Visible = true;
        }
    }
}