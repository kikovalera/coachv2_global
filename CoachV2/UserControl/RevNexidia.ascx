﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevNexidia.ascx.cs" Inherits="CoachV2.UserControl.RevNexidia" %>

 <div class="panel-group" id="accordion">
           
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            What is the purpose of this coaching?<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                            ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                            CssClass="validator"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
   <asp:Panel ID="Panel3" runat="server">
        <div id="Div1" runat="server">
        <div style="height:5px"></div>
            <div class="panel panel-custom" id="PerfResult" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Performance Result <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                             
                                <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true">
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                                        </Scrolling>
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <div>
                                    &nbsp;</div>
                                <div class="col-md-10">
                                </div>
                                
                      
                    </div>
                    
                </div>
            </div>
       
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Agent Strengths and Opportunities<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strength/s: Agent did well on ____"
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunity/ies: Agent will need to improve on ____ "
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
   </asp:Panel>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Begin with Behaviour.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See Opening questions-successes and Areas for development.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                        <%-- <label for="Account" class="control-label">
                                Begin with Behaviour.</label><span class="glyphicon glyphicon-triangle-top triangletop">--%>
                    </div>
                </a>
                <div id="collapseFive" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div id="Div10" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadPositiveBehaviour" runat="server" class="form-control" placeholder="What positive behaviour did the employee exhibit? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox> 
                                        <telerik:RadTextBox ID="RadOpportunityBehaviour" runat="server" class="form-control" placeholder="What opportunity did the employee exhibit in the behaviour? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox>
                                    
                                </div>
                                <div class="col-md-6">
                                   <telerik:RadTextBox ID="RadBeginBehaviourComments" runat="server" class="form-control" placeholder=""
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="9">
                                        </telerik:RadTextBox> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<asp:Panel ID="Panel1" runat="server">
        <div id="Div3" runat="server">
         <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Review Results. Talk Specifics.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See metric specific questions.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseSix" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div id="Div2" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadBehaviourEffect" runat="server" class="form-control" placeholder="How did the behaviour, attitude, skill and knowledge affect the performance and results? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox> 
                                        <telerik:RadTextBox ID="RadRootCause" runat="server" class="form-control" placeholder="What is the root cause? Note: Build understanding with the employee "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox>
                                    
                                </div>
                                <div class="col-md-6">
                                   <telerik:RadTextBox ID="RadReviewResultsComments" runat="server" class="form-control" placeholder=""
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="9">
                                        </telerik:RadTextBox> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Panel>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Outline Action Plans. Keep it concise.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See general proving & discovery.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseSeven" class="panel-collapse collapse in">
                    <div class="panel-body">
                       <%-- <div id="Div3" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadTextBox8" runat="server" class="form-control" placeholder="How does the employee want to address the opportunities? Note: Build on strength. Agree on actions. "
                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                    </telerik:RadTextBox>
                                </div>
                               
                            </div>
                        </div>--%>
                        <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadAddressOpportunities" runat="server" class="form-control" placeholder="How does the employee want to address the opportunities? Note: Build on strength. Agree on actions."
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadActionPlanComments" runat="server" class="form-control" placeholder=""
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           Create Plan
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseEight" class="panel-collapse collapse in">
                    <div class="panel-body">
                       <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadSmartGoals" runat="server" class="form-control" placeholder="What are the SMART goals that you and the agent agreed on? Note: Identify and classify statements based on the SMART
structure below; Establish responsibilities and accountabilities."
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadCreatePlanComments" runat="server" class="form-control" placeholder=""
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
<asp:Panel ID="Panel2" runat="server">
     <div id="Div5" runat="server">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           Follow Through
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseNine" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <telerik:RadTextBox ID="RadFollowThrough" runat="server" class="form-control" placeholder="What activities have you done with the agent to justify his/her understanding of the coaching points?
Note:Ensure that you are confident that the agent will not commit the same challenge/s once you let him/her go back to the phone."
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                    </telerik:RadTextBox>

                      <asp:Panel ID="CNPRPanel" runat="server">
                            <div id="CNPR" runat="server">
                                <div id="collapseCNPR" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                            Previous Coaching Notes</label><br />
                                         <div style="height:5px"></div>
                                        <div>
                                            <div class="panel-body">
                                                <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                <MasterTableView>
                                                    <Columns>
                                                       <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Topic">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                                                        </Scrolling>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                            Previous Perfomance Results</label><br />
                                         <div style="height:5px"></div>
                                        <div>
                                            <div class="panel-body">
                                                <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                 <MasterTableView>
                                                    <Columns>
                                                       <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Target">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Current">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Previous">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Driver">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                              <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                 <ClientSettings>
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                                                        </Scrolling>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
    <div style="height:5px"></div>
      </div>
</asp:Panel>
            
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTen" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            OnItemDataBound="RadDocumentationReview_onItemDatabound" CssClass="RemoveBorders"
                            BorderStyle="None">
                            <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Item Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                        FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                        UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                        AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>

        

            <div class="form-group">
        </div>
        </div>