﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class AboutMain : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckReferencesLink();
        }

        private void CheckReferencesLink()
        {
            if (HttpContext.Current.User.Identity.Name != "")
            {
                btn_references.Visible = true;
                ShowReferenceContent();
            }
        }

        protected void btn_userguide_OnClick(object sender, EventArgs e) 
        {
            pn_userguide.Visible = true;
            pn_coachv2.Visible = false;
            pn_references.Visible = false;
            Label1.Text = "";
        }
        
        protected void btn_coachv2_OnClick(object sender, EventArgs e)
        {
            pn_userguide.Visible = false;
            pn_coachv2.Visible = true;
            pn_references.Visible = false;
        }

        protected void btn_references_OnClick(object sender, EventArgs e)
        {
            pn_userguide.Visible = false;
            pn_coachv2.Visible = false;
            pn_references.Visible = true;

            ShowReferenceContent();
        }

        private void ShowReferenceContent()
        {
            #region Google Form & Survey

            DataAccess ws = new DataAccess();
            DataSet ds = null;
            ds = ws.GetCoachLinks();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string ID = row.Field<int>("ID").ToString();

                    if (ID == "9")
                    {
                        GoogleFormsLink.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            GoogleFormsLink.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "10")
                    {
                        SurveyLink.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            SurveyLink.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                }

            }

            #endregion

            #region Guidelines

            DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            int cim_num = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);

            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(cim_num);
            string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

            string Role = DataHelper.MyRoleInSAP(cim_num);

            DataAccess ws1 = new DataAccess();
            int userhierarchy = ws1.getroleofloggedin_user(cim_num);

            if (rolevalue.IndexOf("VP") < 0)
            {
                if (userhierarchy > 0 && (Role == "Dir" || Role == "OM" || Role == "TL" || rolevalue == "CSR" || rolevalue == "Agent"))
                {
                    GuidelinesLink.Visible = true;

                    //Guidelines
                    if (userhierarchy == 1 || rolevalue == "CSR" || rolevalue == "Agent")
                    {
                        GuidelinesLink.Text = "The Coach 2.0 Guidelines - Comcast Agent.pdf";
                        GuidelinesLink.NavigateUrl = "~/Content/References/The Coach 2.0 Guidelines - Comcast Agent.pdf";
                    }
                    if (userhierarchy == 2 || Role == "TL")
                    {
                        GuidelinesLink.Text = "The Coach 2.0 Guidelines - Comcast Team Leader.pdf";
                        GuidelinesLink.NavigateUrl = "~/Content/References/The Coach 2.0 Guidelines - Comcast Team Leader.pdf";
                    }
                    if (userhierarchy == 3 || Role == "OM")
                    {
                        GuidelinesLink.Text = "The Coach 2.0 Guidelines - Comcast Operations Manager.pdf";
                        GuidelinesLink.NavigateUrl = "~/Content/References/The Coach 2.0 Guidelines - Comcast Operations Manager.pdf";
                    }
                    if (userhierarchy == 4 || Role == "Dir")
                    {
                        GuidelinesLink.Text = "The Coach 2.0 Guidelines - Comcast Director.pdf";
                        GuidelinesLink.NavigateUrl = "~/Content/References/The Coach 2.0 Guidelines - Comcast Director.pdf";
                    }
                }
            }

            #endregion
        }

        protected void btn_processmap_onClick(object sender, EventArgs e) 
        {
            //string ModalLabel = ""; //"Coachee has signed off from this review.";
            //string ModalHeader = ""; // "Success";
            //Image modalimage;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + modalimage + "'); });", true);
        }


    }
}