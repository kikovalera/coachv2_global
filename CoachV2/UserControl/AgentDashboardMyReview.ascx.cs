﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Charting;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class AgentDashboardMyReview : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            int intcim = Convert.ToInt32(cim_num);

            //DataSet ds_reviewlist_agent = DataHelper.Get_Agent_MyReviews(intcim);
            //grd_ReviewList_agent.DataSource = ds_reviewlist_agent;
            //if (ds_reviewlist_agent.Tables[0].Rows.Count > 0)
            //{
            //    label2.Text = Convert.ToString(ds_reviewlist_agent.Tables[0].Rows.Count);
            //}
            //else
            //{
            //    label2.Text = "0";
            //}

            // conditional view of review list for non agents (francis.valera/07172018)
            // reused agent reviewlist based on UAT0719 (francis.valera/07192018)
            // restored conditional review list and separate SPs for agents and upper ranks (francis.valera/07232018)

            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
            if (saprolefordashboard == "TL" || saprolefordashboard == "OM" || saprolefordashboard == "Dir")
            {
                DataSet ds_reviewlist_agent = DataHelper.Get_NonAgentReviewList(intcim);
                grd_ReviewList_agent.DataSource = ds_reviewlist_agent;
                if (ds_reviewlist_agent.Tables[0].Rows.Count > 0)
                {
                    label2.Text = Convert.ToString(ds_reviewlist_agent.Tables[0].Rows.Count);
                }
                else
                {
                    label2.Text = "0";
                }
            }
            else
            {
                DataSet ds_reviewlist_agent = DataHelper.Get_Agent_MyReviews(intcim);
                grd_ReviewList_agent.DataSource = ds_reviewlist_agent;
                if (ds_reviewlist_agent.Tables[0].Rows.Count > 0)
                {
                    label2.Text = Convert.ToString(ds_reviewlist_agent.Tables[0].Rows.Count);
                }
                else
                {
                    label2.Text = "0";
                }
            }
            
        }

        protected void grd_ReviewList_agent_databound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
               
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];
                //5/28/2018

                //val2=reviewid

                //string val2 = hLink.Text;
                string val2 = item["reviewtypeid"].Text;
                string formtype = item["formtype"].Text;
                string val1 = hLink.Text;

                //string val1 = hLink.Text;
                //int reviewid = Convert.ToInt32(val1);
                //DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                //string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString();

                //string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                //if ((formtype == null) || (formtype == ""))
                //{

                //    formtype = "0";
                //}
                string enctxt = DataHelper.Encrypt(Convert.ToInt32(val1));
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                string reviewId = DataHelper.Encrypt(Convert.ToInt32(hLink.Text));
                if (Convert.ToInt32(val2) == 1)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {

                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }
                else if (Convert.ToInt32(val2) == 2)
                {
                    if (Convert.ToInt32(formtype) == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 2)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else if (Convert.ToInt32(formtype) == 3)
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }

                }

                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 4)
                {

                    //hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                    DataSet ds = DataHelper.GetCoachingByTix(val1.ToString());
                    int? ReqSignOff = Convert.ToInt32(ds.Tables[0].Rows[0]["RequireSignOff"]);

                    if (ReqSignOff == 1)
                    {
                        hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/NSOAddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.NavigateUrl = "~/NSOAddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                        hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                    }
                }

                else if (Convert.ToInt32(val2) == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }

                else if (Convert.ToInt32(val2) == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                    hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                }
                
            }
        }
    }
}