﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class MassCoachingSelected : System.Web.UI.UserControl
    {

        public DataTable DataEntryEmployee
        {
            get
            {
                return ViewState["DataEntryEmployee"] as DataTable;
            }
            set
            {
                ViewState["DataEntryEmployee"] = value;
            }
        }

       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                GetDropDownSessionsSelected();
                //PopulateTopicsSelected();
                PerfResultsEmployee();
               
            }
        }
  
       

        protected void GetDropDownSessionsSelected()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionTypeSelected.DataSource = ds;
                RadSessionTypeSelected.DataTextField = "SessionName";
                RadSessionTypeSelected.DataValueField = "SessionID";
                RadSessionTypeSelected.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }
        protected void PopulateTopicsSelected()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionTypeSelected.SelectedValue);
                GetDropDownTopicsSelected(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void GetDropDownTopicsSelected(int SessionID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics(SessionID);
                RadSessionTopicSelected.DataSource = ds;
                RadSessionTopicSelected.DataTextField = "TopicName";
                RadSessionTopicSelected.DataValueField = "TopicID";
                RadSessionTopicSelected.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void RadSessionTypeSelected_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionTypeSelected.SelectedValue);
                GetDropDownTopicsSelected(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        protected void RadAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

               
                if (RadCimNumber.Text!="")
                {
                    if (IsExisting(Convert.ToInt32(RadCimNumber.Text), DataEntryEmployee))
                    {

                        if (IsSubordinate(CIMNumber, RadCimNumber.Text))
                        {
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo(Convert.ToInt32(RadCimNumber.Text));

                            string CIM = ds.Tables[0].Rows[0]["Cim Number"].ToString();
                            string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                            string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();
                            string Name = FirstName + " " + LastName;

                            //this.RadList.Items.Add(this.RadCimNumber.Text.Trim());
                            //RadList.Items.Add(new RadListBoxItem(CIM.ToString(), Name.ToString()));

                            DataTable dt = new DataTable();
                            DataRow dr;
                            dt = (DataTable)ViewState["DataEntryEmployee"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["CIM"] = CIM;
                            dr["Name"] = Name;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["DataEntryEmployee"] = dt;
                            }
                            this.BindListView();

                            //RadGridEmployees.DataSource = DataEntryEmployee;
                            //RadGridEmployees.DataBind();
                            this.RadCimNumber.Text = "";

                        }
                        else
                        {

                            string scriptstring = "alert('Please check the CIM Number.');";
                            // ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true); 
                            // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", scriptstring, true);
                            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "clentscript", scriptstring, true);

                        }
                    }
                    else
                    {
                        string scriptstring = "alert('CIM Number already exists.');";
                        // ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true); 
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", scriptstring, true);
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "clentscript", scriptstring, true);

                    }
                }
                else
                {
                    string scriptstring = "alert('Please check the CIM Number.');";
                   // ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true); 
                   // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", scriptstring, true);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "clentscript",scriptstring, true);
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        //protected void RadRemove_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.RadList.Items.Remove(this.RadList.SelectedItem);
        //    }
        //    catch (Exception ex)
        //    {

        //        string myStringVariable = ex.ToString();
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
        //    }
        //}

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadGridEmployees.Items.Count > 0)
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    int MassCoachingID = ws.InsertMassCoachingSelected(3, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopicSelected.SelectedValue), CIMNumber, 0, false);

                    //foreach (RadListBoxItem item in RadList.Items)
                    //{
                    //    string CIM = item.Value;
                    //    DataAccess wsSelected = new DataAccess();
                    //    wsSelected.InsertMassCoachingSelectedPerCoachee(3, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopicSelected.SelectedValue), CIMNumber, Convert.ToInt32(CIM), MassCoachingID);

                    //}

                    foreach (GridDataItem itm in RadGridEmployees.Items)
                    {

                       Label CIM = (Label)itm.FindControl("LblCIM"); ;

                       DataAccess wsSelected = new DataAccess();
                       wsSelected.InsertMassCoachingSelectedPerCoachee(3, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopicSelected.SelectedValue), CIMNumber, Convert.ToInt32(CIM.Text), MassCoachingID, false);


                       //RadGridEmployees.DataSource = null;
                       //RadGridEmployees.DataBind();
                    }

                    DisableItems();
                    string scriptstring = "alert('Successfully saved review.');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true);
                }
                else
                {
                    string scriptstring = "alert('Please input CIM Numbers of Employees.');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true);
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true); 

            }
        }

        public bool IsSubordinate(int CimNumber, string Subordinate)  
        {

            DataTable ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.CheckIfSubordinate(CimNumber);

            if (ds.Rows.Count > 0)
            {
                DataRow[] foundAuthors = ds.Select("CIM_Number = '" + Subordinate + "'");
                if (foundAuthors.Length != 0)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        

        public void DisableItems()
        {
            RadAdd.Visible = false;
            //RadRemove.Visible = false;
            //RadList.Enabled = false;
            RadSessionTopicSelected.Enabled = false;
            RadSessionTypeSelected.Enabled = false;
            RadAgenda.Enabled = false;
            RadDescription.Enabled = false;
            RadCoacherSignOff.Visible = false;
            RadCimNumber.Enabled = false;
            RadAdd.Visible = false;
            RadGridEmployees.Enabled = false;
            
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                GridDataItem deleteitem = (GridDataItem)e.Item;
                Label LblID = (Label)deleteitem.FindControl("LblCIM");

                string ID = LblID.Text;
                DataTable dt = (DataTable)ViewState["DataEntryEmployee"];

                for (int i = dt.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dt.Rows[i];
                    if (dr["CIM"].ToString() == ID)
                    {
                        dr.Delete();
                    }
                }

                this.BindListView();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true); 

            }
        }

        protected void PerfResultsEmployee()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CIM", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            ViewState["DataEntryEmployee"] = dt;
            BindListView();
        }

        
        public void BindListView()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["DataEntryEmployee"];
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridEmployees.DataSource = dt;
                    RadGridEmployees.DataBind();
                }
                else
                {
                    RadGridEmployees.DataSource = null;
                    RadGridEmployees.DataBind();
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }

        }


        public bool IsExisting(int CIM, DataTable dt)
        {

            DataRow[] foundCIM = dt.Select("CIM = '" + CIM + "'");
            if (foundCIM.Length != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            try
            {
                RadGridEmployees.DataSource = DataEntryEmployee;
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", ex.ToString(), true);
            }
        }

        public void refresh()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            RadSessionTypeSelected.Enabled = true;
            RadSessionTopicSelected.Enabled = true;
            //PopulateTopicsSelected();
            RadSessionTypeSelected.Items.Clear();
            RadSessionTypeSelected.DataSource = null;
            RadSessionTypeSelected.DataBind();
            RadSessionTypeSelected.SelectedIndex = -1;
            RadSessionTypeSelected.Text = String.Empty;
            RadSessionTypeSelected.ClearSelection();
            GetDropDownSessionsSelected();
            PerfResultsEmployee();
            RadAdd.Visible = true;
            RadDescription.Enabled = true;
            RadAgenda.Enabled = true;
            RadDescription.Text = "";
            RadAgenda.Text = "";
            RadCimNumber.Enabled = true;
            RadCoacherSignOff.Visible = true;
            RadCimNumber.Text = "";
            //RadSessionTopicSelected.DataSource = null;
            //RadSessionTopicSelected.DataBind();
            RadSessionTopicSelected.Items.Clear();
            RadSessionTopicSelected.DataSource = null;
            RadSessionTopicSelected.DataBind();
            RadSessionTopicSelected.SelectedIndex = -1;
            RadSessionTopicSelected.Text = String.Empty;
            RadSessionTopicSelected.ClearSelection();
            BindListView();
        }
    }
}