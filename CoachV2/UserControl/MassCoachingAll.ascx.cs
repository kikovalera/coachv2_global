﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class MassCoachingAll : System.Web.UI.UserControl
    {
        //int CompanyGroup;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
         
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                GetDropDownSessionsAll();
                //PopulateTopicsAll();
                
            }
          
        }
       
       

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                ws.InsertMassCoachingAll(1, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopicAll.SelectedValue), CIMNumber, false);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "alert('Successfully saved review.')", true);
                DisableItems();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true); 

            }
        }

      
        protected void GetDropDownSessionsAll()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionTypeAll.DataSource = ds;
                RadSessionTypeAll.DataTextField = "SessionName";
                RadSessionTypeAll.DataValueField = "SessionID";
                RadSessionTypeAll.DataBind();

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true); 
            }
        }

       
      
        protected void PopulateTopicsAll()
        {
            try
            {
                int SessionIDAll;
                SessionIDAll = Convert.ToInt32(RadSessionTypeAll.SelectedValue);
                GetDropDownTopicsAll(SessionIDAll);

               
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true); 
            }
        }
      

        protected void GetDropDownTopicsAll(int SessionID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics(SessionID);
                RadSessionTopicAll.DataSource = ds;
                RadSessionTopicAll.DataTextField = "TopicName";
                RadSessionTopicAll.DataValueField = "TopicID";
                RadSessionTopicAll.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
            }

        }
       
      
        protected void RadSessionTypeAll_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionTypeAll.SelectedValue);
                GetDropDownTopicsAll(SessionID);
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
            }
        }

        public void DisableItems()
        {
            RadSessionTopicAll.Enabled = false;
            RadSessionTypeAll.Enabled = false;
            RadDescription.Enabled = false;
            RadAgenda.Enabled = false;
            RadCoacherSignOff.Visible = false;
        }


        public void refresh()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            RadSessionTypeAll.Items.Clear();
            RadSessionTypeAll.DataSource = null;
            RadSessionTypeAll.DataBind();
            RadSessionTypeAll.Text = String.Empty;
            RadSessionTypeAll.ClearSelection();
            GetDropDownSessionsAll();
            RadDescription.Enabled = true;
            RadAgenda.Enabled = true;
            RadDescription.Text = "";
            RadAgenda.Text = "";
            RadSessionTypeAll.Enabled = true;
            RadSessionTopicAll.Enabled = true;
            RadCoacherSignOff.Visible = true;
            RadSessionTopicAll.Items.Clear();
            RadSessionTopicAll.DataSource = null;
            RadSessionTopicAll.DataBind();
            RadSessionTopicAll.Text = String.Empty;
            RadSessionTopicAll.ClearSelection();
    
            
        }
    }
}