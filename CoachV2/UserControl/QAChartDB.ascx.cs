﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2
{
    public partial class QAChartDB : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                string gmail = ds.Tables[0].Rows[0][13].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_sap_Details = DataHelper.get_adminusername(gmail);
                int acctno = Convert.ToInt32(ds_sap_Details.Tables[0].Rows[0]["accountid"].ToString());
                DataSet ds_KPIforAgent = DataHelper.GetKPIList(acctno);
                cb_KPIList.DataSource = ds_KPIforAgent;
                cb_KPIList.DataBind();
                cb_KPIListScore.DataSource = ds_KPIforAgent;
                cb_KPIListScore.DataBind();
                cb_KPIListCurr.DataSource = ds_KPIforAgent;
                cb_KPIListCurr.DataBind();

            }
        }

        protected void Btn_generate1_Click(object sender, EventArgs e)
        {
            if (dpstartdate.SelectedDate < dpenddate.SelectedDate)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_accountsforQA = DataHelper.GetQAdepartment(intcim);
                int deptnum = Convert.ToInt32(ds_accountsforQA.Tables[0].Rows[0]["departmentid"].ToString());

                DateTime start = dpstartdate.SelectedDate.GetValueOrDefault();
                DateTime end = dpenddate.SelectedDate.GetValueOrDefault();
                start.Date.ToShortDateString();
                end.Date.ToShortDateString();
                DataSet ds_tlfrequency = DataHelper.GetQAFrequency(start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"), deptnum);
                RHTML_Frequency.DataSource = ds_tlfrequency;
                RHTML_Frequency.DataBind();
            }
            else
            {
                string message = "Invalid date input!";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
            }
        }

        protected void btn_generatebehavior_Click(object sender, EventArgs e)
        {
            if (dp_behaviorstart.SelectedDate < dp_behaviorend.SelectedDate)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_accountsforQA = DataHelper.GetQAdepartment(intcim);
                int deptnum = Convert.ToInt32(ds_accountsforQA.Tables[0].Rows[0]["departmentid"].ToString());
                DateTime start = dp_behaviorstart.SelectedDate.GetValueOrDefault();
                DateTime end = dp_behaviorend.SelectedDate.GetValueOrDefault();
                start.Date.ToShortDateString();
                end.Date.ToShortDateString();
                //Top Behaviors Overall
                DataSet ds_behavior = DataHelper.GetBehaviorforQA(start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"), deptnum);
                RHTMLBehavior.DataSource = ds_behavior;
                RHTMLBehavior.DataBind();
            }
            else
            {
                string message = "Invalid date input!";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
            }
        }

        protected void btn_generate_driver_Click(object sender, EventArgs e)
        {
            if (dp_Start_kpivsd.SelectedDate < dp_end_kpivsd.SelectedDate)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_accountsforQA = DataHelper.GetQAdepartment(intcim);
                int deptnum = Convert.ToInt32(ds_accountsforQA.Tables[0].Rows[0]["departmentid"].ToString());
                DateTime start = dp_Start_kpivsd.SelectedDate.GetValueOrDefault();
                DateTime end = dp_end_kpivsd.SelectedDate.GetValueOrDefault();
                start.Date.ToShortDateString();
                end.Date.ToShortDateString();
                int kpi_selected = Convert.ToInt32(cb_KPIList.SelectedValue);
                //KPIVsDriver
                DataSet ds_KPIvsD = DataHelper.GetBehaviorVsKPIQA(start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"), deptnum, kpi_selected);
                RHTMLKPIVsDriver.DataSource = ds_KPIvsD;
                RHTMLKPIVsDriver.DataBind();
            }
            else
            {
                string message = "Invalid date input!";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
            }
        }

        protected void btn_Scr_Click(object sender, EventArgs e)
        {
            if (dp_score_start.SelectedDate < dp_score_end.SelectedDate)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_accountsforQA = DataHelper.GetQAdepartment(intcim);
                int deptnum = Convert.ToInt32(ds_accountsforQA.Tables[0].Rows[0]["departmentid"].ToString());
                DateTime start = dp_score_start.SelectedDate.GetValueOrDefault();
                DateTime end = dp_score_end.SelectedDate.GetValueOrDefault();
                start.Date.ToShortDateString();
                end.Date.ToShortDateString();
                int kpi_selected = Convert.ToInt32(cb_KPIListScore.SelectedValue);
                DataSet ds_score = DataHelper.GetKPISCoreforQA(start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"), deptnum, kpi_selected);
                RHTMLSCORE.DataSource = ds_score;
                RHTMLSCORE.DataBind();
            }
            else
            {
                string message = "Invalid date input!";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
            }
        }

        protected void btn_Curr_Click(object sender, EventArgs e)
        {
            if (dp_currstart.SelectedDate < dp_currend.SelectedDate)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_accountsforQA = DataHelper.GetQAdepartment(intcim);
                int deptnum = Convert.ToInt32(ds_accountsforQA.Tables[0].Rows[0]["departmentid"].ToString());
                DateTime start = dp_currstart.SelectedDate.GetValueOrDefault();
                DateTime end = dp_currend.SelectedDate.GetValueOrDefault();
                start.Date.ToShortDateString();
                end.Date.ToShortDateString();
                int kpi_selected = Convert.ToInt32(cb_KPIListCurr.SelectedValue);
                DataSet ds_scorevstarget = DataHelper.GetKPISCoreVsTargetQA(start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"), deptnum, kpi_selected);
                RHTML_CurrScore.DataSource = ds_scorevstarget;
                RHTML_CurrScore.DataBind();
            }
            else
            {
                string message = "Invalid date input!";
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);
            }
        }
    }
}