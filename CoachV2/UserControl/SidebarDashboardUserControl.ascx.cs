﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CoachV2.AppCode;
namespace CoachV2.UserControl
{
    public partial class SidebarDashboardUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                int int_cim = Convert.ToInt32(cim_num);

                //testing purposes for sir bart's access 
                //string emp_email = "ROMEL.BARATETA@TRANSCOM.COM";//ds.Tables[0].Rows[0]["Email"].ToString();
                //string cim_num = "10113944";  // ds.Tables[0].Rows[0][0].ToString();
                //int int_cim = 10113944;  //Convert.ToInt32(cim_num);
                //string emp_email = "TRACEY.RUSTIA@TRANSCOM.COM";//"ROMEL.BARATETA@TRANSCOM.COM";//ds.Tables[0].Rows[0]["Email"].ToString(); //"MARIA.CASTANEDA@TRANSCOM.COM";//"MARVIN.REGALADO@TRANSCOM.COM"; //
                //string cim_num = "95759"; //"10113944";  // ds.Tables[0].Rows[0][0].ToString();
                //int int_cim = 95759;//10113944;  //Convert.ToInt32(cim_num);
            
                //testing purposes for sir bart's access 
                DataSet ds_usersuperadmin = DataHelper.GetSuperAdminAccessAll(int_cim); //if user has access roleid 13 

                if (ds_usersuperadmin.Tables[0].Rows.Count > 0)
                {
                    HLReportID.Visible = true; // restored admin link as per bart's request on 10082018 (francis.valera)
                    //old value 
                    //HLReportID.Visible = false; 
                }
                else if (ds_usersuperadmin.Tables[0].Rows.Count == 0)
                {
                    HLReportID.Visible = false;
                }

                DataSet ds_getuserroleid = DataHelper.GetEmployeeRoleID(int_cim); //intcim);
                DataSet ds_userselect = DataHelper.GetAllUserRoles(int_cim);
                int userroleid = Convert.ToInt32(ds_getuserroleid.Tables[0].Rows[0]["roleid"]);
                int userrolegroup = Convert.ToInt32(ds_getuserroleid.Tables[0].Rows[0]["rolegroupid"]);
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(int_cim);
                string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());
                DataSet ds_getroleformuserrole = DataHelper.GetAllUserRoles(int_cim);

                CheckIfHasMyReviews(ds_getroleformuserrole);
                checkifhasmyreports( ds_getroleformuserrole);
                checkifhasmyteam(ds_getroleformuserrole);

                //update this for the roles 
                string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(int_cim));

                get_hierarchy(); 
                //string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
                //if  ((saprolefordashboard == "OM") || (saprolefordashboard == "Dir"))
                //{
                //    HyperLink3.Visible = true; //view for triad coaching
                //} 
                //else
                //{
                //    HyperLink3.Visible = false ;
                //}
            
                //update this for the roles 
            }
        }

        protected void get_hierarchy()
        {
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            string cim_num = ds.Tables[0].Rows[0][0].ToString();
            int intcim = Convert.ToInt32(cim_num); 


            DataSet ds_hassubs = DataHelper.CheckforSubs(Convert.ToInt32(intcim));
            DataSet ds1 = null;
            DataAccess ws1 = new DataAccess();
            int userhierarchy;
            userhierarchy = ws1.getroleofloggedin_user(intcim);

            DataAccess ws2 = new DataAccess();
            string department = "";
            department = ws2.getuserdept(intcim);

            int deptid, roleid;
            DataSet ds_userdept = DataHelper.get_adminusername(emp_email);
            if (ds_userdept.Tables[0].Rows.Count > 0)
            {
                deptid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["accountid"].ToString());
                roleid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["roleid"].ToString());
            }
            else
            {
                deptid = 0;
                roleid = 0;
            }
            DataSet ds_checkhierarchy = DataHelper.checkuserhierarchy(intcim);
            if (ds_checkhierarchy.Tables[0].Rows.Count > 0)
            {
                if (deptid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["dept_id"].ToString()) && (roleid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["role_id"].ToString())))
                { }
                else
                {
                    //update user hierarchy
                    DataHelper.Edituserhierarchy(intcim, deptid, roleid, userhierarchy);
                }
            }

            else
            {
                DataHelper.Insertuserhierarchy(intcim, deptid, roleid, userhierarchy);

            }
             
                if ((userhierarchy == 1) || (userhierarchy == 2))
                { //w/o triad
                    
                        HyperLink3.Visible = false; 
                }
                else if ((userhierarchy == 3) || (userhierarchy == 4))
                {//w/ triad
                    HyperLink3.Visible = true;
                }
                else
                {
                    HyperLink3.Visible = false;
                }
           

            //testing purposes
            //if (intcim == 10107032)
            //{
            //    DashboardDiv.Controls.Clear();
            //    QADashboard QADashboard = (QADashboard)LoadControl("UserControl/QADashboard.ascx");
            //    DashboardDiv.Controls.Add(QADashboard);
            //}


        }
        protected void rdbtn_Settings_OnClick_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin.aspx");

        }


        protected void btn_dashboardnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Dashboard.aspx");
        }

        protected void CheckIfHasMyReviews(DataSet ds_getroleformuserrole)
        {
            if (ds_getroleformuserrole.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < ds_getroleformuserrole.Tables[0].Rows.Count; ctr++)
                {
                    int int_rolecatid = Convert.ToInt32(ds_getroleformuserrole.Tables[0].Rows[ctr]["rolecatid"]);
                    if (int_rolecatid == 3)
                    { HyperLink2.Visible = true; }
                }
            }
            else
            {
                HyperLink2.Visible = false;
            }
        }

        protected void LinkButtonMyReviews_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AddReview.aspx");
        }

        protected void checkifhasmyreports( DataSet ds_getroleformuserrole)
        {
            if (ds_getroleformuserrole.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < ds_getroleformuserrole.Tables[0].Rows.Count; ctr++)
                {
                    int int_rolecatid = Convert.ToInt32(ds_getroleformuserrole.Tables[0].Rows[ctr]["rolecatid"]);
                    if (int_rolecatid == 4)
                    { 
                        LinkButtonMyReports.Visible = false; 
                    }
                }
            }

            else
            {
                LinkButtonMyReports.Visible = false;
            }

        }

        protected void checkifhasmyteam(DataSet ds_getroleformuserrole)
        {

            if (ds_getroleformuserrole.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < ds_getroleformuserrole.Tables[0].Rows.Count; ctr++)
                {
                    int int_rolecatid = Convert.ToInt32(ds_getroleformuserrole.Tables[0].Rows[ctr]["rolecatid"]);
                    if (int_rolecatid == 2)
                    { HyperLink1.Visible = true; }
                }
            }

            else
            {
                HyperLink1.Visible = false;
            }

        }

    
    }
}