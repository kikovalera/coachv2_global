﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CoachV2.UserControl
{
    public partial class ReviewsProfUserCtrl : System.Web.UI.UserControl
    {
        string cim_num;
        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (Page.Request.QueryString["CIM"] != null)
            {
                cim_num = Page.Request.QueryString["CIM"];
            }

            else
            {
                DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

                DataSet ds = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

               
            }
        }

        protected void ReviewGrid_PreRender(object sender, EventArgs e)
        {
            //DataSet ds2 = DataHelper.GetUserInfoLocalViaCIMNo(cim_num);
            DataSet ds2 = DataHelper.GetMyReviewsByCIM(cim_num);

            if (ds2.Tables[0].Rows.Count > 0)
            {
                ReviewGrid.DataSource = DataHelper.GetMyReviewsByCIM(cim_num);
                ReviewGrid.DataBind();
            }
            else
            {
                ReviewGrid.Controls.Add(new LiteralControl("No record found"));
            }
        }
    }
}