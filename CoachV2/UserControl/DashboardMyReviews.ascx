﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardMyReviews.ascx.cs"
    Inherits="CoachV2.UserControl.DashboardMyReviews" %>
<div class="panel menuheadercustom" runat="server" id="myreviewtitle">
    <div>
        &nbsp;<span class="glyphicon glyphicon-dashboard"></span>
        <%--replaced span label with proper case (francis.valera/07182018)--%>
        <asp:Label ID="LblMyReviews" runat="server" Text="Coaching Dashboard > My Reviews"></asp:Label>
        <span class="breadcrumb2ndlevel">
            <asp:Label ID="Label1" runat="server" Text=" "></asp:Label>
        </span><span class="breadcrumb2ndlevel">
            <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label>
        </span>
    </div>
</div>
<div class="panel-group" id="accordion">
    <div class="container-fluid" runat="server">
        <div class="row">
            <div class="col-sm-9" id="SearchPane" runat="server">
                <ul class="list-inline" id="menu" runat="server">
                    <li id="listlinkqa" runat="server">
                        <asp:HyperLink ID="HyperLinkQA" runat="server" NavigateUrl="~/AddReviewQA.aspx"><span class="glyphicon glyphicon-plus-sign"></span> Add New QA Review</asp:HyperLink></li>
                    <li id="listlinkhr" runat="server">
                        <asp:HyperLink ID="HyperLinkHR" runat="server" NavigateUrl="~/AddReviewHR.aspx" 
                            Visible="false"><span class="glyphicon glyphicon-plus-sign"></span> Add New HR Review</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLinkRev" runat="server"><span class="glyphicon glyphicon-plus-sign"></span> Add New Review</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/AddMassCoaching.aspx"
                            class="btn"><span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-plus"></span> Add Mass Reviews</asp:HyperLink></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle btn" data-toggle="dropdown"><span class="glyphicon glyphicon-cloud"></span> Remote
                        Coach <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/AddRemoteCoaching.aspx">Sign Off Required</asp:HyperLink></li>
                            <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/NSOAddRemoteCoaching.aspx">No Sign Off Required</asp:HyperLink></li>
                            
                        </ul>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/SearchReview.aspx"><span class="glyphicon glyphicon-search"></span> Search Reviews</asp:HyperLink></li>
                </ul>
                <%--<asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/AddRemoteCoaching.aspx" class="btn"><span class="glyphicon glyphicon-cloud"></span> Remote Coach</asp:HyperLink>--%>
            </div>
            <asp:Panel ID="Search" runat="server">
                <div class="col-sm-3">
                    <%--      <form>--%>
                    <div class="input-group">
                        <asp:TextBox ID="TxtSearch" runat="server" type="text" CssClass="form-control" placeholder="Search…"
                            ValidationGroup="SearchFormReviews" />
                        <span class="input-group-btn">
                            <asp:Button ID="BtnSearchQuery" runat="server" Text="Go" CssClass="btn btn-info"
                                OnClick="BtnSearchQuery_Click" UseSubmitBehavior="true" ValidationGroup="SearchFormReviews" />
                            <%--changed BtnSearchQuery cssclass FROM btn-default TO btn-info (07112018/francis.valera)--%>
                        </span>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Check the following field(s):"
                            ValidationGroup="SearchFormReviews" ShowMessageBox="true" ShowSummary="false" />
                    </div>
                    <%--   </form>   --%>
                </div>
            </asp:Panel>
        </div>
    </div>
</div>
