﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PerfGraphUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.PerfGraphUserCtrl" %>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 10px;">
                From: <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date" DateInput-Enabled="false"
                    DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd" Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="Requiredfieldvalidator2" ControlToValidate="StartDate"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
                To: <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                    DateInput-DateFormat="yyyy-MM-dd" Calendar-ClientEvents-OnDateSelected="OnDateSelected">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="EndDate"
                    ErrorMessage="*"></asp:RequiredFieldValidator> 
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;">
                KPI: <telerik:RadComboBox ID="ddKPIList" runat="server" EmptyMessage="Select KPI">
                    <DefaultItem Value="0" Text="All" />
                </telerik:RadComboBox>
                <asp:HiddenField ID="MyCIM" runat="server" />
                <button type="button" id="check_graph" class="btn btn-md btn-primary" disabled="disabled">
                   <i class="glyphicon glyphicon-search"></i> View</button>
            </div>
           </div> 
        </div>

    </div>
    <div class="col-md-6">
        <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
            padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>  No data found</span>
        <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
            runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
            Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. " Style="padding-top: 10px;
            padding-bottom: 10px;">
        </asp:CompareValidator>
    </div>
</div>
<div id="placeholder" class="col-xs-12 demo-placeholder" style="width: 100%; height: 300px;">
</div>
<script type="text/javascript" src="<%# Eval(HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath )) %>/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    function pageLoad() {
        //  $('#radPackageDate_dateInput').val("Package Date");
        $find("<%= StartDate.ClientID %>").get_dateInput().set_textBoxValue('From');
        $find("<%= EndDate.ClientID %>").get_dateInput().set_textBoxValue('To');
    }

    function OnDateSelected(sender, args) {
        //sender.hideDropDown();
        // sender.get_element().parentNode.nextSibling.focus();
        $("#check_graph").removeAttr("disabled");
    }


    var fplot = function (e, data, options) {
        var jqParent, jqHidden;
        if (e.offsetWidth <= 0 || e.offetHeight <= 0) {
            // lets attempt to compensate for an ancestor with display:none
            jqParent = $(e).parent();
            jqHidden = $("<div style='visibility:hidden'></div>");
            $('body').append(jqHidden);
            jqHidden.append(e);
        }

        var plot = $.plot(e, data, options);

        // if we moved it above, lets put it back
        if (jqParent) {
            jqParent.append(e);
            jqHidden.remove();
        }

        return plot;
    };

    $(document).ready(function () {

        var opts = {
            legend: {
                show: true,
                margin: 10
            },

            series: {
                lines: {
                    show: true,
                    fill: false
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true,
                clickable: true
            },
            yaxis: {
                min: 0,
                max: 50
            },
            xaxis: {
                mode: "categories"
            }

        };

        $("#placeholder").bind("plothover", function (event, pos, item) {

            //if ($("#enablePosition:checked").length > 0) {
            //	var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
            //	$("#hoverdata").text(str);
            //}

            //if ($("#enableTooltip:checked").length > 0) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(0);

                $("#tooltip").html(item.series.label + " " + y)
						.css({ top: item.pageY + 5, left: item.pageX + 5 })
						.fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
            //}
        });



        $("#err-msg").hide();
        $("#check_graph").on("click", function () {
            var data = [];
            var data2 = [];


            var obj;
            var c = [];

            var i = 0;

            var xxxx;
            var dataString = {};
            var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
            var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

            var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
            var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

            var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
            var dropFind = $find("<%= ddKPIList.ClientID %>");
            var KPIid = dropFind.get_value();

            $("<div id='tooltip'></div>").css({
                position: "absolute",
                display: "none",
                border: "1px solid #fdd",
                padding: "2px",
                "background-color": "#fee",
                opacity: 0.80
            }).appendTo("body")



            $.ajax({
                type: "POST",
                data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', KPIid: '" + KPIid + "', CIMNo: '" + CIMNo + "'}",
                contentType: "application/json; charset=utf-8",
                url: "/FakeApi.asmx/getData",
                dataType: 'json',
                success: function (msg) {

                    if (msg != null && msg.d == null) {
                        $("#err-msg").show();
                        fplot($("#placeholder"), [[]], opts);
                    }
                    else {
                        $("#err-msg").hide();
                    }

                    $.each(msg, function (e, f) {

                        c = f.TargetCount;
                        data.push([f.ADate, f.CoachCount]);
                        data2.push([f.ADate, f.TargetCount]);

                        $("#err-msg").hide();

                    });

                    fplot($("#placeholder"), [{
                        data: data,
                        label: "Total Coach count",
                        bars: {
                            show: true,
                            align: "center",
                            barWidth: 0.1,
                            lineWidth: 0.5,
                            fill: 1
                        }
                    }
            ], opts)

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

        });
    });
</script>
