﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2.UserControl
{
    public partial class SearchAdvUserCtrl : System.Web.UI.UserControl
    {
        private const int ItemsPerRequest = 10;

        private int cimNo;

        private int AccId;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultButton = BtnSearchQuery.UniqueID;

            if (!IsPostBack)
            {
                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                ctrlA.Visible = true;
                ctrlA.Text = "> Search Reviews";

                Panel Search = (Panel)DashboardMyReviewsUserControl1.FindControl("Search");
                Search.Visible = true;
                // this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").
                //var searchp = FindControl("DashboardMyReviewsUserControl1").FindControl("Search");
                //searchp.Visible = true;

                LoadAccounts();
                LoadSupervisors();
                LoadCoachee();
                LoadSessionTypes();

                //if (TxtSearch.Text != "")
                //{
                //    SearchGrid.Visible = true;
                //    SearchGrid.DataSource = DataHelper.GetSearchQuery_forAgent(TxtSearch.Text, cimNo);
                //    SearchGrid.DataBind();
                // }
            }
        }

        void LoadAccounts()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //cimNo = TxtSearchCIM.Text == "" ? Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]) : Convert.ToInt32(TxtSearchCIM.Text);

            cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);

            DataSet dsMyDetails = DataHelper.GetMyAcccountDetails(cimNo);
            AccId = Convert.ToInt32(dsMyDetails.Tables[0].Rows[0]["CampaignID"]);

            //LoadAccounts(AccId);
            DDAccount.Items.Clear();
            DDAccount.DataSource = DataHelper.GetAccountsForSearch(AccId);
            DDAccount.DataTextField = "Account";
            DDAccount.DataValueField = "AccountID";
            DDAccount.DataBind();
            DDAccount.SelectedIndex = 0;
        }

        protected void DDAccount_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadSupervisors();
        }

        void LoadSupervisors()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            cimNo = TxtSearchCIM.Text == "" ? Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]) : Convert.ToInt32(TxtSearchCIM.Text);
            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(cimNo));
            //DataSet dsMyDetails = DataHelper.GetMyAcccountDetails(cimNo);
            //AccId = Convert.ToInt32(dsMyDetails.Tables[0].Rows[0]["CampaignID"]);
            //DDSupervisor.DataSource = DataHelper.GetMyAcccountDetails(cimNo);
            //DDSupervisor.DataTextField = "MyFullName";
            //DDSupervisor.DataValueField = "CimNumber";
            //DDSupervisor.DataBind();
            //DDSupervisor.SelectedIndex = 0;
            if (saprolefordashboard == "OM" || saprolefordashboard == "Dir")
            {
                // populate SUPERVISOR dropdown with TLs under OM account (07112018/francis.valera)
                //DataSet dsMyDetails = DataHelper.GetMyAcccountDetails(cimNo);
                //AccId = Convert.ToInt32(dsMyDetails.Tables[0].Rows[0]["CampaignID"]);
                //DDSupervisor.DataSource = DataHelper.GetMyAcccountDetails(cimNo);
                //DDSupervisor.DataTextField = "MyFullName";
                //DDSupervisor.DataValueField = "CimNumber";
                //DDSupervisor.DataBind();
                //DDSupervisor.SelectedIndex = 0;
                DDSupervisor.DataSource = DataHelper.GetSubordinatesAndMe(Convert.ToInt32(cimNo));
                DDSupervisor.DataTextField = "Name";
                DDSupervisor.DataValueField = "CimNumber";
                DDSupervisor.DataBind();
                DDSupervisor.SelectedIndex = 0;
            }
            else
            {
                DataSet dsMyDetails = DataHelper.GetMyAcccountDetails(cimNo);
                AccId = Convert.ToInt32(dsMyDetails.Tables[0].Rows[0]["CampaignID"]);
                DDSupervisor.DataSource = DataHelper.GetMyAcccountDetails(cimNo);
                DDSupervisor.DataTextField = "MyFullName";
                DDSupervisor.DataValueField = "CimNumber";
                DDSupervisor.DataBind();
                DDSupervisor.SelectedIndex = 0;
            }
        }

        void LoadCoachee()
        {
            DDCoachee.Items.Clear();
            DDCoachee.Text = "";
            // Clear DDCoachee combobox text when new supervisor is selected (francis.valera/07162018)
            DDCoachee.DataSource = DataHelper.GetSubordinates(Convert.ToInt32(DDSupervisor.SelectedValue));
            DDCoachee.DataTextField = "Name";
            DDCoachee.DataValueField = "CimNumber";
            DDCoachee.DataBind();
            DDCoachee.SelectedIndex = 0;
        }

        protected void DDSupervisor_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {

            LoadCoachee();
        }

        void LoadSessionTypes()
        {
            DDSessionType.DataSource = DataHelper.GetSessionsLookup(); // .GetAllDepartments();
            DDSessionType.DataTextField = "Session Name";
            DDSessionType.DataValueField = "Session Id";
            DDSessionType.DataBind();
        }

        protected void DDSessionType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTopics();
        }

        void LoadTopics()
        {
            DDTopic.DataSource = DataHelper.GetAllTopics(Convert.ToInt32(DDSessionType.SelectedValue));
            DDTopic.DataTextField = "TopicName";
            DDTopic.DataValueField = "TopicId";
            DDTopic.DataBind();
        }

        protected void DDCoachee_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadSessionTypes();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int AccountID = Convert.ToInt32(DDAccount.SelectedValue);
                int SupervisorID = Convert.ToInt32(DDSupervisor.SelectedValue);
                int CoacheeID = Convert.ToInt32(DDCoachee.SelectedValue);
                //int TopicID = Convert.ToInt32(DDTopic.SelectedValue);
                int TopicID = 0;
                if (DDTopic.SelectedValue != "")
                {
                    TopicID = Convert.ToInt32(DDTopic.SelectedValue);
                }
                //DateTime ReviewDateFrom = Convert.ToDateTime(DPReviewDateFrom.SelectedDate);
                //DateTime ReviewDateTo = Convert.ToDateTime(DPReviewDateTo.SelectedDate).AddHours(23).AddMinutes(59).AddSeconds(59);
                //DateTime FollowUpDateFrom = Convert.ToDateTime(DPFollowUpDateFrom.SelectedDate);
                //DateTime FollowUpDateTo = Convert.ToDateTime(DPFollowUpDateTo.SelectedDate).AddHours(23).AddMinutes(59).AddSeconds(59);
                //SearchGrid.DataSource = DataHelper.GetSearchCoachingTickets(AccountID, SupervisorID, CoacheeID, TopicID, ReviewDateFrom, ReviewDateTo, FollowUpDateFrom, FollowUpDateTo);
                //if (DPFollowUpDateFrom.SelectedDate == null || DPFollowUpDateTo.SelectedDate == null)
                //{
                //    SearchGrid.DataSource = DataHelper.GetSearchCoachingTickets(AccountID, SupervisorID, CoacheeID, TopicID, ReviewDateFrom, ReviewDateTo, null, null);
                //}
                //else
                //{
                //    SearchGrid.DataSource = DataHelper.GetSearchCoachingTickets(AccountID, SupervisorID, CoacheeID, TopicID, ReviewDateFrom, ReviewDateTo, FollowUpDateFrom, FollowUpDateTo);
                //}
                // removed followup date from required field in sql query (francis.valera/07232018)
                SearchGrid.Visible = true;
                SearchGrid.DataSource = DataHelper.GetSearchCoachingTickets(AccountID, SupervisorID, CoacheeID, TopicID, DPReviewDateFrom.SelectedDate, DPReviewDateTo.SelectedDate, DPFollowUpDateFrom.SelectedDate, DPFollowUpDateTo.SelectedDate);
                SearchGrid.DataBind();

                DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
                string cim_num = ds.Tables[0].Rows[0][0].ToString();
                int intcim = Convert.ToInt32(cim_num);
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
                string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                if (rolevalue == "QA")
                    saprolefordashboard = "QA";
                else if (rolevalue == "HR")
                    saprolefordashboard = "HR";
                else
                    saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                string retlabels = DataHelper.ReturnLabels(saprolefordashboard);


                DataSet ds1 = null;
                DataAccess ws1 = new DataAccess();
                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(intcim));

                if ((saprolefordashboard == "TL") || (saprolefordashboard == ""))
                {
                    foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {
                        string txt1 = (Item.FindControl("Label1") as Label).Text;
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        Label lbltriad = (Label)Item.FindControl("Label8");
                        lbltriad.Visible = false;
                        HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                        hlink.Visible = false;

                        //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                        }
                        ////added for talk talk 
                        //string Account = null;

                         
                        string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                        if ((formtype == null) || (formtype == ""))
                        {

                            formtype = "0";
                        }
                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                        if (Convert.ToInt32(val2) == 1)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {

                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                        }

                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 4)
                        {

                            hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 5)
                        {
                            hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 6)
                        {
                            hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                        }
                        else
                        {
                            hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }
                    }

                }
                else if (saprolefordashboard == "HR") //hr
                {
                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                    if ((saprole1 == "TL") || (saprole1 == ""))
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {
                            string txt1 = (Item.FindControl("Label1") as Label).Text;
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            Label lbltriad = (Label)Item.FindControl("Label8");
                            lbltriad.Visible = false;
                            HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                            hlink.Visible = false;

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }

                    }
                    else
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {   //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            //string Account = null;

                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);

                            //check if coachee and coacher signed off already
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                            {
                                if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                {
                                    Label lbltriad = (Label)Item.FindControl("Label8");
                                    lbltriad.Visible = true;
                                    HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                    hlink.Visible = true;
                                }
                            }

                         
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                    }

                }
                else if (saprolefordashboard == "QA") //QA
                {
                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                    if ((saprole1 == "TL") || (saprole1 == ""))
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {
                            string txt1 = (Item.FindControl("Label1") as Label).Text;
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            Label lbltriad = (Label)Item.FindControl("Label8");
                            lbltriad.Visible = false;
                            HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                            hlink.Visible = false;

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }
                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }

                    }
                    else
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {   //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            string Account = null;

                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);

                            //check if coachee and coacher signed off already
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                            {
                                Label lbltriad = (Label)Item.FindControl("Label8");
                                lbltriad.Visible = true;
                                HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                hlink.Visible = true;
                            }

                            
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }
                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink


                        }
                    }
                }
                else //if OM and Dir
                {
                    foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                    {   //hyperlink
                        string txt2 = (Item.FindControl("TopicName") as Label).Text;
                        HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                        DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                        string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                        if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                        }
                        else
                        {
                            Label lblreviewtype = (Label)Item.FindControl("Label9");
                            lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                        }
                        //added for talk talk 
                        string Account = null;

                        string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                        Label txtdesc = (Label)Item.FindControl("Label1");

                        string txtdesctolow = txtdesc.Text.ToLower();
                        string txtseatolow = TxtSearch.Text.ToLower();
                        string searchterm = Convert.ToString(TxtSearch.Text);

                        string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                        string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                        //check if coachee and coacher signed off already
                        if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                        {
                            if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                            {
                                Label lbltriad = (Label)Item.FindControl("Label8");
                                lbltriad.Visible = true;
                                HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                hlink.Visible = true;
                                hlink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                // fixed hyperlink of triad coaching review search on OM view (francis/valera 07172018)
                            }
                        }

                        //check if coachee and coacher signed off already

                        string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                        if ((formtype == null) || (formtype == ""))
                        {

                            formtype = "0";
                        }

                        if (Convert.ToInt32(val2) == 1)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {

                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                        }
                        else if (Convert.ToInt32(val2) == 2)
                        {
                            if (Convert.ToInt32(formtype) == 1)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 2)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else if (Convert.ToInt32(formtype) == 3)
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                        }

                        else if (Convert.ToInt32(val2) == 3)
                        {
                            hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 4)
                        {

                            hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 5)
                        {
                            hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 6)
                        {
                            hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }

                        else if (Convert.ToInt32(val2) == 7)
                        {
                            hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                        }
                        else
                        {
                            hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                            //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                            //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                        }
                        //hyperlink


                    }

                }
            }
            catch (Exception em)
            {
                SearchGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", em.Message)));
            }
        }

        protected void BtnReset_Click(object sender, EventArgs e)
        {
            DDAccount.ClearSelection();
            DDSupervisor.ClearSelection();
            DDCoachee.ClearSelection();
            DDTopic.ClearSelection();
            DDSessionType.ClearSelection();
            DPReviewDateFrom.Clear();
            DPReviewDateTo.Clear();
            DPFollowUpDateFrom.Clear();
            DPFollowUpDateTo.Clear();
            LoadAccounts();
            LoadSupervisors();
            LoadCoachee();
            LoadSessionTypes();
            //LoadTopics();
            SearchGrid.Visible = true;
            SearchGrid.DataSource = null;
            SearchGrid.DataBind();
            TxtSearch.Text = "";
            TextBox SearchText = (TextBox)DashboardMyReviewsUserControl1.FindControl("TxtSearch");
            SearchText.Text = "";
        }

        protected void BtnSearchQuery_Click(object sender, EventArgs e)
        {

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            cimNo = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]);

            try
            {
                if (TxtSearch.Text != "")
                {
                    SearchGrid.Visible = true;
                    SearchGrid.DataSource = DataHelper.GetSearchQuery(TxtSearch.Text, cimNo);
                    SearchGrid.DataBind();
                    DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
                    string cim_num = ds.Tables[0].Rows[0][0].ToString();
                    int intcim = Convert.ToInt32(cim_num);

                    DataSet ds1 = null;
                    DataAccess ws1 = new DataAccess();
                    ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(intcim));
                    DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
                    string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                    string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                    if (rolevalue == "QA")
                        saprolefordashboard = "QA";
                    else if (rolevalue == "HR")
                        saprolefordashboard = "HR";
                    else
                        saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                    string retlabels = DataHelper.ReturnLabels(saprolefordashboard);

                    if ((saprolefordashboard == "TL") || (saprolefordashboard == ""))
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {
                            string txt1 = (Item.FindControl("Label1") as Label).Text;
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);


                            Label lbltriad = (Label)Item.FindControl("Label8");
                            lbltriad.Visible = false;
                            HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                            hlink.Visible = false;

                            //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            //string Account = null;

                             
                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }
                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }
                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {
                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }

                    }
                    else if (saprolefordashboard == "HR") //hr
                    {
                        string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                        if ((saprole1 == "TL") || (saprole1 == ""))
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {
                                string txt1 = (Item.FindControl("Label1") as Label).Text;
                                Label txtdesc = (Label)Item.FindControl("Label1");

                                string txtdesctolow = txtdesc.Text.ToLower();
                                string txtseatolow = TxtSearch.Text.ToLower();
                                string searchterm = Convert.ToString(TxtSearch.Text);


                                Label lbltriad = (Label)Item.FindControl("Label8");
                                lbltriad.Visible = false;
                                HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                hlink.Visible = false;

                                //hyperlink
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                

                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }

                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                //hyperlink

                                if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    //description
                                    string str = null;
                                    string[] strArr = null;
                                    int count = 0;
                                    str = txtdesc.Text;
                                    char[] splitchar = { ' ' };
                                    strArr = str.Split(splitchar);
                                    string desc = "";

                                    string str_searchval = null;
                                    string[] str_searchArr = null;
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count = 0; count <= strArr.Length - 1; count++)
                                    {
                                        string val1 = strArr[count].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            desc = desc + " " + strArr[count].ToString();

                                        }

                                    }
                                    txtdesc.Text = desc;
                                    //description

                                    //Strengths 
                                    string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtstrengths = (Label)Item.FindControl("Label6");
                                    string txtstrengthslow = txtstrengths.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrStrengths = null;
                                        int count1 = 0;
                                        str = txtstrengths.Text;
                                        strArrStrengths = str.Split(splitchar);
                                        string strengths = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                        {
                                            string val1 = strArrStrengths[count1].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                strengths = strengths + " " + strArrStrengths[count1].ToString();

                                            }

                                        }
                                        txtstrengths.Text = strengths;


                                    }
                                    //Strengths 

                                    //Opportunity 
                                    string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtOpportunity = (Label)Item.FindControl("Label7");
                                    string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrOpportunity = null;
                                        int count2 = 0;
                                        str = txtOpportunity.Text;
                                        strArrOpportunity = str.Split(splitchar);
                                        string Opportunity = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                        {
                                            string val1 = strArrOpportunity[count2].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                            }

                                        }
                                        txtOpportunity.Text = Opportunity;


                                    }
                                    //Opportunity 
                                    //session name  
                                    string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtSessionname = (Label)Item.FindControl("Label3");
                                    string txtSessionnamelow = txtSessionname.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrSessionname = null;
                                        int count3 = 0;
                                        str = txtSessionname.Text;
                                        strArrSessionname = str.Split(splitchar);
                                        string Sessionname = "";
                                        str_searchval = TxtSearch.Text;


                                        for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                        {
                                            string val1 = strArrSessionname[count3].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                            }

                                        }
                                        txtSessionname.Text = Sessionname;


                                    }
                                    //session name 


                                }
                                else
                                {


                                    string str_searchval = null;
                                    string str = "";
                                    string[] str_searchArr = null;
                                    char[] splitchar = { ' ' };
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    //created on 
                                    string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtCreatedon = (Label)Item.FindControl("Label4");
                                    string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrCreatedon = null;
                                        int count4 = 0;
                                        str = txtCreatedon.Text;
                                        strArrCreatedon = str.Split(splitchar);
                                        string Createdon = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                        {
                                            string val1 = strArrCreatedon[count4].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                            }
                                            else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                        }
                                        txtCreatedon.Text = Createdon;


                                    }
                                    //createdon

                                    //followdate 
                                    string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtfollowdate = (Label)Item.FindControl("Label5");
                                    string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrfollowdate = null;
                                        int count5 = 0;
                                        str = txtfollowdate.Text;
                                        strArrfollowdate = str.Split(splitchar);
                                        string followdate = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                        {
                                            string val1 = strArrfollowdate[count5].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                            }
                                            else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                        }
                                        txtfollowdate.Text = followdate;


                                    }
                                    //followdate
                                }
                            }
                        }
                        else
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {   //hyperlink
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                //added for talk talk 
                                string Account = null;

                                string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtdesc = (Label)Item.FindControl("Label1");

                                string txtdesctolow = txtdesc.Text.ToLower();
                                string txtseatolow = TxtSearch.Text.ToLower();
                                string searchterm = Convert.ToString(TxtSearch.Text);

                                //check if coachee and coacher signed off already
                                if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                                {
                                    if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                    {
                                        Label lbltriad = (Label)Item.FindControl("Label8");
                                        lbltriad.Visible = true;
                                        HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                        hlink.Visible = true;
                                    }
                                }

                                //check if coachee and coacher signed off already


                                 
                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }

                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                //hyperlink

                                if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {



                                    //description
                                    string str = null;
                                    string[] strArr = null;
                                    int count = 0;
                                    str = txtdesc.Text;
                                    char[] splitchar = { ' ' };
                                    strArr = str.Split(splitchar);
                                    string desc = "";

                                    string str_searchval = null;
                                    string[] str_searchArr = null;
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count = 0; count <= strArr.Length - 1; count++)
                                    {
                                        string val1 = strArr[count].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            desc = desc + " " + strArr[count].ToString();

                                        }

                                    }
                                    txtdesc.Text = desc;
                                    //description

                                    //Strengths 
                                    string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtstrengths = (Label)Item.FindControl("Label6");
                                    string txtstrengthslow = txtstrengths.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrStrengths = null;
                                        int count1 = 0;
                                        str = txtstrengths.Text;
                                        strArrStrengths = str.Split(splitchar);
                                        string strengths = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                        {
                                            string val1 = strArrStrengths[count1].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                strengths = strengths + " " + strArrStrengths[count1].ToString();

                                            }

                                        }
                                        txtstrengths.Text = strengths;


                                    }
                                    //Strengths 

                                    //Opportunity 
                                    string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtOpportunity = (Label)Item.FindControl("Label7");
                                    string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrOpportunity = null;
                                        int count2 = 0;
                                        str = txtOpportunity.Text;
                                        strArrOpportunity = str.Split(splitchar);
                                        string Opportunity = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                        {
                                            string val1 = strArrOpportunity[count2].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                            }

                                        }
                                        txtOpportunity.Text = Opportunity;


                                    }
                                    //Opportunity 
                                    //session name  
                                    string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtSessionname = (Label)Item.FindControl("Label3");
                                    string txtSessionnamelow = txtSessionname.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrSessionname = null;
                                        int count3 = 0;
                                        str = txtSessionname.Text;
                                        strArrSessionname = str.Split(splitchar);
                                        string Sessionname = "";
                                        str_searchval = TxtSearch.Text;


                                        for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                        {
                                            string val1 = strArrSessionname[count3].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                            }

                                        }
                                        txtSessionname.Text = Sessionname;


                                    }
                                    //session name 


                                }
                                else
                                {


                                    string str_searchval = null;
                                    string str = "";
                                    string[] str_searchArr = null;
                                    char[] splitchar = { ' ' };
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    //created on 
                                    string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtCreatedon = (Label)Item.FindControl("Label4");
                                    string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrCreatedon = null;
                                        int count4 = 0;
                                        str = txtCreatedon.Text;
                                        strArrCreatedon = str.Split(splitchar);
                                        string Createdon = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                        {
                                            string val1 = strArrCreatedon[count4].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                            }
                                            else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                        }
                                        txtCreatedon.Text = Createdon;


                                    }
                                    //createdon

                                    //followdate 
                                    string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtfollowdate = (Label)Item.FindControl("Label5");
                                    string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrfollowdate = null;
                                        int count5 = 0;
                                        str = txtfollowdate.Text;
                                        strArrfollowdate = str.Split(splitchar);
                                        string followdate = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                        {
                                            string val1 = strArrfollowdate[count5].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                            }
                                            else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                        }
                                        txtfollowdate.Text = followdate;


                                    }
                                    //followdate
                                }
                            }
                        }

                    }
                    else if (saprolefordashboard == "QA") //QA
                    {
                        string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                        if ((saprole1 == "TL") || (saprole1 == ""))
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {
                                string txt1 = (Item.FindControl("Label1") as Label).Text;
                                Label txtdesc = (Label)Item.FindControl("Label1");

                                string txtdesctolow = txtdesc.Text.ToLower();
                                string txtseatolow = TxtSearch.Text.ToLower();
                                string searchterm = Convert.ToString(TxtSearch.Text);


                                Label lbltriad = (Label)Item.FindControl("Label8");
                                lbltriad.Visible = false;
                                HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                hlink.Visible = false;

                                //hyperlink
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                //added for talk talk 
                                 
                                
                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }

                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                

                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                //hyperlink

                                if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    //description
                                    string str = null;
                                    string[] strArr = null;
                                    int count = 0;
                                    str = txtdesc.Text;
                                    char[] splitchar = { ' ' };
                                    strArr = str.Split(splitchar);
                                    string desc = "";

                                    string str_searchval = null;
                                    string[] str_searchArr = null;
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count = 0; count <= strArr.Length - 1; count++)
                                    {
                                        string val1 = strArr[count].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            desc = desc + " " + strArr[count].ToString();

                                        }

                                    }
                                    txtdesc.Text = desc;
                                    //description

                                    //Strengths 
                                    string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtstrengths = (Label)Item.FindControl("Label6");
                                    string txtstrengthslow = txtstrengths.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrStrengths = null;
                                        int count1 = 0;
                                        str = txtstrengths.Text;
                                        strArrStrengths = str.Split(splitchar);
                                        string strengths = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                        {
                                            string val1 = strArrStrengths[count1].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                strengths = strengths + " " + strArrStrengths[count1].ToString();

                                            }

                                        }
                                        txtstrengths.Text = strengths;


                                    }
                                    //Strengths 

                                    //Opportunity 
                                    string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtOpportunity = (Label)Item.FindControl("Label7");
                                    string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrOpportunity = null;
                                        int count2 = 0;
                                        str = txtOpportunity.Text;
                                        strArrOpportunity = str.Split(splitchar);
                                        string Opportunity = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                        {
                                            string val1 = strArrOpportunity[count2].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                            }

                                        }
                                        txtOpportunity.Text = Opportunity;


                                    }
                                    //Opportunity 
                                    //session name  
                                    string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtSessionname = (Label)Item.FindControl("Label3");
                                    string txtSessionnamelow = txtSessionname.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrSessionname = null;
                                        int count3 = 0;
                                        str = txtSessionname.Text;
                                        strArrSessionname = str.Split(splitchar);
                                        string Sessionname = "";
                                        str_searchval = TxtSearch.Text;


                                        for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                        {
                                            string val1 = strArrSessionname[count3].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                            }

                                        }
                                        txtSessionname.Text = Sessionname;


                                    }
                                    //session name 


                                }
                                else
                                {


                                    string str_searchval = null;
                                    string str = "";
                                    string[] str_searchArr = null;
                                    char[] splitchar = { ' ' };
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    //created on 
                                    string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtCreatedon = (Label)Item.FindControl("Label4");
                                    string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrCreatedon = null;
                                        int count4 = 0;
                                        str = txtCreatedon.Text;
                                        strArrCreatedon = str.Split(splitchar);
                                        string Createdon = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                        {
                                            string val1 = strArrCreatedon[count4].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                            }
                                            else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                        }
                                        txtCreatedon.Text = Createdon;


                                    }
                                    //createdon

                                    //followdate 
                                    string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtfollowdate = (Label)Item.FindControl("Label5");
                                    string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrfollowdate = null;
                                        int count5 = 0;
                                        str = txtfollowdate.Text;
                                        strArrfollowdate = str.Split(splitchar);
                                        string followdate = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                        {
                                            string val1 = strArrfollowdate[count5].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                            }
                                            else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                        }
                                        txtfollowdate.Text = followdate;


                                    }
                                    //followdate
                                }
                            }
                        }
                        else
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {   //hyperlink
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                //added for talk talk 
                                string Account = null;

                                string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtdesc = (Label)Item.FindControl("Label1");

                                string txtdesctolow = txtdesc.Text.ToLower();
                                string txtseatolow = TxtSearch.Text.ToLower();
                                string searchterm = Convert.ToString(TxtSearch.Text);

                                //check if coachee and coacher signed off already
                                if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"].ToString().Equals("")))
                                {
                                    if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                    {
                                        Label lbltriad = (Label)Item.FindControl("Label8");
                                        lbltriad.Visible = true;
                                        HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                        hlink.Visible = true;
                                    }
                                }

                                //check if coachee and coacher signed off already

                                 
                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }
                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                //hyperlink

                                if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {



                                    //description
                                    string str = null;
                                    string[] strArr = null;
                                    int count = 0;
                                    str = txtdesc.Text;
                                    char[] splitchar = { ' ' };
                                    strArr = str.Split(splitchar);
                                    string desc = "";

                                    string str_searchval = null;
                                    string[] str_searchArr = null;
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count = 0; count <= strArr.Length - 1; count++)
                                    {
                                        string val1 = strArr[count].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            desc = desc + " " + strArr[count].ToString();

                                        }

                                    }
                                    txtdesc.Text = desc;
                                    //description

                                    //Strengths 
                                    string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtstrengths = (Label)Item.FindControl("Label6");
                                    string txtstrengthslow = txtstrengths.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrStrengths = null;
                                        int count1 = 0;
                                        str = txtstrengths.Text;
                                        strArrStrengths = str.Split(splitchar);
                                        string strengths = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                        {
                                            string val1 = strArrStrengths[count1].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                strengths = strengths + " " + strArrStrengths[count1].ToString();

                                            }

                                        }
                                        txtstrengths.Text = strengths;


                                    }
                                    //Strengths 

                                    //Opportunity 
                                    string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtOpportunity = (Label)Item.FindControl("Label7");
                                    string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrOpportunity = null;
                                        int count2 = 0;
                                        str = txtOpportunity.Text;
                                        strArrOpportunity = str.Split(splitchar);
                                        string Opportunity = "";
                                        str_searchval = TxtSearch.Text;
                                        str_searchArr = toup_textsearch.Split(splitchar);


                                        for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                        {
                                            string val1 = strArrOpportunity[count2].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                            }

                                        }
                                        txtOpportunity.Text = Opportunity;


                                    }
                                    //Opportunity 
                                    //session name  
                                    string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtSessionname = (Label)Item.FindControl("Label3");
                                    string txtSessionnamelow = txtSessionname.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrSessionname = null;
                                        int count3 = 0;
                                        str = txtSessionname.Text;
                                        strArrSessionname = str.Split(splitchar);
                                        string Sessionname = "";
                                        str_searchval = TxtSearch.Text;


                                        for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                        {
                                            string val1 = strArrSessionname[count3].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                            }
                                            else
                                            {
                                                Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                            }

                                        }
                                        txtSessionname.Text = Sessionname;


                                    }
                                    //session name 


                                }
                                else
                                {


                                    string str_searchval = null;
                                    string str = "";
                                    string[] str_searchArr = null;
                                    char[] splitchar = { ' ' };
                                    str_searchval = TxtSearch.Text;
                                    string toup_textsearch = str_searchval.ToLower();
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    //created on 
                                    string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtCreatedon = (Label)Item.FindControl("Label4");
                                    string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrCreatedon = null;
                                        int count4 = 0;
                                        str = txtCreatedon.Text;
                                        strArrCreatedon = str.Split(splitchar);
                                        string Createdon = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                        {
                                            string val1 = strArrCreatedon[count4].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                            }
                                            else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                        }
                                        txtCreatedon.Text = Createdon;


                                    }
                                    //createdon

                                    //followdate 
                                    string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                    Label txtfollowdate = (Label)Item.FindControl("Label5");
                                    string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                    //                    string txtseatolow = textsearch.Text.ToLower();
                                    if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                    {
                                        string[] strArrfollowdate = null;
                                        int count5 = 0;
                                        str = txtfollowdate.Text;
                                        strArrfollowdate = str.Split(splitchar);
                                        string followdate = "";
                                        str_searchval = TxtSearch.Text;

                                        for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                        {
                                            string val1 = strArrfollowdate[count5].ToLower();
                                            if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                            {
                                                followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                            }
                                            else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                        }
                                        txtfollowdate.Text = followdate;


                                    }
                                    //followdate
                                }
                            }
                        }
                    }
                    else //if OM and Dir
                    {
                        foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                        {   //hyperlink
                            string txt2 = (Item.FindControl("TopicName") as Label).Text;
                            HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                            if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                            }
                            else
                            {
                                Label lblreviewtype = (Label)Item.FindControl("Label9");
                                lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                            }
                            //added for talk talk 
                            string Account = null;

                            string txt1 = (Item.FindControl("Label1") as Label).Text;//(Label)itm.FindControl("Label1");
                            Label txtdesc = (Label)Item.FindControl("Label1");

                            string txtdesctolow = txtdesc.Text.ToLower();
                            string txtseatolow = TxtSearch.Text.ToLower();
                            string searchterm = Convert.ToString(TxtSearch.Text);

                            string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                            string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));

                            //check if coachee and coacher signed off already
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"].ToString().Equals("")))
                            {
                                if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                {
                                    Label lbltriad = (Label)Item.FindControl("Label8");
                                    lbltriad.Visible = true;
                                    HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                    hlink.Visible = true;
                                    hlink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                }
                            }

                            string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                            if ((formtype == null) || (formtype == ""))
                            {

                                formtype = "0";
                            }

                            if (Convert.ToInt32(val2) == 1)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {

                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                            }
                            else if (Convert.ToInt32(val2) == 2)
                            {
                                if (Convert.ToInt32(formtype) == 1)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 2)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else if (Convert.ToInt32(formtype) == 3)
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                            }

                            else if (Convert.ToInt32(val2) == 3)
                            {
                                hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 4)
                            {

                                hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 5)
                            {
                                hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 6)
                            {
                                hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            else if (Convert.ToInt32(val2) == 7)
                            {
                                hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                            }
                            else
                            {
                                hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                            }

                            //hyperlink

                            if (txtdesctolow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                            {



                                //description
                                string str = null;
                                string[] strArr = null;
                                int count = 0;
                                str = txtdesc.Text;
                                char[] splitchar = { ' ' };
                                strArr = str.Split(splitchar);
                                string desc = "";

                                string str_searchval = null;
                                string[] str_searchArr = null;
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                for (count = 0; count <= strArr.Length - 1; count++)
                                {
                                    string val1 = strArr[count].ToLower();
                                    if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                    {
                                        desc = desc + " " + "<b>" + strArr[count].ToString() + "</b>";

                                    }
                                    else
                                    {
                                        desc = desc + " " + strArr[count].ToString();

                                    }

                                }
                                txtdesc.Text = desc;
                                //description

                                //Strengths 
                                string txt6 = (Item.FindControl("Label6") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtstrengths = (Label)Item.FindControl("Label6");
                                string txtstrengthslow = txtstrengths.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtstrengthslow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrStrengths = null;
                                    int count1 = 0;
                                    str = txtstrengths.Text;
                                    strArrStrengths = str.Split(splitchar);
                                    string strengths = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count1 = 0; count1 <= strArrStrengths.Length - 1; count1++)
                                    {
                                        string val1 = strArrStrengths[count1].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            strengths = strengths + " " + "<b>" + strArrStrengths[count1].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            strengths = strengths + " " + strArrStrengths[count1].ToString();

                                        }

                                    }
                                    txtstrengths.Text = strengths;


                                }
                                //Strengths 

                                //Opportunity 
                                string txt7 = (Item.FindControl("Label7") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtOpportunity = (Label)Item.FindControl("Label7");
                                string txtOpportunitylow = txtOpportunity.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtOpportunitylow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrOpportunity = null;
                                    int count2 = 0;
                                    str = txtOpportunity.Text;
                                    strArrOpportunity = str.Split(splitchar);
                                    string Opportunity = "";
                                    str_searchval = TxtSearch.Text;
                                    str_searchArr = toup_textsearch.Split(splitchar);


                                    for (count2 = 0; count2 <= strArrOpportunity.Length - 1; count2++)
                                    {
                                        string val1 = strArrOpportunity[count2].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Opportunity = Opportunity + " " + "<b>" + strArrOpportunity[count2].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Opportunity = Opportunity + " " + strArrOpportunity[count2].ToString();

                                        }

                                    }
                                    txtOpportunity.Text = Opportunity;


                                }
                                //Opportunity 
                                //session name  
                                string txt3 = (Item.FindControl("Label3") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtSessionname = (Label)Item.FindControl("Label3");
                                string txtSessionnamelow = txtSessionname.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtSessionnamelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrSessionname = null;
                                    int count3 = 0;
                                    str = txtSessionname.Text;
                                    strArrSessionname = str.Split(splitchar);
                                    string Sessionname = "";
                                    str_searchval = TxtSearch.Text;


                                    for (count3 = 0; count3 <= strArrSessionname.Length - 1; count3++)
                                    {
                                        string val1 = strArrSessionname[count3].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Sessionname = Sessionname + " " + "<b>" + strArrSessionname[count3].ToString() + "</b>";

                                        }
                                        else
                                        {
                                            Sessionname = Sessionname + " " + strArrSessionname[count3].ToString();

                                        }

                                    }
                                    txtSessionname.Text = Sessionname;


                                }
                                //session name 


                            }
                            else
                            {


                                string str_searchval = null;
                                string str = "";
                                string[] str_searchArr = null;
                                char[] splitchar = { ' ' };
                                str_searchval = TxtSearch.Text;
                                string toup_textsearch = str_searchval.ToLower();
                                str_searchArr = toup_textsearch.Split(splitchar);


                                //created on 
                                string txt4 = (Item.FindControl("Label4") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtCreatedon = (Label)Item.FindControl("Label4");
                                string txtCreatedonlow = txtCreatedon.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txtCreatedonlow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrCreatedon = null;
                                    int count4 = 0;
                                    str = txtCreatedon.Text;
                                    strArrCreatedon = str.Split(splitchar);
                                    string Createdon = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count4 = 0; count4 <= strArrCreatedon.Length - 1; count4++)
                                    {
                                        string val1 = strArrCreatedon[count4].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            Createdon = Createdon + " " + "<b>" + strArrCreatedon[count4].ToString() + "</b>";

                                        }
                                        else { Createdon = Createdon + " " + strArrCreatedon[count4].ToString(); }

                                    }
                                    txtCreatedon.Text = Createdon;


                                }
                                //createdon

                                //followdate 
                                string txt5 = (Item.FindControl("Label5") as Label).Text;//(Label)itm.FindControl("Label1");
                                Label txtfollowdate = (Label)Item.FindControl("Label5");
                                string txttxtfollowdatelow = txtfollowdate.Text.ToLower();
                                //                    string txtseatolow = textsearch.Text.ToLower();
                                if (txttxtfollowdatelow.Contains(Convert.ToString(txtseatolow)))//(txtdesc.Text.Contains(Convert.ToString(textsearch.Text)))
                                {
                                    string[] strArrfollowdate = null;
                                    int count5 = 0;
                                    str = txtfollowdate.Text;
                                    strArrfollowdate = str.Split(splitchar);
                                    string followdate = "";
                                    str_searchval = TxtSearch.Text;

                                    for (count5 = 0; count5 <= strArrfollowdate.Length - 1; count5++)
                                    {
                                        string val1 = strArrfollowdate[count5].ToLower();
                                        if (str_searchArr.Contains(Convert.ToString(val1.ToLower())))  //(!Convert.ToString(strArr[count].ToUpper()).Contains(Convert.ToString(searchterm))) //( || (!Convert.ToString(strArr[count].ToUpper()).StartsWith(Convert.ToString(toup_textsearch)))) //!strArr[count].Equals(Convert.ToString(textsearch.Text)))//textsearch.Text)))
                                        {
                                            followdate = followdate + " " + "<b>" + strArrfollowdate[count5].ToString() + "</b>";

                                        }
                                        else { followdate = followdate + " " + strArrfollowdate[count5].ToString(); }

                                    }
                                    txtfollowdate.Text = followdate;


                                }
                                //followdate
                            }
                        }

                    }
                }
                else
                {
                    string ModalLabel = "Searching Failed, input value first before searching";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                }
            }

            catch (Exception ex)
            {
                string ModalLabel = "Searching Failed, " + ex.Message;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }


        private static DataTable GetData(string q)
        {

            DataSet ds1 = DataHelper.GetAccountsLookupSearch(q);

            DataTable data = ds1.Tables[0];


            return data;
        }

        private static string GetStatusMessage(int offset, int total)
        {
            if (total <= 0)
                return "No matches";

            return String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total);
        }


        protected void DDAccount_ItemsRequested(object sender, Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs e)
        {
            DataTable data = GetData(e.Text);

            int itemOffset = e.NumberOfItems;
            int endOffset = Math.Min(itemOffset + ItemsPerRequest, data.Rows.Count);
            e.EndOfItems = endOffset == data.Rows.Count;

            for (int i = itemOffset; i < endOffset; i++)
            {
                DDAccount.Items.Add(new RadComboBoxItem(data.Rows[i]["Account"].ToString(), data.Rows[i]["AccountID"].ToString()));
            }

            e.Message = GetStatusMessage(endOffset, data.Rows.Count);
        }

        protected void BtnSearchCIM_Click(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            cimNo = TxtSearchCIM.Text == "" ? Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["CIM_Number"]) : Convert.ToInt32(TxtSearchCIM.Text);

            LoadAccounts();
            LoadSupervisors();
            LoadCoachee();
            LoadSessionTypes();
        }

        protected void SearchGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    if (TxtSearch.Text != "")
                    {
                        SearchGrid.Visible = true;
                        SearchGrid.DataSource = DataHelper.GetSearchQuery(TxtSearch.Text, cimNo);
                        //SearchGrid.DataBind();

                        DataSet ds1 = null;
                        DataAccess ws1 = new DataAccess();
                        ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(cimNo));


                        DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
                        string cim_num = ds.Tables[0].Rows[0][0].ToString();
                        int intcim = Convert.ToInt32(cim_num);
                        DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
                        string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                        string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                        if (rolevalue == "QA")
                            saprolefordashboard = "QA";
                        else if (rolevalue == "HR")
                            saprolefordashboard = "HR";
                        else
                            saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                        string retlabels = DataHelper.ReturnLabels(saprolefordashboard);

                        if ((saprolefordashboard == "TL") || (saprolefordashboard == ""))
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {
                                Label lbltriad = (Label)Item.FindControl("Label8");
                                lbltriad.Visible = false;
                                HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                hlink.Visible = false;

                                //hyperlink view 
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                //added for talk talk 
                                //string Account = null;
                                 
                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }
                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                
                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                //hyperlink
                            }

                        }

                        else
                        {
                            foreach (GridDataItem Item in SearchGrid.MasterTableView.Items)
                            {
                                string txt2 = (Item.FindControl("TopicName") as Label).Text;
                                HyperLink hlink1 = (HyperLink)Item.FindControl("link1");
                                DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(txt2));
                                //triad hyperlink
                                if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                {
                                    if ((cim_num != ds_get_review.Tables[0].Rows[0]["coacheeid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["supervisorid"].ToString()) && (cim_num != ds_get_review.Tables[0].Rows[0]["createdby"].ToString()))
                                    {
                                        Label lbltriad = (Label)Item.FindControl("Label8");
                                        lbltriad.Visible = true;
                                        HyperLink hlink = (HyperLink)Item.FindControl("reviewlink");
                                        hlink.Visible = true;
                                    }
                                }
                                //triad hyperlink

                                //hyperlink view 

                                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;
                                if (ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString() != "3")
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString();
                                }
                                else
                                {
                                    Label lblreviewtype = (Label)Item.FindControl("Label9");
                                    lblreviewtype.Text = ds_get_review.Tables[0].Rows[0]["reviewtypename"].ToString() + " for ticket id : " + ds_get_review.Tables[0].Rows[0]["masscoachingid"].ToString();
                                }
                                 
                                string formtype = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                                if ((formtype == null) || (formtype == ""))
                                {

                                    formtype = "0";
                                }
                                string enctxt = DataHelper.Encrypt(Convert.ToInt32(txt2));
                                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(val2));
                

                                if (Convert.ToInt32(val2) == 1)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {

                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                }
                                else if (Convert.ToInt32(val2) == 2)
                                {
                                    if (Convert.ToInt32(formtype) == 1)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 2)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else if (Convert.ToInt32(formtype) == 3)
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }
                                    else
                                    {
                                        hlink1.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                        //hLinkname.NavigateUrl = "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                        //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                    }

                                }

                                else if (Convert.ToInt32(val2) == 3)
                                {
                                    hlink1.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 4)
                                {

                                    hlink1.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 5)
                                {
                                    hlink1.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 6)
                                {
                                    hlink1.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }

                                else if (Convert.ToInt32(val2) == 7)
                                {
                                    hlink1.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;

                                }
                                else
                                {
                                    hlink1.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt + "&ReviewType=" + enctxt1;
                                    //hLinkname.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                                    //hLinkname.ToolTip = "Coaching Ticket : " + hLink.Text;
                                }
                                //hyperlink

                            }

                        }

                    }
                    else
                    {
                        int AccountID = Convert.ToInt32(DDAccount.SelectedValue);
                        int SupervisorID = Convert.ToInt32(DDSupervisor.SelectedValue);
                        int CoacheeID = Convert.ToInt32(DDCoachee.SelectedValue);
                        int TopicID = Convert.ToInt32(DDTopic.SelectedValue);
                        DateTime ReviewDateFrom = Convert.ToDateTime(DPReviewDateFrom.SelectedDate);
                        DateTime ReviewDateTo = Convert.ToDateTime(DPReviewDateTo.SelectedDate).AddHours(23).AddMinutes(59).AddSeconds(59);
                        DateTime FollowUpDateFrom = Convert.ToDateTime(DPFollowUpDateFrom.SelectedDate);
                        DateTime FollowUpDateTo = Convert.ToDateTime(DPFollowUpDateTo.SelectedDate).AddHours(23).AddMinutes(59).AddSeconds(59);

                        SearchGrid.Visible = true;
                        SearchGrid.DataSource = DataHelper.GetSearchCoachingTickets(AccountID, SupervisorID, CoacheeID, TopicID, ReviewDateFrom, ReviewDateTo, FollowUpDateFrom, FollowUpDateTo);

                    }

                }
                catch (Exception ex)
                {
                    SearchGrid.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        // added for encrypted reviewid on search review (francis.valera/07122018)
        // added reviewtype on search review (francis.valera/07162018)
        protected string enc_text(object CoachingTicket, object ReviewID)
        {
            string ka = "~/AddTriadCoaching.aspx?CoachingTicket=";
            return ka + DataHelper.Encrypt(Convert.ToInt32(CoachingTicket)) + "&ReviewType=" + ReviewID;
        }

    }
}