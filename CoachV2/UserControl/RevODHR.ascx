﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevODHR.ascx.cs" Inherits="CoachV2.UserControl.RevODHR" %>

<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        <div class="panel-heading">
            <h4 class="panel-title">
                Description<span class="glyphicon glyphicon-triangle-top triangletop"></span>
            </h4>
        </div>
    </a>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body">
            <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
            </telerik:RadTextBox> 
        </div>
    </div>
</div>
<div class="panel panel-custom" id="PerfResult" runat="server">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <div class="panel-heading">
            <h4 class="panel-title">
                Performance Result <span class="glyphicon glyphicon-triangle-top triangletop">
                </span>
            </h4>
        </div>
    </a>
    <div id="collapseThree" class="panel-collapse collapse in">
        <div class="panel-body">
              <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true">
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                    </Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <div>
                &nbsp;</div> 
        </div>
        <asp:Panel ID="CNPRPanel" runat="server">
            <div id="CNPR" runat="server">
                <div id="collapseCNPR" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                            Previous Coaching Notes</label><br />
                        <div style="height: 5px">
                        </div>
                        <div>
                            <div class="panel-body">
                                <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                    <MasterTableView>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Session">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Topic">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="true">
                                        </Scrolling>
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
<asp:Panel ID="PanelStOpps" runat="server">
    <div id="Div1" runat="server">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Strengths<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                    </telerik:RadTextBox>
                </div>
            </div>
        </div>
        <div style="height: 5px">
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Opportunities<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseFive" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                    </telerik:RadTextBox>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
        <div class="panel-heading">
            <h4 class="panel-title">
                Commitment<span class="glyphicon glyphicon-triangle-top triangletop"></span>
            </h4>
        </div>
    </a>
    <div id="collapseSix" class="panel-collapse collapse in">
        <div class="panel-body">
<%--            <telerik:RadGrid ID="grd_Commitment" runat="server" AutoGenerateColumns="false" CssClass="RemoveBorders"
                BorderStyle="None" Height="700px">
                <MasterTableView>
                    <ColumnGroups>
                        <telerik:GridColumnGroup Name="Grow" HeaderText="Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below"
                            HeaderStyle-HorizontalAlign="Center" />
                    </ColumnGroups>
                    <HeaderStyle Width="102px" />
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Goal" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="RadTextBox2" runat="server" Text="What do you want to achieve?"
                                    TextMode="MultiLine" Enabled="false" Width="100%" Height="100%" Rows="10">
                                </telerik:RadTextBox>
                                <telerik:RadTextBox ID="txtGoal" runat="server" TextMode="MultiLine" Width="100%"
                                    Height="100%" Text='<%# Eval("Goal_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Reality" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="RadTextBox3" runat="server" Text="Where are you know? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?"
                                    Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                </telerik:RadTextBox>
                                <telerik:RadTextBox ID="txtReality" runat="server" TextMode="MultiLine" Width="100%"
                                    Height="100%" Text='<%# Eval("Reality_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Options" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="RadTextBox4" runat="server" Text="What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that?"
                                    Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                </telerik:RadTextBox>
                                <telerik:RadTextBox ID="txtOptions" runat="server" TextMode="MultiLine" Width="100%"
                                    Height="100%" Text='<%# Eval("Options_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                Option 1
                                                
                                                Option 2
                                                
                                                Option 3">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Way Forward" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="RadTextBox5" runat="server" Text="What option do you think will work the best? What will you do and when? What support and resources do you need?"
                                    Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                </telerik:RadTextBox>
                                <telerik:RadTextBox ID="txtWayForward" runat="server" TextMode="MultiLine" Width="100%"
                                    Height="100%" Text='<%# Eval("WayForward_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    <Resizing AllowResizeToFit="True" />
                </ClientSettings>
            </telerik:RadGrid>--%>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center">
                        Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below
                    </h5>
                </div>
                <div id="no-more-tables">
                    <table class="col-sm-12 table-bordered table-striped table-condensed cf">
                		<thead class="cf">
                			<tr>
                				<th align="center">Goal</th>
                				<th align="center">Reality</th>
                				<th align="center">Options</th>
                				<th align="center">Way Forward</th>
                			</tr>
                		</thead>
                		<tbody>
                			<tr>
                				<td data-title="Goal"><telerik:RadTextBox ID="RadTextBox1" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox2" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                				<td data-title="Reality"><telerik:RadTextBox ID="RadTextBox3" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox4" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                				<td data-title="Options"><telerik:RadTextBox ID="RadTextBox5" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox6" ReadOnly="true" runat="server" Placeholder="(Free-form field)

Option 1
                                                
Option 2
                                                
Option 3" TextMode="MultiLine" Rows="15"></telerik:RadTextBox></td>
                				<td data-title="Way Forward"><telerik:RadTextBox ID="RadTextBox7" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox8" ReadOnly="true" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15"></telerik:RadTextBox></td>
                			</tr>
            		</tbody>
            	</table>
            </div>
        </div>
        </div>
        <asp:Panel ID="PanelPreviousCommitment" runat="server">
            <div id="Div2" runat="server">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="Previous Perfomance Results">
                                <b>Previous Commitment</b></label>
                            <span></span>
                        </div>
                    </div>
                    <div class="panel-zero" id="PreviousCommitment" runat="server">
                      
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDiv3">
        <div class="panel-heading">
            <h4 class="panel-title">
                Coach Feedback <span class="glyphicon glyphicon-triangle-top triangletop">
                </span>
            </h4>
        </div>
    </a>
    <div id="collapseDiv3" class="panel-collapse collapse in">
        <div class="panel-body">
            <telerik:RadTextBox ID="RadCoacherFeedback" runat="server" class="form-control" placeholder="Description"
                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
            </telerik:RadTextBox>
        </div>
    </div>
</div>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
        <div class="panel-heading">
            <h4 class="panel-title">
                Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
            </h4>
        </div>
    </a>
    <div id="collapseTen" class="panel-collapse collapse in">
        <div class="panel-body">
            <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                OnItemDataBound="RadDocumentationReview_onItemDatabound" CssClass="RemoveBorders"
                BorderStyle="None" RenderMode="Auto">
                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Item Number">
                            <ItemTemplate>
                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                            <ItemTemplate>
                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                            <ItemTemplate>
                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" />
                </ClientSettings>
            </telerik:RadGrid>
            <div>
                &nbsp;
            </div>
            <div class="col-sm-12">
            </div>
        </div>
    </div>
</div>
