﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using CoachV2.AppCode;
using System.IO;

namespace CoachV2.UserControl
{
    public partial class RevDefaultHR : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
               
                if (Page.Request.QueryString["Coachingticket"] != null)
                {
                    int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 2220;
                    GetMassCoachingDetails(CoachingTicket);
                    LoadDocumentations(CoachingTicket);
                    LoadKPIReview(CoachingTicket);
                    LoadPreviousCoachingNotes(CoachingTicket);
                    LoadPreviousPerformanceResults(CoachingTicket);
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        public void LoadPreviousCoachingNotes(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                grd_Coaching_Notes.DataSource = ds;
                grd_Coaching_Notes.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    CheckBox PrevCNNotes = this.Parent.FindControl("CheckBox2") as CheckBox;
                    PrevCNNotes.Checked = true;
                    PrevCNNotes.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);
                RadGrid1.DataSource = ds;
                RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }      
        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (ds.Tables[0].Rows.Count > 0)
                {                    
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    RadStrengths.Text = ds.Tables[0].Rows[0]["Strengths"].ToString();
                    RadOpportunities.Text = ds.Tables[0].Rows[0]["Opportunity"].ToString();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        public void LoadDocumentations(int CoachingTicket)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(CoachingTicket), 3);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    RadGridDocumentation.DataSource = ds;
                    RadGridDocumentation.Rebind();
                }
                else
                {
                    RadGridDocumentation.DataSource = string.Empty;
                    RadGridDocumentation.Rebind();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        public void LoadPreviousPerformanceResults(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPR.DataSource = ds;
                RadGridPR.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    CheckBox PrevCNNotes = this.Parent.FindControl("CheckBox1") as CheckBox;
                    PrevCNNotes.Checked = true;
                    PrevCNNotes.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
    }
}