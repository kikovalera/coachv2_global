﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CoachV2.UserControl
{
    public partial class ProfileExtraInfoUserCtrl : System.Web.UI.UserControl
    {
        string cim_num, email;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tab"] != "" && Request.QueryString["tab"] == "profile")
                fake_title.Visible = false;


            if (Page.Request.QueryString["CIM"] != null)
            {
                cim_num = Page.Request.QueryString["CIM"];
                RadLinkButton1.Visible = false;

                DataSet dsx = DataHelper.GetUserInfoViaCIMNo(cim_num);

                email = dsx.Tables[0].Rows[0]["Email"].ToString();
            }

            else
            {
                DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                cim_num = dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

                email = HttpContext.Current.User.Identity.Name.Split('|')[0];

                DataSet ds = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

                if (ds.Tables[0].Rows.Count == 0)
                    RadLinkButton1.Visible = false;
            }

            if (!IsPostBack)
            {

                DataSet ds = DataHelper.GetUserInfo(email);
                RadAdditionalInfo.Text = ds.Tables[0].Rows[0]["AdditionalInfo"].ToString();
            }
        }
    }
}