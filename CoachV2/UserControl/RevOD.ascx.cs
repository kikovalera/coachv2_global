﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;

namespace CoachV2.UserControl
{
    public partial class RevOD : System.Web.UI.UserControl
    {
        string retrole;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(cim_num));
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CIMNumber);
                string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                if (rolevalue == "QA")
                    saprolefordashboard = "QA";
                else if (rolevalue == "HR")
                    saprolefordashboard = "HR";
                else
                    saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(cim_num));

                string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
                retrole = "";
                if (saprolefordashboard == "TL")
                {
                    retrole = "TL";
                }
                else if (saprolefordashboard == "OM")
                {
                    retrole = "OM";
                }
                else if (saprolefordashboard == "Dir")
                {
                    retrole = "OM";
                }
                else if (saprolefordashboard == "QA")
                {


                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(cim_num));
                    if ((saprole1 == "OM") || (saprole1 == "Dir"))
                    {
                        retrole = "OM";
                    }
                    else
                    {

                        retrole = "";
                    }
                }
                else if (saprolefordashboard == "HR")
                {


                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(cim_num));

                    if ((saprole1 == "OM") || (saprole1 == "Dir"))
                    {
                        retrole = "OM";
                    }
                    else
                    {
                        retrole = "";
                    }
                }
                else
                {
                    retrole = "";
                }

                if (Page.Request.QueryString["Coachingticket"] != null)
                {                    
                    //Decrypt
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    int CoachingTicket = Convert.ToInt32(decrypt);
                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    
                    //int CoachingTicket = 1260;
                    GetMassCoachingDetails(CoachingTicket);
                    LoadDocumentationsReview();
                    LoadKPIReview(CoachingTicket);
                    LoadPreviousCoachingNotes(CoachingTicket);

                        LoadPreviousCommitment(CoachingTicket);
                        GetCommitment(CoachingTicket);
                   
                    
                    DisableElements();
                    GetTriadNotes(CoachingTicket);
                  

                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
        private void getScore(int reviewId) {
            DataSet ds_coachingevaluation = DataHelper.GetScoreforTriad(reviewId);
            grd_coachingevaluation.DataSource = ds_coachingevaluation;
            if (ds_coachingevaluation.Tables[0].Rows.Count > 0)
            {
                txttotalscore.Text = Convert.ToString(ds_coachingevaluation.Tables[0].Rows[4]["totalscore"]);
            }
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                //Decrypt
                string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                int CoachingTicket = Convert.ToInt32(decrypt);
                    
                //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                //int CoachingTicket = 755;
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(CoachingTicket, 3);
                //ds = ws.GetUploadedDocuments(79);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    RadDocumentationReview.DataSource = ds;
                    RadDocumentationReview.Rebind();
                }
                else
                {
                    RadDocumentationReview.DataSource = string.Empty;
                    RadDocumentationReview.Rebind();
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);   
            }

        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;
                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }

        // added fullname hyperlink column (francis.valera/071020181612)
        protected void RadGridCN_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FullName"].Controls[0];
                hLink.ToolTip = "Coaching Ticket : " + hLink.Text;
                string coachingtkt = (item.FindControl("LabelCT") as Label).Text;
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(coachingtkt));
                //hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt1 + "&ReviewType="+ Page.Request.QueryString["ReviewType"];

                //added for url assignments for previous coaching notes (francis.valera/08162018)
                int nReviewTypeID = Convert.ToInt32((item.FindControl("LabelReviewTypeID") as Label).Text);
                string enctxt2 = DataHelper.Encrypt(nReviewTypeID);
                string formtype = (item.FindControl("LabelFormType") as Label).Text;
                int nFormType = 0;
                if ((formtype == null) || (formtype == "")) {
                    nFormType = 0; 
                } 
                else 
                { 
                    nFormType = Convert.ToInt32(formtype); 
                }
                if (nReviewTypeID == 1)
                {
                    if (nFormType == 1) {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                    }
                    else {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                    }
                }
                else if (nReviewTypeID == 2) 
                {
                    hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 4)
                {
                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else 
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
            }
        }

        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (!Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('|')[3]).Equals((int)ds.Tables[0].Rows[0]["CoacheeID"])) {
                    int saved, audited;

                    if (ds.Tables[0].Rows[0]["saved"] is DBNull)
                    {
                        saved = 0;
                    }
                    else
                    {
                        saved = 1;
                    }

                    if (ds.Tables[0].Rows[0]["audit"] is DBNull)
                    {
                        audited = 0;
                    }
                    else
                    {
                        audited = 1;
                    }

                    //if (retrole == "OM")
                    //{
                    //    effectivenessdiv.Visible = true;
                    //    getScore(CoachingTicket);
                    //}
                    //else if (saved == 1 && audited == 1)
                    //{
                    //    effectivenessdiv.Visible = true;
                    //    getScore(CoachingTicket);
                    //}

                    //additional validation to hide COACHING EFFECTIVENESS EVALUATION section (francis.valera/07132018)
                    //if(retrole == "OM") 
                    //{
                    //    if (CIMNumber != Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"]))
                    //    {
                    //        effectivenessdiv.Visible = true;
                    //        getScore(CoachingTicket);
                    //    }
                    //}
                    //else if (saved == 1 && audited == 1)
                    //{
                    //    if (CIMNumber != Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"]))
                    //    {
                    //        effectivenessdiv.Visible = true;
                    //        getScore(CoachingTicket);
                    //    }
                    //}
                    
                    // revised coaching effectiveness visibility restriction (francis.valera/07192018)
                    int nCreatedBy = Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"]);
                    if (saved == 1 && audited == 1)
                    {
                        // auditby capture only when "audited" is 1 (francis.valera/07232018)
                        int nAuditBy = Convert.ToInt32(ds.Tables[0].Rows[0]["auditby"]);
                        if (nCreatedBy == CIMNumber || nAuditBy == CIMNumber)
                        {
                            effectivenessdiv.Visible = true;
                            getScore(CoachingTicket);
                        }
                    }
                    
                     
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string CoacherCIM = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                    RadTextBox RadReviewID = this.Parent.FindControl("RadReviewID") as RadTextBox;
                    RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                    Label LblCimNumber = this.Parent.FindControl("LblCimNumber") as Label;
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                    Label LblFullName = this.Parent.FindControl("LblFullName") as Label;
                    LblFullName.Text = ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    //RadTextBox RadCoachingDate = this.Parent.FindControl("RadCoachingDate") as RadTextBox;
                    //RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    Label LblSessionType = this.Parent.FindControl("LblSessionType") as Label;
                    LblSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Label LblSessionTopic = this.Parent.FindControl("LblSessionTopic") as Label;
                    LblSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                    //RadAgenda.Text = ds.Tables[0].Rows[0]["Agenda"].ToString();
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    //RadAgenda.ReadOnly = true;
                    //RadDescription.ReadOnly = true;

                    RadStrengths.Text = ds.Tables[0].Rows[0]["Strengths"].ToString();
                    RadOpportunities.Text = ds.Tables[0].Rows[0]["Opportunity"].ToString();

                    RadCoacherFeedback.Text = ds.Tables[0].Rows[0]["CoacherFeedback"].ToString();
                   

                    Label Label3 = this.Parent.FindControl("Label3") as Label;
                    Label3.Text = "My Reviews";
                    Label Label1 = this.Parent.FindControl("Label1") as Label;
                    Label1.Text = " > Add Review ";
                    Label Label2 = this.Parent.FindControl("Label2") as Label;
                    Label2.Text = " > " + ds.Tables[0].Rows[0]["CoacheeName"].ToString();

                    int coacheesigned;

                    if (ds.Tables[0].Rows[0]["coacheesigned"] is DBNull)
                    {
                        coacheesigned = 0;
                    }
                    else
                    {
                        coacheesigned = 1;
                    }

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacheeSignedDate"])))
                    {
                        if (Convert.ToInt32(LblCimNumber.Text) == CIMNumber)
                        {
                            
                            if (IsPH(CIMNumber) == true)
                            {
                                RadTextBox RadSSN = this.Parent.FindControl("RadSSN") as RadTextBox;
                                RadSSN.Visible = false;
                                RadTextBox RadCoacheeCIM = this.Parent.FindControl("RadCoacheeCIM") as RadTextBox;
                                RadCoacheeCIM.Text = Convert.ToString(CIMNumber);
                                RadCoacheeCIM.Enabled = false;
                                RadButton RadCoacheeSignOff = this.Parent.FindControl("RadCoacheeSignOff") as RadButton;
                                RadCoacheeSignOff.Visible = true;
                            }
                            else
                            {
                                RadTextBox RadSSN = this.Parent.FindControl("RadSSN") as RadTextBox;
                                RadSSN.Visible = false;
                                RadTextBox RadCoacheeCIM = this.Parent.FindControl("RadCoacheeCIM") as RadTextBox;
                                RadCoacheeCIM.Text = "";
                                RadCoacheeCIM.Enabled = true;
                                RadButton RadCoacheeSignOff = this.Parent.FindControl("RadCoacheeSignOff") as RadButton;
                                RadCoacheeSignOff.Visible = true;
                            }


                        }
                        else
                        {
                            RadTextBox RadSSN = this.Parent.FindControl("RadSSN") as RadTextBox;
                            RadSSN.Visible = false;
                            RadTextBox RadCoacheeCIM = this.Parent.FindControl("RadCoacheeCIM") as RadTextBox;
                            RadCoacheeCIM.Visible = false;
                            RadButton RadCoacheeSignOff = this.Parent.FindControl("RadCoacheeSignOff") as RadButton;
                            RadCoacheeSignOff.Visible = false;
                        }
                    }
                    else
                    {
                        RadTextBox RadSSN = this.Parent.FindControl("RadSSN") as RadTextBox;
                        RadSSN.Visible = false;
                        RadTextBox RadCoacheeCIM = this.Parent.FindControl("RadCoacheeCIM") as RadTextBox;
                        RadCoacheeCIM.Visible = false;
                        RadButton RadCoacheeSignOff = this.Parent.FindControl("RadCoacheeSignOff") as RadButton;
                        RadCoacheeSignOff.Visible = false;
                    }

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacherSignedDate"])))
                    {
                        if (Convert.ToInt32(CoacherCIM) == CIMNumber)
                        {
                            

                            if (coacheesigned == 0)
                            {
                                RadButton RadCoacherSignOff = this.Parent.FindControl("RadCoacherSignOff") as RadButton;
                                RadCoacherSignOff.Visible = false;
                            }
                            else
                            {
                                RadButton RadCoacherSignOff = this.Parent.FindControl("RadCoacherSignOff") as RadButton;
                                RadCoacherSignOff.Visible = true;
                            }


                        }
                        else
                        {
                            RadButton RadCoacherSignOff = this.Parent.FindControl("RadCoacherSignOff") as RadButton;
                            RadCoacherSignOff.Visible = false;
                        }
                    }
                    else
                    {
                        RadButton RadCoacherSignOff = this.Parent.FindControl("RadCoacherSignOff") as RadButton;
                        RadCoacherSignOff.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }

        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReviewODv2(ReviewID);
                RadGridPRR.DataSource = ds;
                RadGridPRR.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
              //  string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
              //  string ModalHeader = "Error Message";
              //  ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);   
            }

        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        public void LoadPreviousCoachingNotes(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                RadGridCN.DataSource = ds;
                RadGridCN.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);   
            }
        }      
        public void DisableElements()
        {

            RadDescription.Enabled = false;
            if (RadStrengths.Visible)
            {
                RadStrengths.Enabled = false;
            }
            if (RadOpportunities.Visible)
            {
                RadOpportunities.Enabled = false;
            }
            if (RadCoacherFeedback.Visible)
            {
                RadCoacherFeedback.Enabled = false;
            }
        }
        public void LoadPreviousCommitment(int CoachingTicket)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetIncHistoryDataTable(CoachingTicket, 1);

            foreach (DataRow row in dt.Rows)
            {
                // display GROW section only when reviewtype is 1 (francis.valera/07252018)
                int reviewtype = row.Field<Int32>("ReviewTypeID");
                if (reviewtype == 1)
                {
                    int id = row.Field<Int32>("ReviewID");
                    PanelPreviousCommitment.Visible = true;
                    Test Test2 = (Test)LoadControl("Test.ascx");
                    Test2.ReviewIDSelected = Convert.ToInt32(id);
                    PreviousCommitment.Controls.Add(Test2);
                }
            }
        }
        public void GetCommitment(int CoachingTicket)
        {
            //DataSet ds_scoring = DataHelper.GetCommentsforTriad(Convert.ToInt32(CoachingTicket));
            //grd_Commitment.DataSource = ds_scoring;
            //grd_Commitment.Enabled = false;
            RadTextBox1.Text = "What do you want to achieve?";
            RadTextBox3.Text = "Where are you now? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox5.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that? (SMART)";
            RadTextBox7.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

            DataSet ds_scoring = DataHelper.GetCommentsforTriad(CoachingTicket);
            if (ds_scoring.Tables[0].Rows.Count > 0)
            {
                RadTextBox2.Text = ds_scoring.Tables[0].Rows[0]["Goal_Text"].ToString();
                RadTextBox4.Text = ds_scoring.Tables[0].Rows[0]["Reality_Text"].ToString();
                RadTextBox6.Text = ds_scoring.Tables[0].Rows[0]["Options_Text"].ToString();
                RadTextBox8.Text = ds_scoring.Tables[0].Rows[0]["WayForward_Text"].ToString();

            }
        }


        private void GetTriadNotes(int CoachingTicket)
        {
            grd_notesfromcoacher.DataSource = DataHelper.GetNotesforTriadforPDF(CoachingTicket);
            grd_notesfromcoacher.Rebind();

        }

    }
}