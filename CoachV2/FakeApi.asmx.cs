﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using CoachV2.Entities;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Services;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2
{
    /// <summary>
    /// Summary description for FakeApi
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [System.Web.Script.Services.ScriptService]
    public class FakeApi : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData(string StartDate, string EndDate, string KPIid, string CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetGraphValues(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), Convert.ToInt32(KPIid), Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["WeekStart"]) + " - " + Convert.ToString(ds.Tables[0].Rows[i]["WeekEnd"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else 
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            } 

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataformyteam(string StartDate, string EndDate, string KPIid, string CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetGraphValues(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), Convert.ToInt32(KPIid), Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]) ,
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataVOC(string StartDate, string EndDate, string CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetGraphValuesVOC(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }  
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataFCR(string StartDate, string EndDate, string CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetGraphValuesFCR(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            } 

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataAHT(string StartDate, string EndDate, string CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetGraphValuesAHT(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            } 

        }


        public static long GetJavascriptTimestamp(System.DateTime input)
        {
            System.TimeSpan span = new System.TimeSpan(System.DateTime.Parse("1/1/1970").Ticks);
            System.DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }


        #region TL charts

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForTL(string StartDate, string EndDate, int CIMNo, int subcimno, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForTL(StartDate, EndDate, Convert.ToInt32(CIMNo), subcimno, DataView); 
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]), // + " - " + Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    //),TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"])
                    //CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehavior_ForTL(string StartDate, string EndDate, int CIMNo, int kpiid, int subcimno)
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehaviorforTL(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, subcimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new  AnalyticsStats
                {
                    //ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]), // + " - " + Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                        //),TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"])
                        //CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"]
                    
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForTL(string StartDate, string EndDate, int CIMNo, int kpiid, int subcimno)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_TL(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, subcimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"])
                    ,ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetKPISCorevsTargetforTl(string StartDate, string EndDate, int CIMNo, int kpiid ,int subcimno)
        {
            List<AnalyticsTL_CurrVsTarget> fakeList = new List<AnalyticsTL_CurrVsTarget>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_TL(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, subcimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_CurrVsTarget
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    target = Convert.ToInt32(ds.Tables[0].Rows[row]["Target"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataTL_AHTScore(string StartDate, string EndDate, int CIMNo, int subcimno )
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_TL_AHT(StartDate, EndDate, CIMNo, subcimno );
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataTL_FCRScore(string StartDate, string EndDate, int CIMNo , int subcimno )
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_TL_FCR(StartDate, EndDate, CIMNo ,  subcimno );
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataTL_VOCScore(string StartDate, string EndDate, int CIMNo  , int  subcimno )
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_TL_VOC(StartDate, EndDate, CIMNo, subcimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }



            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
        #endregion TL charts


        #region agent charts
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForAgent(string StartDate, string EndDate, int CIMNo, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetDatesForChart1(StartDate, EndDate, Convert.ToInt32(CIMNo), DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehavior_ForAgent(string StartDate, string EndDate, int CIMNo, int kpiid1)
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehavior(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid1);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }
      
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallAHT_ForAgent(string StartDate, string EndDate, int CIMNo)
        {
            List<AnalyticsAgent_OverallKPI> fakeList = new List<AnalyticsAgent_OverallKPI>();

            DataSet ds = DataHelper.getAHTFrequency_agent(StartDate, EndDate, Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsAgent_OverallKPI
                {
                    coached = Convert.ToInt32(ds.Tables[0].Rows[i]["current"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallFCR_ForAgent(string StartDate, string EndDate, int CIMNo)
        {
            List<AnalyticsAgent_OverallKPI> fakeList = new List<AnalyticsAgent_OverallKPI>();

            DataSet ds = DataHelper.getFCRFrequency_agent(StartDate, EndDate, Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsAgent_OverallKPI
                {
                    coached = Convert.ToInt32(ds.Tables[0].Rows[i]["current"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallVOC_ForAgent(string StartDate, string EndDate, int CIMNo)
        {
            List<AnalyticsAgent_OverallKPI> fakeList = new List<AnalyticsAgent_OverallKPI>();

            DataSet ds = DataHelper.getVOCFrequency_agent(StartDate, EndDate, Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsAgent_OverallKPI
                {
                    coached = Convert.ToInt32(ds.Tables[0].Rows[i]["current"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetKPISCorevsTargetforAgent(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["description"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_VOCScore_Agent(string StartDate, string EndDate, int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.getGlidepath_agentvoc(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
       
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_FCRScore_Agent(string StartDate, string EndDate, int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();



         DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
          
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_AHTScore_Agent(string StartDate, string EndDate, int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();

             
           DataSet ds = DataHelper.getGlidepath_agentaht(StartDate, EndDate, CIMNo);
               DataTable dtTemp = new DataTable();
            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        #endregion agent charts

        
        #region operations manager charts

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForOM(string StartDate, string EndDate, int CIMNo, int TLCimNo, int AgentCimNo, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForOM(StartDate, EndDate, Convert.ToInt32(CIMNo), TLCimNo, AgentCimNo, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count ==  0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_GetBehaviorForOM(string StartDate, string EndDate, int CIMNo, int kpiid, int TLCimNo, int AgentCimno)
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehaviorForOM(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, TLCimNo, AgentCimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForOM(string StartDate, string EndDate, int CIMNo, int kpiid, int TLCimno, int AgentCimno)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVSKPI_forOM(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid , TLCimno,  AgentCimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_VOCScore_OM(string StartDate, string EndDate, int CIMNo , int TLCimno, int AgentCimno)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_OM_VOC(StartDate, EndDate, CIMNo,  TLCimno,  AgentCimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }


            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_AHTScore_OM(string StartDate, string EndDate, int CIMNo , int TLCimno, int AgentCimno)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_OM_AHT(StartDate, EndDate, CIMNo, TLCimno, AgentCimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_FCRScore_OM(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_OM_FCR(StartDate, EndDate, CIMNo,TLCimno,  AgentCimno);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getsubordinates( int TLCimno, int AgentCimno)
        {
            DataSet ds = DataHelper.GetTLAgents(TLCimno, AgentCimno);
            DataTable dtTemp = new DataTable();

            foreach (DataRow row in dtTemp.Rows) 
           {
                RadComboBoxItemData itemData = new RadComboBoxItemData();
                itemData.Text = row["Name"].ToString();
                itemData.Value = row["CimNumber"].ToString();
              
            }

        }


        //[WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        //public static List<ListItem> GetCustomers(int Cimnumber, int AccountID)
        //{
        //    string query = "pr_Coach_GetSubordinatesofUser";
        //    string constr = ConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand(query))
        //        {
        //            List<ListItem> customers = new List<ListItem>();
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add("@Cimnumber", Cimnumber);
        //            cmd.Parameters.Add("@AccountID", AccountID);

        //            cmd.Connection = con;
        //            con.Open();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                while (sdr.Read())
        //                {
        //                    customers.Add(new ListItem
        //                    {
        //                        Value = sdr["CimNumber"].ToString(),
        //                        Text = sdr["Name"].ToString()
        //                    });
        //                }
        //            }
        //            con.Close();
        //            return customers;
        //        }
        //    }
        //}


        //[WebMethod]
        //public static CountryDetails[] BindDatatoDropdown(int cimnumber, int accountid)
        //{
        //    DataTable dt = new DataTable();
        //    List<CountryDetails> details = new List<CountryDetails>();
        //    string constr = ConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString;
           
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("pr_Coach_GetSubordinatesofUser", con))
        //        {
        //            con.Open();
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add("cimnumber", cimnumber);
        //            cmd.Parameters.Add("accountid", accountid);
        //            SqlDataAdapter da = new SqlDataAdapter(cmd);
        //            da.Fill(dt);
        //            foreach (DataRow dtrow in dt.Rows)
        //            {
        //                CountryDetails country = new CountryDetails();
        //                country.CimNumber = Convert.ToInt32(dtrow["CimNumber"].ToString());
        //                country.Name = dtrow["Name"].ToString();
        //                details.Add(country);
        //            }
        //        }
        //    }
        //    return details.ToArray();
        //}
        //public class CountryDetails
        //{
        //    public int CimNumber { get; set; }
        //    public string Name { get; set; }
        //}
        
        #endregion operations manager charts


        #region director

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForDir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForOM_Directors(StartDate, EndDate, Convert.ToInt32(CIMNo), OMCimNo, TLCimNo, AgentCimNo, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForDir1(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int DataView, int Siteid, int Account, int LObID)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForOM_Directors1(StartDate, EndDate, Convert.ToInt32(CIMNo), OMCimNo, TLCimNo, AgentCimNo, DataView, Siteid, Account, LObID);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_GetBehaviorForDir(string StartDate, string EndDate, int CIMNo, int kpiid,int OMCimNo, int TLCimNo, int AgentCimNo) 
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehaviorforOMDIR(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, OMCimNo, TLCimNo, AgentCimNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            } 
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }



        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForDIR(string StartDate, string EndDate, int CIMNo, int kpiid, int OMCimNo, int TLCimNo, int AgentCimNo) 
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_OMDIR(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, OMCimNo, TLCimNo, AgentCimNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForVP(string StartDate, string EndDate, int CIMNo, int kpiid, int DirCimNo, int OMCimNo, int TLCimNo, int AgentCimNo, int LobID)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_VP(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid, DirCimNo, OMCimNo, TLCimNo, AgentCimNo, LobID);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_VOCScore_Dir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_DIR_VOC(StartDate, EndDate, CIMNo,  OMCimNo,  TLCimNo,  AgentCimNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_AHTScore_Dir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_DIR_AHT(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_FCRScore_Dir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_DIR_FCR(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        #endregion director


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_TriadCoaching(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = null;
            ds = DataHelper.Get_TriadCoachingChart(StartDate, EndDate, CIMNo, TLCimno, AgentCimno, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["DATE"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getTriad_for_OM(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno,  int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.Get_TriadCoachingChart(StartDate, EndDate, CIMNo, TLCimno, AgentCimno, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_TriadCoaching_HR_QA(string StartDate, string EndDate, int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.Get_TriadCoachingChart_HR_QA(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            { 
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            } 
        }
 
 
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_TriadCoachingDir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int DataView)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.Get_TriadCoachingChartDir(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo, DataView );
            DataTable dtTemp = new DataTable();
             int targetval = 0;
            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                if (ds.Tables[0].Rows[i]["target"] is DBNull)
                {
                    targetval = 0;

                }
                else
                {
                    targetval = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]);
                }
                fakeList.Add(new Analytics
                {   
               
                   
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = targetval, 
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
 
        #region HR

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForHR(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForHR(StartDate, EndDate, Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]), 
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else 
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehavior_ForHR(string StartDate, string EndDate, int CIMNo, int kpiid )
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehaviorforHR(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid );
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"]) 

                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForHR(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_HR(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"])
                    ,
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetKPISCorevsTargetforHR(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsTL_CurrVsTarget> fakeList = new List<AnalyticsTL_CurrVsTarget>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_HR(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_CurrVsTarget
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    target = Convert.ToInt32(ds.Tables[0].Rows[row]["Target"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataHR_AHTScore(string StartDate, string EndDate, int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_HR_AHT(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataHR_FCRScore(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_HR_FCR(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataHR_VOCScore(string StartDate, string EndDate, int CIMNo   )
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_HR_VOC(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
        #endregion

        #region QA

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForQA(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetFrequencyForQA(StartDate, EndDate, Convert.ToInt32(CIMNo));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]), // + " - " + Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                        //),TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"])
                        //CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehavior_ForQA(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetBehaviorforQA(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    //ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]), // + " - " + Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                    //),TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Target"])
                    //CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["Current"]

                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehaviorvsKPI_ForQA(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsTL_KPIVSB> fakeList = new List<AnalyticsTL_KPIVSB>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_QA(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_KPIVSB
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    descriptions = Convert.ToString(ds.Tables[0].Rows[row]["Description"])
                    ,
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["coachedkpi"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetKPISCorevsTargetforQA(string StartDate, string EndDate, int CIMNo, int kpiid)
        {
            List<AnalyticsTL_CurrVsTarget> fakeList = new List<AnalyticsTL_CurrVsTarget>();

            DataSet ds = DataHelper.GetBehaviorVsKPI_QA(StartDate, EndDate, Convert.ToInt32(CIMNo), kpiid);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsTL_CurrVsTarget
                {
                    coachedkpis = Convert.ToInt32(ds.Tables[0].Rows[i]["coachedkpi"]),
                    target = Convert.ToInt32(ds.Tables[0].Rows[row]["Target"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    AvgCurr = Convert.ToInt32(ds.Tables[0].Rows[i]["AvgCurr"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataQA_AHTScore(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_QA_AHT(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataQA_FCRScore(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_QA_FCR(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataQA_VOCScore(string StartDate, string EndDate, int CIMNo)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.GetCoachingFrequency_QA_VOC(StartDate, EndDate, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
        #endregion
        #region from BI

        //agent
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_agent_bi(string StartDate, string EndDate, int CIMNo, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.getGlidepath_agent_bi(StartDate, EndDate, CIMNo, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getkpiaveragescores_agent(string StartDate, string EndDate, int CIMNo, int KPIhierarchy, int DataView)
        {
            List<AnalyticsAgent_OverallKPI> fakeList = new List<AnalyticsAgent_OverallKPI>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.gettopkpiscores_aget(StartDate, EndDate, CIMNo, KPIhierarchy, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsAgent_OverallKPI
                {
                    coached = Convert.ToInt32(ds.Tables[0].Rows[i]["current"]),
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        //TL


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_tl_bi(string StartDate, string EndDate, int CIMNo, int subcimno, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.getGlidepath_TL_bi(StartDate, EndDate, CIMNo, subcimno, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        //actual vs target
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void gettopkpiscores_TL(string StartDate, string EndDate, int CIMNo, int subcimno, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.gettopkpiscores_for_TL(StartDate, EndDate, CIMNo, subcimno, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        //frequency vs score
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataTL_ScoreVSFrequency(string StartDate, string EndDate, int CIMNo, int subcimno, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();



            DataSet ds = DataHelper.GetCoachingFrequency_VS_ScoresTL(StartDate, EndDate, CIMNo, subcimno, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                List<Analytics> fakeList1 = new List<Analytics>();
                fakeList1.Clear();
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList1));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        //TL

        //OM 
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_OM_coachingfrequency(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.gettopkpiscores_for_OM(StartDate, EndDate, CIMNo, TLCimno, AgentCimno, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]), //.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                List<Analytics_Dec> fakeList1 = new List<Analytics_Dec>();
                fakeList1.Clear();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList1));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

          

        }
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_OM_bi(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno, int KPI)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.getGlidepath_OM_bi(StartDate, EndDate, CIMNo, TLCimno, AgentCimno, KPI);
            DataTable dtTemp = new DataTable();
            //JavaScriptSerializer js = new JavaScriptSerializer();
            
            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getactualglide_for_OM(string StartDate, string EndDate, int CIMNo, int TLCimno, int AgentCimno, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();
            //  DataSet ds = DataHelper.getGlidepath_agentfcr(StartDate, EndDate, CIMNo);
            DataSet ds = DataHelper.getactualglide_for_OM(StartDate, EndDate, CIMNo, TLCimno, AgentCimno, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {

                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });

            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_Dir_bi(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();


            //            DataSet ds = DataHelper.GetCoachingFrequency_DIR_FCR(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo);
            DataSet ds = DataHelper.getGlidepath_Dir_bi(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_QA_bi(string StartDate, string EndDate, int CIMNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.getGlidepath_QA_bi(StartDate, EndDate, CIMNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getGlidepath_HR_bi(string StartDate, string EndDate, int CIMNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();



            DataSet ds = DataHelper.getGlidepath_HR_bi(StartDate, EndDate, CIMNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        //director scores
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_Score_Dir(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI, int DataView, int SiteID, int LobID)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();


            DataSet ds = DataHelper.GetCoachingFrequency_DIR_Scores(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo, KPI, DataView, SiteID, LobID, 0);
            DataTable dtTemp = new DataTable();
             
          
                for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
                {
                    fakeList.Add(new Analytics_Dec
                    {
                        ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                        CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                        TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                        CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                    });
                } 
            
            if (ds.Tables[0].Rows.Count > 0)
                {

                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }



        //GetCoachingFrequency_DIR_Glides
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCoachingFrequency_DIR_Glides1(string StartDate, string EndDate, int CIMNo, int OMCimNo, int TLCimNo, int AgentCimNo, int KPI, int DataView)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();


            DataSet ds = DataHelper.GetCoachingFrequency_DIR_Glides(StartDate, EndDate, CIMNo, OMCimNo, TLCimNo, AgentCimNo, KPI, DataView);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        // qa scores vs coaching frequency
        //        

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCoachingFrequency_QA_Scores(string StartDate, string EndDate, int CIMNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_QA_Scores(StartDate, EndDate, CIMNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        //QA scores
        //GetCoachingFrequency_HR_Scores
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCoachingFrequency_HR_Scores(string StartDate, string EndDate, int CIMNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();


            DataSet ds = DataHelper.GetCoachingFrequency_HR_Scores(StartDate, EndDate, CIMNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["Date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }
        //populate cb_AHTFvS

        //for my team performance summary charts
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getDataforMyTeam(string StartDate, string EndDate, int CIMNo, int KPI)
        {
            List<Analytics> fakeList = new List<Analytics>();
            DataSet ds = DataHelper.GetCoachingFrequency_ForMyTeam(StartDate, EndDate, CIMNo, KPI);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                List<Analytics> fakeList1 = new List<Analytics>();
                fakeList1.Clear();
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList1));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getoverallDataformyteam(string StartDate, string EndDate, int KPIid , int CIMNo )
        {
            List<Analytics> fakeList = new List<Analytics>();
            DataSet ds = DataHelper.GetCoachingOverallFrequency_ForMyTeam(StartDate, EndDate, KPIid, CIMNo);
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToInt32(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                List<Analytics> fakeList1 = new List<Analytics>();
                fakeList1.Clear();
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList1));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            //return JsonConvert.SerializeObject(fakeList);

            //return null;

        }

        #region VP
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overall_ForVP(string StartDate, string EndDate, int CIMNo, int Dircimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int SiteId, int accountId, int LobID, int KPIID)
        {
            List<Analytics> fakeList = new List<Analytics>();
            DataSet ds = null;

            if(KPIID == 0)
                ds = DataHelper.GetOverAllCoachingFrequencyVP(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(accountId), Convert.ToInt32(LobID), KPIID);
            else
                ds = DataHelper.GetOverAllCoachingFrequencyVP(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(accountId), Convert.ToInt32(LobID), KPIID);
            
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getData_overallbehavior_ForVP(string StartDate, string EndDate, int CIMNo, int Dircimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int SiteId, int LobID, int kpiid)
        {
            List<AnalyticsStats> fakeList = new List<AnalyticsStats>();

            DataSet ds = DataHelper.GetOverAllBehaviorVP(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(LobID), Convert.ToInt32(kpiid));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new AnalyticsStats
                {
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coacheecount"]),
                    description = Convert.ToString(ds.Tables[0].Rows[i]["description"])
                    
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }


        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetOverBehaviorVSKPIVP(string StartDate, string EndDate, int CIMNo, int Dircimno, int OMCimno, int TLCimNo, int AgentCimNo, int DataView, int SiteId, int accountId, int kpiid)
        {
            List<Analytics> fakeList = new List<Analytics>();

            DataSet ds = DataHelper.GetOverAllBehaviorVP(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(accountId), Convert.ToInt32(kpiid));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]
                    )
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        //coaching frequency vs kpi name
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFVSKPIVP(string StartDate, string EndDate, int CIMNo, int Dircimno, int OMCimno, int TLCimNo, int AgentCimNo, int kpiid, int DataView, int SiteId, int accountId)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();

            DataSet ds = DataHelper.GetFrequencyVSKPI(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(accountId), Convert.ToInt32(kpiid));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToInt32(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                    
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        [WebMethod]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAVST_VP(string StartDate, string EndDate, int CIMNo, int Dircimno, int OMCimno, int TLCimNo, int AgentCimNo,  int kpiid, int DataView, int SiteId, int accountId)
        {
            List<Analytics_Dec> fakeList = new List<Analytics_Dec>();

            DataSet ds = DataHelper.GetAVSKPI_VP(StartDate, EndDate, Convert.ToInt32(CIMNo), Convert.ToInt32(Dircimno), Convert.ToInt32(OMCimno), Convert.ToInt32(TLCimNo), Convert.ToInt32(AgentCimNo), Convert.ToInt32(DataView), Convert.ToInt32(SiteId), Convert.ToInt32(accountId), Convert.ToInt32(kpiid));
            DataTable dtTemp = new DataTable();

            for (int i = 0, row = 0; i < ds.Tables[0].Rows.Count; i++, row++)
            {
                fakeList.Add(new Analytics_Dec
                {
                    ADate = Convert.ToString(ds.Tables[0].Rows[i]["date"]),
                    CoachCount = Convert.ToInt32(ds.Tables[0].Rows[i]["coached"]),
                    TargetCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["target"]),
                    CurrentCount = Convert.ToDecimal(ds.Tables[0].Rows[i]["current"])
                });
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (ds.Tables[0].Rows.Count == 0)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                HttpContext.Current.Response.BufferOutput = true;
                Context.Response.Write(JsonConvert.SerializeObject(fakeList));
                Context.Response.Flush();
                Context.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        //actual vs target 
        #endregion
            //return JsonConvert.SerializeObject(fakeList);

            //return null;
 

        [WebMethod]
        public RadComboBoxItemData[] getagents(object context)
        {
            IDictionary<string, object> contextDictionary = (IDictionary<string, object>)context;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString);
            string filterString = ((string)contextDictionary["FilterString"]).ToLower();
            SqlCommand selectCommand = new SqlCommand(
         @" select  CIM_Number, First_Name 
        from    [SAP].[dbo].[tbl_SAP_Hld_Export] 
        where   CIM_Number= 10115015 '", connection);
            SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
            DataTable products = new DataTable();
            adapter.Fill(products);
            List<RadComboBoxItemData> result = new List<RadComboBoxItemData>(products.Rows.Count);
            foreach (DataRow row in products.Rows)
            {
                RadComboBoxItemData itemData = new RadComboBoxItemData();
                itemData.Text = row["First_Name"].ToString();
                itemData.Value = row["CIM_Number"].ToString();
                result.Add(itemData);
            }
            return result.ToArray();
        }
        #endregion from BI
    }
}
