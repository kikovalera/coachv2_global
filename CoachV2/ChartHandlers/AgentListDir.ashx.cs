﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CoachV2.Entities;
using System.Configuration;
using CoachV2.Entities;
using Newtonsoft.Json;

namespace CoachV2.ChartHandlers
{
    /// <summary>
    /// Summary description for AgentListDir
    /// </summary>
    public class AgentListDir : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string cim = context.Request["cim"];

            if (cim == "0")
            {
                //string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = ds.Tables[0].Rows[0][2].ToString();
                //string gmail = ds.Tables[0].Rows[0][13].ToString();
                //int intcim = Convert.ToInt32(cim_num);
                ////GetSubordinates
                //DataSet ds_tllist = null;

                //string json = JsonConvert.SerializeObject(ds_tllist, Formatting.Indented); //(new JavaScriptSerializer().Serialize(ds_tllist, formatt);
                //context.Response.ContentType = "text/json";
                //context.Response.Write(json);
            }
            else if (cim != "0")
            {

                DataSet ds_tllist = null;
                ds_tllist = DataHelper.GetSubordinatesforcharts(Convert.ToInt32(cim));

                string json = JsonConvert.SerializeObject(ds_tllist, Formatting.Indented); //(new JavaScriptSerializer().Serialize(ds_tllist, formatt);
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}