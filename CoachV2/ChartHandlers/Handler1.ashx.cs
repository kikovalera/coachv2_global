﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CoachV2.Entities;
namespace CoachV2
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //List<string> cimnumber = new List<string>();
            //cimnumber.Add("10137155");
            //cimnumber.Add("10137152");
            //cimnumber.Add("10127455");
            //string json = (new JavaScriptSerializer().Serialize(cimnumber));
            //context.Response.ContentType = "text/json";
            //context.Response.Write(json); 
            DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = ds.Tables[0].Rows[0][2].ToString();
            string gmail = ds.Tables[0].Rows[0][13].ToString();
            int intcim = Convert.ToInt32(cim_num);

            //GetSubordinates
            DataSet ds_tllist = DataHelper.GetSubordinatesforcharts(intcim);

            //List<Dictionary<int, string>> agentlist = new List<Dictionary<int, string>>();
            //for (int i = 0, row = 0; i < ds_tllist.Tables[0].Rows.Count; i++, row++)
            //{
            //    //cimnumber.Add(new Subordinates_Analytics { 
            //    //cimno = Convert.ToInt32(ds_tllist.Tables[i].Rows[0]["Cimnumber"]),
            //    //fullname = Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])

            //    //});
            //    //agentlist.Add(Convert.ToInt32(ds_tllist.Tables[0].Rows[i]["Cimnumber"]), Convert.ToString(ds_tllist.Tables[0].Rows[i]["Name"]) ); //, Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])); //, Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])
            //}

            List<string> cimnumber = new List<string>();
            cimnumber.Add("0");
            for (int i = 0, row = 0; i < ds_tllist.Tables[0].Rows.Count; i++, row++)
            {
                //cimnumber.Add(new Subordinates_Analytics { 
                //cimno = Convert.ToInt32(ds_tllist.Tables[i].Rows[0]["Cimnumber"]),
                //fullname = Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])

                //});
                cimnumber.Add(Convert.ToString(ds_tllist.Tables[0].Rows[i]["Cimnumber"])); //, Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])); //, Convert.ToString(ds_tllist.Tables[i].Rows[0]["Name"])
            }
            string json = (new JavaScriptSerializer().Serialize(cimnumber));
            context.Response.ContentType = "text/json";
            context.Response.Write(json);


        }

        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}