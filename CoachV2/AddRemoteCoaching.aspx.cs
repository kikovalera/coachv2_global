﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using System.Collections;
using System.Globalization;
using System.IO;
using Telerik.Web.UI;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;

namespace CoachV2
{
    public partial class AddRemoteCoaching : System.Web.UI.Page
    {
        string strFilename;
        protected string CIM;
        protected int requestID;
        protected string ccmCIM;
        protected string reviewID;
        protected string reportsTo;
        protected string userIsAgent;

        string toName = "";
        string target = "";
        string subject = "";
        string toEmail = "";
        string htmlBody = "";
        string fromName = "";
        string fromEmail = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);
                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CIMNumber, 2);
                DashboardMyReviewsUserControl1.Visible = true;

                //if ((Page.Request.QueryString["CoachingTicket"] != null))
                if (Page.Request.QueryString["CoachingTicket"] != null)
                {
                    lblstatus.Visible = true;
                    //decryption
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    int CoachingTicket = Convert.ToInt32(decrypt);
                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                    LoadAssignedReviewData(CoachingTicket);
                    LoadCoacheeFeedback(CoachingTicket);
                    LoadCoacherFeedback(CoachingTicket);
                    pn_coacheedetails.Visible = true;
                    pn_coacher.Visible = false;
                    DashboardMyReviewsUserControl1.Visible = false;
                    loadcoacheedetails(CoachingTicket);
                }
                else
                {
                    lblstatus.Visible = false;
                    RadCoacherSignOff.Visible = false;
                    if (ds_userselect2.Tables[0].Rows.Count > 0)
                    {
                        //Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                        //ctrlB.Visible = true;
                        //ctrlB.Text = "Add Remote Coach";

                        //Label ctrlC = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                        //ctrlC.Visible = true;
                        //ctrlC.Text = " > ";

                        Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                        ctrlReviews.Text = " Coaching Dashboard ";
                        Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                        ctrlA.Text = " > My Reviews >";
                        Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                        ctrlB.Visible = true;
                        ctrlB.Text = "Add Remote Coach";

                        bool Supervisor;
                        Supervisor = CheckIfHasSubordinates(CIMNumber);

                        if (Supervisor == true)
                        {
                            RadAccount.Enabled = true;
                            RadSupervisor.Enabled = true;
                            RadCoacheeName.Enabled = true;
                            GetAccounts(CIMNumber);
                            GetDropDownSessions();
                            //PopulateTopics();
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;
                            RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                            RadGridDocumentation.DataSource = string.Empty;
                            RadGridDocumentation.DataBind();
                            RadCoacheeSignOff.Visible = false;
                            RadSSN.Visible = false;
                            RadCoacheeCIMNum.Visible = false;

                        }
                        else
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
            }
        }
        protected void PopulateTopics()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("PopulateTopics " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "PopulateTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownSessions " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetDropDownSessions " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void GetDropDownTopics(int SessionID)
        {
            try
            {
                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID, CIMNumber,true);
                //remove One-on-One Compliance if mass review or remote coaching
                ds.Tables[0].AsEnumerable().Where(r => r.Field<Int32>("TopicId") == 49).ToList().ForEach(row => row.Delete());
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownTopics " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void GetAccounts(int CimNumber)
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates2(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                RadAccount.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
               // RadWindowManager1.RadAlert("GetAccounts " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetAccounts " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }

        }
        protected void GetSubordinates(int CimNumber)
        {
            try
            {

                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates2(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSubordinates " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }

        }
        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates2(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }
        protected void LoadSubordinates()
        {
            try
            {
                GetSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue));
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadSubordinates " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        {
            try
            {
                RadSupervisor.Items.Clear();
                //DataSet ds = null;
                //DataAccess ws = new DataAccess();
                //ds = ws.GetSubordinatesWithTeam(CimNumber, AccountID);
                //RadSupervisor.DataSource = ds;
                //RadSupervisor.DataTextField = "Name";
                //RadSupervisor.DataValueField = "CimNumber";
                //RadSupervisor.DataBind();

                DataSet dsInfo = null;
                DataAccess wsInfo = new DataAccess();
                dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
                if (dsInfo.Tables[0].Rows.Count > 0)
                {
                    string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                    string FullName = FirstName + " " + LastName;
                    RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSubordinatesWithTeam " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinatesWithTeam " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadSessionType_SelectedIndexChanged " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadSessionType_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        protected void RadAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);

                GetSubordinatesWithTeam(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadAccount.SelectedValue));
                RadCoacheeName.Text = string.Empty;
                RadCoacheeName.ClearSelection();
                RadCoacheeName.SelectedIndex = -1;
                RadSupervisor.Text = string.Empty;
                RadSupervisor.ClearSelection();
                RadSupervisor.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadAccount_SelectedIndexChanged " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadAccount_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);   
            }
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            try
            {
                LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
                RadCoacheeName.Text = string.Empty;
                RadCoacheeName.ClearSelection();
                RadCoacheeName.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadSupervisor_SelectedIndexChanged " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadSupervisor_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true); 
            }
        }
        public void LoadTTSubordinates(int CimNumber, int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinatesWithAccounts2(CimNumber, AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadTTSubordinates" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadTTSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true); 
            }
        }
        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadReviewID.Text != "0")
                {
                    DataSet ds = null;
                    DataAccess ws1 = new DataAccess();
                    ds = ws1.CheckIfCanSignOff(Convert.ToInt32(RadReviewID.Text), 0);

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                        string ReviewID = RadReviewID.Text;
                        DataAccess ws = new DataAccess();
                        ws.UpdateReviewRemote(Convert.ToInt32(ReviewID), 0);
                        DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coacheesigned off 
                        DisableItems();
                        RadCoacheeFeedback.Enabled = false;
                        RadCoacheeFeedbackValidator.Enabled = false;
                        RadCoacherFeedback.Enabled = false;
                        RadCoacherFeedbackValidator.Enabled = false;
                        RadCoacherSignOff.Visible = false;
                        RadSave.Visible = false;

                        DataSet dsx = DataHelper.GetCoachingByTix(ReviewID.ToString());

                        //if (Convert.ToInt32(dsx.Tables[0].Rows[0]["CoacheeSigned"]) == 1)
                        //{
                        //    string stats = "*Coach Sign Off: " + HttpContext.Current.User.Identity.Name.Split('|')[1].ToString() + " " + DateTime.Now.ToString("M/d/yyyy");
                        //    lblstatus.Text = stats;

                        //}

                       

                        RadCoacherSignOff.Visible = false;
                        string ModalLabel = "Coach has signed off from this review.";
                        //RadWindowManager1.RadAlert(myStringVariable + "", 500, 200, "Success", "", "");
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);        
                        //RadAsyncUpload1.Visible = false;

                        lblstatus.Visible = true;
                        loadcoacheedetails(Convert.ToInt32(ReviewID));
                    }
                    else
                    {
                        //RadWindowManager1.RadAlert("Cannot sign off, no response yet from the coachee" + "", 500, 200, "Error Message", "", "");
                        string ModalLabel = "Cannot sign off, no response yet from the coachee.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
                    }
                }
                else
                {
                    //RadWindowManager1.RadAlert("Cannot sign off, save the review first" + "", 500, 200, "Error Message", "", "");
                    string ModalLabel = "Cannot sign off, save the review first.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true); 
                }

            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadCoacherSignOff_Click " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadCoacherSignOff_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);   
            }
        }
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);

                    DataSet ds = null;
                    DataAccess ws1 = new DataAccess();
                    ds = ws1.CheckIfCanSignOff(Convert.ToInt32(RadReviewID.Text), 0);

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        if (IsPH(CIMNumber))
                        {
                            if (RadSSN.Text != "")
                            {
                                if (CheckSSN() == true)
                                {
                                        DataAccess ws = new DataAccess();
                                        ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                                        DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coacheesigned off 


                                        string stats = "*Coachee Sign Off: " + HttpContext.Current.User.Identity.Name.Split('|')[1].ToString() + " " + DateTime.Now.ToString("M/d/yyyy");
                                        lblstatus.Text = stats;

                                        string ModalLabel = "Coachee has signed off from this review.";
                                        //RadWindowManager1.RadAlert(script + "", 500, 200, "Success", "", "");
                                        string ModalHeader = "Success";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);        
                                        RadSSN.Visible = false;
                                        RadCoacheeCIMNum.Visible = false;
                                        RadCoacheeSignOff.Visible = false;
                                        RadSave.Visible = false;
                                        RadCoacheeFeedback.Enabled = false;
                                        RadCoacheeFeedbackValidator.Enabled = false;

                                        //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                                        loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));
                                }
                                else
                                {
                                    //RadWindowManager1.RadAlert("Incorrect Last 3 SSID." + "", 500, 200, "Error Message", "", "");
                                    string ModalLabel = "Incorrect Last 3 SSID.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
                                }
                                
                            }
                            else
                            {
                                //RadWindowManager1.RadAlert("Last 3 SSID is a required field." + "", 500, 200, "Error Message", "", "");
                                string ModalLabel = "Last 3 SSID is a required field.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
                            }
                        }
                        else
                        {
                            DataAccess ws = new DataAccess();
                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                            DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coacheesigned off 

                            string stats = "*Coachee Sign Off: " + HttpContext.Current.User.Identity.Name.Split('|')[1].ToString() + " " + DateTime.Now.ToString("M/d/yyyy");
                            lblstatus.Text = stats;

                            string ModalLabel = "Coachee has signed off from this review.";
                            //RadWindowManager1.RadAlert("Coachee Sign Off" + script + "", 500, 200, "Success", "", "");
                            string ModalHeader = "Success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            RadSSN.Visible = false;
                            RadCoacheeCIMNum.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;

                            //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                            loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));

                            //if (RadCoacheeCIMNum.Text != "")
                            //{
                            //    if (Convert.ToInt32(RadCoacheeCIMNum.Text) == Convert.ToInt32(CIMNumber))
                            //    {
                            //        if (Convert.ToInt32(RadReviewID.Text) != 0)
                            //        {
                            //            DataAccess ws = new DataAccess();
                            //            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                            //            DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coacheesigned off 

                            //            string stats = "*Coachee Sign Off: " + HttpContext.Current.User.Identity.Name.Split('|')[1].ToString() + " " + DateTime.Now.ToString("M/d/yyyy");
                            //            lblstatus.Text = stats;

                            //            string ModalLabel = "Coachee has signed off from this review.";
                            //            //RadWindowManager1.RadAlert("Coachee Sign Off" + script + "", 500, 200, "Success", "", "");
                            //            string ModalHeader = "Success";
                            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);        
                            //            RadSSN.Visible = false;
                            //            RadCoacheeCIMNum.Visible = false;
                            //            RadCoacheeSignOff.Visible = false;
                            //            RadCoacheeFeedback.Enabled = false;
                            //            RadCoacheeFeedbackValidator.Enabled = false;
                                        
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    //RadWindowManager1.RadAlert("CIM Number is a required field." + "", 500, 200, "Error Message", "", "");
                            //    string ModalLabel = "CIM Number is a required field.";
                            //    string ModalHeader = "Error Message";
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
                            //}

                        }
                    }
                else
                {
                    //RadWindowManager1.RadAlert("Cannot sign off, no response yet from you" + "", 500, 200, "Error Message", "", "");
                    string ModalLabel = "Cannot sign off, no response yet from you.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);               
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadCoacheeSignOff_Click " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadCoacheeSignOff_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        public bool CheckSSN()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        {
            //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            int CIMNumber = Convert.ToInt32(cim_num);

            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void DisableItems()
        {
            RadAccount.Enabled = false;
            RadSupervisor.Enabled = false;
            RadCoacheeName.Enabled = false;
            RadSessionType.Enabled = false;
            RadSessionTopic.Enabled = false;
            RadCoacherFeedback.Text = "";
            RadCoacheeFeedback.Text = "";
        }
      
        protected void RadSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RadReviewID.Text) == 0)
                {
                    if (RadCoacherFeedback.Text != "")
                    {
                        string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                        DataSet ds1 = null;
                        DataAccess ws1 = new DataAccess();
                        ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
                        string emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
                        int ReviewID;
                        DataAccess ws = new DataAccess();
                        ReviewID = ws.InsertRemoteCoaching(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadCoacherFeedback.Text, Convert.ToInt32(cim_num), true);
                        RadReviewID.Text = Convert.ToString(ReviewID);
                        UploadDocumentation(0, ReviewID);
                        LoadDocumentations();
                        LoadCoacheeFeedback(ReviewID);
                        LoadCoacherFeedback(ReviewID);
                        DisableItems();
                        SendEmail(1, ReviewID, HttpContext.Current.User.Identity.Name.Split('|')[0].ToString());
                        SendEmail(2, ReviewID, emailCoachee);
                        RadCoacherSignOff.Visible = true;
                        string ModalLabel = "Successfully saved review with coaching ticket " + ReviewID + "";
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                    else
                    {
                        string ModalLabel = "Coach Feedback is a required field.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                }
                else
                {
                    if (CheckIfAlreadySignedOff(Convert.ToInt32(RadReviewID.Text)) == false)
                    {
                        string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                        int a = FeedbackRole(Convert.ToInt32(cim_num), Convert.ToInt32(RadReviewID.Text));
                        if (a == 1)//Coacher
                        {
                            if (RadCoacherFeedback.Text != "")
                            {
                                DataSet ds1 = null;
                                DataAccess ws1 = new DataAccess();
                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(LblCimNumber.Text));
                                string emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
                                DataAccess ws = new DataAccess();ws.UpdateRemoteCoachFeedback(Convert.ToInt32(RadReviewID.Text), Convert.ToInt32(cim_num), RadCoacherFeedback.Text);
                                LoadCoacherFeedback(Convert.ToInt32(RadReviewID.Text));
                                LoadCoacheeFeedback(Convert.ToInt32(RadReviewID.Text));
                                RadCoacherFeedback.Text = "";
                                UploadDocumentation(0, Convert.ToInt32(RadReviewID.Text));
                                LoadDocumentations();
                                SendEmail(3, Convert.ToInt32(RadReviewID.Text), emailCoachee);

                                string ModalLabel1 = "Successfully saved feedback.";
                                string ModalHeader1 = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader1 + "','" + ModalLabel1 + "'); });", true);
                            }
                            else
                            {
                                string ModalLabel = "Coach Feedback is a required field.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            }
                        }
                        else if (a == 2)
                        {
                            if (RadCoacheeFeedback.Text != "")
                            {
                                DataSet ds1 = null;
                                DataAccess ws1 = new DataAccess();
                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(LblCimCoacher.Text));
                                string emailCoacher = ds1.Tables[0].Rows[0]["Email"].ToString();
                                //RadCoacheeFeedback.Text = "";
                                DataAccess ws = new DataAccess();
                                ws.UpdateRemoteCoachFeedback(Convert.ToInt32(RadReviewID.Text), Convert.ToInt32(cim_num), RadCoacheeFeedback.Text);
                                LoadCoacheeFeedback(Convert.ToInt32(RadReviewID.Text));
                                LoadCoacherFeedback(Convert.ToInt32(RadReviewID.Text));
                                RadCoacheeFeedback.Text = "";
                                SendEmail(3, Convert.ToInt32(RadReviewID.Text), emailCoacher);

                                string ModalLabel1 = "Successfully saved feedback.";
                                //RadWindowManager1.RadAlert(myStringVariable + "", 500, 200, "Success", "", "");
                                string ModalHeader1 = "Success";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader1 + "','" + ModalLabel1 + "'); });", true);
                            }
                            else
                            {
                                //RadWindowManager1.RadAlert("Coachee Feedback is a required field." + "", 500, 200, "Error Message", "", "");
                                string ModalLabel = "Coachee Feedback is a required field.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            }

                        }

                    }
                    else
                    {
                        RadSave.Visible = false;
                        string ModalLabel = "Coaching Ticket has already been signed off. Cannot save additional comment.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));
                    }

                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadSave_Click " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = ex.Message.ToString();//"RadSave_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }       
        public void LoadCoacherFeedback(int ReviewId)
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetRemoteCoachFeedback(ReviewId, 1);
                PagedDataSource pds = new PagedDataSource();
                DataView dv = new DataView(dt);
                pds.DataSource = dv;
                pds.AllowPaging = true;
                pds.PageSize = 4;
                pds.CurrentPageIndex = PageNumber2;
                if (pds.PageCount > 1)
                {
                    rptPaging2.Visible = true;
                    ArrayList arraylist = new ArrayList();
                    for (int i = 0; i < pds.PageCount; i++)
                        arraylist.Add((i + 1).ToString());
                    rptPaging2.DataSource = arraylist;
                    rptPaging2.DataBind();
                }
                else
                {
                    rptPaging2.Visible = false;
                }
                rptCoacherFeedback.DataSource = pds;
                rptCoacherFeedback.DataBind();

            }
            catch (Exception ex)
            {               
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadCoacherFeedback " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        public void LoadCoacheeFeedback(int ReviewId)
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetRemoteCoachFeedback(ReviewId, 0);

                PagedDataSource pds = new PagedDataSource();
                DataView dv = new DataView(dt);
                pds.DataSource = dv;
                pds.AllowPaging = true;
                pds.PageSize = 4;
                pds.CurrentPageIndex = PageNumber;
                if (pds.PageCount > 1)
                {
                    rptPaging.Visible = true;
                    ArrayList arraylist = new ArrayList();
                    for (int i = 0; i < pds.PageCount; i++)
                        arraylist.Add((i + 1).ToString());
                    rptPaging.DataSource = arraylist;
                    rptPaging.DataBind();
                }
                else
                {
                    rptPaging.Visible = false;
                }
                rptCoacheeFeedback.DataSource = pds;
                rptCoacheeFeedback.DataBind();

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadCoacheeFeedback " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
        }
        public bool CheckIfAlreadySignedOff(int ReviewID)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetAssignedReviewsData(ReviewID);

            if (!String.IsNullOrEmpty(ds.Tables[0].Rows[0]["CoacherSigned"].ToString()))
            {
                return true;
            }
            else if (!String.IsNullOrEmpty(ds.Tables[0].Rows[0]["CoacheeSigned"].ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void LoadAssignedReviewData(int ReviewID)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetAssignedReviewsData(ReviewID);

           // DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
           // string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];

            string coachersigned = "";
            if (ds.Tables[0].Rows[0]["CoacherSigned"] is DBNull)
            {
                coachersigned = "0";
            }
            else
            {
                coachersigned = "1";
            }
            string coacheesigned = "";
            if (ds.Tables[0].Rows[0]["CoacheeSigned"] is DBNull)
            {
                coacheesigned = "0";
            }
            else
            {
                coacheesigned = "1";
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                int a = FeedbackRole(Convert.ToInt32(cim_num), ReviewID);
                if (a == 1)//Coacher
                {
                    if (coachersigned == "1" && coacheesigned == "1")
                    {
                        RadSSN.Visible = false;
                        RadCoacheeSignOff.Visible = false;
                        RadCoacheeCIMNum.Visible = false;
                        RadCoacherSignOff.Visible = false;
                        RadSave.Visible = false;
                        RadCoacherFeedback.Enabled = false;
                        RadCoacherFeedbackValidator.Enabled = false;
                        RadCoacheeFeedback.Enabled = false;
                        RadCoacheeFeedbackValidator.Enabled = false;
                        RadAsyncUpload1.Visible = false;
                    }
                    else if (coachersigned == "1" || coacheesigned == "1")
                    {
                        if (coachersigned == "1")
                        {
                            RadSSN.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                            RadCoacheeCIMNum.Visible = false;
                            RadCoacherSignOff.Visible = false;
                            RadSave.Visible = false;
                            RadCoacherFeedback.Enabled = false;
                            RadCoacherFeedbackValidator.Enabled = false;
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;
                            RadAsyncUpload1.Visible = false;
                        }
                        else if (coacheesigned == "1")
                        {
                            RadSSN.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                            RadCoacheeCIMNum.Visible = false;
                            RadCoacherFeedback.Enabled = false;
                            RadCoacherFeedbackValidator.Enabled = false;
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;
                            RadAsyncUpload1.Visible = false;
                            RadSave.Visible = false;
                            RadCoacherSignOff.Visible = true;
                        }
                    }
                    else
                    {
                        // enable feedback when coachersigned and coacheesigned is 0 (francis.valera/08012018)
                        //RadCoacherFeedback.Enabled = false;
                        RadCoacherFeedback.Enabled = true;
                        RadCoacheeFeedback.Enabled = false;
                        RadCoacheeFeedbackValidator.Enabled = false;
                        // show (true) coach signoff button on pageload (francis.valera/08092018)
                        RadCoacherSignOff.Visible = true;
                        RadAsyncUpload1.Visible = true;
                        RadSSN.Visible = false;
                        RadCoacheeCIMNum.Visible = false;
                        RadCoacheeSignOff.Visible = false;
                    }
                }
                else if (a == 2)//Coachee
                {
                    if (coachersigned == "1" || coacheesigned == "1")
                    {
                        if (coachersigned == "1" && coacheesigned == "1")
                        {
                            RadSSN.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                            RadCoacheeCIMNum.Visible = false;
                            RadCoacherSignOff.Visible = false;
                            RadSave.Visible = false;
                            RadCoacherFeedback.Enabled = false;
                            RadCoacherFeedbackValidator.Enabled = false;
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;
                            RadAsyncUpload1.Visible = false;
                        }
                        else if (coachersigned == "1")
                        {
                            RadCoacheeFeedback.Enabled = true;
                            RadCoacherFeedback.Enabled = false;
                            RadCoacherFeedbackValidator.Enabled = false;
                            RadCoacherSignOff.Visible = false;
                            RadAsyncUpload1.Visible = false;
                            if (IsPH(Convert.ToInt32(ds.Tables[0].Rows[0]["CoacheeID"].ToString())))
                            {
                                RadCoacheeCIMNum.Text = cim_num;
                                RadCoacheeCIMNum.Enabled = false;
                                RadSSN.Visible = true;
                                RadCoacheeSignOff.Visible = true;
                                RadSave.Visible = false;
                            }
                            else
                            {
                                RadCoacheeCIMNum.Text = "";
                                RadSSN.Visible = false;
                                RadCoacheeSignOff.Visible = true;
                                RadSave.Visible = false;
                            }
                        }
                        else if (coacheesigned == "1")
                        {
                            RadSSN.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                            RadCoacheeCIMNum.Visible = false;
                            RadCoacherSignOff.Visible = false;
                            RadSave.Visible = false;
                            RadCoacherFeedback.Enabled = false;
                            RadCoacherFeedbackValidator.Enabled = false;
                            RadCoacheeFeedback.Enabled = false;
                            RadCoacheeFeedbackValidator.Enabled = false;
                            RadAsyncUpload1.Visible = false;
                        }
                    }
                    else
                    {
                        RadCoacheeFeedback.Enabled = true;
                        RadCoacherFeedback.Enabled = false;
                        RadCoacherFeedbackValidator.Enabled = false;
                        RadCoacherSignOff.Visible = false;
                        RadAsyncUpload1.Visible = false;
                        if (IsPH(Convert.ToInt32(ds.Tables[0].Rows[0]["CoacheeID"].ToString())))
                        {
                            RadCoacheeCIMNum.Text = cim_num;
                            RadCoacheeCIMNum.Enabled = false;
                            RadSSN.Visible = true;
                            RadCoacheeSignOff.Visible = true;
                            RadSave.Visible = true;
                        }
                        else
                        {
                            RadCoacheeCIMNum.Text = "";
                            RadSSN.Visible = false;
                            RadCoacheeSignOff.Visible = true;
                            RadSave.Visible = true;
                        }
                    }
                }
                else
                {
                    RadSave.Visible = false;
                    RadCoacheeCIMNum.Visible = false;
                    RadSSN.Visible = false;
                    RadCoacherSignOff.Visible = false;
                    RadCoacheeSignOff.Visible = false;
                }

                RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                RadAccount.AllowCustomText = true;
                RadAccount.Text = ds.Tables[0].Rows[0]["Account"].ToString();
                RadSupervisor.AllowCustomText = true;
                RadSupervisor.Text = ds.Tables[0].Rows[0]["SupervisorName"].ToString();
                RadCoacheeName.AllowCustomText = true;
                RadCoacheeName.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                RadSessionType.AllowCustomText = true;
                RadSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                RadSessionTopic.AllowCustomText = true;
                RadSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                RadCoachingDate.Text = ds.Tables[0].Rows[0]["CreatedOn"].ToString();
                DisableItems();
                LoadDocumentations();
            }
        }
        public int FeedbackRole(int CimNumber, int ReviewID)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetAssignedReviewsData(ReviewID);

            //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];

            if (ds.Tables[0].Rows.Count > 0)
            {

                LblCimCoacher.Text = ds.Tables[0].Rows[0]["CreatedBy"].ToString();
                LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                if (ds.Tables[0].Rows[0]["CreatedBy"].ToString() == cim_num)
                {
                    
                    return 1;//Coacher
                }
                else if (ds.Tables[0].Rows[0]["CoacheeID"].ToString() == cim_num)
                {
                    return 2;//Coachee
                }
                else
                {
                    return 3;//Not Related
                }
            }
            else
            {
                return 3;//Not Related
            }

        }
        protected void rptPaging_ItemCommand(object source, CommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            LoadCoacheeFeedback(Convert.ToInt32(RadReviewID.Text));
        }
        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }
        protected void rptPaging2_ItemCommand(object source, CommandEventArgs e)
        {
            PageNumber2 = Convert.ToInt32(e.CommandArgument) - 1;
            LoadCoacherFeedback(Convert.ToInt32(RadReviewID.Text));
        }
        public int PageNumber2
        {
            get
            {
                if (ViewState["PageNumber2"] != null)
                    return Convert.ToInt32(ViewState["PageNumber2"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber2"] = value;
            }
        }
        protected void loadcoacheedetails(int reviewid)
        {

            //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            //string coachid = "";
            //decryption
            string coachid = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));

            //coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(coachid);
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);
            if (CIMNumber_userlogged == (int)ds_get_review.Tables[0].Rows[0]["createdby"]) {
                btn_Audit1.Visible = false;
            }
            int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
            LblCimNumber.Text = coacheeid.ToString();
            DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
            DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
            DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));
            int coacheesigned, coachersigned;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = 1;
            }
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = 1;// Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
            }

            string coachersigneddate1, coacheesigneddate1;
            if (ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"] is DBNull)
            {
                coacheesigneddate1 = "";
            }
            else
            {
                coacheesigneddate1 = ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"].ToString();
            }

            if (ds_get_review.Tables[0].Rows[0]["coachersigneddate1"] is DBNull)
            {
                coachersigneddate1 = "";
            }
            else
            {
                coachersigneddate1 = ds_get_review.Tables[0].Rows[0]["coachersigneddate1"].ToString();
            }

            if (coacheesigned == 1) //if coacheesigned off
            {
                if (coachersigned == 1) // if coachersigned off 
                {
                    RadAsyncUpload1.Visible = false;    
                    lblstatus.Text = "*Coach: " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                     " Coach Sign off date : " + coachersigneddate1 + " Coachee Sign off date : " + coacheesigneddate1;
                }
                else if (coachersigned == 0) //if no coacher sign off
                {
                    lblstatus.Text = "*Needs sign off from Coach :" + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                     " Coachee Sign off date : " + coacheesigneddate1;
                }

            }
            else if (coachersigned == 1) //if coachersigned off
            {
                if (coacheesigned == 1) //   if no coachee sign off
                {
                    RadAsyncUpload1.Visible = false;    
                    lblstatus.Text = "*Coach: " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                      " Coach Sign off date : " + coachersigneddate1 + " Coachee Sign off date : " + coacheesigneddate1;
                }
                else if (coacheesigned == 0) //   if no coachee sign off
                {
                    lblstatus.Text = "*Needs sign off from Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + "." + "Coach: " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString()) + " already signed off." +
                       " Coach Sign off date : " + coachersigneddate1;
                }
            }

            else if ((coacheesigned == 0) && (coacheesigned == 0))
            {
                lblstatus.Text = "*Needs sign off from Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " and " + "Coach: " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString());
 
            }



           
            string coachersigneddate = ds_get_review.Tables[0].Rows[0]["CoacherSigneddate"].ToString();
            string coacheesigneddate = ds_get_review.Tables[0].Rows[0]["CoacheeSigneddate"].ToString();

            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CIMNumber_userlogged);
            string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

            if (rolevalue == "QA")
                saprolefordashboard = "QA";
            else if (rolevalue == "HR")
                saprolefordashboard = "HR";
            else
                saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

            string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
            string retrole = "";
            if (saprolefordashboard == "TL")
            {
                retrole = "TL";
            }
            else if (saprolefordashboard == "OM")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "Dir")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "QA")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {

                    retrole = "";
                }
            }
            else if (saprolefordashboard == "HR")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {
                    retrole = "";
                }
            }
            else
            {
                retrole = "";
            }
            //old 
            if ((coacheesigned == 1) && (coachersigned == 1) && (retrole == "OM"))
            {
                btn_Audit1.Visible = false;
            }
            if (coachersigned == 1)
            {
                RadCoacherSignOff.Visible = false;
            }
            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
            string ImgDefault;


            string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
            FakeURLID.Value = URL;
            if (!Directory.Exists(directoryPath))
            {
                ImgDefault = URL + "/Content/images/no-photo.jpg";
            }
            else
            {

                ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
            }

            RadReviewID.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["ID"]);

            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                //RadFollowup.SelectedDate = Convert.ToDateTime(ds_get_review.Tables[0].Rows[0]["FollowDate"]);
                RadFollowup.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }

            ProfImage.ImageUrl = ImgDefault;
            LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
            LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
            lblSite.Text = ds.Tables[0].Rows[0]["Sitename"].ToString();
            LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
            LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
            LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
            //LblRegion.Text = ds.Tables[0].Rows[0]["Province_State"].ToString();
            LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();
            LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
            LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";
            if (ds_get_review.Tables[0].Rows[0]["Topicname"] is DBNull) { }
            else
            {
                SessionTopic.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Topicname"]);
            }

            if (ds_get_review.Tables[0].Rows[0]["SessionName"] is DBNull) { }
            else
            {
                SessionType.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["SessionName"]);
            }

            RadTextBox1.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Id"].ToString());


            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                //RadFollowup.SelectedDate = Convert.ToDateTime(ds_get_review.Tables[0].Rows[0]["FollowDate"]);
                RadFollowup.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }
        }
        //documentation
        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload1.ClientID), true);

        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        //documentation
        //export to PDF 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                try
                {
                    DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                    DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                    DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisorid"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                    string ImgDefault;
                    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                    FakeURLID.Value = URL;
                    if (!Directory.Exists(directoryPath))
                    {
                        ImgDefault = URL + "/Content/images/no-photo.jpg";
                    }
                    else
                    {

                        ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                    }

                    //Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                    //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    //pdfDoc.Open();
                    //pdfDoc.NewPage();

                    Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                    BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.NewPage();

                    //coaching specifics


                    string cs = "Coaching Ticket ";
                    PdfPTable specificstable = new PdfPTable(2);
                    string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                    specificscell.Colspan = 2;
                    //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                    specificscell.Border = 0;
                    specificstable.AddCell(specificscell);
                    //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                    //specificscelli00.Border = 0;
                    //                   specificscelli00.Rowspan = 7;


                    PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                    specificscelli1.Border = 0;
                    PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                    specificscelli2.Border = 0;


                    PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                    specificscelli3.Border = 0;
                    PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                    specificscelli4.Border = 0;

                    PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                    specificscelli5.Border = 0;
                    string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                    PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                    specificscelli6.Border = 0;

                    PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                    specificscelli7.Border = 0;
                    PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                    specificscelli8.Border = 0;

                    PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                    specificscelli9.Border = 0;
                    string campval = "";
                    if (ds2.Tables[0].Rows.Count == 0)
                    {
                    }
                    else
                    {
                        campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                    }
                    PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                    specificscelli10.Border = 0;

                    PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                    specificscelli11.Border = 0;
                    PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                    specificscelli12.Border = 0;

                    PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                    specificscelli13.Border = 0;
                    PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                    specificscelli14.Border = 0;

                    PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                    specificscelli15.Border = 0;
                    PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                    specificscelli16.Border = 0;

                    PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                    specificscelli17.Border = 0;
                    PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                    specificscelli18.Border = 0;

                    //specificstable.AddCell(gif);
                    specificstable.AddCell(specificscelli1);
                    specificstable.AddCell(specificscelli2);
                    specificstable.AddCell(specificscelli15);
                    specificstable.AddCell(specificscelli16);
                    specificstable.AddCell(specificscelli3);
                    specificstable.AddCell(specificscelli4);
                    specificstable.AddCell(specificscelli5);
                    specificstable.AddCell(specificscelli6);
                    specificstable.AddCell(specificscelli7);
                    specificstable.AddCell(specificscelli8);
                    specificstable.AddCell(specificscelli9);
                    specificstable.AddCell(specificscelli10);
                    specificstable.AddCell(specificscelli11);
                    specificstable.AddCell(specificscelli12);
                    specificstable.AddCell(specificscelli13);
                    specificstable.AddCell(specificscelli14);
                    specificstable.AddCell(specificscelli17);
                    specificstable.AddCell(specificscelli18);
                    specificstable.SpacingAfter = 40;


                    //coaching specifics

                    //coachee feedback

                    string coacheeresps = "";
                    string coacheeresponse = "";

                    PdfPTable tblfeedback = new PdfPTable(4);
                    string feedbackheader = "";// "Goal    Reality     Options     Way Forward";
                    PdfPCell feedbackcell = new PdfPCell(new Phrase(feedbackheader, font));
                    feedbackcell.Colspan = 4;
                    feedbackcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    tblfeedback.AddCell(feedbackcell);
                    tblfeedback.SpacingBefore = 30;
                    tblfeedback.SpacingAfter = 30;


                    DataSet ds_coacheefeedback = DataHelper.GetRemoteCoachFeedbackpdf(Convert.ToInt32(RadReviewID.Text), 0);
                    if (ds_coacheefeedback.Tables[0].Rows.Count > 0)
                    {

                        for (int ctr = 0; ctr < ds_coacheefeedback.Tables[0].Rows.Count; ctr++)
                        {
                            coacheeresponse = Convert.ToString(ds_coacheefeedback.Tables[0].Rows[ctr]["Name"]) + " " + Convert.ToString(ds_coacheefeedback.Tables[0].Rows[ctr]["DateTimeCreated"]) + Environment.NewLine + Convert.ToString(ds_coacheefeedback.Tables[0].Rows[ctr]["Feedback"]);
                            coacheeresps = coacheeresponse + Environment.NewLine + coacheeresps;
                        }


                    }

                    else
                    {
                        coacheeresps = "";

                    }



                    Paragraph p11 = new Paragraph();
                    string line11 = "Coachee Feedback History" + Environment.NewLine + coacheeresps;
                    p11.Alignment = Element.ALIGN_LEFT;
                    p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p11.Add(line11);
                    p11.SpacingBefore = 10;
                    p11.SpacingAfter = 10;

                    string coacherresps = "";
                    string coacherresponse = "";

                    DataSet ds_coacherfeedback = DataHelper.GetRemoteCoacherFeedbackpdf(Convert.ToInt32(RadReviewID.Text), 1);
                    if (ds_coacherfeedback.Tables[0].Rows.Count > 0)
                    {

                        for (int ctr = 0; ctr < ds_coacherfeedback.Tables[0].Rows.Count; ctr++)
                        {
                            coacherresponse = Convert.ToString(ds_coacherfeedback.Tables[0].Rows[ctr]["Name"]) + " " + Convert.ToString(ds_coacherfeedback.Tables[0].Rows[ctr]["DateTimeCreated"]) + Environment.NewLine + Convert.ToString(ds_coacherfeedback.Tables[0].Rows[ctr]["Feedback"]);
                            coacherresps = coacherresponse + Environment.NewLine + coacherresps;
                        }


                    }

                    else
                    {
                        coacheeresps = "";

                    }
                    //coach feedback history
                    Paragraph p12 = new Paragraph();
                    string line12 = "Coach Feedback History" + Environment.NewLine + coacherresps;
                    p12.Alignment = Element.ALIGN_LEFT;
                    p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p12.Add(line12);
                    p12.SpacingBefore = 10;
                    p12.SpacingAfter = 10;

                    string documents, alldocs = "";
                    DataSet ds_remotedocumentation = DataHelper.GetRemoteCoachDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                    if (ds_remotedocumentation.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation.Tables[0].Rows.Count; ctr++)
                        {
                            documents = Convert.ToString(ds_remotedocumentation.Tables[0].Rows[ctr]["DocumentName"]);
                            alldocs = documents + Environment.NewLine + alldocs;

                        }
                    }
                    else
                    {
                        alldocs = "No documents uploaded.";
                    }


                    Paragraph p13 = new Paragraph();
                    string line13 = "Documentation " + Environment.NewLine;
                    p13.Alignment = Element.ALIGN_LEFT;
                    p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p13.Add(line13);
                    p13.SpacingBefore = 10;
                    p13.SpacingAfter = 10;


                    PdfPTable table = new PdfPTable(4);
                    string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cell = new PdfPCell(new Phrase(header, font));
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cell);
                    table.SpacingBefore = 30;
                    table.SpacingAfter = 30;
                    table.WidthPercentage = 100;

                    table.HorizontalAlignment = 0;
                    table.TotalWidth = 600f;
                    table.LockedWidth = true;
                    float[] widths = new float[] { 150f, 150f, 150f, 150f };
                    table.SetWidths(widths);



                    DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                    if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                                notescellh1.Border = 0;
                                table.AddCell(notescellh1);
                                PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                                notescellh2.Border = 0;
                                table.AddCell(notescellh2);
                                PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                                notescellh3.Border = 0;
                                table.AddCell(notescellh3);
                                PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                                notescellh4.Border = 0;
                                table.AddCell(notescellh4);

                            }
                            //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                            //alldocs = documents + Environment.NewLine + alldocs;

                            PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                            notescelli1.Border = 0;
                            table.AddCell(notescelli1);


                            PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                            notescelli2.Border = 0;
                            table.AddCell(notescelli2);

                            PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                            notescelli3.Border = 0;
                            table.AddCell(notescelli3);

                            PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                            notescelli4.Border = 0;
                            table.AddCell(notescelli4);

                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));

                        }
                    }
                    else
                    {
                        PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                        notescellh1.Border = 0;
                        table.AddCell(notescellh1);
                        PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                        notescellh2.Border = 0;
                        table.AddCell(notescellh2);
                        PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                        notescellh3.Border = 0;
                        table.AddCell(notescellh3);
                        PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                        notescellh4.Border = 0;
                        table.AddCell(notescellh4);
                        PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded.", font));
                        notescellh5.Border = 0;
                        table.AddCell(notescellh5);
                        PdfPCell notescellh6 = new PdfPCell(new Phrase("", font));
                        notescellh6.Border = 0;
                        table.AddCell(notescellh6);
                        table.AddCell(notescellh6);
                        table.AddCell(notescellh6);
                    }


                    pdfDoc.Add(specificstable); //coaching specifics
                    pdfDoc.Add(new Paragraph(p11)); //coahee feedback
                    pdfDoc.Add(new Paragraph(p12)); //coacher feedback
                    pdfDoc.Add(new Paragraph(p13)); //docu header
                    pdfDoc.Add(table);//documentation


                    pdfDoc.Close();


                    Response.ContentType = "application/pdf";
                    string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                        + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                    string filenameheader = filename + ".pdf";
                    string filenameheadername = "filename=" + filenameheader;
                    Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();



                }
                catch (Exception ex)
                {
                    // Response.Write(ex.Message.ToString());
                    string ModalLabel = "btn_ExporttoPDF_Click " + ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }
            }
            else
            {
                //string script = "alert('Please submit the review before creating a pdf.')";
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "its working", script, true);
                string ModalLabel = "Please submit the review before creating a pdf.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }            
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        protected void btn_Audit1_Click(object sender, EventArgs e)
        {
            btn_Audit1.Attributes.Add("onclick", " CloseWindow();");

            //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);
            int reviewid;
            //decryption
            string coachid = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));

            //coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(coachid);
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);
            string val1 = Convert.ToString(reviewid);

            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

            Response.Redirect("~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2);

        }
        private void SendEmail(int CoachType, int ReviewID, string emailAddressCCM)
        {

            if (CoachType == 1)//Coachee
            {
                toName = "Remote Coaching Update";
                fromName = "Remote Coaching Update";
                fromEmail = "ras@nucomm.net";

                //string CCMEmail = emailAddressCCM;
                //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
                target = "http://q9vmdevapp06.nucomm.net/Coach2/Default.aspx?ReturnUrl=%2fCoach2%2f";// "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
                htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + RadCoacheeName.SelectedValue + "-"+ RadCoacheeName.SelectedItem.Text + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\".</font></p>";
                htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
                subject = "Coach Request with ReviewID " + ReviewID.ToString() + " ";
                toEmail = emailAddressCCM;
                EmailNotifs ws = new EmailNotifs();
                ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
            }
            else if (CoachType == 2)
            {

                toName = "Remote Coaching Update";
                fromName = "Remote Coaching Update";
                fromEmail = "ras@nucomm.net";

                //string CCMEmail = emailAddressCCM;
                //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
                target = "http://q9vmdevapp06.nucomm.net/Coach2/Default.aspx?ReturnUrl=%2fCoach2%2f";// "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
                htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + RadCoacheeName.SelectedValue + "-" + RadCoacheeName.SelectedItem.Text + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\".</font></p>";
                htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
                subject = "Coach Request with ReviewID " + ReviewID.ToString() + " ";
                toEmail = emailAddressCCM;
                EmailNotifs ws = new EmailNotifs();
                ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
            }//Coacher
            else
            {
                toName = "Remote Coaching Update";
                fromName = "Remote Coaching Update";
                fromEmail = "ras@nucomm.net";

                //string CCMEmail = emailAddressCCM;
                //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
                target = "http://q9vmdevapp06.nucomm.net/Coach2/Default.aspx?ReturnUrl=%2fCoach2%2f";//  "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
                htmlBody = "<p><font face=arial size=-1>A remote coaching ticket #" + ReviewID.ToString() + " has been updated.</font></p>";
                htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
                subject = "Coach Request with ReviewID " + ReviewID.ToString() + " ";
                toEmail = emailAddressCCM;
                EmailNotifs ws = new EmailNotifs();
                ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
            
            }
        }
    }
}