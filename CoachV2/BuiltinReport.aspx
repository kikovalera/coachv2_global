﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="BuiltinReport.aspx.cs" Inherits="CoachV2.BuiltinReport" %>
<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/DashboardMyReviewsReport.ascx" TagName="DashboardMyReviewsUserControl" TagPrefix="ucdash5" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
  
   <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="cb_sessiontype">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="cb_sessionTopic" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
                
            </telerik:AjaxSetting>
        <%--    <telerik:AjaxSetting AjaxControlID="cb_account">
            <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cb_LOB" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            </AjaxSettings>
            </telerik:RadAjaxManager>
    <div class="menu-content bg-alt">
        <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
        <div class="panel-group" id="accordion">
            <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Built in Report <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                     <asp:HiddenField ID="HiddenReportID" runat="server" Value="0" />
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblReportFilters" runat="server" Text="Report Filters"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cb_dashboards" EmptyMessage="Status" runat="server" AppendDataBoundItems="true"  DataValueField="TicketTypeID" DataTextField="TypeName">
                                        </telerik:RadComboBox>
<%--                                        <asp:RequiredFieldValidator ID="rq1" ControlToValidate="cb_dashboards" runat="server"   ></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cb_sessiontype" runat="server" EmptyMessage="Session Type"  AppendDataBoundItems="true"
                                         DataTextField="SessionName" DataValueField="SessionId" OnSelectedIndexChanged="cb_sessiontype_onselectedindex" AutoPostBack="true" >
                                        </telerik:RadComboBox>
                                    </td>
                                    <td>
                                        <telerik:RadComboBox ID="cb_sessiontopic" runat="server" EmptyMessage="Session Topic"                                        
                                        AppendDataBoundItems="true"  DataValueField="TopicID"  DataTextField="TopicName" AutoPostBack="true"> 
                                        </telerik:RadComboBox>
                                    </td>
<%--                                    <td>
                                        <telerik:RadComboBox ID="cb_users" runat="server" EmptyMessage="Users"  DataTextField="RoleName"  DataValueField="RoleId" AppendDataBoundItems="true" >                     
                                        </telerik:RadComboBox>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td>
                                            
                                    </td>
                                      <td>
                                        <telerik:RadComboBox ID="cb_users" runat="server" EmptyMessage="Users"  DataTextField="RoleName"  DataValueField="RoleId" AppendDataBoundItems="true" >                     
                                        </telerik:RadComboBox>
                                    </td> 
                                    <td>
                                        <telerik:RadComboBox ID="cb_department" runat="server" EmptyMessage="Department" AppendDataBoundItems="true" DataValueField="DepartmentID" DataTextField="Department" >
                                        </telerik:RadComboBox>
                                    </td>
                                   <%-- <td>
                                        <telerik:RadComboBox ID="cb_account" runat="server" EmptyMessage="Account"  AppendDataBoundItems="true"  DataValueField="DepartmentID" DataTextField="Department" >  
                                        </telerik:RadComboBox>
                                    </td>
                                                                        
                                       <td>
                                        <telerik:RadComboBox ID="cb_LOB" runat="server" EmptyMessage="LOB" AppendDataBoundItems="true" DataValueField="LOBiD" DataTextField="LOB">
                                        </telerik:RadComboBox>     
                                       </td>--%>
                                </tr>
                                <tr>
                                    <td> 
                                        <asp:Label ID="lblDates" runat="server" Text="Select Date Range"></asp:Label>                                        
                                    </td>
                                    <td>
                                          <%--     <telerik:RadDatePicker ID="dp_start" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                        DateInput-DateFormat="yyyy-MM-dd"    >--%>
                                       <telerik:RadDatePicker ID="dp_start" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker> 
                                       <%-- <asp:RequiredFieldValidator ID="rq7" ControlToValidate="dp_start" runat="server"  ErrorMessage="*"   ></asp:RequiredFieldValidator>                                    
               --%>                        
                                    </td>                                   
                                     <td>                                     
                                      <%--   <telerik:RadDatePicker ID="dp_end" runat="server" placeholder="Enter start date"
                        DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"  
                        DateInput-DateFormat="yyyy-MM-dd"  >
                                         </telerik:RadDatePicker>--%>
                                          <telerik:RadDatePicker ID="dp_end" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker> 
                                        <%--<asp:RequiredFieldValidator ID="rq8" ControlToValidate="dp_end" runat="server" ErrorMessage="*"  ></asp:RequiredFieldValidator>                                    
--%>
                                    </td>
                                    <td>
                                        <telerik:RadButton id="btnViewReport" runat="server"  Text="View Report"  OnClick="btnViewReport_onClick" CausesValidation="false" ></telerik:RadButton>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function dp_dateselected(sender, args) {

         
            var RadDatePicker1 = $find("<%= dp_start.ClientID %>");
            var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

            var RadDatePicker2 = $find("<%= dp_end.ClientID %>");
            var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");
         
        }
        </script>
</asp:Content>


