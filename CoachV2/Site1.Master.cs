﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CoachV2.AppCode;
using System.Data;

namespace CoachV2
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetCoachLinks();
        }
        protected void LoginStatus1_LoggedOut(Object sender, System.EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("~/Default.aspx?ReturnUrl=" + HttpContext.Current.Request.Url.AbsoluteUri);
        }
        private void GetCoachLinks()
        {
            DataAccess ws = new DataAccess();
            DataSet ds = null;
            ds = ws.GetCoachLinks();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string ID = row.Field<int>("ID").ToString();

                    if (ID == "1")
                    {
                        Home.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            Home.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "2")
                    {
                        About.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            About.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "3")
                    {
                        Contact.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            Contact.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "4")
                    {
                        Guidelines.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            Guidelines.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "5")
                    {
                        Copyright.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            Copyright.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "6")
                    {
                        TU.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            TU.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "7")
                    {
                        CR.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            CR.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }
                    if (ID == "8")
                    {
                        OT.Text = row.Field<string>("Description").ToString();
                        if (row.Field<string>("Link").ToString() != null)
                        {
                            OT.NavigateUrl = row.Field<string>("Link").ToString();
                        }
                    }


                }

            }

        }

    }
}