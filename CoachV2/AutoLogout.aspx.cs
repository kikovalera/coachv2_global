﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net;
using System.Diagnostics;
using Microsoft.AspNet.Membership.OpenAuth;

namespace CoachV2
{
    public partial class AutoLogout : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Process.Start("https://accounts.google.com/Logout");
            Response.Write("<script language=javascript>this.window.opener = null;window.open('','_self'); window.close();   </script>");
        }
    }
}