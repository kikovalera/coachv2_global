﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using CoachV2.AppCode;

namespace CoachV2
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterOpenAuth();
            //RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        //    Response.AppendHeader("Pragma", "no-cache");
        //    Response.AppendHeader("Expires", "0");

        //    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //Response.Cache.SetExpires(DateTime.Now);
        //    //Response.Cache.SetNoStore();
        //    //Response.Cache.SetMaxAge(new TimeSpan(0, 0, 30));
        //}

        void Application_Error(object sender, EventArgs e)
        {
            //USE THIS TO LOG EXCEPTION AND RETAIN DEFAULT YSOD
            //LogMe.InJson(Server.GetLastError(), "Logged from CoachV2GlobalErrorHandler");
            //Server.ClearError();
            //Response.TrySkipIisCustomErrors = true;

            //USE THIS TO LOG EXCEPTION AND REDIRECT (AND DISPLAY ERRORMSG) TO HOMEPAGE
            //Session["globalexception"] = Server.GetLastError();
            //LogMe.InJson(Server.GetLastError(), "Logged from CoachV2GlobalErrorHandler");
            //string strlogid = "";
            //strlogid = LogMe.InJson(Server.GetLastError(), "Logged from CoachV2GlobalErrorHandler");
            //Response.Redirect("~/Default.aspx?errlogid=" + strlogid);

        }
    }
}