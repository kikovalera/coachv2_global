﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
namespace CoachV2
{
    public partial class setuprole : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string category = string.Empty;
            string val = string.Empty;
            try
            {
                category = Request["cat"].ToString();
                val = Request["val"].ToString();
            }
            catch (NullReferenceException)
            {
                return;
            }
            string handlerUrl = "/setup_role.ashx?cat="+ category +"&val="+val;
            string response = (new WebClient()).DownloadString(handlerUrl);
            Label1.Text = response;
        }
    }
}