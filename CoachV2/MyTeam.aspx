﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyTeam.aspx.cs" Inherits="CoachV2.MyTeam" %>
<%@ Register src="UserControl/MyTeamPerfUserCtrl.ascx" tagname="MyTeamPerfUserCtrl" tagprefix="uc1" %>
<%@ Register src="UserControl/MyTeamProfileUserCtrl.ascx" tagname="MyTeamProfileUserCtrl" tagprefix="uc2" %>
<%@ Register src="UserControl/MyTeamLogsUserCtrl.ascx" tagname="MyTeamLogsUserCtrl" tagprefix="uc3" %>
<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarDashboardUserControl" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc4:SidebarDashboardUserControl ID="SidebarDashboardUserControl1" 
    runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <uc1:MyTeamPerfUserCtrl ID="MyTeamPerfUserCtrl1" runat="server" />
<uc2:MyTeamProfileUserCtrl ID="MyTeamProfileUserCtrl1" runat="server" />
<uc3:MyTeamLogsUserCtrl ID="MyTeamLogsUserCtrl1" runat="server" />
</asp:Content>
