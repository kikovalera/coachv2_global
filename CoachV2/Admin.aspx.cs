﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;


namespace CoachV2.Content
{
    public partial class Admin : System.Web.UI.Page //System.Web.UI.UserControl
    {
        const int MaxTotalBytes = 1048576; // 1 MB
        Int64 totalBytes;


        public bool? IsRadAsyncValid
        {
            get
            {
                if (Session["IsRadAsyncValid"] == null)
                {
                    Session["IsRadAsyncValid"] = true;
                }

                return Convert.ToBoolean(Session["IsRadAsyncValid"].ToString());
            }
            set
            {
                Session["IsRadAsyncValid"] = value;
            }
        }


        protected void RadGrid2_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                RadAsyncUpload upload = ((GridEditableItem)e.Item)["Upload"].FindControl("AsyncUpload1") as RadAsyncUpload;
                TableCell cell = (TableCell)upload.Parent;

                CustomValidator validator = new CustomValidator();
                validator.ErrorMessage = "Please select file to be uploaded";
                validator.ClientValidationFunction = "validateRadUpload";
                validator.Display = ValidatorDisplay.Dynamic;
                cell.Controls.Add(validator);
            }
        }

        protected string TrimDescription(string description)
        {
            if (!string.IsNullOrEmpty(description) && description.Length > 200)
            {
                return string.Concat(description.Substring(0, 200), "...");
            }
            return description;
        }

        protected void RadGrid2_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DataAccess ws = new DataAccess();
            DataSet ds = null;
            ds = ws.GetCoachLinks();
            if (ds.Tables[0].Rows.Count > 0)
            {
                RadGrid2.DataSource = ds;
            }
        }

        protected void RadGrid2_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (!IsRadAsyncValid.Value)
                {
                    e.Canceled = true;
                    RadAjaxManager1.Alert("The length of the uploaded file must be less than 1 MB");
                    return;
                }

                GridEditableItem editedItem = e.Item as GridEditableItem;
                int ID = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ID"].ToString());
                string imageName = (editedItem["FileName"].FindControl("txbFileName") as RadTextBox).Text;
                string description = (editedItem["Description"].FindControl("txbDescription") as RadTextBox).Text;
                string link = (editedItem["Link"].FindControl("txbLink") as RadTextBox).Text;
                RadAsyncUpload radAsyncUpload = editedItem["Upload"].FindControl("AsyncUpload1") as RadAsyncUpload;

                //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                int CIMNumber = Convert.ToInt32(cim_num);

                if (radAsyncUpload.UploadedFiles.Count > 0)
                {

                    foreach (UploadedFile f in radAsyncUpload.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + description + "-" + ID + f.GetExtension());
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.UpdateCoachLinks(ID, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + description + "-" + ID + f.GetExtension(), CIMNumber, f.GetNameWithoutExtension(), description);
                    }
                }
                else
                {
                    DataAccess ws = new DataAccess();
                    ws.UpdateCoachLinks(ID, link, CIMNumber, DBNull.Value.ToString(), description);

                }

                string ModalLabel = "Successfully updated data.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
            catch (Exception ex)
            {
                string ModalLabel = "RadGrid2_UpdateCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }

        protected void RadGrid2_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetEditMode", "isEditMode = true;", true);
            }
        }
        protected void AsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            if ((totalBytes < MaxTotalBytes) && (e.File.ContentLength < MaxTotalBytes))
            {
                e.IsValid = true;
                totalBytes += e.File.ContentLength;
                IsRadAsyncValid = true;
            }
            else
            {
                e.IsValid = false;
                IsRadAsyncValid = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            IsRadAsyncValid = null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

       
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            int int_cim = Convert.ToInt32(cim_num);

            DataSet ds_drivers = DataHelper.GetDriversLookup();
            grd_Drivers.DataSource = ds_drivers;
            btnKPI.BackColor = System.Drawing.Color.DarkGray;

            DataSet ds_session = DataHelper.GetSessionsLookup();
            grd_session.DataSource = ds_session;
            grd_session.DataBind();

            DataSet ds_account = DataHelper.GetAccountsLookup();
            grd_Account.DataSource = ds_account;
            grd_Account.DataBind();

            DataSet ds_supervisor = DataHelper.GetSupervisorLookup();
            grd_Supervisor.DataSource = ds_supervisor;
            grd_Supervisor.DataBind();

            DataSet ds_KPIList = DataHelper.GetKPIIList_admin();
            grd_KPILIst.DataSource = ds_KPIList;
            grd_KPILIst.DataBind();

            //DataSet ds_KPIList2 = DataHelper.GetKPIIList_admin();
            //RadGrid1.DataSource = ds_KPIList2;
            //lblForKpiCreated.Text = "KPIs Assigned by Admin : " + Convert.ToString(ds_KPIList2.Tables[0].Rows.Count);

            if (!IsPostBack)
            {

                GetHierarchy();
                DataSet ds_KPIList2 = DataHelper.GetTopKPIIList_admin();
 //               RadGrid1.DataSource = ds_KPIList2;
                lblForKpiCreated.Text = "KPIs Assigned by Admin : " + Convert.ToString(ds_KPIList2.Tables[0].Rows.Count);

                DataSet ds_userselect = DataHelper.GetAllUserRoles(int_cim);
                var mark_13 = "0";
                var mark_14 = "0";
                var mark_15 = "0";
                //for kpi addition

                RadGridUpload.DataSource = string.Empty;
                RadGridUpload.Rebind();
                LoadUploadedItems();

                if (ds_userselect.Tables[0].Rows.Count > 0) //13,14,15
                {
                    for (int ctr = 0; ctr < ds_userselect.Tables[0].Rows.Count; ctr++)
                    {


                        DataSet ds_userselect13 = DataHelper.GetUserRolesAssigned(int_cim, 13);
                        if (ds_userselect13.Tables[0].Rows.Count > 0)
                        {
                            mark_13 = "1";
                        }
                        DataSet ds_userselect14 = DataHelper.GetUserRolesAssigned(int_cim, 14);
                        if (ds_userselect14.Tables[0].Rows.Count > 0)
                        {
                            mark_14 = "1";
                        }
                        DataSet ds_userselect15 = DataHelper.GetUserRolesAssigned(int_cim, 15);
                        if (ds_userselect15.Tables[0].Rows.Count > 0)
                        {
                            mark_15 = "1";
                        }

                    }

                    if ((mark_13 == "1") && (mark_14 == "0") && (mark_15 == "0")) //13 only
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;

                        Pn_KPI.Visible = false;
                        ic_kpi.Visible = true;

                        Pn_Lookup.Visible = true;

                    }
                    else if ((mark_13 == "0") && (mark_14 == "1") && (mark_15 == "0")) //14only
                    {
                        Pn_Admin.Visible = false;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = false;
                    }
                    else if ((mark_13 == "0") && (mark_14 == "0") && (mark_15 == "1")) //15only
                    {
                        Pn_Admin.Visible = true;
                        Pn_KPI.Visible = false;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "1") && (mark_15 == "1")) //13,14,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;

                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "1") && (mark_15 == "0")) //13,14
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "1") && (mark_14 == "0") && (mark_15 == "1")) //13,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = false;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }
                    else if ((mark_13 == "0") && (mark_14 == "1") && (mark_15 == "1")) //14,15
                    {
                        Pn_Admin.Visible = true;
                        ic_users.Visible = true;
                        Pn_KPI.Visible = true;
                        ic_kpi.Visible = true;
                        Pn_Lookup.Visible = true;
                    }

                }
                else
                {


                }

            }

        }

        protected void display_access()
        {


            int role_id = 0;

            //int cim_val = Convert.ToInt32(txtCimnumber.Text);

            //DataSet ds_userselect = DataHelper.GetAllUserRoles(Convert.ToInt32(txtCimnumber.Text));
            try
            {

                int cim_val = Convert.ToInt32(txtCimnumber.Text);

                DataSet ds_userselect = DataHelper.GetAllUserRoles(Convert.ToInt32(txtCimnumber.Text));
          
                if (ds_userselect.Tables[0].Rows.Count > 0) //ds_userroles.Tables[0].Rows.Count > 0)
                {

                    //cb_Login : 1
                    //cb_AddReview : 2
                    //cb_perfsummary : 3
                    //cb_reviewLogs : 4
                    //cb_profile : 5
                    //cb_addnewreview : 6
                    //cb_addMassReview : 7
                    //cb_remotecoach : 8
                    //cb_searchReviews : 9
                    //cb_ReportBuilder : 10
                    //cb_BuiltInReports : 11
                    //cb_SavedReports : 12
                    //cb_Users : 13
                    //cb_KPI : 14
                    //cb_SearchUsers : 15
                    //cb_EditProfile : 16
                    //cb_qareview : 18 
                    for (int ctr = 0; ctr < ds_userselect.Tables[0].Rows.Count; ctr++)
                    {
                        string user_roleid = ds_userselect.Tables[0].Rows[ctr][1].ToString();

                        if (user_roleid == "1")
                        {
                            cb_Login.Checked = true;
                        }

                        if (user_roleid == "2")
                        {
                            cb_AddReview.Checked = true;
                        }

                        if (user_roleid == "3")
                        {
                            cb_perfsummary.Checked = true;
                        }

                        if (user_roleid == "4")
                        {
                            cb_reviewLogs.Checked = true;
                        }

                        if (user_roleid == "5")
                        {
                            cb_profile.Checked = true;
                        }

                        if (user_roleid == "6")
                        {
                            cb_addnewreview.Checked = true;
                        }

                        if (user_roleid == "7")
                        {
                            cb_addMassReview.Checked = true;
                        }

                        if (user_roleid == "8")
                        {
                            cb_remotecoach.Checked = true;
                        }

                        if (user_roleid == "9")
                        {
                            cb_searchReviews.Checked = true;
                        }

                        if (user_roleid == "10")
                        {
                            cb_ReportBuilder.Checked = true;
                        }

                        if (user_roleid == "11")
                        {
                            cb_BuiltInReports.Checked = true;
                        }


                        if (user_roleid == "12")
                        {
                            cb_SavedReports.Checked = true;
                        }

                        if (user_roleid == "13")
                        {
                            cb_Users.Checked = true;
                        }

                        if (user_roleid == "14")
                        {
                            cb_KPI.Checked = true;
                        }

                        if (user_roleid == "15")
                        {
                            cb_SearchUsers.Checked = true;
                        }

                        if (user_roleid == "16")
                        {
                            cb_EditProfile.Checked = true;
                        }

                        if (user_roleid == "17")
                        {
                            cb_notifications.Checked = true;
                        }

                        if (user_roleid == "18")
                        {
                            cb_qareview.Checked = true;
                        }
                    }
                }
                else
                {
                    string ModalLabel = "Please enter valid Cim!";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);



                }
            }
            catch (Exception ex)
            {
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            if (txtCimnumber.Text != string.Empty)
            {
                display_access();

                string ModalLabel = "User Role for " + txtCimnumber.Text + " displayed!";
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                //ClientScript.RegisterStartupScript("LaunchServerSide", "<script> $(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });<script> ");
                //string message = "User Role for " + txtCimnumber.Text + " displayed!";
                // ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert(\"" + message + "\");", true);

            }

            else
            {
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('Please enter Valid Cim Number.');", true);

                string ModalLabel = "Please enter Valid Cim Number.";
                string ModalHeader = "Error Message";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        protected void btn_submit_OnClick(object sender, EventArgs e)
        {

            if (txtCimnumber.Text != string.Empty)
            {

                DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string gmailaccount = ds.Tables[0].Rows[0][13].ToString().ToLower();


                int role_id = 0;
                int cim_val = Convert.ToInt32(txtCimnumber.Text);
                string cim_num = ds.Tables[0].Rows[0][2].ToString();
                DataSet ds_userselect = DataHelper.GetAllUserRoles(cim_val);

                //cannot edit own profile,
                if (Convert.ToString(txtCimnumber.Text) == cim_num)
                {
                    string ModalLabel = "Editting own access is not allowed!";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                    //string message = "Editting own access is not allowed!";
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                    //    "alert(\"" + message + "\");", true);
                }
                else if (Convert.ToString(txtCimnumber.Text) != cim_num)
                {
                    try
                    {
                        if (ds_userselect.Tables[0].Rows.Count > 0)
                        {

                            if (cb_Login.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);

                                DataHelper.UpdateUserToInactive(Convert.ToInt32(txtCimnumber.Text)); //set user to inactive
                                DataHelper.RemoveUserRolesAssigned(cim_val, 1); //remove for log in
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 1);

                                try
                                {

                                    if (ds_userselect1.Tables[0].Rows.Count == 0)
                                    {
                                        role_id = 1;
                                        int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                        DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                                        DataHelper.UpdateUserToActive(Convert.ToInt32(txtCimnumber.Text)); //set user to active


                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ModalLabel = ex.Message.ToString();
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }



                            }


                            if (cb_AddReview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 2);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 2);

                                try
                                {

                                    if (ds_userselect1.Tables[0].Rows.Count == 0)
                                    {
                                        role_id = 2;
                                        int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                        DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ModalLabel = ex.Message.ToString();
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }

                            }

                            if (cb_perfsummary.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 3);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 3);

                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 3;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);

                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                                }


                            }

                            if (cb_reviewLogs.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 4);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 4);

                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 4;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_profile.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 5);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 5);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 5;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_addnewreview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 6);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 6);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 6;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_addMassReview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 7);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 7);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 7;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }


                            if (cb_remotecoach.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 8);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 8);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 8;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }


                            if (cb_searchReviews.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 9);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 9);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 9;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_ReportBuilder.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 10);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 10);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 10;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_BuiltInReports.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 11);

                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 11);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 11;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }


                            if (cb_SavedReports.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 12);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 12);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 12;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }
                            }

                            if (cb_Users.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 13);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 13);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 13;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_KPI.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 14);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 14);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 14;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_SearchUsers.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 15);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 15);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 15;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_EditProfile.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 16);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 16);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 16;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }
                            //for notifications
                            if (cb_notifications.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 17);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 17);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 17;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                            if (cb_qareview.Checked == false)
                            {
                                int cimval = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.RemoveUserRolesAssigned(cim_val, 18);
                            }
                            else
                            {
                                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(txtCimnumber.Text), 18);
                                if (ds_userselect1.Tables[0].Rows.Count == 0)
                                {
                                    role_id = 18;
                                    int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                    DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                                }

                            }

                        }
                        else //for first time role assigning
                        {
                            if (cb_Login.Checked == true)
                            {
                                role_id = 1;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);


                            }

                            if (cb_AddReview.Checked == true)
                            {
                                role_id = 2;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_perfsummary.Checked == true)
                            {
                                role_id = 3;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_reviewLogs.Checked == true)
                            {
                                role_id = 4;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_profile.Checked == true)
                            {
                                role_id = 5;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_addnewreview.Checked == true)
                            {
                                role_id = 6;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_addMassReview.Checked == true)
                            {
                                role_id = 7;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_remotecoach.Checked == true)
                            {
                                role_id = 8;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_searchReviews.Checked == true)
                            {
                                role_id = 9;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_ReportBuilder.Checked == true)
                            {
                                role_id = 10;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_BuiltInReports.Checked == true)
                            {
                                role_id = 11;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }


                            if (cb_SavedReports.Checked == true)
                            {
                                role_id = 12;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_Users.Checked == true)
                            {
                                role_id = 13;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_KPI.Checked == true)
                            {
                                role_id = 14;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_SearchUsers.Checked == true)
                            {
                                role_id = 15;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);
                            }

                            if (cb_EditProfile.Checked == true)
                            {
                                role_id = 16;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_notifications.Checked == true)
                            {
                                role_id = 17;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }

                            if (cb_qareview.Checked == true)
                            {
                                role_id = 18;
                                int cimvalue = Convert.ToInt32(txtCimnumber.Text);
                                DataHelper.InsertUserRole(cimvalue, role_id, cim_num, DateTime.Now);

                            }
                        }
                        display_access();
                        string ModalLabel1 = "User Role for " + cim_val + " saved!";
                        string ModalHeader1 = "Success Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader1 + "','" + ModalLabel1 + "'); });", true);

                        //string message = "User Role for " + cim_val + " saved!";
                        //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                        //    "alert(\"" + message + "\");", true);
                    }
                    catch (Exception ex)
                    {
                        string ModalLabel = ex.Message.ToString();
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }
            else
            {

                string ModalLabel = "Please enter Valid Cim Number.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('Please enter Valid Cim Number.');", true);
            }
        }

        protected void btn_reset_OnClick(object sender, EventArgs e)
        {
            txtCimnumber.Text = "";
            cb_Login.Checked = false;
            cb_AddReview.Checked = false;
            cb_perfsummary.Checked = false;
            cb_reviewLogs.Checked = false;
            cb_profile.Checked = false;
            cb_addnewreview.Checked = false;
            cb_addMassReview.Checked = false;
            cb_remotecoach.Checked = false;
            cb_searchReviews.Checked = false;
            cb_ReportBuilder.Checked = false;
            cb_BuiltInReports.Checked = false;
            cb_SavedReports.Checked = false;
            cb_Users.Checked = false;
            cb_KPI.Checked = false;
            cb_SearchUsers.Checked = false;
            cb_EditProfile.Checked = false;
            cb_qareview.Checked = false;

        }

        protected void btn_Button1(object sender, EventArgs e)
        {
            if ((cb_kpiname.SelectedValue != "") && (cb_account.SelectedValue != ""))
            {
                try
                {
                    //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                    int int_cim = Convert.ToInt32(cim_num);

                    DataSet ds_kpiidlast = DataHelper.GetKPIIDfromScorecardv2(Convert.ToInt32(cb_kpiname.SelectedValue));
       
                    if (ds_kpiidlast.Tables[0].Rows.Count > 0)
                    {


                        //int kpiid = Convert.ToInt32(ds_kpiidlast.Tables[0].Rows[0][0]);
                        int kpiid = Convert.ToInt32(cb_kpiname.SelectedValue.ToString());
                        string KPItrim1 = Convert.ToString(ds_kpiidlast.Tables[0].Rows[0]["KPIName"]);

                        DataSet ds_ifexist = DataHelper.GetAssignedKPIv2(Convert.ToInt32(cb_kpiname.SelectedValue), Convert.ToInt32(cb_account.SelectedValue));
                        if (ds_ifexist.Tables[0].Rows.Count > 0)
                        {
                            string ModalLabel = "KPI : " + KPItrim1 + " is already Assigned to : " + cb_account.Text;
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            RadGrid1.Focus();
                        }
                        else
                        {
                            DataHelper.InsertNewKPIv2(Convert.ToInt32(kpiid), Convert.ToString(KPItrim1), Convert.ToString(KPItrim1), Convert.ToInt32(cb_valuetype.SelectedValue), Convert.ToInt32(cb_linkto.SelectedValue), cim_num, 0, Convert.ToInt32(cb_account.SelectedValue), DateTime.Now, Convert.ToInt32(RadHierarchy2.SelectedValue));
                           Response.AppendHeader("Refresh", "0.1");

                            string ModalLabel = "KPI : " + cb_kpiname.Text + " Assigned to : " + cb_account.Text;
                            string ModalHeader = "Success Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                            RadGrid1.Focus();
                        }

                    }

                }
                catch (Exception ex)
                {
                    string ModalLabel = ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            else
            {

                string ModalLabel = "Please select KPI Name / Account!";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


            }
        }

        protected void GetAccount(int CIMNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                cb_account.DataSource = ds;
                cb_account.DataTextField = "Account";
                cb_account.DataValueField = "AccountID";
                cb_account.DataBind();

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //// ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //    "alert('" + myStringVariable + "');", true);
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }

        protected void loadkpidetails()
        {

            DataSet ds_kpifromscorecard = DataHelper.GetKPIFromScorecardList();//.GetKPIFromScorecard();
            cb_kpiname.DataSource = ds_kpifromscorecard;
            cb_kpiname.DataBind();

            DataSet ds_link_to = DataHelper.GetKPILinkTo();
            cb_linkto.DataSource = ds_link_to;
            cb_linkto.DataBind();

            DataSet ds_valueto = DataHelper.GetValueto();
            cb_valuetype.DataSource = ds_valueto;
            cb_valuetype.DataBind();
        }

        //OnUpdateCommand
        protected void radgrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
               if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
            {
                try
                {
                    //DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];                    
                    GridEditFormItem updateItem = (GridEditFormItem)e.Item;
                    int rowindex = updateItem.ItemIndex;
                    Label kpiid = (Label)updateItem.FindControl("txtRow");
                    RadComboBox RadKPI = (RadComboBox)updateItem.FindControl("cb_kpiname");
                    RadComboBox Radvalue = (RadComboBox)updateItem.FindControl("cb_valuetype");
                    RadComboBox Radlinkto = (RadComboBox)updateItem.FindControl("cb_linkto");
                    RadComboBox RadAccount = (RadComboBox)updateItem.FindControl("cb_account");
                    RadComboBox RadHierarchy = (RadComboBox)updateItem.FindControl("cb_hierarchy");
                    string kpiname_text = RadKPI.SelectedItem.Text;

                    DataAccess ws = new DataAccess();
                    ws.UpdateKPIAdminv2(Convert.ToInt32(kpiid.Text),Convert.ToInt32(Radvalue.SelectedValue), Convert.ToInt32(Radlinkto.SelectedValue), Convert.ToString(cim_num), Convert.ToInt32(RadAccount.SelectedValue), DateTime.Now, Convert.ToInt32(RadHierarchy.SelectedValue));                        
                    string ModalLabel = "Update on KPI : " + kpiname_text + " Sucessful!";
                    string ModalHeader = "Success Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    RadGrid1.Focus();
                }
                catch (Exception ex)
                {
                    string ModalLabel = ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
                RadGrid1.MasterTableView.ClearEditItems();
                Response.AppendHeader("Refresh", "0.1");
                RadGrid1.Focus();
            }
         
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
               // DataSet ds = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];


                DataHelper.DeleteKPIAdmin(Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("Row")), Convert.ToString(cim_num), DateTime.Now);
                //string myStringVariable = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!"; 
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                //           "alert('" + myStringVariable + "');", true);

                string ModalLabel = "KPI: " + ((GridDataItem)e.Item).GetDataKeyValue("Num") + " Deleted!";
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                Response.AppendHeader("Refresh", "0.1");
                RadGrid1.Focus();

            }
            catch (Exception ex)
            {
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                RadGrid1.Focus();
            }
        }

        protected void radgrid1_databound(object sender, GridItemEventArgs e)
        {

            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
             {
                   
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("cb_kpiname");
                    DataSet ds = new DataSet();
                    //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
                    ds = DataHelper.GetKPIFromScorecardListv2(); //GetKPIFromScorecard();
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    //comboBrand.SelectedValue = selectedItem.Value;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("cb_kpiname");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPI Name"].ToString();
                    // selectedItem.Value = ((DataRowView)e.Item.DataItem)["KPI_ID"].ToString();
                    selectedItem.Value = ((DataRowView)e.Item.DataItem)["KPIID"].ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI Name"] = selectedItem.Value;
                    combo.SelectedValue = selectedItem.Value;

                    RadComboBox combo1 = (RadComboBox)edititem.FindControl("cb_valuetype");
                    RadComboBoxItem selectedItem1 = new RadComboBoxItem();
                    selectedItem1.Text = ((DataRowView)e.Item.DataItem)["Value Type"].ToString();
                    selectedItem1.Value = ((DataRowView)e.Item.DataItem)["valuetype"].ToString();
                    combo1.Items.Add(selectedItem1);
                    selectedItem1.DataBind();
                    Session["Value Type"] = selectedItem.Value;
                    RadComboBox comboBrand1 = (RadComboBox)edititem.FindControl("cb_valuetype");
                    DataSet ds1 = new DataSet();
                    //ds = DataHelper.GetKPIIList_admin(); //ws.GetKPIList();
                    ds1 = DataHelper.GetValueto();
                    comboBrand1.DataSource = ds1;
                    comboBrand1.DataBind();
                    comboBrand1.SelectedValue = selectedItem1.Value;

                    RadComboBox combo2 = (RadComboBox)edititem.FindControl("cb_linkto");
                    RadComboBoxItem selectedItem2 = new RadComboBoxItem();
                    selectedItem2.Text = ((DataRowView)e.Item.DataItem)["Assign Type"].ToString();
                    selectedItem2.Value = ((DataRowView)e.Item.DataItem)["linkto"].ToString();
                    combo2.Items.Add(selectedItem2);
                    selectedItem2.DataBind();
                    Session["Assign Type"] = selectedItem2.Value;
                    RadComboBox comboBrand2 = (RadComboBox)edititem.FindControl("cb_linkto");
                    DataSet ds2 = new DataSet();
                    ds2 = DataHelper.GetKPILinkTo();
                    comboBrand2.DataSource = ds2;
                    comboBrand2.DataBind();
                    comboBrand2.SelectedValue = selectedItem2.Value;

                    RadComboBox combo3 = (RadComboBox)edititem.FindControl("cb_account");
                    RadComboBoxItem selectedItem3 = new RadComboBoxItem();
                    selectedItem3.Text = ((DataRowView)e.Item.DataItem)["Assigned to"].ToString();
                    selectedItem3.Value = ((DataRowView)e.Item.DataItem)["ACCOUNTID"].ToString();
                    combo3.Items.Add(selectedItem3);
                    selectedItem3.DataBind();
                    Session["Assigned to"] = selectedItem3.Value;
                    RadComboBox comboBrand3 = (RadComboBox)edititem.FindControl("cb_account");
                    DataSet ds3 = new DataSet();
                    //ds3 = DataHelper.GetAccountsforKPI();
                    DataAccess ws3 = new DataAccess();
                    ds3 = ws3.GetLKPKPIAccounts();
                    comboBrand3.DataSource = ds3;
                    comboBrand3.DataBind();
                    comboBrand3.SelectedValue = selectedItem3.Value;

                    RadComboBox combo4 = (RadComboBox)edititem.FindControl("cb_hierarchy");
                    RadComboBoxItem selectedItem4 = new RadComboBoxItem();
                    //selectedItem4.Text = ((DataRowView)e.Item.DataItem)["hierarchy_name"].ToString();
                    selectedItem4.Value = ((DataRowView)e.Item.DataItem)["hierarchy"].ToString();
                    combo4.Items.Add(selectedItem4);
                    selectedItem4.DataBind();
                    Session["hierarchy"] = selectedItem4.Value;
                    RadComboBox comboBrand4 = (RadComboBox)edititem.FindControl("cb_hierarchy");

                    combo4.Items.Clear();

                    for (int hierarchy = 0; hierarchy <= 5; hierarchy++)
                    {
                        string hierarchy_name;
                        if (hierarchy == 0)
                        {
                            hierarchy_name = "";
                            combo4.Items.Add(new Telerik.Web.UI.RadComboBoxItem(hierarchy_name, hierarchy.ToString()));
                        }
                        else
                        {
                            hierarchy_name = hierarchy.ToString();
                            combo4.Items.Add(new Telerik.Web.UI.RadComboBoxItem(hierarchy_name.ToString(), hierarchy.ToString()));
                        }
                    }

                    comboBrand4.SelectedValue = selectedItem4.Value;
                
            }

        }

        protected void btnKPI_onclick(object sender, EventArgs e)
        {
            btnKPI.BackColor = System.Drawing.Color.DarkGray;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSession.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.Empty;
            grd_Account.Visible = false;
            grd_Supervisor.Visible = false;
            grd_session.Visible = false;
            grd_Drivers.Visible = true;
            grd_Drivers.Focus();

        }

        protected void btnSession_onclick(object sender, EventArgs e)
        {
            btnSession.BackColor = System.Drawing.Color.DarkGray;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.Empty;

            grd_Account.Visible = false;
            grd_Supervisor.Visible = false;
            grd_session.Visible = true;
            grd_Drivers.Visible = false;
            grd_session.Focus();
        }

        protected void btnAcct_onclick(object sender, EventArgs e)
        {
            btnSession.BackColor = System.Drawing.Color.Empty;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.DarkGray;
            btnSup.BackColor = System.Drawing.Color.Empty;
            grd_session.Visible = false;
            grd_Supervisor.Visible = false;
            grd_Account.Visible = true;
            grd_Drivers.Visible = false;
            grd_Account.Focus();
        }

        protected void btnSup_onclick(object sender, EventArgs e)
        {
            btnSession.BackColor = System.Drawing.Color.Empty;
            btnKPI.BackColor = System.Drawing.Color.Empty;
            btnAcct.BackColor = System.Drawing.Color.Empty;
            btnSup.BackColor = System.Drawing.Color.DarkGray;
            grd_Account.Visible = false;
            grd_Supervisor.Visible = true;
            grd_session.Visible = false;
            grd_Drivers.Visible = false;
            grd_Supervisor.Focus();
        }

        protected void RadGridUpload_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }

        protected void btnDummy_Click(object sender, EventArgs e)
        {
            string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
            foreach (UploadedFile f in RadUpload.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = @"C:\Documentation\";
                string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                string path = f.GetNameWithoutExtension() + "-" + datename + " - " + cim_num + f.GetExtension();
                LabelPath.Text = f.GetNameWithoutExtension() + "-" + datename + " - " + cim_num + f.GetExtension();
                //f.SaveAs(targetFolder + path);
                f.SaveAs(Path.Combine(targetFolder, path));
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                LabelHost.Text = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                //ws.InsertUploadedAgentPerformance(f.GetName(), Convert.ToInt32(cim_num), host + "/Documentation/" + path);
                HiddenFileName.Value = f.GetName();
                UpdateData(targetFolder + LabelPath.Text);

                LoadUploadedItems();

                string ModalLabel = "Successfully uploaded file.";
                string ModalHeader = "Success.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        public void LoadUploadedItems()
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetUploadedDocuments(0, 4);

            RadGridUpload.DataSource = ds;
            RadGridUpload.Rebind();
        }

        public void UpdateData(string excelfilepath)
        {
            try
            {
                string cim_num = HttpContext.Current.User.Identity.Name.Split('|')[3];
                //String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", excelfilepath);
                String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelfilepath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                DataSet data = new DataSet();
                HiddenUploadID.Value = "0";
                foreach (var sheetName in GetExcelSheetNames(excelConnString))
                {
                    if (sheetName != null)
                    {
                        using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                        {
                            using (OleDbCommand cmd = new OleDbCommand("Select DISTINCT [CIM],[NAME],[LOB],[DATE],[KPI],[TARGET],[SCORE],[SUPERVISOR CIM],[SUPERVISOR NAME],[NUMERATOR],[DENOMINATOR] from [" + sheetName + "]", excelConnection)) 
                            {
                                excelConnection.Open();
                                if (CheckIfColumnsExists(excelConnection, sheetName) == true)
                                {
                                    if (HiddenUploadID.Value == "0")
                                    {
                                        DataAccess ws1 = new DataAccess();
                                        int UploadID = ws1.InsertUploadedAgentPerformance(HiddenFileName.Value, Convert.ToInt32(cim_num), LabelHost.Text + "/Documentation/" + LabelPath.Text);
                                        HiddenUploadID.Value = UploadID.ToString();
                                    }

                                    string TableName = "TmpTableBIData";
                                    //string AgentPerformanceTable = "AgentPerformance3"; // changed to AgentPerformance (francis.valera/09182018)
                                    string AgentPerformanceTable = "AgentPerformance";
                                    DataTable dt = new DataTable("MyTable");
                                    dt = ConvertExcelToDataTable(excelfilepath, sheetName);
                                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["cn_CoachV2"].ConnectionString))
                                    {
                                        using (SqlCommand command = new SqlCommand("", conn))
                                        {
                                            try
                                            {
                                                conn.Open();
                                                //Creating temp table on database
                                                command.CommandText = "CREATE TABLE #" + TableName + "([CIM] int null,[Name] nvarchar(250) null,[LOB] nvarchar(250) null,[Supervisor_CIM] int null,[Date] datetime null,[KPI] nvarchar(250) null,[Target] decimal(18,5) null,[Score] decimal(18,5) null,[Numerator] decimal(18,5) null,[Denominator] decimal(18,5) null,[Supervisor_Name] nvarchar(250) null)";
                                                command.ExecuteNonQuery();

                                                //Bulk insert into temp table
                                                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                                                {
                                                    bulkcopy.BulkCopyTimeout = 660;
                                                    bulkcopy.DestinationTableName = "#" + TableName;
                                                    SqlBulkCopyColumnMapping CIM = new SqlBulkCopyColumnMapping("CIM", "CIM");
                                                    bulkcopy.ColumnMappings.Add(CIM);
                                                    SqlBulkCopyColumnMapping Name = new SqlBulkCopyColumnMapping("Name", "Name");
                                                    bulkcopy.ColumnMappings.Add(Name);
                                                    SqlBulkCopyColumnMapping LOB = new SqlBulkCopyColumnMapping("LOB", "LOB");
                                                    bulkcopy.ColumnMappings.Add(LOB);
                                                    SqlBulkCopyColumnMapping Supervisor_CIM = new SqlBulkCopyColumnMapping("Supervisor CIM", "Supervisor_CIM");
                                                    bulkcopy.ColumnMappings.Add(Supervisor_CIM);
                                                    SqlBulkCopyColumnMapping Date = new SqlBulkCopyColumnMapping("Date", "Date");
                                                    bulkcopy.ColumnMappings.Add(Date);
                                                    SqlBulkCopyColumnMapping KPI = new SqlBulkCopyColumnMapping("KPI", "KPI");
                                                    bulkcopy.ColumnMappings.Add(KPI);
                                                    SqlBulkCopyColumnMapping Target = new SqlBulkCopyColumnMapping("Target", "Target");
                                                    bulkcopy.ColumnMappings.Add(Target);
                                                    SqlBulkCopyColumnMapping Score = new SqlBulkCopyColumnMapping("Score", "Score");
                                                    bulkcopy.ColumnMappings.Add(Score);
                                                    SqlBulkCopyColumnMapping Numerator = new SqlBulkCopyColumnMapping("Numerator", "Numerator");
                                                    bulkcopy.ColumnMappings.Add(Numerator);
                                                    SqlBulkCopyColumnMapping Denominator = new SqlBulkCopyColumnMapping("Denominator", "Denominator");
                                                    bulkcopy.ColumnMappings.Add(Denominator);
                                                    SqlBulkCopyColumnMapping Supervisor_Name = new SqlBulkCopyColumnMapping("Supervisor Name", "Supervisor_Name");
                                                    bulkcopy.ColumnMappings.Add(Supervisor_Name);
                                                    bulkcopy.WriteToServer(dt);
                                                    bulkcopy.Close();
                                                }

                                                // Updating destination table, and dropping temp table
                                                command.CommandTimeout = 300;
                                                command.CommandText = "UPDATE T SET HideFromList=1 FROM " + AgentPerformanceTable + " T INNER JOIN #" + TableName + " Temp ON T.CIM = Temp.CIM and convert(varchar(10),T.[Date],101) = convert(varchar(10),Temp.[Date],101) and T.KPI = Temp.KPI; DROP TABLE #" + TableName + ";";
                                                command.ExecuteNonQuery();

                                                //Bulk insert into AgentPerformanceTable
                                                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                                                {

                                                    bulkcopy.BulkCopyTimeout = 660;
                                                    bulkcopy.DestinationTableName = AgentPerformanceTable;
                                                    SqlBulkCopyColumnMapping CIM1 = new SqlBulkCopyColumnMapping("CIM", "CIM");
                                                    bulkcopy.ColumnMappings.Add(CIM1);
                                                    SqlBulkCopyColumnMapping Name1 = new SqlBulkCopyColumnMapping("Name", "Name");
                                                    bulkcopy.ColumnMappings.Add(Name1);
                                                    SqlBulkCopyColumnMapping LOB1 = new SqlBulkCopyColumnMapping("LOB", "LOB");
                                                    bulkcopy.ColumnMappings.Add(LOB1);
                                                    SqlBulkCopyColumnMapping Supervisor_CIM1 = new SqlBulkCopyColumnMapping("Supervisor CIM", "Supervisor_CIM");
                                                    bulkcopy.ColumnMappings.Add(Supervisor_CIM1);
                                                    SqlBulkCopyColumnMapping Date1 = new SqlBulkCopyColumnMapping("Date", "Date");
                                                    bulkcopy.ColumnMappings.Add(Date1);
                                                    SqlBulkCopyColumnMapping KPI1 = new SqlBulkCopyColumnMapping("KPI", "KPI");
                                                    bulkcopy.ColumnMappings.Add(KPI1);
                                                    SqlBulkCopyColumnMapping Target1 = new SqlBulkCopyColumnMapping("Target", "Target");
                                                    bulkcopy.ColumnMappings.Add(Target1);
                                                    SqlBulkCopyColumnMapping Score1 = new SqlBulkCopyColumnMapping("Score", "Score");
                                                    bulkcopy.ColumnMappings.Add(Score1);
                                                    SqlBulkCopyColumnMapping Numerator1 = new SqlBulkCopyColumnMapping("Numerator", "Numerator");
                                                    bulkcopy.ColumnMappings.Add(Numerator1);
                                                    SqlBulkCopyColumnMapping Denominator1 = new SqlBulkCopyColumnMapping("Denominator", "Denominator");
                                                    bulkcopy.ColumnMappings.Add(Denominator1);
                                                    SqlBulkCopyColumnMapping Supervisor_Name1 = new SqlBulkCopyColumnMapping("Supervisor Name", "Supervisor_Name");
                                                    bulkcopy.ColumnMappings.Add(Supervisor_Name1);
                                                    bulkcopy.WriteToServer(dt);
                                                    bulkcopy.Close();
                                                }


                                            }
                                            catch (Exception ex)
                                            {
                                                string ModalLabel = "UpdateData " + ex.Message.ToString();
                                                string ModalHeader = "Error.";
                                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                                            }
                                            finally
                                            {
                                                conn.Close();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string ModalLabel = "Please check the excel columns.";
                                    string ModalHeader = "Error.";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                                    //break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "UpdateData " + ex.Message.ToString();
                string ModalHeader = "Error.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

            if (File.Exists(excelfilepath))
            {
                File.Delete(excelfilepath);
            }
        }

        public DataTable ConvertExcelToDataTable(string FileName, string sheetName)
        {
            DataTable dtResult = null;
            using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT DISTINCT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
                return dtResult; //Returning Dattable  
            }
        }        

        public void ExcelColumns()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ColumnName", typeof(string));
            dt2.Columns.Add("Exists", typeof(int));
            ViewState["ExcelColumns"] = dt2;
        }

        protected bool CheckIfColumnsExists(OleDbConnection excelConnection, string SheetName)
        {
            //ExcelColumns();
            string name;
            BIColumns();
            bool a = false;
            DataTable dt = (DataTable)ViewState["BIColumns"];
            DataTable myColumns = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, SheetName, null });
            foreach (DataRow dtRow in myColumns.Rows)
            {
                name = dtRow["COLUMN_NAME"].ToString();
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "CIM")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {   
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "CIM")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "NAME")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "NAME")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "LOB")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "LOB")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "DATE")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "DATE")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "KPI")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "KPI")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "TARGET")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "TARGET")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "SCORE")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "SCORE")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "SUPERVISORCIM")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "SUPERVISORCIM")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "SUPERVISORNAME")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "SUPERVISORNAME")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "NUMERATOR")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "NUMERATOR")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
                if (dtRow["COLUMN_NAME"].ToString().Replace(" ", "").ToUpper() == "DENOMINATOR")
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["ColumnName"].ToString().Replace(" ", "").ToUpper() == "DENOMINATOR")
                        {
                            dr["Exists"] = 1;
                        }
                    }
                }
            }
            int c = 0;
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {

                DataRow dr = dt.Rows[i];
                if (dr["Exists"].ToString() == DBNull.Value.ToString())
                {
                    a = false;
                }
                else
                {
                    c = c + 1;
                }

            }
            int count = dt.Rows.Count;
            if (c == count)
            {
                a = true;
            }
            return a;

        }

        public void BIColumns()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns["ID"].AutoIncrement = true;
            dt.Columns["ID"].AutoIncrementSeed = 1;
            dt.Columns["ID"].AutoIncrementStep = 1;
            dt.Columns.Add("ColumnName", typeof(string));
            dt.Columns.Add("Exists", typeof(int));
            ViewState["BIColumns"] = dt;

            DataTable dt2;
            dt2 = (DataTable)ViewState["BIColumns"];
            DataRow dr;
            dr = dt.NewRow();
            dr["ColumnName"] = "CIM";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "NAME";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "LOB";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "DATE";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "KPI";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "TARGET";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "SCORE";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "SUPERVISOR CIM";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "SUPERVISOR NAME";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "NUMERATOR";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
            dr["ColumnName"] = "DENOMINATOR";
            dt2.Rows.Add(dr);
            dr = dt.NewRow();
        }

        static string[] GetExcelSheetNames(string connectionString)
        {
            OleDbConnection con = null;
            DataTable dt = null;
            con = new OleDbConnection(connectionString);
            con.Open();
            dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (dt == null)
            {
                return null;
            }

            String[] excelSheetNames = new String[dt.Rows.Count];
            int i = 0;

            foreach (DataRow row in dt.Rows)
            {
                if (!row["TABLE_NAME"].ToString().Contains("FilterDatabase"))
                {
                    excelSheetNames[i] = row["TABLE_NAME"].ToString();
                    i++;
                }
            }
            con.Close();
            return excelSheetNames;

        }

       protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
       {
               DataSet ds_KPIList2 = DataHelper.GetTopKPIIList_admin();
               RadGrid1.DataSource = ds_KPIList2;
               lblForKpiCreated.Text = "KPIs Assigned by Admin : " + Convert.ToString(ds_KPIList2.Tables[0].Rows.Count);
          
       }

       private void GetHierarchy()
       {

           RadHierarchy2.Items.Clear();

           for (int hierarchy = 0; hierarchy <= 5; hierarchy++)
           {
               string hierarchy_name;
               if (hierarchy == 0)
               {
                   hierarchy_name = "";
                   RadHierarchy2.Items.Add(new Telerik.Web.UI.RadComboBoxItem(hierarchy_name, hierarchy.ToString()));
               }
               else
               {
                   hierarchy_name = hierarchy.ToString();
                   RadHierarchy2.Items.Add(new Telerik.Web.UI.RadComboBoxItem(hierarchy_name.ToString(), hierarchy.ToString()));
               }
           }

       }
    }
}