﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.IO;

namespace CoachV2
{
    public partial class UpdateReview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                if (Page.ClientQueryString != string.Empty)
                {
                
                    get_review();
                    LoadDocumentations();
                }

            }
        }
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
            int coacheesigned;
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
            }
            if (coacheesigned != 1)
            {

                try
                {
                    if (IsPH(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"])))
                    {
                        if (RadSSN.Text != "")
                        {
                            if (CheckSSN() == true)
                            {
                                if (Convert.ToInt32(RadReviewID.Text) != 0)
                                {
                                    CheckSSN();
                                    int ReviewID = Convert.ToInt32(RadReviewID.Text);
                                    DataHelper.Get_CoacheeSignOff(ReviewID);
                                    RadCoacheeSignOff.Enabled = false;
                                    
                                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                                    RadCoacheeSignOff.Enabled = false;
                                    //lblSignedoffDateCoachee.Visible = true;
                                    string ModalLabel = "Coachee has signed off from this review.";
                                    string ModalHeader = "Success";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                    //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                                    get_review();
                                }
                            }
                            else
                            {
                                string ModalLabel = "Incorrect last 3 digits of SSN.";
                                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
 
                            }
                        }
                        else
                        {
                            string ModalLabel = "SSN is a required field.";
                            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
                        }

                    }
                    else
                    {
                        if (Convert.ToInt32(RadReviewID.Text) != 0)
                        {
                            DataHelper.Get_CoacheeSignOff(Convert.ToInt32(RadReviewID.Text));
                            RadCoacheeSignOff.Enabled = false;
                            string ModalLabel = "Coachee has signed off from this review.";
                            
                            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            RadCoacheeSignOff.Enabled = false;
                            //lblSignedoffDateCoachee.Visible = true;
                            string ModalHeader = "Success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                            //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                            get_review();
                        }
                    
                    }

                   
                }
                catch (Exception ex)
                {
                    //string myStringVariable = ex.ToString();
                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                    string ModalLabel = "RadCoacheeSignOff_Click " + ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
                }
            }
            else
            {
                string ModalLabel = "Coaching ticket already signed off by Coachee!";
                get_review();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadCoacheeSignOff.Enabled = false;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     

            }
        }
        public bool CheckSSN()
        {
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));

            int CIMNumber = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheeid"]);//RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheeid"]), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //protected void GetAccounts(int CimNumber)
        //{
        //    try
        //    {
        //        DataTable dt = null;
        //        DataAccess ws = new DataAccess();
        //        dt = ws.GetSubordinates(CimNumber);

        //        var distinctRows = (from DataRow dRow in dt.Rows
        //                            select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

        //        RadAccount.Items.Clear();

        //        foreach (var row in distinctRows)
        //        {
        //            RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }

        //}

        //protected void GetDropDownSessions()
        //{
        //    try
        //    {
        //        DataSet ds = null;
        //        DataAccess ws = new DataAccess();
        //        ds = ws.GetSessionTypes();
        //        RadSessionType.DataSource = ds;
        //        RadSessionType.DataTextField = "SessionName";
        //        RadSessionType.DataValueField = "SessionID";
        //        RadSessionType.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }
        //}

        //protected void GetDropDownTopics(int SessionID)
        //{
        //    try
        //    {
        //        DataSet ds = null;
        //        DataAccess ws = new DataAccess();
        //        ds = ws.GetSessionTopics(SessionID);
        //        RadSessionTopic.DataSource = ds;
        //        RadSessionTopic.DataTextField = "TopicName";
        //        RadSessionTopic.DataValueField = "TopicID";
        //        RadSessionTopic.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }
        //}

        //protected void GetSubordinates(int CimNumber)
        //{
        //    try
        //    {

        //        DataTable dt = null;
        //        DataAccess ws = new DataAccess();
        //        dt = ws.GetSubordinates(CimNumber);

        //        var distinctRows = (from DataRow dRow in dt.Rows
        //                            select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

        //        RadCoacheeName.Items.Clear();

        //        foreach (var row in distinctRows)
        //        {
        //            RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }

        //}

        //protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        //{
        //    try
        //    {
        //        RadSupervisor.Items.Clear();
        //        DataSet ds = null;
        //        DataAccess ws = new DataAccess();
        //        ds = ws.GetSubordinatesWithTeam(CimNumber, AccountID);
        //        RadSupervisor.DataSource = ds;
        //        RadSupervisor.DataTextField = "Name";
        //        RadSupervisor.DataValueField = "CimNumber";
        //        RadSupervisor.DataBind();

        //        DataSet dsInfo = null;
        //        DataAccess wsInfo = new DataAccess();
        //        dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
        //        if (dsInfo.Tables[0].Rows.Count > 0)
        //        {
        //            string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
        //            string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
        //            string FullName = FirstName + " " + LastName;
        //            RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

        //    }
        //}

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
            int coachersigned, coacheesigned;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coachersigned"]);
            }

            if (coachersigned != 1)
            {

                try
                {

                    if (Convert.ToInt32(RadReviewID.Text) != 0)
                    {

                        int ReviewID = Convert.ToInt32(RadReviewID.Text);
                        DataHelper.Get_CoacherSignOff(ReviewID);
                        RadCoacherSignOff.Enabled = false;
                        string ModalLabel = "Coach has signed off from this review.";
                        get_review();
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        RadCoacherSignOff.Enabled = false;
                        //string ModalLabel ="Please submit the review before creating a pdf."
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     

                    }
                }
                catch (Exception ex)
                {
                    //string myStringVariable = ex.ToString();
                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                    string ModalLabel = "RadCoacherSignOff_Click " + ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
                }
            }
            else
            {
                string ModalLabel = "Coaching ticket already signed off by Coach!";
                get_review();

                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadCoacherSignOff.Enabled = false;
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    

            }
        }
        protected void get_review()
        {
            SidebarUserControl.Visible = true;


            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            string coachid = "";
            int reviewid;

            string dectxt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            coachid = dectxt;
            reviewid = Convert.ToInt32(coachid);
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

            int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
            DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
            DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
            DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));

            DataSet ds3x = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review.Tables[0].Rows[0]["createdby"]));

            string createdByName = ds3x.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds3x.Tables[0].Rows[0]["Last_Name"].ToString();


            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
            string ImgDefault;

            string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
            FakeURLID.Value = URL;

            if (!Directory.Exists(directoryPath))
            {
                ImgDefault = URL + "/Content/images/no-photo.jpg";
            }
            else
            {

                ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
            }

            RadReviewID.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["ID"]);

            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                RadCoachingDate.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }
            ProfImage.ImageUrl = ImgDefault;
            LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
            LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
            lblSite.Text = ds.Tables[0].Rows[0]["Sitename"].ToString();
            LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
            LblCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString();
            LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
            LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();

            LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
            LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";
            if (ds_get_review.Tables[0].Rows[0]["Topicname"] is DBNull) { }
            else
            {
                SessionTopic.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Topicname"]);
            }

            if (ds_get_review.Tables[0].Rows[0]["SessionName"] is DBNull) { }
            else
            {
                SessionType.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["SessionName"]);
            }

            RadReviewID.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Id"].ToString());


            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                RadCoachingDate.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }

            if (ds_get_review.Tables[0].Rows[0]["Description"] is DBNull) { }
            else
            {
                RadDescription.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Description"].ToString());
            }

            if (ds_get_review.Tables[0].Rows[0]["Strengths"] is DBNull) { }
            else
            {
                RadStrengths.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Strengths"].ToString());
            }

            if (ds_get_review.Tables[0].Rows[0]["Opportunity"] is DBNull) { }
            else { RadOpportunities.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Opportunity"].ToString()); }

            if (ds_get_review.Tables[0].Rows[0]["Commitment"] is DBNull) { }
            else
            {
                RadCommitment.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Commitment"].ToString());
            }

            DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadReviewID.Text));
            RadGrid1.DataSource = ds_KPILIST;
            RadGrid1.DataBind(); //performance 

            DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
            grd_Coaching_Notes.DataSource = ds_coachingnotes;
            grd_Coaching_Notes.DataBind();

            LoadPreviousPerformanceResults(Convert.ToInt32(RadReviewID.Text));

            int coacheesigned, coachersigned;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = 1;
            }
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = 1;// Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
            }

            string coachstatus;

            string coachersigneddate = ds_get_review.Tables[0].Rows[0]["CoacherSigneddate"].ToString();
            string coacheesigneddate = ds_get_review.Tables[0].Rows[0]["CoacheeSigneddate"].ToString();


            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CIMNumber_userlogged);
            string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

            if (rolevalue == "QA")
                saprolefordashboard = "QA";
            else if (rolevalue == "HR")
                saprolefordashboard = "HR";
            else
                saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

            string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
            string retrole = "";
            if (saprolefordashboard == "TL")
            {
                retrole = "TL";
            }
            else if (saprolefordashboard == "OM")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "Dir")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "QA")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));
                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {

                    retrole = "";
                }
            }
            else if (saprolefordashboard == "HR")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber_userlogged));

                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {
                    retrole = "";
                }
            }
            else
            {
                retrole = "";
            }

            if ((coacheesigned == 1) && (coachersigned == 1) && (retrole == "OM"))
            {

                btn_Audit1.Visible = true;
            }

            
            string coachersigneddate1, coacheesigneddate1;

            if (ds_get_review.Tables[0].Rows[0]["RequireSignOff"] is DBNull || Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["RequireSignOff"]) == 0)
            {
                RadCoacherSignOff.Visible = false;
                RadCoacherSignOff.Enabled = false;
                lblSignoffstatus.Visible = true;
                coachstatus = "This review did not require any sign off.";
                lblSignoffstatus.Text = coachstatus;
            }
            else
            {

                if (ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"] is DBNull)
                {
                    coacheesigneddate1 = "";
                }
                else
                {
                    coacheesigneddate1 = ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"].ToString();
                }

                if (ds_get_review.Tables[0].Rows[0]["coachersigneddate1"] is DBNull)
                {
                    coachersigneddate1 = "";
                }
                else
                {
                    coachersigneddate1 = ds_get_review.Tables[0].Rows[0]["coachersigneddate1"].ToString();
                }

                if (coachersigned != 1)
                {
                    RadCoacherSignOff.Visible = true;
                    RadCoacherSignOff.Enabled = true;

                    if (coacheesigned != 1)
                    {
                        lblSignoffstatus.Visible = true;
                        coachstatus = "*Needs sign off from Coachee : " + " "
                           + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + ". "
                           + " Coach: " + " " + createdByName
                           + " already signed off."
                           + " Coach Sign off date : " + coachersigneddate1;
                        lblSignoffstatus.Text = coachstatus;
                    }
                    else
                    {
                        lblSignoffstatus.Visible = true;
                        coachstatus = "*Needs sign off from Coach: " + " " + createdByName
                            + ". Coachee: " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + "."
                            + " already signed off."
                            + " Coachee Sign off date : " + coacheesigneddate1;
                        lblSignoffstatus.Text = coachstatus;
                    }

                }
                else if (coachersigned == 1)
                {
                    RadCoacherSignOff.Visible = false;
                    RadCoacherSignOff.Enabled = false;
                    if (coacheesigned != 1)
                    {
                        lblSignoffstatus.Visible = true;
                        coachstatus = "*Needs sign off from Coachee : " + " "
                            + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + "."
                            + " Coach: " + " " + createdByName
                            + " already signed off."
                            + " Coach Sign off date : " + coachersigneddate1;
                        lblSignoffstatus.Text = coachstatus;

                    }
                    else
                    {
                        RadSSN.Visible = false;
                        lblSignoffstatus.Visible = true;
                        coachstatus = "*Coach: " + " " + createdByName
                            + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString())
                            + " already signed off." + " Coach Sign off date : " + coachersigneddate1 + " Coachee Sign off date : " + coacheesigneddate1;
                        lblSignoffstatus.Text = coachstatus;
                    }


                }
            }

            //}
            //else { }


        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }

        public void LoadPreviousPerformanceResults(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPR.DataSource = ds;
                RadGridPR.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
     
            }
        }

        protected void btn_Audit1_Click(object sender, EventArgs e)
        {
            btn_Audit1.Attributes.Add("onclick", " CloseWindow();");
            
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            string coachid = "";
            int reviewid;
            coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(coachid);
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);
            string val1 = Convert.ToString(reviewid);
            
            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

            Response.Redirect("~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2);
            
        }
    }

}