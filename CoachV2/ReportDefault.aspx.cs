﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CoachV2.AppCode;
using System.Data;

namespace CoachV2
{
    public partial class ReportDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                ctrlReviews.Text = " Coaching Dashboard ";
                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                ctrlA.Text = " > My Reports";
                Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                ctrlB.Text = " > Recently Used Reports";
                ctrlB.Visible = true;
                GetRecentlyUsedReports();

            }
        }
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
         if (e.Item is GridDataItem)
           {
               GridDataItem item = (GridDataItem)e.Item;
               Label LblExported = (Label)item.FindControl("Label1");
               Image Image1 = item.FindControl("Image1") as Image;
               if (LblExported.Text == "True")
               {

                   Image1.ImageUrl = "~/Content/images/export.png";
               }
               else
               {
                   //Image1.Style.Add("display", "none");
                   Image1.ImageUrl = "~/Content/images/grey.png";
               }
           }
       }
        //protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

        //        GridDataItem item = (GridDataItem)e.Item;
        //        Label LblReportID = (Label)item.FindControl("ReportID");
        //        //HyperLink hLinkname = (HyperLink)item["ReportName"].Controls[0];
        //        string val1 = LblReportID.Text;

        //       //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ID + "&UserID=" + cim_num + "','Graph','height=500,width=1000');", true);

        //        //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + LblReportID.Text + "&UserID=" + 10115015 + "','Graph','height=400,width=500');", true);
        //        //string redirect = "GeneratedReport.aspx?ReportID=" + LblReportID.Text + "&UserID=" + cim_num + "'";
        //        ////Response.Write(redirect);

        //        //Response.Write(String.Format("window.open('{0}','_blank')", ResolveUrl(redirect)));

        //        string script = " <script type=\"text/javascript\">  window.open('GeneratedReport.aspx?ReportID=" + LblReportID.Text + "&UserID=" + cim_num + "','Graph','height=500,width=1000');   </script> ";
        //       //  this.Page.ClientScript.RegisterStartupScript(typeof(Page), "alert", script);
        //       ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", script, false);
        //    }
        //}

        protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

                Report rt = new Report();
                DataSet ds = null;
                ds = rt.GenerateRecentlyUsedReports2(cim_num, 1);

                try
                {
                    

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        RadGrid1.DataSource = ds;
                       // RadGrid1.DataBind();
                    }

                }
                catch (Exception ex)
                {
                    RadGrid1.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
                }
            }
        }

        protected void btnlnk_Click(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            LinkButton btnlnk = sender as LinkButton;
            GridDataItem item = btnlnk.NamingContainer as GridDataItem;
            int ID = Convert.ToInt32(item.GetDataKeyValue("ReportID"));

            Report rt = new Report();
            rt.UpdateReportStatus(ID, 3);
            GetRecentlyUsedReports();
            // ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ID + "&UserID=" + 10115015 + "','Graph','height=500,width=1000');", true);
            //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ID + "&UserID=" + cim_num + "','Graph','height=500,width=1000');", true);
        //    string redirect = "<script>window.open('GeneratedReport.aspx?ReportID=" + ID + "&UserID=" + cim_num + "','Graph','height=400,width=500');</script>";
        //    Response.Write(redirect);
            string script = " <script type=\"text/javascript\">  window.open('GeneratedReport.aspx?ReportID=" + ID + "&UserID=" + cim_num + "','Graph','height=500,width=1000');   </script> ";
            //  this.Page.ClientScript.RegisterStartupScript(typeof(Page), "alert", script);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", script, false);
        }

        public void GetRecentlyUsedReports()
        {

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
          
            Report rt = new Report();
            DataSet ds = null;
            ds = rt.GenerateRecentlyUsedReports2(cim_num,1);

            if (ds.Tables[0].Rows.Count > 0)
            {
                RadGrid1.DataSource = ds;
                RadGrid1.DataBind();
            }
            else
            {
                RadGrid1.DataSource = string.Empty;
                RadGrid1.Rebind();
            }
        }
    }
}