﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="CoachV2.EditProfile" %>
<%@ Register src="UserControl/ProfileEditMainUserCtrl.ascx" tagname="ProfileEditMainUserCtrl" tagprefix="uc1" %>
<%@ Register src="UserControl/SidebarEditProfileUserControl.ascx" tagname="SidebarEditProfileUserControl" tagprefix="uc2" %>
<%@ Register src="UserControl/ProfileEditExpUserCtrl.ascx" tagname="ProfileEditExpUserCtrl" tagprefix="uc3" %>
<%@ Register src="UserControl/ProfileEditEducUserCtrl.ascx" tagname="ProfileEditEducUserCtrl" tagprefix="uc4" %>
<%@ Register src="UserControl/ProfileEditSkillUserControl.ascx" tagname="ProfileEditSkillUserControl" tagprefix="uc5" %>
<%@ Register src="UserControl/ProfileEditCertUserControl.ascx" tagname="ProfileEditCertUserControl" tagprefix="uc6" %>
<%@ Register src="UserControl/ProfileEditExtraUserCtrl.ascx" tagname="ProfileEditExtraUserCtrl" tagprefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc2:SidebarEditProfileUserControl ID="SidebarEditProfileUserControl1" 
        runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    
    <uc1:ProfileEditMainUserCtrl ID="ProfileEditMainUserCtrl1" runat="server" Visible="true" />
    <uc3:ProfileEditExpUserCtrl ID="ProfileEditExpUserCtrl1" runat="server" Visible="false" />
    <uc4:ProfileEditEducUserCtrl ID="ProfileEditEducUserCtrl1" runat="server" Visible="false" />
    

    
    <uc5:ProfileEditSkillUserControl ID="ProfileEditSkillUserControl1" 
        runat="server" Visible="false" />
    <uc6:ProfileEditCertUserControl ID="ProfileEditCertUserControl1" 
        runat="server"  Visible="false" />
    <uc7:ProfileEditExtraUserCtrl ID="ProfileEditExtraUserCtrl1" runat="server" Visible="false" />
    


</asp:Content>
