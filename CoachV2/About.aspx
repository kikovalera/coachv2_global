﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="CoachV2.About" MasterPageFile="~/Site.Master" %>
 
<%@ Register src="UserControl/AboutPageSidebar.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/AboutMain.ascx" tagname="MainUC" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" Visible="true" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<uc2:MainUC ID="Main" runat="server" Visible="true"/>
</asp:Content>  