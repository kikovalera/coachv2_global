﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;

namespace CoachV2
{
    public partial class CMT : System.Web.UI.Page
    {
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
          
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string PreviewType = Session["PreviewType"].ToString();

                CMTCim.Text = Session["CMTCIM"].ToString();
                CMTSupervisor.Text = Session["CMTSupName"].ToString();
                CMTAccount.Text = Session["CMTAccount"].ToString();
                CMTSessionType.Text = Session["CMTSessionType"].ToString();
                CMTTopic.Text = Session["CMTSessionTopic"].ToString();
                CMTHRComments.Text = Session["CMTHRComment"].ToString();
                if (PreviewType == "2")
                {
                    CMTDepartment.Text = Session["CMTDepartment"].ToString();
                    CMTAccount.Text = Session["CMTCampaign"].ToString();
                }
                //CoachingNotes();
                BindListViewCN();
                BindListViewPR();
                LoadDocs();
                Label1.Text =" > Add Review > " + Session["CMTName"].ToString();
                Label2.Text = " > Preview ";
                Label2.Visible = true;
            }

        }
        private void BindListViewPR()
        {
            string PreviewType = Session["PreviewType"].ToString();
            if (PreviewType == "1")
            {
                if (Session["CMTPR"] != null)
                {

                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["CMTPR"];
                    if (dt.Rows.Count > 0 && dt != null)
                    {

                        RadGridPR.DataSource = dt;
                        RadGridPR.DataBind();
                    }
                    else
                    {
                        RadGridPR.DataSource = null;
                        RadGridPR.DataBind();
                    }
                }
            }
            else
            {
                string CMTReviewID = Session["CMTReviewID"].ToString();
                LoadPreviousPerformanceResults(Convert.ToInt32(CMTReviewID));
            }
        }

        public void LoadPreviousPerformanceResults(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPR.DataSource = ds;
                RadGridPR.DataBind();

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        private void BindListViewCN()
        {
            string PreviewType = Session["PreviewType"].ToString();
            if (PreviewType == "1")
            {
                if (Session["CMTCN"] != null)
                {
                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["CMTCN"];
                    if (dt.Rows.Count > 0 && dt != null)
                    {

                        RadGridCN.DataSource = dt;
                        RadGridCN.DataBind();
                    }
                    else
                    {
                        RadGridCN.DataSource = null;
                        RadGridCN.DataBind();
                    }
                }
            }
            else {
                string CMTReviewID = Session["CMTReviewID"].ToString();
                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(CMTReviewID));
                RadGridCN.DataSource = ds_coachingnotes;
                RadGridCN.DataBind();
            }
        }

        private void LoadDocs()
        {
            if (Session["Docs"] != null)
            {
                DataTable dt = new DataTable();
                dt = (DataTable)Session["Docs"];
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridDocumentation.DataSource = dt;
                    RadGridDocumentation.DataBind();
                }
                else
                {
                    RadGridDocumentation.DataSource = null;
                    RadGridDocumentation.DataBind();
                }
            }
        }
    }
}