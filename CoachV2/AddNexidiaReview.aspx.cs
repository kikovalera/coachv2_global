﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;


namespace CoachV2
{
    public partial class AddNexidiaReview : System.Web.UI.Page
    {
        #region Nexidia
        public List<Nexidia> lstNexidia
        {
            get
            {
                if (Session["lstNexidia"] != null)
                {
                    return (List<Nexidia>)Session["lstNexidia"];
                }
                else
                {
                    return new List<Nexidia>();
                }
            }
            set
            {
                Session["lstNexidia"] = value;
            }
        }
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
        }
        public void PerformanceResults()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewKPIID", typeof(string));
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Name", typeof(string));
            dt2.Columns.Add("Target", typeof(string));
            dt2.Columns.Add("Current", typeof(string));
            dt2.Columns.Add("Previous", typeof(string));
            dt2.Columns.Add("Driver_Name", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["PR"] = dt2;

            BindListViewPR();
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.btn_ExporttoPDF);
            if (!IsPostBack)
            {
                //ScriptManager scriptManager1 = ScriptManager.GetCurrent(this.Page);
                //scriptManager1.RegisterPostBackControl(this.btn_ExporttoPDF);
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //ForTesting
                //int CIMNumber = Convert.ToInt32(CIMTEST);
                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CIMNumber, 2);

                if (ds_userselect2.Tables[0].Rows.Count > 0)
                {
                    bool Supervisor;
                    Supervisor = CheckIfHasSubordinates(CIMNumber);

                    if (Supervisor == true)
                    {
                            string CIM = "";

                            if (Request["CIM"] != null) //Add Review Thru My Team
                            {
                                CIM = Request.QueryString["CIM"];
                                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                                ctrlA.Text = "> My Team > Add Review >";
                                SetLabels(Convert.ToInt32(CIM));
                                
                           
                            }
                            else //Add Review Thru My Reviews
                            {
                                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                                ctrlA.Text = " > Add Review";
                                Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                                ctrlB.Text = "";
                              
                            
                            }
                        RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                        GetSessionFocus("Nexidia");
                        RadDocumentationReview.DataSource = string.Empty;
                        RadDocumentationReview.Rebind();
                        LoadTTCampaigns();
                        GetDropDownSessions();
                        List<Nexidia> list = new List<Nexidia>();
                        lstNexidia = list;
                    }
                    else
                    {
                      Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                Response.Redirect("~/Default.aspx");
                }
                   
                
            }
            
        }
        private void BindListViewCN()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridCN.DataSource = dt;
                RadGridCN.DataBind();
            }
            else
            {
                RadGridCN.DataSource = null;
                RadGridCN.DataBind();
            }
        }
        private void BindListViewPR()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridPR.DataSource = dt;
                RadGridPR.DataBind();
            }
            else
            {
                RadGridPR.DataSource = null;
                RadGridPR.DataBind();
            }
        }
        public void GetSessionFocus(string Account)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionFocus(Account);
                CBList.DataSource = ds;
                CBList.DataTextField = "SessionFocusName";
                CBList.DataValueField = "ID";
                CBList.DataBind();
            }

            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("GetSessionFocus" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
            }
        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = lstNexidia;
        }
        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllData();
                lstNexidia.Insert(0, new Nexidia() { NexidiaID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid1.Rebind();
            }
        }
        protected void saveAllData()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
            {
                Guid NexidiaID = new Guid(item.GetDataKeyValue("NexidiaID").ToString());
                Nexidia emp = lstNexidia.Where(i => i.NexidiaID == NexidiaID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;

            }
        }
        protected void OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem insertitem = (GridDataItem)e.Item;
                RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                DataSet ds = new DataSet();
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIList();
                //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                combo.DataSource = ds;
                combo.DataTextField = "Name";
                combo.DataValueField = "KPIID";
                combo.DataBind();
                combo.ClearSelection();

                RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                DataSet dsTarget = null;
                DataAccess wsTarget = new DataAccess();
                dsTarget = wsTarget.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();

            }

        }
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
        }
        protected void OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridEditableItem edititem = (GridEditableItem)e.Item;
                RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI");
                RadComboBoxItem selectedItem = new RadComboBoxItem();
                string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                if (a != "0")
                {
                    selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI"] = selectedItem.Value;
                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIList();
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    comboBrand.SelectedValue = selectedItem.Value;
                }


                RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                DataSet dsTarget = null;
                DataAccess wsTarget = new DataAccess();
                dsTarget = wsTarget.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                if (b != "0")
                {
                    selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                    combodriver.Items.Add(selectedItemdriver);
                    selectedItemdriver.DataBind();
                    Session["Driver"] = selectedItemdriver.Value;
                    RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                    DataSet dsdriver = new DataSet();
                    DataAccess wsdriver = new DataAccess();
                    dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                    comboBranddriver.DataSource = dsdriver;
                    comboBranddriver.DataTextField = "Description";
                    comboBranddriver.DataValueField = "Driver_ID";
                    comboBranddriver.DataBind();
                    comboBranddriver.SelectedValue = selectedItemdriver.Value;
                }
            }
        }
        public void RadKPI_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("RadKPI_SelectedIndexChanged" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            commandItem.FireCommandEvent("InitInsert", null);
        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        public void LoadTTCampaigns()
        {
            try
            {
                //DataSet ds = null;
                //DataAccessGenericWeb ws = new DataAccessGenericWeb();
                //ds = ws.GetTalktalkCampaigns(1, 1, 20196);

                //RadAccount.DataSource = ds;
                //RadAccount.DataTextField = "Account";
                //RadAccount.DataValueField = "AccountID";
                //RadAccount.DataBind();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates(CIMNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                RadAccount.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }

            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("LoadTTCampaigns" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
            }
        }
        public void LoadTTSupervisors(int AccountID)
        { 
        
             try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount(AccountID);

                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();

            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("LoadTTSupervisors" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
            }
        }
        public void LoadTTSubordinates(int CimNumber,int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinatesWithAccounts(CimNumber,AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("LoadTTSubordinates" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        public void GetDropDownSessions()
        {
            try
            {

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
                VisibleCheckBox();
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("GetDropDownSessions" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void GetDropDownTopics(int SessionID, int CimNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID, CimNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
                VisibleCheckBox();
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("GetDropDownTopics" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSupervisors(Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
            RadSupervisor.Text = string.Empty;
            RadSupervisor.ClearSelection();
            RadSupervisor.SelectedIndex = -1;
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GetDropDownTopics(Convert.ToInt32(RadSessionType.SelectedValue),10115015);
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;
            VisibleCheckBox();
        }
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            VisibleCheckBox();
        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText==true)
            {

                if (CheckBox1.Checked == true)
                {
                    CoachingNotes();
                    int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                    //int CoachingCIM = 10107032;
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetCoachingNotes(CoachingCIM);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        RadGrid2.DataSource = ds;
                        RadGrid2.DataBind();
                        Window1.VisibleOnPageLoad = true;
                        RadGridCN.Visible = true;
                    }
                    else
                    {

                        string myStringVariable = "No data for previous performance results.";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        
                    }
                }

                else
                {
                    ViewState["CN"] = null;
                    RadGridCN.DataSource = null;
                    RadGridCN.DataBind();


                }
            }
            else
            {
                CheckBox1.Checked = false;
                string myStringVariable = "Please select the employee first.";
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadWindowManager1.RadAlert(""+ myStringVariable +"", 500, 200,"Error Message" , "", "");
           
            }
        }
        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText==true)
            {
                if (CheckBox2.Checked == true)
                {
                    PerformanceResults();
                    int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetPerformanceResults(CoachingCIM);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        RadGrid3.DataSource = ds;
                        RadGrid3.DataBind();
                        Window2.VisibleOnPageLoad = true;
                        RadGridPR.Visible = true;
                    }
                    else
                    {
                        string myStringVariable = "No data for previous performance results.";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    }
                }

                else
                {
                    RadGridPR.DataSource = null;
                    RadGridPR.DataBind();
                    RadGridPR.Visible = false;

                }
            }
            else
            {
                CheckBox2.Checked = false;
                string myStringVariable = "Please select the employee first.";
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
            }
        }
        protected void RadCNLink_Click(object sender, EventArgs e)
        {
            
                string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                bool chec;
                foreach (GridDataItem item in RadGrid2.SelectedItems)
                {
                    CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                    id = item["ReviewID"].Text;
                    chec = chk.Checked;
                    fullname = item["FullName"].Text;
                    cimnumber = item["CIMNumber"].Text;
                    session = item["Session"].Text;
                    topic = item["Topic"].Text;
                    reviewdate = item["ReviewDate"].Text;
                    assignedby = item["AssignedBy"].Text;

                    DataTable dt = new DataTable();
                    DataRow dr;
                    //assigning ViewState value in Data Table  
                    dt = (DataTable)ViewState["CN"];
                    //Adding value in datatable  
                    dr = dt.NewRow();
                    dr["ReviewID"] = id;
                    dr["FullName"] = fullname;
                    dr["CIMNumber"] = cimnumber;
                    dr["Session"] = session;
                    dr["Topic"] = topic;
                    dr["ReviewDate"] = reviewdate;
                    dr["AssignedBy"] = assignedby;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["CN"] = dt;
                    }
                    this.BindListViewCN();
                }
            ViewState["CN"] = null;
            Window1.VisibleOnPageLoad = false;


        }
        protected void RadCNCancel_Click(object sender, EventArgs e)
        {
            Window1.VisibleOnPageLoad = false;
            CheckBox1.Checked = false;
            RadGridCN.DataSource = null;
            RadGridCN.DataBind();
        }
        protected void RadPRLink_Click(object sender, EventArgs e)
        {

           
                string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                bool chec;
                foreach (GridDataItem item in RadGrid3.SelectedItems)
                {
                    CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                    id = item["ReviewKPIID"].Text;
                    chec = chk.Checked;
                    coachingid = item["ID"].Text;
                    fullname = item["FullName"].Text;
                    cimnumber = item["CIMNumber"].Text;
                    name = item["Name"].Text;
                    target = item["Target"].Text;
                    current = item["Current"].Text;
                    previous = item["Previous"].Text;
                    drivername = item["Driver_Name"].Text;
                    reviewdate = item["ReviewDate"].Text;
                    assignedby = item["AssignedBy"].Text;

                    DataTable dt = new DataTable();
                    DataRow dr;
                    //assigning ViewState value in Data Table  
                    dt = (DataTable)ViewState["PR"];
                    //Adding value in datatable  
                    dr = dt.NewRow();
                    dr["ReviewKPIID"] = id;
                    dr["ReviewID"] = coachingid;
                    dr["FullName"] = fullname;
                    dr["CIMNumber"] = cimnumber;
                    dr["Name"] = name;
                    dr["Target"] = target;
                    dr["Current"] = current;
                    dr["Previous"] = previous;
                    dr["Driver_Name"] = drivername;
                    dr["ReviewDate"] = reviewdate;
                    dr["AssignedBy"] = assignedby;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["PR"] = dt;
                    }
                    this.BindListViewPR();
               


            }
            ViewState["PR"] = null;
            Window2.VisibleOnPageLoad = false;


        }
        protected void RadPRCancel_Click(object sender, EventArgs e)
        {
            Window2.VisibleOnPageLoad = false;
            CheckBox2.Checked = false;
            RadGridPR.DataSource = null;
            RadGridPR.DataBind();
        }
        private void VisibleCheckBox()
        {
            if (RadSessionTopic.SelectedIndex == -1)
            {
                CNPR.Visible = false;
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                FollowUp.Visible = true;
                //PerfResult.Visible = false;
                //Div1.Visible = false;
                Div3.Visible = false;
                Div5.Visible = false;
            }
            else if (RadSessionTopic.SelectedItem.Text != "Awareness" && RadSessionTopic.SelectedItem.Text != "1st Warning")
            {
                CNPR.Visible = true;
                CheckBox1.Visible = true;
                CheckBox2.Visible = true;
                PerfResult.Visible = true;
                FollowUp.Visible = true;
                Div3.Visible = true;
                Div5.Visible = true;
                Div1.Visible = true;

            }
            else
            {
                CNPR.Visible = false;
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                FollowUp.Visible = true;
                PerfResult.Visible = false;
                Div3.Visible = false;
                Div5.Visible = false;
                Div1.Visible = false;


            }
        }
        protected void RadCoacheeName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                {
                    RadCoacheeCIM.Text = RadCoacheeName.SelectedValue.ToString();
                    RadCoacheeCIM.Enabled = false;
                    RadSSN.Visible = true;
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();
                    //RadGridCN.DataBind();
                    RadGridPR.DataSource = null;
                    RadGridPR.Rebind();
                    //RadGridPR.DataBind();


                }
                else
                {
                    RadCoacheeCIM.Text = "";
                    RadCoacheeCIM.Enabled = true;
                    RadSSN.Visible = false;
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();
                    //RadGridCN.DataBind();
                    RadGridPR.DataSource = null;
                    RadGridPR.Rebind();
                    //RadGridPR.DataBind();



                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
            //    if (CBList.SelectedIndex != -1)
            //    {
                    if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                    {
                        if (RadSSN.Text != "")
                        {
                            if (CheckSSN() == true)
                            {
                                if (Convert.ToInt32(RadReviewID.Text) == 0)
                                {
                                    string FollowDate;
                                    if (RadFollowup.SelectedDate.HasValue)
                                    {
                                        FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                    }
                                    else
                                    {
                                        FollowDate = DBNull.Value.ToString();
                                    }

                                    string Description;
                                    if (RadDescription.Text != null)
                                    {
                                        Description = RadDescription.Text;
                                    }
                                    else
                                    {
                                        Description = DBNull.Value.ToString();
                                    }

                                    string Strengths;
                                    if (RadStrengths.Text != null)
                                    {
                                        Strengths = RadStrengths.Text;
                                    }
                                    else
                                    {
                                        Strengths = DBNull.Value.ToString();
                                    }

                                    string Opportunity;
                                    if (RadOpportunities.Text != null)
                                    {
                                        Opportunity = RadOpportunities.Text;
                                    }
                                    else
                                    {
                                        Opportunity = DBNull.Value.ToString();
                                    }

                                    int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

                                    string PositiveBehaviour;
                                    if (RadPositiveBehaviour.Text != null)
                                    {
                                        PositiveBehaviour = RadPositiveBehaviour.Text;
                                    }
                                    else
                                    {
                                        PositiveBehaviour = DBNull.Value.ToString();
                                    }

                                    string OpportunityBehaviour;
                                    if (RadOpportunityBehaviour.Text != null)
                                    {
                                        OpportunityBehaviour = RadOpportunityBehaviour.Text;
                                    }
                                    else
                                    {
                                        OpportunityBehaviour = DBNull.Value.ToString();
                                    }

                                    string BeginBehaviourComments;
                                    if (RadBeginBehaviourComments.Text != null)
                                    {
                                        BeginBehaviourComments = RadBeginBehaviourComments.Text;
                                    }
                                    else
                                    {
                                        BeginBehaviourComments = DBNull.Value.ToString();
                                    }

                                    string BehaviourEffects;
                                    if (RadBehaviourEffect.Text != null)
                                    {
                                        BehaviourEffects = RadBehaviourEffect.Text;
                                    }
                                    else
                                    {
                                        BehaviourEffects = DBNull.Value.ToString();
                                    }

                                    string RootCause;
                                    if (RadRootCause.Text != null)
                                    {
                                        RootCause = RadRootCause.Text;
                                    }
                                    else
                                    {
                                        RootCause = DBNull.Value.ToString();
                                    }

                                    string ReviewResultsComments;
                                    if (RadReviewResultsComments.Text != null)
                                    {
                                        ReviewResultsComments = RadReviewResultsComments.Text;
                                    }
                                    else
                                    {
                                        ReviewResultsComments = DBNull.Value.ToString();
                                    }

                                    string AddressOpportunities;
                                    if (RadAddressOpportunities.Text != null)
                                    {
                                        AddressOpportunities = RadAddressOpportunities.Text;
                                    }
                                    else
                                    {
                                        AddressOpportunities = DBNull.Value.ToString();
                                    }

                                    string ActionPlanComments;
                                    if (RadActionPlanComments.Text != null)
                                    {
                                        ActionPlanComments = RadActionPlanComments.Text;
                                    }
                                    else
                                    {
                                        ActionPlanComments = DBNull.Value.ToString();
                                    }

                                    string SmartGoals;
                                    if (RadSmartGoals.Text != null)
                                    {
                                        SmartGoals = RadSmartGoals.Text;
                                    }
                                    else
                                    {
                                        SmartGoals = DBNull.Value.ToString();
                                    }

                                    string CreatePlanComments;
                                    if (RadCreatePlanComments.Text != null)
                                    {
                                        CreatePlanComments = RadCreatePlanComments.Text;
                                    }
                                    else
                                    {
                                        CreatePlanComments = DBNull.Value.ToString();
                                    }

                                    string FollowThrough;
                                    if (RadFollowThrough.Text != null)
                                    {
                                        FollowThrough = RadFollowThrough.Text;
                                    }
                                    else
                                    {
                                        FollowThrough = DBNull.Value.ToString();
                                    }



                                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                    int CIMNumber = Convert.ToInt32(cim_num);
                                    int NexidiaReviewID;
                                    DataAccess ws = new DataAccess();
                                    NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
                                    RadReviewID.Text = Convert.ToString(NexidiaReviewID);

                                    InsertReviewKPI();
                                    InsertReviewHistory(1);
                                    InsertReviewHistory(2);
                                    LoadKPIReview(NexidiaReviewID);
                                    UploadDocumentationReview(0, NexidiaReviewID);
                                    LoadDocumentationsReview();
                                    DisableElements();
                                    RadCoacheeSignOff.Visible = false;
                                    string script = "Coachee has signed off from this review.";
                                    RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                                    RadSSN.Visible = false;
                                    RadCoacherSignOff.Visible = true;
                                    RadCoacheeCIM.Visible = false;
                                }
                                else
                                {
                                    DataAccess ws = new DataAccess();
                                    ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                                    UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
                                    LoadDocumentationsReview();
                                    RadCoacheeSignOff.Visible = false;
                                    string script = "Coachee has signed off from this review.";
                                    RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                                    DisableElements();
                                    RadSSN.Visible = false;
                                    RadCoacheeCIM.Visible = false;

                                }
                            }
                            else
                            {

                                string script = "Incorrect last 3 digits of SSN.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                                //DisableElements();

                            }
                        }


                        else
                        {

                            string script = "SSN is a required field.";
                            RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                            //DisableElements();
                        }
                    }
                    else
                    {
                        if (RadCoacheeCIM.Text != "")
                        {
                            if (Convert.ToInt32(RadCoacheeCIM.Text) == Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()))
                            {
                                if (Convert.ToInt32(RadReviewID.Text) == 0)
                                {

                                    //DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                    //string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                    //int CIMNumber = Convert.ToInt32(cim_num);
                                    //int ReviewID;
                                    //DataAccess ws = new DataAccess();
                                    //ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
                                    //RadReviewID.Text = Convert.ToString(ReviewID);
                                    string FollowDate;
                                    if (RadFollowup.SelectedDate.HasValue)
                                    {
                                        FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                    }
                                    else
                                    {
                                        FollowDate = DBNull.Value.ToString();
                                    }

                                    string Description;
                                    if (RadDescription.Text != null)
                                    {
                                        Description = RadDescription.Text;
                                    }
                                    else
                                    {
                                        Description = DBNull.Value.ToString();
                                    }

                                    string Strengths;
                                    if (RadStrengths.Text != null)
                                    {
                                        Strengths = RadStrengths.Text;
                                    }
                                    else
                                    {
                                        Strengths = DBNull.Value.ToString();
                                    }

                                    string Opportunity;
                                    if (RadOpportunities.Text != null)
                                    {
                                        Opportunity = RadOpportunities.Text;
                                    }
                                    else
                                    {
                                        Opportunity = DBNull.Value.ToString();
                                    }

                                    int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

                                    string PositiveBehaviour;
                                    if (RadPositiveBehaviour.Text != null)
                                    {
                                        PositiveBehaviour = RadPositiveBehaviour.Text;
                                    }
                                    else
                                    {
                                        PositiveBehaviour = DBNull.Value.ToString();
                                    }

                                    string OpportunityBehaviour;
                                    if (RadOpportunityBehaviour.Text != null)
                                    {
                                        OpportunityBehaviour = RadOpportunityBehaviour.Text;
                                    }
                                    else
                                    {
                                        OpportunityBehaviour = DBNull.Value.ToString();
                                    }

                                    string BeginBehaviourComments;
                                    if (RadBeginBehaviourComments.Text != null)
                                    {
                                        BeginBehaviourComments = RadBeginBehaviourComments.Text;
                                    }
                                    else
                                    {
                                        BeginBehaviourComments = DBNull.Value.ToString();
                                    }

                                    string BehaviourEffects;
                                    if (RadBehaviourEffect.Text != null)
                                    {
                                        BehaviourEffects = RadBehaviourEffect.Text;
                                    }
                                    else
                                    {
                                        BehaviourEffects = DBNull.Value.ToString();
                                    }

                                    string RootCause;
                                    if (RadRootCause.Text != null)
                                    {
                                        RootCause = RadRootCause.Text;
                                    }
                                    else
                                    {
                                        RootCause = DBNull.Value.ToString();
                                    }

                                    string ReviewResultsComments;
                                    if (RadReviewResultsComments.Text != null)
                                    {
                                        ReviewResultsComments = RadReviewResultsComments.Text;
                                    }
                                    else
                                    {
                                        ReviewResultsComments = DBNull.Value.ToString();
                                    }

                                    string AddressOpportunities;
                                    if (RadAddressOpportunities.Text != null)
                                    {
                                        AddressOpportunities = RadAddressOpportunities.Text;
                                    }
                                    else
                                    {
                                        AddressOpportunities = DBNull.Value.ToString();
                                    }

                                    string ActionPlanComments;
                                    if (RadActionPlanComments.Text != null)
                                    {
                                        ActionPlanComments = RadActionPlanComments.Text;
                                    }
                                    else
                                    {
                                        ActionPlanComments = DBNull.Value.ToString();
                                    }

                                    string SmartGoals;
                                    if (RadSmartGoals.Text != null)
                                    {
                                        SmartGoals = RadSmartGoals.Text;
                                    }
                                    else
                                    {
                                        SmartGoals = DBNull.Value.ToString();
                                    }

                                    string CreatePlanComments;
                                    if (RadCreatePlanComments.Text != null)
                                    {
                                        CreatePlanComments = RadCreatePlanComments.Text;
                                    }
                                    else
                                    {
                                        CreatePlanComments = DBNull.Value.ToString();
                                    }

                                    string FollowThrough;
                                    if (RadFollowThrough.Text != null)
                                    {
                                        FollowThrough = RadFollowThrough.Text;
                                    }
                                    else
                                    {
                                        FollowThrough = DBNull.Value.ToString();
                                    }


                                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                    int CIMNumber = Convert.ToInt32(cim_num);
                                    int NexidiaReviewID;
                                    DataAccess ws = new DataAccess();
                                    NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
                                    RadReviewID.Text = Convert.ToString(NexidiaReviewID);



                                    InsertReviewKPI();
                                    InsertReviewHistory(1);
                                    InsertReviewHistory(2);
                                    LoadKPIReview(NexidiaReviewID);
                                    UploadDocumentationReview(0, NexidiaReviewID);
                                    LoadDocumentationsReview();
                                    DisableElements();
                                    RadCoacheeSignOff.Visible = false;
                                    string script = "Coachee has signed off from this review.";
                                    RadWindowManager1.RadAlert("" + script + "", 500, 200, "", "", "");
                                    RadSSN.Visible = false;
                                    RadCoacheeCIM.Visible = false;
                                    RadCoacherSignOff.Visible = true;

                                }
                                else
                                {
                                    DataAccess ws = new DataAccess();
                                    ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                                    UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
                                    LoadDocumentationsReview();
                                    RadCoacheeSignOff.Visible = false;
                                    string script = "Coachee has signed off from this review.";
                                    RadWindowManager1.RadAlert("" + script + "", 500, 200, "", "", "");
                                    DisableElements();
                                    RadSSN.Visible = false;

                                }
                            }
                            else
                            {

                                string script = "Incorrect CIM Number.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                                //DisableElements();

                            }
                        }


                        else
                        {
                            string myStringVariable = "Coachee CIM is a required field.";
                            RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                            //DisableElements();
                        }

                    }
                //}
                //else
                //{
                //    string myStringVariable = "Please select from the items in Session Focus.";
                //    RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                //}
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }

        }
        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {

                //if (Convert.ToInt32(RadReviewID.Text) == 0)
                //{
                //    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                //    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                //    int CIMNumber = Convert.ToInt32(cim_num);
                //    int ReviewID;
                //    DataAccess ws = new DataAccess();
                //    ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 0, CIMNumber, 1, 1);
                //    RadReviewID.Text = Convert.ToString(ReviewID);

                //    InsertReviewKPI();
                //    InsertReviewHistory(1);
                //    InsertReviewHistory(2);
                //    LoadKPIReview(ReviewID);
                //    DisableElements();
                //    RadCoacherSignOff.Visible = false;
                //    string myStringVariable = "Coacher has signed off from this review.";
                //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                //}
                //else
                //{

                    DataAccess ws = new DataAccess();
                    ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 1);
                    RadCoacherSignOff.Visible = false;
                    string myStringVariable = "Coach has signed off from this review.";
                    DisableElements();
                    RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "", "", "");


                //}
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }
        }

        private void InsertReviewKPI()
        {
            try
            {
                int ReviewID;
                //Label Target,Current,Previous,KPIID,DriverID;
                RadComboBox KPI, Driver;
                RadTextBox Target;
                RadNumericTextBox Current, Previous;

                foreach (GridDataItem itm in RadGrid1.Items)
                {
                    ReviewID = Convert.ToInt32(RadReviewID.Text);
                    KPI = (RadComboBox)itm.FindControl("RadKPI");
                    Driver = (RadComboBox)itm.FindControl("RadDriver");
                    Target = (RadTextBox)itm.FindControl("RadTarget");
                    Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                    Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");

                    if (Current.Text != "" && Previous.Text != "")
                    {
                        DataAccess ws = new DataAccess();
                        ws.InsertReviewKPI(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue));

                    }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }

        }

        private void InsertReviewHistory(int InclusionType)
        {
            try
            {
                if (CNPR.Visible == true && CheckBoxes.Visible == true)
                {


                    if (InclusionType == 1)
                    {
                        int ReviewID;
                        //string ReviewHistoryID;

                        foreach (GridDataItem itm in RadGridCN.Items)
                        {
                            ReviewID = Convert.ToInt32(RadReviewID.Text);
                            //ReviewHistoryID = itm["ReviewID"].Text;
                            Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                            DataAccess ws = new DataAccess();
                            ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                        }
                    }
                    else
                    {
                        int ReviewID;
                        //string ReviewHistoryID;

                        foreach (GridDataItem itm in RadGridPR.Items)
                        {
                            ReviewID = Convert.ToInt32(RadReviewID.Text);
                            //ReviewHistoryID = itm["ReviewKPIID"].Text;
                            Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                            DataAccess ws = new DataAccess();
                            ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }

        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);
                RadGridPRR.DataSource = ds;
                RadGridPRR.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }

        }
        public void UploadDocumentationReview(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload2.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
        }

        public void LoadDocumentationsReview()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadDocumentationReview.DataSource = ds;
                RadDocumentationReview.Rebind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
            }

        }

        public bool CheckSSN()
        {
            int CIMNumber = Convert.ToInt32(RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void DisableElements()
        {
            RadAccount.Enabled = false;
            RadSupervisor.Enabled = false;
            RadCoacheeName.Enabled = false;
            RadSessionType.Enabled = false;
            RadSessionTopic.Enabled = false;
            CBList.Enabled = false;
            RadFollowup.Enabled = false;
            RadDescription.Enabled = false;
            RadPositiveBehaviour.Enabled = false;
            RadOpportunityBehaviour.Enabled = false;
            RadBeginBehaviourComments.Enabled = false;
            RadAddressOpportunities.Enabled = false;
            RadActionPlanComments.Enabled = false;
            RadSmartGoals.Enabled = false;
            RadCreatePlanComments.Enabled = false;
            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, RadAsyncUpload2, null);
            RadAsyncUpload2.Visible = false;
            if (CheckBoxes.Visible)
            {
                CheckBox1.Enabled = false;
                CheckBox2.Enabled = false;
            }
            if (CNPR.Visible)
            {
                RadGrid1.Visible = false;
                RadButton1.Visible = false;
            }
            if (RadStrengths.Visible)
            {
                RadStrengths.Enabled = false;
            }
            if (RadOpportunities.Visible)
            {
                RadOpportunities.Enabled = false;
            }
            if (RadBehaviourEffect.Visible)
            {
                RadBehaviourEffect.Enabled = false;
            }
            if (RadRootCause.Visible)
            {
                RadRootCause.Enabled = false;
            }
            if (RadReviewResultsComments.Visible)
            {
                RadReviewResultsComments.Enabled = false;
            }
            if (RadFollowThrough.Visible)
            {
                RadFollowThrough.Enabled = false;
            }
        }
        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }
        protected void SetLabels(int CIMNumber)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));


                if (ds.Tables[0].Rows.Count > 0)
                {
                    string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();

                    string AccountID = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                    string Account = ds.Tables[0].Rows[0]["Account"].ToString();

                    string SupervisorID = ds.Tables[0].Rows[0]["Reports_To"].ToString();
                    string SupervisorFirstName = ds.Tables[0].Rows[0]["SupFirst Name"].ToString();
                    string SupervisorLastName = ds.Tables[0].Rows[0]["SupLast Name"].ToString();

                    string FullName = FirstName + " " + LastName;
                    string SupFullName = SupervisorFirstName + " " + SupervisorLastName;

                    RadAccount.AllowCustomText = true;
                    RadAccount.Text = Account;
                    RadAccount.SelectedValue = AccountID;
                    RadCoacheeName.AllowCustomText = true;
                    RadCoacheeName.SelectedValue = CIMNumber.ToString();
                    RadCoacheeName.Text = FullName;
                    RadSupervisor.AllowCustomText = true;
                    RadSupervisor.SelectedValue = SupervisorID;
                    RadSupervisor.Text = SupFullName;
                    RadAccount.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeCIM.Text = CIMNumber.ToString();
                    //LblSelected.Text = FullName;
                    Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                    ctrlA.Text = FullName;
                    ctrlA.Visible = true;
                }

                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue)))
                {
                    RadCoacheeCIM.Text = RadCoacheeName.SelectedValue;
                    RadCoacheeCIM.Enabled = false;
                    RadSSN.Visible = true;
                }
                else
                {
                    RadCoacheeCIM.Text = "";
                    RadSSN.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }


        }
        #endregion




        //export to PDF 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {

            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                try
                {
                    DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                    DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                    DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                    string ImgDefault;
                    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                    //FakeURLID.Value = URL;
                    //if (!Directory.Exists(directoryPath))
                    //{
                    //    ImgDefault = URL + "/Content/images/no-photo.jpg";
                    //}
                    //else
                    //{

                    //    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                    //}

                    Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.NewPage();

                    string cs = "Coaching Ticket ";
                    PdfPTable specificstable = new PdfPTable(2);
                    string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader));
                    specificscell.Colspan = 2;
                    //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                    specificscell.Border = 0;
                    specificstable.AddCell(specificscell);
                    //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                    //specificscelli00.Border = 0;
                    //                   specificscelli00.Rowspan = 7;


                    PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs)));
                    specificscelli1.Border = 0;
                    PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text)));
                    specificscelli2.Border = 0;


                    PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name "));
                    specificscelli3.Border = 0;
                    PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"])));
                    specificscelli4.Border = 0;

                    PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor "));
                    specificscelli5.Border = 0;
                    string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                    PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname));
                    specificscelli6.Border = 0;

                    PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department "));
                    specificscelli7.Border = 0;
                    PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"])));
                    specificscelli8.Border = 0;


                    PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign "));
                    specificscelli9.Border = 0;
                    string campval = "";
                    if  (ds2.Tables[0].Rows.Count == 0)
                    {
                    }
                    else
                    {
                        campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                    }
                    PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval)));
                    specificscelli10.Border = 0;

                    PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic "));
                    specificscelli11.Border = 0;
                    PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])));
                    specificscelli12.Border = 0;

                    PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  "));
                    specificscelli13.Border = 0;
                    PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])));
                    specificscelli14.Border = 0;

                    //specificstable.AddCell(gif);
                    specificstable.AddCell(specificscelli1);
                    specificstable.AddCell(specificscelli2);
                    specificstable.AddCell(specificscelli3);
                    specificstable.AddCell(specificscelli4);
                    specificstable.AddCell(specificscelli5);
                    specificstable.AddCell(specificscelli6);
                    specificstable.AddCell(specificscelli7);
                    specificstable.AddCell(specificscelli8);
                    specificstable.AddCell(specificscelli9);
                    specificstable.AddCell(specificscelli10);
                    specificstable.AddCell(specificscelli11);
                    specificstable.AddCell(specificscelli12);
                    specificstable.AddCell(specificscelli13);
                    specificstable.AddCell(specificscelli14);
                    specificstable.SpacingAfter = 40;

//                    ds_get_review_forPDF.Tables[0].Rows[0][""]
                    Paragraph p9 = new Paragraph(); //Description
                    string line9 = "What is the purpose of this coaching? : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]);
                    p9.Alignment = Element.ALIGN_LEFT;
                    p9.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p9.Add(line9);
                    p9.SpacingBefore = 40;
                    
                    Paragraph p10 = new Paragraph();
                    string line10 = "Begin with Behaviour " + Environment.NewLine + "What positive behaviour did the employee exhibit? " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["PositiveBehaviour"]);
                    string line10a = Environment.NewLine + "What opportunity did the employee exhibit in the behaviour? " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["OpportunityBehaviour"]);
                    p10.Alignment = Element.ALIGN_LEFT; 
                    p10.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p10.Add(line10);
                    p10.Add(line10a);
                    p10.SpacingBefore = 10;
                    p10.SpacingAfter = 10;

                    Paragraph p11 = new Paragraph();
                    string line11 = "Questions(See Opening questions-successes and Areas for development.Prepare 1-2.)." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["BeginBehaviourComments"]);
                    p11.Alignment = Element.ALIGN_LEFT;
                    p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p11.Add(line11);
                    p11.SpacingBefore = 10;
                    p11.SpacingAfter = 10;

                    Paragraph p12 = new Paragraph();
                    string line12 = "Outline Action Plans. Keep it concise." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["AddressOpportunities"]);
                    p12.Alignment = Element.ALIGN_LEFT;
                    p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p12.Add(line12);
                    p12.SpacingBefore = 10;
                    p12.SpacingAfter = 10;

                    Paragraph p14 = new Paragraph();
                    string line14 = "Questions(See general proving & discovery.Prepare 1-2.)." + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["ActionPlanComments"]); //(RadActionPlanComments.Text);
                    p14.Alignment = Element.ALIGN_LEFT;
                    p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p14.Add(line14);
                    p14.SpacingBefore = 10;
                    p14.SpacingAfter = 10;


                    Paragraph p15 = new Paragraph();
                    string line15 = "Create Plan" + Environment.NewLine + Convert.ToString(RadSmartGoals.Text) + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatePlanComments"]); //(RadCreatePlanComments.Text);
                    p15.Alignment = Element.ALIGN_LEFT;
                    p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p15.Add(line15);
                    p15.SpacingBefore = 10;
                    p15.SpacingAfter = 10;



                    Paragraph p18 = new Paragraph();
                    string line18 = "Performance Result " + Environment.NewLine;
                    p18.Alignment = Element.ALIGN_LEFT;
                    p18.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p18.Add(line18);
                    p18.SpacingBefore = 10;
                    p18.SpacingAfter = 10;


                    PdfPTable perftable = new PdfPTable(5);
                    string perfheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell perfcell = new PdfPCell(new Phrase(perfheader));
                    perfcell.Colspan = 5;
                    perfcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    perftable.AddCell(perfcell);
                    perftable.SpacingBefore = 30;
                    perftable.SpacingAfter = 30;
                    DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadReviewID.Text));
                    if (ds_KPILIST.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_KPILIST.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI"));
                                perfcellh1.Border = 0;
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                                perfcellh2.Border = 0;
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current"));
                                perfcellh3.Border = 0;
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous"));
                                perfcellh4.Border = 0;
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                                perfcellh5.Border = 0;
                                perftable.AddCell(perfcellh1);
                                perftable.AddCell(perfcellh2);
                                perftable.AddCell(perfcellh3);
                                perftable.AddCell(perfcellh4);
                                perftable.AddCell(perfcellh5);
                            }
                            PdfPCell perfcellitem1 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["KPI"])));
                            perfcellitem1.Border = 0;
                            perftable.AddCell(perfcellitem1);

                            PdfPCell perfcellitem2 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Target"])));
                            perfcellitem2.Border = 0;
                            perftable.AddCell(perfcellitem2);

                            PdfPCell perfcellitem3 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Current"])));
                            perfcellitem3.Border = 0;
                            perftable.AddCell(perfcellitem3);

                            PdfPCell perfcellitem4 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Previous"])));
                            perfcellitem4.Border = 0;
                            perftable.AddCell(perfcellitem4);


                            PdfPCell perfcellitem5 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Description"])));
                            perfcellitem5.Border = 0;
                            perftable.AddCell(perfcellitem5);
                        }
                    }
                    else
                    {
                        //perftable.AddCell("Target");
                        //perftable.AddCell("Current");
                        //perftable.AddCell("Previous");
                        //perftable.AddCell("Description");
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI"));
                        perfcellh1.Border = 0;
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                        perfcellh2.Border = 0;
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current"));
                        perfcellh3.Border = 0;
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous"));
                        perfcellh4.Border = 0;
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                        perfcellh5.Border = 0;
                        perftable.AddCell(perfcellh1);
                        perftable.AddCell(perfcellh2);
                        perftable.AddCell(perfcellh3);
                        perftable.AddCell(perfcellh4);
                        perftable.AddCell(perfcellh5);
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Performance Summary"));
                        perfcelli1.Border = 0;
                        perftable.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(" "));
                        perfcelli2.Border = 0;
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                    }
 
                    Paragraph p16 = new Paragraph();
                    string line16 = "Coaching Notes " + Environment.NewLine;
                    p16.Alignment = Element.ALIGN_LEFT;
                    p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p16.Add(line16);
                    p16.SpacingBefore = 10;
                    p16.SpacingAfter = 10;



                    PdfPTable tablenotes = new PdfPTable(5);
                    string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell notescell = new PdfPCell(new Phrase(notesheader));
                    notescell.Colspan = 5;
                    notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    tablenotes.AddCell(notescell);
                    tablenotes.SpacingBefore = 30;
                    tablenotes.SpacingAfter = 30;
                    DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                    if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                                perfcellh1.Border = 0;
                                tablenotes.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                                perfcellh2.Border = 0;
                                tablenotes.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                                perfcellh3.Border = 0;
                                tablenotes.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                                perfcellh4.Border = 0;
                                tablenotes.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                                perfcellh5.Border = 0;
                                tablenotes.AddCell(perfcellh4);

                            }
                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"])));
                            perfcelli1.Border = 0;
                            tablenotes.AddCell(perfcelli1);

                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"])));
                            perfcelli2.Border = 0;
                            tablenotes.AddCell(perfcelli2);

                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"])));
                            perfcelli3.Border = 0;
                            tablenotes.AddCell(perfcelli3);

                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"])));
                            perfcelli4.Border = 0;
                            tablenotes.AddCell(perfcelli4);

                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"])));
                            perfcelli5.Border = 0;
                            tablenotes.AddCell(perfcelli5);


                        }

                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                        perfcellh1.Border = 0;
                        tablenotes.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                        perfcellh2.Border = 0;
                        tablenotes.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                        perfcellh3.Border = 0;
                        tablenotes.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                        perfcellh4.Border = 0;
                        tablenotes.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                        perfcellh5.Border = 0;
                        tablenotes.AddCell(perfcellh4);

                        PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes."));
                        notescelli1.Border = 0;
                        tablenotes.AddCell(notescelli1);
                        PdfPCell notescelli2 = new PdfPCell(new Phrase(" "));
                        notescelli2.Border = 0;
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                    }


                    DataSet ds_perfhistory = null;
                    DataAccess ws1 = new DataAccess();
                    ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadReviewID.Text)), 2);

                    //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    Paragraph p17 = new Paragraph();
                    string line17 = "Performance history";
                    p17.Alignment = Element.ALIGN_LEFT;
                    p17.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p17.Add(line17);
                    p17.SpacingBefore = 10;
                    p17.SpacingAfter = 10;

                    PdfPTable tableperfhist = new PdfPTable(9);
                    string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                    PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader));
                    perfhistcell.Colspan = 9;
                    perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    tableperfhist.AddCell(perfhistcell);
                    tableperfhist.SpacingBefore = 30;
                    tableperfhist.SpacingAfter = 30;
                    if (CheckBox2.Checked == true)
                    {
                        if (ds_perfhistory.Tables[0].Rows.Count > 0)
                        {
                            for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                            {
                                if (ctr == 0)
                                {
                                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                                    perfcellh1.Border = 0;
                                    tableperfhist.AddCell(perfcellh1);
                                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("FullName"));
                                    perfcellh2.Border = 0;
                                    tableperfhist.AddCell(perfcellh2);
                                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number"));
                                    perfcellh3.Border = 0;
                                    tableperfhist.AddCell(perfcellh3);
                                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target"));
                                    perfcellh4.Border = 0;
                                    tableperfhist.AddCell(perfcellh4);
                                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current"));
                                    perfcellh5.Border = 0;
                                    tableperfhist.AddCell(perfcellh5);
                                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous"));
                                    perfcellh6.Border = 0;
                                    tableperfhist.AddCell(perfcellh6);
                                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name"));
                                    perfcellh7.Border = 0;
                                    tableperfhist.AddCell(perfcellh7);
                                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date"));
                                    perfcellh8.Border = 0;
                                    tableperfhist.AddCell(perfcellh8);
                                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By"));
                                    perfcellh9.Border = 0;
                                    tableperfhist.AddCell(perfcellh9);

                                }

                                PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"])));
                                perfcelli1.Border = 0;
                                tableperfhist.AddCell(perfcelli1);
                                PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"])));
                                perfcelli2.Border = 0;
                                tableperfhist.AddCell(perfcelli2);
                                PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"])));
                                perfcelli3.Border = 0;
                                tableperfhist.AddCell(perfcelli3);
                                PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"])));
                                perfcelli4.Border = 0;
                                tableperfhist.AddCell(perfcelli4);
                                PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"])));
                                perfcelli5.Border = 0;
                                tableperfhist.AddCell(perfcelli5);
                                PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"])));
                                perfcelli6.Border = 0;
                                tableperfhist.AddCell(perfcelli6);
                                PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"])));
                                perfcelli7.Border = 0;
                                tableperfhist.AddCell(perfcelli7);
                                PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"])));
                                perfcelli8.Border = 0;
                                tableperfhist.AddCell(perfcelli8);
                                PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"])));
                                perfcelli9.Border = 0;
                                tableperfhist.AddCell(perfcelli9);

                            }
                        }
                        else
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                            perfcellh1.Border = 0;
                            tableperfhist.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("FullName"));
                            perfcellh2.Border = 0;
                            tableperfhist.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number"));
                            perfcellh3.Border = 0;
                            tableperfhist.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target"));
                            perfcellh4.Border = 0;
                            tableperfhist.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current"));
                            perfcellh5.Border = 0;
                            tableperfhist.AddCell(perfcellh5);
                            PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous"));
                            perfcellh6.Border = 0;
                            tableperfhist.AddCell(perfcellh6);
                            PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name"));
                            perfcellh7.Border = 0;
                            tableperfhist.AddCell(perfcellh7);
                            PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date"));
                            perfcellh8.Border = 0;
                            tableperfhist.AddCell(perfcellh8);
                            PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By"));
                            perfcellh9.Border = 0;
                            tableperfhist.AddCell(perfcellh9);
                            PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Previous Performance History."));
                            perfcelli1.Border = 0;
                            tableperfhist.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(""));
                            perfcelli2.Border = 0;
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);
                            tableperfhist.AddCell(perfcelli2);

                        }


                    }
                    else
                    {

                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                        perfcellh1.Border = 0;
                        tableperfhist.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("FullName"));
                        perfcellh2.Border = 0;
                        tableperfhist.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number"));
                        perfcellh3.Border = 0;
                        tableperfhist.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target"));
                        perfcellh4.Border = 0;
                        tableperfhist.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current"));
                        perfcellh5.Border = 0;
                        tableperfhist.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous"));
                        perfcellh6.Border = 0;
                        tableperfhist.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name"));
                        perfcellh7.Border = 0;
                        tableperfhist.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date"));
                        perfcellh8.Border = 0;
                        tableperfhist.AddCell(perfcellh8);
                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By"));
                        perfcellh9.Border = 0;
                        tableperfhist.AddCell(perfcellh9);
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Previous Performance History."));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(""));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);


                    }
                    Paragraph p13 = new Paragraph();
                    string line13 = "Documentation " + Environment.NewLine;
                    p13.Alignment = Element.ALIGN_LEFT;
                    p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p13.Add(line13);
                    p13.SpacingBefore = 10;
                    p13.SpacingAfter = 10;

                    PdfPTable table = new PdfPTable(4);
                    string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cell = new PdfPCell(new Phrase(header));
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cell);
                    table.SpacingBefore = 30;
                    table.SpacingAfter = 30;

                    DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                    if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                        {

                            if (ctr == 0)
                            {
                                //table.AddCell("Item Number");
                                //table.AddCell("Document Name");
                                //table.AddCell("Uploaded By");
                                //table.AddCell("Date Uploaded");
                                PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number"));
                                cellh1.Border = 0;
                                table.AddCell(cellh1);
                                PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name"));
                                cellh2.Border = 0;
                                table.AddCell(cellh2);
                                PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By"));
                                cellh3.Border = 0;
                                table.AddCell(cellh3);
                                PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                                cellh4.Border = 0;
                                table.AddCell(cellh4);

                            }
                            //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                            //alldocs = documents + Environment.NewLine + alldocs;
                             //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                             //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                             //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                             //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));
                            PdfPCell celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"])));
                            celli1.Border = 0;
                            table.AddCell(celli1);


                            PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"])));
                            celli2.Border = 0;
                            table.AddCell(celli2);

                            PdfPCell celli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"])));
                            celli3.Border = 0;
                            table.AddCell(celli3);

                            PdfPCell celli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"])));
                            celli4.Border = 0;
                            table.AddCell(celli4);
                        }
                    }
                    else
                    {
                        PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number"));
                        cellh1.Border = 0;
                        table.AddCell(cellh1);
                        PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name"));
                        cellh2.Border = 0;
                        table.AddCell(cellh2);
                        PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By"));
                        cellh3.Border = 0;
                        table.AddCell(cellh3);
                        PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                        cellh4.Border = 0;
                        table.AddCell(cellh4);

                        PdfPCell celli1 = new PdfPCell(new Phrase("No documents uploaded."));
                        celli1.Border = 0;
                        table.AddCell(celli1);
                        PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString("")));
                        celli2.Border = 0;
                        table.AddCell(celli2);
                        table.AddCell(celli2);
                        table.AddCell(celli2);

                    }

                
               

                    pdfDoc.Add(specificstable);
                    pdfDoc.Add(new Paragraph(p9)); //description
                    pdfDoc.Add(new Paragraph(p10)); //"Begin with Behaviour "
                    pdfDoc.Add(new Paragraph(p11)); //"Questions(See Opening questions-successes and Areas for development.Prepare 1-2.)."
                    pdfDoc.Add(new Paragraph(p12)); //"Outline Action Plans. Keep it concise."
                    pdfDoc.Add(new Paragraph(p14)); //"Questions(See general proving & discovery.Prepare 1-2.)." 
                    pdfDoc.Add(new Paragraph(p15)); //create plan
                    pdfDoc.Add(new Paragraph(p18)); //perftable
                    pdfDoc.Add(perftable);
                    pdfDoc.Add(new Paragraph(p16)); //coaching notes
                    pdfDoc.Add(tablenotes); //coaching notes
                    pdfDoc.Add(new Paragraph(p17));
                    pdfDoc.Add(tableperfhist);
                    pdfDoc.Add(new Paragraph(p13)); //documentation
                    pdfDoc.Add(table);//documentation
                   


                    pdfDoc.Close();


                    Response.ContentType = "application/pdf";
                    string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                        + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                    string filenameheader = filename + ".pdf";
                    string filenameheadername = "filename=" + filenameheader;
                    Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message.ToString());

                }
            }
            else
            {
                string script = "alert('Please submit the review before creating a pdf.')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "its working", script, true);

            }

        }        //export to PDF
      
          
    }
}
