﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddODReview.aspx.cs" Inherits="CoachV2.AddODReview"  %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
  <script type="text/javascript" src="libs/js/JScript1.js"></script>

<script type="text/javascript"">
    

    var objChkd;

    function HandleOnCheck() {

        var chkLst = document.getElementById('CBList');
        if (objChkd && objChkd.checked)
            objChkd.checked = false;
        objChkd = event.srcElement;
    }
    function OnClientSelectedIndexChanged(sender, args) {
        __doPostBack('RadKPI', '');
    }
    function toggleTables() {
        document.getElementById('RadAsyncUpload2').style.display = 'block';
        document.getElementById('RadAsyncUpload2').style.display = 'none';
    }

      function onRequestStart(sender, args) {
  
                               if (args.get_eventTarget().indexOf("btn_ExporttoPDF") >= 0) {
                                    args.set_enableAjax(false);

                              }
                              }
</script>
<script type="text/javascript">
    function callBackFn(arg) {
        alert("this is the client-side callback function. The RadAlert returned: " + arg);
    }
  
 
</script>
<telerik:RadScriptBlock ID="Sc" runat="server">
<script type="text/javascript">
    function ValidateModuleList(source, args) {
        var chkListModules = document.getElementById('<%= CBList.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
</script>
</telerik:RadScriptBlock>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAccount">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSupervisor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Window1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridCN" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="PreviousCommitment" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Window2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadSessionTopic">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2"/>
                    <telerik:AjaxUpdatedControl ControlID="PanelStOpps" />
                    <telerik:AjaxUpdatedControl ControlID="PanelPreviousCommitment" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="Followup" />
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGrid1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2"/>
<%--                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel2" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="Followup" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGrid1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadKPI">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
             </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" />
                    <%--<telerik:AjaxUpdatedControl ControlID="CoacheeSSS"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSSN"  />--%>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeSignOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadReviewID" />
                    <telerik:AjaxUpdatedControl ControlID="RadDocumentationReview"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAccount"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSupervisor"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCoacheeName"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic"/>
                    <telerik:AjaxUpdatedControl ControlID="CBList"/>
                    <telerik:AjaxUpdatedControl ControlID="RadFollowup"/>
                    <telerik:AjaxUpdatedControl ControlID="RadDescription"/>
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxes"/>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel3"/>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1"/>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPRR"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCallID" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="grd_Commitment" />
                    <telerik:AjaxUpdatedControl ControlID="RadCoacherFeedback" />
                    <telerik:AjaxUpdatedControl ControlID="PanelStOpps" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="btn_ExporttoPDF">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadReviewID" />
                    <telerik:AjaxUpdatedControl ControlID="RadDocumentationReview"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAccount"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSupervisor"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCoacheeName"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic"/>
                    <telerik:AjaxUpdatedControl ControlID="CBList"/>
                    <telerik:AjaxUpdatedControl ControlID="RadFollowup"/>
                    <telerik:AjaxUpdatedControl ControlID="RadDescription"/>
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxes"/>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel3"/>
                    <telerik:AjaxUpdatedControl ControlID="SignOut"/>
                    <telerik:AjaxUpdatedControl ControlID="RadGridPRR"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCallID" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="grd_Commitment" />
                    <telerik:AjaxUpdatedControl ControlID="RadCoacherFeedback" />
                    <telerik:AjaxUpdatedControl ControlID="PanelStOpps" />
                </UpdatedControls>
            </telerik:AjaxSetting>
         </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:Panel ID="pnlMain" runat="server">
        <div class="menu-content bg-alt">
            <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
            <div class="panel-group" id="accordion">
                  <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="Account" class="control-label col-xs-2">
                                Coaching Ticket</label>
                            <div class="col-xs-3">
                                <telerik:RadTextBox ID="RadReviewID" runat="server" Width="100%" class="form-control"
                                    Text="0" ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                            <label for="Account" class="control-label col-xs-3">
                                Coaching Date</label>
                            <div class="col-xs-2">
                                <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                    Text="0" ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                              <div class="col-xs-2 text-right">
                             <asp:HiddenField ID="FakeURLID" runat="server" />
                             <asp:HiddenField ID="hfGridHtml" runat="server" />
                                <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                    Visible="true" OnClick="btn_ExporttoPDF_Click">
                                    <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                </telerik:RadButton>
                            </div>
                        </div>
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th id="SelectCoacheeGroup" runat="server">
                                        Select Coachee
                                    </th>
                                    <th>
                                        Choose Review Specifics
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="CoacheeInfoGroup" runat="server">
                                        <div class="form-group">
                                            <label for="Account" class="control-label col-xs-4">
                                                Account</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadAccount" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="1" 
                                                    Width="100%" EmptyMessage="-Select Account-" onselectedindexchanged="RadAccount_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                                    ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Supervisor" class="control-label col-xs-4">
                                                Supervisor</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSupervisor" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="2" 
                                                    Width="100%" EmptyMessage="-Select Supervisor-" onselectedindexchanged="RadSupervisor_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="CoacheeName" class="control-label col-xs-4">
                                                Coachee Name</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadCoacheeName" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="3" 
                                                    Width="100%" EmptyMessage="-Select Coachee-" onselectedindexchanged="RadCoacheeName_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                                    ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Call ID</label>
                                            <div class="col-xs-8">
                                                <telerik:RadNumericTextBox ID="RadCallID" runat="server" class="form-control"
                                                    RenderMode="Lightweight" TabIndex="4" 
                                                    Width="100%" EmptyMessage="-Enter Call ID-" NumberFormat-GroupSeparator="" >
                                                    <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                </telerik:RadNumericTextBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="AddReview"
                                                    ControlToValidate="RadCallID" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Session Type</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="4" 
                                                    Width="100%" EmptyMessage="-Select Session Type-" 
                                                    onselectedindexchanged="RadSessionType_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSessionType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Topic" class="control-label col-xs-4">
                                                Topic</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                                    TabIndex="5" AutoPostBack="true" Width="100%" 
                                                    EmptyMessage="-Select Session Topic-" 
                                                    onselectedindexchanged="RadSessionTopic_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                  <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSessionTopic" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Topic" class="control-label col-xs-4">
                                                Session Focus</label>
                                            <div class="col-xs-8">
                                                <asp:CheckBoxList ID="CBList" runat="server" RepeatLayout="Table" RepeatDirection="Vertical"
                                                    Onclick="return HandleOnCheck()">
                                                </asp:CheckBoxList>
                                                <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                                    ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field." ValidationGroup="AddReview"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group" runat="server" id="CheckBoxes">
                                            <div class="col-xs-9">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Include Previous Coaching Notes"
                                                    AutoPostBack="true" Width="100%" OnCheckedChanged="CheckBox1_CheckedChanged" /><br />
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group" runat="server" id="FollowUp">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Follow up Date</label>
                                            <div class="col-xs-8">
                                                <telerik:RadDatePicker ID="RadFollowup" runat="server" placeholder="Date" class="form-control"
                                                    TabIndex="6" Width="75%" OnLoad="RadFollowup_Load">
                                                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                        FastNavigationNextText="&amp;lt;&amp;lt;">
                                                    </Calendar>
                                                    <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                        TabIndex="6">
                                                        <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                        <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                        <FocusedStyle Resize="None"></FocusedStyle>
                                                        <DisabledStyle Resize="None"></DisabledStyle>
                                                        <InvalidStyle Resize="None"></InvalidStyle>
                                                        <HoveredStyle Resize="None"></HoveredStyle>
                                                        <EnabledStyle Resize="None"></EnabledStyle>
                                                    </DateInput>
                                                    <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                </telerik:RadDatePicker>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="AddReview"
                                                    ControlToValidate="RadFollowup" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Description<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom" id="PerfResult" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Performance Result <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource" 
                                        AllowPaging="false" Width="100%"  AllowAutomaticInserts="false" AllowAutomaticUpdates="true"
                                        EnableLoadOnDemand="True"  EnableViewState="true"  OnItemDataBound="OnItemDataBoundHandler"
                                        OnInsertCommand="RadGrid1_InsertCommand" OnUpdateCommand="RadGrid1_UpdateCommand" OnDeleteCommand="RadGrid1_DeleteCommand" CssClass="RemoveBorders" BorderStyle="None">
                                        <PagerStyle AlwaysVisible="true" />                                        
                                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Bottom" EditMode="EditForms" InsertItemDisplay="Bottom" >
                                            <CommandItemSettings AddNewRecordText="Add More" ShowRefreshButton="false" ShowAddNewRecordButton="false"/>
                                            <EditFormSettings>
                                                  <EditColumn UniqueName="EditCommandColumn" ButtonType="PushButton" >
                                                  </EditColumn>
                                                </EditFormSettings>
                                            <Columns>
                                         
                                             <telerik:GridTemplateColumn HeaderText="ID" UniqueName="ID" DataField="ID" Visible="false">
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblID" runat="server" Text='<%# Eval("ID") %>' DataTextField="ID"  DataValueField="ID" Enabled="false" Visible="false">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="RadID" runat="server" DataTextField="ID"  DataValueField="ID" Enabled="false" Visible="false">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </asp:Label>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                <telerik:GridTemplateColumn HeaderText="KPI" UniqueName="KPIID" DataField="KPIID">
                                                    <ItemTemplate>
                                                       <%# Eval("KPIID") %>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadComboBox ID="RadKPI" runat="server" DataTextField="Name"  DataValueField="KPIID" OnSelectedIndexChanged="RadKPI_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false">
                                                      </telerik:RadComboBox>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                  <telerik:GridTemplateColumn HeaderText="KPIValue" UniqueName="KPIValue" DataField="KPIValue" Visible="false">
                                                    <ItemTemplate>
                                                         <asp:Label ID="LblKPI" runat="server" Text='<%# Eval("Name") %>' DataTextField="Name"  DataValueField="Name">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                  
                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                    <ItemTemplate>
                                                      <asp:Label ID="LblTarget" runat="server" Text='<%# Eval("Target") %>' DataTextField="Target"  DataValueField="Target">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadTextBox ID="RadTarget" runat="server" Enabled="false" >
                                                        </telerik:RadTextBox>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                 <telerik:GridTemplateColumn HeaderText="Current">
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblCurrent" runat="server" Text='<%# Eval("Current") %>' DataTextField="Current"  DataValueField="Current">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadNumericTextBox ID="RadCurrent" runat="server" >
                                                        </telerik:RadNumericTextBox>
<%--                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  runat="server" ErrorMessage="*"
                                                ControlToValidate="RadCurrent"></asp:RequiredFieldValidator>--%>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                 <telerik:GridTemplateColumn HeaderText="Previous">
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblPrevious" runat="server" Text='<%# Eval("Previous") %>' DataTextField="Previous"  DataValueField="Previous">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadNumericTextBox ID="RadPrevious" runat="server" >
                                                        </telerik:RadNumericTextBox>
<%--                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="RadPrevious"></asp:RequiredFieldValidator>--%>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                <telerik:GridTemplateColumn HeaderText="Change">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblChange" runat="server" Text='<%# Eval("Change")%>' DataTextField="Previous"  DataValueField="Previous">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadNumericTextBox ID="RadChange" runat="server" >
                                                        </telerik:RadNumericTextBox>
<%--                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="*"
                                                ControlToValidate="RadChange"></asp:RequiredFieldValidator>--%>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                    <ItemTemplate>
                                                        <%# Eval("Driver") %>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                                           </telerik:RadComboBox>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                   <telerik:GridTemplateColumn HeaderText="DriverValue" UniqueName="DriverValue" DataField="DriverValue" Visible="false">
                                                    <ItemTemplate>
                                                         <asp:Label ID="LblDriver" runat="server" Text='<%# Eval("DriverName") %>' DataTextField="DriverName"  DataValueField="DriverName">
                                                      </asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                   <telerik:GridTemplateColumn HeaderText="Behaviour">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBoxBehaviour" runat="server" Text='<%# Eval("Behaviour") %>' ReadOnly="true" TextMode="MultiLine" Rows="5">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadTextBox ID="RadBehaviour" runat="server" Width="100%" TextMode="MultiLine" Rows="5">
                                                        </telerik:RadTextBox>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                   <telerik:GridTemplateColumn HeaderText="Root Cause">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBoxRootCause" runat="server" Text='<%# Eval("RootCause") %>' ReadOnly="true" TextMode="MultiLine" Rows="5">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadTextBox ID="RadRootCause" runat="server" Width="100%" TextMode="MultiLine" Rows="5">
                                                        </telerik:RadTextBox>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>

                                                      <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn" ButtonType="PushButton" HeaderText="Edit" HeaderStyle-Width="100px" UpdateText="Update"></telerik:GridEditCommandColumn>
                                                        <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Text="Delete" ButtonType="PushButton" HeaderText="Delete" ConfirmTextFormatString="Are you sure you want to delete the selected item?" ConfirmTextFields="Model" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                                                 </Columns>
                                        </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                </Scrolling>
                            </ClientSettings>
                                    </telerik:RadGrid>
                        <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                </Scrolling>
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;</div>
                        <div class="col-md-10">
                        </div>
                        <div class="col-md-2">
                            <telerik:RadButton ID="RadButton1" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                              ForeColor="White" ValidationGroup="Perf" OnClick="Button1_Click">
                            </telerik:RadButton>
                        </div>
                    </div>
                    <asp:Panel ID="CNPRPanel" runat="server">
                            <div id="CNPR" runat="server">
                                <div id="collapseCNPR" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                            Previous Coaching Notes</label><br />
                                         <div style="height:5px"></div>
                                        <div>
                                            <div class="panel-body">
                                                <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                <MasterTableView>
                                                    <Columns>
                                                       <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Topic">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                                        </Scrolling>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </div>

                                 
                                </div>
                            </div>
                        </asp:Panel>
                </div>
            </div>
            <div style="height:5px"></div>
<asp:Panel ID="PanelStOpps" runat="server">
<div id="Div1" runat="server">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Strengths<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Opportunities<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseFive" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
</div>
</asp:Panel>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Commitment<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseSix" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="grd_Commitment" runat="server" AutoGenerateColumns="false" CssClass="RemoveBorders" BorderStyle="None" Height="700px">
                            <MasterTableView>
                                <ColumnGroups>
                                    <telerik:GridColumnGroup Name="Grow" HeaderText="Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below" HeaderStyle-HorizontalAlign="Center" />
                                </ColumnGroups>
                                <HeaderStyle Width="102px" />
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Goal" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="RadTextBox2" runat="server" Text="What do you want to achieve?"
                                                TextMode="MultiLine" Enabled="false" Width="100%" Height="100%" Rows="10">
                                            </telerik:RadTextBox>
                                            <telerik:RadTextBox ID="txtGoal" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="100%" Text='<%# Eval("Goal_Text") %>'  Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Reality" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="RadTextBox3" runat="server" Text="Where are you now? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?"
                                                Width="100%" Height="100%" TextMode="MultiLine" Enabled="false"  Rows="10">
                                            </telerik:RadTextBox>
                                            <telerik:RadTextBox ID="txtReality" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="100%" Text='<%# Eval("Reality_Text") %>'  Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                              
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Options" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="RadTextBox4" runat="server" Text="What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that? (SMART)"
                                                Width="100%" Height="100%" TextMode="MultiLine" Enabled="false"  Rows="10">
                                            </telerik:RadTextBox>
                                            <telerik:RadTextBox ID="txtOptions" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="100%" Text='<%# Eval("Options_Text") %>'  Rows="15" Placeholder="(Free-form field)
                                                Option 1
                                                
                                                Option 2
                                                
                                                Option 3">
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Way Forward" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="RadTextBox5" runat="server" Text="What option do you think will work the best? What will you do and when? What support and resources do you need?"
                                                Width="100%" Height="100%" TextMode="MultiLine" Enabled="false"  Rows="10">
                                            </telerik:RadTextBox>
                                            <telerik:RadTextBox ID="txtWayForward" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="100%" Text='<%# Eval("WayForward_Text") %>'  Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <asp:Panel ID="PanelPreviousCommitment" runat="server">
                        <div id="Div2" runat="server">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="Previous Perfomance Results">
                                            <b>Previous Commitment</b></label>
                                        <span></span>
                                    </div>
                                </div>
                                <%--<div class="row">
                                    <div class="col-md-2">
                                        <label for="Previous Perfomance Results">
                                            <b></b>
                                        </label>
                                        <span></span>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>
                                            <label for="inputPassword">
                                                <b>From: Coaching Ticket #</b></label><span><telerik:RadTextBox ID="RadFReviewID"
                                                    runat="server">
                                                </telerik:RadTextBox></span></h6>
                                    </div>
                                    <div class="col-md-4">
                                        <h6>
                                            <label for="inputPassword">
                                                <b>Coaching Date:</b></label><span><telerik:RadTextBox ID="RadFCoachingDate" runat="server">
                                                </telerik:RadTextBox></span></h6>
                                    </div>
                                </div>--%>
                                <div class="panel-body" id="PreviousCommitment" runat="server">
                                   <%-- <telerik:RadGrid ID="grdPrevCom" runat="server" AutoGenerateColumns="false" CssClass="RemoveBorders"
                                        BorderStyle="None" Height="700px">
                                        <MasterTableView>
                                            <ColumnGroups>
                                                <telerik:GridColumnGroup Name="Grow" HeaderText="Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below"
                                                    HeaderStyle-HorizontalAlign="Center" />
                                            </ColumnGroups>
                                            <HeaderStyle Width="102px" />
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Goal" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBox2" runat="server" Text="What do you want to achieve?"
                                                            TextMode="MultiLine" Enabled="false" Width="100%" Height="100%" Rows="10">
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtGoal" runat="server" TextMode="MultiLine" Width="100%"
                                                            Height="100%" Text='<%# Eval("Goal_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Reality" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBox3" runat="server" Text="Where are you know? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?"
                                                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtReality" runat="server" TextMode="MultiLine" Width="100%"
                                                            Height="100%" Text='<%# Eval("Reality_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Options" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBox4" runat="server" Text="What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that?"
                                                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtOptions" runat="server" TextMode="MultiLine" Width="100%"
                                                            Height="100%" Text='<%# Eval("Options_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                Option 1
                                                
                                                Option 2
                                                
                                                Option 3">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Way Forward" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <telerik:RadTextBox ID="RadTextBox5" runat="server" Text="What option do you think will work the best? What will you do and when? What support and resources do you need?"
                                                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="txtWayForward" runat="server" TextMode="MultiLine" Width="100%"
                                                            Height="100%" Text='<%# Eval("WayForward_Text") %>' Rows="15" Placeholder="(Free-form field)
                                                1.
                                                
                                                2.
                                                
                                                3.">
                                                        </telerik:RadTextBox>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                            <Resizing AllowResizeToFit="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDiv3">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coach Feedback <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseDiv3" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadCoacherFeedback" runat="server" class="form-control" placeholder="Description"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTen" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            OnItemDataBound="RadDocumentationReview_onItemDatabound" CssClass="RemoveBorders"
                            BorderStyle="None">
                            <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Item Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                        FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                        UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                        AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;
                        </div>
                        <div class="col-sm-12">
                            <telerik:RadAsyncUpload ID="RadAsyncUpload2" runat="server" MultipleFileSelection="Automatic" OnClientValidationFailed="OnClientValidationFailed"
                                AllowedFileExtensions=".pdf,.xls,.xlsx,.png" MaxFileSize="1000000" Width="40%" PostbackTriggers="RadCoacheeSignOff" >
                                <Localization Select="                             " />
                            </telerik:RadAsyncUpload>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="container-fluid" id="SignOut" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                            TabIndex="12" placeholder="Coachee CIM Number" Visible="true">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSID"
                            RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" onclick="RadCoacheeSignOff_Click"></telerik:RadButton>
                           <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" Visible="false" onclick="RadCoacherSignOff_Click">
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Width="1000px" Height="500px">
                <Windows>
                    <telerik:RadWindow runat="server" ID="Window1" Modal="true" Behaviors="Maximize,Minimize,Move" >
                        <ContentTemplate>
                            <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                                Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic">
                                <MasterTableView DataKeyNames="ReviewID">
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                        </telerik:GridClientSelectColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                    <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <telerik:RadButton ID="RadCNLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNLink_Click">
                                        </telerik:RadButton>
                                        &nbsp;
                                        <telerik:RadButton ID="RadCNCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNCancel_Click">
                                        </telerik:RadButton>
                                    </span>
                                </div>
                                </div>
                        </ContentTemplate>
                    </telerik:RadWindow>
                 
                    
                </Windows>
            </telerik:RadWindowManager>
        </div>
         </div>
        </div>
    </asp:Panel> 
</asp:Content>
