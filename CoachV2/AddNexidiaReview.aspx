﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNexidiaReview.aspx.cs" Inherits="CoachV2.AddNexidiaReview" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript" src="libs/js/JScript1.js"></script>
<script type="text/javascript">
    

    var objChkd;

    function HandleOnCheck() {

        var chkLst = document.getElementById('CBList');
        if (objChkd && objChkd.checked)
            objChkd.checked = false;
        objChkd = event.srcElement;
    }

            function onRequestStart(sender, args) {
  
                               if (args.get_eventTarget().indexOf("btn_ExporttoPDF") >= 0) {
                                    args.set_enableAjax(false);

                              }
                              }
</script>
<script type="text/javascript">
    function callBackFn(arg) {
        alert("this is the client-side callback function. The RadAlert returned: " + arg);
    }
  
 
</script>
<telerik:RadScriptBlock ID="Sc" runat="server">
<script type="text/javascript">
    function ValidateModuleList(source, args) {
        var chkListModules = document.getElementById('<%= CBList.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;

 
    }
</script>
</telerik:RadScriptBlock>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
       <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAccount">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSupervisor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Window1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Window2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadSessionTopic">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel2" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="Followup" />
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGrid1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel2" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" />
                    <telerik:AjaxUpdatedControl ControlID="Followup" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGrid1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadKPI">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
             </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" />
                    <%--<telerik:AjaxUpdatedControl ControlID="CoacheeSSS"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSSN"  />--%>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeSignOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadReviewID" />
                    <telerik:AjaxUpdatedControl ControlID="RadDocumentationReview"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAccount"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSupervisor"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCoacheeName"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic"/>
                    <telerik:AjaxUpdatedControl ControlID="CBList"/>
                    <telerik:AjaxUpdatedControl ControlID="RadFollowup"/>
                    <telerik:AjaxUpdatedControl ControlID="RadDescription"/>
                    <telerik:AjaxUpdatedControl ControlID="RadPositiveBehaviour"/>
                    <telerik:AjaxUpdatedControl ControlID="RadOpportunityBehaviour"/>
                    <telerik:AjaxUpdatedControl ControlID="RadBeginBehaviourComments"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAddressOpportunities"/>
                    <telerik:AjaxUpdatedControl ControlID="RadActionPlanComments"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSmartGoals"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCreatePlanComments"/>
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxes"/>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel3"/>
                    <telerik:AjaxUpdatedControl ControlID="SignOut"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btn_ExporttoPDF">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadReviewID" />
                    <telerik:AjaxUpdatedControl ControlID="RadDocumentationReview"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAccount"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSupervisor"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCoacheeName"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionType"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic"/>
                    <telerik:AjaxUpdatedControl ControlID="CBList"/>
                    <telerik:AjaxUpdatedControl ControlID="RadFollowup"/>
                    <telerik:AjaxUpdatedControl ControlID="RadDescription"/>
                    <telerik:AjaxUpdatedControl ControlID="RadPositiveBehaviour"/>
                    <telerik:AjaxUpdatedControl ControlID="RadOpportunityBehaviour"/>
                    <telerik:AjaxUpdatedControl ControlID="RadBeginBehaviourComments"/>
                    <telerik:AjaxUpdatedControl ControlID="RadAddressOpportunities"/>
                    <telerik:AjaxUpdatedControl ControlID="RadActionPlanComments"/>
                    <telerik:AjaxUpdatedControl ControlID="RadSmartGoals"/>
                    <telerik:AjaxUpdatedControl ControlID="RadCreatePlanComments"/>
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxes"/>
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel1"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel2"/>
                    <telerik:AjaxUpdatedControl ControlID="Panel3"/>
                    <telerik:AjaxUpdatedControl ControlID="SignOut"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
         </AjaxSettings>
    </telerik:RadAjaxManager>
    <%--  <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
    <label for="Export to PDF" class="control-label col-xs-10">
    </label>
    <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
        Visible="true" OnClick="btn_ExporttoPDF_Click">
        <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
    </telerik:RadButton>--%> 
    <asp:Panel ID="pnlMain" runat="server">
        <div class="menu-content bg-alt">
             
       <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
     <div class="panel-group" id="accordion">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group">
                            
                            <label for="Account" class="control-label col-xs-2">
                                Coaching Ticket</label>
                            <div class="col-xs-3">
                                <telerik:RadTextBox ID="RadReviewID" runat="server" Width="80%" class="form-control"
                                    Text="0" ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                            <label for="Account" class="control-label col-xs-3">
                                Coaching Date</label>
                            <%--<div class="col-xs-4">--%>
                             <div class="col-xs-2">
                                <telerik:RadTextBox ID="RadCoachingDate" Width="80%" runat="server" class="form-control"
                                    Text="0" ReadOnly="true">
                                </telerik:RadTextBox>                               
                            </div> 
                          <div class="col-xs-2 text-right">
                          <asp:HiddenField ID="HiddenField1" runat="server" />
                          <asp:HiddenField ID="FakeURLID" runat="server" />
                             <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                 Visible="true" OnClick="btn_ExporttoPDF_Click">
                                 <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                             </telerik:RadButton>
                         </div>
                         </div>
                         
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        Select Coachee
                                    </th>
                                    <th>
                                        Choose Review Specifics
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="Account" class="control-label col-xs-4">
                                                Account</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadAccount" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="1" Width="100%"
                                                    EmptyMessage="-Select Account-" OnSelectedIndexChanged="RadAccount_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                                    ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Supervisor" class="control-label col-xs-4">
                                                Supervisor</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSupervisor" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="2" Width="100%"
                                                    EmptyMessage="-Select Supervisor-" OnSelectedIndexChanged="RadSupervisor_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="CoacheeName" class="control-label col-xs-4">
                                                Coachee Name</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadCoacheeName" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="3" Width="100%"
                                                    EmptyMessage="-Select Coachee-" OnSelectedIndexChanged="RadCoacheeName_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                                    ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Session Type</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="4" Width="100%"
                                                    EmptyMessage="-Select Session Type-" OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSessionType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Topic" class="control-label col-xs-4">
                                                Topic</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                                    TabIndex="5" AutoPostBack="true" Width="100%" EmptyMessage="-Select Session Topic-"
                                                    OnSelectedIndexChanged="RadSessionTopic_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSessionTopic" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Topic" class="control-label col-xs-4">
                                                Session Focus</label>
                                            <div class="col-xs-8">
                                                <asp:CheckBoxList ID="CBList" runat="server" RepeatLayout="Table" RepeatDirection="Vertical"
                                                    Onclick="return HandleOnCheck()">
                                                </asp:CheckBoxList>
                                                <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                                    ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field." ValidationGroup="AddReview"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group" runat="server" id="CheckBoxes">
                                            <div class="col-xs-9">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Include Previous Coaching Notes"
                                                    AutoPostBack="true" Width="100%" OnCheckedChanged="CheckBox1_CheckedChanged" /><br />
                                                <asp:CheckBox ID="CheckBox2" runat="server" Text="Include Previous Performance Results"
                                                    AutoPostBack="true" OnCheckedChanged="CheckBox2_CheckedChanged" /><br />
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group" runat="server" id="FollowUp">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Follow up Date</label>
                                            <div class="col-xs-8">
                                                <telerik:RadDatePicker ID="RadFollowup" runat="server" placeholder="Date" class="form-control"
                                                    TabIndex="6" Width="75%">
                                                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                        FastNavigationNextText="&amp;lt;&amp;lt;">
                                                    </Calendar>
                                                    <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                        TabIndex="6">
                                                        <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                        <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                        <FocusedStyle Resize="None"></FocusedStyle>
                                                        <DisabledStyle Resize="None"></DisabledStyle>
                                                        <InvalidStyle Resize="None"></InvalidStyle>
                                                        <HoveredStyle Resize="None"></HoveredStyle>
                                                        <EnabledStyle Resize="None"></EnabledStyle>
                                                    </DateInput>
                                                    <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                </telerik:RadDatePicker>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="AddReview"
                                                    ControlToValidate="RadFollowup" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            What is the purpose of this coaching?<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                            ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                            CssClass="validator"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
   <asp:Panel ID="Panel3" runat="server">
        <div id="Div1" runat="server">
        <div style="height:5px"></div>
            <div class="panel panel-custom" id="PerfResult" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Performance Result <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                                <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource"
                                    AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGrid1_ItemCommand"
                                    OnItemCreated="OnItemDataBoundHandler" OnItemDataBound="OnItemDataBound" OnPreRender="RadGrid1_PreRender"
                                    ClientIDMode="AutoID">
                                    <MasterTableView DataKeyNames="NexidiaID" CommandItemDisplay="Bottom" CellSpacing="0">
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                                                Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="KPI">
                                                <ItemTemplate>
                                                    <telerik:RadComboBox ID="RadKPI" runat="server" OnSelectedIndexChanged="RadKPI_SelectedIndexChanged"
                                                        AutoPostBack="true" CausesValidation="false" DropDownAutoWidth="Enabled">
                                                    </telerik:RadComboBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Target">
                                                <ItemTemplate>
                                                    <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                                    </telerik:RadTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Current">
                                                <ItemTemplate>
                                                    <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'>
                                                    </telerik:RadNumericTextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                        ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Previous">
                                                <ItemTemplate>
                                                    <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'>
                                                    </telerik:RadNumericTextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                        ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                                                Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Driver">
                                                <ItemTemplate>
                                                    <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                                    </telerik:RadComboBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                        </Scrolling>
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true">
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                        </Scrolling>
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <div>
                                    &nbsp;</div>
                                <div class="col-md-10">
                                </div>
                                <div class="col-md-2">
                                    <telerik:RadButton ID="RadButton1" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                                        OnClick="Button1_Click" ForeColor="White" ValidationGroup="Perf">
                                    </telerik:RadButton>
                                </div>

                      
                    </div>
                    
                </div>
            </div>
       
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Agent Strengths and Opportunities<span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strength/s: Agent did well on ____"
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunity/ies: Agent will need to improve on ____ "
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
   </asp:Panel>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Begin with Behaviour.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See Opening questions-successes and Areas for development.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                        <%-- <label for="Account" class="control-label">
                                Begin with Behaviour.</label><span class="glyphicon glyphicon-triangle-top triangletop">--%>
                    </div>
                </a>
                <div id="collapseFive" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadPositiveBehaviour" runat="server" class="form-control" placeholder="What positive behaviour did the employee exhibit? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox> 
                                        <telerik:RadTextBox ID="RadOpportunityBehaviour" runat="server" class="form-control" placeholder="What opportunity did the employee exhibit in the behaviour? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox>
                                    
                                </div>
                                <div class="col-md-6">
                                   <telerik:RadTextBox ID="RadBeginBehaviourComments" runat="server" class="form-control" placeholder=""
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="9">
                                        </telerik:RadTextBox> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<asp:Panel ID="Panel1" runat="server">
        <div id="Div3" runat="server">
         <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Review Results. Talk Specifics.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See metric specific questions.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseSix" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div id="Div2" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadBehaviourEffect" runat="server" class="form-control" placeholder="How did the behaviour, attitude, skill and knowledge affect the performance and results? "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox> 
                                        <telerik:RadTextBox ID="RadRootCause" runat="server" class="form-control" placeholder="What is the root cause? Note: Build understanding with the employee "
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                        </telerik:RadTextBox>
                                    
                                </div>
                                <div class="col-md-6">
                                   <telerik:RadTextBox ID="RadReviewResultsComments" runat="server" class="form-control" placeholder=""
                                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="9">
                                        </telerik:RadTextBox> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Panel>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Outline Action Plans. Keep it concise.
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Questions(See general proving & discovery.Prepare 1-2.).
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseSeven" class="panel-collapse collapse in">
                    <div class="panel-body">
                       <%-- <div id="Div3" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <telerik:RadTextBox ID="RadTextBox8" runat="server" class="form-control" placeholder="How does the employee want to address the opportunities? Note: Build on strength. Agree on actions. "
                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                    </telerik:RadTextBox>
                                </div>
                               
                            </div>
                        </div>--%>
                        <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadAddressOpportunities" runat="server" class="form-control" placeholder="How does the employee want to address the opportunities? Note: Build on strength. Agree on actions."
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadActionPlanComments" runat="server" class="form-control" placeholder=""
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           Create Plan
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseEight" class="panel-collapse collapse in">
                    <div class="panel-body">
                       <div class="input-group col-lg-12">
                            <telerik:RadTextBox ID="RadSmartGoals" runat="server" class="form-control" placeholder="What are the SMART goals that you and the agent agreed on? Note: Identify and classify statements based on the SMART
structure below; Establish responsibilities and accountabilities."
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                            <span class="input-group-addon" style="padding:1px; visibility:hidden"></span>
                             <telerik:RadTextBox ID="RadCreatePlanComments" runat="server" class="form-control" placeholder=""
                                TextMode="MultiLine" Width="100%"  RenderMode="Lightweight" Rows="5" TabIndex="9">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:5px"></div>
<asp:Panel ID="Panel2" runat="server">
     <div id="Div5" runat="server">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           Follow Through
                            <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseNine" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <telerik:RadTextBox ID="RadFollowThrough" runat="server" class="form-control" placeholder="What activities have you done with the agent to justify his/her understanding of the coaching points?
Note:Ensure that you are confident that the agent will not commit the same challenge/s once you let him/her go back to the phone."
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                    </telerik:RadTextBox>

                      <asp:Panel ID="CNPRPanel" runat="server">
                            <div id="CNPR" runat="server">
                                <div id="collapseCNPR" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                            Previous Coaching Notes</label><br />
                                         <div style="height:5px"></div>
                                        <div>
                                            <div class="panel-body">
                                                <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                <MasterTableView>
                                                    <Columns>
                                                       <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Topic">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                    <ClientSettings>
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                                        </Scrolling>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                            Previous Perfomance Results</label><br />
                                         <div style="height:5px"></div>
                                        <div>
                                            <div class="panel-body">
                                                <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                 <MasterTableView>
                                                    <Columns>
                                                       <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Target">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Current">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Previous">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                         <telerik:GridTemplateColumn HeaderText="Driver">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                              <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                 <ClientSettings>
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                                        </Scrolling>
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
    <div style="height:5px"></div>
      </div>
</asp:Panel>
            
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTen" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            OnItemDataBound="RadDocumentationReview_onItemDatabound" CssClass="RemoveBorders"
                            BorderStyle="None">
                            <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Item Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                        FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                        UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                        AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;
                        </div>
                        <div class="col-sm-12">
                            <telerik:RadAsyncUpload ID="RadAsyncUpload2" runat="server" MultipleFileSelection="Automatic"
                                AllowedFileExtensions=".pdf,.xlsx,.xls,.png" MaxFileSize="1000000" Width="40%" PostbackTriggers="RadCoacheeSignOff" >
                              <Localization Select="                             " />
                            </telerik:RadAsyncUpload>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="container-fluid" id="SignOut" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                            TabIndex="12" placeholder="Coachee CIM Number" Visible="true">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSID"
                            RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" onclick="RadCoacheeSignOff_Click"></telerik:RadButton>
                           <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" Visible="false" onclick="RadCoacherSignOff_Click"><%-- "btn btn-primary" --%>
                            </telerik:RadButton> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Width="1000px" Height="500px">
                <Windows>
                    <telerik:RadWindow runat="server" ID="Window1" Modal="true" Behaviors="Maximize,Minimize,Move" >
                        <ContentTemplate>
                            <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                                Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic">
                                <MasterTableView DataKeyNames="ReviewID">
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                        </telerik:GridClientSelectColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                    <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <telerik:RadButton ID="RadCNLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNLink_Click">
                                        </telerik:RadButton>
                                        &nbsp;
                                        <telerik:RadButton ID="RadCNCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNCancel_Click">
                                        </telerik:RadButton>
                                    </span>
                                </div>
                                </div>
                        </ContentTemplate>
                    </telerik:RadWindow>
                    <telerik:RadWindow runat="server" ID="Window2" Modal="true" Behaviors="Maximize,Minimize,Move">
                        <ContentTemplate>
                            <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                                Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic">
                                <MasterTableView DataKeyNames="ReviewKPIID">
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                        </telerik:GridClientSelectColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                    <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <telerik:RadButton ID="RadPRLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadPRLink_Click">
                                        </telerik:RadButton>
                                        &nbsp;
                                        <telerik:RadButton ID="RadPRCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadPRCancel_Click">
                                        </telerik:RadButton>
                                    </span>
                                </div>
                            </div>
                        </ContentTemplate>
                    </telerik:RadWindow>
                    
                </Windows>
            </telerik:RadWindowManager>
        </div>
        </div>
    </div>
  </asp:Panel> 
</asp:Content>
