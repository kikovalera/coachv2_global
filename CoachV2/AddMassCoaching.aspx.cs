﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;

namespace CoachV2
{
    public partial class AddMassCoaching : System.Web.UI.Page
    {

        public DataTable DataSelectedEmployees
        {
            get
            {
                return ViewState["DataSelectedEmployees"] as DataTable;
            }
            set
            {
                ViewState["DataSelectedEmployees"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //LoadDocumentations();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

                Session["Role"] = Session["Role"] == null ? dsSAPInfo.Tables[0].Rows[0]["Role"].ToString() : Session["Role"];

                int CIMNumber = Convert.ToInt32(cim_num);
                GetAccounts(CIMNumber);
                GetDropDownSessions();
                RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                PerfResultsEmployee();
                GetMassCoachingTypes();
                PanelGroups.Visible = false;
                PanelSelected.Visible = false;
                RadGridDocumentation.DataSource = String.Empty;
                RadGridDocumentation.DataBind();

                Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                ctrlReviews.Text = " Coaching Dashboard ";
                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                ctrlA.Text = " > My Reviews";
                Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                ctrlB.Text = " > Add Mass Review";
                ctrlB.Visible = true;
            }
            else {
                LoadDocumentations();
            }

        }

        private void GetMassCoachingTypes()
        {
            try
            {
                int CIMNumber = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('|')[3]);
                // replace Session["Role"] with actual value based on login role (francis.valera/07192018)
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(CIMNumber);
                string roleval = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ////ds = ws.GetCoachTypes(CIMNumber,Session["Role"].ToString());
                ds = ws.GetCoachTypes(CIMNumber, roleval);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RadMassType.DataSource = ds;
                    RadMassType.DataTextField = "MassCoachingType";
                    RadMassType.DataValueField = "ID";
                    RadMassType.DataBind();
                }
            }
            catch (Exception ex)
            {
                // provided generic error msg during RadMassType binding (francis.valera/07192018)
                //string ModalLabel = "GetMassCoachingTypes Error " + ex.Message.ToString();
                string ModalLabel = "Unable to complete loading process (MassCoachingTypes). Please reload the page to try again.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void PerfResultsEmployee()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CIM", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            ViewState["DataSelectedEmployees"] = dt;
            BindListView();
        }
        protected void RadMassType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadMassReviewElements();
        }
        public void LoadMassReviewElements()
        {
            if (RadMassType.SelectedItem.Text == "Mass Review All")
            {
                PanelGroups.Visible = false;
                PanelSelected.Visible = false;
            }
            else if (RadMassType.SelectedItem.Text == "Mass Review Groups")
            {
                PanelGroups.Visible = true;
                PanelSelected.Visible = false;
                 
                string role = Session["Role"].ToString();
                //string om = "Manager";
                string[] om = new string[] {"Manager","BM" };
                if (role.Equals("TL"))
                {
                    RadSite.Enabled = false;
                    RadAccount.Enabled = false;
                    RadCampaign.Enabled = false;
                }
                if (om.Any(s => role.Contains(s))) {
                    RadSite.Enabled = false;
                    RadCampaign.Enabled = false;
                }
            }
            else if (RadMassType.SelectedItem.Text == "Mass Review Selected")
            {
                PanelGroups.Visible = false;
                PanelSelected.Visible = true;
            }
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {  
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                //string ModalLabel = "RadSessionType_SelectedIndexChanged " + ex.Message.ToString();

                // provided generic error msg during RadMassType binding (francis.valera/07192018)
                //string ModalLabel = "GetMassCoachingTypes Error " + ex.Message.ToString();
                string ModalLabel = "Unable to complete loading process (SessionTypes). Please reload the page to try again.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);     
            }
        }
        protected void GetDropDownTopics(int SessionID)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID,CIMNumber);
                //remove One-on-One Compliance if mass review or remote coaching
                ds.Tables[0].AsEnumerable().Where(r => r.Field<Int32>("TopicId") == 49).ToList().ForEach(row => row.Delete());
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }
        }
        protected void GetAccounts(int CimNumber)
        {
            try
            {
                //DataTable dt = null;
                DataTable dt = new DataTable();
                DataAccess ws = new DataAccess();
                DataTable dtallsub = null;
                dtallsub = ws.CheckIfSubordinate(CimNumber);

                foreach (DataRow tempdtrow in dtallsub.Rows)
                {
                    DataTable tempdt = new DataTable();
                    DataAccess ws2 = new DataAccess();
                    tempdt = ws2.GetSubordinates2(int.Parse(tempdtrow["CIM_Number"].ToString()));
                    if (tempdt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count == 0)
                        {
                            dt = tempdt.Clone();
                        }
                        foreach (DataRow newrow in tempdt.Rows)
                        {
                            dt.ImportRow(newrow);
                        }
                    }
                }

                //dt = ws.GetSubordinates2(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                RadCampaign.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCampaign.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }

                var distinctRowsSites = (from DataRow dRowSites in dt.Rows
                                         select new { col1Site = dRowSites["CompanySiteID"], col2Site = dRowSites["companysite"] }).Distinct();

                RadSite.Items.Clear();

                foreach (var row in distinctRowsSites)
                {
                    RadSite.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2Site.ToString(), row.col1Site.ToString()));
                }

                var distinctRowsClient = (from DataRow dRowClient in dt.Rows
                                          select new { col1Client = dRowClient["clientID"], col2Client = dRowClient["client"] }).Distinct();

                RadAccount.Items.Clear();

                foreach (var row in distinctRowsClient)
                {
                    RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2Client.ToString(), row.col1Client.ToString()));
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetAccounts " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }

        }
        protected void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetDropDownSessions " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }
        }
        protected void PopulateTopics()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "PopulateTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }
        }
        protected void RadAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                if (RadCimNumber.Text.Trim().Equals(HttpContext.Current.User.Identity.Name.Split('|')[3]))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('Not Valid','Own cim number is not allowed'); });", true);
                    return;
                }

                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);


                if (RadCimNumber.Text != "")
                {
                    if (IsExisting(Convert.ToInt32(RadCimNumber.Text), DataSelectedEmployees))
                    {

                        if (IsSubordinate(CIMNumber, RadCimNumber.Text))
                        {
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo(Convert.ToInt32(RadCimNumber.Text));

                            string CIM = ds.Tables[0].Rows[0]["Cim Number"].ToString();
                            string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                            string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();
                            string Name = FirstName + " " + LastName;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            dt = (DataTable)ViewState["DataSelectedEmployees"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["CIM"] = CIM;
                            dr["Name"] = Name;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["DataSelectedEmployees"] = dt;
                            }
                            BindListView();
                            RadCimNumber.Text = "";

                        }
                        else
                        {

                            //string scriptstring = "alert('Please check the CIM Number.');";
                            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + scriptstring + "');", true);
                            string ModalLabel = "Please check the CIM Number.";
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       

                        }
                    }
                    else
                    {
                        //string scriptstring = "alert('CIM Number already exists.');";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + scriptstring + "');", true);
                        string ModalLabel = "CIM Number already exists.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
                    }
                }
                else
                {
                    //string scriptstring = "alert('Please check the CIM Number.');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + scriptstring + "');", true);
                    string ModalLabel = "Please check the CIM Number.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "Please check the CIM Number.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);       
            }
        }
        public bool IsSubordinate(int CimNumber, string Subordinate)
        {

            DataTable ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.CheckIfSubordinate(CimNumber);

            if (ds.Rows.Count > 0)
            {
                DataRow[] foundAuthors = ds.Select("CIM_Number = '" + Subordinate + "'");
                if (foundAuthors.Length != 0)
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public void BindListView()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["DataSelectedEmployees"];
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridEmployees.DataSource = dt;
                    RadGridEmployees.DataBind();
                }
                else
                {
                    RadGridEmployees.DataSource = null;
                    RadGridEmployees.DataBind();
                }
            }
            catch (Exception ex)
            {
               // string myStringVariable = ex.ToString();
               //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "BindListView " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public bool IsExisting(int CIM, DataTable dt)
        {

            DataRow[] foundCIM = dt.Select("CIM = '" + CIM + "'");
            if (foundCIM.Length != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            try
            {
                RadGridEmployees.DataSource = DataSelectedEmployees;
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "RadGrid1_NeedDataSource " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                GridDataItem deleteitem = (GridDataItem)e.Item;
                Label LblID = (Label)deleteitem.FindControl("LblCIM");

                string ID = LblID.Text;
                DataTable dt = (DataTable)ViewState["DataSelectedEmployees"];

                for (int i = dt.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dt.Rows[i];
                    if (dr["CIM"].ToString() == ID)
                    {
                        dr.Delete();
                    }
                }

                this.BindListView();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "RadGrid1_DeleteCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        protected void RadSend_Click(object sender, EventArgs e)
        {
            SaveForm(false);
        }

        protected void RadRequiredBtn_Click(object sender, EventArgs e)
        {
            SaveForm(true);
        }



        private void SaveForm(bool isRequired)
        {
            RadSend.Enabled = false;
            try
            {
                if (RadMassType.SelectedItem.Text == "Mass Review All")
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    int ReviewID = ws.InsertMassCoachingAll(1, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, isRequired);
                    RadReviewID.Text = ReviewID.ToString();

                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "alert('Successfully saved review.')", true);                  
                    UploadDocumentation(0, ReviewID);
                    DataHelper.SetTicketStatus(6, Convert.ToInt32(ReviewID)); // update status to coachersigned off 
                    LoadDocumentations();
                    DisableItems();
                    string ModalLabel = "Successfully saved review.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    if (isRequired == true)
                    {
                        LblSignOffMsg.Text = "Coach signed off " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }

                    if (isRequired == false)
                    {
                        LblSignOffMsg.Text = "Coach saved at " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }
                
                }
                else if (RadMassType.SelectedItem.Text == "Mass Review Groups")
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();

                    int Site;
                    if (RadSite.SelectedValue == "")
                    {
                        Site = 0;
                    }
                    else
                    {
                        Site = Convert.ToInt32(RadSite.SelectedValue);
                    }

                    int Account;
                    if (RadAccount.SelectedValue == "")
                    {
                        Account = 0;
                    }
                    else
                    {
                        Account = Convert.ToInt32(RadAccount.SelectedValue);
                    }

                    int Campaign;
                    if (RadCampaign.SelectedValue == "")
                    {
                        Campaign = 0;
                    }
                    else
                    {
                        Campaign = Convert.ToInt32(RadCampaign.SelectedValue);
                    }

                    int ReviewID = ws.InsertMassCoachingGroups(2, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, Site, Account, Campaign, isRequired);
                    RadReviewID.Text = ReviewID.ToString();
                    UploadDocumentation(1, ReviewID);
                    DataHelper.SetTicketStatus(6, Convert.ToInt32(ReviewID)); // update status to coachersigned off 
                    LoadDocumentations();
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "alert('Successfully saved review.')", true);
                    DisableItems();
                    string ModalLabel = "Successfully saved review.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    if (isRequired == true)
                    {
                        LblSignOffMsg.Text = "Coach signed off " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }

                    if (isRequired == false)
                    {
                        LblSignOffMsg.Text = "Coach saved at " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }
                }
                else if (RadMassType.SelectedItem.Text == "Mass Review My Cluster")//mass cluster
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    int ReviewID = ws.InsertMassCoachingAll(4, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, isRequired);
                    RadReviewID.Text = ReviewID.ToString();


                    UploadDocumentation(0, ReviewID);
                    DataHelper.SetTicketStatus(6, Convert.ToInt32(ReviewID)); // update status to coachersigned off 
                    LoadDocumentations();
                    DisableItems();
                    string ModalLabel = "Successfully saved review.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                    if (isRequired == true)
                    {
                        LblSignOffMsg.Text = "Coach signed off " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }

                    if (isRequired == false)
                    {
                        LblSignOffMsg.Text = "Coach saved at " + DateTime.Now.ToString();
                        LblSignOffMsg.Visible = true;
                    }
                }
                else
                {

                    if (RadGridEmployees.Items.Count > 0)
                    {
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        int MassCoachingID = ws.InsertMassCoachingSelected(3, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, 0, isRequired);
                        RadReviewID.Text = MassCoachingID.ToString();
                        UploadDocumentation(3, MassCoachingID);

                        foreach (GridDataItem itm in RadGridEmployees.Items)
                        {

                            Label CIM = (Label)itm.FindControl("LblCIM"); ;

                            DataAccess wsSelected = new DataAccess();
                            wsSelected.InsertMassCoachingSelectedPerCoachee(3, RadDescription.Text, RadAgenda.Text, Convert.ToInt32(RadSessionTopic.SelectedValue), CIMNumber, Convert.ToInt32(CIM.Text), MassCoachingID, isRequired);

                        }

                        DisableItems();
                        LoadDocumentations();
                        //string scriptstring = "alert('Successfully saved review.');";
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true);
                        DataHelper.SetTicketStatus(6, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                        string ModalLabel = "Successfully saved review.";
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        if (isRequired == true)
                        {
                            LblSignOffMsg.Text = "Coach signed off " + DateTime.Now.ToString();
                            LblSignOffMsg.Visible = true;
                        }

                        if (isRequired == false)
                        {
                            LblSignOffMsg.Text = "Coach saved at " + DateTime.Now.ToString();
                            LblSignOffMsg.Visible = true;
                        }
                    }
                    else
                    {
                        //string scriptstring = "alert('Please input CIM Numbers of Employees.');";
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", scriptstring, true);
                        string ModalLabel = "Please input CIM Numbers of Employees.";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        RadSend.Enabled = true;
                        RadRequireSignOff.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                RadSend.Enabled = false;
                string ModalLabel = "RadSend_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }
        }
        public void DisableItems()
        {
            RadMassType.Enabled = false;
            RadSessionTopic.Enabled = false;
            RadSessionType.Enabled = false;
            RadAgenda.Enabled = false;
            RadDescription.Enabled = false;
            RadAsyncUpload1.Visible = false;
            if (PanelGroups.Visible == true)
            {
                RadSite.Enabled = false;
                RadAccount.Enabled = false;
                RadCampaign.Enabled = false;        
            }

            if (PanelSelected.Visible == true)
            {
                RadCimNumber.Enabled = false;
                RadAdd.Visible = false;
                RadGridEmployees.Enabled = false;
            }
            RadSend.Visible = false;
            RadRequireSignOff.Visible = false;
        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds=null;
                DataAccess ws = new DataAccess();
                ds=ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text),1);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }
            
        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
               
            }

        }

        //export to PDF 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                try
                {
                    DataSet ds_get_review_forPDF = DataHelper.GetReviewIDforMassReview(Convert.ToInt32(RadReviewID.Text));
                    DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                    DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                    string ImgDefault;
                    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
                    DataSet ds3 = DataHelper.GetUserInfo(Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Email"]));
                    //FakeURLID.Value = URL;

                    //if (!Directory.Exists(directoryPath))
                    //{
                    //    ImgDefault = URL + "/Content/images/no-photo.jpg";
                    //}
                    //else
                    //{

                    //    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                    //}

                    //Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                    //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    //pdfDoc.Open();
                    //pdfDoc.NewPage();

                    Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                    BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.NewPage();

                    string coachingnum = "Coaching Ticket : " + Convert.ToString(RadReviewID.Text);
                    string specifics = "Coaching Date : " + Convert.ToString(RadCoachingDate.Text);
                    string line1 = "Coaching Specifics" + Environment.NewLine + coachingnum + "                                   " + specifics;
                    pdfDoc.AddTitle(coachingnum);
                    // pdfDoc.Add(new Paragraph(coachingnum )); //Convert.ToString()
                    Paragraph p1 = new Paragraph();
                    p1.Add(line1);
                    p1.SpacingBefore = 10;
                    p1.SpacingAfter = 10;

                    p1.Alignment = Element.ALIGN_JUSTIFIED; //Element.ALIGN_LEFT;
                    p1.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);

                    DataSet ds_cimnumbers = DataHelper.GetCimnumbersforMassReview(Convert.ToInt32(RadReviewID.Text));

                    PdfPTable cimtable = new PdfPTable(1);
                    string cimheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cimcell = new PdfPCell(new Phrase(cimheader, font));
                    cimcell.Colspan = 1;
                    cimcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    cimtable.AddCell(cimcell);
                    cimtable.SpacingBefore = 10;
                    cimtable.SpacingAfter = 10;
                    if (ds_cimnumbers.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_cimnumbers.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                //                                cimtable.AddCell("Coachee Cim Number");
                                PdfPCell cimcellh1 = new PdfPCell(new Phrase("Coachee Cim Number", font));
                                cimcellh1.Border = 0;
                                cimtable.AddCell(cimcellh1);
                            }
                            //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                            //alldocs = documents + Environment.NewLine + alldocs;
                            PdfPCell cimcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_cimnumbers.Tables[0].Rows[ctr]["Coacheeid"]), font));
                            cimcelli1.Border = 0;
                            cimtable.AddCell(cimcelli1);

                        }
                    }
                    else
                    {
                        //cimtable.AddCell("Coachee Cim Number");

                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        //cimtable.AddCell(Convert.ToString(ds_cimnumbers.Tables[0].Rows[0]["Coacheeid"]));

                        PdfPCell cimcellh1 = new PdfPCell(new Phrase("Coachee Cim Number", font));
                        cimcellh1.Border = 0;
                        cimtable.AddCell(cimcellh1);
                        PdfPCell cimcelli1 = new PdfPCell(new Phrase("No Cim Number", font));
                        cimcelli1.Border = 0;
                        cimtable.AddCell(cimcelli1);
                    }

                    //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
                    //gif.ScaleToFit(125f, 125f);
                    //gif.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                    //gif.IndentationLeft = 15f;
                    //gif.IndentationRight = 15f;
                    //gif.SpacingAfter = 15f;
                    //gif.SpacingBefore = 15f;



                    Paragraph p4 = new Paragraph();
                    string line4 = ""; //"Account : " + Convert.ToString(RadAccount.Text);
                    p4.Alignment = Element.ALIGN_LEFT;
                    p4.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    //p4.Add(line4);

                    Paragraph p2 = new Paragraph();
                    string line2 = "Review Type : " + Convert.ToString(RadMassType.Text); //"Name : " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]) + "         
                    p2.Alignment = Element.ALIGN_LEFT;
                    p2.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p2.Add(line2);

                    Paragraph p7 = new Paragraph();
                    string line7 = Environment.NewLine + "Session Type : " + Convert.ToString(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])); //RadSessionType.Text);
                    string line7a = Environment.NewLine + "Topic :" + Convert.ToString(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])); //RadSessionTopic.Text);
                    p7.Alignment = Element.ALIGN_LEFT;
                    p7.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p7.Add(line7a);
                    p7.Add(line7);

                    Paragraph p6 = new Paragraph();
                    string line6 = "";//"Cim Number : " + Convert.ToString(RadCimNumber.Text);
                    p6.Alignment = Element.ALIGN_LEFT;
                    p6.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    //p6.Add(line6);


                    Paragraph p3 = new Paragraph();
                    string line3 = "";//"Site : " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["sitename"]);
                    p3.Alignment = Element.ALIGN_LEFT;
                    p3.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p3.Add(line3);


                    Paragraph p5 = new Paragraph();
                    string line5 = "";//"Campaign : " + Convert.ToString(ds3.Tables[0].Rows[0]["Campaign"]);
                    p5.Alignment = Element.ALIGN_LEFT;
                    p5.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p5.Add(line5);


                    Paragraph p8 = new Paragraph();
                    string line8 = "Description : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]); //RadDescription.Text);
                    p8.Alignment = Element.ALIGN_LEFT;
                    p8.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p8.Add(line8);
                    p8.SpacingBefore = 10;
                    p8.SpacingAfter = 10;


                    Paragraph p9 = new Paragraph();
                    string line9 = "Agenda: " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Agenda"]);//RadAgenda.Text);
                    p9.Alignment = Element.ALIGN_LEFT;
                    p9.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p9.Add(line9);
                    p9.SpacingBefore = 10;
                    p9.SpacingAfter = 10;

                    Paragraph p13 = new Paragraph();
                    string line13 = "Documentation " + Environment.NewLine;
                    p13.Alignment = Element.ALIGN_LEFT;
                    p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                    p13.Add(line13);
                    p13.SpacingBefore = 10;
                    p13.SpacingAfter = 10;


                    PdfPTable table = new PdfPTable(4);
                    string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cell = new PdfPCell(new Phrase(header, font));
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right 
                    table.AddCell(cell);
                    table.SpacingBefore = 30;
                    table.SpacingAfter = 30;
                    table.WidthPercentage = 100;

                    table.HorizontalAlignment = 0;
                    table.TotalWidth = 600f;
                    table.LockedWidth = true;
                    float[] widths = new float[] { 150f, 150f, 150f, 150f };
                    table.SetWidths(widths);

                    DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                    if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                                cellh1.Border = 0;
                                table.AddCell(cellh1);
                                PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                                cellh2.Border = 0;
                                table.AddCell(cellh2);
                                PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                                cellh3.Border = 0;
                                table.AddCell(cellh3);
                                PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                                cellh4.Border = 0;
                                table.AddCell(cellh4);


                            }
                            PdfPCell celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                            celli1.Border = 0;
                            table.AddCell(celli1);


                            PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                            celli2.Border = 0;
                            table.AddCell(celli2);

                            PdfPCell celli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                            celli3.Border = 0;
                            table.AddCell(celli3);

                            PdfPCell celli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                            celli4.Border = 0;
                            table.AddCell(celli4);
                        }
                    }
                    else
                    {
                        PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number", font));
                        cellh1.Border = 0;
                        table.AddCell(cellh1);
                        PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name", font));
                        cellh2.Border = 0;
                        table.AddCell(cellh2);
                        PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                        cellh3.Border = 0;
                        table.AddCell(cellh3);
                        PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                        cellh4.Border = 0;
                        table.AddCell(cellh4);

                        PdfPCell celli1 = new PdfPCell(new Phrase("No documents uploaded.", font));
                        celli1.Border = 0;
                        table.AddCell(celli1);
                        PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(""), font));
                        celli2.Border = 0;
                        table.AddCell(celli2);
                        table.AddCell(celli2);
                        table.AddCell(celli2);
                    }




                    pdfDoc.Add(new Paragraph(p1)); //specifics
                    pdfDoc.Add(new Paragraph(p2)); // reviewtype
                    pdfDoc.Add(cimtable);
                    pdfDoc.Add(new Paragraph(p4)); //account
                    pdfDoc.Add(new Paragraph(p7)); //topic
                    pdfDoc.Add(new Paragraph(p3)); //site
                    pdfDoc.Add(new Paragraph(p5)); //campaign
                    pdfDoc.Add(new Paragraph(p8)); //documentation
                    pdfDoc.Add(new Paragraph(p9)); //agenda
                    pdfDoc.Add(new Paragraph(p13)); //documentation header
                    pdfDoc.Add(table); //documentation table


                    pdfDoc.Close();


                    Response.ContentType = "application/pdf";
                    string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_Coach_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                    string filenameheader = filename + ".pdf";
                    string filenameheadername = "filename=" + filenameheader;
                    Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();




                }
                catch (Exception ex)
                {
                    //Response.Write(ex.Message.ToString());
                    string ModalLabel = "btn_ExporttoPDF_Click " + ex.Message.ToString();
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            else
            {
                //string script = "alert('Please submit the review before creating a pdf.')";
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "its working", script, true);
                string ModalLabel = "Please submit the review before creating a pdf.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //GridDataItem deleteitem = (GridDataItem)e.Item;
                //Label LblID = (Label)deleteitem.FindControl("LblCIM");

                //string ID = LblID.Text;
                //DataTable dt = (DataTable)ViewState["DataSelectedEmployees"];

                //for (int i = dt.Rows.Count - 1; i >= 0; i--)
                //{
                //    DataRow dr = dt.Rows[i];
                //    if (dr["CIM"].ToString() == ID)
                //    {
                //        dr.Delete();
                //    }
                //}

                this.BindListView();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "RadGrid1_DeleteCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }



    }
}