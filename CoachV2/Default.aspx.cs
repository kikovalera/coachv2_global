﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using CoachV2.AppCode;
using System.Data;
using Microsoft.AspNet.Membership.OpenAuth;
using System.Net;
using System.Globalization;
using System.Web.Security;

namespace CoachV2
{
    public partial class Default : System.Web.UI.Page
    {
        public string ReturnUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            //if (Session["globalexception"] != null)
            //{
            //    if (Request.QueryString["errlogid"] != null)
            //    {
            //        lblerrlogid.InnerHtml = "We apologize for the interruption. The page you were trying to access encountered an error.<br />A log was already created to help our Dev Team fix the issue as soon as possible: " + Request.QueryString["errlogid"].ToString().Trim() + ".";
            //    }
            //    errordiv.Visible = true;
            //    Session["globalexception"] = null;
            //}
            //else
            //{
                string fakeEmail;
                ReturnUrl = Request.QueryString["ReturnUrl"];

                if (HttpContext.Current.User.Identity.Name.Split('|')[0] != "")
                {

                    if (Session["bot"] != null)
                    {
                        fakeEmail = Session["bot"].ToString();
                    }
                    else
                    {
                        fakeEmail = HttpContext.Current.User.Identity.Name.Split('|')[0].ToString();
                    }

                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(fakeEmail);

                    if (dsSAPInfo.Tables[0].Rows.Count > 0)
                    {

                        DataSet dsUserInfo = DataHelper.GetUserInfo(fakeEmail);
                        Session["Role"] = dsSAPInfo.Tables[0].Rows[0]["Role"].ToString();
                        //add role for user on first log in
                        string emp_email = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                        DataSet ds_getrolefromsap = DataHelper.GetEmployeeRoleFromSapDetails(emp_email);

                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int int_cim = Convert.ToInt32(cim_num);

                        if (ds_getrolefromsap.Tables[0].Rows.Count > 0)
                        {
                            DataSet ds_getuserroleid = DataHelper.GetEmployeeRoleID(int_cim);
                            DataSet ds_countrolesassigned = DataHelper.GetAllUserRoles(int_cim);
                            int userroleid = Convert.ToInt32(ds_getuserroleid.Tables[0].Rows[0]["roleid"]);
                            int userrolegroup = Convert.ToInt32(ds_getuserroleid.Tables[0].Rows[0]["rolegroupid"]);


                            if (ds_countrolesassigned.Tables[0].Rows.Count == 0)
                            {
                                get_hierarchy();

                            }

                        }//add role for user on first log in

                        //checking if has subordinates to auto allow user to have my team and my reviews
                        DataSet ds_subordinates = DataHelper.Get_Subordinates(int_cim); //check if user has subordinates
                        if (ds_subordinates.Tables[0].Rows.Count > 0)
                        {
                            DataSet ds_countrolesassigned = DataHelper.GetAllUserRoles(int_cim);
                            if (ds_countrolesassigned.Tables[0].Rows.Count > 0)
                            {
                                for (int ctr = 0; ctr < ds_countrolesassigned.Tables[0].Rows.Count; ctr++)
                                {
                                    DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 2);
                                    if (ds_userselect2.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                                    }


                                    DataSet ds_userselect3 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 3);
                                    if (ds_userselect3.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Add Review 
                                    }
                                    DataSet ds_userselect4 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 4);
                                    if (ds_userselect4.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Add Review 
                                    }
                                    DataSet ds_userselect5 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 5);
                                    if (ds_userselect5.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                                    }
                                    DataSet ds_userselect6 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 6);
                                    if (ds_userselect6.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                                    }
                                    DataSet ds_userselect7 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 7);
                                    if (ds_userselect7.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                                    }
                                    DataSet ds_userselect8 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 8);
                                    if (ds_userselect8.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                                    }

                                    DataSet ds_userselect9 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 9);
                                    if (ds_userselect9.Tables[0].Rows.Count == 0)
                                    {
                                        DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                                    }

                                    //DataSet ds_userselect10 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 10);
                                    //if (ds_userselect10.Tables[0].Rows.Count == 0)
                                    //{
                                    //    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);   //My Reports - Report Builder
                                    //}
                                    //DataSet ds_userselect11 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 11);
                                    //if (ds_userselect11.Tables[0].Rows.Count == 0)
                                    //{
                                    //    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);   //My Reports - Built In Reports
                                    //}

                                    //DataSet ds_userselect12 = DataHelper.GetUserRolesAssigned(Convert.ToInt32(int_cim), 12);
                                    //if (ds_userselect12.Tables[0].Rows.Count == 0)
                                    //{
                                    //    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);   //My Reports - Saved Reports
                                    //}
                                }
                            }
                        }
                        else
                        {

                            DataHelper.RemoveUserRolesAssigned(int_cim, 2);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 3);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 4);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 5);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 6);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 7);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 8);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 9);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 10);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 11);
                            DataHelper.RemoveUserRolesAssigned(int_cim, 12);

                        }

                        DataSet ds_dept = DataHelper.GetSapDept(int_cim);

                        if (ds_dept.Tables[0].Rows.Count > 0)
                        {

                            if (ds_dept.Tables[0].Rows[0]["deptsap"].ToString() != ds_dept.Tables[0].Rows[0]["deptuserinfo"].ToString())
                            {
                                // DataHelper.UpdateDeptUserinfo(int_cim, ds_dept.Tables[0].Rows[0]["deptsap"].ToString(), ds_dept.Tables[0].Rows[0]["saprole"].ToString());
                                DataHelper.UpdateDeptUserinfodetails(int_cim, ds_dept.Tables[0].Rows[0]["deptsap"].ToString(), ds_dept.Tables[0].Rows[0]["saprole"].ToString(), ds_dept.Tables[0].Rows[0]["sapreports_to"].ToString());
                            }
                        }

                        if (dsUserInfo.Tables[0].Rows.Count > 0)
                        {
                            using (WebClient wc = new WebClient())
                            {
                                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString()));

                                if (!Directory.Exists(directoryPath))
                                {
                                    if (Session["bot"] == null)
                                    {
                                        Directory.CreateDirectory(directoryPath);
                                        wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString() + ".jpg");

                                    }
                                }
                                else
                                {
                                    System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);
                                    try
                                    {
                                        foreach (FileInfo file in di.GetFiles())
                                        {
                                            if (Session["bot"] == null)
                                            {
                                                file.Delete();
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {

                                        //do nothing
                                    }

                                    if (Session["bot"] == null)
                                    {
                                        wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString() + ".jpg");

                                    }
                                }

                                // wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2],  (!System.IO.Directory.Exists(@"Content\\uploads\\"+CIMNo)) ? System.IO.Directory.CreateDirectory(@"Content\\uploads\\"+CIMNo : "");


                            }

                            if (Convert.ToBoolean(dsUserInfo.Tables[0].Rows[0]["Inactive"]) == true)
                            {
                                FormsAuthentication.SignOut();
                                Session.Abandon();

                                Response.Redirect("~/Default.aspx");
                            }

                        }
                        else
                        {
                            string CIMNo = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                            string FirstName = dsSAPInfo.Tables[0].Rows[0]["First_Name"].ToString();
                            string LastName = dsSAPInfo.Tables[0].Rows[0]["Last_Name"].ToString();
                            string ReportingTo = dsSAPInfo.Tables[0].Rows[0]["ReportsTo"].ToString();
                            int Gender = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["Gender"]);

                            string MobileNo = dsSAPInfo.Tables[0].Rows[0]["Mobile_Number"].ToString();
                            string LandLineNo = dsSAPInfo.Tables[0].Rows[0]["Phone_Number"].ToString();
                            string TranscomEmail = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                            string Role = dsSAPInfo.Tables[0].Rows[0]["Role"].ToString();
                            string Site2 = dsSAPInfo.Tables[0].Rows[0]["Site"].ToString();
                            string Region = "";
                            string Country = dsSAPInfo.Tables[0].Rows[0]["Country"].ToString();
                            string Department = dsSAPInfo.Tables[0].Rows[0]["Department"].ToString();

                            using (WebClient wc = new WebClient())
                            {
                                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + CIMNo));

                                if (!Directory.Exists(directoryPath))
                                {
                                    if (Session["bot"] == null)
                                    {
                                        Directory.CreateDirectory(directoryPath);
                                        wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + CIMNo + ".jpg");

                                    }
                                }
                                else
                                {
                                    if (Session["bot"] == null)
                                    {
                                        wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + CIMNo + ".jpg");

                                    }
                                }

                                // wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2],  (!System.IO.Directory.Exists(@"Content\\uploads\\"+CIMNo)) ? System.IO.Directory.CreateDirectory(@"Content\\uploads\\"+CIMNo : "");


                            }





                            DateTime DateOfBirth = DateTime.ParseExact(dsSAPInfo.Tables[0].Rows[0]["Birthday"].ToString(),
                                                "yyyyMMdd",
                                                CultureInfo.InvariantCulture,
                                                DateTimeStyles.None);

                            DateTime DateStartTranscom = DateTime.ParseExact(dsSAPInfo.Tables[0].Rows[0]["Start_Date"].ToString(),
                                                "yyyyMMdd",
                                                CultureInfo.InvariantCulture,
                                                DateTimeStyles.None);

                            //string UserId = System.Web.HttpContext.Current.User.Identity.;

                            MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                            string userId = string.Empty;
                            try
                            {
                                userId = mu.ProviderUserKey.ToString();
                            }
                            catch (NullReferenceException)
                            {

                                Response.Write("<script>alert('You are not registered user');</script>");
                                return;
                            }

                            DataHelper.InsertUserInfo(userId,
                                CIMNo,
                                FirstName,
                                LastName,
                                "",
                                ReportingTo,
                                Gender,
                                DateOfBirth,
                                DateStartTranscom,
                                "",
                                MobileNo,
                                LandLineNo,
                                TranscomEmail,
                                "",
                                Role,
                                Site2,
                                Region,
                                Country,
                                Department,
                                "",
                                "",
                                (CIMNo + ".jpg"),
                                DateTime.Now,
                                DateTime.Now, false);



                        }
                    } // if  if (dsSAPInfo.Tables[0].Rows.Count > 0)
                }
                else
                {
                    FormsAuthentication.SignOut();
                }
            //}
            
        }

        public IEnumerable<ProviderDetails> GetProviderNames()
        {
            return OpenAuth.AuthenticationClients.GetAll();
        }


        protected void BtnLoginGoogle_Click(object sender, EventArgs e)
        {
          
            string redirectUrl = "~/ExternalLandingPage.aspx";
       

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                var resolvedReturnUrl = ResolveUrl(ReturnUrl);
                redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
            }

            OpenAuth.RequestAuthentication("google", redirectUrl);
        }



        protected void get_hierarchy()
        {
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            int   deptid, roleid ;

            DataSet ds_userdept = DataHelper.get_adminusername(emp_email);
            //if (ds_userdept.Tables[0].Rows.Count > 0){
            deptid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["accountid"].ToString());
            roleid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["roleid"].ToString());
            //}

            string cim_num = ds.Tables[0].Rows[0][0].ToString();
            int int_cim = Convert.ToInt32(cim_num);
            DataSet ds_hassubs = DataHelper.CheckforSubs(Convert.ToInt32(int_cim));
            DataSet ds1 = null;
            DataAccess ws1 = new DataAccess();
            int userhierarchy;
            userhierarchy = ws1.getroleofloggedin_user(int_cim);

            DataAccess ws2 = new DataAccess();
            string department = "";
            department = ws2.getuserdept(int_cim);

            DataSet ds_qarole = DataHelper.checkroleifQA(roleid);
            if (department == "HR")
            {
                if (userhierarchy == 1)
                {
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now); //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 19, cim_num, DateTime.Now);   //My  Review  for agent
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy);

                }
                else if (userhierarchy == 2)
                {
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

                }
                else if ((userhierarchy == 3) || (userhierarchy == 4))
                {
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

                
                }
            }
            else if (department == "QA")
            {
                if (userhierarchy == 1)
                {
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now); //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 19, cim_num, DateTime.Now);   //My  Review  for agent
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy);
                    DataHelper.InsertUserRole(int_cim, 18, cim_num, DateTime.Now);   //My Reviews - Add QA
                }
                else if (userhierarchy == 2)
                {  
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy);
                    DataHelper.InsertUserRole(int_cim, 18, cim_num, DateTime.Now);   //My Reviews - Add QA
                }
                else if ((userhierarchy == 3) || (userhierarchy == 4))
                {
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy);
                    DataHelper.InsertUserRole(int_cim, 18, cim_num, DateTime.Now);   //My Reviews - Add QA

                }
            }
            else
            {   if (ds_qarole.Tables[0].Rows.Count > 0){
                if (roleid == Convert.ToInt32(ds_qarole.Tables[0].Rows[0]["roleid"].ToString())) 

                    DataHelper.InsertUserRole(int_cim, 18, cim_num, DateTime.Now);   //My Reviews - Add QA
                }
                if (userhierarchy == 1)
                {//agent
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now); //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 19, cim_num, DateTime.Now);   //My  Review  for agent
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

               
                }
                else if (userhierarchy == 2)
                { //tl
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

                }
                else if (userhierarchy == 3)
                {
                    //om
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

                }
                else
                {   //dir
                    DataHelper.InsertUserRole(int_cim, 1, cim_num, DateTime.Now);   //General - Log In   
                    DataHelper.InsertUserRole(int_cim, 2, cim_num, DateTime.Now);   //My Team - Add Review 
                    DataHelper.InsertUserRole(int_cim, 3, cim_num, DateTime.Now);   //My Team - Performance  Summary
                    DataHelper.InsertUserRole(int_cim, 4, cim_num, DateTime.Now);   //My Team - Review Logs 
                    DataHelper.InsertUserRole(int_cim, 5, cim_num, DateTime.Now);   //My Team - Profile
                    DataHelper.InsertUserRole(int_cim, 6, cim_num, DateTime.Now);   //My Reviews - Add New Review  
                    DataHelper.InsertUserRole(int_cim, 7, cim_num, DateTime.Now);   //My Reviews - Add Mass Reviews
                    DataHelper.InsertUserRole(int_cim, 8, cim_num, DateTime.Now);   //My Reviews - Remote Coach  
                    DataHelper.InsertUserRole(int_cim, 9, cim_num, DateTime.Now);   //My Reviews - Search Reviews 
                    DataHelper.InsertUserRole(int_cim, 10, cim_num, DateTime.Now);  //My Reports	- Report Builder
                    DataHelper.InsertUserRole(int_cim, 11, cim_num, DateTime.Now);  //My Reports	- Built In Reports
                    DataHelper.InsertUserRole(int_cim, 12, cim_num, DateTime.Now);  //My Reports	- Saved Reports
                    DataHelper.InsertUserRole(int_cim, 16, cim_num, DateTime.Now); //edit profile
                    DataHelper.Insertuserhierarchy(int_cim, deptid, roleid, userhierarchy); 

                }
            }
        }
    }
}