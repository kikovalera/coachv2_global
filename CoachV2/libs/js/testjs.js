﻿(function () {

    var demo = window.demo = window.demo || {};
    var $ = $telerik.$;

    demo.load = function () {
        jqItemsCountNtbWrapper = $("li.SpecificCoordinates");

        var jqActionListsTypesButtons = $(".actionsList input[type='radio']");

        jqActionListsTypesButtons.on("click", itemsTypeChange);

        $(jqActionListsTypesButtons[0]).click();

        var selectedRadio = jqActionListsTypesButtons.filter(function () {

            return this.checked;

        }).get(0);

        toggleItemsCountNtbVisibility(selectedRadio);
    }


    function itemsTypeChange(e) {
        var imageElement = $(".imageElement");

        toggleItemsCountNtbVisibility(e.target);

        if (e.target.value == "SpecificCoordinates") {
            imageElement.off();
        }
        else {
            imageElement.off()
                         .on(e.target.value, showMenu);
        }
    }



    function toggleItemsCountNtbVisibility(radioButton) {
        var toShow = radioButton.value == "SpecificCoordinates";
        changeVisibility(jqItemsCountNtbWrapper, toShow);
    }



    function changeVisibility(jqElement, toShow) {
        jqElement[toShow ? "show" : "hide"]();
    }

    function showMenuAt(e) {
        var x = demo.editOffsetX.get_value();
        var y = demo.editOffsetY.get_value();
        if (isNaN(x) || isNaN(y)) {
            alert("Please provide valid integer coordinates");
            return;
        }

        $telerik.cancelRawEvent(e);

        demo.contextMenu.showAt(x, y);
    }

    function showMenu(e) {

        $telerik.cancelRawEvent(e);

        if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(demo.contextMenu.get_element(), e.relatedTarget))) {
            demo.contextMenu.show(e);
        }
    }

    window.OnClientItemClicked = function (sender, args) {
        var imageJQueryElement = $telerik.$(".imageElement");
        imageJQueryElement.attr("src", args.get_item().get_value());
        imageJQueryElement.attr("alt", args.get_item().get_text());
    };

    window.OnClientShown = function (sender, args) {
        var imageJQueryElement = $telerik.$(".imageElement");
        var selectedItemText = imageJQueryElement.attr("alt");

        $telerik.$(sender.get_allItems()).each(function () {
            $telerik.$(this.get_element()).attr("class", "rmItem")
        })

        $(sender.findItemByText(selectedItemText).get_element()).attr("class", "rmItem rmSelected");
    }

    window.OnClientClicked = function (sender, args) {
        var domEvent = args.get_domEvent();
        showMenuAt(domEvent);
    }

} ());