﻿; (function () {
    var demo = window.demo = {};
    var toolTip;

    //    Sys.Application.add_load(function () {
    //        toolTip = $telerik.findControl(document, "RadToolTip1");
    //    });

    demo.RowMouseOver = function (sender, args) {
        var item = args.get_gridDataItem();

        //        if (item.get_itemIndex() == 4 || item.get_itemIndex() == 5) {
        //            toolTip.set_targetControl(item.get_element());
        //            setTimeout(function () {
        //                toolTip.show();
        //            }, 11);
        //        }
        //        else {
        //            toolTip.hide();
        //        }
    };

})();
function Validate_Checkbox() {
    var chks = $("#<%= CBList.ClientID %> input:checkbox");

    var hasChecked = false;
    for (var i = 0; i < chks.length; i++) {
        if (chks[i].checked) {
            hasChecked = true;
            break;
        }
    }
    if (hasChecked == false) {
        openModal("Error Message", "Please select at least one checkbox..!")

        return false;
    }

    return true;
}

function OnClientValidationFailed(sender, args) {
    var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
    if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
        if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
            //alert("Only PDF files can be uploaded!");
            openModal("Error Message", "Only PDF,Excel and PNG files can be uploaded!")
        }
        else {
            //alert("Only less than 1 MB file size can be uploaded!");
            openModal("Error Message", "Only less than 1 MB file size can be uploaded!")
        }
    }
    else {
        //alert("not correct extension!");
        openModal("Error Message", "not correct extension!")
    }
    setTimeout(function () { deleteInvalid(sender) }, 250);
}
function deleteInvalid(sender) {
    var invalidIndexes = [];
    $telerik.$(".ruUploadSuccess, .ruUploadFailure", sender.get_element()).each(function (index) {
        if ($telerik.$(this).hasClass('ruUploadFailure')) {
            invalidIndexes.push(index);
        }
    });

    while (invalidIndexes.length) {
        sender.deleteFileInputAt(invalidIndexes.pop());
    }
}
function confirmDeletes(sender, eventArgs) {
    if (!confirm("Are you sure you want to delete the selected row?")) eventArgs.set_cancel(true);
}

function openModal(header, message) {
    $('#myModal').modal('show');
    $('.manager').text(header);
    $('.modal-body').text(message);
}

function OnClientValidationFailedAgentPerformance(sender, args) {
    var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
    if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
        if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
            //alert("Only PDF files can be uploaded!");
            openModal("Error Message", "Only .xlsx files can be uploaded!")
        }
        else if (sender.get_allowedFileExtensions() != "."+ fileExtention) {
            openModal("Error Message", "Only .xlsx files can be uploaded!")
        }
        else {
//        alert(sender.get_allowedFileExtensions())
//        alert("."+fileExtention)
            alert("Only less than 1 MB file size can be uploaded!");
            //            openModal("Error Message", "Only less than 1 MB file size can be uploaded!")
           // openModal("Error Message", fileExtention)
        }
    }
    else {
        //alert("not correct extension!");
        openModal("Error Message", "not correct extension!")
    }
    setTimeout(function () { deleteInvalid(sender) }, 250);
}

// 2 functions added to auto resize textarea for addreview, massreview, remotecoaching (francis.valera/09282018)
function setHeight(inputElement, eventArgs) {
    window.setTimeout(function () {
        var sender = $find(inputElement.id);
        sender._textBoxElement.style.height = "";
        window.setTimeout(function () {
            sender._textBoxElement.style.height = sender._textBoxElement.scrollHeight + 2 + "px";
            sender._originalTextBoxCssText += "height: " + sender._textBoxElement.style.height + ";";
        }, 0);
    }, 0);
}
function RadTextBoxLoad(sender) {
    sender._textBoxElement.style.height = "";
    sender._textBoxElement.style.height = sender._textBoxElement.scrollHeight + 2 + "px";
    sender._originalTextBoxCssText += "height: " + sender._textBoxElement.style.height + ";";
}
function ClientSideClick(myButton) {
    // Client side validation
    if (typeof (Page_ClientValidate) == 'function') {
        if (Page_ClientValidate() == false)
        { return false; }
    }

    //make sure the button is not of type "submit" but "button"
    if (myButton.getAttribute('type') == 'button') {
        // disable the button
        myButton.disabled = true;
        myButton.className = "btn-inactive";
        myButton.value = "processing...";
    }
    return true;
}