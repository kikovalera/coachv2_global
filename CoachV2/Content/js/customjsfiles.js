var date_today = new Date().toDateString();
$("#date_holder").text(date_today);

$("#overall_btn").click(function() {
  $("#overall").show();
  $("#voc").hide();
  $("#aht").hide();
  $("#s4").hide();
  $("#overall_btn").css("color", "white");
});

$("#voc_btn").click(function() {
  $("#overall").hide();
  $("#voc").show();
  $("#aht").hide();
  $("#s4").hide();
  $("#voc_btn").css("color", "white");
});

$("#aht_btn").click(function() {
  $("#overall").hide();
  $("#voc").hide();
  $("#aht").show();
  $("#s4").hide();
  $("#aht_btn").css("color", "white");
});

$("#s4_btn").click(function() {
  $("#overall").hide();
  $("#voc").hide();
  $("#aht").hide();
  $("#s4").show();
  $("#s4_btn").css("color", "white");
});

$("#overall1_btn").click(function() {
  $("#overall1").show();
  $("#voc1").hide();
  $("#aht1").hide();
  $("#s41").hide();
  $("#overall1_btn").css("color", "white");
});

$("#voc1_btn").click(function() {
  $("#overall1").hide();
  $("#voc1").show();
  $("#aht1").hide();
  $("#s41").hide();
  $("#voc1_btn").css("color", "white");
});

$("#aht1_btn").click(function() {
  $("#overall1").hide();
  $("#voc1").hide();
  $("#aht1").show();
  $("#s41").hide();
  $("#aht1_btn").css("color", "white");
});

$("#s41_btn").click(function() {
  $("#overall1").hide();
  $("#voc1").hide();
  $("#aht1").hide();
  $("#s41").show();
  $("#s41_btn").css("color", "white");
});


// Report Builder

$("#datasourceusers").click(function() {
  $("#datafieldsusers").show();
  $("#datafieldsreviews").hide();
  $("#datafieldskpi").hide();
  $("#datasourceusers").removeClass("btn btn-primary btn-small");
  $("#datasourceusers").addClass("btn btn-success btn-small");
  $("#datasourcereviews").removeClass("btn btn-primary btn-small");
  $("#datasourcereviews").removeClass("btn btn-success btn-small");
  $("#datasourcereviews").addClass("btn btn-primary btn-small");
  $("#datasourcekpi").removeClass("btn btn-primary btn-small");
  $("#datasourcekpi").removeClass("btn btn-success btn-small");
  $("#datasourcekpi").addClass("btn btn-primary btn-small");

});

$("#datasourcereviews").click(function() {
  $("#datafieldsusers").hide();
  $("#datafieldsreviews").show();
  $("#datafieldskpi").hide();
  $("#datasourcereviews").removeClass("btn btn-primary btn-small");
  $("#datasourcereviews").addClass("btn btn-success btn-small");
  $("#datasourceusers").removeClass("btn btn-primary btn-small");
  $("#datasourceusers").removeClass("btn btn-success btn-small");
  $("#datasourceusers").addClass("btn btn-primary btn-small");
  $("#datasourcekpi").removeClass("btn btn-primary btn-small");
  $("#datasourcekpi").removeClass("btn btn-success btn-small");
  $("#datasourcekpi").addClass("btn btn-primary btn-small");
});

$("#datasourcekpi").click(function() {
  $("#datafieldsusers").hide();
  $("#datafieldsreviews").hide();
  $("#datafieldskpi").show();
  $("#datasourcekpi").removeClass("btn btn-primary btn-small");
  $("#datasourcekpi").addClass("btn btn-success btn-small");
  $("#datasourcereviews").removeClass("btn btn-primary btn-small");
  $("#datasourcereviews").removeClass("btn btn-success btn-small");
  $("#datasourcereviews").addClass("btn btn-primary btn-small");
  $("#datasourceusers").removeClass("btn btn-primary btn-small");
  $("#datasourceusers").removeClass("btn btn-success btn-small");
  $("#datasourceusers").addClass("btn btn-primary btn-small");
});



$("#generate").click(function() {

  if ($("#datasourceusers").hasClass("btn btn-success btn-small") && $("#datasourcereviews").hasClass("btn btn-primary btn-small") && $("#datasourcekpi").hasClass("btn btn-primary btn-small")) {
    $(".displayuser").show();
    $(".displayreview").hide();
    $(".displaykpi").hide();
  }

  else if ($("#datasourceusers").hasClass("btn btn-primary btn-small") && $("#datasourcereviews").hasClass("btn btn-success btn-small") && $("#datasourcekpi").hasClass("btn btn-primary btn-small")) {
    $(".displayuser").hide();
    $(".displayreview").show();
    $(".displaykpi").hide();
  }

  else if ($("#datasourceusers").hasClass("btn btn-primary btn-small") && $("#datasourcereviews").hasClass("btn btn-primary btn-small") && $("#datasourcekpi").hasClass("btn btn-success btn-small")) {
    $(".displayuser").hide();
    $(".displayreview").hide();
    $(".displaykpi").show();
  }
  $("#generate").css("color", "white");
});


$(".datafields").each(function() {
  $(".datafields").click(function() {
    $(this).removeClass("datafields btn btn-primary btn-small");
    $(this).addClass("datafields btn btn-info btn-small");
  });
});


// My Settings Link To Label Change

$("#linkto").change(function() {

  if ($("#linkto").val() == "Account") {
    $('label[for="Account"]').text("Account");
  }
  else if ($("#linkto").val() == "LOB") {
    $('label[for="Account"]').text("LOB");
  }
  else if ($("#linkto").val() == "Role") {
    $('label[for="Account"]').text("Role");
  }
  else {

  }

});

$('[data-toggle="popover"]').popover({
  placement: 'left'
});

$('[data-toggle="tooltip"]').tooltip({
  placement: 'top'
});


// Back to Top
$('body').append('<div id="toTop" class="btn btn-custom"><span class="glyphicon glyphicon-triangle-top"></span><br>UP</div>');
$(window).scroll(function() {
  if ($(this).scrollTop() != 0) {
    $('#toTop').fadeIn();
  } else {
    $('#toTop').fadeOut();
  }
});
$('#toTop').click(function() {
  $("html, body").animate({scrollTop: 0}, 600);
  return false;
});

// Date Picker: Add Review
var date_hidden_value = new Date().toLocaleDateString('en-US');
$("#addreviewfollowupdate").datepicker();
$("#addreviewfollowupdate").val(date_hidden_value);

// Date Picker: QA Review
$("#addqareviewfollowupdate").datepicker();
$("#addqareviewfollowupdate").val(date_hidden_value);


// Date Picker: Search Review
$("#searchreviewreviewdate").datepicker();
$("#searchreviewreviewdate").val(date_hidden_value);
$("#searchreviewfollowupdate").datepicker();
$("#searchreviewfollowupdate").val(date_hidden_value);

// Date Picker: Top Behaviors Overall
$("#date1").datepicker();
$("#date1").val(date_hidden_value);
$("#date2").datepicker();
$("#date2").val(date_hidden_value);


// Date Picker: Behaviors  VS. KPI
$("#date3").datepicker();
$("#date3").val(date_hidden_value);
$("#date4").datepicker();
$("#date4").val(date_hidden_value);

// Coaching Frequency Vs. VOC Score
$("#date5").datepicker();
$("#date5").val(date_hidden_value);
$("#date6").datepicker();
$("#date6").val(date_hidden_value);

// Coaching Frequency Vs. FCR Score
$("#date7").datepicker();
$("#date7").val(date_hidden_value);
$("#date8").datepicker();
$("#date8").val(date_hidden_value);

// FCR Glide Path Targets Vs. Actual
$("#date9").datepicker();
$("#date9").val(date_hidden_value);
$("#date10").datepicker();
$("#date10").val(date_hidden_value);

// Coaching Frequency Vs. AHT
$("#date11").datepicker();
$("#date11").val(date_hidden_value);
$("#date12").datepicker();
$("#date12").val(date_hidden_value);

// AHT Glide Path Targets Vs. Actual
$("#date13").datepicker();
$("#date13").val(date_hidden_value);
$("#date14").datepicker();
$("#date14").val(date_hidden_value);


// AHT Glide Path Targets Vs. Actual
$("#date15").datepicker();
$("#date15").val(date_hidden_value);
$("#date16").datepicker();
$("#date16").val(date_hidden_value);

$("#date17").datepicker();
$("#date18").val(date_hidden_value);
$("#date19").datepicker();
$("#date20").val(date_hidden_value);

$("#date21").datepicker();
$("#date21").val(date_hidden_value);
$("#date22").datepicker();
$("#date22").val(date_hidden_value);

$("#date23").datepicker();
$("#date23").val(date_hidden_value);
$("#date24").datepicker();
$("#date24").val(date_hidden_value);

$("#date25").datepicker();
$("#date25").val(date_hidden_value);
$("#date26").datepicker();
$("#date26").val(date_hidden_value);

$("#date27").datepicker();
$("#date27").val(date_hidden_value);
$("#date28").datepicker();
$("#date28").val(date_hidden_value);

$("#date29").datepicker();
$("#date29").val(date_hidden_value);
$("#date30").datepicker();
$("#date30").val(date_hidden_value);



  