﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExternalLandingPage.aspx.cs" Inherits="CoachV2.ExternalLandingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 id="heading" runat="server">
                    <asp:HiddenField ID="googleaccountHiddenField" runat="server" />
                    <asp:Label ID="lblEmail" runat="server"></asp:Label><asp:Label ID="lblGoogleName"
                        Visible="false" runat="server"></asp:Label>
                    <asp:Label ID="lblProfileImage" Visible="false" runat="server"></asp:Label>
                </h3>
            </div>
            <div class="panel-body">
                <div id="original_message" runat="server">
                    You've authenticated with <strong>Google</strong> as <strong class="primary">
                        <asp:Label ID="lblUsername" runat="server"></asp:Label></strong>.
                    <asp:Label runat="server" ID="AdditionalCaveat"> Please confirm
                below by clicking the Login button.</asp:Label>
                    <br />
                    <br />
                    <div class="text-center">
                        <asp:Button ID="Button1" runat="server" Text="Log in" ValidationGroup="NewUser" CssClass="btn btn-success"
                            OnClick="logIn_Click" />
                        <asp:Button ID="Button2" runat="server" Text="Cancel" CausesValidation="false" CssClass="btn btn-primary"
                            OnClick="cancel_Click" />
                        <asp:Label ID="ModelErrorMessage1" runat="server" />
                        
                    </div>
                </div>
                <!--<div id="unauthenticated_msg" runat="server">
                    
                            
                                <asp:ImageButton ID="btnRequestLogin2" runat="server" Text="Google Login" OnClick="BtnLoginGoogle_Click"
                                    ImageUrl="Content/images/btn_google_signin_dark_normal_web@2x.png" onmouseover="this.src='Content/images/btn_google_signin_dark_focus_web@2x.png'"
                                    onmouseout="this.src='Content/images/btn_google_signin_dark_normal_web@2x.png'"
                                    onmousedown="this.src='Content/images/btn_google_signin_dark_pressed_web@2x.png'"
                                    onmouseup="this.src='Content/images/btn_google_signin_dark_normal_web@2x.png'" />-->
                          
                        
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
    </div>
</asp:Content>
