﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CoachV2.AppCode;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace CoachV2
{
    public partial class CMTPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] != null)
                {
                    LoadCMTDropdowns();
                   // string ticket = Decrypt(Page.Request.QueryString["Coachingticket"]);
                    //decryption
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    int CoachingTicket = Convert.ToInt32(decrypt);
                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    GetCMTReviewDetails(CoachingTicket);
                    LoadInfractionClass();
                    loadcoacheedetails(CoachingTicket);
                    LoadDocumentations();


                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

        }
        protected void loadcoacheedetails(int reviewid)
        {
            try
            {
                DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));

                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;


                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = host + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = host + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                ProfImage.ImageUrl = ImgDefault;
                LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
                LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
                LblSite.Text = ds.Tables[0].Rows[0]["sitename"].ToString();
                LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
                LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
                LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
                //LblCIMNo.Text = Convert.ToString(coacheeid); //cim_num;
                //LblRegion.Text = ds.Tables[0].Rows[0]["Province_State"].ToString();
                LblRegion.Text = "English Speaking Region";
                LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
                LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";


                Session["CMTCIM"] = coacheeid;
                Session["CMTSupName"] = LblMySupervisor.Text;
                Session["CMTAccount"] = ds2.Tables[0].Rows[0]["Campaign"].ToString();
                Session["CMTName"] = LblFullName.Text;
                Session["CMTDepartment"] = ds.Tables[0].Rows[0]["Department"].ToString();
                Session["CMTCampaign"] = ds2.Tables[0].Rows[0]["Campaign"].ToString();
            }
            catch (Exception ex)
            {
                string ModalLabel = "loadcoacheedetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadDocs()
        {
            try
            {
                    Docspo();
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string Name = dsSAPInfo.Tables[0].Rows[0]["First_Name"].ToString() + " " + dsSAPInfo.Tables[0].Rows[0]["Last_Name"].ToString();
                    string DateToday = DateTime.Now.ToShortDateString();

                    foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
                    {
                        string filename = f.GetNameWithoutExtension() + f.GetExtension();

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["Docs"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["FileName"] = filename;
                        dr["UploadedBy"] = Name;
                        dr["DateUploaded"] = DateToday;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["Docs"] = dt;
                        }
                        this.BindListViewDocs();

                    }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadDocs " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void Docspo()
        {
            try
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("ID", typeof(int));
                dt2.Columns["ID"].AutoIncrement = true;
                dt2.Columns["ID"].AutoIncrementSeed = 1;
                dt2.Columns["ID"].AutoIncrementStep = 1;
                dt2.Columns.Add("FileName", typeof(string));
                dt2.Columns.Add("UploadedBy", typeof(string));
                dt2.Columns.Add("DateUploaded", typeof(string));
                ViewState["Docs"] = dt2;

                BindListViewDocs();
            }
            catch (Exception ex)
            {
                string ModalLabel = "Docspo " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        private void BindListViewDocs()
        {
            try
            {
                    DataTable dt = new DataTable();
                    dt = (DataTable)ViewState["Docs"];

                    if (dt.Rows.Count > 0 && dt != null)
                    {

                        Session["Docs"] = dt;
                    }
            }
            catch (Exception ex)
            {
                string ModalLabel = "BindListViewDocs " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadCMTPreview_Click(object sender, EventArgs e)
        {
            try
            {
                Session["PreviewType"] = 2;
                Session["CMTHRComment"] = RadHRComments.Text;
                LoadDocs();
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "var Mleft = (screen.width/2)-(760/2);var Mtop = (screen.height/2)-(700/2);window.open( 'CMT.aspx', null, 'height=700,width=760,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );", true);
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadCMTPreview_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadSearchBlackoutYes_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenSearchBlackout.Value = "1";
                RadSearchBlackout.Enabled = false;
                RadSearchBlackout.ForeColor = Color.Black;
                RadCMTSaveSubmit.Text = "Save";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadSearchBlackoutYes_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadSearchBlackoutNo_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);
        }
        protected void RadSearchBlackout_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMT(); });", true);
        }
        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            try
            {
                foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
                {
                    string targetFolder = Server.MapPath("~/Documentation/");
                    //string targetFolder = "C:\\Documentation\\";
                    f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                    ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload1.ClientID), true);
            }
            catch (Exception ex)
            {
                string ModalLabel = "UploadDocumentation " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadCMTSaveSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                {
                    int HRRep = string.IsNullOrEmpty(RadHRRep.Text) ? 0 : int.Parse(RadHRRep.Text);
                    int CaseLvl = string.IsNullOrEmpty(RadCaseLevel.SelectedValue) ? 0 : int.Parse(RadCaseLevel.SelectedValue);
                    int MJCat = string.IsNullOrEmpty(RadMajorCategory.SelectedValue) ? 0 : int.Parse(RadMajorCategory.SelectedValue);
                    int IC = string.IsNullOrEmpty(RadInfractionClass.SelectedValue) ? 0 : int.Parse(RadInfractionClass.SelectedValue);
                    int Stat = string.IsNullOrEmpty(RadStatus.SelectedValue) ? 0 : int.Parse(RadStatus.SelectedValue);
                    int WHCase = string.IsNullOrEmpty(RadWithHoldCase.SelectedValue) ? 0 : int.Parse(RadWithHoldCase.SelectedValue);
                    int HCType = string.IsNullOrEmpty(RadHoldCaseType.SelectedValue) ? 0 : int.Parse(RadHoldCaseType.SelectedValue);
                    int EmpAc = string.IsNullOrEmpty(RadEmployeeAccepts.SelectedValue) ? 0 : int.Parse(RadEmployeeAccepts.SelectedValue);
                    int Wit1 = string.IsNullOrEmpty(RadWitness1.Text) ? 0 : int.Parse(RadWitness1.Text);
                    int Wit2 = string.IsNullOrEmpty(RadWitness2.Text) ? 0 : int.Parse(RadWitness2.Text);
                    int WithSuspension = string.IsNullOrEmpty(RadWithSuspension.SelectedValue) ? 0 : int.Parse(RadWithSuspension.SelectedValue);
                    int CheckC = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
                    int NODWithHold = string.IsNullOrEmpty(RadNODWithHoldCase.SelectedValue) ? 0 : int.Parse(RadNODWithHoldCase.SelectedValue);
                    int NODHoldCaseType = string.IsNullOrEmpty(RadNODHoldCaseType.SelectedValue) ? 0 : int.Parse(RadNODHoldCaseType.SelectedValue);
                    int NODEmployeeAccept = string.IsNullOrEmpty(RadNODEmployeeAccept.SelectedValue) ? 0 : int.Parse(RadNODEmployeeAccept.SelectedValue);
                    int NODWitness1 = string.IsNullOrEmpty(RadNODWitness1.Text) ? 0 : int.Parse(RadNODWitness1.Text);
                    int NODWitness2 = string.IsNullOrEmpty(RadNODWitness2.Text) ? 0 : int.Parse(RadNODWitness2.Text);
                    int NODEscalatedBy = string.IsNullOrEmpty(RadEscalatedBy.Text) ? 0 : int.Parse(RadEscalatedBy.Text);
                    int NODCaseDecision = string.IsNullOrEmpty(RadCaseDecision.SelectedValue) ? 0 : int.Parse(RadCaseDecision.SelectedValue);
                    int NODNonAcceptance = string.IsNullOrEmpty(RadNODNonAcceptance.SelectedValue) ? 0 : int.Parse(RadNODNonAcceptance.SelectedValue);
                    int NODEoWAction = string.IsNullOrEmpty(RadEoW.SelectedValue) ? 0 : int.Parse(RadEoW.SelectedValue);


                    DateTime NTEDraft = Convert.ToDateTime(RadNTEDraft.SelectedDate);
                    DateTime NTEApproval = Convert.ToDateTime(RadNTEApproval.SelectedDate);
                    DateTime NTEIssueDate = Convert.ToDateTime(RadNTEIssueDate.SelectedDate);

                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    DataAccess ws = new DataAccess();
                    ws.UpdateCMTReview(Convert.ToInt32(RadReviewID.Text), Convert.ToInt32(HiddenSearchBlackout.Value), RadHRComments.Text, HRRep, RadSapReference.Text, CaseLvl, MJCat, IC, Stat, RadNTEDraft.SelectedDate, RadNTEApproval.SelectedDate, RadNTEIssueDate.SelectedDate, WHCase, RadHoldStartDate.SelectedDate, RadHoldEndDate.SelectedDate, HCType, RadHRImmediateComments.Text,
                    EmpAc, RadEmployeeAcceptDate.SelectedDate, Wit1, RadSignDate1.SelectedDate, Wit2, RadSignDate2.SelectedDate, RadNonAcceptance.SelectedValue, RadNonAcceptanceNotes.Text, RadRTNTEReceiveDate.SelectedDate, WithSuspension, RadAdminHearingDate1.SelectedDate, RadAdminHearingDate2.SelectedDate, RadAdminHearingConduct.SelectedDate
                    , CheckC, RadNODSubmittedDate.SelectedDate, RadNODApproval.SelectedDate, RadNODIssueDate.SelectedDate, NODWithHold, RadNODHoldStartDate.SelectedDate, RadNODHoldEndDate.SelectedDate, NODHoldCaseType, RadNODHrSuperiorNotes.Text, NODEmployeeAccept, RadNODEmployeeSignDate.SelectedDate, NODWitness1, RadNODWitness1SignDate.SelectedDate,
                    NODWitness2, RadNODWitness2SignDate.SelectedDate, RadNODNonAcceptance.SelectedValue, RadNODNonAcceptanceNotes.Text, RadNODReturnDate.SelectedDate, NODEscalatedBy, NODCaseDecision, RadNODCaseDecisionNotes.Text, RadNODEOWNotes.Text, NODEoWAction);
                    UploadDocumentation(0, Convert.ToInt32(RadReviewID.Text));
                    LoadDocumentations();
                    string ModalLabel = "Review has been updated.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "RadCMTSaveSubmit_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        private static T FindControl<T>(ControlCollection controls, string controlId)
        {
            T ctrl = default(T);
            foreach (Control ctl in controls)
            {
                if (ctl.GetType() != typeof(Telerik.Web.UI.GridTableRow))
                {
                    if (ctl.ClientID.Length <= controlId.Length)
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                    else
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                }


                if (ctl.Controls.Count > 0 && ctrl == null)
                    ctrl = FindControl<T>(ctl.Controls, controlId);
            }
            return ctrl;
        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        protected void RadMajorCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetCMTDropdowns(3, Convert.ToInt32(RadMajorCategory.SelectedValue));

                RadInfractionClass.AllowCustomText = true;
                RadInfractionClass.Text = "";
                RadInfractionClass.ClearSelection();
                RadInfractionClass.AllowCustomText = false;
                RadInfractionClass.DataSource = ds;
                RadInfractionClass.DataTextField = "Infraction_Name";
                RadInfractionClass.DataValueField = "id";
                RadInfractionClass.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadMajorCategory_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadCMTDropdowns()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetCMTDropdowns(1, 0);

                RadCaseLevel.DataSource = ds;
                RadCaseLevel.DataTextField = "Case_Name";
                RadCaseLevel.DataValueField = "Case_ID";
                RadCaseLevel.DataBind();

                DataSet ds2 = null;
                DataAccess ws2 = new DataAccess();
                ds2 = ws2.GetCMTDropdowns(2, 0);

                RadMajorCategory.DataSource = ds2;
                RadMajorCategory.DataTextField = "MJ_Name";
                RadMajorCategory.DataValueField = "MJ_ID";
                RadMajorCategory.DataBind();

                DataSet ds3 = null;
                DataAccess ws3 = new DataAccess();
                ds3 = ws3.GetCMTDropdowns(4, 0);

                RadStatus.DataSource = ds3;
                RadStatus.DataTextField = "Status";
                RadStatus.DataValueField = "ID";
                RadStatus.DataBind();

                DataSet ds4 = null;
                DataAccess ws4 = new DataAccess();
                ds4 = ws4.GetCMTDropdowns(5, 0);

                RadCaseDecision.DataSource = ds4;
                RadCaseDecision.DataTextField = "Decision_Name";
                RadCaseDecision.DataValueField = "ID";
                RadCaseDecision.DataBind();

                DataSet ds5 = null;
                DataAccess ws5 = new DataAccess();
                ds5 = ws5.GetCMTDropdowns(6, 0);

                RadHoldCaseType.DataSource = ds5;
                RadHoldCaseType.DataTextField = "Hold_Case_Type";
                RadHoldCaseType.DataValueField = "ID";
                RadHoldCaseType.DataBind();

                RadNODHoldCaseType.DataSource = ds5;
                RadNODHoldCaseType.DataTextField = "Hold_Case_Type";
                RadNODHoldCaseType.DataValueField = "ID";
                RadNODHoldCaseType.DataBind();

                DataSet ds6 = null;
                DataAccess ws6 = new DataAccess();
                ds6 = ws6.GetCMTDropdowns(7, 0);


                RadNODNonAcceptance.DataSource = ds6;
                RadNODNonAcceptance.DataTextField = "Non_Acceptance_Reason";
                RadNODNonAcceptance.DataValueField = "ID";
                RadNODNonAcceptance.DataBind();


                RadNonAcceptance.DataSource = ds6;
                RadNonAcceptance.DataTextField = "Non_Acceptance_Reason";
                RadNonAcceptance.DataValueField = "ID";
                RadNonAcceptance.DataBind();

                RadGridDocumentation.DataSource = String.Empty;
                RadGridDocumentation.DataBind();

                DataSet ds7 = null;
                DataAccess ws7 = new DataAccess();
                ds7 = ws7.GetCMTDropdowns(8, 0);


                RadEoW.DataSource = ds7;
                RadEoW.DataTextField = "Act_Rqrd";
                RadEoW.DataValueField = "ID";
                RadEoW.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadCMTDropdowns " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadInfractionClass()
        {
            try
            {
                if (RadMajorCategory.SelectedIndex != -1)
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetCMTDropdowns(3, Convert.ToInt32(RadMajorCategory.SelectedValue));

                    RadInfractionClass.DataSource = ds;
                    RadInfractionClass.DataTextField = "Infraction_Name";
                    RadInfractionClass.DataValueField = "id";
                    RadInfractionClass.DataBind();
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadInfractionClass " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void GetCMTReviewDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetCMTReview(CoachingTicket);
                Session["CMTReviewID"] = CoachingTicket;
                if (ds.Tables[0].Rows.Count > 0)
                {


                    string SessionName = "Session Name: " + ds.Tables[0].Rows[0]["SessionName"].ToString();
                    string TopicName = "Topic: " + ds.Tables[0].Rows[0]["TopicName"].ToString();
                    RadReviewID.Text = ds.Tables[0].Rows[0]["ReviewID"].ToString();
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                    LblFullName.Text = ds.Tables[0].Rows[0]["EmployeeName"].ToString();
                    LblSessionType.Text = SessionName;
                    LblSessionTopic.Text = TopicName;


                    Session["CMTSessionType"] = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Session["CMTSessionTopic"] = ds.Tables[0].Rows[0]["TopicName"].ToString();


                    if (ds.Tables[0].Rows[0]["CreatedBy"].ToString() != Convert.ToString(CIMNumber))
                    {
                        RadCMTPreview.Visible = false;
                        RadCMTSaveSubmit.Visible = false;
                        //RadSearchBlackout.Enabled = false;
                    }

                    if (ds.Tables[0].Rows[0]["SearchBlackout"].ToString() == "True")
                    {
                        RadSearchBlackout.Enabled = false;
                        RadSearchBlackout.ForeColor = Color.Black;
                        RadCMTSaveSubmit.Text = "Save";
                    }




                    RadHRComments.Text = ds.Tables[0].Rows[0]["HRComment"].ToString();
                    Session["CMTHRComment"] = RadHRComments.Text;
                    RadHRRep.Text = ds.Tables[0].Rows[0]["HRCIM"].ToString();
                    RadSapReference.Text = ds.Tables[0].Rows[0]["TandimSAPRefNumber"].ToString();

                    RadCaseLevel.SelectedValue = ds.Tables[0].Rows[0]["CaseLevelID"].ToString();


                    Label3.Text = "Coaching Dashboard > ";
                    Label1.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    Label2.Text = " > " + ds.Tables[0].Rows[0]["EmployeeName"].ToString();
                    RadMajorCategory.SelectedValue = ds.Tables[0].Rows[0]["MajorCategoryID"].ToString();
                    RadInfractionClass.SelectedValue = ds.Tables[0].Rows[0]["InfractionClassID"].ToString();
                    RadStatus.SelectedValue = ds.Tables[0].Rows[0]["Status"].ToString();

                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEDraftSubmittedApproval"]) == false)
                    {
                        RadNTEDraft.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEDraftSubmittedApproval"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEDraftApproval"]) == false)
                    {
                        RadNTEApproval.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEDraftApproval"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEIssueDate"]) == false)
                    {
                        RadNTEIssueDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEIssueDate"].ToString());
                    }
                    RadWithHoldCase.SelectedValue = ds.Tables[0].Rows[0]["NTEWithHoldCase"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEHoldStartDate"]) == false)
                    {
                        RadHoldStartDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEHoldStartDate"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEHoldEndDate"]) == false)
                    {
                        RadHoldEndDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEHoldEndDate"].ToString());
                    }
                    RadHoldCaseType.SelectedValue = ds.Tables[0].Rows[0]["NTEHoldCaseType"].ToString();
                    RadHRImmediateComments.Text = ds.Tables[0].Rows[0]["NTEHRSuperiorComments"].ToString();
                    RadEmployeeAccepts.SelectedValue = ds.Tables[0].Rows[0]["NTEmployeeAccepts"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEEmployeeSignDate"]) == false)
                    {
                        RadEmployeeAcceptDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEEmployeeSignDate"].ToString());
                    }
                    RadWitness1.Text = ds.Tables[0].Rows[0]["NTEWitness1"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEWitness1SignDate"]) == false)
                    {
                        RadSignDate1.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEWitness1SignDate"].ToString());
                    }
                    RadWitness2.Text = ds.Tables[0].Rows[0]["NTEWitness2"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NTEWitness2SignDate"]) == false)
                    {
                        RadSignDate2.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NTEWitness2SignDate"].ToString());
                    }
                    RadNonAcceptance.SelectedValue = ds.Tables[0].Rows[0]["NTENonAcceptanceReason"].ToString();
                    RadNonAcceptanceNotes.Text = ds.Tables[0].Rows[0]["NTENonAcceptanceNotes"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["RTNTEDate"]) == false)
                    {
                        RadRTNTEReceiveDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["RTNTEDate"].ToString());
                    }
                    RadWithSuspension.SelectedValue = ds.Tables[0].Rows[0]["PWithSuspension"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["PSchedule1"]) == false)
                    {
                        RadAdminHearingDate1.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["PSchedule1"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["PSchedule2"]) == false)
                    {
                        RadAdminHearingDate2.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["PSchedule2"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["PAdminHearing"]) == false)
                    {
                        RadAdminHearingConduct.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["PAdminHearing"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["CheckBoxChoice"]) == false)
                    {
                        CBList.SelectedValue = ds.Tables[0].Rows[0]["CheckBoxChoice"].ToString();
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODSubmissionApprovalDate"]) == false)
                    {
                        RadNODSubmittedDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODSubmissionApprovalDate"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODApprovalDate"]) == false)
                    {
                        RadNODApproval.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODApprovalDate"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODIssueDate"]) == false)
                    {
                        RadNODIssueDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODIssueDate"].ToString());
                    }
                    RadNODWithHoldCase.SelectedValue = ds.Tables[0].Rows[0]["NODWithHoldCase"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODHoldStartDate"]) == false)
                    {
                        RadNODHoldStartDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODHoldStartDate"].ToString());
                    }
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODHoldEndDate"]) == false)
                    {
                        RadNODHoldEndDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODHoldEndDate"].ToString());
                    }
                    RadNODHoldCaseType.SelectedValue = ds.Tables[0].Rows[0]["NODHoldCaseType"].ToString();
                    RadNODHrSuperiorNotes.Text = ds.Tables[0].Rows[0]["NODHRSuperiorNotes"].ToString();
                    RadNODEmployeeAccept.SelectedValue = ds.Tables[0].Rows[0]["NODEmployeeAccepts"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODEmployeeSignDate"]) == false)
                    {
                        RadNODEmployeeSignDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODEmployeeSignDate"].ToString());
                    }
                    RadNODWitness1.Text = ds.Tables[0].Rows[0]["NODWitness1"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODWitness1SignDate"]) == false)
                    {
                        RadNODWitness1SignDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODWitness1SignDate"].ToString());
                    }
                    RadNODWitness2.Text = ds.Tables[0].Rows[0]["NODWitness2"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODWitness2SignDate"]) == false)
                    {
                        RadNODWitness2SignDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODWitness2SignDate"].ToString());
                    }
                    RadNODNonAcceptance.SelectedValue = ds.Tables[0].Rows[0]["NODNonAcceptanceReason"].ToString();
                    RadNODNonAcceptanceNotes.Text = ds.Tables[0].Rows[0]["NODNonAcceptanceNotes"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODReturnDate"]) == false)
                    {
                        RadNODReturnDate.SelectedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["NODReturnDate"].ToString());
                    }
                    RadEscalatedBy.Text = ds.Tables[0].Rows[0]["NODEscalated"].ToString();
                    RadCaseDecision.SelectedValue = ds.Tables[0].Rows[0]["NODCaseDecision"].ToString();
                    RadNODCaseDecisionNotes.Text = ds.Tables[0].Rows[0]["NODCaseDecisionNotes"].ToString();
                    RadNODEOWNotes.Text = ds.Tables[0].Rows[0]["NODEOWNotes"].ToString();
                    if (DBNull.Value.Equals(ds.Tables[0].Rows[0]["NODEoWAction"]) == false)
                    {
                        RadEoW.SelectedValue = ds.Tables[0].Rows[0]["NODEoWAction"].ToString();
                    }

                    DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(CoachingTicket));
                    grd_Coaching_Notes.DataSource = ds_coachingnotes;
                    grd_Coaching_Notes.DataBind();
                    LoadPreviousPerformanceResults(Convert.ToInt32(RadReviewID.Text));

                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "GetCMTReviewDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        public void LoadPreviousPerformanceResults(int ReviewID)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 2);
                RadGridPRCMT.DataSource = ds;
                RadGridPRCMT.DataBind();

            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "LoadPreviousPerformanceResults " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }


    }
}