﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MyProfile.aspx.cs" Inherits="CoachV2.MyProfile" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="UserControl/SidebarUserControl.ascx" TagName="SidebarUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/ProfileMainUserControl.ascx" TagName="ProfileMainUserControl"
    TagPrefix="uc2" %>
<%@ Register Src="UserControl/ProfileExperienceUserControl.ascx" TagName="ProfileExperienceUserControl"
    TagPrefix="uc3" %>
<%@ Register src="UserControl/ProfileEducUserControl.ascx" tagname="ProfileEducUserControl" tagprefix="uc4" %>
<%@ Register src="UserControl/ProfileSkillUserControl.ascx" tagname="ProfileSkillUserControl" tagprefix="uc5" %>
<%@ Register src="UserControl/ProfileCertUserControl.ascx" tagname="ProfileCertUserControl" tagprefix="uc6" %>
<%@ Register src="UserControl/ProfileExtraInfoUserCtrl.ascx" tagname="ProfileExtraInfoUserCtrl" tagprefix="uc7" %>
<%@ Register src="UserControl/AgentSearch.ascx" tagname="AgentSearchCtrl" tagprefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl ID="SidebarUserControl1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server" >
    <uc2:ProfileMainUserControl ID="ProfileMainUserControl1" runat="server" Visible="false"  />
    <uc4:ProfileEducUserControl ID="ProfileEducUserControl1" runat="server" Visible="false" />
    <uc3:ProfileExperienceUserControl ID="ProfileExperienceUserControl1" runat="server"
        Visible="false" />

    <uc7:ProfileExtraInfoUserCtrl ID="ProfileExtraInfoUserCtrl1" runat="server" />

    <uc6:ProfileCertUserControl ID="ProfileCertUserControl1" runat="server" />
    <uc5:ProfileSkillUserControl ID="ProfileSkillUserControl1" runat="server" />
    <uc8:AgentSearchCtrl ID="AgentSearchCtrl1" runat="server"  Visible="false" />
</asp:Content>
