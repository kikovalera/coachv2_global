﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using CoachV2.AppCode;

namespace CoachV2
{
    public partial class ReportBuilder : System.Web.UI.Page
    {
        int checkedpo = 0;
        int CIMNo;
        public DataTable Columns
        {
            get
            {
                return ViewState["Columns"] as DataTable;
            }
            set
            {
                ViewState["Columns"] = value;
            }
        }
        public DataTable NewColumns
        {
            get
            {
                return ViewState["NewColumns"] as DataTable;
            }
            set
            {
                ViewState["NewColumns"] = value;
            }
        }
        protected void ColumnsPo()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DataSourceID", typeof(string));
            dt.Columns.Add("Fields", typeof(string));
            ViewState["Columns"] = dt;
            BindListView();
        }
        protected void newColumnsPo()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DataSourceID", typeof(string));
            dt.Columns.Add("Fields", typeof(string));
            ViewState["newColumns"] = dt;
            BindListView();
        }
        private void BindListView()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["Columns"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadListBox2.DataSource = dt;
                RadListBox2.DataTextField = dt.Columns["Fields"].ToString();
                RadListBox2.DataValueField = dt.Columns["DataSourceID"].ToString();
                RadListBox2.DataBind();


            }
            else
            {
                RadListBox2.Items.Clear();
                RadListBox2.DataSource = null;
                RadListBox2.DataBind();


            }
        }
        private void BindListViewNew()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["newColumns"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadListBox1.DataSource = dt;
                RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                RadListBox1.DataBind();
            }
            else
            {
                RadListBox1.DataSource = null;
                RadListBox1.DataBind();
            }
        }       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                ctrlReviews.Text = " Coaching Dashboard ";
                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                ctrlA.Text = " > My Reports";
                Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                ctrlB.Text = " > Report Builder";
                ctrlB.Visible = true;

                ColumnsPo();
                newColumnsPo();

            }

        }

        protected void radUsers_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenTableName.Value = "UsersTable";
                HiddenTableID.Value = "1";
                Rbuilder.Visible = true;
                radUsers.BackColor = System.Drawing.Color.SkyBlue;
                radUsers.ForeColor = System.Drawing.Color.White;

                radReviews.BackColor = System.Drawing.Color.Empty;
                radReviews.ForeColor = System.Drawing.Color.Empty;


                radHRTables.BackColor = System.Drawing.Color.Empty;
                radHRTables.ForeColor = System.Drawing.Color.Empty;

                radKPI.BackColor = System.Drawing.Color.Empty;
                radKPI.ForeColor = System.Drawing.Color.Empty;

                Report rt = new Report();
                DataSet ds = null;
                ds = rt.GetColumns2("UsersTable");
                DataTable dt = null;
                dt = ds.Tables[0];
                ViewState["newColumns"] = dt;

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    RadListBox1.DataTextField = "Fields";
                //    RadListBox1.DataValueField = "DataSourceID";
                //    RadListBox1.DataSource = ds;
                //    RadListBox1.DataBind();
                //}
                if (dt.Rows.Count > 0)
                {
                    RadListBox1.DataSource = dt;
                    RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                    RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                    RadListBox1.DataBind();
                }

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    RadListBox1.DataTextField = "Fields";
                //    RadListBox1.DataValueField = "DataSourceID";
                //    RadListBox1.DataSource = ds;
                //    RadListBox1.DataBind();

                //}

                if (RadListBox2.Items.Count > 0)
                {

                    GetNotSelectedItems();
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        protected void radReviews_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenTableName.Value = "Reviews";
                HiddenTableID.Value = "2";
                Rbuilder.Visible = true;
                radReviews.BackColor = System.Drawing.Color.SkyBlue;
                radReviews.ForeColor = System.Drawing.Color.White;

                radUsers.BackColor = System.Drawing.Color.Empty;
                radUsers.ForeColor = System.Drawing.Color.Empty;

                radKPI.BackColor = System.Drawing.Color.Empty;
                radKPI.ForeColor = System.Drawing.Color.Empty;

                radHRTables.BackColor = System.Drawing.Color.Empty;
                radHRTables.ForeColor = System.Drawing.Color.Empty;

                Report rt = new Report();
                DataSet ds = null;
                ds = rt.GetColumns2("Reviews");
                DataTable dt = null;
                dt = ds.Tables[0];
                ViewState["newColumns"] = dt;

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    RadListBox1.DataTextField = "Fields";
                //    RadListBox1.DataValueField = "DataSourceID";
                //    RadListBox1.DataSource = ds;
                //    RadListBox1.DataBind();
                //}
                if (dt.Rows.Count > 0)
                {
                    RadListBox1.DataSource = dt;
                    RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                    RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                    RadListBox1.DataBind();
                }

                if (RadListBox2.Items.Count > 0)
                {

                    GetNotSelectedItems();
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        protected void RadAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadListBox1.CheckedItems.Count > 0)
                {

                    DataTable dt = new DataTable();
                    DataRow drow;
                    dt = (DataTable)ViewState["Columns"];

                    foreach (RadListBoxItem item in RadListBox1.Items)
                    {
                        if (item.Checked == true)
                        {
                            drow = dt.NewRow();
                            drow["DataSourceID"] = item.Value;
                            drow["Fields"] = item.Text;
                            dt.Rows.Add(drow);

                        }
                    }
                    BindListView();

                 
                    GetNotSelectedItems();
                }
                else
                {
                    string myStringVariable = "Please select data fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }

        }

        protected void RadPreview_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                CIMNo = Convert.ToInt32(cim_num);

                if (RadReportName.Text == "")
                {
                    string myStringVariable = "Report Name is a required field.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }

                else if (RadListBox2.Items.Count == 0)
                {
                    string myStringVariable = "Please select data fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }
                else
                {
                    Report rp = new Report();
                    DataSet dsr = null;
                    dsr = rp.CheckIfReportNameExists(RadReportName.Text, CIMNo.ToString(), 1);

                    if (dsr.Tables[0].Rows.Count > 0)
                    {
                        string ModalLabel = "Report already exists. Please use another report name";
                        string ModalHeader = "Error";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        RadReportName.Focus();
                    }
                    else
                    {

                        DataTable dt = GetListBoxItems(RadListBox2);
                        ViewState["Columns"] = dt;

                        if (HiddenReportID.Value == "0")
                        {
                            string DateFrom, DateTo;
                            if (RadDateRangeFrom.SelectedDate.HasValue)
                            {
                                DateFrom = RadDateRangeFrom.SelectedDate.ToString();
                            }
                            else
                            {
                                DateFrom = DBNull.Value.ToString();
                            }

                            if (RadDateRangeTo.SelectedDate.HasValue)
                            {
                                DateTo = RadDateRangeTo.SelectedDate.ToString();
                            }
                            else
                            {
                                DateTo = DBNull.Value.ToString();
                            }

                            Report rt = new Report();
                            int ReportID = rt.InsertReportNew(RadReportName.Text, CIMNo.ToString(), DateFrom, DateTo, 0, 0, 1);
                            HiddenReportID.Value = ReportID.ToString();

                            int ReportIDDataSource;
                            DataView view = new DataView(dt);
                            DataTable distinctValues = view.ToTable(true, "DataSourceID");

                            for (int i = distinctValues.Rows.Count - 1; i >= 0; i--)
                            {
                                DataRow dr = distinctValues.Rows[i];
                                string dsid = dr["DataSourceID"].ToString();

                                Report rt2 = new Report();
                                ReportIDDataSource = rt2.InsertintoReportDataSourceNew(ReportID, dsid);
                            }

                            foreach (RadListBoxItem item in RadListBox2.Items)
                            {

                                string DataSourceID = item.Value;
                                string ColumnName = item.Text;

                                Report rt3 = new Report();
                                rt3.InsertintoReportColumnsNew(ReportID, ColumnName, Convert.ToInt32(DataSourceID));

                            }

                            ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ReportID + "&UserID=" + Convert.ToString(CIMNo) + "','Graph','height=400,width=500');", true);
                            //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + 1 + "&UserID=" + 10115015 + "','Graph','height=400,width=500');", true);

                        }
                        else
                        {
                            int ReportID = Convert.ToInt32(HiddenReportID.Value);
                            ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ReportID + "&UserID=" + Convert.ToString(CIMNo) + "','Graph','height=400,width=500');", true);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        public static DataTable GetListBoxItems(RadListBox RadListBox2)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DataSourceID");
            dt.Columns.Add("Fields");


            foreach (RadListBoxItem itm in RadListBox2.Items)
            {
                DataRow dr = dt.NewRow();
                dr[0] = itm.Value;
                dr[1] = itm.Text;
                dt.Rows.Add(dr);
            }
            return dt;

        }

        private void GetNotSelectedItems()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["Columns"];
            DataTable dt2 = new DataTable();
            dt2 = (DataTable)ViewState["newColumns"];


            //DataTable dt = new DataTable();
            //dt = Columns1;
            //DataTable dt2 = new DataTable();
            //dt2 = Columns2;

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                string ds = dr["Fields"].ToString();
                string dsid = dr["DataSourceID"].ToString();

                //for (int a = dt2.Rows.Count - 1; a >= 0; a--)
                //{

                //        DataRow dr2 = dt2.Rows[i];
                //        if (dr2.RowState != DataRowState.Deleted)
                //        {
                //            if (dr2["DataSourceID"].ToString() == dsid && dr2["Fields"].ToString() == ds)
                //            {

                //                dr2.Delete();

                //            }
                //        }                    
                //}

                DataRow[] result = dt2.Select("Fields='" + ds + "' AND DataSourceID ='" + dsid + "'");

                foreach (DataRow row in result)
                {
                    if (row.RowState != DataRowState.Deleted)
                    {
                        row.Delete();
                    }
                }
            }

            BindListViewNew();

        }

        protected void RadButtonAddAll_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow drow;
                dt = (DataTable)ViewState["Columns"];

                foreach (RadListBoxItem item in RadListBox1.Items)
                {
                    drow = dt.NewRow();
                    drow["DataSourceID"] = item.Value;
                    drow["Fields"] = item.Text;
                    dt.Rows.Add(drow);

                }
                BindListView();
                //DataTable dt1, dt2;
                //dt1 = (DataTable)ViewState["Columns"];
                //dt2 = (DataTable)ViewState["NewColumns"];
                GetNotSelectedItems();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);


            }
        }

        private void GetRemovedItems()
        {
            ReloadColumns(HiddenTableName.Value);
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["Columns"];
            DataTable dt2 = new DataTable();
            dt2 = (DataTable)ViewState["newColumns"];


            //DataTable dt = new DataTable();
            //dt = Columns1;
            //DataTable dt2 = new DataTable();
            //dt2 = Columns2;

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                string ds = dr["Fields"].ToString();
                string dsid = dr["DataSourceID"].ToString();

                //for (int a = dt2.Rows.Count - 1; a >= 0; a--)
                //{

                //        DataRow dr2 = dt2.Rows[i];
                //        if (dr2.RowState != DataRowState.Deleted)
                //        {
                //            if (dr2["DataSourceID"].ToString() == dsid && dr2["Fields"].ToString() == ds)
                //            {

                //                dr2.Delete();

                //            }
                //        }                    
                //}

                DataRow[] result = dt2.Select("Fields='" + ds + "' AND DataSourceID ='" + dsid + "'");

                foreach (DataRow row in result)
                {
                    if (row.RowState != DataRowState.Deleted)
                    {
                        row.Delete();
                    }
                }
            }

            BindListView();
            BindListViewNew();

        }
        
        protected void RadButtonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadListBox2.CheckedItems.Count > 0)
                {

                    DataTable dt = new DataTable();
                    //DataRow drow;
                    dt = (DataTable)ViewState["Columns"];

                    foreach (RadListBoxItem item in RadListBox2.Items)
                    {
                        if (item.Checked == true)
                        {
                            string datasourceid = item.Value;
                            string field = item.Text;
                          

                            for (int i = dt.Rows.Count - 1; i >= 0; i--)
                            {
                                DataRow dr = dt.Rows[i];
                                //string ds = dr["Fields"].ToString();
                                //string dsid = dr["DataSourceID"].ToString();

                                //for (int a = dt2.Rows.Count - 1; a >= 0; a--)
                                //{

                                //        DataRow dr2 = dt2.Rows[i];
                                //        if (dr2.RowState != DataRowState.Deleted)
                                //        {
                                //            if (dr2["DataSourceID"].ToString() == dsid && dr2["Fields"].ToString() == ds)
                                //            {

                                //                dr2.Delete();

                                //            }
                                //        }                    
                                //}

                                DataRow[] result = dt.Select("Fields='" + field + "' AND DataSourceID ='" + datasourceid + "'");

                                foreach (DataRow row in result)
                                {
                                    if (row.RowState != DataRowState.Deleted)
                                    {
                                        row.Delete();
                                    }
                                }
                            }

                        }
                    }

                    BindListView();
                    GetRemovedItems();
                }
                else
                {
                    string myStringVariable = "Please select data fields to remove.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }

        }

        public void ReloadColumns(string TableName)
        {
            Report rt = new Report();
            DataSet ds = null;
            ds = rt.GetColumns2(TableName);
            DataTable dt = null;
            dt = ds.Tables[0];
            ViewState["newColumns"] = dt;

            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    RadListBox1.DataTextField = "Fields";
            //    RadListBox1.DataValueField = "DataSourceID";
            //    RadListBox1.DataSource = ds;
            //    RadListBox1.DataBind();
            //}
            if (dt.Rows.Count > 0)
            {
                RadListBox1.DataSource = dt;
                RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                RadListBox1.DataBind();
            }
        }

        protected void RadButtonRemoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadListBox2.Items.Count > 0)
                {
                    //DataTable dt = new DataTable();
                    ////DataRow drow;
                    //dt = (DataTable)ViewState["Columns"];

                    //for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    //{
                    //    DataRow row = dt.Rows[i];
                    //    if (row.RowState == DataRowState.Deleted) { dt.Rows.RemoveAt(i); }
                    //}
                    ColumnsPo();
                    BindListView();
                    BindListViewNew();
                    GetRemovedItems();
                }
                else
                {
                    string myStringVariable = "Please select data fields to remove.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);
                
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);


            }
        }

        protected void radKPI_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenTableName.Value = "ReviewKPI";
                HiddenTableID.Value = "2";
                Rbuilder.Visible = true;
                radKPI.BackColor = System.Drawing.Color.SkyBlue;
                radKPI.ForeColor = System.Drawing.Color.White;

                radUsers.BackColor = System.Drawing.Color.Empty;
                radUsers.ForeColor = System.Drawing.Color.Empty;

                radReviews.BackColor = System.Drawing.Color.Empty;
                radReviews.ForeColor = System.Drawing.Color.Empty;

                radHRTables.BackColor = System.Drawing.Color.Empty;
                radHRTables.ForeColor = System.Drawing.Color.Empty;

                Report rt = new Report();
                DataSet ds = null;
                ds = rt.GetColumns2("ReviewKPI");
                DataTable dt = null;
                dt = ds.Tables[0];
                ViewState["newColumns"] = dt;

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    RadListBox1.DataTextField = "Fields";
                //    RadListBox1.DataValueField = "DataSourceID";
                //    RadListBox1.DataSource = ds;
                //    RadListBox1.DataBind();
                //}
                if (dt.Rows.Count > 0)
                {
                    RadListBox1.DataSource = dt;
                    RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                    RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                    RadListBox1.DataBind();
                }

                if (RadListBox2.Items.Count > 0)
                {

                    GetNotSelectedItems();
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        protected void radHRTables_Click(object sender, EventArgs e)
        {

            try
            {
                HiddenTableName.Value = "HRTables";
                HiddenTableID.Value = "4";
                Rbuilder.Visible = true;
                radHRTables.BackColor = System.Drawing.Color.SkyBlue;
                radHRTables.ForeColor = System.Drawing.Color.White;
                radUsers.BackColor = System.Drawing.Color.Empty;
                radUsers.ForeColor = System.Drawing.Color.Empty;
                radKPI.BackColor = System.Drawing.Color.Empty;
                radKPI.ForeColor = System.Drawing.Color.Empty;
                radReviews.BackColor = System.Drawing.Color.Empty;
                radReviews.ForeColor = System.Drawing.Color.Empty;
                Report rt = new Report();
                DataSet ds = null;
                ds = rt.GetColumns2("HRTables");
                DataTable dt = null;
                dt = ds.Tables[0];
                ViewState["newColumns"] = dt;

                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    RadListBox1.DataTextField = "Fields";
                //    RadListBox1.DataValueField = "DataSourceID";
                //    RadListBox1.DataSource = ds;
                //    RadListBox1.DataBind();
                //}
                if (dt.Rows.Count > 0)
                {
                    RadListBox1.DataSource = dt;
                    RadListBox1.DataTextField = dt.Columns["Fields"].ToString();
                    RadListBox1.DataValueField = dt.Columns["DataSourceID"].ToString();
                    RadListBox1.DataBind();
                }

                if (RadListBox2.Items.Count > 0)
                {

                    GetNotSelectedItems();
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        protected void RadSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                CIMNo = Convert.ToInt32(cim_num);

                if (RadReportName.Text == "")
                {
                    string myStringVariable = "Report Name is a required field.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }

                else if (RadListBox2.Items.Count == 0)
                {
                    string myStringVariable = "Please select data fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }
                else
                {

                    //checking if report name is already existing

                    Report rp = new Report();
                    DataSet dsr = null;
                    dsr = rp.CheckIfReportNameExists(RadReportName.Text, cim_num, 2);
                    
                if (dsr.Tables[0].Rows.Count > 0)
                {
                    string ModalLabel = "Report already exists. Please use another report name";
                    string ModalHeader = "Error";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    RadReportName.Focus();
                }
                else
                {
                    DataTable dt = GetListBoxItems(RadListBox2);
                    ViewState["Columns"] = dt;

                    if (HiddenReportID.Value == "0")
                    {
                        string DateFrom, DateTo;
                        if (RadDateRangeFrom.SelectedDate.HasValue)
                        {
                            DateFrom = RadDateRangeFrom.SelectedDate.ToString();
                        }
                        else
                        {
                            DateFrom = DBNull.Value.ToString();
                        }

                        if (RadDateRangeTo.SelectedDate.HasValue)
                        {
                            DateTo = RadDateRangeTo.SelectedDate.ToString();
                        }
                        else
                        {
                            DateTo = DBNull.Value.ToString();
                        }

                        Report rt = new Report();
                        int ReportID = rt.InsertReportNew(RadReportName.Text, CIMNo.ToString(), DateFrom, DateTo, 0, 0, 1);
                        HiddenReportID.Value = ReportID.ToString();

                        int ReportIDDataSource;
                        DataView view = new DataView(dt);
                        DataTable distinctValues = view.ToTable(true, "DataSourceID");

                        for (int i = distinctValues.Rows.Count - 1; i >= 0; i--)
                        {
                            DataRow dr = distinctValues.Rows[i];
                            string dsid = dr["DataSourceID"].ToString();

                            Report rt2 = new Report();
                            ReportIDDataSource = rt2.InsertintoReportDataSourceNew(ReportID, dsid);
                        }

                        foreach (RadListBoxItem item in RadListBox2.Items)
                        {

                            string DataSourceID = item.Value;
                            string ColumnName = item.Text;

                            Report rt3 = new Report();
                            rt3.InsertintoReportColumnsNew(ReportID, ColumnName, Convert.ToInt32(DataSourceID));

                        }

                        Report rpt = new Report();
                        rpt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 1);

                        string ModalLabel = "Successfully saved report.";
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);


                    }
                    else
                    {
                        int ReportID = Convert.ToInt32(HiddenReportID.Value);

                        Report rt = new Report();
                        rt.UpdateReportStatus(Convert.ToInt32(HiddenReportID.Value), 1);

                        string ModalLabel = "Successfully saved report.";
                        string ModalHeader = "Success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }

                }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }
        }

        protected void RadTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadListBox1.CheckedItems.Count > 0)
                {

                    DataTable dt = new DataTable();
                    DataRow drow;
                    dt = (DataTable)ViewState["Columns"];

                    foreach (RadListBoxItem item in RadListBox1.Items)
                    {
                        if (item.Checked == true)
                        {
                            drow = dt.NewRow();
                            drow["DataSourceID"] = item.Value;
                            drow["Fields"] = item.Text;
                            dt.Rows.Add(drow);

                        }
                    }
                    BindListView();


                    GetNotSelectedItems();
                }
                else
                {
                    string myStringVariable = "Please select data fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

            }

        }


   
    }
}