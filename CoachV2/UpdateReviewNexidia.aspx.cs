﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using System.IO;

namespace CoachV2
{
    public partial class UpdateReviewNexidia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] == null)
                {

                    Response.Redirect("~/Default.aspx");
                 }
                else
                {
                    pn_coacheedetails.Visible = true;
                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 1260;
                    
                    //decryption
                    string decryptxt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    loadcoacheedetails(Convert.ToInt32(decryptxt));
                    //loadcoacheedetails(Convert.ToInt32(Page.Request.QueryString["Coachingticket"]));//(CoachingTicket);                   


                    //Response.Redirect("~/Default.aspx");
                }
            }
        }
        protected void loadcoacheedetails(int reviewid)
        {
            try
            {

                DataSet ds3 = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string emp_email = ds3.Tables[0].Rows[0]["Email"].ToString();
                string cim_num = ds3.Tables[0].Rows[0][0].ToString();
                int intcim = Convert.ToInt32(cim_num);

                DataSet ds_get_review = DataHelper.Get_ReviewIDv2(reviewid);

                int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));

                DataSet ds3x = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review.Tables[0].Rows[0]["createdby"]));

                string createdByName = ds3x.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds3x.Tables[0].Rows[0]["Last_Name"].ToString();

                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString() + "/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString() + ".jpg"));
                string ImgDefault;

                int coacheesigned, coachersigned, saved, audited;
                if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
                {
                    coachersigned = 0;
                }
                else
                {
                    coachersigned = 1;
                }
                if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
                {
                    coacheesigned = 0;
                }
                else
                {
                    coacheesigned = 1;
                }

                if (ds_get_review.Tables[0].Rows[0]["saved"] is DBNull)
                {
                    saved = 0;
                }
                else
                {
                    saved = 1;
                }
                if (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull)
                {
                    audited = 0;
                }
                else
                {
                    audited = 1;
                }


                string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
                string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

                if (rolevalue == "QA")
                    saprolefordashboard = "QA";
                else if (rolevalue == "HR")
                    saprolefordashboard = "HR";
                else
                    saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
                string retrole = "";
                if (saprolefordashboard == "TL")
                {
                    retrole = "TL";
                }
                else if (saprolefordashboard == "OM")
                {
                    retrole = "OM";
                }
                else if (saprolefordashboard == "Dir")
                {
                    retrole = "OM";
                }
                else if (saprolefordashboard == "QA")
                {


                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                    if ((saprole1 == "OM") || (saprole1 == "Dir"))
                    {
                        retrole = "OM";
                    }
                    else
                    {

                        retrole = "";
                    }
                }
                else if (saprolefordashboard == "HR")
                {


                    string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                    if ((saprole1 == "OM") || (saprole1 == "Dir"))
                    {
                        retrole = "OM";
                    }
                    else
                    {
                        retrole = "";
                    }
                }
                else
                {
                    retrole = "";
                }

                if (host.Substring(host.Length - 1, 1) != @"/") { 
                    host+=@"\";
                }   
                if (!File.Exists(directoryPath.Remove(directoryPath.Length-1)))
                {
                    ImgDefault = host + "Content/images/no-photo.jpg";
                }
                else
                {
                    ImgDefault = host + "Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                ProfImage.ImageUrl = ImgDefault;
                LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
                LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
                LblSite.Text = ds.Tables[0].Rows[0]["sitename"].ToString();
                LblDept.Text = ds_get_review.Tables[0].Rows[0]["Account"].ToString();
                LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
                LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
                LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();
                LblClient.Text = ds_get_review.Tables[0].Rows.Count > 0 ? ds_get_review.Tables[0].Rows[0]["Division"].ToString() : "";
                LblCampaign.Text = ds_get_review.Tables[0].Rows.Count > 0 ? ds_get_review.Tables[0].Rows[0]["LOB"].ToString() : "";

                string coachersigneddate, coacheesigneddate;

                if (ds_get_review.Tables[0].Rows[0]["RequireSignOff"] != DBNull.Value && Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["RequireSignOff"]) == 0)
                {
                    RadCoacherSignOff.Visible = false;
                    RadCoacherSignOff.Enabled = false;
                    lblstatus.Visible = true;
                    lblstatus.Text = "*Coach: " + " " + createdByName + " has saved this review on " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["CreatedOn"]);
                }
                else
                {

                    if (ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"] is DBNull)
                    {
                        coacheesigneddate = "";
                    }
                    else
                    {
                        coacheesigneddate = ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"].ToString();
                    }

                    if (ds_get_review.Tables[0].Rows[0]["coachersigneddate1"] is DBNull)
                    {
                        coachersigneddate = "";
                    }
                    else
                    {
                        coachersigneddate = ds_get_review.Tables[0].Rows[0]["coachersigneddate1"].ToString();
                    }

                    if (coacheesigned == 1) //if coacheesigned off
                    {
                        if (coachersigned == 1) // if coachersigned off 
                        {
                            lblstatus.Text = "*Coach: " + " " + createdByName + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                             " Coach Sign off date : " + coachersigneddate + " Coachee Sign off date : " + coacheesigneddate;
                        }
                        else if (coachersigned == 0) //if no coacher sign off
                        {
                            lblstatus.Text = "*Needs sign off from Coach :" + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                             " Coachee Sign off date : " + coacheesigneddate;
                        }


                    }
                    else if (coachersigned == 1) //if coachersigned off
                    {
                        if (coacheesigned == 1) //   if no coachee sign off
                        {
                            lblstatus.Text = "*Coach: " + " " + createdByName + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                              " Coach Sign off date : " + coachersigneddate + " Coachee Sign off date : " + coacheesigneddate;
                        }
                        else if (coacheesigned == 0) //   if no coachee sign off
                        {
                            lblstatus.Text = "*Needs sign off from Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + "." + "Coach: " + " " + createdByName + " already signed off." +
                               " Coach Sign off date : " + coachersigneddate;
                        }
                    }

                    // added based on jira/cv-58 (francis.valera/08092018)
                    else if ((coacheesigned == 0) && (coacheesigned == 0))
                    {
                        lblstatus.Text = "*Needs sign off from Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " and " + "Coach: " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Supervisor name"].ToString());

                    }
                }

                // test formtype regardless of review type (francis.valera/07162018)
                string Acct = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                if (Acct == "1")
                {
                    RevNex1.Visible = true;
                    RevOD2.Visible = false;

                }
                else if (Acct == "2")
                {
                    RevNex1.Visible = true;
                    RevOD2.Visible = false;
                }
                else
                {
                    RevOD2.Visible = true;
                    RevNex1.Visible = false;
                }

                // fixed proceed to audit button visibility (francis.valera/07172018)
                // additional restriction for audit button (francis.valera/07192018)
                int nAuditor = Convert.ToInt32(ds3x.Tables[0].Rows[0]["Reports_To"].ToString());
                int nCreatedBy = (int)ds_get_review.Tables[0].Rows[0]["createdby"];
                int nCoacheeID = (int)ds_get_review.Tables[0].Rows[0]["coacheeid"];
                int nReviewTypeID = (int)ds_get_review.Tables[0].Rows[0]["reviewtypeid"];

                if (nCreatedBy != intcim || nCoacheeID != intcim)
                {
                    // additional visibility restriction from coacheesigned and coachersigned (francis.valera/07272018)
                    // if (nReviewTypeID == 1 && audited == 0 && saved == 0)
                    if (nReviewTypeID == 1 && audited == 0 && saved == 0 && coacheesigned == 1 && coachersigned == 1)
                    {
                        if (nAuditor == intcim)
                        {
                            btn_Audit1.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "loadcoacheedetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataAccess ws = new DataAccess();
                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                RadCoacheeSignOff.Visible = false;
                //string myStringVariable = "Coachee has signed off from this review.";
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "", "", "");

                string ModalLabel = "Coachee has signed off from this review.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));
            }
            catch (Exception ex)
            {
                string ModalLabel = "loadcoacheedetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataAccess ws = new DataAccess();
                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 1);
                RadCoacherSignOff.Visible = false;
                //string myStringVariable = "Coach has signed off from this review.";
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "", "", "");

                string ModalLabel = "Coach has signed off from this review.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));
            }
            catch (Exception ex)
            {
                string ModalLabel = "loadcoacheedetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void btn_Audit1_Click(object sender, EventArgs e)
        {
            btn_Audit1.Attributes.Add("onclick", " CloseWindow();");

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            string coachid = "";
            int reviewid;
            coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(DataHelper.Decrypt(coachid));
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);
            string val1 = Convert.ToString(reviewid);

            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

            Response.Redirect("~/AddTriadCoaching.aspx?CoachingTicket=" + coachid + "&ReviewType=" + val2);

        }

        
    }
}