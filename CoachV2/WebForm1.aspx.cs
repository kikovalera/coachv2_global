﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;

namespace CoachV2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public List<OD> lstOD
        {
            get
            {
                if (Session["lstOD"] != null)
                {
                    return (List<OD>)Session["lstOD"];
                }
                else
                {
                    return new List<OD>();
                }
            }
            set
            {
                Session["lstOD"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                List<OD> list = new List<OD>();
                lstOD = list;
                
                
            }

        }
        protected void RadGridN_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGridN.DataSource = lstOD;
            
        }
        protected void RadGridN_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                //saveAllData();
                lstOD.Insert(0, new OD() { ODID = Guid.NewGuid() });
                e.Canceled = true;
                RadGridN.Rebind();

            }
        }
        protected void saveAllData()
        {
            //Update Session
            foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            {
                Guid ODID = new Guid(item.GetDataKeyValue("ODID").ToString());
                OD emp = lstOD.Where(i => i.ODID == ODID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Change = (item.FindControl("RadChange") as RadTextBox).Text;
                emp.Behavior = (item.FindControl("RadBehavior") as RadTextBox).Text;
                emp.RootCause = (item.FindControl("RadRootCause") as RadTextBox).Text;
            }
        }
        protected void RadGridN_OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem insertitem = (GridDataItem)e.Item;
                RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                DataSet ds = new DataSet();
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIList();
                //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                combo.DataSource = ds;
                combo.DataTextField = "Name";
                combo.DataValueField = "KPIID";
                combo.DataBind();
                combo.ClearSelection();

                RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                DataSet dsTarget = null;
                DataAccess wsTarget = new DataAccess();
                dsTarget = wsTarget.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();

              

            }
           

        }
        protected void RadGridN_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadGridN.MasterTableView.IsItemInserted = true; 
                GridCommandItem commandItem = (GridCommandItem)RadGridN.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
        }
        protected void RadGridN_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridEditableItem edititem = (GridEditableItem)e.Item;
                RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI");
                RadComboBoxItem selectedItem = new RadComboBoxItem();
                string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                if (a != "0")
                {
                    selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI"] = selectedItem.Value;
                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIList();
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    comboBrand.SelectedValue = selectedItem.Value;
                }


                RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                DataSet dsTarget = null;
                DataAccess wsTarget = new DataAccess();
                dsTarget = wsTarget.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                if (b != "0")
                {
                    selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                    combodriver.Items.Add(selectedItemdriver);
                    selectedItemdriver.DataBind();
                    Session["Driver"] = selectedItemdriver.Value;
                    RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                    DataSet dsdriver = new DataSet();
                    DataAccess wsdriver = new DataAccess();
                    dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                    comboBranddriver.DataSource = dsdriver;
                    comboBranddriver.DataTextField = "Description";
                    comboBranddriver.DataValueField = "Driver_ID";
                    comboBranddriver.DataBind();
                    comboBranddriver.SelectedValue = selectedItemdriver.Value;
                }
            }
        }


        public void RadKPI_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }

        }
        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {

            foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            {
                //GridEditableItem item = (GridEditableItem)e.Item;
                Guid ODID = new Guid(item.GetDataKeyValue("ODID").ToString());
                OD emp = lstOD.Where(i => i.ODID == ODID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Change = (item.FindControl("RadChange") as RadNumericTextBox).Text;
                //RadTextBox LblBehavior = item.FindControl("LblBehavior") as RadTextBox;
                //RadTextBox LblRootCause = item.FindControl("LblRootCause") as RadTextBox;
                //LblBehavior.Display = true;
                //LblRootCause.Display = true;

                if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
                {
                    GridEditableItem item2 = (GridEditableItem)e.Item;
                    //Guid ODID = new Guid(item.GetDataKeyValue("ODID").ToString());
                    //OD emp = lstOD.Where(i => i.ODID == ODID).First();
                    emp.Behavior = (item2.FindControl("RadBehavior") as RadTextBox).Text;
                    emp.RootCause = (item2.FindControl("RadRootCause") as RadTextBox).Text;




                }
            }
            //GridCommandItem commandItem = (GridCommandItem)RadGridN.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            //commandItem.FireCommandEvent("InitInsert", null);
            //foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            //{
            //    item.Edit = true;
            //}
            //RadGridN.MasterTableView.IsItemInserted = true;
            

            //here bind Listview after data save in ViewState  
            


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            GridCommandItem commandItem = (GridCommandItem)RadGridN.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            commandItem.FireCommandEvent("InitInsert", null);
            foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            {
                item.Edit = true;
            }
        }
        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            {
                //GridEditableItem item = (GridEditableItem)e.Item;
                Guid ODID = new Guid(item.GetDataKeyValue("ODID").ToString());
                OD emp = lstOD.Where(i => i.ODID == ODID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Change = (item.FindControl("RadChange") as RadNumericTextBox).Text;
                //RadTextBox LblBehavior = item.FindControl("LblBehavior") as RadTextBox;
                //RadTextBox LblRootCause = item.FindControl("LblRootCause") as RadTextBox;
                //LblBehavior.Display = true;
                //LblRootCause.Display = true;

                if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
                {
                    GridEditFormItem updateItem = (GridEditFormItem)e.Item;
                    emp.Behavior = (updateItem.FindControl("RadBehavior") as RadTextBox).Text;
                    emp.RootCause = (updateItem.FindControl("RadRootCause") as RadTextBox).Text;
                }
            }
            //GridCommandItem commandItem = (GridCommandItem)RadGridN.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            //commandItem.FireCommandEvent("InitInsert", null);
            //foreach (GridDataItem item in RadGridN.MasterTableView.Items)
            //{
            //    item.Edit = true;
            //}
        }
    }
}
