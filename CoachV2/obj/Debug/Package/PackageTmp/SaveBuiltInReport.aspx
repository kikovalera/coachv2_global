﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaveBuiltInReport.aspx.cs" Inherits="CoachV2.SaveBuiltInReport"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet" />
    <link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="libs/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" />
    <!-- MetisMenu CSS -->
    <link href="libs/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <!-- Timeline CSS -->
    <link href="Content/dist/css/timeline.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="Content/dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="Content/dist/css/jquery-ui.css" rel="stylesheet" />

    <!-- Morris Charts CSS -->
    <link href="libs/morrisjs/morris.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link href="libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script src="libs/js/testjs.js" type="text/javascript"></script>
    <style type="text/css">
        .RadContextMenu1
        {
            top: 268px;
            left: 602px;
        }
    </style>
</head>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function openModal(header, message) {
                $('#myModal').modal('show');
                $('.manager').text(header);
                $('.modal-body').text(message);
            }
           
        </script>
    </telerik:RadScriptBlock>
        <script type="text/javascript">
            $(window).load(function () {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");
            });

    </script>

         
<body>

<div class="se-pre-con"></div>
    <form id="form1" runat="server">  
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <div class="panel menuheadercustom" runat="server" id="myreviewtitle">
            <div style="text-align: center;">
                <span class="breadcrumb2ndlevel">
                    <asp:Label ID="Label1" runat="server" Text="Save Report"></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
                        ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span></div>
        </div>
        <div class="panel-body">
            <asp:HiddenField ID="HiddenReportID" Value="0" runat="server" />
            <asp:HiddenField ID="HiddenUserID" Value="0" runat="server" />
            <telerik:RadTextBox ID="RadReportName" runat="server" PlaceHolder="Report Name" Width="100%">
                                            </telerik:RadTextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" 
                                                ControlToValidate="RadReportName" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>

        </div>
        <div>
            &nbsp;</div>
        <div>
            &nbsp;</div>
        <div style="text-align: center;">
            <telerik:RadButton ID="RadBack" runat="server" Text="Back" CssClass="btn btn-info btn-small"
                ForeColor="White" OnClick="RadBack_Click">
            </telerik:RadButton>
            <telerik:RadButton ID="RadSave" runat="server" Text="Save" CssClass="btn btn-info btn-small"
                ForeColor="White" OnClick="RadSave_Click">
            </telerik:RadButton>
            <%--<telerik:RadButton EnableSplitButton="true" ID="RadButton1" runat="server" Text="Export"
                ForeColor="White" OnClick="RadButton1_Click" OnClientClicked="OnClientClicked">
            </telerik:RadButton>
            <telerik:RadContextMenu ID="RadContextMenu1" runat="server" OnItemClick="RadMenu1_ItemClick"
                CssClass="RadContextMenu1">
                <Items>
                    <telerik:RadMenuItem Text="CSV">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem Text="EXCEL">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadContextMenu>--%>
        </div>
        <div>
            &nbsp;</div>
        <div>
            &nbsp;</div>
    <div id="myModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            &times;</button>
                        <h4 id="ModalHeader" class="manager">
                            </h4>
                    </div>
                    <div class="modal-body">
                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript" src="libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="libs/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Morris Charts JavaScript -->
    <!--<script type="text/javascript" src="libs/raphael/raphael-min.js"></script>
    <script type="text/javascript" src="libs/morrisjs/morris.min.js"></script>
    <script type="text/javascript" src="Content/js/morris-data.js"></script>-->

     <script type="text/javascript" src="libs/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="libs/flot/jquery.flot.time.js"></script>
    <script type="text/javascript" src="libs/flot/jquery.flot.categories.js"></script>
    
    <!-- <script type="text/javascript" src="Content/js/morris-data.js"></script>-->
    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="libs/js/sb-admin-2.js"></script>
    <!-- Custom JavaScript -->
    
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript" src="Content/js/customjsfiles.js"></script>
    </form>
</body>
</html>
