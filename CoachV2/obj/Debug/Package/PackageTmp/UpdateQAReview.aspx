﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateQAReview.aspx.cs" Inherits="CoachV2.UpdateQAReview" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
  <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
 <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadCommitment" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadSave" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblstatus" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server"></telerik:RadAjaxPanel>
    <div class="menu-content bg-alt">
        <asp:Panel ID="pn_coacheedetails" Visible="false" runat="server">
            <div class="panel menuheadercustom">
                <div>
                    &nbsp;<span><asp:Label ID="Label3" runat="server" Text=""></asp:Label></span><span><asp:Label
                        ID="Label1" runat="server" Text=""></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
                            ID="Label2" runat="server" Text=""></asp:Label></span>
                </div>
            </div>
            <div class="panel-group" id="Div1">
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                </span>
                            </h4>
                        </div>
                    </a>
                    <div id="Div2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="Account" class="control-label col-sm-3">
                                    Coaching Ticket</label>
                                <div class="col-sm-3">
                                    <telerik:RadTextBox ID="RadReviewID" runat="server" class="form-control" Text="0"
                                        ReadOnly="true" Enabled="false" Width="100%">
                                    </telerik:RadTextBox>
                                </div>
                                <label for="Account" class="control-label col-sm-3">
                                    Coaching Date</label>
                                <div class="col-sm-3">
                                    <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                        Text="0" ReadOnly="true">
                                    </telerik:RadTextBox>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="container-fluid" id="Div6" runat="server">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:HiddenField ID="FakeURLID" runat="server" />
                                        <div id="profileimageholder">
                                            <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Name: </b><span>
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span></h5>
                                        <h5>
                                            <b>Position: </b><span>
                                                <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Site: </b><span>
                                                <asp:Label ID="LblSite" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Reporting Manager: </b><span>
                                                <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Region: </b><span>
                                                <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Country: </b><span>
                                                <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Department: </b><span>
                                                <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Client: </b><span>
                                                <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Campaign: </b><span>
                                                <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b></b><span>
                                                <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Session Type: </b><span>
                                                <asp:Label ID="LblSessionType" runat="server" Text="My Session Type" /></span></h5>
                                        <h5>
                                            <b>Topic: </b><span>
                                                <asp:Label ID="LblSessionTopic" runat="server" Text="My Session Type" /></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div id="PanelQARev" runat="server">
        </div>
        <div class="panel menuheadercustom">
            <span class="breadcrumb2ndlevel">
                <asp:Label ID="TLLabel" runat="server" Text="Supervisor Section"></asp:Label></span>
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Commitment <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseSix" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadCommitment" runat="server" class="form-control" placeholder="Commitment"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" Enabled="false">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="AddSave"
                        ControlToValidate="RadCommitment" Display="Dynamic" ErrorMessage="*This is a required field."
                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <br />
            <div class="col-sm-12">
                <span class="addreviewbuttonholder">
                    <telerik:RadButton ID="RadSave" runat="server" Text="Save" CssClass="btn btn-info btn-small"
                        ValidationGroup="AddSave" ForeColor="White" OnClick="RadSave_Click">
                    </telerik:RadButton>
                </span>
                <br />
                <br />
                <br />
            </div>
        </div>

        <asp:Label ID="lblstatus" runat="server"  ForeColor="Red" Visible="true"></asp:Label>
    </div> 
    
</asp:Content>
