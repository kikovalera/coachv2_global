﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CMTPreview.aspx.cs" Inherits="CoachV2.CMTPreview" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript" src="libs/js/JScript1.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
    <link href="libs/css/gridview.css" rel="stylesheet" type="text/css" />
    <link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"">
    var objChkd;
    function HandleOnCheck() {
        var chkLst = document.getElementById('CBList');
        if (objChkd && objChkd.checked)
            objChkd.checked = false;
        objChkd = event.srcElement;
    }   
</script>
<telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= RadSearchBlackout.UniqueID %>") {
                    args.set_enableAjax(false);
                }
//                if (args.get_eventTarget() == "<%= RadCMTSaveSubmit.UniqueID %>") {
//                    args.set_enableAjax(false);
//                }
            }
        </script>
        <script type="text/javascript">
            function openCMT() {
                $('#myCMTPop').modal('show');
            }
            function closeCMT() {
                $('#myCMTPop').modal('hide');
            }
            
        </script>
    </telerik:RadScriptBlock>
<%--   <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
     <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="RadSearchBlackout">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="HiddenSearchBlackout" />
                    <telerik:AjaxUpdatedControl ControlID="CMTButtons" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadCMTSaveSubmit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CMTView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
 
        </AjaxSettings>
    </telerik:RadAjaxManager> --%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadMajorCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadInfractionClass" LoadingPanelID="RadAjaxLoadingPanel1" />
                     <telerik:AjaxUpdatedControl ControlID="CMTButtons" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
<%--            <telerik:AjaxSetting AjaxControlID="RadCMTSaveSubmit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CMTView" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="conditionalPostback"></telerik:RadAjaxPanel>
    <asp:Panel ID="pnlMain" runat="server">
        <div class="menu-content bg-alt">
            <div class="panel menuheadercustom">
                <div>
                    &nbsp;<span><asp:Label ID="Label3" runat="server" Text=""></asp:Label></span><span><asp:Label
                        ID="Label1" runat="server" Text=""></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
                            ID="Label2" runat="server" Text=""></asp:Label></span>
                </div>
            </div>
            <div class="panel-group" id="Div1">
                <div class="panel panel-custom">
                    <div class="panel panel-custom">
                        <a data-toggle="collapse" data-parent="#accordion" href="#Div2">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                    </span>
                                </h4>
                            </div>
                        </a>
                        <div id="Div2" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        Coaching Ticket:<asp:Label ID="RadReviewID" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:Label ID="LblSessionType" runat="server" Text="My Session Type" />
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:Label ID="LblSessionTopic" runat="server" Text="My Session Type" />
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="container-fluid" id="Div5" runat="server">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <asp:HiddenField ID="HiddenSearchBlackout" Value="0" runat="server" />
                                            <telerik:RadButton ID="RadSearchBlackout" runat="server" Text="Search Blackout" CssClass="btn btn-info btn-small"
                                                ForeColor="White" Visible="true" OnClick="RadSearchBlackout_Click" Font-Size="Smaller">
                                            </telerik:RadButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid" id="Div6" runat="server">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="profileimageholder1">
                                                <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <h5>
                                                <b>Name: </b><span>
                                                    <asp:Label ID="LblFullName" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Position: </b><span>
                                                    <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Site: </b><span>
                                                    <asp:Label ID="LblSite" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Reporting Manager: </b><span>
                                                    <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Region: </b><span>
                                                    <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Country: </b><span>
                                                    <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Department: </b><span>
                                                    <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Client: </b><span>
                                                    <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b>Campaign: </b><span>
                                                    <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                            <h5>
                                                <b></b><span>
                                                    <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="CMTView" runat="server">
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#HRComments">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                HR Comments <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                            </h4>
                        </div>
                    </a>
                    <div id="HRComments" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <label for="Previous Perfomance Results">
                                Comments Section</label><br />
                            <div style="height: 10px">
                            </div>
                            <hr />
                            <telerik:RadTextBox ID="RadHRComments" runat="server" class="form-control" placeholder="HR Comments"
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ValidationGroup="AddReview"
                                ForeColor="Red" ControlToValidate="RadHRComments" Display="Dynamic" ErrorMessage="*This is a required field."
                                CssClass="validator"></asp:RequiredFieldValidator>
                            <asp:Panel ID="Panel5" runat="server">
                                <div id="Div3" runat="server">
                                    <div id="Div4" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                                Previous Coaching Notes</label><br />
                                            <div style="height: 5px">
                                            </div>
                                            <div>
                                                <div class="panel-body">
                                                    <telerik:RadGrid ID="grd_Coaching_Notes" CssClass="RemoveBorders" RenderMode="Lightweight"
                                                        runat="server" AutoGenerateColumns="true">
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="True" />
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <label for="Previous Perfomance Results" class="control-label col-xs-5 col-md-3">
                                                Previous Perfomance Results</label><br />
                                            <div style="height: 5px">
                                            </div>
                                            <div>
                                                <div class="panel-body">
                                                    <telerik:RadGrid ID="RadGridPRCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                        <MasterTableView>
                                                            <Columns>
                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                                                            </Scrolling>
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentationCMT">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                            </h4>
                        </div>
                    </a>
                    <div id="collapseDocumentationCMT" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                CssClass="RemoveBorders" OnItemDataBound="RadGridDocumentation_onItemDatabound"
                                BorderStyle="None" GridLines="None">
                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                        </telerik:GridHyperLinkColumn>
                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                        </telerik:GridHyperLinkColumn>
                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div>
                                &nbsp;
                            </div>
                            <div id="CMTUpload" runat="server">
                                <div class="col-sm-12">
                                    <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" MultipleFileSelection="Automatic"
                                        OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf"
                                        MaxFileSize="1000000" Width="40%" PostbackTriggers="RadCMTSaveSubmit,RadCMTPreview">
                                        <Localization Select="                             " />
                                    </telerik:RadAsyncUpload>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#NTE">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                NTE Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                            </h4>
                        </div>
                    </a>
                    <div id="NTE" class="panel-collapse collapse in">
                        <div style="height: 30px; line-height: 30px; text-align: center;">
                            -HR Section-
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        HR Representative:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadNumericTextBox ID="RadHRRep" runat="server" EmptyMessage="-Type CIM-"
                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="" Enabled="false">
                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                        </telerik:RadNumericTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="Supervisor">
                                        Tandim/Sap Reference #:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadTextBox ID="RadSapReference" runat="server" EmptyMessage="-Type CIM"
                                            Class="form-control" Width="75%">
                                        </telerik:RadTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Case Level:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadCaseLevel" DropDownAutoWidth="Enabled" runat="server"
                                            class="form-control" Width="75%" EmptyMessage="-Select-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Major Category:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadMajorCategory" DropDownAutoWidth="Enabled" AutoPostBack="true"
                                            runat="server" class="form-control" Width="75%" EmptyMessage="-Select-" OnSelectedIndexChanged="RadMajorCategory_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Infraction Class:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadInfractionClass" DropDownAutoWidth="Enabled" runat="server"
                                            class="form-control" Width="75%" EmptyMessage="-Select-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        Status:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadStatus" runat="server" class="form-control" Width="75%"
                                            EmptyMessage="-Select-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="Supervisor">
                                        NTE draft submitted for HR Approval:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadNTEDraft" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        NTE draft Approval Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadNTEApproval" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        NTE Issue Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadNTEIssueDate" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div style="height: 30px; line-height: 30px; text-align: center;">
                            -HR and Immediate Superior Section-
                        </div>
                        <div>
                            &nbsp;
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        With hold case?</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadWithHoldCase" DropDownAutoWidth="Enabled" runat="server"
                                            class="form-control" Width="75%" EmptyMessage="Yes/No">
                                            <Items>
                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Hold Start Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadHoldStartDate" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Hold End Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadHoldEndDate" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        Hold Case Type:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadHoldCaseType" runat="server" class="form-control" Width="75%"
                                            EmptyMessage="-Select-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <telerik:RadTextBox ID="RadHRImmediateComments" runat="server" class="form-control"
                                            placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                            Rows="5" TabIndex="7">
                                        </telerik:RadTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                        </div>
                        <div style="height: 30px; line-height: 30px; text-align: center;">
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        Employee Accepts?</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadEmployeeAccepts" DropDownAutoWidth="Enabled" runat="server"
                                            class="form-control" Width="75%" EmptyMessage="Yes/No">
                                            <Items>
                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Sign Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadEmployeeAcceptDate" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Witness:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadNumericTextBox ID="RadWitness1" runat="server" EmptyMessage="-Type CIM-"
                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                        </telerik:RadNumericTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Sign Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadSignDate1" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Witness:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadNumericTextBox ID="RadWitness2" runat="server" EmptyMessage="-Type CIM-"
                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                        </telerik:RadNumericTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-6">
                                        Sign Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadSignDate2" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        Non-acceptance reason:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadComboBox ID="RadNonAcceptance" runat="server" class="form-control" Width="75%"
                                            EmptyMessage="-Select-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <telerik:RadTextBox ID="RadNonAcceptanceNotes" runat="server" class="form-control"
                                            placeholder="Notes (if applicable)" TextMode="MultiLine" RenderMode="Lightweight"
                                            Rows="5" TabIndex="7" Width="100%">
                                        </telerik:RadTextBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                        </div>
                        <div style="height: 30px; line-height: 30px; text-align: center;">
                            -HR Section-
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        RTNTE Receive Date:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadRTNTEReceiveDate" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#PSD">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Preventive Suspension Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                </span>
                            </h4>
                        </div>
                    </a>
                    <div id="PSD" class="panel-collapse collapse in">
                        <div style="height: 30px; line-height: 30px; text-align: center;">
                            -HR Section-
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">
                                        With Suspension?</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadWithSuspension" DropDownAutoWidth="Enabled" runat="server"
                                            class="form-control" Width="100%" EmptyMessage="Yes/No">
                                            <Items>
                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Supervisor">
                                        Admin Hearing:</label>
                                    <div class="col-sm-8">
                                        <telerik:RadDatePicker ID="RadAdminHearingDate1" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="60%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                        <asp:Label ID="LblSchedule1" runat="server" Text="Schedule 1"></asp:Label>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                    </div>
                                    <div class="col-sm-8">
                                        <telerik:RadDatePicker ID="RadAdminHearingDate2" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="60%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                        <asp:Label ID="LblSchedule2" runat="server" Text="Schedule 2"></asp:Label>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                </form>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="email">
                                        Admin hearing conducted on:</label>
                                    <div class="col-sm-6">
                                        <telerik:RadDatePicker ID="RadAdminHearingConduct" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#NOD">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                NOD Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                            </h4>
                        </div>
                    </a>
                    <div id="NOD" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                -HR Section-</div>
                            <div>
                                <asp:CheckBoxList ID="CBList" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal"
                                    Onclick="return HandleOnCheck()" CssClass="chkChoice">
                                    <asp:ListItem Value="1" Text="Managerial Termination"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Client Escalation"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Non MT/CE"></asp:ListItem>
                                </asp:CheckBoxList>
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="email">
                                            NOD submission for approval:</label>
                                        <telerik:RadDatePicker ID="RadNODSubmittedDate" runat="server" placeholder="Date"
                                            class="form-control" TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="email">
                                            NOD approval Date:</label>
                                        <telerik:RadDatePicker ID="RadNODApproval" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="email">
                                            NOD issue date:</label>
                                        <telerik:RadDatePicker ID="RadNODIssueDate" runat="server" placeholder="Date" class="form-control"
                                            TabIndex="6" Width="75%">
                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                            </Calendar>
                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                TabIndex="6">
                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                <EnabledStyle Resize="None"></EnabledStyle>
                                            </DateInput>
                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                        </telerik:RadDatePicker>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                            <div style="height: 5px">
                            </div>
                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                -HR and Immediate Superior Section-
                            </div>
                            <div>
                                &nbsp;
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            With hold case?</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadNODWithHoldCase" DropDownAutoWidth="Enabled" runat="server"
                                                class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                <Items>
                                                    <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                    <telerik:RadComboBoxItem Value="2" Text="No" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Hold Start Date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODHoldStartDate" runat="server" placeholder="Date"
                                                class="form-control" TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Hold End Date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODHoldEndDate" runat="server" placeholder="Date" class="form-control"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            Hold Case Type:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadNODHoldCaseType" runat="server" class="form-control"
                                                Width="75%" EmptyMessage="-Select-">
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <telerik:RadTextBox ID="RadNODHrSuperiorNotes" runat="server" class="form-control"
                                                placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                                Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                            </div>
                            <div style="height: 30px; line-height: 30px; text-align: center;">
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            Employee Accepts?</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadNODEmployeeAccept" DropDownAutoWidth="Enabled" runat="server"
                                                class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                <Items>
                                                    <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                    <telerik:RadComboBoxItem Value="2" Text="No" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Sign Date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODEmployeeSignDate" runat="server" placeholder="Date"
                                                class="form-control" TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Witness:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadNumericTextBox ID="RadNODWitness1" runat="server" EmptyMessage="-Type CIM-"
                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                            </telerik:RadNumericTextBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Sign Date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODWitness1SignDate" runat="server" placeholder="Date"
                                                class="form-control" TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Witness:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadNumericTextBox ID="RadNODWitness2" runat="server" EmptyMessage="-Type CIM-"
                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                            </telerik:RadNumericTextBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-6">
                                            Sign Date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODWitness2SignDate" runat="server" placeholder="Date"
                                                class="form-control" TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            Non-acceptance reason:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadNODNonAcceptance" runat="server" class="form-control"
                                                Width="100%" EmptyMessage="-Select-">
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <telerik:RadTextBox ID="RadNODNonAcceptanceNotes" runat="server" class="form-control"
                                                placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                                Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                            </div>
                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                -HR Section-
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            NOD return date:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadDatePicker ID="RadNODReturnDate" runat="server" placeholder="Date" class="form-control"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            Escalated by:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadNumericTextBox ID="RadEscalatedBy" runat="server" EmptyMessage="-Type CIM-"
                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                            </telerik:RadNumericTextBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            Case Decision:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadCaseDecision" runat="server" class="form-control" Width="100%"
                                                EmptyMessage="-Select-">
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div>
                            <telerik:RadTextBox ID="RadNODCaseDecisionNotes" runat="server" class="form-control"
                                placeholder="State additional note for the case decision" TextMode="MultiLine"
                                Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                            </telerik:RadTextBox>
                            <div style="height: 20px">
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">
                                            End-of-week action required:</label>
                                        <div class="col-sm-6">
                                            <telerik:RadComboBox ID="RadEoW" runat="server" class="form-control" Width="100%"
                                                EmptyMessage="-Select-">
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                            </div>
                            <telerik:RadTextBox ID="RadNODEOWNotes" runat="server" class="form-control" placeholder="Action required"
                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="container-fluid" id="CMTButtons" runat="server">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <telerik:RadButton ID="RadCMTPreview" runat="server" Text="Preview" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadCMTPreview_Click">
                                </telerik:RadButton>
                                <telerik:RadButton ID="RadCMTSaveSubmit" runat="server" Text="Submit" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadCMTSaveSubmit_Click">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>

    <div id="myCMTPop" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 id="H1" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="panel menuheadercustom">
                            <div style="text-align: center;">
                                <h4>
                                    Activate Search Blackout
                                </h4>
                            </div>
                        </div>
                        <div class="panel-group" id="Div7">
                            <div class="row row-custom">
                                <div class="col-sm-12">
                                    <div style="text-align: center;">
                                        <h5>
                                            You have created a For Termination Progressive Discipline Review.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            To keep the information on this review from being searched and/or accessed by anyone
                                            other than the review creator, it is recommended to activate the Search Blackout
                                            feature.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            Would you like to activate the Search Blackout feature?
                                        </h5>
                                        <br></br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div style="text-align: center;">
                                <telerik:RadButton ID="RadSearchBlackOutYes" runat="server" Text="Yes" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutYes_Click">
                                </telerik:RadButton>
                                <telerik:RadButton ID="RadSearchBlackOutNo" runat="server" Text="No" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutNo_Click">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
