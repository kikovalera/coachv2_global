﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AddReview.aspx.cs" Inherits="CoachV2.AddReview" %>

<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/DashboardMyReviews.ascx" TagName="DashboardMyReviewsUserControl"
    TagPrefix="ucdash5" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript" src="libs/js/JScript1.js"></script>
    <link href="libs/css/gridview.css" rel="stylesheet" type="text/css" />
    <link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        var objChkd;

        function HandleOnCheck() {

            var chkLst = document.getElementById('CBList');
            if (objChkd && objChkd.checked)
                objChkd.checked = false;
            objChkd = event.srcElement;
        }

        var objChkd2;

        function HandleOnCheck2() {

            var chkLst2 = document.getElementById('CheckBoxList1');


            if (objChkd2 && objChkd2.checked)
                objChkd2.checked = false;

            objChkd2 = event.srcElement;


        }


    </script>
    <telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">

            function ValidateModuleList(source, args) {
                var chkListModules = document.getElementById('<%= CheckBoxList1.ClientID %>');
                var chkListinputs = chkListModules.getElementsByTagName("input");
                for (var i = 0; i < chkListinputs.length; i++) {
                    if (chkListinputs[i].checked) {
                        args.IsValid = true;
                        return;
                    }
                }
                args.IsValid = false;
            }

            function openQuickActionRadWindow() {
                var oWnd = $find("<%=Window1.ClientID%>");
                oWnd.show();
                window.setTimeout(function () {
                    oWnd.close();
                }, 10000);
            }

            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btn_ExporttoPDF.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= CheckBox1.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= CheckBox2.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= RadCoacheeSignOff.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= UploadDummy.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= CyborgCMT.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= RadSessionTopic.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= RadCMTSaveSubmit.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= RadSaveBtn.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }

            function BtnDummy(sender, args) {
                var clickButton = document.getElementById("<%= UploadDummy.ClientID %>");
                clickButton.click();
            }
            function CyborgCMT(sender, args) {
                var clickButton = document.getElementById("<%= CyborgCMT.ClientID %>");
                clickButton.click();
            }
        </script>
        <script type="text/javascript">

            function openInc() {
                $('#myInc').modal('show');
            }
            function closeModal() {
                $('#myInc').modal('hide');
            }
            function openCMT() {
                $('#myCMTPop').modal('show');
            }
            function closeCMT() {
                $('#myCMTPop').modal('hide');
            }
            function openCMTv2() {
                $('#myCMTPopv2').modal('show');
            }
            function closeCMTv2() {
                $('#myCMTPopv2').modal('hide');
            }
            function openCMTv3() {
                $('#myCMTPopv3').modal('show');
            }
            function closeCMTv3() {
                $('#myCMTPopv3').modal('hide');
            }
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAccount">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSupervisor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadCoacheeName" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadSite" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadCampaign" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadDivision" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadLOB" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadSupervisor" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CNPRPanel" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel9" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Checkboxes" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMajorCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadInfractionClass" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSearchBlackout">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CMTButtons" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="HiddenSearchBlackout" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid4">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid4" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadKPI4">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid4" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButton2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid4" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid9">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid9" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadKPIOD">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid9" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadDataView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid9" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButton3">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid9" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton3" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacherSignOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadKPI">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButton1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadButton1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSSN">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBox2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CheckBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="conditionalPostback">
        <asp:Panel ID="pnlMain" runat="server">
            <div class="menu-content bg-alt">
                <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
                <div class="panel-group" id="accordion">
                    <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                    </span>
                                </h4>
                            </div>
                        </a>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body" style="padding-bottom: 5px">
                                <div class="form-group">
                                    <div class="col-sm-4 col-md-2 col-md-push-10">
                                        <div class="col-sm-2">
                                            <asp:HiddenField ID="FakeURLID" runat="server" />
                                            <asp:HiddenField ID="hfGridHtml" runat="server" />
                                            <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                                Visible="true" OnClick="btn_ExporttoPDF_Click">
                                                <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                            </telerik:RadButton>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-md-pull-2">
                                        <label for="Account" class="control-label col-sm-3">
                                            Coaching Ticket</label>
                                        <div class="col-sm-3">
                                            <telerik:RadTextBox ID="RadReviewID" Width="100%" runat="server" class="form-control"
                                                Text="0" ReadOnly="true">
                                            </telerik:RadTextBox>
                                        </div>
                                        <label for="Account" class="control-label col-sm-3">
                                            Coaching Date</label>
                                        <div class="col-sm-3">
                                            <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                                Text="0" ReadOnly="true">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="panel-body" style="padding-top: 5px">
                                <div class="form-group col-sm-5" id="SelectCoacheeGroup" runat="server">
                                    <form class="form-horizontal">
                                    <label for="name2" class="control-label">
                                        Select Coachee</label>
                                    <div id="CoacheeDetails" runat="server">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">
                                                Site:</label>
                                            <div class="col-sm-8">
                                                <telerik:RadComboBox ID="RadSite" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                    DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select Site-"
                                                    CausesValidation="True">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSite" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">
                                                Campaign:</label>
                                            <div class="col-sm-8">
                                                <telerik:RadComboBox ID="RadCampaign" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                    DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select Campaign-"
                                                    CausesValidation="True">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                                                    ControlToValidate="RadCampaign" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">
                                                Division:</label>
                                            <div class="col-sm-8">
                                                <telerik:RadComboBox ID="RadDivision" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                    DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select Division-"
                                                    CausesValidation="True">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                                                    ControlToValidate="RadDivision" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">
                                                LOB:</label>
                                            <div class="col-sm-8">
                                                <telerik:RadComboBox ID="RadLOB" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                    DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select LOB-"
                                                    CausesValidation="True">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ValidationGroup="AddReview"
                                                    ControlToValidate="RadLOB" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div id="DivAccount" runat="server">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">
                                                Account:</label>
                                            <div class="col-sm-8">
                                                <telerik:RadComboBox ID="RadAccount" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                    DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select Account-"
                                                    OnSelectedIndexChanged="RadAccount_SelectedIndexChanged" CausesValidation="True">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                                    ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="Supervisor">
                                            Supervisor:</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSupervisor" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                DropDownAutoWidth="Disabled" TabIndex="2" Width="100%" EmptyMessage="-Select Supervisor-"
                                                OnSelectedIndexChanged="RadSupervisor_SelectedIndexChanged" CausesValidation="True">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                                ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Coachee Name</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadCoacheeName" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                DropDownAutoWidth="Disabled" TabIndex="3" Width="100%" EmptyMessage="-Select Coachee-"
                                                OnSelectedIndexChanged="RadCoacheeName_SelectedIndexChanged" CausesValidation="True">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                                ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    </form>
                                </div>
                                <div class="form-group col-sm-5" id="SelectCoachingSpecifics" runat="server">
                                    <label for="name2" class="control-label">
                                        Coaching Specifics</label>
                                    <div class="form-group" id="CallID" runat="server">
                                        <label for="SessionType" class="control-label col-sm-4">
                                            Call ID</label>
                                        <div class="col-sm-8">
                                            <telerik:RadTextBox ID="RadCallID" runat="server" class="form-control" RenderMode="Lightweight"
                                                TabIndex="4" Width="100%" EmptyMessage="-Enter Call ID-" onkeypress="return isNumber(event)">
                                            </telerik:RadTextBox>
                                            <%--<telerik:RadTextBox ID="RadCallID" runat="server" class="form-control" RenderMode="Lightweight"
                                                TabIndex="4" Width="100%" EmptyMessage="-Enter Call ID-" CausesValidation="False"
                                                onkeypress="return isNumber(event)">
                                            </telerik:RadTextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="AddReview"
                                                ControlToValidate="RadCallID" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>--%>
                                            <%--validation removed for Call ID as requested (07102018/francis.valera)--%>
                                        </div>
                                    </div>
                                    <div id="CallIDDiv" runat="server">
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="SessionType" class="control-label col-sm-4">
                                            Session Type</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true"
                                                RenderMode="Lightweight" TabIndex="4" Width="100%" EmptyMessage="-Select Session Type-"
                                                OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RadSessionTypeRequiredFieldValidator13"
                                                ValidationGroup="AddReview" ControlToValidate="RadSessionType" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="*This is a Required field." CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="Topic" class="control-label col-sm-4">
                                            Topic</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                                TabIndex="5" AutoPostBack="true" Width="100%" EmptyMessage="-Select Session Topic-"
                                                OnSelectedIndexChanged="RadSessionTopic_SelectedIndexChanged">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RadSessionTopicRequiredFieldValidator14"
                                                ValidationGroup="AddReview" ControlToValidate="RadSessionTopic" ForeColor="Red"
                                                Display="Dynamic" ErrorMessage="*This is a Required field." CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group" id="SessionFocusGroup" runat="server">
                                        <label for="Topic" class="control-label col-sm-4">
                                            Session Focus</label>
                                        <div class="col-sm-8">
                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatLayout="Table" RepeatDirection="Vertical"
                                                Onclick="return HandleOnCheck2()">
                                            </asp:CheckBoxList>
                                            <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                                ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field." ValidationGroup="AddReview"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group" runat="server" id="CheckBoxes">
                                        <div class="col-sm-12">
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Include Previous Coaching Notes"
                                                AutoPostBack="true" OnCheckedChanged="CheckBox1_CheckedChanged" /><br />
                                            <asp:CheckBox ID="CheckBox2" runat="server" Text="Include Previous Performance Results"
                                                AutoPostBack="true" OnCheckedChanged="CheckBox2_CheckedChanged" /><br />
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group" runat="server" id="FollowUp">
                                        <label for="SessionType" class="control-label col-sm-4">
                                            Follow up Date</label>
                                        <div class="col-sm-8">
                                            <telerik:RadDatePicker ID="RadFollowup" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="100%" OnLoad="RadFollowup_Load">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="AddReview"
                                                ControlToValidate="RadFollowup" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                                <div class="form-group col-sm-2" id="SelectSearchBlackout" runat="server">
                                    <telerik:RadButton ID="RadSearchBlackout" runat="server" Text="Search Blackout" CssClass="btn btn-info btn-small"
                                        ForeColor="White" Visible="false" OnClick="RadSearchBlackout_Click" Font-Size="Smaller">
                                    </telerik:RadButton>
                                    <asp:HiddenField ID="HiddenSearchBlackout" Value="0" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="DefaultForm" runat="server">
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel1" runat="server">
                            <div id="AddReviewDefault" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Description"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" AutoCompleteType="None" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                                                <ClientEvents OnLoad="RadTextBoxLoad" />
                                            </telerik:RadTextBox>
                                            <%--                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                                                ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel3" runat="server">
                            <div id="Div1" runat="server">
                                <div class="panel panel-custom" id="PerfResult" runat="server">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Performance Result <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                </span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource"
                                                AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGrid1_ItemCommand"
                                                OnItemCreated="OnItemDataBoundHandler" OnItemDataBound="OnItemDataBound" AllowAutomaticDeletes="true"
                                                OnPreRender="RadGrid1_PreRender" ClientIDMode="AutoID" RenderMode="Lightweight">
                                                <MasterTableView DataKeyNames="UniqueID" CommandItemDisplay="Bottom" CellSpacing="0">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" FilterControlAltText=""
                                                            Text="Delete" UniqueName="DeleteColumn" Resizable="false" ConfirmText="Delete performance result?">
                                                            <HeaderStyle CssClass="rgHeader ButtonColumnHeader"></HeaderStyle>
                                                            <ItemStyle CssClass="ButtonColumn" />
                                                        </telerik:GridButtonColumn>
                                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="KPI">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadKPI" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                    DropDownAutoWidth="Enabled" OnSelectedIndexChanged="RadKPI_SelectedIndexChanged">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadKPIRadDriverRadTargetRequiredFieldValidator3"
                                                                    runat="server" ErrorMessage="*" ControlToValidate="RadKPI" Display="Dynamic"
                                                                    ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Target">
                                                            <ItemTemplate>
                                                                <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                                                </telerik:RadTextBox>
                                                                <asp:RequiredFieldValidator ID="RadTargetRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadTarget" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Current">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'>
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Previous">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'>
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Driver">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadDriverRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadDriver" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true" RenderMode="LightWeight">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <div>
                                                &nbsp;</div>
                                            <div class="col-md-10">
                                            </div>
                                            <div class="col-md-2">
                                                <telerik:RadButton ID="RadButton1" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                                                    ForeColor="White" ValidationGroup="Perf" OnClick="Button1_Click">
                                                </telerik:RadButton>
                                            </div>
                                            <asp:Panel ID="CNPRPanel" runat="server">
                                                <div id="CNPR" runat="server">
                                                    <div id="collapseCNPR" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                        <%--  <asp:HyperLink ID="ct" Text='<%# Eval("ReviewID") %>' runat="server"  NavigateUrl="~/AddMassCoaching.aspx" ></asp:HyperLink>--%>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridBoundColumn DataField="FormType" HeaderText="formtype" UniqueName="FormType"
                                                                                FilterControlToolTip="FormType" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReviewTypeID" HeaderText="ReviewTypeID" UniqueName="ReviewTypeID"
                                                                                FilterControlToolTip="ReviewTypeID" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                                <%--<telerik:GridTemplateColumn HeaderText="Review Type ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelADx" Text='<%# Eval("ReviewTypeID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Form Type">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelADxx" Text='<%# Eval("FormType") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>--%>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("Review KPI ID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("Review Date") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("Assigned By") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel2" runat="server">
                            <div id="AddReviewDefault2" runat="server">
                                <asp:Panel ID="DefaultPane" runat="server">
                                    <asp:Panel ID="DefaultPane1" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Strengths <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseFour" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strengths" onkeydown="setHeight(this,event)" style="overflow:hidden !important;"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9" AutoCompleteType="None">
                                                        <ClientEvents OnLoad="RadTextBoxLoad" />
                                                    </telerik:RadTextBox>
                                                    <%--                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ValidationGroup="AddReview"
                                                        ControlToValidate="RadStrengths" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                        CssClass="validator"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Opportunities <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseFive" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunities" onkeydown="setHeight(this,event)" style="overflow:hidden !important;"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="10" AutoCompleteType="None">
                                                        <ClientEvents OnLoad="RadTextBoxLoad" />
                                                    </telerik:RadTextBox>
                                                    <%--                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                                                        ControlToValidate="RadOpportunities" Display="Dynamic" ErrorMessage="*This is a required field."
                                                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Commitment <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseSix" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadCommitment" runat="server" class="form-control" placeholder="Commitment"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="11" AutoCompleteType="None">
                                                    </telerik:RadTextBox>
                                                    <%--                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                                                        ControlToValidate="RadCommitment" Display="Dynamic" ErrorMessage="*This is a required field."
                                                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                                <div style="height: 5px">
                                </div>
                                <asp:Panel ID="DefaultPane2" runat="server">
                                    <asp:Panel ID="DefaultPane3" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentationCMT">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseDocumentationCMT" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:Panel ID="DefaultPane4" runat="server">
                                                        <asp:Panel ID="DefaultPane5" runat="server">
                                                            <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                                CssClass="RemoveBorders" OnItemDataBound="RadDocumentationReview_onItemDatabound"
                                                                BorderStyle="None" GridLines="None">
                                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                                        </telerik:GridHyperLinkColumn>
                                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                                        </telerik:GridHyperLinkColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="True" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <telerik:RadAsyncUpload ID="RadAsyncUpload2" runat="server" MultipleFileSelection="Automatic"
                                                            OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf,.png,.xls.xlsx"
                                                            PostbackTriggers="UploadDummy" MaxFileSize="1000000" Width="40%">
                                                            <Localization Select="" />
                                                        </telerik:RadAsyncUpload>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel4" runat="server">
                            <asp:Panel ID="CMTView" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#HRComments">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                HR Comments <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="HRComments" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <label for="Previous Perfomance Results">
                                                Comments Section</label><br />
                                            <div style="height: 5px">
                                            </div>
                                            <telerik:RadTextBox ID="RadHRComments" runat="server" class="form-control" placeholder="HR Comments"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" AutoCompleteType="None">
                                            </telerik:RadTextBox>
                                            <%--                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ValidationGroup="AddReview"
                                                ForeColor="Red" ControlToValidate="RadHRComments" Display="Dynamic" ErrorMessage="*This is a required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>--%>
                                            <asp:Panel ID="Panel5" runat="server">
                                                <div id="Div2" runat="server">
                                                    <div id="Div3" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCNCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridBoundColumn DataField="FormType" HeaderText="formtype" UniqueName="FormType"
                                                                                FilterControlToolTip="FormType" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReviewTypeID" HeaderText="ReviewTypeID" UniqueName="ReviewTypeID"
                                                                                FilterControlToolTip="ReviewTypeID" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPRCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("Review KPI ID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("Review Date") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("Assigned By") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 5px">
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentation">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseDocumentation" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                CssClass="RemoveBorders" OnItemDataBound="RadGridDocumentation_onItemDatabound"
                                                BorderStyle="None" GridLines="None">
                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <div>
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-12">
                                                <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" MultipleFileSelection="Automatic"
                                                    OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf,.png,.xls,.xlsx"
                                                    MaxFileSize="1000000" Width="40%" PostbackTriggers="RadCMTPreview,CyborgCMT">
                                                    <Localization Select="                             " />
                                                </telerik:RadAsyncUpload>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#NTE">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                NTE Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="NTE" class="panel-collapse collapse in">
                                        <div style="height: 30px; line-height: 30px; text-align: center;">
                                            -HR Section-
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        HR Representative:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadNumericTextBox ID="RadHRRep" runat="server" EmptyMessage="-Type CIM-"
                                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="" Enabled="false">
                                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                        </telerik:RadNumericTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="Supervisor">
                                                        Tandim/Sap Reference #:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadTextBox ID="RadSapReference" runat="server" EmptyMessage="-Type CIM"
                                                            Class="form-control" Width="75%">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Case Level:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadCaseLevel" DropDownAutoWidth="Enabled" runat="server"
                                                            class="form-control" Width="75%" EmptyMessage="-Select-">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Major Category:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadMajorCategory" DropDownAutoWidth="Enabled" AutoPostBack="true"
                                                            runat="server" class="form-control" Width="75%" EmptyMessage="-Select-" OnSelectedIndexChanged="RadMajorCategory_SelectedIndexChanged">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Infraction Class:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadInfractionClass" DropDownAutoWidth="Enabled" runat="server"
                                                            class="form-control" Width="75%" EmptyMessage="-Select-">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        Status:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadStatus" runat="server" class="form-control" Width="75%"
                                                            EmptyMessage="-Select-">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="Supervisor">
                                                        NTE draft submitted for HR Approval:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadNTEDraft" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        NTE draft Approval Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadNTEApproval" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        NTE Issue Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadNTEIssueDate" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 30px; line-height: 30px; text-align: center;">
                                            -HR and Immediate Superior Section-
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        With hold case?</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadWithHoldCase" DropDownAutoWidth="Enabled" runat="server"
                                                            class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Hold Start Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadHoldStartDate" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Hold End Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadHoldEndDate" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        Hold Case Type:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadHoldCaseType" runat="server" class="form-control" Width="75%"
                                                            EmptyMessage="-Select-">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <telerik:RadTextBox ID="RadHRImmediateComments" runat="server" class="form-control"
                                                            placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                                            Rows="5" TabIndex="7">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                            </div>
                                        </div>
                                        <div style="height: 30px; line-height: 30px; text-align: center;">
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        Employee Accepts?</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadEmployeeAccepts" DropDownAutoWidth="Enabled" runat="server"
                                                            class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Sign Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadEmployeeAcceptDate" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Witness:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadNumericTextBox ID="RadWitness1" runat="server" EmptyMessage="-Type CIM-"
                                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                        </telerik:RadNumericTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Sign Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadSignDate1" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Witness:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadNumericTextBox ID="RadWitness2" runat="server" EmptyMessage="-Type CIM-"
                                                            Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                            <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                        </telerik:RadNumericTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="CoacheeName" class="control-label col-sm-6">
                                                        Sign Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadSignDate2" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        Non-acceptance reason:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadComboBox ID="RadNonAcceptance" runat="server" class="form-control" Width="75%"
                                                            EmptyMessage="-Select-">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <telerik:RadTextBox ID="RadNonAcceptanceNotes" runat="server" class="form-control"
                                                            placeholder="Notes (if applicable)" TextMode="MultiLine" RenderMode="Lightweight"
                                                            Rows="5" TabIndex="7" Width="100%">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                            </div>
                                        </div>
                                        <div style="height: 30px; line-height: 30px; text-align: center;">
                                            -HR Section-
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        RTNTE Receive Date:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadRTNTEReceiveDate" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#PSD">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Preventive Suspension Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                </span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="PSD" class="panel-collapse collapse in">
                                        <div style="height: 30px; line-height: 30px; text-align: center;">
                                            -HR Section-
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-4" for="email">
                                                        With Suspension?</label>
                                                    <div class="col-sm-8">
                                                        <telerik:RadComboBox ID="RadWithSuspension" DropDownAutoWidth="Enabled" runat="server"
                                                            class="form-control" Width="100%" EmptyMessage="Yes/No">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                                <telerik:RadComboBoxItem Value="2" Text="No" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-4" for="Supervisor">
                                                        Admin Hearing:</label>
                                                    <div class="col-sm-8">
                                                        <telerik:RadDatePicker ID="RadAdminHearingDate1" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="60%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                        <asp:Label ID="LblSchedule1" runat="server" Text="Schedule 1"></asp:Label>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <telerik:RadDatePicker ID="RadAdminHearingDate2" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="60%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                        <asp:Label ID="LblSchedule2" runat="server" Text="Schedule 2"></asp:Label>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                                </form>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6" for="email">
                                                        Admin hearing conducted on:</label>
                                                    <div class="col-sm-6">
                                                        <telerik:RadDatePicker ID="RadAdminHearingConduct" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#NOD">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                NOD Details <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="NOD" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                                -HR Section-</div>
                                            <div>
                                                <asp:CheckBoxList ID="CBList" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal"
                                                    Onclick="return HandleOnCheck()" CssClass="chkChoice">
                                                    <asp:ListItem Value="1" Text="Managerial Termination"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Client Escalation"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Non MT/CE"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="email">
                                                            NOD submission for approval:</label>
                                                        <telerik:RadDatePicker ID="RadNODSubmittedDate" runat="server" placeholder="Date"
                                                            class="form-control" TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="email">
                                                            NOD approval Date:</label>
                                                        <telerik:RadDatePicker ID="RadNODApproval" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="email">
                                                            NOD issue date:</label>
                                                        <telerik:RadDatePicker ID="RadNODIssueDate" runat="server" placeholder="Date" class="form-control"
                                                            TabIndex="6" Width="75%">
                                                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                FastNavigationNextText="&amp;lt;&amp;lt;">
                                                            </Calendar>
                                                            <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                TabIndex="6">
                                                                <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                <FocusedStyle Resize="None"></FocusedStyle>
                                                                <DisabledStyle Resize="None"></DisabledStyle>
                                                                <InvalidStyle Resize="None"></InvalidStyle>
                                                                <HoveredStyle Resize="None"></HoveredStyle>
                                                                <EnabledStyle Resize="None"></EnabledStyle>
                                                            </DateInput>
                                                            <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                        </telerik:RadDatePicker>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="height: 5px">
                                            </div>
                                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                                -HR and Immediate Superior Section-
                                            </div>
                                            <div>
                                                &nbsp;
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            With hold case?</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadNODWithHoldCase" DropDownAutoWidth="Enabled" runat="server"
                                                                class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                                <Items>
                                                                    <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                                    <telerik:RadComboBoxItem Value="2" Text="No" />
                                                                </Items>
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Hold Start Date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODHoldStartDate" runat="server" placeholder="Date"
                                                                class="form-control" TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Hold End Date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODHoldEndDate" runat="server" placeholder="Date" class="form-control"
                                                                TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            Hold Case Type:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadNODHoldCaseType" runat="server" class="form-control"
                                                                Width="75%" EmptyMessage="-Select-">
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <telerik:RadTextBox ID="RadNODHrSuperiorNotes" runat="server" class="form-control"
                                                                placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                                                Rows="5" TabIndex="7">
                                                            </telerik:RadTextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                            </div>
                                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            Employee Accepts?</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadNODEmployeeAccept" DropDownAutoWidth="Enabled" runat="server"
                                                                class="form-control" Width="75%" EmptyMessage="Yes/No">
                                                                <Items>
                                                                    <telerik:RadComboBoxItem Value="1" Text="Yes" />
                                                                    <telerik:RadComboBoxItem Value="2" Text="No" />
                                                                </Items>
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Sign Date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODEmployeeSignDate" runat="server" placeholder="Date"
                                                                class="form-control" TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Witness:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadNumericTextBox ID="RadNODWitness1" runat="server" EmptyMessage="-Type CIM-"
                                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                            </telerik:RadNumericTextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Sign Date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODWitness1SignDate" runat="server" placeholder="Date"
                                                                class="form-control" TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Witness:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadNumericTextBox ID="RadNODWitness2" runat="server" EmptyMessage="-Type CIM-"
                                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                            </telerik:RadNumericTextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label for="CoacheeName" class="control-label col-sm-6">
                                                            Sign Date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODWitness2SignDate" runat="server" placeholder="Date"
                                                                class="form-control" TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            Non-acceptance reason:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadNODNonAcceptance" runat="server" class="form-control"
                                                                Width="100%" EmptyMessage="-Select-">
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <telerik:RadTextBox ID="RadNODNonAcceptanceNotes" runat="server" class="form-control"
                                                                placeholder="Notes (if applicable)" TextMode="MultiLine" Width="100%" RenderMode="Lightweight"
                                                                Rows="5" TabIndex="7">
                                                            </telerik:RadTextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                            </div>
                                            <div style="height: 30px; line-height: 30px; text-align: center;">
                                                -HR Section-
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            NOD return date:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadDatePicker ID="RadNODReturnDate" runat="server" placeholder="Date" class="form-control"
                                                                TabIndex="6" Width="75%">
                                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                                </Calendar>
                                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                                    TabIndex="6">
                                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                                </DateInput>
                                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                                            </telerik:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            Escalated by:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadNumericTextBox ID="RadEscalatedBy" runat="server" EmptyMessage="-Type CIM-"
                                                                Class="form-control" Width="75%" NumberFormat-GroupSeparator="">
                                                                <NumberFormat AllowRounding="false" DecimalDigits="10" />
                                                            </telerik:RadNumericTextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            Case Decision:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadCaseDecision" runat="server" class="form-control" Width="100%"
                                                                EmptyMessage="-Select-">
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                            <telerik:RadTextBox ID="RadNODCaseDecisionNotes" runat="server" class="form-control"
                                                placeholder="State additional note for the case decision" TextMode="MultiLine"
                                                Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                            <div style="height: 20px">
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-6" for="email">
                                                            End-of-week action required:</label>
                                                        <div class="col-sm-6">
                                                            <telerik:RadComboBox ID="RadEoW" runat="server" class="form-control" Width="100%"
                                                                EmptyMessage="-Select-">
                                                            </telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        &nbsp;</div>
                                                </div>
                                            </div>
                                            <telerik:RadTextBox ID="RadNODEOWNotes" runat="server" class="form-control" placeholder="Action required"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <div style="height: 5px">
                    </div>
                    <div id="NexidiaForm" runat="server">
                        <asp:Panel ID="NexidiaPane" runat="server">
                            <asp:Panel ID="NexidiaPane1" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                What is the purpose of this coaching?<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse21" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadTextBox ID="RadDescriptionNexidia" runat="server" class="form-control"  onkeydown="setHeight(this,event)" style="overflow:hidden !important;"
                                                placeholder="Commend agent for / Address agent's opportunities on" TextMode="MultiLine"
                                                Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                                <ClientEvents OnLoad="RadTextBoxLoad" />
                                            </telerik:RadTextBox>
                                            <%--                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ValidationGroup="AddReview"
                                                ForeColor="Red" ControlToValidate="RadDescriptionNexidia" Display="Dynamic" ErrorMessage="*This is a required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="Panel6" runat="server">
                                    <div id="Div6" runat="server">
                                        <div style="height: 5px">
                                        </div>
                                        <div class="panel panel-custom" id="Div7" runat="server">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Performance Result <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                        </span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapse22" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadGrid ID="RadGrid4" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid4_NeedDataSource"
                                                        AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGrid4_ItemCommand"
                                                        OnItemCreated="RadGrid4_OnItemDataBoundHandler" OnItemDataBound="RadGrid4_OnItemDataBound"
                                                        OnPreRender="RadGrid4_PreRender" ClientIDMode="AutoID" RenderMode="Lightweight">
                                                        <MasterTableView DataKeyNames="NexidiaID" CommandItemDisplay="Bottom" CellSpacing="0">
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                                                                    Visible="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="KPI">
                                                                    <ItemTemplate>
                                                                        <telerik:RadComboBox ID="RadKPI4" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                            DropDownAutoWidth="Enabled" OnSelectedIndexChanged="RadKPINexidia_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                        <asp:RequiredFieldValidator ID="RadKPI4RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                            ControlToValidate="RadKPI4" Display="Dynamic" ValidationGroup="PerfNexidia"></asp:RequiredFieldValidator>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                    <ItemTemplate>
                                                                        <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                                                        </telerik:RadTextBox>
                                                                        <asp:RequiredFieldValidator ID="RadTargetRequiredFieldValidator3" runat="server"
                                                                            ErrorMessage="*" ControlToValidate="RadTarget" Display="Dynamic" ValidationGroup="PerfNexidia"></asp:RequiredFieldValidator>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                    <ItemTemplate>
                                                                        <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'>
                                                                        </telerik:RadNumericTextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                            ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="PerfNexidia"></asp:RequiredFieldValidator>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                    <ItemTemplate>
                                                                        <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'>
                                                                        </telerik:RadNumericTextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                            ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="PerfNexidia"></asp:RequiredFieldValidator>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                                                                    Visible="false">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                    <ItemTemplate>
                                                                        <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                                                        </telerik:RadComboBox>
                                                                        <asp:RequiredFieldValidator ID="RadDriverRequiredFieldValidator3" runat="server"
                                                                            ErrorMessage="*" ControlToValidate="RadDriver" Display="Dynamic" ValidationGroup="PerfNexidia"></asp:RequiredFieldValidator>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="True" />
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                    <telerik:RadGrid ID="RadGrid5" runat="server" Visible="true" RenderMode="Lightweight">
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="True" />
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                    <div>
                                                        &nbsp;</div>
                                                    <div class="col-md-10">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <telerik:RadButton ID="RadButton2" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                                                            ForeColor="White" ValidationGroup="PerfNexidia" OnClick="Button2_Click">
                                                        </telerik:RadButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Agent Strengths and Opportunities<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapse23" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="input-group col-lg-12">
                                                        <telerik:RadTextBox ID="RadStrengthsNexidia" runat="server" class="form-control" onkeydown="txtboxkeydown(this,event)" style="overflow:hidden !important;"
                                                            placeholder="Strength/s: Agent did well on ____" TextMode="MultiLine" Width="100%"
                                                            RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                        </telerik:RadTextBox>
                                                        <span class="input-group-addon" style="padding: 1px; visibility: hidden"></span>
                                                        <telerik:RadTextBox ID="RadOpportunitiesNexidia" runat="server" class="form-control" onkeydown="txtboxkeydown(this,event)" style="overflow:hidden !important;"
                                                            placeholder="Opportunity/ies: Agent will need to improve on ____ " TextMode="MultiLine"
                                                            Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div style="height: 5px">
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse24">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4 class="panel-title">
                                                        Begin with Behaviour.
                                                    </h4>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6 class="panel-title">
                                                        Questions(See Opening questions-successes and Areas for development.Prepare 1-2.).
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div id="collapse24" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="Div11" class="container-fluid" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <telerik:RadTextBox ID="RadPositiveBehaviour" runat="server" class="form-control"
                                                            placeholder="What positive behaviour did the employee exhibit? " TextMode="MultiLine"
                                                            Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                        </telerik:RadTextBox>
                                                        <telerik:RadTextBox ID="RadOpportunityBehaviour" runat="server" class="form-control"
                                                            placeholder="What opportunity did the employee exhibit in the behaviour? " TextMode="MultiLine"
                                                            Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <telerik:RadTextBox ID="RadBeginBehaviourComments" runat="server" class="form-control"
                                                            placeholder="" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10"
                                                            TabIndex="9">
                                                        </telerik:RadTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="Panel7" runat="server">
                                    <div id="Div12" runat="server">
                                        <div style="height: 5px">
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse25">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h4 class="panel-title">
                                                                Review Results. Talk Specifics.
                                                            </h4>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h6 class="panel-title">
                                                                Questions(See metric specific questions.Prepare 1-2.).
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div id="collapse25" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="Div14" class="container-fluid" runat="server">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <telerik:RadTextBox ID="RadBehaviourEffect" runat="server" class="form-control" placeholder="How did the behaviour, attitude, skill and knowledge affect the performance and results? "
                                                                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                                </telerik:RadTextBox>
                                                                <telerik:RadTextBox ID="RadRootCause" runat="server" class="form-control" placeholder="What is the root cause? Note: Build understanding with the employee "
                                                                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                                </telerik:RadTextBox>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <telerik:RadTextBox ID="RadReviewResultsComments" runat="server" class="form-control"
                                                                    placeholder="" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10"
                                                                    TabIndex="9">
                                                                </telerik:RadTextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div style="height: 5px">
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse26">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4 class="panel-title">
                                                        Outline Action Plans. Keep it concise.
                                                    </h4>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6 class="panel-title">
                                                        Questions(See general proving & discovery.Prepare 1-2.).
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div id="collapse26" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="input-group col-lg-12">
                                                <telerik:RadTextBox ID="RadAddressOpportunities" runat="server" class="form-control"
                                                    placeholder="How does the employee want to address the opportunities? Note: Build on strength. Agree on actions."
                                                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                </telerik:RadTextBox>
                                                <span class="input-group-addon" style="padding: 1px; visibility: hidden"></span>
                                                <telerik:RadTextBox ID="RadActionPlanComments" runat="server" class="form-control"
                                                    placeholder="" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5"
                                                    TabIndex="9">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 5px">
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse27">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Create Plan <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse27" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="input-group col-lg-12">
                                                <telerik:RadTextBox ID="RadSmartGoals" runat="server" class="form-control" placeholder="What are the SMART goals that you and the agent agreed on? Note: Identify and classify statements based on the SMART
structure below; Establish responsibilities and accountabilities." TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5"
                                                    TabIndex="9">
                                                </telerik:RadTextBox>
                                                <span class="input-group-addon" style="padding: 1px; visibility: hidden"></span>
                                                <telerik:RadTextBox ID="RadCreatePlanComments" runat="server" class="form-control"
                                                    placeholder="" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5"
                                                    TabIndex="9">
                                                </telerik:RadTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 5px">
                                </div>
                                <asp:Panel ID="Panel8" runat="server">
                                    <div id="Div15" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse28">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Follow Through <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                        </span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapse28" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadFollowThrough" runat="server" class="form-control" placeholder="What activities have you done with the agent to justify his/her understanding of the coaching points?
Note:Ensure that you are confident that the agent will not commit the same challenge/s once you let him/her go back to the phone." TextMode="MultiLine"
                                                        Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                    </telerik:RadTextBox>
                                                    <asp:Panel ID="Panel9" runat="server">
                                                        <div id="Div16" runat="server">
                                                            <div id="Div17" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <label for="Previous Perfomance Results">
                                                                        Previous Coaching Notes</label><br />
                                                                    <div style="height: 5px">
                                                                    </div>
                                                                    <div>
                                                                        <div class="panel-body">
                                                                            <telerik:RadGrid ID="RadGrid6" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                                <MasterTableView>
                                                                                    <Columns>
                                                                                        <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridBoundColumn DataField="FormType" HeaderText="formtype" UniqueName="FormType"
                                                                                FilterControlToolTip="FormType" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReviewTypeID" HeaderText="ReviewTypeID" UniqueName="ReviewTypeID"
                                                                                FilterControlToolTip="ReviewTypeID" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                                    </Columns>
                                                                                </MasterTableView>
                                                                                <ClientSettings>
                                                                                    <Scrolling AllowScroll="True" />
                                                                                </ClientSettings>
                                                                            </telerik:RadGrid>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <label for="Previous Perfomance Results">
                                                                        Previous Perfomance Results</label><br />
                                                                    <div style="height: 5px">
                                                                    </div>
                                                                    <div>
                                                                        <div class="panel-body">
                                                                            <telerik:RadGrid ID="RadGrid7" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                                <MasterTableView>
                                                                                    <Columns>
                                                                                        <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCT" Text='<%# Eval("Review KPI ID") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Session">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Target">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Current">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelDN" Text='<%# Eval("Driver Name") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelCDate" Text='<%# Eval("Review Date") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="LabelABy" Text='<%# Eval("Assigned By") %>' runat="server"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                    </Columns>
                                                                                </MasterTableView>
                                                                                <ClientSettings>
                                                                                    <Scrolling AllowScroll="True" />
                                                                                </ClientSettings>
                                                                            </telerik:RadGrid>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 5px">
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <div class="panel panel-custom">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse29">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapse29" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <asp:Panel ID="NexidiaPane2" runat="server">
                                        <asp:Panel ID="NexidiaPane3" runat="server">
                                            <telerik:RadGrid ID="RadGrid8" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                CssClass="RemoveBorders" OnItemDataBound="RadGrid8_onItemDatabound" BorderStyle="None"
                                                GridLines="None">
                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-12">
                                        <telerik:RadAsyncUpload ID="RadAsyncUpload3" runat="server" MultipleFileSelection="Automatic"
                                            OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf,.xls,.png,.xlsx"
                                            MaxFileSize="1000000" Width="40%" PostbackTriggers="UploadDummy">
                                            <Localization Select="                             " />
                                        </telerik:RadAsyncUpload>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="height: 5px">
                    </div>
                    <div id="ODForm" runat="server">
                        <asp:Panel ID="ODPane" runat="server">
                            <asp:Panel ID="ODPane1" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse40">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Description<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse40" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadTextBox ID="RadDescriptionOD" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                                                TextMode="MultiLine" Width="100%" Rows="5" Resize="None" RenderMode="Lightweight" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                                                <ClientEvents OnLoad="RadTextBoxLoad" />
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                                <div style="height: 5px">
                                </div>
                                <div class="panel panel-custom" id="Div19" runat="server">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse30">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Performance Result <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                </span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse30" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadGrid ID="RadGrid9" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid9_NeedDataSource"
                                                AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGrid9_ItemCommand"
                                                OnItemCreated="RadGrid9_OnItemDataBoundHandler" OnItemDataBound="RadGrid9_OnItemDataBound"
                                                OnPreRender="RadGrid9_PreRender" ClientIDMode="AutoID" RenderMode="Lightweight">
                                                <MasterTableView DataKeyNames="ODID" CommandItemDisplay="Bottom" CellSpacing="0">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" FilterControlAltText=""
                                                            Text="Delete" UniqueName="DeleteColumn" Resizable="false" ConfirmText="Delete performance result?">
                                                            <HeaderStyle CssClass="rgHeader ButtonColumnHeader"></HeaderStyle>
                                                            <ItemStyle CssClass="ButtonColumn" />
                                                        </telerik:GridButtonColumn>
                                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="KPI">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadKPIOD" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                    DropDownAutoWidth="Enabled" OnSelectedIndexChanged="RadKPIOD_SelectedIndexChanged">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadKPIRadDriverRadTargetRequiredFieldValidator3"
                                                                    runat="server" ErrorMessage="*" ControlToValidate="RadKPIOD" Display="Dynamic"
                                                                    ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                                <asp:Label ID="LabelAsOf" Text="" runat="server" Font-Size="X-Small" ForeColor="Red"
                                                                    Font-Italic="true"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridBoundColumn DataField="DataViewValue" UniqueName="DataViewValue" HeaderText="DataViewValue"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="DataView">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadDataView" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                    DropDownAutoWidth="Enabled" EmptyMessage="-Select DataView-" OnSelectedIndexChanged="RadDataViewOD_SelectedIndexChanged">
                                                                    <Items>
                                                                        <telerik:RadComboBoxItem Value="Daily" Text="Daily" />
                                                                        <telerik:RadComboBoxItem Value="Weekly" Text="Weekly" />
                                                                        <telerik:RadComboBoxItem Value="Monthly" Text="Monthly" />
                                                                    </Items>
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadDataViewRequiredFieldValidatorRadDataView" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadDataView" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Target">
                                                            <ItemTemplate>
                                                                <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                                                </telerik:RadTextBox>
                                                                <asp:RequiredFieldValidator ID="RadTargetRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadTarget" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Current">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'
                                                                    ReadOnly="true">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Previous">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'
                                                                    ReadOnly="true">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Change">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadChange" runat="server" Text='<%# Eval("Change") %>'
                                                                    ReadOnly="true">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4RadChange" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadChange" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Driver">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled" CheckBoxes="true"
                                                                    EnableCheckAllItemsCheckBox="true">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadDriverRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadDriver" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Behavior">
                                                            <ItemTemplate>
                                                                <telerik:RadTextBox ID="RadBehavior" runat="server" Text='<%# Eval("Behavior") %>'
                                                                    TextMode="MultiLine">
                                                                </telerik:RadTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4RadBehavior" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadBehavior" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Root Cause">
                                                            <ItemTemplate>
                                                                <telerik:RadTextBox ID="RadRootCause" runat="server" Text='<%# Eval("RootCause") %>'
                                                                    TextMode="MultiLine">
                                                                </telerik:RadTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4RadRootCause" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadRootCause" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <telerik:RadGrid ID="RadGrid10" runat="server" Visible="true" RenderMode="Lightweight">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <div>
                                                &nbsp;</div>
                                            <div class="col-md-10">
                                            </div>
                                            <div class="col-md-2">
                                                <telerik:RadButton ID="RadButton3" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                                                    ForeColor="White" OnClick="Button3_Click">
                                                </telerik:RadButton>
                                            </div>
                                        </div>
                                        <asp:Panel ID="Panel10" runat="server">
                                            <div id="Div21" runat="server">
                                                <div id="Div22" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <label for="Previous Perfomance Results" id="lblPrevCoachingNotes" runat="server">
                                                            Previous Coaching Notes</label><br />
                                                        <div style="height: 5px">
                                                        </div>
                                                        <div>
                                                            <div class="panel-body">
                                                                <telerik:RadGrid ID="RadGrid11" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false" OnItemDataBound="RadGrid11_onitemdatabound">
                                                                    <MasterTableView>
                                                                        <Columns>
                                                                            <telerik:GridHyperLinkColumn DataTextField="ReviewID" HeaderText="Coaching Ticket" UniqueName="CoachingTicket"
                                                                                FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                                                                Visible="false" Display="false">
                                                                            </telerik:GridHyperLinkColumn>
                                                                            
                                                                            <telerik:GridHyperLinkColumn DataTextField="FullName" HeaderText="Name" UniqueName="FullName"
                                                                                FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="FullName"
                                                                                AllowSorting="true" ShowSortIcon="true" HeaderStyle-Font-Bold="true" Target="_blank">
                                                                            </telerik:GridHyperLinkColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Session">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridBoundColumn DataField="FormType" HeaderText="formtype" UniqueName="FormType"
                                                                                FilterControlToolTip="FormType" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReviewTypeID" HeaderText="ReviewTypeID" UniqueName="ReviewTypeID"
                                                                                FilterControlToolTip="ReviewTypeID" Display="false">
                                                                            </telerik:GridBoundColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                    <ClientSettings>
                                                                        <Scrolling AllowScroll="True" />
                                                                    </ClientSettings>
                                                                </telerik:RadGrid>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div style="height: 5px">
                                </div>
                                <asp:Panel ID="PanelStOpps" runat="server">
                                    <div id="Div23" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse31">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Strengths<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapse31" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadStrengthsOD" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                                                        onkeydown="setHeight(this,event)" style="overflow:hidden !important;" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                                        <ClientEvents OnLoad="RadTextBoxLoad" />
                                                    </telerik:RadTextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 5px">
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse32">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Opportunities<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapse32" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadOpportunitiesOD" runat="server" class="form-control" placeholder="Commend agent for / Address agent's opportunities on"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                                                        <ClientEvents OnLoad="RadTextBoxLoad" />
                                                    </telerik:RadTextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <div class="panel panel-custom">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse33">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Commitment<span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapse33" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <asp:Panel ID="ODPane4" runat="server">
                                        <asp:Panel ID="ODPane5" runat="server">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="text-center">
                                                        Get the agent/employee's commitment by identifying the problem, setting goals and
                                                        clearly identifying the end-result using the four phases of GROW Coaching method
                                                        below
                                                    </h5>
                                                </div>
                                                <div id="no-more-tables">
                                                    <table class="col-sm-12 table-bordered table-striped table-condensed cf">
                                                        <thead class="cf">
                                                            <tr>
                                                                <th align="center">
                                                                    Goal
                                                                </th>
                                                                <th align="center">
                                                                    Reality
                                                                </th>
                                                                <th align="center">
                                                                    Options
                                                                </th>
                                                                <th align="center">
                                                                    Way Forward
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td data-title="Goal">
                                                                    <telerik:RadTextBox ID="RadTextBox1" runat="server" TextMode="MultiLine" Rows="10"
                                                                        Enabled="false">
                                                                    </telerik:RadTextBox>
                                                                    <telerik:RadTextBox ID="RadTextBox2" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                                    </telerik:RadTextBox>
                                                                </td>
                                                                <td data-title="Reality">
                                                                    <telerik:RadTextBox ID="RadTextBox3" runat="server" TextMode="MultiLine" Rows="10"
                                                                        Enabled="false">
                                                                    </telerik:RadTextBox>
                                                                    <telerik:RadTextBox ID="RadTextBox4" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                                    </telerik:RadTextBox>
                                                                </td>
                                                                <td data-title="Options">
                                                                    <telerik:RadTextBox ID="RadTextBox5" runat="server" TextMode="MultiLine" Rows="10"
                                                                        Enabled="false">
                                                                    </telerik:RadTextBox>
                                                                    <telerik:RadTextBox ID="RadTextBox6" runat="server" Placeholder="(Free-form field)

Option 1
                                                
Option 2
                                                
Option 3" TextMode="MultiLine" Rows="15">
                                                                    </telerik:RadTextBox>
                                                                </td>
                                                                <td data-title="Way Forward">
                                                                    <telerik:RadTextBox ID="RadTextBox7" runat="server" TextMode="MultiLine" Rows="10"
                                                                        Enabled="false">
                                                                    </telerik:RadTextBox>
                                                                    <telerik:RadTextBox ID="RadTextBox8" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                                    </telerik:RadTextBox>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </asp:Panel>
                                </div>
                                <asp:Panel ID="PanelPreviousCommitment" runat="server">
                                    <div id="Div27" runat="server">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Previous Perfomance Results">
                                                        <b>Previous Commitment</b></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <div class="panel-body" id="PreviousCommitment" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="ODPane6" runat="server">
                            <asp:Panel ID="ODPane7" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse34">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Coach Feedback <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                </span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse34" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadTextBox ID="RadCoacherFeedback" runat="server" class="form-control" placeholder="Description"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                                                <ClientEvents OnLoad="RadTextBoxLoad" />
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <div class="panel panel-custom">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse35">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapse35" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <asp:Panel ID="ODPane2" runat="server">
                                        <asp:Panel ID="ODPane3" runat="server">
                                            <telerik:RadGrid ID="RadGrid12" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                CssClass="RemoveBorders" OnItemDataBound="RadGrid12_onItemDatabound" BorderStyle="None"
                                                GridLines="None">
                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="col-sm-12">
                                        <telerik:RadAsyncUpload ID="RadAsyncUpload4" runat="server" MultipleFileSelection="Automatic"
                                            OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf,.xls,.png,.xlsx"
                                            MaxFileSize="1000000" Width="40%" PostbackTriggers="UploadDummy">
                                            <Localization Select="                             " />
                                        </telerik:RadAsyncUpload>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="SignOut" class="container-fluid" runat="server" style="display: none">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                                        TabIndex="12" placeholder="Coachee CIM Number" AutoPostBack="true" Visible="false">
                                    </telerik:RadTextBox>
                                    <%--  <asp:RequiredFieldValidator runat="server" ID="RadCoacheeCIMRequiredFieldValidator13" ValidationGroup="AddReview"
                                        ControlToValidate="RadCoacheeCIM" ForeColor="Red" Display="Dynamic" ErrorMessage="*"
                                        CssClass="validator"></asp:RequiredFieldValidator>--%>
                                    <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSID"
                                        RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3" Visible="false">
                                    </telerik:RadTextBox>
                                    <%--   <asp:RequiredFieldValidator runat="server" ID="RadSSNRequiredFieldValidator13" ValidationGroup="AddReview"
                                        ControlToValidate="RadSSN" ForeColor="Red" Display="Dynamic" ErrorMessage="*"
                                        CssClass="validator"></asp:RequiredFieldValidator>--%>
                                    <telerik:RadButton ID="RadCoacheeSignOff" Visible="false" runat="server" Text="Coachee Sign Off"
                                        CssClass="btn btn-info btn-small" ValidationGroup="AddReview" OnClick="RadCoacheeSignOff_Click"
                                        ForeColor="White">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off" CssClass="btn btn-info btn-small"
                                        ValidationGroup="AddReview" OnClick="RadCoacherSignOff_Click" ForeColor="White"
                                        Visible="false">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="UploadDummy" runat="server" Text="UploadDummy" OnClick="UploadDummy_Click"
                                        Style="display: none;">
                                    </telerik:RadButton>
                                </div>
                            </div>
                        </div>
                        <div class="fakeSave" class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <asp:HiddenField ID="hiddenfieldAccount" runat="server" />
                                    <telerik:RadButton ID="RadSaveBtn" runat="server" Text="Save" CssClass="btn btn-info btn-small"
                                        ValidationGroup="AddReview" ForeColor="White" OnClick="RadSaveBtn_Click">
                                    </telerik:RadButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display: none">
                        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
                            <Windows>
                                <telerik:RadWindow runat="server" NavigateUrl="https://www.google.com/accounts/Logout"
                                    ID="Window1">
                                </telerik:RadWindow>
                            </Windows>
                        </telerik:RadWindowManager>
                    </div>
                    <asp:Panel ID="btngoogle" runat="server" Visible="false">
                        <asp:ImageButton ID="btnRequestLogin" runat="server" Text="Google Login" OnClick="btnRequestLogin_Click"
                            ImageUrl="Content/images/logogoogle.png" Width="250px" Height="50px" ImageAlign="Middle" />
                    </asp:Panel>
                    <div style="display: none">
                        <asp:TextBox ID="txtReceiverID" runat="server" Visible="true"></asp:TextBox>
                        <asp:Label ID="lblhdDomain" runat="server" Visible="false"></asp:Label>
                        <asp:Panel ID="googledetails" runat="server">
                            <tr>
                                <br>
                                <td rowspan="4">
                                    <div id="img">
                                        <asp:Image ID="imgprofile" runat="server" Height="100px" Width="100px" Style="border-radius: 50%;"
                                            ImageAlign="Right" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </asp:Panel>
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid" id="CMTButtons" runat="server">
                            <div class="row">
                                <div class="col-md-11 text-right">
                                    <telerik:RadButton ID="RadCMTPreview" runat="server" Text="Preview" CssClass="btn btn-info btn-small"
                                        ForeColor="White" OnClick="RadCMTPreview_Click" Visible="false" ValidationGroup="AddReview">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="RadCMTSaveSubmit" runat="server" Text="Submit" CssClass="btn btn-info btn-small"
                                        ForeColor="White" ValidationGroup="AddReview" Visible="false" OnClick="RadCMTSaveSubmit_Click">
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="CyborgCMT" runat="server" Text="CyborgCMT" OnClick="CyborgCMT_Click"
                                        Style="display: none;">
                                    </telerik:RadButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
    <div id="myInc" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="ModalHeader" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <telerik:RadGrid ID="RadGrid2" runat="server" AllowMultiRowSelection="True" Font-Size="Smaller"
                        GroupPanelPosition="Top" ResolvedRenderMode="Classic" RenderMode="Lightweight"
                        OnItemDataBound="RadGrid2_OnItemDatabound" CssClass="RemoveBorders" AutoGenerateColumns="false">
                        <MasterTableView DataKeyNames="ReviewID">
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                </telerik:GridClientSelectColumn>
                                <telerik:GridHyperLinkColumn DataTextField="ReviewID" HeaderText="Review ID" UniqueName="ReviewID"
                                    AllowFiltering="true" Target="_blank">
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridBoundColumn DataField="FullName" HeaderText="Name" UniqueName="FullName"
                                    FilterControlToolTip="Nametip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CIMNumber" HeaderText="Cim" UniqueName="CimNumber"
                                    FilterControlToolTip="Cimtip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Session" HeaderText="Session" UniqueName="Session"
                                    FilterControlToolTip="Sessiontip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Topic" HeaderText="Topic" UniqueName="Topic"
                                    FilterControlToolTip="Topictip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReviewDate" HeaderText="Review Date" UniqueName="ReviewDate"
                                    FilterControlToolTip="ReviewDatetip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AssignedBy" HeaderText="Assigned By" UniqueName="AssignedBy"
                                    FilterControlToolTip="AssignedBytip">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReviewTypeID" HeaderText="ReviewTypeID" UniqueName="ReviewTypeID"
                                    FilterControlToolTip="ReviewTypeID" Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FormType" HeaderText="FormType" UniqueName="FormType"
                                    FilterControlToolTip="FormType" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="true">
                            <Scrolling AllowScroll="True" />
                            <Selecting AllowRowSelect="True"></Selecting>
                            <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                        Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic" RenderMode="Lightweight"
                        CssClass="RemoveBorders">
                        <MasterTableView DataKeyNames="Review KPI ID">
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                </telerik:GridClientSelectColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="true">
                            <Scrolling AllowScroll="True" />
                            <Selecting AllowRowSelect="True"></Selecting>
                            <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <div class="modal-footer">
                    <span class="addreviewbuttonholder">
                        <telerik:RadButton ID="RadCNLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadCNLink_Click">
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton ID="RadCNCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadCNCancel_Click">
                        </telerik:RadButton>
                        <telerik:RadButton ID="RadPRLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadPRLink_Click">
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton ID="RadPRCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadPRCancel_Click">
                        </telerik:RadButton>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div id="myCMTPop" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="H1" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="panel menuheadercustom">
                            <div style="text-align: center;">
                                <h4>
                                    Activate Search Blackout
                                </h4>
                            </div>
                        </div>
                        <div class="panel-group" id="Div4">
                            <div class="row row-custom">
                                <div class="col-sm-12">
                                    <div style="text-align: center;">
                                        <h5>
                                            You have created a For Termination Progressive Discipline Review.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            To keep the information on this review from being searched and/or accessed by anyone
                                            other than the review creator, it is recommended to activate the Search Blackout
                                            feature.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            Would you like to activate the Search Blackout feature?
                                        </h5>
                                        <br></br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div style="text-align: center;">
                                <telerik:RadButton ID="RadSearchBlackOutYes" runat="server" Text="Yes" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutYes_Click">
                                </telerik:RadButton>
                                <telerik:RadButton ID="RadSearchBlackOutNo" runat="server" Text="No" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutNo_Click">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myCMTPopv2" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="H2" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="panel menuheadercustom">
                            <div style="text-align: center;">
                                <h4>
                                    Confirm action to activate Search Blackout
                                </h4>
                            </div>
                        </div>
                        <div class="panel-group" id="Div8">
                            <div class="row row-custom">
                                <div class="col-sm-12">
                                    <div style="text-align: center;">
                                        <h5>
                                            You are about to activate the Search Blackout feature.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            Activating the feature ensures that the information on this review is kept from
                                            being searched and/or accessed by anyone other than the review creator.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            The process cannot be undone once the feature is activated.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            Would you like to proceed with the activation of the Search Blackout?
                                        </h5>
                                        <br></br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div style="text-align: center;">
                                <telerik:RadButton ID="RadSearchBlackOutYesv2" runat="server" Text="Yes" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutYesv2_Click">
                                </telerik:RadButton>
                                <telerik:RadButton ID="RadSearchBlackOutNov2" runat="server" Text="No" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadSearchBlackoutNov2_Click">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myCMTPopv3" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="H3" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="panel menuheadercustom">
                            <div style="text-align: center;">
                                <h4>
                                    Confirm action to share review
                                </h4>
                            </div>
                        </div>
                        <div class="panel-group" id="Div9">
                            <div class="row row-custom">
                                <div class="col-sm-12">
                                    <div style="text-align: center;">
                                        <h5>
                                            You are about to share this review, all its information including the details of
                                            the case progression, with the employee's immediate superior.
                                        </h5>
                                        <br></br>
                                        <h5>
                                            Would you like to share this review?
                                        </h5>
                                        <br></br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div style="text-align: center;">
                                <telerik:RadButton ID="RadCMTShareYes" runat="server" Text="Yes" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadCMTShareYes_Click">
                                </telerik:RadButton>
                                <telerik:RadButton ID="RadCMTShareNo" runat="server" Text="No" CssClass="btn btn-info btn-small"
                                    ForeColor="White" OnClick="RadCMTShareNo_Click">
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
