﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true"
    MasterPageFile="~/Site.Master" CodeBehind="AddTriadCoaching.aspx.cs" Inherits="CoachV2.AddTriadCoaching" %>

<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="UserControl/SearchAdvUserCtrl.ascx" TagName="SearchAdvUserCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarDashboardUserControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript" src="libs/js/JScript1.js"></script>
    <%--<link href="libs/css/gridview.css" rel="stylesheet" type="text/css" />
<link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        div.RemoveBorders .rgHeader, div.RemoveBorders th.rgResizeCol, div.RemoveBorders .rgFilterRow td
        {
            border-width: 0 0 1px 0; /*top right bottom left*/
        }
        
        div.RemoveBorders .rgRow td, div.RemoveBorders .rgAltRow td, div.RemoveBorders .rgEditRow td, div.RemoveBorders .rgFooter td
        {
            border-width: 0;
            padding-left: 7px; /*needed for row hovering and selection*/
        }
        
        div.RemoveBorders .rgGroupHeader td, div.RemoveBorders .rgFooter td
        {
            padding-left: 7px;
        }
    </style>
    <script type="text/javascript">
        function onRequestStart(sender, args) {


        }
        function responseEnd(sender, eventArgs) {

        }

        // added for accumulating component scores (francis.valera/09132018)
        function drpScore_OnClientSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            sender.set_text(item.get_text());

            var masterTable = $find("<%=grd_coachingevaluation.ClientID%>").get_masterTableView();
            var count = masterTable.get_dataItems().length;
            var totalscore = 0;
            var combobox;
            var rowscore;
            var noscore = 0;
            var items = masterTable.get_dataItems();
            for (var i = 0; i < count; i++) {
                var row = items[i];
                combobox = row.findControl("drpScore");
                rowscore = combobox.get_text();
                if (rowscore != "") {
                    totalscore = totalscore + parseInt(rowscore);
                }
                else 
                {
                    noscore = noscore + 1;
                }
            }
            var radtotalscore = $find("<%=txttotalscore.ClientID %>");
            radtotalscore.set_value(totalscore);
            if (noscore > 0) {
                radtotalscore.get_element().style.color = "red";
            }
            else {
                radtotalscore.get_element().style.color = "#909090";
            }
        }
    </script>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25" MinDisplayTime="1000" CssClass="Loading2" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <%--<AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="txt_scoring">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grd_coachingevaluation" />
                    <telerik:AjaxUpdatedControl ControlID="txttotalscore" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>--%>
    </telerik:RadAjaxManager>
   
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }


            
            </script>
            </telerik:RadScriptBlock>
    <div class="menu-content bg-alt">
        <div class="panel menuheadercustom">
            <div>
                &nbsp; <span class="glyphicon glyphicon-dashboard"></span>Coaching Dashboard > My Triad Coaching <span class="breadcrumb2ndlevel">
                    <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span>
            </div>
        </div>
        <asp:Panel ID="pn_reviews" runat="server">
            <div class="panel-collapse collapse in">
                <div class="panel-body">
                    <div>
                        <telerik:RadGrid ID="grd_tlreviews" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            CssClass="RemoveBorders" BorderStyle="None" Visible="false" OnItemDataBound="grd_tlreviews_Itemdatabound">
                            <MasterTableView DataKeyNames="Topic">
                                <Columns>
                                    <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                        UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                        Visible="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                        FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="NameField"
                                        AllowSorting="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"
                                        AllowSorting="true">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                        AllowSorting="true" FilterControlToolTip="Escalationctip">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" DataField="Topic"
                                        SortExpression="Topic" AllowSorting="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'> </asp:Label>
                                            <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor"></i>
                                            <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="lbl1"
                                                RelativeTo="Element" Position="BottomCenter" RenderInPageRoot="true">
                                                <%# DataBinder.Eval(Container.DataItem, "Topic")%>
                                            </telerik:RadToolTip>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                        AllowSorting="true" FilterControlToolTip="Reviewtip">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <Resizing AllowResizeToFit="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <uc1:SearchAdvUserCtrl ID="SearchAdvUserCtrl1" runat="server" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pn_coacheeview" Visible="false" runat="server">
            <div class="panel-group" id="Div1">
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#coachingspecs">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                </span>
                            </h4>
                        </div>
                    </a>
                    <div id="coachingspecs" class="panel-collapse collapse in">
                        <div class="panel-body" style="padding-bottom: 5px">
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2 col-md-push-10">
                                    <div class="col-sm-2">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:HiddenField ID="hfGridHtml" runat="server" />
                                        <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                            Visible="true" OnClick="btn_ExporttoPDF_Click">
                                            <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                        </telerik:RadButton>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-10 col-md-pull-2">
                                    <label for="Account" class="control-label col-sm-3">
                                        Coaching Ticket</label>
                                    <div class="col-sm-3">
                                        <telerik:RadTextBox ID="RadTextBox1" runat="server" class="form-control" Text="0"
                                            ReadOnly="true" Enabled="false" Width="100%">
                                        </telerik:RadTextBox>
                                    </div>
                                    <label for="Account" class="control-label col-sm-3">
                                        Coaching Date</label>
                                    <div class="col-sm-3">
                                        <telerik:RadTextBox ID="RadDatePicker1" Width="100%" runat="server" class="form-control"
                                            Text="0" ReadOnly="true">
                                        </telerik:RadTextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="container-fluid" id="Div6" runat="server">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:HiddenField ID="FakeURLID" runat="server" />
                                        <div id="profileimageholder">
                                            <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Name: </b><span>
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span></h5>
                                        <h5>
                                            <b>Position: </b><span>
                                                <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Site: </b><span>
                                                <asp:Label ID="lblSite" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Reporting Manager: </b><span>
                                                <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Region: </b><span>
                                                <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Country: </b><span>
                                                <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Department: </b><span>
                                                <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Client: </b><span>
                                                <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Campaign: </b><span>
                                                <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b></b><span>
                                                <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Session Type: </b><span>
                                                <asp:Label ID="SessionType" runat="server" Text="My Session Type" /></span></h5>
                                        <h5>
                                            <b>Topic: </b><span>
                                                <asp:Label ID="SessionTopic" runat="server" Text="My Session Type" /></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pn_OMView" Visible="false" runat="server">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Description"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" Enabled="false" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                            ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                            CssClass="validator"></asp:RequiredFieldValidator>
                        --%>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Performance Result <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="true" AllowPaging="true"
                            CssClass="RemoveBorders" BorderStyle="None" RenderMode="Lightweight">
                            <MasterTableView CellSpacing="0">
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="false" RenderMode="Lightweight">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseStrenghts">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Strengths <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseStrenghts" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="TxtStrenghts" runat="server" class="form-control" placeholder="Strengths"
                            Enabled="false" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOpportunities">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Opportunities <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOpportunities" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="txtOpportunities" runat="server" class="form-control" placeholder="Opportunities"
                            Enabled="false" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCommitment">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Commitment <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseCommitment" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="txtCommitment" runat="server" class="form-control" placeholder="Commitment"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7"
                            Visible="false">
                        </telerik:RadTextBox>
                    </div>
                    <div>
                        <%--<div class="panel-body">--%>
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center">
                                    Get the agent/employee's commitment by identifying the problem, setting goals and
                                    clearly identifying the end-result using the four phases of GROW Coaching method
                                    below
                                </h5>
                            </div>
                            <div id="no-more-tables">
                                <table class="col-sm-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                        <tr>
                                            <th align="center">
                                                Goal
                                            </th>
                                            <th align="center">
                                                Reality
                                            </th>
                                            <th align="center">
                                                Options
                                            </th>
                                            <th align="center">
                                                Way Forward
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-title="Goal">
                                                <telerik:RadTextBox ID="RadTextBox2" runat="server" TextMode="MultiLine" Rows="10"
                                                    Enabled="false" ReadOnly="true">
                                                </telerik:RadTextBox>
                                                <telerik:RadTextBox ID="RadTextBox3" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td data-title="Reality">
                                                <telerik:RadTextBox ID="RadTextBox4" runat="server" TextMode="MultiLine" Rows="10"
                                                    Enabled="false" ReadOnly="true">
                                                </telerik:RadTextBox>
                                                <telerik:RadTextBox ID="RadTextBox5" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td data-title="Options">
                                                <telerik:RadTextBox ID="RadTextBox6" runat="server" TextMode="MultiLine" Rows="10"
                                                    Enabled="false" ReadOnly="true">
                                                </telerik:RadTextBox>
                                                <telerik:RadTextBox ID="RadTextBox7" runat="server" Placeholder="(Free-form field)

Option 1
                                                
Option 2
                                                
Option 3" TextMode="MultiLine" Rows="15">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td data-title="Way Forward">
                                                <telerik:RadTextBox ID="RadTextBox8" runat="server" TextMode="MultiLine" Rows="10"
                                                    Enabled="false" ReadOnly="true">
                                                </telerik:RadTextBox>
                                                <telerik:RadTextBox ID="RadTextBox9" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15">
                                                </telerik:RadTextBox>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pn_coacherfeedback" Visible="false" runat="server">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCoacherFeedBack">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coach Feedback <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseCoacherFeedBack" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="txtCoacherFeedback" runat="server" class="form-control" placeholder="Coach to provide his/her feedback on the agent/employee's Commitment and/or summary of the review session."
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                    </div>
                </div>
            </div>
            <%--Coacher Feedback --%>
            <div class="panel panel-custom">
                <%--Documentation--%>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentation">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseDocumentation" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="grd_Documentation" runat="server" Visible="false">
                        </telerik:RadGrid>
                        <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            OnItemDataBound="RadGridDocumentation_onItemDatabound" CssClass="RemoveBorders"
                            BorderStyle="None">
                            <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Item Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                        FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                        UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                        AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pn_save" runat="server" Visible="false">
            <div class="panel panel-custom">
                <%--Documentation--%>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCoachingEvaluations">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coaching Effectiveness Evaluation <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseCoachingEvaluations" class="panel-collapse collapse in">
                    <div class="panel-collapse collapse in">
                        <div class="panel-body">
                            <%--remove onitemcreated in order to use new scoring dropdown (francis.valera/09132018--%>
                            <telerik:RadGrid ID="grd_coachingevaluation" runat="server" AutoGenerateColumns="False"
                                GroupPanelPosition="Top" ResolvedRenderMode="Classic" OnItemDataBound="grd_coachingevaluation_OnItemDatabound">
                                <MasterTableView>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Coaching Components" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="txt_ccomponent" runat="server" Text='<%# Eval("CoachingComponents") %>'
                                                    TextMode="MultiLine" BorderStyle="None" Width="100%" Enabled="false">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>

                                        <%--replaced scoring template from textbox to dropdown--%>
                                        <telerik:GridTemplateColumn HeaderText="Scores" HeaderStyle-HorizontalAlign="Center" UniqueName="CompScore">
                                            <ItemTemplate>
                                                <telerik:RadComboBox ID="drpScore" runat="server" AutoPostBack="false" OnClientSelectedIndexChanged="drpScore_OnClientSelectedIndexChanged" Width="100%">
                                                </telerik:RadComboBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <%--disabled scoring from numerictextbox to give way for dropdown (francis.valera/09132018)--%>
                                        <telerik:GridTemplateColumn HeaderText="Scoring" Visible="false" HeaderStyle-Width="30px">
                                            <ItemTemplate>
                                                <telerik:RadNumericTextBox ID="txt_scoring" runat="server" Text='<%# Eval("scoring") %>'
                                                    TextMode="MultiLine" BorderStyle="None" Width="100%" AutoPostBack="true">
                                                    <%--OnTextChanged="scorevalidation"--%>
                                                </telerik:RadNumericTextBox>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txt_scoring" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator> --%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="Pre Step" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="txt_prestep" runat="server" Text='<%# Eval("prepstep") %>'
                                                    TextMode="MultiLine" BorderStyle="None" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Proper Step" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="txt_Properstep" runat="server" Text='<%# Eval("properstep") %>'
                                                    TextMode="MultiLine" BorderStyle="None" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Post Step" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="txt_Poststep" runat="server" Text='<%# Eval("poststep") %>'
                                                    TextMode="MultiLine" BorderStyle="None" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                        <%--added new hidden column to handle default component scores (francis.valera/09132018)--%>
                                        <telerik:GridBoundColumn DataField="ComponentScore" UniqueName="ComponentScore" HeaderText="" Visible="true" Display="false"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true" />
                                    <Resizing AllowResizeToFit="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="col-xs-4">
                                <asp:Label ID="lbltotalscore" Text="TOTAL SCORE" runat="server" Width="100%"></asp:Label>
                            </div>
                            <div class="col-xs-8">
                                <telerik:RadNumericTextBox ID="txttotalscore" ReadOnly="true" runat="server" Width="100%">
                                </telerik:RadNumericTextBox>
                            </div>
                            <br />
                            <br />
                            <telerik:RadGrid ID="grd_notesfromcoacher" runat="server" AutoGenerateColumns="false"
                                CssClass="RemoveBorders" BorderStyle="None">
                                <MasterTableView>
                                    <ColumnGroups>
                                        <telerik:GridColumnGroup Name="Notegroup" HeaderText="Note" HeaderStyle-HorizontalAlign="Center" />
                                    </ColumnGroups>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Did Well" ColumnGroupName="Notegroup" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%-- <asp:TextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>'></asp:TextBox>--%>
                                                <telerik:RadTextBox ID="txt_didwell" runat="server" Text='<%# Eval("DidWell") %>'
                                                    TextMode="MultiLine" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Do Differently" ColumnGroupName="Notegroup"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%-- <asp:TextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'></asp:TextBox>--%>
                                                <telerik:RadTextBox ID="txt_DoDifferent" runat="server" Text='<%# Eval("DoDifferent") %>'
                                                    TextMode="MultiLine" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pn_signoff" runat="server" Visible="true">
            <div id="SignOut" class="container-fluid" runat="server">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <telerik:RadButton ID="btn_Audit" runat="server" Text="Audit" ValidationGroup="AddReview"
                            CssClass="btn btn-info btn-small" ForeColor="White" Visible="false" OnClick="btn_Audit_Click"
                            AutoPostBack="true">
                        </telerik:RadButton>
                        <telerik:RadButton ID="btn_Save" runat="server" Text="Save" ValidationGroup="AddReview"
                            CssClass="btn btn-info btn-small" ForeColor="White" Visible="false" OnClick="btn_Save_Click">
                            <%--OnClick="RadCoacherSignOff_Click"--%>
                        </telerik:RadButton>
                        <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                            placeholder="Coachee CIM" Enabled="false" Visible="false">
                            <%--Visible="false"--%>
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSS ID"
                            RenderMode="Lightweight" ValidationGroup="AddReview" TextMode="Password" Visible="false"
                            Enabled="false">
                        </telerik:RadTextBox>
                        <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                            ValidationGroup="AddReview" CssClass="btn btn-info btn-small" ForeColor="White"
                            Visible="false" Enabled="false" OnClick="RadCoacheeSignOff_Click">
                            <%-- Visible="false" --%>
                            <%----%>
                        </telerik:RadButton>
                        <telerik:RadTextBox ID="RadCoachCim" runat="server" class="form-control" RenderMode="Lightweight"
                            placeholder="Coach CIM" Enabled="false" Visible="false">
                            <%--Visible="false"--%>
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="RadCoachSSN" runat="server" class="form-control" placeholder="Last 3 SSS ID"
                            RenderMode="Lightweight" ValidationGroup="AddReview" TextMode="Password" Visible="false"
                            Enabled="false">
                        </telerik:RadTextBox>
                        <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off" ValidationGroup="AddReview"
                            CssClass="btn btn-info btn-small" ForeColor="White" Visible="false" OnClick="RadCoacherSignOff_Click">
                            <%-- Visible="false"--%>
                            <%--OnClick="RadCoacherSignOff_Click"--%>
                        </telerik:RadButton>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div style="height: 5px">
        </div>
    </div>
</asp:Content>
