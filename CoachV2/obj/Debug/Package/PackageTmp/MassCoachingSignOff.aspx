﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MassCoachingSignOff.aspx.cs" Inherits="CoachV2.MassCoachingSignOff" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<script type="text/javascript" src="libs/js/JScript1.js"></script>
     <asp:Panel ID="pn_coacheedetails" Visible="false"  runat="server" >
  <div class="panel menuheadercustom">
    <div>
        &nbsp;<span><asp:Label
            ID="Label3" runat="server" Text=""></asp:Label></span><span><asp:Label
            ID="Label1" runat="server" Text=""></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text=""></asp:Label></span>
     </div>
</div>
         <div class="panel-group" id="Div1">
             <div class="panel panel-custom">
                 <div class="panel panel-custom">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                         <div class="panel-heading">
                             <h4 class="panel-title">
                                 Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                 </span>
                             </h4>
                         </div>
                     </a>
                     <div id="Div2" class="panel-collapse collapse in">
                         <div class="panel-body">
                             <div class="form-group">
                                 <label for="Account" class="control-label col-sm-3">
                                     Coaching Ticket</label>
                                 <div class="col-sm-3">
                                     <telerik:RadTextBox ID="RadReviewID" Width="100%" runat="server" class="form-control"
                                         Text="0" ReadOnly="true"></telerik:RadTextBox>
                                 </div>
                                 <label for="Account" class="control-label col-sm-3">
                                     Coaching Date</label>
                                 <div class="col-sm-3">
                                     <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                         Text="0" ReadOnly="true">
                                     </telerik:RadTextBox>
                                 </div>
                             </div>
                         </div>
                         <div class="panel-body">
                             <div class="container-fluid" id="Div6" runat="server">
                                 <div class="row">
                                     <div class="col-md-4">
                                         <asp:HiddenField ID="FakeURLID" runat="server" />
                                         <div id="profileimageholder">
                                             <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                         </div>
                                     </div>
                                     <div class="col-md-4">
                                         <h5>
                                             <b>Name: </b><span>
                                                 <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span></h5>
                                         <h5>
                                             <b>Position: </b><span>
                                                 <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Site: </b><span>
                                                 <asp:Label ID="LblSite" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Reporting Manager: </b><span>
                                                 <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Region: </b><span>
                                                 <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Country: </b><span>
                                                 <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Department: </b><span>
                                                 <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Client: </b><span>
                                                 <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b>Campaign: </b><span>
                                                 <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                         <h5>
                                             <b></b><span>
                                                 <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                                     </div>
                                     <div class="col-md-4">
                                         <h5>
                                             <b>Session Type: </b><span>
                                                 <asp:Label ID="LblSessionType" runat="server" Text="My Session Type" /></span></h5>
                                         <h5>
                                             <b>Topic: </b><span>
                                                 <asp:Label ID="LblSessionTopic" runat="server" Text="My Session Type" /></span></h5>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>

</asp:Panel>
    
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadDescription" Width="100%" class="form-control" placeholder="Description"
                    runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="5" ValidationGroup="MassCoaching">
                    <ClientEvents OnLoad="RadTextBoxLoad" />
                </telerik:RadTextBox>
       
            </div>
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Agenda <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseThree" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadAgenda" Width="100%" class="form-control" placeholder="Agenda"
                    runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="15">
                    <ClientEvents OnLoad="RadTextBoxLoad" />
                </telerik:RadTextBox>
               
            </div>
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseQADocumentation">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseQADocumentation" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                    AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                    OnItemDataBound="RadGridDocumentation_onItemDatabound" CssClass="RemoveBorders"
                    BorderStyle="None">
                    <MasterTableView DataKeyNames="Id">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Item Number">
                                <ItemTemplate>
                                    <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                AllowSorting="true" Target="_blank" ShowSortIcon="true">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                <ItemTemplate>
                                    <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                <ItemTemplate>
                                    <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowResizeToFit="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <div>
                    &nbsp;
                </div>
              </div>
        </div>
    </div>

<%--   <div class="panel-body">
            <div class="form-group">
                <div class="col-xs-6 col-sm-3">
                    <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                        TabIndex="12" placeholder="Coachee CIM Number">
                    </telerik:RadTextBox>
                </div>
                 <div class="col-xs-6 col-sm-3">
                    <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="SSN"
                        RenderMode="Lightweight" TabIndex="13" TextMode="Password"
                        MaxLength="3">
                    </telerik:RadTextBox>
                 </div>
                <div class="col-md-4">
                    <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                        CssClass="btn btn-primary" ValidationGroup="AddReview" OnClick="RadCoacheeSignOff_Click"
                        ForeColor="White">
                    </telerik:RadButton>
                </div>
            </div>
        </div>--%>
         <div class="panel-body">
                <div class="container-fluid" id="SignOut" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                            TabIndex="12" placeholder="Coachee CIM Number" Visible="true">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSID"
                            RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" onclick="RadCoacheeSignOff_Click"></telerik:RadButton>
                            <telerik:RadButton ID="btn_Audit1" runat="server" Text="Audit"
                            ValidationGroup="AddReview" CssClass="btn btn-info btn-small"
                            ForeColor="White" OnClick="btn_Audit1_Click" Visible="false" >
                        </telerik:RadButton>

                        </div>
                    </div>
                </div>
                 <asp:Label ID="lblstatus" runat="server"  ForeColor="Red" Font-Size="Small"></asp:Label>
            </div>
</asp:Content>
