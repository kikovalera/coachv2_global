﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="CoachV2.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="panel-body">
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
            <telerik:RadGrid ID="RadGridN" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGridN_NeedDataSource"
                AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGridN_ItemCommand" AllowMultiRowEdit="true"
                OnItemCreated="RadGridN_OnItemDataBoundHandler" OnItemDataBound="RadGridN_OnItemDataBound"
                OnPreRender="RadGridN_PreRender" ClientIDMode="AutoID" OnInsertCommand="RadGrid1_InsertCommand" AllowAutomaticInserts="false" AllowAutomaticUpdates="true" OnUpdateCommand="RadGrid1_UpdateCommand">
                <MasterTableView DataKeyNames="ODID" CommandItemDisplay="Top" CellSpacing="0"
                    EditMode="EditForms" InsertItemDisplay="Bottom">
                    <EditFormSettings EditColumn-ButtonType="PushButton">
                        <EditColumn InsertText="Add More" >
                        </EditColumn>
                        <FormCaptionStyle BackColor="Red" />                        
                    </EditFormSettings>
                    <CommandItemSettings ShowAddNewRecordButton="true" ShowRefreshButton="false" />
                    <Columns>
                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false"
                            ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                            Visible="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="KPI">
                            <ItemTemplate>
                                <telerik:RadComboBox ID="RadKPI" runat="server" OnSelectedIndexChanged="RadKPI_SelectedIndexChanged"
                                    AutoPostBack="true" CausesValidation="false" DropDownAutoWidth="Enabled">
                                </telerik:RadComboBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Target">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Current">
                            <ItemTemplate>
                                <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'>
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Previous">
                            <ItemTemplate>
                                <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'>
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                    ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="% Change">
                            <ItemTemplate>
                                <telerik:RadNumericTextBox ID="RadChange" runat="server" Text='<%# Eval("Previous") %>'>
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                                    ControlToValidate="RadChange" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                            Visible="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Driver" ReadOnly="true">
                            <ItemTemplate>
                                <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                </telerik:RadComboBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Behavior" UniqueName="Behavior" >
                            <ItemTemplate>
                                <telerik:RadTextBox ID="LblBehavior" runat="server" Text='<%# Eval("Behavior") %>'
                                    ReadOnly="true">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadTextBox ID="RadBehavior" runat="server" Text='<%# Eval("Behavior") %>'>
                                </telerik:RadTextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                          <telerik:GridTemplateColumn HeaderText="Root Cause" UniqueName="RootCause">
                            <ItemTemplate>
                                <telerik:RadTextBox ID="LblRootCause" runat="server" Text='<%# Eval("RootCause") %>'
                                    ReadOnly="true">
                                </telerik:RadTextBox>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadTextBox ID="RadRootCause" runat="server" Text='<%# Eval("RootCause") %>'>
                                </telerik:RadTextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                    </Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true">
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="true">
                    </Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            <div>
                &nbsp;</div>
            <div class="col-md-10">
            </div>
            <div class="col-md-2">
                <telerik:RadButton ID="RadButton1" runat="server" Text="Add More" CssClass="btn btn-primary"
                    OnClick="Button1_Click" ForeColor="White" ValidationGroup="Perf">
                </telerik:RadButton>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
