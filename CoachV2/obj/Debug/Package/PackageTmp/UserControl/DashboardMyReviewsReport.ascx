﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardMyReviewsReport.ascx.cs" Inherits="CoachV2.UserControl.DashboardMyReviewsReport" %>
<%--
<div class="panel menuheadercustom" runat="server" id ="myreviewtitle">
    <div>
        &nbsp;<span class="glyphicon glyphicon-tasks"></span> MY REPORTS ><span class="breadcrumb2ndlevel"><asp:Label
            ID="Label1" runat="server" Text=" Report Builder "></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span></div>
</div>--%>
<div class="panel menuheadercustom" runat="server" id ="myreviewtitle">
    <div>
        &nbsp;<span class="glyphicon glyphicon-dashboard"></span><asp:Label
            ID="LblMyReviews" runat="server" Text="MY REVIEWS"></asp:Label><span class="breadcrumb2ndlevel"><asp:Label
            ID="Label1" runat="server" Text=" "></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span></div>
</div>
<div class="panel-group" id="accordion">
 <div id="Div1" class="container-fluid" runat="server">
    <div class="row">
        <div class="col-sm-9" id="SearchPane" runat="server">
            <asp:HyperLink ID="HyperLinkRB" runat="server" NavigateUrl="~/ReportBuilder.aspx" class="btn"><span class="glyphicon glyphicon-edit"></span> Report Builder</asp:HyperLink>
            <asp:HyperLink ID="HyperLinkBIR" runat="server" class="btn"  NavigateUrl="~/BuiltinReport.aspx"><span class="glyphicon glyphicon-cog"></span> Built In Reports</asp:HyperLink>
            <asp:HyperLink ID="HyperLinkSR" runat="server" NavigateUrl="~/SavedReports.aspx" class="btn"><span class="glyphicon glyphicon-floppy-saved"></span> Saved Report</asp:HyperLink>
        </div>
    </div>
</div>
</div>