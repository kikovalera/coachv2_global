﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MassCoachingSelected.ascx.cs" Inherits="CoachV2.UserControl.MassCoachingSelected" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxySelected" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadSessionTypeSelected">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadSessionTopicSelected" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridEmployees" />
                <telerik:AjaxUpdatedControl ControlID="RadCimNumber" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadRemove">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<asp:Panel ID="CoachingAllPanel" runat="server">
<div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Details
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="CIM Number" class="control-label col-xs-4">
                                            Cim Number</label>
                                        <div class="col-xs-8">
                                            <telerik:RadTextBox ID="RadCimNumber" runat="server" placeholder="CIM Number">
                                            </telerik:RadTextBox>
                                           <telerik:RadButton ID="RadAdd" runat="server" CssClass="btn btn-primary" ForeColor="White"
                                                Text="Add" OnClick="RadAdd_Click">
                                            </telerik:RadButton>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="Account" class="control-label col-xs-4">
                                        </label>
                                        <div class="col-xs-8">
                                            <%--<telerik:RadListBox ID="RadList" runat="server" ItemPlaceholderID="CIM Number" PageSize="3"
                                                AllowPaging="true" Width="100%">
                                               <Items></Items>
                                            </telerik:RadListBox>--%>
                                            <telerik:RadGrid ID="RadGridEmployees" runat="server" AutoGenerateColumns="false" OnDeleteCommand="RadGrid1_DeleteCommand"  OnNeedDataSource="RadGrid1_NeedDataSource" AllowAutomaticInserts="false" AllowAutomaticUpdates="true"
                                        EnableLoadOnDemand="True"  EnableViewState="true">
                                            <MasterTableView DataKeyNames="CIM" CssClass="RadGrid" GridLines="None"  >
                                                <Columns>
                                                    <telerik:GridTemplateColumn HeaderText="CIM Number" UniqueName="CIM" DataField="CIM"
                                                        Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblCIM" runat="server" Text='<%# Eval("CIM") %>' DataTextField="CIM"
                                                                Visible="true">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Name" UniqueName="Name" DataField="Name"
                                                        Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblName" runat="server" Text='<%# Eval("Name") %>' DataTextField="Name"
                                                              Visible="true">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                         
                                                  <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Text="Delete" ButtonType="PushButton" HeaderText="Delete" ConfirmTextFormatString="Are you sure you want to delete the selected item?" ConfirmTextFields="Model" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                                                 </Columns>
                                              </MasterTableView>
                                            </telerik:RadGrid>
<%--                                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="MassCoaching"
                                            ForeColor="Red" ControlToValidate="RadList" Display="Dynamic" ErrorMessage="*This is a required field."
                                            CssClass="validator"></asp:RequiredFieldValidator>--%> 
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="SessionType" class="control-label col-xs-4">
                                            Session Type</label>
                                        <div class="col-xs-8">
                                            <telerik:RadComboBox ID="RadSessionTypeSelected" runat="server" class="form-control"
                                                RenderMode="Lightweight" DropDownAutoWidth="Enabled" AutoPostBack="true" CausesValidation="true"
                                                OnSelectedIndexChanged="RadSessionTypeSelected_SelectedIndexChanged" EmptyMessage="Select Session Type" >
                                            </telerik:RadComboBox>
                                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadSessionTypeSelected" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="Topic" class="control-label col-xs-4">
                                            Topic</label>
                                        <div class="col-xs-8">
                                            <telerik:RadComboBox ID="RadSessionTopicSelected" runat="server" class="form-control"
                                                RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="Select Session Topic">
                                            </telerik:RadComboBox>
                                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="MassCoaching"
                ForeColor="Red" ControlToValidate="RadSessionTopicSelected" Display="Dynamic" ErrorMessage="*This is a required field."
                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</asp:Panel>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                 <telerik:RadTextBox ID="RadDescription" Width="100%" class="form-control" placeholder="Description" runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="5" ValidationGroup="MassCoaching"></telerik:RadTextBox>
                       <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="MassCoaching"
                    ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                    CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Agenda <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseThree" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadAgenda" Width="100%" class="form-control" placeholder="Agenda" runat="server" TextMode="MultiLine" RenderMode="Lightweight" Rows="15"></telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="MassCoaching"
                    ForeColor="Red" ControlToValidate="RadAgenda" Display="Dynamic" ErrorMessage="*This is a required field."
                    CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="row">
            <br>
            <div class="col-sm-12">
                <span class="addreviewbuttonholder">
                    <telerik:RadButton ID="RadCoacherSignOff" CssClass="btn btn-primary" 
                    runat="server" Text="Coach Sign Off" ValidationGroup="MassCoaching" 
                    ForeColor="White" onclick="RadCoacherSignOff_Click"></telerik:RadButton>
                </span>
            </div>
        </div>