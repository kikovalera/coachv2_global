﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardDirRosterUserCtrl.ascx.cs" Inherits="CoachV2.UserControl.DashboardDirRosterUserCtrl" %>
<%--<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="AgentGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="AgentGrid"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
--%>
<!-- comment -->
<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
 <%--       <div>
            &nbsp;<span class="glyphicon glyphicon-file"></span> MY Team
        </div>--%>
        <%--replaced span label with proper case (francis.valera/07182018)--%>
        <div>&nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard > My Team</div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Director Roster <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseOne" class="panel-collapse collapse in">
            
            <div class="no-bg">
                <telerik:RadGrid ID="AgentGrid" runat="server" OnNeedDataSource="AgentGrid_NeedDataSource"
                    OnPreRender="AgentGrid_PreRender" Skin="Bootstrap" AllowSorting="True" RenderMode="Auto"
                    AllowPaging="True" PageSize="7" CssClass="RemoveBorders" GridLines="None" BorderStyle="None">
                    <PagerStyle Mode="NextPrevAndNumeric" Position="TopAndBottom" PageSizeControlType="RadComboBox">
                    </PagerStyle>
                    
                  <%--added for static header alignment--%>
                   <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>

                    <MasterTableView CommandItemDisplay="None" PageSize="7" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyNames="CimNumber" GridLines="None" TableLayout="Auto">
                        <%--CssClass="RadGrid"--%>
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="Name" HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="CIM" HeaderText="CIM" DataField="CimNumber"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="AccountID" HeaderText="Account" DataField="account"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Division" HeaderText="Division" DataField="Division"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="LOB" HeaderText="LOB" DataField="LOB" HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <span class="glyphicon glyphicon-align-justify" data-html="true" tabindex="0" data-trigger="focus"
                                        data-toggle="popover" data-content="<span class='glyphicon glyphicon-plus-sign'></span> <a href='AddReview.aspx?CIM=<%# encryp_cim_url(Eval("CimNumber")) %>' style='color: #444;'>Add Review</a><br><span class='glyphicon glyphicon-signal'></span> <a href='MyTeam.aspx?tab=perf&CIM=<%# Eval("CimNumber") %>' style='color: #444;'>Director's Performance Summary</a><br><span class='glyphicon glyphicon-edit'></span> <a href='MyTeam.aspx?tab=logs&CIM=<%# Eval("CimNumber") %>' style='color: #444;'>Director's Review Logs</a><br><span class='glyphicon glyphicon-user'></span> <a href='MyTeam.aspx?tab=profile&CIM=<%# Eval("CimNumber") %>' style='color: #444;'>Director's Profile</a>"
                                        data-original-title="" title=""></span>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        
                        <%--added for static pager alignment--%>
                        <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                        </telerik:GridTemplateColumn>

                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" EnableVirtualScrollPaging="true" />
                    </ClientSettings>
                    <FilterMenu RenderMode="Lightweight">
                    </FilterMenu>
                    <HeaderContextMenu RenderMode="Lightweight">
                    </HeaderContextMenu>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <br />
    <br />
</div>
