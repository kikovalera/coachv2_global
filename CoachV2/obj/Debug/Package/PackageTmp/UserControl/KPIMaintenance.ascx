﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KPIMaintenance.ascx.cs" Inherits="CoachV2.UserControl.KPIMaintenance" %>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
	 
}

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

div.scrollsides
{
    border-width:0;   
    overflow:auto;
padding-left:7px;
border-width:0 0 1px 0; 
}


    .col-sm-2
    {
        width: 724px;
    }


</style>

<asp:Panel ID="Pn_KPIMaintenance" runat="server"  >

<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapselookup">
        <div class="panel-heading">
            <h4 class="panel-title">
                Create KPI <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
               </span>
            </h4>
        </div>
    </a>
                        <div id="collapselookup" class="panel-collapse collapse in">
                      <div class="panel-body" style="padding-bottom:5px" >
                        <div class="panel-body" style="padding-bottom:1px">
                            <div  class="form-group" >
                                <label for="kpiname" class="control-label col-sm-2">
                                    KPI Name</label>
                                <div class="col-sm-2">       
                                <asp:TextBox ID="txtkpiname" runat="server" Width="299px" ></asp:TextBox>                        
                                  </div>
                               <%-- <div class="col-sm-2">
                                </div>--%>
                                <label for="linkto" class="control-label col-sm-2">
                                    Account</label>
                                <div class="col-sm-2">
                                    <telerik:RadComboBox ID="cb_account" Filter="Contains" runat="server" DataSourceID="SqlDataSource3"
                                        DataTextField="Account" DataValueField="AccountID" CausesValidation="true">
                                    </telerik:RadComboBox>
                                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" SelectCommand="pr_Coach_getActiveAccount"
                                        SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                    </asp:SqlDataSource>
                                </div>
                                 <label for="kpiname" class="control-label col-sm-2">
                                    Target </label>
                                <div class="col-sm-2">       
                                <asp:TextBox ID="txtTarget" runat="server"  ></asp:TextBox>                                                         
                                </div>
                        </div>
                        <div class="panel-body" style="padding-bottom:1px" >
                        <div class="form-group">
                            <label for="valuetype" class="control-label col-sm-2">
                                Value Type</label>
                            <div class="col-sm-2">
                                <telerik:RadComboBox ID="cb_valuetype" DataSourceID="SqlDataSource1" runat="server"
                                    DataTextField="valuetoname" DataValueField="valueid" CausesValidation="true"
                                    RenderMode="Lightweight">
                                </telerik:RadComboBox>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="SELECT  [valueid],[valuetoname] FROM [CoachV2].[dbo].[tbl_Coach_Valueto] where [hidefromlist] = 0"
                                    SelectCommandType="Text" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                </asp:SqlDataSource>
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                        </div>
                        </div>
                        <div class="panel-body" style="padding-bottom:1px" >
                         <div class="form-group">
                            <label for="linkto" class="control-label col-sm-2">
                                Link to
                            </label>
                            <div class="col-sm-2">
                                <telerik:RadComboBox ID="cb_linkto" DataSourceID="SqlDataSource2" runat="server"
                                    ResolvedRenderMode="Classic" DataTextField="linktype" DataValueField="id" CausesValidation="true"
                                    RenderMode="Lightweight">
                                </telerik:RadComboBox>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" SelectCommand="SELECT [id] ,[linktype]  FROM [CoachV2].[dbo].[tbl_Coach_LinkType] where hidefromlist = 0"
                                    SelectCommandType="Text" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                </asp:SqlDataSource>
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                            <div class="col-sm-2">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                                <br />
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <asp:Button ID="Button1" runat="server" Text="Add KPI" CssClass="btn btn-info btn-small"
                                            ForeColor="White" OnClick="Button1_oclivk" /><%-- OnClick="btn_test" --%>
                                    </span>
                                </div>
                            </div>
                    </div>
               </div>
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_kpi2">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                KPI List<span class="glyphicon glyphicon-triangle-bottom trianglebottom"> </span>
                            </h4>
                        </div>
                    </a>
                    <div id="collapse_kpi2" class="panel-collapse collapse in">
                        <div class="panel-body">
                        <asp:Label ID="lblForKpiCreated" runat="server" Visible="true"></asp:Label>
     
                <telerik:RadGrid ID="RadGrid1" 
                    EnableLoadOnDemand="True"  EnableViewState="true"  AllowAutomaticInserts="false" AllowAutomaticUpdates="true"
                    RenderMode="Auto"
                    AllowPaging="True" runat="server" AllowFilteringByColumn="false" AutoGenerateColumns="false"
                    GroupPanelPosition="Top" ResolvedRenderMode="Classic" CssClass="RemoveBorders"
                    BorderStyle="None"  >
                    <MasterTableView DataKeyNames="Row"  EditMode="EditForms"  TableLayout="Auto"> <%--EditMode="EditForms" OnUpdateCommand="radgrid1_UpdateCommand" OnDeleteCommand="RadGrid1_DeleteCommand" OnItemDataBound="radgrid1_databound"  --%>
              <%--  <Columns>
                        <telerik:GridTemplateColumn UniqueName="Rows"  DataField="Row" HeaderText="KPI" HeaderStyle-Font-Bold="true" Visible="false" Display="false">
                   <ItemTemplate>
                  <%# DataBinder.Eval(Container.DataItem, "Row")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="txtRow" Text='<%# DataBinder.Eval(Container.DataItem, "Row") %>'
                            Visible="false" Enabled="false"   />
                    </EditItemTemplate>  
                    </telerik:GridTemplateColumn>
                    
                        <telerik:GridTemplateColumn UniqueName="Rows1"  DataField="Num" HeaderText="Rows" HeaderStyle-Font-Bold="true" Visible="true" >
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Num")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="txtRowpartition" Text='<%# DataBinder.Eval(Container.DataItem, "Num") %>'
                            Visible="true" Enabled="false" />
                    </EditItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="KPI_Name"  DataField="KPI Name" HeaderText="KPI Name" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "KPI Name" )%>
                    </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cb_kpiname" Filter="Contains"  DataValueField="ID" DataTextField="KPI"
                                runat="server"  Width="100%" CausesValidation="true">
                            </telerik:RadComboBox>  
                        </EditItemTemplate>
                </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="Value_Type"  DataField="Value Type" HeaderText="Value Type" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Value Type")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadComboBox ID="cb_valuetype" runat="server"
                            DataTextField="valuetoname" DataValueField="valueid" CausesValidation="true">
                        </telerik:RadComboBox>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                   <telerik:GridTemplateColumn UniqueName="Assign_Type"  DataField="Assign_Type" HeaderText="Assign Type" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Assign Type")%>
                    </ItemTemplate>
                       <EditItemTemplate>
                           <telerik:RadComboBox ID="cb_linkto"  runat="server"
                               ResolvedRenderMode="Classic" DataTextField="linktype" DataValueField="id" CausesValidation="true">
                           </telerik:RadComboBox>
                       </EditItemTemplate>
                </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="Assigned_to" DataField="Assigned to" HeaderText="Assigned to"
                                HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "Assigned to")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="cb_account" Filter="Contains" runat="server" 
                                        Width="100%" DataTextField="Account" DataValueField="AccountID" CausesValidation="true">
                                    </telerik:RadComboBox>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn UniqueName="Created_by" HeaderText="Created by" DataField="Created by"
                                HeaderStyle-Font-Bold="true"  ReadOnly="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                  <telerik:GridEditCommandColumn UniqueName="EditCommandColumn"   >
                    </telerik:GridEditCommandColumn>
                   <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Text="Delete"  ButtonType="LinkButton" ConfirmTextFormatString="Are you sure you want to delete the selected item?" ConfirmTextFields="Model" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                        </Columns>--%>
                    </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                 </div>
                 </div>
                 </div>

</asp:Panel>
