﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarDashboardUserControl.ascx.cs" Inherits="CoachV2.UserControl.SidebarDashboardUserControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="menu-content">
    <div class="panel panel-default">
        <div class="panel-body">
            <span class="glyphicon glyphicon-dashboard"></span>&nbsp;Coaching Dashboard</div>
    </div>
  <%--  <div class="panel panel-default">--%>

        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="list-group-item" NavigateUrl="~/Dashboard.aspx?tab=team" Visible="false">
                <span class="glyphicon glyphicon-file"></span> My Team
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink2" runat="server" CssClass="list-group-item" NavigateUrl="~/Dashboard.aspx?tab=myreviews">
                <span class="glyphicon glyphicon-list-alt"></span> My Reviews
        </asp:HyperLink>
        <asp:HyperLink ID="HyperLink3" runat="server" CssClass="list-group-item" NavigateUrl="~/AddTriadCoaching.aspx">
               <span class="glyphicon glyphicon-eye-open"></span> My Triad Coaching
        </asp:HyperLink> 
               
        <asp:HyperLink ID="LinkButtonMyReports" runat="server" CssClass="list-group-item" NavigateUrl="~/ReportDefault.aspx" Visible="false">
        <span class="glyphicon glyphicon-tasks"></span> My Reports</asp:HyperLink>

        <asp:HyperLink ID="HLReportID" runat="server" CssClass="list-group-item" NavigateUrl="~/Admin.aspx">
            <span class="glyphicon glyphicon-cog"></span> Settings
        </asp:HyperLink>
        <br />
        
  
</div>
