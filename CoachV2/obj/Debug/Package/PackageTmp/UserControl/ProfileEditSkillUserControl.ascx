﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditSkillUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileEditSkillUserControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
           <a href="MyProfile.aspx?tab=skill" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>
      </i></a>
        <div>
            &nbsp;<span class="glyphicon glyphicon-saved"></span> My Skills
        </div>
    </div>
    <telerik:RadGrid ID="ExpGrid" runat="server" AutoGenerateColumns="false" AllowSorting="true"
        OnNeedDataSource="ExpGrid_NeedDataSource" OnInsertCommand="ExpGrid_InsertCommand"
        OnDeleteCommand="ExpGrid_DeleteCommand" OnUpdateCommand="ExpGrid_UpdateCommand"  RenderMode="Auto" OnItemCommand="ExpGrid_ItemCommand">
        <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" DataKeyNames="SkillID" 
            EditMode="EditForms"  >
            <EditFormSettings>
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <CommandItemSettings ShowExportToExcelButton="true"  AddNewRecordText="Add Skill"
                ShowExportToCsvButton="true" />
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Edit" ID="EditLink" runat="server"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Description" SortExpression="Description"
                    DataField="Description" ColumnEditorID="CompanyName" HeaderText="Skill Description"
                    HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Description")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtDescription" runat="server" EmptyMessage="Please enter skill description"
                            Text='<%# DataBinder.Eval(Container.DataItem, "Description")%>' />
                        <asp:RequiredFieldValidator ID="reqDescription" runat="server" Text="Enter skill description"
                            ControlToValidate="TxtDescription"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Rating" SortExpression="Rating" DataField="Rating"
                    ColumnEditorID="RoleName" HeaderText="Rating" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Rating") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="TxtRating" NumberFormat-DecimalDigits="0" runat="server" AllowOutOfRangeAutoCorrect="false"
                            EmptyMessage="Please enter rating between 1 and 10. 1 being the lowest and 10 being the highest. " MinValue="1" MaxValue="10" Text='<%# DataBinder.Eval(Container.DataItem, "Rating")%>' />
                            <asp:Label ID="lblrating" Text="Please enter rating between 1 and 10. 1 being the lowest and 10 being the highest. "  runat="server"></asp:Label>
                            <asp:RequiredFieldValidator ID="reqRating" runat="server" Text="Enter rating between 1 and 10. 1 being the lowest and 10 being the highest."
                            ControlToValidate="TxtRating"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="NumericTextBoxRangeValidator" 
                                    runat="server" 
                                    ControlToValidate="TxtRating"
                                    ErrorMessage="Please enter in a number between 1 and 10. 1 being the lowest and 10 being the highest." 
                                    Display="Dynamic"
                                    MaximumValue="10" MinimumValue="1" Type="Integer">
                </asp:RangeValidator>
                        <br />
                        <br />
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Delete" ID="DeleteLink" runat="server" OnClientClick="return confirm('Are you sure to delete this record?');"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <Scrolling AllowScroll="true" />
        </ClientSettings>
    </telerik:RadGrid>
    <br />
    <br />
</div>
