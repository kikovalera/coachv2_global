﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ops_ChartAgents.ascx.cs"
    Inherits="CoachV2.UserControl.Ops_ChartAgents" %>
<div class="panel-group" id="Div1_agent_charts">
    <asp:HiddenField ID="FakeURLID" runat="server" />
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Charts <span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>&nbsp;
                </h4>
            </div>
        </a>
    </div>
    <div id="collapseFour" class="panel-collapse collapse in">
        <div class="panel-body">
            <div id="collapseTL" class="panel-collapse collapse in">
                <br />
                <div class="form-horizontal col-md-12" role="form">
                    <button type="button" class="btn btn-custom" id="check_graph" style="float: left;
                        clear: both; margin-right: 10px;">
                        Overall</button>
                    <asp:DataList ID="dlKPI" runat="server" RepeatColumns="10" RepeatDirection="Horizontal">
                        <ItemTemplate>
                            <button class="btn btn-custom click-me-kpi" kpi-id='<%#Eval("hierarchy").ToString() %>'>
                                <%# Eval("kpi").ToString()%></button>
                            &nbsp;
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server" Visible="false"
                                CssClass="blacktext"> 
                            </asp:Label>
                        </FooterTemplate>
                    </asp:DataList>
                </div>
                <div id="overall-holder" style="padding-top: 20px;">
                    <div id="overalldate" style="width: 100%;">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4">Overall Coaching Frequency</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="overalldatechecker"/>
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                                DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="overalldatechecker"/>
                            </telerik:RadDatePicker>
                        </div>
                        <div class="col-sm-3" style="display: none;">
                            <div class="help">
                                KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <asp:DropDownList ID="ddKPIList2" runat="server" Width="150px" CssClass="form-control"
                                AppendDataBoundItems="true" AutoPostBack="false" Visible="false">
                                <asp:ListItem Value="0" Text="All" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="cb_overalldataview" runat="server" AppendDataBoundItems="true"
                                DataValueField="DataViewID" DataTextField="DataView">
                                <%--   OnClientSelectedIndexChanged="OnClientSelectedIndexChangedAgentOverall"  OnClientSelectedIndexChanged="OVERALL"--%>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                &nbsp;</div>
                            <button id="Button1" class="btn btn-info">Run</button>
                        </div>
                    </div>
                    <div id="overallfrequency" style="width: 100%; height: 500px;">
                    </div>
                    <div id="overall_behaviordates">
                        <br />
                        <br />
                        <br />
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4">Top Behavioral Drivers</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="overallagentbehaviordatechecker" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="overallagentbehaviordatechecker" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="cb_kpi_behavior1" runat="server" AppendDataBoundItems="true"
                                DataValueField="KPIID" DataTextField="KpiName" ResolvedRenderMode="Classic">
                                <%--OnClientSelectedIndexChanged="overallagentbehavior"--%>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                &nbsp;</div>
                            <button id="Button2" class="btn btn-info">Run</button>
                        </div>
                    </div>
                    <div id="behaviorvsKPI" class="col-xs-12 demo-placeholder" style="width: 100%; height: 400px;">
                    </div>
                    <div id="overall_kpivsb">
                        <br />
                        <br />
                        <br />
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4">Behavior vs KPI</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="overallagentbehaviorvskpichecker"  />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="overallagentbehaviorvskpichecker"  />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </div>
                            <telerik:RadComboBox ID="cb_kpi_behavior_kpi" AppendDataBoundItems="true" runat="server"
                                DataValueField="KPIID" DataTextField="KpiName">
                                <%--OnClientSelectedIndexChanged="overallagentbehaviorvskpi" >--%>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                &nbsp;</div>
                            <button id="Button3" class="btn btn-info">Run</button>
                        </div>
                    </div>
                    <div id="DIV2_kpivsscore" class="col-xs-12 demo-placeholder" style="width: 100%;
                        height: 400px;">
                    </div>
                </div>
                <%--top 1 kpi--%>
                <div id="chart-holder">
                    <div id="Div1">
                        <br />
                        <br />
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <%--<asp:Label ID="Label1" runat="server" Font-Size="Large">Score vs KPI</asp:Label>--%>
                                         <font size="4" id="freqvsscore">Average AHT Score</font>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="RadDatePicker1" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                <ClientEvents OnDateSelected="fakeShit1" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="RadDatePicker2" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="fakeShit1" />
                            </telerik:RadDatePicker>
                            &nbsp;
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="RadComboBox1" runat="server" AppendDataBoundItems="true"
                                DataValueField="DataViewID" DataTextField="DataView">
                                <%--   OnClientSelectedIndexChanged="OnClientSelectedIndexChangedAgentOverall"OnClientSelectedIndexChanged="cbChangeChartFreqVScore"  OnClientSelectedIndexChanged="OVERALL"--%>
                            </telerik:RadComboBox>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                &nbsp;</div>
                            <button id="BtnFx" class="btn btn-info">Run</button>
                        </div>
                    </div>
                    <div id="top1" runat="server" visible="true">
                        <div id="Overall_AHT_Agent" style="width: 100%; height: 100%;">
                        </div>
                    </div>
                    <div id="Div2_FCRGlidedates">
                        <br />
                        <br />
                        <br />
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        <font size="4" id="tarvsactual">
                                            <%--<asp:Label ID="lbl_3rdkpi_actualvstarget" runat="server" Font-Size="Large"> </asp:Label>--%>
                                            Targets Vs. Actual</font>
                                        <%--                    <font size="4">FCR Glide Path Targets Vs. Actual</font>--%>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-sm-3">
                            <div class="help">
                                From <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_fcrglide_start" runat="server" placeholder="Enter start date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="fakeShit" />
                            </telerik:RadDatePicker>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                To <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <telerik:RadDatePicker ID="dp_fcrglide_end" runat="server" Empty="Enter end date"
                                DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                                DateInput-DateFormat="yyyy-MM-dd">
                                 <ClientEvents OnDateSelected="fakeShit" />
                            </telerik:RadDatePicker>
                        </div>
                        <div class="col-sm-3">
                            <div class="help">
                                Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                            <telerik:RadComboBox ID="cb_FCR_Scoredt" runat="server" AppendDataBoundItems="true"
                                DataValueField="DataViewID" DataTextField="DataView">
                                <%--   OnClientSelectedIndexChanged="OnClientSelectedIndexChangedAgentOverall"  OnClientSelectedIndexChanged="OVERALL"--%>
                            </telerik:RadComboBox>
                        </div>
                         <div class="col-sm-3">
                            <div class="help">
                                &nbsp;</div>
                            <button id="BtnTarVAct" class="btn btn-info">Run</button>
                        </div>
                    </div>
                    <div id="DIV2_FCRGlide" class="col-xs-12 demo-placeholder" style="width: 100%; height: 400px;">
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <asp:HiddenField ID="MyCIM" runat="server" />
                    <div>
                        <div>
                            <span id="err-msg" class="col-xs-12 alert alert-danger" style="padding-top: 10px;
                                padding-bottom: 10px;"><i class="glyphicon glyphicon-warning-sign"></i>No data found</span>
                            <asp:CompareValidator ID="dateCompareValidator" CssClass="col-md-12 alert alert-warning"
                                runat="server" ControlToValidate="EndDate" ControlToCompare="StartDate" Operator="GreaterThan"
                                Type="Date" ErrorMessage="<i class='glyphicon glyphicon-warning-sign'></i> The second date must be after the first one. "
                                Style="padding-top: 10px; padding-bottom: 10px;">
                            </asp:CompareValidator>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/exporting.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
<script type='text/javascript'>

    var kpiid;
    var kpilabel;

    function OnDateSelected(sender, args) {
        // $("#check_graph").removeAttr("disabled");
    }

    function OnDateSelected2(sender, args) {
        //$("#check_graph").removeAttr("disabled");
    }

    function overallagentbehaviorvskpichecker(sender, args) {


        var startDate = $find('<%=dp_behaviorkpi_start.ClientID%>');
        var endDate = $find('<%=dp_behaviorkpi_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#Button3').prop('disabled', true);
        } else {
            $('#Button3').prop('disabled', false);
        }
    }


    function overallagentbehaviordatechecker(sender, args) {
        var startDate = $find('<%=dp_behavior_start.ClientID%>');
        var endDate = $find('<%=dp_behavior_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#Button2').prop('disabled', true);
        } else {
            $('#Button2').prop('disabled', false);
        }
    }

    function overalldatechecker(sender, args) {
        var startDate = $find('<%=StartDate.ClientID%>');
        var endDate = $find('<%=EndDate.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#Button1').prop('disabled', true);
        } else {
            $('#Button1').prop('disabled', false);
        }
    }

    function fakeShit(sender, args) {

        var startDate = $find('<%=dp_fcrglide_start.ClientID%>');
        var endDate = $find('<%=dp_fcrglide_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTarVAct').prop('disabled', true);
        } else {
            $('#BtnTarVAct').prop('disabled', false);
        }
    }

    function fakeShit1(sender, args) {

        var startDate = $find('<%=RadDatePicker1.ClientID%>');
        var endDate = $find('<%=RadDatePicker2.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnFx').prop('disabled', true);
        } else {
            $('#BtnFx').prop('disabled', false);
        }
    }


    var URL = $("#<%= FakeURLID.ClientID %>").val();

    function cbChangeChartFreqVScore(sender, args) {
        chart1loader(kpiid, kpilabel);
    }

    function cbChangeChartTarVScore(sender, args) {
        chart2loader(kpiid, kpilabel);
    }

    $("#err-msg").hide();

    $('.highcharts-data-table').remove();


    $('.highcharts-data-table').remove();

    function overall() {


        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var data3 = [];
        var data_kpivsbehavior1 = [];
        var data_kpivsbehavior2 = [];
        var data_bvsk1 = [];
        var data_bvsk2 = [];


        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var DataV = $find('<%=cb_overalldataview.ClientID %>');
        var DataView = DataV.get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    //c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);
                    //                    data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                }
            );

                Highcharts.chart('overallfrequency', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: {
                        tickInterval: 5,
                        min: 0,
                        title: {
                            text: ''
                        },
                        color: '#FF00FF'
                    },
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 5,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            },
                            fillOpacity: 1,
                            fillColor: '#289CCC'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        showTable: true
                    },
                    series: [{
                        name: 'Coaching Frequency',
                        data: data,
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                });
            }
        });
    }

    function overallagentbehavior() {
        $('.highcharts-data-table').remove();
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");

        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()

        var data3 = [];

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid1: '" + kpiid1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehavior_ForAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    //c = f.TargetCount;
                    data3.push([f.description, f.CoachCount]);
                    //                    data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                }
                );
                Highcharts.chart('behaviorvsKPI', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        tickInterval: 5,
                        min: 0,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        series: {
                            colorByPoint: true
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        showTable: true
                    },


                    series: [{
                        name: 'Overall Behavior',
                        data: data3,
                        type: 'column',
                        yaxis: 0,
                        //color: '#2F9473', // ['#2F9473', '#27A6D9'],
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                });
            }
        });
    }

    function overallagentbehaviorvskpi(sender, args) {
        $('.highcharts-data-table').remove();
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var data_bvsk1 = [];
        var data_bvsk2 = [];
        var RadDatePickerbkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>");
        var selectedDatebkpi1 = RadDatePickerbkpi1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerbkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>");
        var selectedDatebkpi2 = RadDatePickerbkpi2.get_selectedDate().format("yyyy-MM-dd");

        var kpiidbkpi = $find('<%=cb_kpi_behavior_kpi.ClientID %>');
        var kpiidbkpi1 = kpiidbkpi.get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetKPISCorevsTargetforAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);

                    $("#err-msg").hide();

                }
            );
                Highcharts.chart('DIV2_kpivsscore', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        tickInterval: 5,
                        min: 0,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        allowDecimals: false,
                        tickInterval: 20,
                        max: 100,
                        labels: {
                            format: '{value}%'
                        }
                    }],
                    tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    exporting: {
                        showTable: true
                    },


                    series: [{
                        name: 'Behavior',
                        data: data_bvsk1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
                });
            }
        });
    }

    $("#check_graph").on("click", function () {

        $(this).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

        $('.highcharts-data-table').remove();
        $("#overall-holder").show();

        $('#overalldate').show();
        $('#overallfrequency').show();
        $('#overall_behaviordates').show();

        $('#behaviorvsKPI').show();
        $('#overall_kpivsb').show();
        $('#DIV2_kpivsscore').show();

        $('.click-me-kpi').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });


        $("#chart-holder").hide();


        var data = [];
        var data_aht1 = [];
        var data_aht2 = [];
        var data3 = [];
        var data_kpivsbehavior1 = [];
        var data_kpivsbehavior2 = [];
        var data_bvsk1 = [];
        var data_bvsk2 = [];


        var RadDatePicker1 = $find("<%= StartDate.ClientID %>");
        var selectedDate1 = RadDatePicker1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePicker2 = $find("<%= EndDate.ClientID %>");
        var selectedDate2 = RadDatePicker2.get_selectedDate().format("yyyy-MM-dd");

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();

        var DataV = $find('<%=cb_overalldataview.ClientID %>');
        var DataView = DataV.get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "',  CIMNo: '" + CIMNo + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    //c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);
                    //                    data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                }
            );

                Highcharts.chart('overallfrequency', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: ''
                        },
                        color: '#FF00FF'
                    },
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 5,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            },
                            fillOpacity: 1,
                            fillColor: '#289CCC'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        showTable: true
                    },
                    series: [{
                        name: 'Coaching Frequency',
                        data: data,
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL FREQUENCY DIV1
            } //SUCCESSFUNCTION OA FRE
        }); //AJAX OVERALL FREQUENCY


        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");

        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()


        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid1: '" + kpiid1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehavior_ForAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    //c = f.TargetCount;
                    data3.push([f.description, f.CoachCount]);
                    //                    data2.push([f.ADate, f.TargetCount]);

                    $("#err-msg").hide();

                }
            );
                Highcharts.chart('behaviorvsKPI', {
                    chart: {
                        type: 'area',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        tickInterval: 5,
                        min: 0,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    { tickInterval: 5,
                        min: 0,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
                    tooltip: {
                        pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {
                        series: {
                            colorByPoint: true
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        showTable: true
                    },
                    series: [{
                        name: 'Overall Behavior',
                        data: data3,
                        type: 'column',
                        yaxis: 0,
                        //color: '#2F9473', // ['#2F9473', '#27A6D9'],
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    }]
                }); //OVERALL Behavior
            } //SUCCESS BEHAVIOR
        }); //BEHAVIOR AJAX


        var data_bvsk1 = [];
        var data_bvsk2 = [];
        var RadDatePickerbkpi1 = $find("<%= dp_behaviorkpi_start.ClientID %>");
        var selectedDatebkpi1 = RadDatePickerbkpi1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerbkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>");
        var selectedDatebkpi2 = RadDatePickerbkpi2.get_selectedDate().format("yyyy-MM-dd");

        var kpiidbkpi = $find('<%=cb_kpi_behavior_kpi.ClientID %>');
        var kpiidbkpi1 = kpiidbkpi.get_selectedItem().get_value()

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi1 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/GetKPISCorevsTargetforAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);

                    $("#err-msg").hide();

                }
            );
                Highcharts.chart('DIV2_kpivsscore', {
                    chart: {
                        type: 'xy',
                        spacingBottom: 30
                    },
                    title: {
                        text: '',
                        align: 'left',
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontSize: '20px',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    xAxis: {
                        type: "category",
                        color: '#FF00FF'
                    },
                    yAxis: [{
                        allowDecimals: false,
                        tickInterval: 5,
                        min: 0,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF'

                    },
                    {
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        allowDecimals: false,
                        tickInterval: 20,
                        max: 100,
                        labels: {
                            format: '{value}%'
                        }
                    }],
                    tooltip: {
                        // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
                    },
                    plotOptions: {

                    },
                    exporting: {
                        showTable: true
                    },


                    series: [{
                        name: 'Behavior',
                        data: data_bvsk1,
                        type: 'column',
                        yaxis: 0,
                        color: '#27A6D9',
                        marker: {
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        }
                    },
                    {
                        name: 'KPI',
                        type: 'spline',
                        yAxis: 1,
                        data: data_bvsk2,
                        color: '#BC6563',
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + ' %</b>';
                            }
                        }
                    }]
                });
            }
        });




    });

    function getRandomColor(v) {
        var color = ['#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4', '#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4'];


        return color[v];
    }


    function chart1loader(v, label) {  //overall aht
        $('.highcharts-data-table').remove();

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var dataah1 = [];
        var RadDatePickerAHT1 = $find("<%= RadDatePicker1.ClientID %>");
        var selectedDatebAHT1 = RadDatePickerAHT1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerAHT2 = $find("<%= RadDatePicker2.ClientID %>");
        var selectedDateAHT2 = RadDatePickerAHT2.get_selectedDate().format("yyyy-MM-dd");
        var KPIhierarchy = v;

        var DataV = $find('<%=RadComboBox1.ClientID %>').get_selectedItem().get_value();

        $("#freqvsscore").html("Coaching Frequency VS. " + label.toUpperCase() + " Score");

        var highC = Highcharts.chart('Overall_AHT_Agent', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        title: {
                            text: '',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        color: '#FF00FF',
                        opposite: true,
                        min: 0,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }
                    }],
            tooltip: {
                pointFormat: label + ' : <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },


            series: [{
                name: label,
                data: dataah1,
                type: 'column',
                yaxis: 0,
                color: getRandomColor(v),
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebAHT1 + "', EndDate: '" + selectedDateAHT2 + "',  CIMNo: '" + CIMNo + "' ,  KPIhierarchy: '" + KPIhierarchy + "', DataView:'" + DataV + "' }",
            contentType: "application/json; charset=u   tf-8",
            url: URL + "/FakeApi.asmx/getkpiaveragescores_agent",
            // url: URL + "/FakeApi.asmx/getData_overallAHT_ForAgent",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    dataah1.push([f.ADate, f.coached]);

                    $("#err-msg").hide();
                }

            );

                highC.series[0].setData(dataah1);

                highC.hideLoading();

                $("#err-msg").hide();

            }
        });

        highC.showLoading();
    }



    function chart2loader(v, label) {

        var data_voc1 = [];
        var data_voc2 = [];

        var colorx = getRandomColor(v);

        var date1 = $find("<%= dp_fcrglide_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var date2 = $find("<%= dp_fcrglide_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var DV = $find('<%=cb_FCR_Scoredt.ClientID %>').get_selectedItem().get_value();

        $("#tarvsactual").html("Target "+label.toUpperCase() + " Vs. Actual " +label);

        var kpiidxxxx = label;

        var highC = Highcharts.chart('DIV2_FCRGlide', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                title: {
                    text: 'Target ' + label,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                        { allowDecimals: false,
                            title: {
                                text: 'Actual ' + label,
                                style: {
                                    // margin: '50px', // does not work for some reasons, see workaround below
                                    color: '#707070',
                                    fontWeight: 'bold',
                                    textTransform: 'none'
                                }
                            },
                            color: '#FF00FF',
                            opposite: true,
                            min: 0,
                            labels: {
                                formatter: function () {
                                    return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                                }
                            }
                        }],
            tooltip: {

            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'Target ' + label,
                yAxis: 0,
                data: data_voc1,
                type: 'spline',
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                },
                tooltip: {
                    pointFormatter: function () {
                        if (data_voc1[this.category].toString().indexOf('*') >= 0) {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ':<b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + ' </b> fiscal month target change';
                            }
                        } else {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                            }
                        }
                    }
                }
            },
                        {
                            name: 'Actual ' + label,
                            type: 'spline',
                            yAxis: 0,
                            data: data_voc2,
                            color: colorx,
                            lineWidth: 3,
                            marker: {
                                enabled: true,
                                fillColor: '#fff',
                                lineWidth: 2,
                                lineColor: '#ccc'
                            },
                            tooltip: {
                                pointFormatter: function () {
                                    if (this.y == 0 && parseInt(data_voc1[this.index][1]) == 0) {
                                        return label + ' <b>No data available</b>';
                                    } else {
                                        return label + ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                                    }
                                }
                            }
                        }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + date1 + "', EndDate: '" + date2 + "', CIMNo: '" + CIMNo + "', KPI: '" + v + "', DataView: '" + DV + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getGlidepath_agent_bi",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_voc1.push([f.ADate, f.TargetCount]);
                    data_voc2.push([f.ADate, f.CurrentCount]);

                    

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_voc1);
                highC.series[1].setData(data_voc2);

                highC.hideLoading();

            }
        });

        highC.showLoading();

    }

    $(document).ready(function () {

        $("#check_graph").trigger('click');

        var lastClicked = 0;



        $(".click-me-kpi").on("click", function () {
            var v = $(this).attr("kpi-id");
            var label = $(this).text();

            kpiid = v;
            kpilabel = label;


            resetColor(v);


            $("#overall-holder").hide();
            $("#chart-holder").show();



            chart1loader(v, label);
            chart2loader(v, label);

            return false;
        });

        $("#BtnTarVAct").on("click", function () {

            chart2loader(kpiid, kpilabel);

            return false;
        });

        $("#BtnFx").on("click", function () {

            chart1loader(kpiid, kpilabel);

            return false;
        });

        $("#Button1").on("click", function () {

            overall();

            return false;
        });

        $("#Button2").on("click", function () {

            overallagentbehavior();

            return false;
        });

        $("#Button3").on("click", function () {

            overallagentbehaviorvskpi();

            return false;
        });

    });

    function resetColor(v) {
        $.each($('.click-me-kpi'), function (key, xx) {
            var kpiid = $(xx).attr('kpi-id');

            if (kpiid == v) {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            } else {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            }

            $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        });
    }
    
</script>