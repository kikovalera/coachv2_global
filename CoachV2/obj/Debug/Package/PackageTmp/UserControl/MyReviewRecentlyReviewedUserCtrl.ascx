﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MyReviewRecentlyReviewedUserCtrl.ascx.cs" Inherits="CoachV2.UserControl.MyReviewRecentlyReviewedUserCtrl" %>
<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="ReviewsGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ReviewsGrid"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/* added for static header alignment (francis.valera/08092018) */
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>
<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo2" aria-expanded="true" class="collapsed">
        <div class="panel-heading">
            <h4 class="panel-title">
                Recently Reviewed <asp:Label ID="LblDistinctRole" runat="server" Text="Agencia" /> <span class="glyphicon glyphicon-triangle-right triangleright">
                </span> <span class="label label-default"> <asp:Label ID="lblRecentlyReviewed" runat="server" Visible="true"></asp:Label>
                    </span>
            </h4>
        </div>
    </a>
    <div id="collapseTwo2" class="panel-collapse collapse" aria-expanded="true" style="height: 0px;">
        <div class="panel-body">
            <telerik:RadGrid ID="ReviewsGrid" runat="server" AllowAutomaticInserts="False" OnNeedDataSource="ReviewsGrid_NeedDataSource"
                OnItemDataBound="ReviewsGrid_onitemdatabound" OnPreRender="ReviewsGrid_PreRender"
                CssClass="RemoveBorders" BorderStyle="None" RenderMode="Lightweight">

                <%--added for static header alignment (francis.valera/08092018)--%>
                <ClientSettings>
                    <Scrolling UseStaticHeaders="true"/>
                </ClientSettings>

                <MasterTableView CssClass="RadGrid" GridLines="None" AllowPaging="True" PageSize="10"
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" ShowHeadersWhenNoRecords="true">
                    <CommandItemSettings ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="Id" HeaderText="Coaching Ticket" UniqueName="CoachingTicket"
                            FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                            Visible="false" Display="false">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn DataTextField="CoacheeName" HeaderText="Name" UniqueName="CoacheeName"
                            FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Name"
                            AllowSorting="true" ShowSortIcon="true" HeaderStyle-Font-Bold="true">
                        </telerik:GridHyperLinkColumn>
                        <%-- <telerik:GridBoundColumn UniqueName="CoacheeName" HeaderText="Name" DataField="CoacheeName"
                            HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn UniqueName="CIM" HeaderText="CIM" DataField="CoacheeCIM"
                            HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="SessionName" HeaderText="Session Type" DataField="SessionName"
                            HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
<%--                        <telerik:GridBoundColumn UniqueName="TopicName" HeaderText="Topic" DataField="TopicName"
                            HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn UniqueName="TopicName" HeaderText="Topic" SortExpression="TopicName"
                            DataField="TopicName" HeaderStyle-Font-Bold="true" ItemStyle-Width="160px"  >
                            <ItemTemplate>
                                <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "TopicName")%>'  Width="90px" > </asp:Label>
                                <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true" 
                                    Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                    <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="Review Date" DataField="CreatedOn"
                            DataType="System.DateTime" DataFormatString="{0:d}" HeaderStyle-Font-Bold="true">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn UniqueName="StatusName" HeaderText="Status" DataField="StatusName"
                            HeaderStyle-Font-Bold="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="CoachingType" HeaderText="Coaching Type" DataField="Coaching Type"
                            HeaderStyle-Font-Bold="true"> 
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                        <%--added for static header alignment (francis.valera/08092018)--%>
                        <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                        <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                    </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
    </div>
</div>
