﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyTeamPerfUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.MyTeamPerfUserCtrl" %>
<%@ Register src="PerfGraphUserControl.ascx" tagname="PerfGraphUserControl" tagprefix="uc2" %>
<div class="menu-content bg-alt">
        <div class="panel menuheadercustom">
            
            <div>
                &nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard > MY TEAM >  <asp:Label runat="server" ID="LblFullName2" CssClass="breadcrumb2ndlevel" Text="GURL" /> > Analytics
            </div>
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Profile Summary <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                     <div class="panel-body">
                        <div class="container-fluid" id="Div6" runat="server">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div id="profileimageholder">
                                        <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                    </div>
                                </div>                               
                                <div class="form-group col-sm-8" style="padding: 0px">
                                    <form>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span class="namefont">
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <div class="col-sm-12">
                                            <span>
                                                <asp:Label ID="LblMyRole" runat="server" Text="My role" /></span>
                                        </div>
                                    </div>
                                     <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                         <div class="col-sm-12">
                                             <span>
                                                 <asp:Label ID="LblSite" runat="server" Text="Manila.Ortigas" /></span>
                                         </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Reporting Manager:</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="LblMySupervisor" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Region:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblRegion" runat="server" Text="My region" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Country:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblCountry" runat="server" Text="My Manager" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Department:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblDept" runat="server" Text="My Dept" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Client:</label>
                                        <div class="col-sm-8">
                                            <asp:Label ID="LblClient" runat="server" Text="My client" />
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0px; margin-bottom: 0px">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Campaign:</label>
                                        <div class="col-sm-8">
                                             <asp:Label ID="LblCampaign" runat="server" Text="My campaign" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Analytics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        
                        <div class="profiledetails_">
                            
                            <uc2:PerfGraphUserControl ID="PerfGraphUserControl1" runat="server" />
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>