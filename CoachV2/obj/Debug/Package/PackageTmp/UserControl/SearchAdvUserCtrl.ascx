﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SearchAdvUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.SearchAdvUserCtrl" %>
    <%@ Register src="~/UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
    <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
    <div class="row row-custom">            
        <div class="col-sm-12">
            <div class="input-group">
                <asp:TextBox ID="TxtSearch" runat="server" type="text" CssClass="form-control" placeholder="Enter Review Id and press GO" ValidationGroup="SearchForm" />
                <span class="input-group-btn">
                    <asp:Button ID="BtnSearchQuery" runat="server" Text="Go" CssClass="btn btn-info" 
                        OnClick="BtnSearchQuery_Click" UseSubmitBehavior="true" ValidationGroup="SearchForm" />
                </span>
            </div>  
        </div>
    </div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Check the following field(s):" ValidationGroup="SearchForm" ShowMessageBox="true" ShowSummary="false" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" HeaderText="Check the following field(s):" ValidationGroup="SearchCriteria" ShowMessageBox="true" ShowSummary="false" />
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Search <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div>
                <asp:RequiredFieldValidator ID="reqCIM" ControlToValidate="TxtSearchCIM"  ErrorMessage="Search field is required" runat="server" ValidationGroup="SearchForm" Display="None" SetFocusOnError="True" />
            </div>
            <div class="panel-body">
            <!--<div class="pull-right">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="email">
                                CIM No:</label>
                            <asp:TextBox ID="TxtSearchCIM" runat="server" placeholder="Search CIM" CssClass="form-control" />
                        </div>

                        <asp:LinkButton runat="server" ID="BtnSearchCIM" 
                            CssClass="btn btn-md btn-primary" onclick="BtnSearchCIM_Click" ValidationGroup="SearchForm" 
                            ><i class="glyphicon glyphicon-search"></i> Search</asp:LinkButton>
                    </div>
                </div>-->
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th colspan="2">
                                Search by Filters
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="Account" class="control-label col-xs-4">Account</label>
                                        <div class="col-xs-8">
                                            <telerik:RadComboBox ID="DDAccount" runat="server" AutoPostBack="true" Width="100%"
                                                OnSelectedIndexChanged="DDAccount_SelectedIndexChanged" EmptyMessage="Choose Account" AppendDataBoundItems="true">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="reqDDAccount" runat="server" ErrorMessage="Account is required"
                                                ControlToValidate="DDAccount" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Supervisor" class="control-label col-xs-4">Supervisor</label>
                                        <div class="col-xs-8">
                                            <telerik:RadComboBox ID="DDSupervisor" AllowCustomText="false" runat="server"
                                                OnSelectedIndexChanged="DDSupervisor_SelectedIndexChanged" Width="100%" AutoPostBack="true">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="reqDDSupervisor" runat="server" ErrorMessage="Supervisor is required"
                                                ControlToValidate="DDSupervisor" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-xs-4">Coachee Name</label>
                                        <div class="col-xs-8">
                                            <telerik:RadComboBox ID="DDCoachee" AllowCustomText="false" runat="server" Width="100%" AutoPostBack="true"
                                                OnSelectedIndexChanged="DDCoachee_SelectedIndexChanged" AppendDataBoundItems="true">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="reqDDCoachee" runat="server" ErrorMessage="Coachee is required"
                                                ControlToValidate="DDCoachee" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="SessionType" class="control-label col-xs-5">Session Type</label>
                                        <div class="col-xs-6">
                                            <telerik:RadComboBox ID="DDSessionType" runat="server" OnSelectedIndexChanged="DDSessionType_SelectedIndexChanged"
                                                AutoPostBack="true" EmptyMessage="Choose session type" Width="100%">
                                            </telerik:RadComboBox>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07242018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDDSessionType" runat="server" ErrorMessage="Session type is required"
                                                ControlToValidate="DDSessionType" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Topic" class="control-label col-xs-5">Topic</label>
                                        <div class="col-xs-6">
                                            <telerik:RadComboBox ID="DDTopic" runat="server" EmptyMessage="Choose topic" Width="100%"></telerik:RadComboBox>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07242018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDDTopic" runat="server" ErrorMessage="Topic is required"
                                                ControlToValidate="DDTopic" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="followupdate" class="control-label col-xs-5">
                                            Review Date <span class="glyphicon glyphicon-calendar"></span>
                                        </label>
                                        <div class="col-xs-5">
                                            <telerik:RadDatePicker runat="server" ID="DPReviewDateFrom" Width="100%"></telerik:RadDatePicker>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07242018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDPReviewDateFrom" runat="server" ErrorMessage="Review Start Date is required"
                                                ControlToValidate="DPReviewDateFrom"  Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="followupdate" class="control-label col-xs-5"></label>
                                        <div class="col-xs-5">
                                            <telerik:RadDatePicker runat="server" ID="DPReviewDateTo" Width="100%"></telerik:RadDatePicker>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07242018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDPReviewDateTo" runat="server" ErrorMessage="Review End Date is required"
                                                    ControlToValidate="DPReviewDateTo"  Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="dateCompareValidator1" runat="server" ControlToValidate="DPReviewDateTo"
                                                ControlToCompare="DPReviewDateFrom" Operator="GreaterThan" Type="Date" ValidationGroup="SearchCriteria"
                                                ErrorMessage="The review date (end) must be after the first one. " Display="None" SetFocusOnError="true">
                                            </asp:CompareValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="followupdate" class="control-label col-xs-5">
                                            Follow Up Date <span class="glyphicon glyphicon-calendar"></span>
                                        </label>
                                        <div class="col-xs-5">
                                            <telerik:RadDatePicker runat="server" ID="DPFollowUpDateFrom" Width="100%">
                                                <ClientEvents OnDateSelected="OnFollowUpDateFRSelected"/>
                                            </telerik:RadDatePicker>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07232018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDPFollowUpDateFrom" runat="server" ErrorMessage="Follow up start date is required"
                                                ControlToValidate="DPFollowUpDateFrom" Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="followupdate" class="control-label col-xs-5"></label>
                                        <div class="col-xs-5">
                                            <telerik:RadDatePicker runat="server" ID="DPFollowUpDateTo" Width="100%">
                                                <ClientEvents OnDateSelected="OnFollowUpDateTOSelected"/>
                                            </telerik:RadDatePicker>
                                            <%--removed requiredvalidator prod0721 (francis.valera/07232018)--%>
                                            <%--<asp:RequiredFieldValidator ID="reqDPFollowUpDateTo" runat="server" ErrorMessage="Follow up end date is required"
                                                ControlToValidate="DPFollowUpDateTo" ValidationGroup="SearchCriteria" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="dateCompareValidator2"
                                                runat="server" ControlToValidate="DPFollowUpDateTo" ControlToCompare="DPFollowUpDateFrom" Operator="GreaterThan"
                                                Type="Date" ErrorMessage="The follow up date (end) must be after the first one. " Display="None" SetFocusOnError="true" ValidationGroup="SearchCriteria">
                                            </asp:CompareValidator>--%>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <span class="addreviewbuttonholder">
                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-info btn-small"
                    OnClick="BtnSearch_Click" ValidationGroup="SearchCriteria" />
                <%--<input type="reset" class="btn btn-info btn-small" value="Reset" runat="server"  />--%> 
                <asp:Button class="btn btn-info btn-small"  runat="server"  OnClick="BtnReset_Click"  Text="Reset" /> <%--"btn btn-custom btn-small" ValidationGroup="SearchCriteria" --%>
            </span>
        </div>
    </div>
    <br />
    <telerik:RadGrid ID="SearchGrid" runat="server"  onneeddatasource="SearchGrid_NeedDataSource">
        <MasterTableView CssClass="RadGrid" GridLines="None"
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ReviewID" CommandItemDisplay="None">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-8">
                                    
                                        <b><asp:Label ID="Label2" runat="server" Text='<%# Eval("FullNameCoachee")%>' />, 
                                        Ticket #:
                                    <asp:Label ID="TopicName" runat="server" Text='<%# Eval("ReviewID")%>' /></b> 
                                    <asp:HyperLink ID="link1" runat="server" Text="View Here"  Target="_blank" ></asp:HyperLink>
                                     <br />
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description")%>' /><br />
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Strengths")%>' /><br />
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Opportunity")%>' /><br />
                                    <asp:Label ID="Label8" runat="server" Text="  Click here to audit for Triad Coaching :" Visible="false" />                                     
                                    <%--<asp:HyperLink ID="reviewlink" runat="server"  Text='<%# Eval("ReviewID")%>'  Visible="false"  Target="_blank" NavigateUrl='<%# Eval("ReviewID", "~/AddTriadCoaching.aspx?CoachingTicket={0}") %>' ></asp:HyperLink>--%>
                                    <%--<asp:HyperLink ID="reviewlink" runat="server"  Text='<%# Eval("ReviewID")%>'  Visible="false"  Target="_blank" NavigateUrl='<%# enc_text(Eval("ReviewID"),Eval("ReviewID")) %>' ></asp:HyperLink>--%>
                                    <asp:HyperLink ID="reviewlink" runat="server"  Text='<%# Eval("ReviewID")%>'  Visible="false"  Target="_blank" ></asp:HyperLink>
                                </div>
                                <div class="col-xs-4">
                                    R. Date <i class="glyphicon glyphicon-calendar"></i><span>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("CreatedOn", "{0:ddd, MMM d, yyyy}")%>' /></span><br />
                                    F. Date <i class="glyphicon glyphicon-calendar"></i><span>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("FollowDate", "{0:ddd, MMM d, yyyy}")%>' /></span><br />
                                     <i class="glyphicon glyphicon-tags"></i><span>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("SessionName")%>' /></span>
                                    </ul>   <br />
                                    Review Type:    <asp:Label ID="Label9" runat="server"  Font-Bold="true" /></span>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

            </Columns>
        </MasterTableView>
                              <ClientSettings>
                             <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                             <Resizing AllowResizeToFit="True" />
                         </ClientSettings>
    </telerik:RadGrid>
    <script type="text/javascript">
        // added clientside validation to prevent submitting incomplete daterange for followup date (francis.valera/07232018)
        function OnFollowUpDateFRSelected(sender, args) {
            var todate = $find("<%=DPFollowUpDateTo.ClientID %>");
            if (todate.get_selectedDate() == null) {
                var dt = sender.get_selectedDate();
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                todate.set_selectedDate(myDate);
            }
            else if (todate.get_selectedDate() != null && sender.get_selectedDate() > todate.get_selectedDate()) {
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                todate.set_selectedDate(myDate);
            }
        }
        function OnFollowUpDateTOSelected(sender, args) {
            var fromdate = $find("<%=DPFollowUpDateFrom.ClientID %>");
            if (fromdate.get_selectedDate() == null) {
                var dt = sender.get_selectedDate();
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                fromdate.set_selectedDate(myDate);
            }
            else if (fromdate.get_selectedDate() != null && sender.get_selectedDate() < fromdate.get_selectedDate()) {
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                fromdate.set_selectedDate(myDate);
            }
        }

        function OnReviewDateFRSelected(sender, args) {
            var todate = $find("<%=DPReviewDateTo.ClientID %>");
            if (todate.get_selectedDate() == null) {
                var dt = sender.get_selectedDate();
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                todate.set_selectedDate(myDate);
            }
            else if (todate.get_selectedDate() != null && sender.get_selectedDate() > todate.get_selectedDate()) {
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                todate.set_selectedDate(myDate);
            }
        }
        function OnReviewDateTOSelected(sender, args) {
            var fromdate = $find("<%=DPFollowUpDateFrom.ClientID %>");
            if (fromdate.get_selectedDate() == null) {
                var dt = sender.get_selectedDate();
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                fromdate.set_selectedDate(myDate);
            }
            else if (fromdate.get_selectedDate() != null && sender.get_selectedDate() < fromdate.get_selectedDate()) {
                var myDate = new Date(sender.get_selectedDate().format("MM/dd/yyyy"));
                fromdate.set_selectedDate(myDate);
            }
        }
    </script>