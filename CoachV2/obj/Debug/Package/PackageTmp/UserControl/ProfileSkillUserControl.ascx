﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileSkillUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileSkillUserControl" %>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom" runat="server" id="fake_title">
        <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx?tab=skill" ID="RadLinkButton1"
            ForeColor="White" ToolTip="Edit profile" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit trianglebottom"></i>
        </asp:HyperLink>
        <div>
            &nbsp;<span class="glyphicon glyphicon-saved"></span> MY Skill
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Skill <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <telerik:RadGrid ID="EducGrid" runat="server" OnPreRender="EducGrid_PreRender" AllowAutomaticInserts="False"
                        OnNeedDataSource="EducGrid_NeedDataSource" CssClass="RemoveBorders" GridLines="None"  BorderStyle="None" >
                        
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>

                        <MasterTableView CommandItemDisplay="Top"  AllowPaging="True"
                            PageSize="10" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="SkillID"> <%--CssClass="RadGrid" GridLines="None"--%>
                            <CommandItemSettings ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Description" HeaderText="Skill Description"
                                    DataField="Description" HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Rating" HeaderText="Rating" DataField="Rating"
                                    HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>

                            </Columns>
                        </MasterTableView>
                          <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
</div>
