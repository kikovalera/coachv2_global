﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QAChartDB.ascx.cs" Inherits="CoachV2.QAChartDB" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="panel-group" id="Div3">
    <div class="row row-custom">
        <div class="col-sm-5">
            <form>
            <div class="input-group">
                </span>
            </div>
            </form>
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseQAnonSub">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Charts (QA without subordinates)<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>
                </h4>
            </div>
        </a>
        <br />
    </div>
    <div id="collapseQAnonSub" class="panel-collapse collapse in">
        <div class="panel-body">
       
            <asp:Panel runat="server" ID="Pn_overall" Visible="true">
                <label id="lblstartdate" runat="server">
                    To:
                </label>
                <telerik:RadDatePicker ID="dpstartdate" runat="server" Culture="en-US" ResolvedRenderMode="Classic">
                </telerik:RadDatePicker>
                <label id="lblenddate" runat="server">
                    From:
                </label>
                <telerik:RadDatePicker ID="dpenddate" runat="server">
                </telerik:RadDatePicker>
                <telerik:RadButton ID="Btn_generate1" runat="server" Text="Generate" OnClick="Btn_generate1_Click">
                </telerik:RadButton>
                <br />
                <telerik:RadHtmlChart runat="server" ID="RHTML_Frequency">
                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Coaching Frequency" DataFieldY="coached">
                            </telerik:ColumnSeries>
                        </Series>
                        <XAxis DataLabelsField="date">
                        </XAxis>
                        <YAxis>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="true" />
                    </Legend>
                    <%--<ChartTitle Text="Agent Coaching Frequency"></ChartTitle>--%>
                    <ChartTitle Text="Coaching Frequency"></ChartTitle>
                    <%--removed AGENT in label to eliminate confusion in display (francis.valera/07132018--%>
                </telerik:RadHtmlChart>
                <br />
                <label id="Label5" runat="server">
                    To:
                </label>
                <telerik:RadDatePicker ID="dp_behaviorstart" runat="server" Culture="en-US" ResolvedRenderMode="Classic">
                </telerik:RadDatePicker>
                <label id="Label6" runat="server">
                    From:
                </label>
                <telerik:RadDatePicker ID="dp_behaviorend" runat="server">
                </telerik:RadDatePicker>
                <telerik:RadButton ID="btn_generatebehavior" runat="server" Text="Generate" OnClick="btn_generatebehavior_Click">
                </telerik:RadButton>
                <br />
                <telerik:RadHtmlChart runat="server" ID="RHTMLBehavior">
                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Behavior" DataFieldY="coacheecount">
                            </telerik:ColumnSeries>
                        </Series>
<%--                <XAxis DataLabelsField="driver_name">
                </XAxis>
                        --%>
                        <XAxis DataLabelsField="description">
                            <LabelsAppearance RotationAngle="295">
                            </LabelsAppearance>
                        </XAxis>
                        <YAxis>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="true" />
                    </Legend>
                    <ChartTitle Text="Top Behaviors Overall">
                    </ChartTitle>
                </telerik:RadHtmlChart>
                <br />
                <label id="Label2" runat="server">
                    To:
                </label>
                <telerik:RadDatePicker ID="dp_Start_kpivsd" runat="server" Culture="en-US" ResolvedRenderMode="Classic">
                </telerik:RadDatePicker>
                <label id="Label3" runat="server">
                    From:
                </label>
                <telerik:RadDatePicker ID="dp_end_kpivsd" runat="server">
                </telerik:RadDatePicker>
                <label id="Label4" runat="server">
                    KPI:
                </label>
                <telerik:RadComboBox ID="cb_KPIList" runat="server" DataValueField="KPIID" DataTextField="Name"
                    ResolvedRenderMode="Classic">
                </telerik:RadComboBox>
                <br />
                <telerik:RadButton ID="btn_generate_driver" runat="server" Text="Generate" OnClick="btn_generate_driver_Click">
                </telerik:RadButton>
                <br />
                <telerik:RadHtmlChart runat="server" ID="RHTMLKPIVsDriver">
                    <PlotArea>
                        <Series>
                            <telerik:ColumnSeries Name="Behavior" DataFieldY="coachedkpi">
                            </telerik:ColumnSeries>
                            <telerik:LineSeries Name="Cumulative %" DataFieldY="AvgCurr">
                            </telerik:LineSeries>
                        </Series>
                        <XAxis DataLabelsField="driver_name">
                        </XAxis>

                        <YAxis>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="true" />
                    </Legend>
                    <ChartTitle Text="Behavior Vs KPI">
                    </ChartTitle>
                </telerik:RadHtmlChart>
                <br />
                <label id="Label17" runat="server">
                    To:
                </label>
                <telerik:RadDatePicker ID="dp_score_start" runat="server" Culture="en-US" ResolvedRenderMode="Classic">
                </telerik:RadDatePicker>
                <label id="Label18" runat="server">
                    From:
                </label>
                <telerik:RadDatePicker ID="dp_score_end" runat="server">
                </telerik:RadDatePicker>
                <label id="Label19" runat="server">
                    KPI:
                </label>
                <telerik:RadComboBox ID="cb_KPIListScore" runat="server" DataValueField="KPIID" DataTextField="Name"
                    ResolvedRenderMode="Classic">
                </telerik:RadComboBox>
                <br />
                <telerik:RadButton ID="btn_Scr" runat="server" Text="Generate" OnClick="btn_Scr_Click">
                </telerik:RadButton>
              <telerik:RadHtmlChart runat="server" ID="RHTMLSCORE">
                    <PlotArea>
                        <Series>
                         <telerik:LineSeries Name="Score" DataFieldY="Ave_Current">
                            </telerik:LineSeries>
                            <telerik:ColumnSeries Name="Total number of Coaching" DataFieldY="coachedtotal">
                            </telerik:ColumnSeries>
                        </Series>
                        <XAxis DataLabelsField="date">
                        </XAxis>
                        <YAxis>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="true" />
                    </Legend>
                    <ChartTitle Text="Coaching Frequency Vs. Score">
                    </ChartTitle>
                </telerik:RadHtmlChart>
                <br />
                <label id="Label20" runat="server">
                    To:
                </label>
                <telerik:RadDatePicker ID="dp_currstart" runat="server" Culture="en-US" ResolvedRenderMode="Classic">
                </telerik:RadDatePicker>
                <label id="Label21" runat="server">
                    From:
                </label>
                <telerik:RadDatePicker ID="dp_currend" runat="server">
                </telerik:RadDatePicker>
                <label id="Label22" runat="server">
                    KPI:
                </label>
                <telerik:RadComboBox ID="cb_KPIListCurr" runat="server" DataValueField="KPIID" DataTextField="Name"
                    ResolvedRenderMode="Classic">
                </telerik:RadComboBox>
                <br />
                <telerik:RadButton ID="btn_Curr" runat="server" Text="Generate" OnClick="btn_Curr_Click">
                </telerik:RadButton>
                <br />
                <telerik:RadHtmlChart runat="server" ID="RHTML_CurrScore">
                    <PlotArea>
                        <Series>
                            <telerik:LineSeries Name="Target" DataFieldY="target">
                            </telerik:LineSeries>
                            <telerik:LineSeries Name="Average Actual Score" DataFieldY="Ave_Current">
                            </telerik:LineSeries>
                            <telerik:ColumnSeries Name="Number of coached" DataFieldY="coachedtotal">
                            </telerik:ColumnSeries>
                        </Series>
                        <XAxis DataLabelsField="date">
                        </XAxis>
                        <YAxis>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="true" />
                    </Legend>
                    <ChartTitle Text="Glide Path Targets Vs. Actual">
                    </ChartTitle>
                </telerik:RadHtmlChart>
            </asp:Panel>
        </div>
        </div>