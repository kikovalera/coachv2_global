﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddCMTReview.ascx.cs" Inherits="CoachV2.UserControl.AddCMTReview" %>
<div class="panel-group" id="accordion">
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coaching Specifics CMT <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="Account" class="control-label col-xs-2">
                                Coaching Ticket</label>
                            <div class="col-xs-3">
                                <telerik:RadTextBox ID="RadReviewID" runat="server" Width="100%" class="form-control" Text="0"
                                    ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                             <label for="Account" class="control-label col-xs-3">
                                Coaching Date</label>
                            <div class="col-xs-4">
                                <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control" Text="0"
                                    ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                        </div>
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>
                                        Select Coachee
                                    </th>
                                    <th>
                                        Choose Review Specifics
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                 
                                        <div class="form-group">
                                            <label for="Account" class="control-label col-xs-4">
                                                Account</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadAccount" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="1" Width="100%" OnSelectedIndexChanged="RadAccount_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                                    ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                    CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Supervisor" class="control-label col-xs-4">
                                                Supervisor</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSupervisor" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="2" Width="100%" OnSelectedIndexChanged="RadSupervisor_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                                    ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="CoacheeName" class="control-label col-xs-4">
                                                Coachee Name</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadCoacheeName" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="3" Width="100%">
                                                </telerik:RadComboBox>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                                    ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                     
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="SessionType" class="control-label col-xs-4">
                                                Session Type</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true"
                                                    RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="4" Width="100%" OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group">
                                            <label for="Topic" class="control-label col-xs-4">
                                                Topic</label>
                                            <div class="col-xs-8">
                                                <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                                    TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="RadSessionTopic_SelectedIndexChanged" Width="100%">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div class="form-group" runat="server" id="CheckBoxes">
                                            <div class="col-xs-9">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Include Previous Coaching Notes"
                                                    OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" Width="100%" /><br />
                                                <asp:CheckBox ID="CheckBox2" runat="server" Text="Include Previous Performance Results"
                                                    OnCheckedChanged="CheckBox2_CheckedChanged" AutoPostBack="true" /><br />
                                            </div>
                                        </div>
                                        <div>
                                            &nbsp;
                                        </div>
                                        <div>
                                            &nbsp;</div>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Description"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="AddReview"
                            ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                            CssClass="validator"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
           
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Strengths <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strengths"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ValidationGroup="AddReview"
                        ControlToValidate="RadStrengths" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                        CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Opportunities <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseFive" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunities"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="10">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                        ControlToValidate="RadOpportunities" Display="Dynamic" ErrorMessage="*This is a required field."
                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Commitment <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseSix" class="panel-collapse collapse in">
                <div class="panel-body">
                    <telerik:RadTextBox ID="RadCommitment" runat="server" class="form-control" placeholder="Commitment"
                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="11">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="AddReview"
                        ControlToValidate="RadCommitment" Display="Dynamic" ErrorMessage="*This is a required field."
                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
       <div style="height:5px"></div>
    
       <div class="panel-body">
            <div class="form-group">
                <div class="col-xs-6 col-sm-3">
                    <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                        TabIndex="12" placeholder="Coachee CIM Number">
                    </telerik:RadTextBox>
                </div>
                 <div class="col-xs-6 col-sm-3">
                    <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="SSN"
                        RenderMode="Lightweight" TabIndex="13" TextMode="Password"
                        MaxLength="3">
                    </telerik:RadTextBox>
                 </div>
                <div class="col-md-4">
                    <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                        CssClass="btn btn-primary" ValidationGroup="AddReview" OnClick="RadCoacheeSignOff_Click"
                        ForeColor="White">
                    </telerik:RadButton>
                    <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off"
                        CssClass="btn btn-primary" ValidationGroup="AddReview" OnClick="RadCoacherSignOff_Click"
                        ForeColor="White">
                    </telerik:RadButton>
                </div>
            </div>
        </div>
         <div class="form-group">
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Width="1000px" Height="500px">
                <Windows>
                    <telerik:RadWindow runat="server" ID="Window1" Modal="true" Behaviors="Maximize,Minimize">
                        <ContentTemplate>
                            <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                                Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic">
                                <MasterTableView DataKeyNames="ReviewID">
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                        </telerik:GridClientSelectColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                    <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <telerik:RadButton ID="RadCNLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNLink_Click">
                                        </telerik:RadButton>
                                        &nbsp;
                                        <telerik:RadButton ID="RadCNCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadCNCancel_Click">
                                        </telerik:RadButton>
                                    </span>
                                </div>
                                </div>
                        </ContentTemplate>
                    </telerik:RadWindow>
                    <telerik:RadWindow runat="server" ID="Window2" Modal="true" Behaviors="Maximize,Minimize">
                        <ContentTemplate>
                            <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                                Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic">
                                <MasterTableView DataKeyNames="ReviewKPIID">
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                        </telerik:GridClientSelectColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                    <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <telerik:RadButton ID="RadPRLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadPRLink_Click">
                                        </telerik:RadButton>
                                        &nbsp;
                                        <telerik:RadButton ID="RadPRCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                                            Height="30px" OnClick="RadPRCancel_Click">
                                        </telerik:RadButton>
                                    </span>
                                </div>
                        </ContentTemplate>
                    </telerik:RadWindow>
                </Windows>
            </telerik:RadWindowManager>
        </div>
      </div>  