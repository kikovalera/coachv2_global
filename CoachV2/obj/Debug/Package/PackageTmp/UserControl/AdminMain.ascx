﻿<%@ Control Language="C#" AutoEventWireup="true"   CodeBehind="AdminMain.ascx.cs" Inherits="CoachV2.UserControl.AdminMain" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
 <script src="../libs/js/JScript1.js" type="text/javascript"></script>
<link href="../libs/css/gridview.css" rel="stylesheet" type="text/css" />

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
	 
}

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

div.scrollsides
{
    border-width:0;   
    overflow:auto;
padding-left:7px;
border-width:0 0 1px 0; 
}


</style>
<script type="text/javascript">
    function OnClientTabSelected(sender, eventArgs) {
        var tab = eventArgs.get_tab();
        alert(tab.get_text());
    }
</script>
 <telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">
            function BtnDummy(sender, args) {
                var clickButton = document.getElementById("<%= btnDummy.ClientID %>");
                clickButton.click();
            }
            function confirmAddition(upload, args) {
                args.set_cancel(!confirm("Are you sure you want to upload the selected file?"));
            }          
        </script>          
    </telerik:RadScriptBlock>
<div class="menu-content bg-alt">                      
 <div class="panel menuheadercustom">
    <div>
        &nbsp; 
         <span class="glyphicon glyphicon-cog"></span> SETTINGS 
        <span class="breadcrumb2ndlevel"><asp:Label
            ID="Label1" runat="server" Text=" "></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span></div>
</div>        
        
<asp:Button ID="Btn_KPI" runat="server" Text="KPI"  Visible="false"/> 
<asp:Button ID="Btn_Admin" runat="server" Text="Administrator" Visible="false" /> 


 <div class="panel-group" id="Div2">
     <div class="row">
         <div class="col-sm-12">
             <a href="#" class="btn" runat="server" visible="false" id="ic_users"><span class="glyphicon glyphicon-user"></span>Users</a>
             <a href="#" class="btn" runat="server" visible="false" id="ic_kpi"><span class="glyphicon glyphicon-th-large"></span>KPI</a>
         </div>
     </div>
     </div>



<asp:Panel ID="Pn_Admin" runat="server" Visible="false">

<div class="panel-group" id="accordion">
              <div class="row row-custom">
                <div class="col-sm-5">
                  <form>
                    <div class="input-group">
                      </span>
                    </div>
                  </form>
                </div>
              </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <div class="panel-heading">
                <h4 class="panel-title">
                    User Rights Management <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>
                </h4>
            </div>
        </a>
        </div>
    <div id="collapseOne" class="panel-collapse collapse in">
              
                <div class="panel-body">
                    <asp:Label ID="lblCimnumber" runat="server">Cim Number :</asp:Label>
                  <%--  <telerik:RadTextBox ID="txtCimnumber" runat="server" InputType="Number" >
                    </telerik:RadTextBox>--%>
                       <telerik:RadNumericTextBox ID="txtCimnumber" runat="server" MinValue="0" MaxValue="999999999" >
                        <NumberFormat  GroupSeparator="" />                      
                      </telerik:RadNumericTextBox>  

                      <asp:Button ID="btn_Search" runat="server" Text="Search" OnClick="btn_Search_Click" CssClass="btn btn-info btn-small">
                      </asp:Button>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    Category<span class="glyphicon glyphicon-triangle-bottom"></span>
                                </th>
                                <th>
                                    Access
                                    <br />
                                    Rights<span class="glyphicon glyphicon-triangle-bottom"></span>
                                </th>
                                <th>
                                    Grant
                                    <br />
                                    Access <span class="glyphicon glyphicon-triangle-bottom"></span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    General
                                </th>
                                <td>
                                    Login
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_Login" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    My Team
                                </th>
                                <td>
                                    Add Review
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_AddReview" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Performance Summary
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_perfsummary" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Review Logs
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_reviewLogs" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Profile
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_profile" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    My Reviews
                                </th>
                                <td>
                                    Add New Review
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_addnewreview" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Add Mass Reviews
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_addMassReview" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Add QA Review
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_qareview" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Remote Coach</th>
                                    <td>
                                        <asp:CheckBox ID="cb_remotecoach" runat="server" />
                                    </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Search Reviews</th>
                                    <td>
                                        <asp:CheckBox ID="cb_searchReviews" runat="server" />
                                    </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    My Reports
                                </th>
                                <td>
                                    Report Builder
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_ReportBuilder" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Built In Reports
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_BuiltInReports" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Saved Reports
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_SavedReports" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    My Settings
                                </th>
                                <td>
                                    Users
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_Users" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    KPI
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_KPI" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Search Users
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_SearchUsers" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                </th>
                                <td>
                                    Receive Notifications
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_notifications" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    My Profile
                                </th>
                                <td>
                                    Edit Profile
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_EditProfile" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                      <div class="row">
                          <br>
                          <div class="col-sm-12">
                              <span class="addreviewbuttonholder">
                                  <asp:Button ID="btn_submit" runat="server" Text="Submit" OnClick="btn_submit_OnClick" CssClass="btn btn-info btn-small" ForeColor="White"/>
                                  <asp:Button ID="btn_reset" runat="server" Text="Reset" OnClick="btn_reset_OnClick" CssClass="btn btn-info btn-small" ForeColor="White" />
                              </span>
                          </div>
                      </div>
                  </div>
               </div>
        
</div>

</asp:Panel>

<asp:Panel ID="Pn_KPI"  Visible="false" runat="server">
    <div class="panel-group" id="Div1">
            <asp:Panel ID="Pn_KPIList" runat="server"  >
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_kpi1">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Add new KPI<span class="glyphicon glyphicon-triangle-bottom trianglebottom"> </span>
                            </h4>
                        </div>
                    </a>                    
                    <div id="collapse_kpi1" class="panel-collapse collapse in">
                      <div class="panel-body" style="padding-bottom:5px" >
                        <div class="panel-body" style="padding-bottom:1px">
                            <div  class="form-group" >
                                <label for="kpiname" class="control-label col-sm-2">
                                    KPI Name</label>
                                <div class="col-sm-2">
                                    <telerik:RadComboBox ID="cb_kpiname" DataSourceID="ds_kpiname" Filter="Contains"
                                        runat="server" DataValueField="ID" DataTextField="KPI" CausesValidation="true">
                                    </telerik:RadComboBox>
                                    <asp:SqlDataSource ID="ds_kpiname" runat="server" SelectCommand="pr_Coach_GetKPIFromScorecardList"
                                        SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                    </asp:SqlDataSource>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <label for="linkto" class="control-label col-sm-2">
                                    Account</label>
                                <div class="col-sm-2">
                                    <telerik:RadComboBox ID="cb_account" Filter="Contains" runat="server" DataSourceID="SqlDataSource3"
                                        DataTextField="Account" DataValueField="AccountID" CausesValidation="true">
                                    </telerik:RadComboBox>
                                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" SelectCommand="pr_Coach_getActiveAccount"
                                        SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                    </asp:SqlDataSource>
                                </div>
                                <div class="col-sm-2">
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="padding-bottom:1px" >
                        <div class="form-group">
                            <label for="valuetype" class="control-label col-sm-2">
                                Value Type</label>
                            <div class="col-sm-2">
                                <telerik:RadComboBox ID="cb_valuetype" DataSourceID="SqlDataSource1" runat="server"
                                    DataTextField="valuetoname" DataValueField="valueid" CausesValidation="true"
                                    RenderMode="Lightweight">
                                </telerik:RadComboBox>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="SELECT  [valueid],[valuetoname] FROM [CoachV2].[dbo].[tbl_Coach_Valueto] where [hidefromlist] = 0"
                                    SelectCommandType="Text" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                </asp:SqlDataSource>
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                        </div>
                        </div>
                        <div class="panel-body" style="padding-bottom:1px" >
                         <div class="form-group">
                            <label for="linkto" class="control-label col-sm-2">
                                Link to
                            </label>
                            <div class="col-sm-2">
                                <telerik:RadComboBox ID="cb_linkto" DataSourceID="SqlDataSource2" runat="server"
                                    ResolvedRenderMode="Classic" DataTextField="linktype" DataValueField="id" CausesValidation="true"
                                    RenderMode="Lightweight">
                                </telerik:RadComboBox>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" SelectCommand="SELECT [id] ,[linktype]  FROM [CoachV2].[dbo].[tbl_Coach_LinkType] where hidefromlist = 0"
                                    SelectCommandType="Text" ConnectionString="<%$ ConnectionStrings:cn_CoachV2 %>">
                                </asp:SqlDataSource>
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <label for="linkto" class="control-label col-sm-2">
                            </label>
                            <div class="col-sm-2">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                                <br />
                                <div class="col-sm-12">
                                    <span class="addreviewbuttonholder">
                                        <asp:Button ID="Button1" runat="server" Text="Add KPI" OnClick="btn_Button1" CssClass="btn btn-info btn-small"
                                            ForeColor="White" />
                                    </span>
                                </div>
                            </div>
                    </div>
               </div>
                </div>
                <div style="height: 5px">
                </div>
                <div style="height: 5px">
                </div>
                <div style="height: 5px">
                </div>
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_kpi2">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                KPI List<span class="glyphicon glyphicon-triangle-bottom trianglebottom"> </span>
                            </h4>
                        </div>
                    </a>
                    <div id="collapse_kpi2" class="panel-collapse collapse in">
                        <div class="panel-body">
                        <asp:Label ID="lblForKpiCreated" runat="server" Visible="true"></asp:Label>
     
                <telerik:RadGrid ID="RadGrid1" OnItemDataBound="radgrid1_databound"  OnUpdateCommand="radgrid1_UpdateCommand"
                    EnableLoadOnDemand="True"  EnableViewState="true"  AllowAutomaticInserts="false" AllowAutomaticUpdates="true"
                    OnDeleteCommand="RadGrid1_DeleteCommand" RenderMode="Auto"
                    AllowPaging="True" runat="server" AllowFilteringByColumn="false" AutoGenerateColumns="false"
                    GroupPanelPosition="Top" ResolvedRenderMode="Classic" CssClass="RemoveBorders"
                    BorderStyle="None"  >
                    <MasterTableView DataKeyNames="Row"  EditMode="EditForms"  TableLayout="Auto"> <%--EditMode="EditForms"--%>
                        <Columns>
                  <telerik:GridTemplateColumn UniqueName="Rows"  DataField="Row" HeaderText="KPI" HeaderStyle-Font-Bold="true" Visible="false" Display="false">
                   <ItemTemplate>
                  <%# DataBinder.Eval(Container.DataItem, "Row")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="txtRow" Text='<%# DataBinder.Eval(Container.DataItem, "Row") %>'
                            Visible="false" Enabled="false"   />
                    </EditItemTemplate>  
                    </telerik:GridTemplateColumn>
                    
                       <telerik:GridTemplateColumn UniqueName="Rows1"  DataField="Num" HeaderText="Rows" HeaderStyle-Font-Bold="true" Visible="true" >
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Num")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="txtRowpartition" Text='<%# DataBinder.Eval(Container.DataItem, "Num") %>'
                            Visible="true" Enabled="false" />
                    </EditItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="KPI_Name"  DataField="KPI Name" HeaderText="KPI Name" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "KPI Name" )%>
                    </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="cb_kpiname" Filter="Contains"  DataValueField="ID" DataTextField="KPI"
                                runat="server"  Width="100%" CausesValidation="true">
                            </telerik:RadComboBox>  
                        </EditItemTemplate>
                </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="Value_Type"  DataField="Value Type" HeaderText="Value Type" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Value Type")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadComboBox ID="cb_valuetype" runat="server"
                            DataTextField="valuetoname" DataValueField="valueid" CausesValidation="true">
                        </telerik:RadComboBox>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                   <telerik:GridTemplateColumn UniqueName="Assign_Type"  DataField="Assign_Type" HeaderText="Assign Type" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Assign Type")%>
                    </ItemTemplate>
                       <EditItemTemplate>
                           <telerik:RadComboBox ID="cb_linkto"  runat="server"
                               ResolvedRenderMode="Classic" DataTextField="linktype" DataValueField="id" CausesValidation="true">
                           </telerik:RadComboBox>
                       </EditItemTemplate>
                </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="Assigned_to" DataField="Assigned to" HeaderText="Assigned to"
                                HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "Assigned to")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="cb_account" Filter="Contains" runat="server" 
                                        Width="100%" DataTextField="Account" DataValueField="AccountID" CausesValidation="true">
                                    </telerik:RadComboBox>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn UniqueName="Created_by" HeaderText="Created by" DataField="Created by"
                                HeaderStyle-Font-Bold="true"  ReadOnly="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                  <telerik:GridEditCommandColumn UniqueName="EditCommandColumn"   >
                    </telerik:GridEditCommandColumn>
                   <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Text="Delete"  ButtonType="LinkButton" ConfirmTextFormatString="Are you sure you want to delete the selected item?" ConfirmTextFields="Model" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                <ClientSettings>
                    <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:RadGrid ID="grd_KPILIst" ClientSettings-Resizing-AllowResizeToFit="true" RenderMode="Auto"
                    AllowPaging="True" runat="server" AllowFilteringByColumn="false" AutoGenerateColumns="false"
                    GroupPanelPosition="Top" ResolvedRenderMode="Classic" CssClass="RemoveBorders"
                    BorderStyle="None" Visible="false">
                    <ClientSettings>
                        <Scrolling AllowScroll="True" EnableVirtualScrollPaging="true"/>
                        <Resizing AllowResizeToFit="True" />
                    </ClientSettings>
                    <MasterTableView>
                        <Columns>
                            <telerik:GridBoundColumn UniqueName="Rows" HeaderText="Row" DataField="Row" HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="KPI_Name" HeaderText="KPI Name" DataField="KPI Name"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Value_Type" HeaderText="Value Type" DataField="Value Type"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Assign_Type" HeaderText="Assign Type" DataField="Assign Type"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Assigned_to" HeaderText="Assigned to" DataField="Assigned to"
                                HeaderStyle-Font-Bold="true">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                   <span class="glyphicon glyphicon-align-justify" data-html="true" tabindex="0" data-trigger="focus" data-toggle="popover" 
                                   data-content="<span class='glyphicon glyphicon-edit'></span> <a href='Admin.aspx?Row=<%# Eval("Row") %>&Type=1' style='color: #444;'>Edit<br><span class='glyphicon glyphicon-erase'></span>  <a href='Admin.aspx?Row=<%# Eval("Row") %>&Type=2' style='color: #444;'>Delete" </span>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
             
                 </div>
                 </div>
                 </div>
                </asp:Panel>
    </div> 
</asp:Panel>

<asp:Panel ID="Pn_Lookup" runat="server"  Visible="false">

<div class="panel panel-custom">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapselookup">
        <div class="panel-heading">
            <h4 class="panel-title">
                Look Up Management <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
               </span>
            </h4>
        </div>
    </a>
    <div id="collapselookup" class="panel-collapse collapse in">
        <div class="panel-body">
           
            <telerik:RadButton ID="btnKPI" runat="server" Text="KPI and Driver List"   OnClick="btnKPI_onclick" >
            </telerik:RadButton>
            <telerik:RadButton ID="btnSession" runat="server" Text="Session and Topic List"  OnClick="btnSession_onclick" >
            </telerik:RadButton>
            <telerik:RadButton ID="btnAcct" runat="server" Text="Accounts List"   OnClick="btnAcct_onclick" >
            </telerik:RadButton>
            <telerik:RadButton ID="btnSup" runat="server" Text="Supervisor List" OnClick="btnSup_onclick" >
            </telerik:RadButton>

            <asp:Panel ID="PN_LOOKUP_GRID" runat="server"   >
                <telerik:RadGrid ID="grd_Drivers" runat="server"  ClientSettings-Resizing-AllowResizeToFit="true"
                    AllowFilteringByColumn="True" GroupPanelPosition="Top" ResolvedRenderMode="Classic"
                    CssClass="RemoveBorders" BorderStyle="None">
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:RadGrid ID="grd_session" runat="server"  Visible="false" ClientSettings-Resizing-AllowResizeToFit="true"
                    AllowFilteringByColumn="True" GroupPanelPosition="Top" ResolvedRenderMode="Classic"
                    CssClass="RemoveBorders" BorderStyle="None">
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:RadGrid ID="grd_Account" runat="server"   Visible="false" ClientSettings-Resizing-AllowResizeToFit="true"
                     AllowFilteringByColumn="True" GroupPanelPosition="Top" CssClass="RemoveBorders"
                    BorderStyle="None" ResolvedRenderMode="Classic">
                    <ClientSettings>
                        <Scrolling AllowScroll="True"   />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:RadGrid ID="grd_Supervisor" runat="server"  Visible="false"  ClientSettings-Resizing-AllowResizeToFit="true"
                     AllowFilteringByColumn="True" GroupPanelPosition="Top" CssClass="RemoveBorders"
                    BorderStyle="None" ResolvedRenderMode="Classic">
                    <ClientSettings>
                        <Scrolling AllowScroll="True"  />
                    </ClientSettings>
                </telerik:RadGrid>
            </asp:Panel>

      
        </div>
    </div>
 </div>


</asp:Panel>

<asp:Panel ID="Pn_Upload" runat="server">
  <div class="panel panel-custom">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          <div class="panel-heading">
              <h4 class="panel-title">
                  Upload Agent Performance <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                  </span>
              </h4>
          </div>
      </a>
      <div id="Div3" class="panel-collapse collapse in">
          <div class="panel-body">
              <div class="form-group">
                  <div class="col-sm-12">
                      <asp:Label ID="LabelHost" runat="server" Visible="false"></asp:Label>
                      <asp:Label ID="LabelPath" runat="server" Visible="false"></asp:Label>
                      <asp:HiddenField ID="HiddenUploadID" runat="server" Value="0" />
                      <asp:HiddenField ID="HiddenFileName" runat="server" />
                      <telerik:RadGrid ID="RadGridUpload" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                          AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                          CssClass="RemoveBorders" OnItemDataBound="RadGridUpload_onItemDatabound" BorderStyle="None"
                          GridLines="None">
                          <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                              <Columns>
                                  <telerik:GridTemplateColumn HeaderText="Item Number">
                                      <ItemTemplate>
                                          <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                      </ItemTemplate>
                                  </telerik:GridTemplateColumn>
                                  <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                      FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                  </telerik:GridHyperLinkColumn>
                                  <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                      UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                      AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                  </telerik:GridHyperLinkColumn>
                                  <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                      <ItemTemplate>
                                          <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                      </ItemTemplate>
                                  </telerik:GridTemplateColumn>
                                  <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                      <ItemTemplate>
                                          <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                      </ItemTemplate>
                                  </telerik:GridTemplateColumn>
                              </Columns>
                          </MasterTableView>
                          <ClientSettings>
                              <Scrolling AllowScroll="True" />
                          </ClientSettings>
                      </telerik:RadGrid>
                  </div>
              </div>
          </div>
          <div class="panel-body">
              <telerik:RadAsyncUpload ID="RadUpload" runat="server" MultipleFileSelection="Disabled"
                  OnClientValidationFailed="OnClientValidationFailedAgentPerformance" AllowedFileExtensions=".xlsx"
                  PostbackTriggers="btnDummy" MaxFileSize="1000000" Width="40%" OnClientFileUploaded="BtnDummy"
                  OnClientFileUploading="confirmAddition">
                  <Localization Select="                             " />
              </telerik:RadAsyncUpload>
          </div>
          <div class="panel-body">
              <div class="container-fluid" id="CMTButtons" runat="server">
                  <div class="row">
                      <div class="col-md-11 text-right">
                          <telerik:RadButton runat="server" ID="btnDummy" CssClass="btn btn-info btn-small"
                              Text="Upload" ForeColor="White" Style="display: none;" OnClick="btnDummy_Click" />
                      </div>
                  </div>
              </div>
          </div>
      </div>
        </div>
</asp:Panel>

</div>   