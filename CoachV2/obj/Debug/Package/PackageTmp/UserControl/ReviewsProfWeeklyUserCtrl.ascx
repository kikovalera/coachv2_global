﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewsProfWeeklyUserCtrl.ascx.cs" Inherits="CoachV2.UserControl.ReviewsProfWeeklyUserCtrl" %>


<style type="text/css">
/*added for static header alignment */
.rgDataDiv
{
    overflow-x: hidden !important;
}
</style>

<telerik:RadGrid ID="ReviewGrid" runat="server" AllowAutomaticInserts="False" 
    onprerender="ReviewGrid_PreRender">
    
    <%--added for static header alignment--%>
    <ClientSettings>
        <Scrolling UseStaticHeaders="true"/>
    </ClientSettings>

    <MasterTableView CssClass="RadGrid" GridLines="None" AllowPaging="True"
        PageSize="10" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" CommandItemDisplay="None">
        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false"  />
        <Columns>
            <telerik:GridBoundColumn UniqueName="SessionType" HeaderText="Session Type"
                DataField="SessionName" HeaderStyle-Font-Bold="true" ShowSortIcon="true">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="TopicName" HeaderText="Topic"
                DataField="TopicName" HeaderStyle-Font-Bold="true" ShowSortIcon="true">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="StatusName" HeaderText="Status"
                DataField="StatusName" HeaderStyle-Font-Bold="true" ShowSortIcon="true">
            </telerik:GridBoundColumn>
          
            <telerik:GridDateTimeColumn UniqueName="ReviewDate" HeaderText="Review Date" DataField="CreatedOn"
                                    DataType="System.DateTime" DataFormatString="{0: MMM dd, yyyy}" HeaderStyle-Font-Bold="true" />
            
            <telerik:GridDateTimeColumn UniqueName="FollowDate" HeaderText="Follow up Date" DataField="FollowDate"
                                    DataType="System.DateTime" DataFormatString="{0: MMM dd, yyyy}" HeaderStyle-Font-Bold="true" />
                                    
            <%--added for static pager alignment--%>
            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
            </telerik:GridTemplateColumn>

        </Columns>

    </MasterTableView>
    <ClientSettings>
        <Scrolling AllowScroll="True" />
    </ClientSettings>
</telerik:RadGrid>