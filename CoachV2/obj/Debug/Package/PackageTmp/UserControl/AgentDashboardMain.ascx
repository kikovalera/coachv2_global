﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AgentDashboardMain.ascx.cs" Inherits="CoachV2.UserControl.AgentDashboardMain" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="grd_RecentEscalation">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_RecentEscalation"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueFollowups">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueFollowups"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueSignOffs">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueSignOffs"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="panel menuheadercustom">
    &nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard<span class="breadcrumb2ndlevel"><asp:Label
        ID="Label1" runat="server" Text=" "></asp:Label></span>
</div>

<div class="panel-group" id="accordion">    
    <div class="row row-custom"></div>

    <div runat="server" visible="false" id="pn_Escalation1">
        <div class="row row-custom"></div>
        <div class="panel panel-custom"  >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEscalation">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Recent Escalation<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp;<span class="label label-default">
                            <asp:Label ID="lblNotifRecentEscalation" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseEscalation" class="panel-collapse collapse in"  >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_RecentEscalation" runat="server" AllowPaging="True"    OnItemDataBound="grd_RecentEscalation_databound" 
                CssClass="RemoveBorders" BorderStyle="None"
                    AutoGenerateColumns="false" AllowFilteringByColumn="false"  
                    GroupPanelPosition="Top"  RenderMode="Auto"  > <%-- TableLayout="Auto" --%>
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>
                        <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                                                                                                                                                                                                                                                                                                                                                                                                                                                            <Columns>
                    <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                        UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                        Visible="false" Display="false">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                        FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Name"
                            ShowSortIcon="true" SortExpression="Name">
                    </telerik:GridHyperLinkColumn>
                    <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip" >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Escalation date" HeaderText="Escalation date"
                            UniqueName="EscalationcField" FilterControlToolTip="Escalationtip">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Reason" HeaderText="Reason" UniqueName="ReasonField"
                        FilterControlToolTip="Reasontip"  >
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic" 
                            DataField="Topic"  ItemStyle-Width="160px"  >
                        <ItemTemplate>                                
                            <asp:Label ID="lbl1" runat="server"  Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"   > </asp:Label>      
                            <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                            <telerik:RadToolTip ID="RadToolTip1" runat="server" RenderMode="Auto"   TargetControlID="tooltip1"
                                RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner"
                                    RenderInPageRoot="true" Skin="Office2010Silver"  
                                ShowEvent="OnMouseOver" HideEvent="LeaveToolTip"  >
                                <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                            </telerik:RadToolTip>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="StatusField"
                        FilterControlToolTip="Statustip"  >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Escalated by" HeaderText="Escalated by" UniqueName="EscalatedField"
                        FilterControlToolTip="Escalatedbytip"  >
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Team Leader" HeaderText="Team Leader" UniqueName="TeamField"
                        FilterControlToolTip="Teamtip"  >
                    </telerik:GridBoundColumn>      
                        <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype" Display="false"   >
                        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                        </telerik:GridBoundColumn>

                    <%--added for static pager alignment--%>
                    <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                        <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True"    />
                        </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div> 
    </div>
    <div runat="server" visible="false" id="pn_followups">   
        <div class="row row-custom"></div>     
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFollowUps" >
                <div class="panel-heading">
                        <h4 class="panel-title">
                    Overdue Follow Ups<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>&nbsp;<span class="label label-default">
                        <asp:Label ID="lblNotifOverdueFollowups" runat="server" Visible="true"></asp:Label>
                    </span>
                </h4>
                </div>
            </a>
        </div>
        <div id="collapseFollowUps" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadGrid ID="grd_OverdueFollowups" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                    GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="false" 
                OnItemDataBound="grd_OverdueFollowups1_onItemDatabound" CssClass="RemoveBorders" BorderStyle="None" >
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Display="false" Visible="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="NameField"
                                SortExpression = "Name" >
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                FilterControlToolTip="Escalationctip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" DataField="Topic"
                                    SortExpression="Topic" ItemStyle-Width="160px"  >
                                <ItemTemplate> <%-- Text=' <%# DataBinder.Eval(Container.DataItem, "Topic") %>' --%>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic") %>' Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Auto" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner"
                                            RenderInPageRoot="true" Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                    FilterControlToolTip="ReviewFtip">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype" Display="false"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype" Display="false"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                        </telerik:GridBoundColumn>
                        <%--added for static pager alignment--%>
                        <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_signoff">
        <div class="row row-custom"></div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSignOffs">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Overdue Sign Offs<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp;<span class="label label-default">
                            <asp:Label ID="lblNotifOverdueSignOffs" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseSignOffs" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadGrid ID="grd_OverdueSignOffs" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                    GroupPanelPosition="Top" RenderMode="Auto" AutoGenerateColumns="false" OnItemDataBound="grd_OverdueSignOffs_ItemDataBound"
                    CssClass="RemoveBorders" BorderStyle="None">
                        
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Display="false" Visible="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="NameField"
                                SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                FilterControlToolTip="Escalationctip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" DataField="Topic"
                                SortExpression="Topic"   ItemStyle-Width="160px"  >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>' Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Auto" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" RenderInPageRoot="true" Skin="Office2010Silver"
                                            ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                FilterControlToolTip="ReviewFtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>     
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype" Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div> 
</div>