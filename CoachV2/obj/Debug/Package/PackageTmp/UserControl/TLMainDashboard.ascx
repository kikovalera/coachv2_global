﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="TLMainDashboard.ascx.cs" Inherits="CoachV2.UserControl.TLReportsDashboard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="grd_TL_ForTermination">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_TL_ForTermination"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_TL_FinalWarning">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_TL_FinalWarning"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_TL_RecentEscalated">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_TL_RecentEscalated"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_recentlyAuditedbyOM">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_recentlyAuditedbyOM"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_RecentlyReviewedTL">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_RecentlyReviewedTL"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueFollowups">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueFollowups"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_OverdueSignOffs">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_OverdueSignOffs"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
         <telerik:AjaxSetting AjaxControlID="grd_WeeklyReviewTL">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grd_WeeklyReviewTL"  LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment */
.rgDataDiv
{
    overflow-x: hidden !important;
}

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}


</style>

<div class="panel menuheadercustom">
    <div>&nbsp;<span class="glyphicon glyphicon-dashboard"></span> Coaching Dashboard <span class="breadcrumb2ndlevel"><asp:Label
      ID="Label4" runat="server" Text=" "></asp:Label></span>
    </div>
</div>

<div class="panel-group" id="accordion">
    <div class="row row-custom"></div>
               
    <div runat="server" visible="false" id="pn_termination">
        <div class="row row-custom"></div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseForTermination">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        For Termination<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp <span class="label label-default">
                            <asp:Label ID="lblForTermination" runat="server" Visible="false"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseForTermination" class="panel-collapse">
            <div class="panel-body">
                <telerik:RadGrid ID="grd_TL_ForTermination" runat="server" AllowPaging="True" GroupPanelPosition="Top"
                    RenderMode="Auto" AutoGenerateColumns="false" AllowFilteringByColumn="false"
                    OnItemDataBound="grd_TL_ForTermination_onItemDatabound" CssClass="RemoveBorders"
                    BorderStyle="None">
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" TableLayout="Auto" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield"
                                ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"
                                ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Date" HeaderText="Date" UniqueName="DateField"
                                FilterControlToolTip="Datectip" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                                DataField="Topic" ShowSortIcon="true" ItemStyle-Width="160px" >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" 
                                        RenderInPageRoot="true" Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EscalatedBy" HeaderText="Escalated By" UniqueName="EscalatedBy"
                                FilterControlToolTip="Datectip" ShowSortIcon="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_finalwarning">
        <div class="row row-custom"></div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFinalWarning">
                <div class="panel-heading">
                        <h4 class="panel-title">
                        For Final Warning<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblFinalWarning" runat="server"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>     
        </div>
        <div id="collapseFinalWarning" class="panel-collapse collapse in" >
            <div class="panel-body">
                    <telerik:RadGrid ID="grd_TL_FinalWarning"  runat="server"
                        AllowPaging="True" AllowFilteringByColumn="false"   
                        GroupPanelPosition="Top"  RenderMode="Auto"  AutoGenerateColumns="false" OnItemDataBound="grd_TL_FinalWarning_onItemDatabound" CssClass="RemoveBorders" BorderStyle="None" > <%--OnItemDataBound="grd_OverdueFollowups_onItemDatabound"--%>
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>
                        <MasterTableView DataKeyNames="Topic" GridLines="None" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                            <Columns>
                                <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                    UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket" Visible="false" Display="false" >
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridHyperLinkColumn  DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                    FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield" ShowSortIcon="true" SortExpression="Name">
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"  >
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Date" HeaderText="Date" UniqueName="DateField"
                                    FilterControlToolTip="Datectip"  >
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"   SortExpression="Topic"
                                    DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" > 
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                        <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                        <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                            RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner"
                                            RenderInPageRoot="true" Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                            <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                        </telerik:RadToolTip>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                    FilterControlToolTip="ReviewType"   >
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                    FilterControlToolTip="formtype"  Display="false"   >
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                    FilterControlToolTip="reviewtypeid" Display="false"   >
                                </telerik:GridBoundColumn>                            
                                <%--added for static pager alignment--%>
                                <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>                 
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id ="pn_escalated">
        <div class="row row-custom"></div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseRecentEscalated">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Recently Escalated Agents<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblRecentlyEscalated" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseRecentEscalated" class="panel-collapse collapse in" >
            <div class="panel-body">
                <telerik:RadGrid  ID="grd_TL_RecentEscalated"  runat="server" AllowPaging="True"  
                GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="false"
                AllowFilteringByColumn="false" OnItemDataBound="grd_TL_RecentEscalated_databound"   CssClass="RemoveBorders" BorderStyle="None"  >                     
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" CellSpacing="0" TableLayout="Auto" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                    <Columns>
                        <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                            UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket" Visible="false" Display="false" >
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridHyperLinkColumn  DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                            FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield" ShowSortIcon="true" SortExpression="Name">
                        </telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"  >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Session" HeaderText="Session Type" UniqueName="SessionField"  
                            FilterControlToolTip="Sessiontip">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"   SortExpression="Topic"
                            DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px"  >
                            <ItemTemplate>
                                <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                    RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" 
                                    RenderInPageRoot="true" Skin="Office2010Silver" ShowEvent="OnMouseOver" HideEvent="LeaveToolTip"  >
                                    <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                </telerik:RadToolTip>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Escalation" HeaderText="Escalation date" UniqueName="EscalationcField"  
                            FilterControlToolTip="Escalationtip">
                        </telerik:GridBoundColumn> 
                        <telerik:GridBoundColumn DataField="Assigned by" HeaderText="Assigned by" UniqueName="AssignedField"  
                            FilterControlToolTip="Assignedbytip">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype"  Display="false"   >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                        </telerik:GridBoundColumn>                            
                        <%--added for static pager alignment--%>
                        <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                        <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True"  />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_recentlyassigned">
        <div class="row row-custom"></div>
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseRecentlyAssigned">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Recently Assigned Reviews<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp <span class="label label-default">
                            <asp:Label ID="lblRecentlyAssigned" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseRecentlyAssigned" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadGrid ID="grd_RecentlyAssignedReviews" runat="server" AllowPaging="True"
                OnItemDataBound="grd_recentlyAssigned_itemdatabound" GroupPanelPosition="Top"
                RenderMode="Auto" AutoGenerateColumns="false" AllowFilteringByColumn="false"
                CssClass="RemoveBorders" BorderStyle="None">                    
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield" ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                FilterControlToolTip="Sessiontip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                                DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" 
                                        RenderInPageRoot="true" Skin="Office2010Silver" ShowEvent="OnMouseOver" HideEvent="LeaveToolTip"  >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                FilterControlToolTip="Reviewtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_audited">
        <div class="row row-custom"></div>
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseRecentlyAudited">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Recently Audited Reviews by Operations Manager<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblNotifRecentReviewsbyOM" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseRecentlyAudited" class="panel-collapse collapse in" >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_recentlyAuditedbyOM"  OnItemDataBound="grd_recentlyAuditedbyOM_itemdatabound"  runat="server" AllowPaging="True"  
                    GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="false"
                    AllowFilteringByColumn="false" CssClass="RemoveBorders"  BorderStyle="None" >
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield"  
                                ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                    FilterControlToolTip="Sessiontip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"  
                                SortExpression="Topic" DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner"
                                        RenderInPageRoot="true" Skin="Office2010Silver" ShowEvent="OnMouseOver" HideEvent="LeaveToolTip"  >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                    FilterControlToolTip="Reviewtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True"  />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div> 
    </div>
    <div runat="server" visible="false" id="pn_reviewed">
        <div class="row row-custom"></div>
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseRecentlyReviewed">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Recently Reviewed Agents<span class="label label-default"></span> <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp <span class="label label-default">
                            <asp:Label ID="lblRecentlyReviewedTL" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseRecentlyReviewed" class="panel-collapse collapse in"  >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_RecentlyReviewedTL" runat="server" AllowPaging="True"  
                    GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="false"
                    AllowFilteringByColumn="false" OnItemDataBound="grd_RecentlyReviewedTL_databound" CssClass="RemoveBorders" BorderStyle="None" >
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" AllowFiltering="true" DataNavigateUrlFields="Namefield"
                                    ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Escalation date" HeaderText="Escalation date"
                                UniqueName="EscalationcField" FilterControlToolTip="Escalationtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"  
                                SortExpression="Topic" DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" 
                                        RenderInPageRoot="true" Skin="Office2010Silver"  ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="StatusField"
                                    FilterControlToolTip="Statustip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Assigned by" HeaderText="Assigned by" UniqueName="AssignedField"
                                    FilterControlToolTip="Assignedtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_followups">
        <div class="row row-custom"></div>
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFollowUps">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Overdue Follow Ups<span class="label label-default"></span> <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp <span class="label label-default">
                <asp:Label ID="lblNotifOverdueFollowups" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseFollowUps" class="panel-collapse collapse in" >
            <div class="panel-body"> 
                <telerik:RadGrid ID="grd_OverdueFollowups" runat="server" OnItemDataBound="grd_OverdueFollowups_onItemDatabound" AllowPaging="True"  
                GroupPanelPosition="Top" RenderMode="Auto" AutoGenerateColumns="false"
                AllowFilteringByColumn="false" CssClass="RemoveBorders"  BorderStyle="None" >
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield"  
                                ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip" >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                    FilterControlToolTip="Escalationctip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic"  
                                SortExpression="Topic" DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" > 
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner"
                                            RenderInPageRoot="true" Skin="Office2010Silver"   ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                        <br />
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                    FilterControlToolTip="Reviewtip">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                            FilterControlToolTip="ReviewType"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                            FilterControlToolTip="formtype"  Display="false"   >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                            FilterControlToolTip="reviewtypeid" Display="false"   >
                            </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                            <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_signoffs">
        <div class="row row-custom"></div>
        <div class="panel panel-custom" >
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSignOffs">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Overdue Sign Offs<span class="label label-default"></span><span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span>&nbsp;<span class="label label-default">
                            <asp:Label ID="lblNotifOverdueSignOffs" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseSignOffs" class="panel-collapse collapse in"  >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_OverdueSignOffs" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                        GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="false"
                    OnItemDataBound="grd_OverdueSignOffs_onitemdatabound" CssClass="RemoveBorders" BorderStyle="None" >
                    <%--added for static header alignment--%>
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="Topic" AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        <Columns>
                            <telerik:GridHyperLinkColumn DataTextField="CoachingTicket" HeaderText="Coaching Ticket"
                                UniqueName="CoachingTicket" FilterControlToolTip="CoachingTicket" DataNavigateUrlFields="CoachingTicket"
                                Visible="false" Display="false">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridHyperLinkColumn DataTextField="Name" HeaderText="Name" UniqueName="NameField"
                                FilterControlToolTip="Nametip" DataNavigateUrlFields="Namefield" 
                                ShowSortIcon="true" SortExpression="Name">
                            </telerik:GridHyperLinkColumn>
                            <telerik:GridBoundColumn DataField="Cim" HeaderText="Cim" UniqueName="CimField" FilterControlToolTip="Cimtip"
                                >
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Session Type" HeaderText="Session Type" UniqueName="SessionField"
                                    FilterControlToolTip="Escalationctip">
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn DataField="Topic" HeaderText="Topic" UniqueName="TopicField"
                                    SortExpression="Topic" FilterControlToolTip="Topictip">
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridTemplateColumn UniqueName="TopicField" HeaderText="Topic" SortExpression="Topic"
                                DataField="Topic" ShowSortIcon="true"   ItemStyle-Width="160px" >
                                <ItemTemplate>
                                    <asp:Label ID="lbl1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Topic")%>'  Width="100px"  > </asp:Label>
                                    <i class="glyphicon glyphicon-info-sign tooltipdefaultcolor" id="tooltip1" runat="server"></i>
                                    <telerik:RadToolTip RenderMode="Lightweight" ID="RadToolTip1" runat="server" TargetControlID="tooltip1"
                                        RelativeTo="Element" Position="BottomCenter" ShowCallout="false" CssClass="tooltip-inner" 
                                        RenderInPageRoot="true" Skin="Office2010Silver" ShowEvent="OnMouseOver" HideEvent="LeaveToolTip" >
                                        <%# DataBinder.Eval(Container.DataItem, "Tooltip")%>
                                        <br />
                                    </telerik:RadToolTip>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Review Date" HeaderText="Review Date" UniqueName="ReviewField"
                                    FilterControlToolTip="ReviewFtip">
                            </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Review Type" HeaderText="Coaching type" UniqueName="ReviewType"  
                                    FilterControlToolTip="ReviewType"   >
                                </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="formtype" HeaderText="formtype" UniqueName="formtype"  
                                    FilterControlToolTip="formtype"  Display="false"   >
                                </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="reviewtypeid" HeaderText="reviewtypeid" UniqueName="reviewtypeid"  
                                    FilterControlToolTip="reviewtypeid" Display="false"   >
                                </telerik:GridBoundColumn>
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="True"  />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
    <div runat="server" visible="false" id="pn_weekly">
        <div class="row row-custom"></div> 
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseWeeklyReview">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Weekly Review List<span class="label label-default"></span>
                        <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                        </span> &nbsp <span class="label label-default">
                            <asp:Label ID="lblWeekly" runat="server" Visible="true"></asp:Label>
                        </span>
                    </h4>
                </div>
            </a>
        </div>
        <div id="collapseWeeklyReview" class="panel-collapse collapse in" >
            <div class="panel-body">
                <telerik:RadGrid ID="grd_WeeklyReviewTL" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                        GroupPanelPosition="Top"  RenderMode="Auto" AutoGenerateColumns="true"  CssClass="RemoveBorders" BorderStyle="None">
                        <ClientSettings>
                        <Scrolling AllowScroll="True"  />
                    </ClientSettings>
                        <MasterTableView AllowSorting="true"> <%--added AllowSorting 11/06/18--%>
                        </MasterTableView>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</div> 