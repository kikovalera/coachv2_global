﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentSearch.ascx.cs" Inherits="CoachV2.UserControl.AgentSearch" %>
<style type="text/css">

.highlighted {
    background-color: #33ccff;
}

</style>
 <div class="menu-content">
    <div class="panel panel-default">
        <div class="panel-body">
            <span class="glyphicon glyphicon-user"></span>&nbsp;Related Searches for <asp:Label ID="lblrelsearch" runat="server"></asp:Label>
            </div>
    </div>
    <div class="list-group"> 
    <telerik:RadGrid  ID="SearchGrid" runat="server"  onneeddatasource="SearchGrid_NeedDataSource"   > <%--runat="server" Visible="true" OnItemDataBound="SearchGrid_ItemDataBound"   RenderMode="Auto" AllowPaging="true"   OnItemDataBound="SearchGrid_ItemDataBound"  --%>
        <MasterTableView CssClass="RadGrid" GridLines="None" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ReviewID" CommandItemDisplay="None"  >
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false"   />
            <Columns> 
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-8">
                                    
                                        <b><asp:Label ID="Label2" runat="server" Text='<%# Eval("FullNameCoachee")%>' />, 
                                        Ticket #:
                                    <asp:Label ID="TopicName" runat="server"   Text='<%# Eval("ID")%>'  /></b>
                                    <asp:HyperLink ID="link1" runat="server" Text="View Here"  Target="_blank" ></asp:HyperLink>
                                     <br />
                                    <asp:Label ID="Label1" runat="server"  Text='<%# Eval("Description")%>' /><br /> 
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Strengths")%>' /><br />
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Opportunity")%>' /><br />
    
                                </div>
                                <div class="col-xs-4">
                                    R. Date <i class="glyphicon glyphicon-calendar"></i><span>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("CreatedOn", "{0:ddd, MMM d, yyyy}")%>' /></span><br />
                                    F. Date <i class="glyphicon glyphicon-calendar"></i><span>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("FollowDate", "{0:ddd, MMM d, yyyy}")%>' /></span><br />
                                     <i class="glyphicon glyphicon-tags"></i><span>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("SessionName")%>' /></span>
                                    <br />
                                    Review Type:    <asp:Label ID="Label9" runat="server"  Font-Bold="true" /></span>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

            </Columns>
            
        </MasterTableView>
                      <ClientSettings>
                             <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                             <Resizing AllowResizeToFit="True" />
                         </ClientSettings>
    </telerik:RadGrid>

    <telerik:RadButton ID="btn_clear" runat="server" Text="Reset"   ForeColor="White"  CssClass="btn btn-info btn-small"  OnClick="btn_clear_Click"> </telerik:RadButton>

    </div>
</div>