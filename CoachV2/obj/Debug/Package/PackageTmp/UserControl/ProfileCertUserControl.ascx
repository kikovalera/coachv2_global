﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileCertUserControl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileCertUserControl" %>

<style type="text/css">

div.RemoveBorders .rgHeader,
div.RemoveBorders th.rgResizeCol,
div.RemoveBorders .rgFilterRow td
{
	border-width:0 0 1px 0; /*top right bottom left*/
}

/*added for static header alignment (francis.valera/08092018)*/
.rgDataDiv
   {
        overflow-x: hidden !important;
   }

div.RemoveBorders .rgRow td,
div.RemoveBorders .rgAltRow td,
div.RemoveBorders .rgEditRow td,
div.RemoveBorders .rgFooter td
{
	border-width:0;
	padding-left:7px; /*needed for row hovering and selection*/
}

div.RemoveBorders .rgGroupHeader td,
div.RemoveBorders .rgFooter td
{
	padding-left:7px;
}

</style>

<div class="menu-content bg-alt">
    <div class="panel menuheadercustom" id="fake_title" runat="server">
        <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx?tab=cert" ID="RadLinkButton1"
            ForeColor="White" ToolTip="Edit profile" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit trianglebottom"></i>
        </asp:HyperLink>
        <div>
            &nbsp;<span class="glyphicon glyphicon-certificate"></span> MY Certificate
        </div>
    </div>
    <div class="panel-group" id="accordion">
        <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Certifications <span class="glyphicon   glyphicon-triangle-bottom trianglebottom">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    
                    <telerik:RadGrid ID="EducGrid" runat="server" OnPreRender="EducGrid_PreRender" AllowAutomaticInserts="False"
                        OnNeedDataSource="EducGrid_NeedDataSource" CssClass="RemoveBorders" BorderStyle="None" >
                        
                        <%--added for static header alignment--%>
                        <ClientSettings>
                            <Scrolling UseStaticHeaders="true"/>
                        </ClientSettings>

                        <MasterTableView CommandItemDisplay="Top"  GridLines="None" AllowPaging="True"
                            PageSize="20" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="CertID">
                            <CommandItemSettings ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Description" HeaderText="Certificate" DataField="Description"
                                    HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn UniqueName="From" HeaderText="Date Obtained" DataField="DateObtained"
                                    DataType="System.DateTime" DataFormatString="{0:D}" HeaderStyle-Font-Bold="true">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn UniqueName="To" HeaderText="Obtained At" DataField="WhereObtained"
                                    HeaderStyle-Font-Bold="true">
                                </telerik:GridBoundColumn>
                                
                            <%--added for static pager alignment--%>
                            <telerik:GridTemplateColumn UniqueName="lbl2" HeaderText="" SortExpression="lbl2" ItemStyle-Width="10px" >
                                <ItemTemplate><asp:Label ID="lbl2" runat="server" Text='' Width="10px"></asp:Label></ItemTemplate>
                            </telerik:GridTemplateColumn>

                            </Columns>
                        </MasterTableView>
                         <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
</div>
