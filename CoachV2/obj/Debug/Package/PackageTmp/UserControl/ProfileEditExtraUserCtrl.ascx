﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditExtraUserCtrl.ascx.cs" Inherits="CoachV2.UserControl.ProfileEditExtraUserCtrl" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">       
        <a href="MyProfile.aspx?tab=extra" class="pull-right" style="color: #fff;"> <i class="glyphicon glyphicon-arrow-left"></i> </a>
        <%--<a href="javascript:history.go(-1)" class="pull-right" style="color: #fff;"> <i class="glyphicon glyphicon-arrow-left"></i> </a>--%>
        <div>
            &nbsp;<span class="glyphicon glyphicon-add"></span> Additional Info
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Additional Info <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>
                </h4>
            </div>
        </a>
        <div id="collapseFive" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadAdditionalInfo" runat="server" class="form-control" placeholder="Additional Info"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="10"
                    MaxLength="5000">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                    ControlToValidate="RadAdditionalInfo" Display="Dynamic" ErrorMessage="*This is a required field."
                    ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                <div class="row">
                    <br>
                    <div class="col-sm-12">
                        <span class="addreviewbuttonholder">
                            <telerik:RadButton ID="RadSave" CssClass="btn btn-info btn-small" runat="server"
                                Text="Save" ValidationGroup="AddReview" ForeColor="White" OnClick="RadSave_Click">
                            </telerik:RadButton>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
