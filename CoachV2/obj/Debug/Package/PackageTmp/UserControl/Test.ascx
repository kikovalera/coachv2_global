﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Test.ascx.cs" Inherits="CoachV2.UserControl.Test" %>
<link href="../libs/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
<%--<div class="row">
    <div class="col-md-2">
        <label for="Previous Perfomance Results">
            <b></b>
        </label>
        <span></span>
    </div>
    <div class="form-group">
    <div class="col-sm-6">
        <h6>
            <label for="inputPassword" class="control-label">
                <b>From: Coaching Ticket #</b></label><span><telerik:RadTextBox ID="RadFReviewID"
                    runat="server" class="form-control">
                </telerik:RadTextBox></span></h6>
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-6">
        <h6>
            <label for="inputPassword" class="control-label">
                <b>Coaching Date:</b></label><span><telerik:RadTextBox ID="RadFCoachingDate" runat="server" class="form-control">
                </telerik:RadTextBox></span></h6>
    </div>
    </div>
</div>--%>
<div class="panel-zero">
    <div class="form-group">
        <label for="Account" class="control-label col-sm-3">
            From: Coaching Ticket #</label>
        <div class="col-sm-3">
            <telerik:RadTextBox ID="RadFReviewID" runat="server" Width="100%" class="form-control"
                Text="0" ReadOnly="true">
            </telerik:RadTextBox>
        </div>
        <label for="Account" class="control-label col-sm-3">
            Coaching Date:</label>
        <div class="col-sm-3">
            <telerik:RadTextBox ID="RadFCoachingDate" Width="100%" runat="server" class="form-control"
                Text="0" ReadOnly="true">
            </telerik:RadTextBox>
        </div>
    </div>
</div>
<%--<div class="panel-body">--%>
  <%--  <telerik:RadGrid ID="grdPrevCom" runat="server" AutoGenerateColumns="false" CssClass="RemoveBorders"
        BorderStyle="None" Height="700px">
        <MasterTableView>
            <ColumnGroups>
                <telerik:GridColumnGroup Name="Grow" HeaderText="Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below"
                    HeaderStyle-HorizontalAlign="Center" />
            </ColumnGroups>
            <HeaderStyle Width="102px" />
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Goal" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="RadTextBox2" runat="server" Text="What do you want to achieve?"
                            TextMode="MultiLine" Enabled="false" Width="100%" Height="100%" Rows="10">
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="txtGoal" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"
                            Height="100%" Text='<%# Eval("Goal_Text") %>' Rows="15" Placeholder="(Free-form field)
1.
                                                
2.
                                                
3.">
                        </telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Reality" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="RadTextBox3" runat="server" Text="Where are you know? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?"
                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="txtReality" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"
                            Height="100%" Text='<%# Eval("Reality_Text") %>' Rows="15" Placeholder="(Free-form field)
1.
                                                
2.
                                                
3.">
                        </telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Options" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="RadTextBox4" runat="server" Text="What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that?"
                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="txtOptions" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"
                            Height="100%" Text='<%# Eval("Options_Text") %>' Rows="15" Placeholder="(Free-form field)
Option 1
                                                
Option 2
                                                
Option 3">
                        </telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Way Forward" ColumnGroupName="Grow" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="RadTextBox5" runat="server" Text="What option do you think will work the best? What will you do and when? What support and resources do you need?"
                            Width="100%" Height="100%" TextMode="MultiLine" Enabled="false" Rows="10">
                        </telerik:RadTextBox>
                        <telerik:RadTextBox ID="txtWayForward" runat="server" TextMode="MultiLine" Width="100%" Enabled="false"
                            Height="100%" Text='<%# Eval("WayForward_Text") %>' Rows="15" Placeholder="(Free-form field)
1.
                                                
2.
                                                
3.">
                        </telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            <Resizing AllowResizeToFit="True" />
        </ClientSettings>
    </telerik:RadGrid>--%>
<div class="row">
                <div class="col-md-12">
                    <h5 class="text-center">
                        Get the agent/employee's commitment by identifying the problem, setting goals and clearly identifying the end-result using the four phases of GROW Coaching method below
                    </h5>
                </div>
                <div id="no-more-tables">
                    <table class="col-sm-12 table-bordered table-striped table-condensed cf">
                		<thead class="cf">
                			<tr>
                				<th align="center">Goal</th>
                				<th align="center">Reality</th>
                				<th align="center">Options</th>
                				<th align="center">Way Forward</th>
                			</tr>
                		</thead>
                		<tbody>
                			<tr>
                				<td data-title="Goal"><telerik:RadTextBox ID="RadTextBox1" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox2" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                				<td data-title="Reality"><telerik:RadTextBox ID="RadTextBox3" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox4" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                				<td data-title="Options"><telerik:RadTextBox ID="RadTextBox5" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox6" runat="server" Placeholder="(Free-form field)

Option 1
                                                
Option 2
                                                
Option 3" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                				<td data-title="Way Forward"><telerik:RadTextBox ID="RadTextBox7" runat="server" TextMode="MultiLine" Rows="10" Enabled="false"></telerik:RadTextBox>
                                <telerik:RadTextBox ID="RadTextBox8" Placeholder="(Free-form field)

1.
                                                
2.
                                                
3." runat="server" TextMode="MultiLine" Rows="15" ReadOnly="true"></telerik:RadTextBox></td>
                			</tr>
            		</tbody>
            	</table>
            </div>
        </div>
<%--</div>--%>
