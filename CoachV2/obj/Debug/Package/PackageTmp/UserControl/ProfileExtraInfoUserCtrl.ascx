﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileExtraInfoUserCtrl.ascx.cs"
    Inherits="CoachV2.UserControl.ProfileExtraInfoUserCtrl" %>
<div class="menu-content bg-alt">
    <div class="panel menuheadercustom" id="fake_title" runat="server">
        <asp:HyperLink runat="server" NavigateUrl="~/EditProfile.aspx?tab=extra" ID="RadLinkButton1"
            ForeColor="White" ToolTip="Edit profile" CssClass="pull-right">
                <i class="glyphicon glyphicon-edit trianglebottom"></i>
        </asp:HyperLink>
        <div>
            &nbsp;<span class="glyphicon glyphicon-plus"></span> Additional Info
        </div>
    </div>
    <div class="panel panel-custom">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Additional Info <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                    </span>
                </h4>
            </div>
        </a>
        <div id="collapseFive" class="panel-collapse collapse in">
            <div class="panel-body">
                <telerik:RadTextBox ID="RadAdditionalInfo" runat="server" class="form-control" placeholder="Additional Info"
                    TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="10" TabIndex="10"
                    MaxLength="5000" Enabled="false">
                </telerik:RadTextBox>
                <%--                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="AddReview"
                                                        ControlToValidate="RadOpportunities" Display="Dynamic" ErrorMessage="*This is a required field."
                                                        ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
    </div>
</div>
