﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ops_ChartTL.ascx.cs"
    Inherits="CoachV2.UserControl.Ops_ChartTL" %>
<div class="panel-group" id="accordion">
    <div class="row row-custom">
        <div class="col-sm-5">
            <form>
            <div class="input-group">
            </div>
            </form>
        </div>
    </div>
    <div class="panel panel-custom">
        <a id="A1" data-toggle="collapse" data-parent="#accordion" href="#collapseFour1">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Charts <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                </h4>
            </div>
        </a>
        <div id="collapseFour1" class="panel-collapse collapse in">
        <br />
        <div class="panel-body">
            <div class="form-horizontal col-md-12">
                <button type="button" class="btn btn-custom" id="check_graph" style="float: left;
                    clear: both; margin-right: 10px;">
                    Overall</button>
                <asp:DataList ID="dlKPI" runat="server" RepeatColumns="10" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <button class="btn btn-custom click-me-kpi" kpi-id='<%#Eval("hierarchy").ToString() %>'>
                            <%# Eval("kpi").ToString()%></button>
                        &nbsp;
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server" Visible="false"
                            CssClass="blacktext"> 
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
            </div>
            <div id="overall-holder">
                <div id="overalldate" style="width: 100%">
                    <asp:HiddenField ID="FakeURLID" runat="server" />
                    <asp:HiddenField ID="MyCIM" runat="server" />
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Overall Coaching Frequency</font>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_overallagents" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="StartDate" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="overalls" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="EndDate" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                            DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="overalls" />
                        </telerik:RadDatePicker>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_overalldataview" runat="server" AppendDataBoundItems="true"
                            DataValueField="DataViewID" DataTextField="DataView">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnOverall" class="btn btn-info">
                            Run</button>
                    </div>
                </div>
                <div id="overallfrequency" style="width: 100%; height: 100%;">
                </div>
                <%--top 4 frequency versus score--%>
                <div id="overall_behaviordates">
                    <br />
                    <br />
                    <br />
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Top Behavior Drivers</font>
                                    <%-- <font size="4">Overall Behavior</font>--%>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_agentlist2" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            KPI<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_kpi_behavior1" runat="server" AppendDataBoundItems="true"
                            DataValueField="kpiid" DataTextField="kpiname" ResolvedRenderMode="Classic">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behavior_start" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="behaviors" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behavior_end" runat="server" Empty="Enter end date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="behaviors" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnBehavior" class="btn btn-info">
                            Run</button>
                    </div>
                </div>
                <div id="behaviorvsKPI" style="width: 100%; height: 100%;">
                </div>
                <div id="overall_kpivsb">
                    <br />
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4">Behavior vs KPI</font>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_agentlist3" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            KPI <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </div>
                        <telerik:RadComboBox ID="cb_kpi_behavior_kpi" runat="server" DataValueField="KPIID"
                            DataTextField="KPIName" AppendDataBoundItems="true">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behaviorkpi_start" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="kpivsb" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_behaviorkpi_end" runat="server" Empty="Enter end date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="kpivsb" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnKPIvBehavior" class="btn btn-info">
                            Run</button>
                    </div>
                </div>
                <div id="DIV2_kpivsscore" style="width: 100%; height: 100%;">
                </div>
            </div>
            <div id="chart-holder">
                <div id="Div1">
                    <br />
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <%--<asp:Label ID="Label1" runat="server" Font-Size="Large">Score vs KPI</asp:Label>--%>
                                    <font size="4" id="freqvsscore">Average AHT Score</font>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_agentlist4" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_aht_start1" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="cbChangeChartFreqVScore" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_aht_end1" runat="server" Empty="Enter end date" DateInput-Enabled="false"
                            DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd" DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="cbChangeChartFreqVScore" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_top1scoredataview" runat="server" AppendDataBoundItems="true"
                            DataValueField="DataViewID" DataTextField="DataView">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnScoreVKPI" class="btn btn-info">
                            Run</button>
                    </div>
                </div>
                <div id="top1" runat="server" visible="true">
                    <div id="Overall_AHT_Agent" style="width: 100%; height: 100%;">
                    </div>
                </div>
                <div id="Div2_FCRGlidedates">
                    <br />
                    <br />
                    <br />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    <font size="4" id="tarvsactual">
                                        <%--<asp:Label ID="lbl_3rdkpi_actualvstarget" runat="server" Font-Size="Large"> </asp:Label>--%>
                                        Targets Vs. Actual</font>
                                    <%--                    <font size="4">FCR Glide Path Targets Vs. Actual</font>--%>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="col-sm-2">
                        <div class="help">
                            Agent<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_agentlist5" runat="server" DataValueField="CimNumber"
                            DataTextField="Name" AppendDataBoundItems="true">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="All" Value="0" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            From <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_ahtglide_start" runat="server" placeholder="Enter start date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="cbChangeChartTarVScore" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            To <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <telerik:RadDatePicker ID="dp_ahtglide_end" runat="server" Empty="Enter end date"
                            DateInput-Enabled="false" DateInput-DateInput-DisplayDateFormat="yyyy-MM-dd"
                            DateInput-DateFormat="yyyy-MM-dd">
                            <ClientEvents OnDateSelected="cbChangeChartTarVScore" />
                        </telerik:RadDatePicker>
                        &nbsp;
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                            Data View<span class="glyphicon glyphicon-triangle-bottom"></span></div>
                        <telerik:RadComboBox ID="cb_top1avtdataview" runat="server" AppendDataBoundItems="true"
                            DataValueField="DataViewID" DataTextField="DataView" OnClientSelectedIndexChanged="cbChangeChartTarVScore">
                        </telerik:RadComboBox>
                    </div>
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-2">
                        <div class="help">
                           &nbsp;
                        </div>
                        <button id="BtnTarVAct" class="btn btn-info">
                            Run</button>
                    </div>
                    <div id="DIV2_FCRGlide" class="col-xs-12 demo-placeholder" style="width: 100%; height: 400px;">
                    </div>
                </div>
    </div>
</div>
</div>
    </div>
    
</div>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/highcharts.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/data.js"></script>
<script type="text/javascript" src="http://q9vmdevapp06.nucomm.net/Coach2/libs/highcharts/js/modules/exporting.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
<script type='text/javascript'>

    var kpiid;
    var kpilabel;

    var URL = $("#<%= FakeURLID.ClientID %>").val();

    $("#err-msg").hide();

    function overalls(sender, args) {

        var startDate = $find('<%=StartDate.ClientID%>');
        var endDate = $find('<%=EndDate.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnOverall').prop('disabled', true);
        } else {
            $('#BtnOverall').prop('disabled', false);
        }
    }

    function behaviors(sender, args) {


        var startDate = $find('<%=dp_behavior_start.ClientID%>');
        var endDate = $find('<%=dp_behavior_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnBehavior').prop('disabled', true);
        } else {
            $('#BtnBehavior').prop('disabled', false);
        }
    }

    function kpivsb(sender, args) {
        //behaviorvskpi();

        var startDate = $find('<%=dp_behaviorkpi_start.ClientID%>');
        var endDate = $find('<%=dp_behaviorkpi_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnKPIvBehavior').prop('disabled', true);
        } else {
            $('#BtnKPIvBehavior').prop('disabled', false);
        }
    }

    function cbChangeChartFreqVScore(sender, args) {
        //chart1loader(kpiid, kpilabel);

        var startDate = $find('<%=dp_aht_start1.ClientID%>');
        var endDate = $find('<%=dp_aht_end1.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnScoreVKPI').prop('disabled', true);
        } else {
            $('#BtnScoreVKPI').prop('disabled', false);
        }

    }

    function cbChangeChartTarVScore(sender, args) {


        var startDate = $find('<%=dp_ahtglide_start.ClientID%>');
        var endDate = $find('<%=dp_ahtglide_end.ClientID%>');

        if (startDate.get_selectedDate() > endDate.get_selectedDate()) {
            $('#BtnTarVAct').prop('disabled', true);
        } else {
            $('#BtnTarVAct').prop('disabled', false);
        }
                
    }

    function overall() {
        var data = [];

        var selectedDate1 = $find("<%= StartDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDate2 = $find("<%= EndDate.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selec

        var subcimno = $find('<%=cb_overallagents.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('overallfrequency', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: null,
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: {
                tickInterval: 5,
                min: 0,
                title: {
                    text: ''
                },
                color: '#FF00FF'
            },
            tooltip: {
                pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    },
                    fillOpacity: 1,
                    fillColor: '#289CCC'
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },
            series: [{
                name: 'Coaching Frequency',
                data: data,
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        }); //OVERALL FREQUENCY DIV1

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDate1 + "', EndDate: '" + selectedDate2 + "', CIMNo: '" + CIMNo + "', subcimno: '" + subcimno + "', DataView: '" + DataView + "'}", //+ "', subcimno: '" + subcimno + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overall_ForTL", // URL + "/FakeApi.asmx/getData",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    c = f.TargetCount;
                    data.push([f.ADate, f.CoachCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data);
                highC.hideLoading();

            } //SUCCESSFUNCTION OA FRE
        });  //AJAX OVERALL FREQUENCY

        highC.showLoading();
    }

    function overallbehavior() {

        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selec
        var subcimno = $find('<%=cb_overallagents.ClientID %>').get_selectedItem().get_value()
        var DataView = $find('<%=cb_overalldataview.ClientID %>').get_selectedItem().get_value()

        var data3 = [];
        var RadDatePickerb1 = $find("<%= dp_behavior_start.ClientID %>");
        var selectedDateb1 = RadDatePickerb1.get_selectedDate().format("yyyy-MM-dd");

        var RadDatePickerb2 = $find("<%= dp_behavior_end.ClientID %>");
        var selectedDateb2 = RadDatePickerb2.get_selectedDate().format("yyyy-MM-dd");
        var kpiid = $find('<%=cb_kpi_behavior1.ClientID %>');
        var kpiid1 = kpiid.get_selectedItem().get_value()
        var subcim = $find('<%=cb_agentlist2.ClientID %>');
        var subcimno = subcim.get_selectedItem().get_value()


        $('.highcharts-data-table').remove();

        var highC = Highcharts.chart('behaviorvsKPI', {
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: {
                allowDecimals: false,
                tickInterval: 5,
                min: 0,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                }
            },
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {
                series: {
                    colorByPoint: true
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },
            series: [{
                name: 'Overall Behavior',
                data: data3,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            }]
        }); //OVERALL Behavior

        $.ajax({
            type: "POST",
            //data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "'}",
            data: "{ StartDate: '" + selectedDateb1 + "', EndDate: '" + selectedDateb2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiid1 + "', subcimno: '" + subcimno + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehavior_ForTL",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {
                    data3.push([f.description, f.CoachCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data3);
                highC.hideLoading()

            } //SUCCESS BEHAVIOR
        });  //BEHAVIOR AJAX

        highC.showLoading();
    }

    function behaviorvskpi() {

        var data_bvsk1 = [];
        var data_bvsk2 = [];
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selec
        var selectedDatebkpi11 = $find("<%= dp_behaviorkpi_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatebkpi2 = $find("<%= dp_behaviorkpi_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var kpiidbkpi1 = $find('<%=cb_kpi_behavior_kpi.ClientID %>').get_selectedItem().get_value();
        var subcimno = $find('<%=cb_agentlist3.ClientID %>').get_selectedItem().get_value();
        var kpiidbkpi1 = $find('<%=cb_kpi_behavior_kpi.ClientID %>').get_selectedItem().get_value();

        var highC = Highcharts.chart('DIV2_kpivsscore', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                allowDecimals: false,
                tickInterval: 5,
                min: 0,
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
            {
                title: {
                    text: '',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF',
                opposite: true,
                min: 0,
                allowDecimals: false,
                tickInterval: 20,
                max: 100,
                labels: {
                    format: '{value}%'
                }
            }],
            tooltip: {
                // pointFormat: 'Coaching ticket(s): <b>{point.y:,.0f}</b>'
            },
            plotOptions: {

            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'Behavior',
                data: data_bvsk1,
                type: 'column',
                yaxis: 0,
                color: '#27A6D9',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
            {
                name: 'KPI',
                type: 'spline',
                yAxis: 1,
                data: data_bvsk2,
                color: '#BC6563',
                lineWidth: 3,
                marker: {
                    enabled: true,
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                },
                tooltip: {
                    pointFormatter: function () {
                        return ' <b>' + this.y.toFixed(2) + ' %</b>';
                    }
                }
            }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatebkpi11 + "', EndDate: '" + selectedDatebkpi2 + "',  CIMNo: '" + CIMNo + "', kpiid: '" + kpiidbkpi1 + "', subcimno: '" + subcimno + "' }",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getData_overallbehaviorvsKPI_ForTL",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {

                    data_bvsk1.push([f.descriptions, f.coachedkpis]);
                    data_bvsk2.push([f.descriptions, f.AvgCurr]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_bvsk1);
                highC.series[1].setData(data_bvsk2);
                highC.hideLoading();
            }
        });

        highC.showLoading();

    }


    $("#check_graph").on("click", function () {

        $("#overall-holder").show();
        $('.click-me-kpi').css({ "backgroundColor": "#383838", "color": "#F3F3F3" });

        $("#chart-holder").hide();

        $('#overallfrequency').show();
        $('#overalldate').show();
        $('#behaviorvsKPI').show();
        $('#overall_behaviordates').show();
        $('#overall_kpivsb').show();
        $('#DIV2_kpivsscore').show();

        overall();
        overallbehavior()
        behaviorvskpi();

        $('.highcharts-data-table').remove();


    });      //READY FUNCTION


    function getRandomColor(v) {

        var color = ['#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4', '#4572A7', '#AA4643', '#AA7FFF', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#FF00D4'];
        return color[v];
    }

    function chart2loader(v, label) {

        var colorx = getRandomColor(v);
        $("#tarvsactual").html("Target" + label.toUpperCase() + " Vs. Actual" + label.toUpperCase());

        var data_aht3 = [];
        var data_aht4 = [];

        var CIMNo = $("#<%= MyCIM.ClientID %>").val();
        var selectedDateahtglide1 = $find("<%= dp_ahtglide_start.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDateahtglide2 = $find("<%= dp_ahtglide_end.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var subcimno = $find('<%=cb_agentlist5.ClientID %>').get_selectedItem().get_value();
        var DataView = $find('<%=cb_top1avtdataview.ClientID %>').get_selectedItem().get_value();

        var kpi = v;

        var highC = Highcharts.chart('DIV2_FCRGlide', {
            chart: {
                type: 'xy',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                //allowDecimals: false,
                title: {
                    text: 'Target ' + label,
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                {
                    title: {
                        text: 'Actual ' + label,
                        style: {
                            // margin: '50px', // does not work for some reasons, see workaround below
                            color: '#707070',
                            fontWeight: 'bold',
                            textTransform: 'none'
                        }
                    },
                    color: '#FF00FF',
                    opposite: true,
                    min: 0,
                    labels: {
                        formatter: function () {
                            return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                        }
                    }
                }], tooltip: {
                },
            plotOptions: {

            },
            credits: {
                enabled: false
            },

            series: [{
                name: 'Target ' + label,
                data: data_aht3,
                type: 'spline',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                },
                tooltip: {
                    pointFormatter: function () {
                        if (data_aht3[this.category].toString().indexOf('*') >= 0) {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + ' </b> fiscal month target change';
                            }
                        } else {
                            if (this.y == 0) {
                                return "No target value for this date";
                            } else {
                                return label + ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                            }
                        }
                    }
                }
            },
                        {
                            name: "Actual" + label,
                            type: 'spline',
                            yAxis: 0, //what changed
                            data: data_aht4,
                            color: colorx,
                            lineWidth: 3,
                            marker: {
                                enabled: true,
                                fillColor: '#fff',
                                lineWidth: 2,
                                lineColor: '#ccc'
                            },
                            tooltip: {
                                pointFormatter: function () {
                                    if (this.y == 0 && parseInt(data_aht3[this.index][1]) == 0) {
                                        return label + ' <b>No data available</b>';
                                    } else {
                                        return label + ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                                    }
                                }
                            }
                        }]
        });

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDateahtglide1 + "', EndDate: '" + selectedDateahtglide2 + "', CIMNo: '" + CIMNo + "', subcimno: '" + subcimno + "', KPI: '" + kpi + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/gettopkpiscores_TL",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                    //fplot($("#placeholder"), [[]], opts);
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_aht3.push([f.ADate, f.TargetCount]);
                    data_aht4.push([f.ADate, f.CurrentCount]);

                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_aht3);
                highC.series[1].setData(data_aht4);
                highC.hideLoading();
                $("#err-msg").hide();

            }
        });

        highC.showLoading();
    }

    function chart1loader(v, label) {

        var data_voc3 = [];
        var data_voc4 = [];

        var colorx = getRandomColor(v);

        var selectedDatevoc1 = $find("<%= dp_aht_start1.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var selectedDatevoc2 = $find("<%= dp_aht_end1.ClientID %>").get_selectedDate().format("yyyy-MM-dd");
        var CIMNo = $("#<%= MyCIM.ClientID %>").val(); //getting selected date
        var subcimno = $find('<%=cb_agentlist4.ClientID %>').get_selectedItem().get_value()
        var DataView = $find('<%=cb_top1scoredataview.ClientID %>').get_selectedItem().get_value()
        var KPI = v;

        $("#freqvsscore").html("Coaching Frequency VS. " + label.toUpperCase() + " Score");

        var highC = Highcharts.chart('Overall_AHT_Agent', {
            chart: {
                type: 'xy', // 'area',
                spacingBottom: 30
            },
            title: {
                text: '',
                align: 'left',
                style: {
                    // margin: '50px', // does not work for some reasons, see workaround below
                    color: '#707070',
                    fontSize: '20px',
                    fontWeight: 'bold',
                    textTransform: 'none'
                }
            },
            xAxis: {
                type: "category",
                color: '#FF00FF'
            },
            yAxis: [{
                tickInterval: 5,
                min: 0,
                title: {
                    text: 'Total Coaching',
                    style: {
                        // margin: '50px', // does not work for some reasons, see workaround below
                        color: '#707070',
                        fontWeight: 'bold',
                        textTransform: 'none'
                    }
                },
                color: '#FF00FF'

            },
                    { allowDecimals: false,
                        tickInterval: 20, //set ticks to 20
                        max: (label.toString().indexOf('AHT') >= 0 ? 1000 : 100),
                        title: {
                            text: label + ' Score',
                            style: {
                                // margin: '50px', // does not work for some reasons, see workaround below
                                color: '#707070',
                                fontWeight: 'bold',
                                textTransform: 'none'
                            }
                        },
                        opposite: true,
                        min: 0,
                        labels: {
                            format: (label.toString().indexOf('AHT') >= 0 ? '{value}' : '{value}%')
                        }/*,
                        labels: {
                            formatter: function () {
                                return 100 * this.value / $(this.axis.tickPositions).last()[0] + '%';
                            }
                        }*/
                    }],
            tooltip: {
                //pointFormat: txtValue2 + ' : <b>{point.y:,.0f}</b>'
                shared: false
            },
            plotOptions: {

            },
            credits: {
                enabled: false
            },
            exporting: {
                showTable: true
            },

            series: [{
                name: 'TOTAL ' + label + ' Coaching',
                data: data_voc3,
                type: 'column',
                yaxis: 0,
                color: '#2F9473',
                marker: {
                    fillColor: '#fff',
                    lineWidth: 2,
                    lineColor: '#ccc'
                }
            },
                    {
                        name: label,
                        type: 'spline',
                        yAxis: 1,
                        data: data_voc4,
                        color: colorx,
                        lineWidth: 3,
                        marker: {
                            enabled: true,
                            fillColor: '#fff',
                            lineWidth: 2,
                            lineColor: '#ccc'
                        },
                        tooltip: {
                            pointFormatter: function () {
                                return ' <b>' + this.y.toFixed(2) + (label.toString().indexOf('AHT') >= 0 ? '' : '%') + '</b>';
                            }
                        }
                    }]
        }); //voc

        $.ajax({
            type: "POST",
            data: "{ StartDate: '" + selectedDatevoc1 + "', EndDate: '" + selectedDatevoc2 + "',  CIMNo: '" + CIMNo + "', subcimno: '" + subcimno + "', KPI: '" + KPI + "', DataView: '" + DataView + "'}",
            contentType: "application/json; charset=utf-8",
            url: URL + "/FakeApi.asmx/getDataTL_ScoreVSFrequency",
            // url: URL + "/FakeApi.asmx/getDataTL_VOCScore", // URL + "/FakeApi.asmx/getData",
            dataType: 'json',
            success: function (msg) {

                if (msg != null && msg.d == null) {
                    $("#err-msg").show();
                }
                else {
                    $("#err-msg").hide();
                }

                $.each(msg, function (e, f) {


                    data_voc3.push([f.ADate, f.CoachCount]);
                    data_voc4.push([f.ADate, f.CurrentCount]);
                    $("#err-msg").hide();

                });

                highC.series[0].setData(data_voc3);
                highC.series[1].setData(data_voc4);
                highC.hideLoading();
            }
        });

        highC.showLoading();

    }


    $(document).ready(function () {
        $("#check_graph").trigger('click');

        $(".click-me-kpi").on("click", function () {
            var v = $(this).attr("kpi-id");
            var label = $(this).text();

            kpiid = v;
            kpilabel = label;
            resetColor(v);

            $("#overall-holder").hide();
            $("#chart-holder").show();

            chart1loader(v, label);
            chart2loader(v, label);

            return false;
        });

        $("#BtnOverall").on("click", function () {
            overall();
            return false;
        });

        $("#BtnBehavior").on("click", function () {
            overallbehavior();
            return false;
        });

        $("#BtnKPIvBehavior").on("click", function () {
            behaviorvskpi();
            return false;
        });


        $("#BtnScoreVKPI").on("click", function () {
            chart1loader(kpiid, kpilabel);
            return false;
        });

        $("#BtnTarVAct").on("click", function () {
            chart2loader(kpiid, kpilabel);
            return false;
        });
        
    });

    function resetColor(v) {
        $.each($('.click-me-kpi'), function (key, xx) {
            var kpiid = $(xx).attr('kpi-id');

            if (kpiid == v) {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F5D27C" });

            } else {
                $(xx).css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
            }

            $("#check_graph").css({ "backgroundColor": "#383838", "color": "#F3F3F3" });
        });
    }
</script>
