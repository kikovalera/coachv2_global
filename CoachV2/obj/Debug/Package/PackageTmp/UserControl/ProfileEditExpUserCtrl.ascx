﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEditExpUserCtrl.ascx.cs" Inherits="CoachV2.UserControl.ProfileEditExpUserCtrl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<div class="menu-content bg-alt">
    <div class="panel menuheadercustom">
      <%--  <a href="javascript:history.go(-1)" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a>--%>
         <a href="MyProfile.aspx?tab=experience" class="pull-right" style="color: #fff;">  <i class="glyphicon glyphicon-arrow-left"></i> </a> <div>
            &nbsp;<span class="glyphicon glyphicon-saved"></span> My Experience
        </div>
    </div>
    <telerik:radgrid id="ExpGrid" runat="server" autogeneratecolumns="false" allowsorting="true"  RenderMode="Auto"
         
        onneeddatasource="ExpGrid_NeedDataSource" 
        oninsertcommand="ExpGrid_InsertCommand" 
        ondeletecommand="ExpGrid_DeleteCommand" 
        onitemdatabound="ExpGrid_ItemDataBound" 
        onupdatecommand="ExpGrid_UpdateCommand"  OnItemCommand="ExpGrid_ItemCommand"  >
        <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" DataKeyNames="ExpID"
            EditMode="EditForms">
            <EditFormSettings>
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add Experience"
                ShowExportToCsvButton="true" />
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Edit" ID="EditLink" runat="server"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Company" SortExpression="Company" DataField="School"
                    ColumnEditorID="CompanyName" HeaderText="Company" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Company")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtCompanyName" runat="server" EmptyMessage="Please enter company name" Text='<%# DataBinder.Eval(Container.DataItem, "Company")%>' />
                        <asp:RequiredFieldValidator ID="reqCompanyName" runat="server" Text="Enter company name"
                            ControlToValidate="TxtCompanyName"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                

                

                <telerik:GridTemplateColumn UniqueName="Role" SortExpression="Role" DataField="Role"
                    ColumnEditorID="RoleName" HeaderText="Role" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Role") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtRole" runat="server" EmptyMessage="Please enter role" Text='<%# DataBinder.Eval(Container.DataItem, "Role")%>' />
                        <asp:RequiredFieldValidator ID="reqRole" runat="server" Text="Enter role" ControlToValidate="TxtRole"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>


                <telerik:GridTemplateColumn UniqueName="Industry" SortExpression="Industry" DataField="Industry"
                    ColumnEditorID="IndustryName" HeaderText="Industry" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Industry")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtIndustry" runat="server" EmptyMessage="Please enter industry" Text='<%# DataBinder.Eval(Container.DataItem, "Industry")%>' />
                        <asp:RequiredFieldValidator ID="reqIndustry" runat="server" Text="Enter industry" ControlToValidate="TxtIndustry"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>


                <telerik:GridTemplateColumn UniqueName="StartDate" SortExpression="From" DataField="StartDate"
                    ColumnEditorID="txtFrom2" HeaderText="From" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "StartDate", "{0:y}")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label runat="server" ID="WorkStartDate" Text='<%# DataBinder.Eval(Container.DataItem, "StartDate") %>'
                            Visible="false" />
                        <telerik:RadMonthYearPicker ID="TxtStart" runat="server" EmptyMessage="Please enter year start" />
                        <asp:RequiredFieldValidator ID="reqFrom" runat="server" Text="Enter year start" ControlToValidate="TxtStart"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="EndDate" SortExpression="To" DataField="EndDate"
                    ColumnEditorID="txtTo21" HeaderText="To" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "EndDate", "{0:y}")%>
                    </ItemTemplate>
                        <EditItemTemplate>
                        <asp:Label runat="server" ID="WorkEndDate" Text='<%# DataBinder.Eval(Container.DataItem, "EndDate") %>'
                            Visible="false" />
                        <telerik:RadMonthYearPicker ID="TxtEnd" runat="server" EmptyMessage="Please enter year end" />
                        <asp:RequiredFieldValidator ID="reqTo" runat="server" Text="Enter year end" ControlToValidate="TxtEnd"></asp:RequiredFieldValidator>
                    
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                
                
                <telerik:GridTemplateColumn UniqueName="ReasonForLeaving" SortExpression="ReasonForLeaving" DataField="ReasonForLeaving"
                    ColumnEditorID="ReasonForLeavingName" HeaderText="Reason For Leaving" HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "ReasonForLeaving")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TxtRFL" runat="server" EmptyMessage="Please enter reason for leaving" Text='<%# DataBinder.Eval(Container.DataItem, "ReasonForLeaving")%>' />
                        <asp:RequiredFieldValidator ID="reqRFL" runat="server" Text="Enter reason for leaving" ControlToValidate="TxtRFL"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                    Exportable="false">
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Delete" ID="DeleteLink" runat="server" OnClientClick="return confirm('Are you sure to delete this record?');"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings >
        <Scrolling AllowScroll="true" />
        </ClientSettings>
    </telerik:radgrid>
    <br />
    <br />
</div>
