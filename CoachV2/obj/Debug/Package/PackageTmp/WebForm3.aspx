﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="CoachV2.WebForm3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="libs/js/testjs.js" type="text/javascript"></script>
</head>
<telerik:RadScriptBlock runat="Server" ID="RadScriptBlock2">
    <script src="libs/js/testjs.js" type="text/javascript"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
            Sys.Application.add_load(function () {
                demo.contextMenu = $find("<%= RadContextMenu1.ClientID %>");
                demo.editOffsetX = $find('<%= editOffsetX.ClientID %>');
                demo.editOffsetY = $find('<%= editOffsetY.ClientID %>');
 
                demo.load();
            });
            /* ]]> */
        </script>
    </telerik:RadScriptBlock>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div>
        <telerik:RadGrid ID="RadGrid1" runat="server" Visible="true" RenderMode="LightWeight"
            Height="100%" Width="100%">
            <ClientSettings>
                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
            </ClientSettings>
        </telerik:RadGrid>
        <div>
            <telerik:RadButton ID="RadBack" runat="server" Text="Back" CssClass="btn btn-info btn-small"
                ForeColor="White">
            </telerik:RadButton>
            <telerik:RadButton ID="RadSave" runat="server" Text="Save" CssClass="btn btn-info btn-small"
                ForeColor="White">
            </telerik:RadButton>
            <telerik:RadButton RenderMode="Lightweight" EnableSplitButton="true" ID="SplitButton"
                AutoPostBack="false" runat="server" Text="Transfer Item" OnClientClicked="OnClientClicked"
                OnClientLoad="storeSplitButtonReference">
            </telerik:RadButton>
            <telerik:RadContextMenu ID="RadContextMenu1" runat="server" OnClientItemClicked="OnClientItemClicked" OnClientShown="OnClientShown"
                OnClientLoad="storeContextMenuReference">
                <Items>
                    <telerik:RadMenuItem Text="Transfer Right">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem Text="Transfer Left">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadContextMenu>
            <li style="display:none" class="SpecificCoordinates">
                        <telerik:RadNumericTextBox ID="editOffsetX" runat="server" CssClass="qsfexInput" Value="0" NumberFormat-DecimalDigits="0" Type="Number" Label="X - axis:" Size="Narrow"></telerik:RadNumericTextBox>
                        <telerik:RadNumericTextBox ID="editOffsetY" runat="server" CssClass="qsfexInput" Value="0" NumberFormat-DecimalDigits="0" Type="Number" Label="Y - axis:" Size="Narrow"></telerik:RadNumericTextBox>  
                                           
                    </li>
            
        </div>
    </div>
    </form>
</body>
</html>
