﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddMassCoaching.aspx.cs" Inherits="CoachV2.AddMassCoaching" %>
<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
 <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server"> 
 <script type="text/javascript" src="libs/js/JScript1.js"></script>
 <script type="text/javascript">
     function onRequestStart(sender, args) {

         if (args.get_eventTarget().indexOf("RadAdd") >= 0) {
             args.set_enableAjax(false);

         }
     }
 </script>
<script type="text/javascript">
    function OnClientClicking(sender, args) {
        var callBackFunction = Function.createDelegate(sender, function (argument) {
            if (argument) {
                this.click();
            }
        });
        
        var str = 'Are you sure you want to delete';
        str += ' ' + '?'
        radconfirm(str, callBackFunction, 300, 150, null, "Please confirm");
        args.set_cancel(true);
    }
</script>  
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
 <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
  <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="RadAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelSelected" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="RadRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelSelected" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadCimNumber" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMassType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PanelGroups" />
                    <telerik:AjaxUpdatedControl ControlID="PanelSelected" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
 <div class="menu-content bg-alt">
        <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
     <div class="panel-group" id="accordion">
         <div class="panel panel-custom">
             <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                 <div class="panel-heading">
                     <h4 class="panel-title">
                         Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                         </span>
                     </h4>
                 </div>
             </a>
             <div id="collapseOne" class="panel-collapse collapse in">
           <%--       <div class="panel-body">
                        <div class="form-group">
                            <label for="Account" class="control-label col-sm-2">
                                Coaching Ticket</label>
                            <div class="col-sm-3">
                                <telerik:RadTextBox ID="RadReviewID"  Width="100%" runat="server" class="form-control"
                                    Text="0" ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                            <label for="Account" class="control-label col-sm-3">
                                Coaching Date</label>
                            <div class="col-sm-2">
                                <telerik:RadTextBox ID="RadCoachingDate"  Width="100%" runat="server" class="form-control" Text="0"
                                    ReadOnly="true">
                                </telerik:RadTextBox>
                            </div>
                            <div class="col-sm-2 text-right">
                                <asp:HiddenField ID="FakeURLID" runat="server" />
                                <asp:HiddenField ID="hfGridHtml" runat="server" />
                                <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                    Visible="true" OnClick="btn_ExporttoPDF_Click">
                                    <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                </telerik:RadButton>
                            </div>
                        </div>
                    </div>--%>
                  <div class="panel-body" style="padding-bottom: 5px">
                     <div class="form-group">
                         <div class="col-sm-4 col-md-2 col-md-push-10">
                             <div class="col-sm-2">
                                 <asp:HiddenField ID="FakeURLID" runat="server" />
                                 <asp:HiddenField ID="hfGridHtml" runat="server" />
                                 <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                     Visible="true" OnClick="btn_ExporttoPDF_Click">
                                     <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                 </telerik:RadButton>
                             </div>
                         </div>
                         <div class="col-sm-8 col-md-10 col-md-pull-2">
                             <label for="Account" class="control-label col-sm-3">
                                 Coaching Ticket</label>
                             <div class="col-sm-3">
                                 <telerik:RadTextBox ID="RadReviewID" Width="100%" runat="server" class="form-control"
                                     Text="0" ReadOnly="true">
                                 </telerik:RadTextBox>
                             </div>
                             <label for="Account" class="control-label col-sm-3">
                                 Coaching Date</label>
                             <div class="col-sm-3">
                                 <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                     Text="0" ReadOnly="true">
                                 </telerik:RadTextBox>
                             </div>
                         </div>
                     </div>
                 </div>
                 <hr />
                  <div class="panel-body">
                        <div class="form-group col-sm-6" id="SelectCoacheeGroup" runat="server">
                            <form class="form-horizontal">
                            <label for="name2" class="control-label">
                                Select Coachee</label>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="email">
                                    Review Type:</label>
                                <div class="col-sm-8">
                                    <telerik:RadComboBox ID="RadMassType" Width="80%" runat="server" class="form-control"
                                        RenderMode="Lightweight" DropDownAutoWidth="Enabled" CausesValidation="true"
                                        AutoPostBack="true" EmptyMessage="-Select Type-" OnSelectedIndexChanged="RadMassType_SelectedIndexChanged">
                                        <%--<Items>
                                            <telerik:RadComboBoxItem Text="Mass Review All" Value="1" />
                                        </Items>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="Mass Review Groups" Value="2" />
                                        </Items>
                                        <Items>
                                            <telerik:RadComboBoxItem Text="Mass Review Selected" Value="3" />
                                        </Items>--%>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ValidationGroup="MassCoaching"
                                        ControlToValidate="RadMassType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                        CssClass="validator"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                &nbsp;
                            </div>
                            <asp:Panel ID="PanelGroups" runat="server">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Supervisor">
                                        Site:</label>
                                    <div class="col-sm-8">
                                    
                                        <telerik:RadComboBox ID="RadSite" Width="80%" runat="server" class="form-control"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="-Select Site-" >
                                        </telerik:RadComboBox>
                              
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-4">
                                        Account:</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadAccount" Width="80%" runat="server" class="form-control"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" CausesValidation="true"
                                            EmptyMessage="-Select Account-">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-4">
                                        Campaign:</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadCampaign" Width="80%" runat="server" class="form-control"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="-Select Campaign-"
                                            AutoPostBack="true">
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="PanelSelected" runat="server">
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <label for="CoacheeName" class="control-label col-sm-4">
                                    Cim Number:</label>
                                <div class="col-sm-8">
                                    <telerik:RadTextBox ID="RadCimNumber" runat="server" placeholder="CIM Number" MaxLength="10">
                                    </telerik:RadTextBox>
                                    <telerik:RadButton ID="RadAdd" runat="server" CssClass="btn btn-info btn-small" ForeColor="White"
                                        Text="Add" OnClick="RadAdd_Click">
                                    </telerik:RadButton>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGridEmployees" runat="server" AutoGenerateColumns="false"
                                        OnDeleteCommand="RadGrid1_DeleteCommand" OnNeedDataSource="RadGrid1_NeedDataSource"
                                        AllowAutomaticInserts="false" AllowAutomaticUpdates="true" EnableLoadOnDemand="True"
                                        EnableViewState="true" CssClass="RemoveBorders" GridLines="None" BorderStyle="None" RenderMode="Lightweight">
                                        <MasterTableView DataKeyNames="CIM">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="CIM Number" UniqueName="CIM" DataField="CIM"
                                                    Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblCIM" runat="server" Text='<%# Eval("CIM") %>' DataTextField="CIM"
                                                            Visible="true">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Name" UniqueName="Name" DataField="Name"
                                                    Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblName" runat="server" Text='<%# Eval("Name") %>' DataTextField="Name"
                                                            Visible="true">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Text="Delete"
                                                    ButtonType="PushButton" HeaderText="Delete" ConfirmTextFormatString="Are you sure you want to delete the selected item?"
                                                    ConfirmTextFields="Model" ConfirmDialogType="RadWindow">
                                                </telerik:GridButtonColumn>--%>
                                                <telerik:GridTemplateColumn UniqueName="DeleteColumn">
                                                    <ItemTemplate>
                                                        <telerik:RadButton ID="ImageButton1" runat="server" OnClientClicking="OnClientClicking" CommandName="Delete" Text="Delete"></telerik:RadButton>                                                         
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                          <ClientSettings>
                                        <Scrolling AllowScroll="True" />
                                    </ClientSettings>
                                    </telerik:RadGrid>
                                </div>
                            </div>                       
                            <div>
                                &nbsp;</div>
                            </asp:Panel>
                            </form>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="name2" class="control-label">
                                Coaching Specifics</label>
                            <div class="form-group">
                                <label for="SessionType" class="control-label col-sm-4">
                                    Session Type</label>
                                <div class="col-sm-8">
                                    <telerik:RadComboBox ID="RadSessionType" Width="80%" runat="server" class="form-control"
                                        AutoPostBack="true" RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="4"
                                        OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged" EmptyMessage="-Select Session Type-">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="MassCoaching"
                                        ControlToValidate="RadSessionType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                        CssClass="validator"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                &nbsp;
                            </div>
                            <div class="form-group">
                                <label for="Topic" class="control-label col-sm-4">
                                    Topic</label>
                                <div class="col-sm-8">
                                    <telerik:RadComboBox ID="RadSessionTopic" Width="80%" runat="server" class="form-control"
                                        DropDownAutoWidth="Enabled" TabIndex="5" EmptyMessage="-Select Session Topic-">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="MassCoaching"
                                        ControlToValidate="RadSessionTopic" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                        CssClass="validator"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                &nbsp;
                            </div>
                        </div>                     
                    </div>
             </div>
         </div>
         <div class="panel panel-custom">
             <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                 <div class="panel-heading">
                     <h4 class="panel-title">
                         Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                     </h4>
                 </div>
             </a>
             <div id="collapseTwo" class="panel-collapse collapse in">
                 <div class="panel-body">
                     <telerik:RadTextBox ID="RadDescription" class="form-control" placeholder="Quick description or purpose of the review session"
                         runat="server" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" ValidationGroup="MassCoaching"  onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                         <ClientEvents OnLoad="RadTextBoxLoad" />
                     </telerik:RadTextBox>
                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="MassCoaching"
                         ForeColor="Red" ControlToValidate="RadDescription" Display="Dynamic" ErrorMessage="*This is a required field."
                         CssClass="validator"></asp:RequiredFieldValidator>
                 </div>
             </div>
         </div>
         <div class="panel panel-custom">
             <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                 <div class="panel-heading">
                     <h4 class="panel-title">
                         Agenda <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                     </h4>
                 </div>
             </a>
             <div id="collapseThree" class="panel-collapse collapse in">
                 <div class="panel-body">
                     <telerik:RadTextBox ID="RadAgenda" class="form-control" placeholder="State the agenda of the review"
                         runat="server" TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="15" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                         <ClientEvents OnLoad="RadTextBoxLoad" />
                     </telerik:RadTextBox>
                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="MassCoaching"
                         ForeColor="Red" ControlToValidate="RadAgenda" Display="Dynamic" ErrorMessage="*This is a required field."
                         CssClass="validator"></asp:RequiredFieldValidator>
                 </div>
             </div>
         </div>
         <div class="panel panel-custom">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseQADocumentation">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                            </span>
                        </h4>
                    </div>
                </a>
             <div id="collapseQADocumentation" class="panel-collapse collapse in">
                 <div class="panel-body">
                    <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                        AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                        OnItemDataBound="RadGridDocumentation_onItemDatabound" CssClass="RemoveBorders" BorderStyle="None">
                        <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Item Number">
                                     <ItemTemplate>
                                         <asp:Label ID="LabelCT"  runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                     </ItemTemplate>
                                 </telerik:GridTemplateColumn>
                                 <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path"
                                     UniqueName="FilePath" FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false"  >
                                 </telerik:GridHyperLinkColumn>
                                 <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName" UniqueName="DocumentName"
                                     FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName" AllowSorting="true" Target="_blank"
                                     ShowSortIcon="true">
                                </telerik:GridHyperLinkColumn>
                                 <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                     <ItemTemplate>
                                         <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                     </ItemTemplate>
                                 </telerik:GridTemplateColumn>
                                 <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                     <ItemTemplate>
                                         <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                     </ItemTemplate>
                                 </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                     <div>
                     &nbsp;
                     </div>
                     <div class="col-sm-12">
                         <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" MultipleFileSelection="Automatic" AllowedFileExtensions=".pdf,.png,.xls,.xlsx" MaxFileSize="1000000" Width="40%" OnClientValidationFailed="OnClientValidationFailed" PostbackTriggers="RadSend">
                        <Localization Select="" />
                        </telerik:RadAsyncUpload>
                     </div>
                 </div>
             </div>
            </div>
         <div class="row">
             <br>
             <div class="col-sm-12">
                 <span class="addreviewbuttonholder">
                     <telerik:RadButton ID="RadSend" CssClass="btn btn-info btn-small" runat="server"
                         Text="Send" ValidationGroup="MassCoaching" ForeColor="White" 
                     onclick="RadSend_Click">
                     </telerik:RadButton>
                 </span>
             </div>
         </div>
           
     </div>
 </div> 
 <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Width="300px" Height="150px">
        <Windows>
            <telerik:RadWindow runat="server" ID="Window1" Modal="true" Behaviors="Maximize,Minimize,Move">
                <ContentTemplate>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</asp:Content>
