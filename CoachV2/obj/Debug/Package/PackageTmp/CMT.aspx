﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CMT.aspx.cs" Inherits="CoachV2.CMT" %>


<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
  <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

<div class="menu-content bg-alt">
<%--<ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />--%>
<div class="panel menuheadercustom" runat="server" id ="myreviewtitle">
    <div>
        &nbsp;<span class="glyphicon glyphicon-dashboard"></span> MY REVIEWS<span class="breadcrumb2ndlevel"><asp:Label
            ID="Label1" runat="server" Text=" "></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text="" Visible="false"></asp:Label></span></div>
</div>
    <div class="panel panel-custom">
        <div id="HRComments" class="panel-collapse collapse in">
            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    <u>Coaching Specifics</u></label><br />
                <div style="height: 10px">
                </div>
                <hr />
                <div class="form-group">
                    <label for="Account" class="control-label col-sm-4">
                        CIM:</label>
                    <div class="col-sm-8">
                        <asp:Label ID="CMTCim" runat="server"></asp:Label>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <div class="form-group">
                    <label for="Supervisor" class="control-label col-sm-4">
                        Supervisor</label>
                    <div class="col-sm-8">
                       <asp:Label ID="CMTSupervisor" runat="server"></asp:Label>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <div class="form-group">
                    <label for="CoacheeName" class="control-label col-sm-4">
                        Department</label>
                    <div class="col-sm-8">
                         <asp:Label ID="CMTDepartment" runat="server"></asp:Label>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <div class="form-group">
                    <label for="CoacheeName" class="control-label col-sm-4">
                        Campaign</label>
                    <div class="col-sm-8">
                         <asp:Label ID="CMTAccount" runat="server"></asp:Label>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <div class="form-group">
                    <label for="CoacheeName" class="control-label col-sm-4">
                        Session Type</label>
                    <div class="col-sm-8">
                         <asp:Label ID="CMTSessionType" runat="server"></asp:Label>
                    </div>
                </div>
                 <div>
                    &nbsp;
                </div>
                <div class="form-group">
                    <label for="CoacheeName" class="control-label col-sm-4">
                        Topic</label>
                    <div class="col-sm-8">
                         <asp:Label ID="CMTTopic" runat="server"></asp:Label>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results">
                   <u>HR Comments</u></label><br />
                <div style="height: 10px">
                </div>
                <hr />
               <asp:Label ID="CMTHRComments" runat="server"></asp:Label>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    Previous Coaching Notes</label><br />
                <div style="height: 10px">
                </div>
                <div class="panel-body">
                    <telerik:RadGrid ID="RadGridCN" CssClass="RemoveBorders" RenderMode="Lightweight"
                        runat="server" AutoGenerateColumns="true">
                        <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    Previous Perfomance Results</label><br />
                <div style="height: 10px">
                </div>
                <div>
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                            <MasterTableView>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="CIM Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Session">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Target">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Current">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Previous">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Driver">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Assigned By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    <u>Documentation</u></label><br />
                <div style="height: 10px">
                </div>
                <div class="panel-body">
                    <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                        AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                        CssClass="RemoveBorders" BorderStyle="None" RenderMode="Lightweight">
                        <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Item Number">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                               <telerik:GridTemplateColumn HeaderText="Item Number">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelUploaded" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <div>
                    &nbsp;
                </div>
            
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    <u>NTE Details</u></label><br />
                <div style="height: 10px">
                </div>
                <h5>--No Details--</h5>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    <u>Preventive Suspension Details</u></label><br />
                <div style="height: 10px">
                </div>
                   <h5>--No Details--</h5>
            </div>

            <div class="panel-body">
                <label for="Previous Perfomance Results" >
                    <u>NOD Details</u></label><br />
                <div style="height: 10px">
                </div>
                <h5>--No Details--</h5>
            </div>
        </div>
    </div>

</div>
</asp:Content>
