﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportBuilder.aspx.cs" Inherits="CoachV2.ReportBuilder" %>

<%@ Register Src="UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/DashboardMyReviewsReport.ascx" TagName="DashboardMyReviewsUserControl" TagPrefix="ucdash5" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<link href="Content/dist/css/sb-admin-2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="libs/js/JScript1.js"></script>
<style type="text/css">

    div.RadListBox .rlbTransferTo,
    div.RadListBox .rlbTransferToDisabled,
    div.RadListBox .rlbTransferAllToDisabled,
    div.RadListBox .rlbTransferAllTo,
    div.RadListBox .rlbTransferAllRight,
    div.RadListBox .rlbMoveUp,
    div.RadListBox .rlbMoveDown,
    div.RadListBox .rlbTransferAllLeft,
    div.RadListBox .rlbButtonAreaLeft
    {
       display: none;
    }
 

    
</style>
    <telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">
            var sema_used;
            function itemClicked(sender, args) {
                if (!sema_used) {
                    sema_used = true;
                    var item = args.get_item();
                    if (item.get_checked()) {
                        item.uncheck();
                        item.unselect();
                    }
                    else {
                        item.check();
                        item.select();
                    }
                    selectCheckedItems(item.get_listBox());
                    sema_used = false;
                }
            }
            function OnClientSelectedIndexChanged(sender, args) {
                args.get_item();
                sender.get_checkedItems();
            }
            function OnClientItemChecked(sender, args) {
                args.get_item().set_selected(args.get_item().get_checked());
            }
            function OnClientSelectedIndexChanged(sender, args) {
                args.get_item();
                sender.get_checkedItems();
            }
            function selectCheckedItems(listbox) {
                if (listbox) {
                    var items = listbox.get_checkedItems();
                    for (var i in items) {
                        items[i].select();
                    }
                }
            }
            function clientTransfering(sender, args) {
              args.set_cancel(true);
                var checkedNodes = args.get_sourceListBox().get_checkedItems();
//                for (var i in checkedNodes) {
//                    var item = checkedNodes[i];

//                    args.get_sourceListBox().transferToDestination(item);
//                                }
                document.getElementById('<%= RadButton1.ClientID %>').click();
            }
            function fncsave() {
                document.getElementById('<%= RadButton1.ClientID %>').click();
            }
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= radUsers.UniqueID %>") {
                    args.set_enableAjax(false);
                }

                if (args.get_eventTarget() == "<%= radReviews.UniqueID %>") {
                    args.set_enableAjax(false);
                }

                if (args.get_eventTarget() == "<%= RadPreview.UniqueID %>") {
                    args.set_enableAjax(false);
                }

                if (args.get_eventTarget() == "<%= RadSave.UniqueID %>") {
                    args.set_enableAjax(false);
                }

                if (args.get_eventTarget() == "<%= radKPI.UniqueID %>") {
                    args.set_enableAjax(false);
                }


                if (args.get_eventTarget() == "<%= radHRTables.UniqueID %>") {
                    args.set_enableAjax(false);
                }

                if (args.get_eventTarget() == "<%= RadButton1.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }

        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadListBox1">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadListBox2">
                <UpdatedControls>
                  <%--  <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAdd">
                <UpdatedControls>
              <%--      <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAddAll">
                <UpdatedControls>
                   <%-- <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonRemove">
                <UpdatedControls>
                   <%-- <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonRemoveAll">
                <UpdatedControls>
                 <%--   <telerik:AjaxUpdatedControl ControlID="RadListBox1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadListBox2" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
<%--    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>--%>
       <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" Transparency="1">   
       
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="conditionalPostback">
        <asp:Panel ID="pnlMain" runat="server">
            <div class="menu-content bg-alt">
                <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
                <div class="panel-group" id="accordion">
                    <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Build Report <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                </h4>
                            </div>
                        </a>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="form-group col-sm-12" id="SelectCoacheeGroup" runat="server">
                                    <form class="form-horizontal">
                                    <div class="form-group">
                                        <asp:HiddenField ID="HiddenReportID" runat="server" Value="0" />
                                        <asp:HiddenField ID="HiddenTableName" runat="server" Value="0" />
                                        <asp:HiddenField ID="HiddenTableID" runat="server" Value="0" />
                                        <label class="control-label col-sm-2" for="email">
                                            Report Name</label>
                                        <div class="col-sm-1">
                                            <asp:Label ID="LblValidator" runat="server" Text="*" Width="100%" CssClass="validator">
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-9">
                                            <telerik:RadTextBox ID="RadReportName" runat="server" PlaceHolder="Report Name" Width="100%">
                                            </telerik:RadTextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="BuildReport"
                                                ControlToValidate="RadReportName" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="selectdatasource" class="control-label col-sm-3">
                                            Select Date Range</label>
                                        <div class="col-sm-4">
                                            <telerik:RadDatePicker ID="RadDateRangeFrom" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="col-sm-4">
                                            <telerik:RadDatePicker ID="RadDateRangeTo" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="75%">
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    <div>
                                        &nbsp;</div>
                                    <div class="form-group">
                                        <label for="selectdatasource" class="control-label col-sm-3">
                                            Select Data Source</label>
                                        <div class="col-sm-9">
                                            <telerik:RadButton ID="radUsers" runat="server" Text="Users" CssClass="list-group-item"
                                                OnClick="radUsers_Click">
                                            </telerik:RadButton>
                                            &nbsp;
                                            <telerik:RadButton ID="radReviews" runat="server" Text="Reviews" CssClass="list-group-item"
                                               OnClick="radReviews_Click">
                                            </telerik:RadButton>
                                            &nbsp;
                                            <telerik:RadButton ID="radKPI" runat="server" Text="KPI" 
                                                CssClass="list-group-item" onclick="radKPI_Click">
                                            </telerik:RadButton>
                                            &nbsp;
                                            <telerik:RadButton ID="radHRTables" runat="server" Text="HR Tables" 
                                                CssClass="list-group-item" onclick="radHRTables_Click">
                                            </telerik:RadButton>
                                            &nbsp;
                                            <telerik:RadButton ID="radECN" runat="server" Text="ECN" CssClass="list-group-item">
                                            </telerik:RadButton>
                                            &nbsp;
                                        </div>
                                    </div>
                                        <div>
                                            &nbsp;</div>
                                        <div>
                                            &nbsp;</div>
                                            
                                        <asp:Panel ID="Panel1" runat="server">
                                            <asp:Panel ID="Rbuilder" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <label for="selectdatasource" class="control-label col-sm-12">
                                                        Select Data Fields</label>
                                                    <div class="col-sm-3">
                                                        <telerik:RadButton ID="RadButtonAdd" Width="100px" runat="server" CssClass="btn btn-info btn-small"
                                                            ForeColor="White" Text="Add"
                                                            OnClick="RadAdd_Click">
                                                        </telerik:RadButton>
                                                        <br />
                                                        <div>
                                                            &nbsp;</div>
                                                        <telerik:RadButton ID="RadButtonAddAll" Width="100px" runat="server" CssClass="btn btn-info btn-small"
                                                            ForeColor="White" Text="Add All"
                                                             OnClick="RadButtonAddAll_Click">
                                                        </telerik:RadButton>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <telerik:RadListBox RenderMode="Lightweight" ID="RadListBox1" CssClass="horizontalListbox"
                                                            runat="server" SelectionMode="Multiple" AllowTransfer="true" TransferToID="RadListBox2"
                                                            AutoPostBackOnTransfer="true" AllowReorder="true" Skin="Office2010Blue" AutoPostBackOnReorder="true"
                                                            EnableDragAndDrop="true" AutoPostBack="true" CheckBoxes="true" ButtonSettings-ShowTransfer="true" OnClientTransferring="clientTransfering"  OnClientItemChecked="OnClientItemChecked" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"  >
                                                          <%--  OnClientSelectedIndexChanged="itemClicked" OnClientItemChecked="itemClicked" OnClientDropped="fncsave"--%>
                                                            <ButtonSettings RenderButtonText="true" ShowTransferAll="true" />
                                                            <Localization AllToLeft="Add All" AllToRight="All To Right" ToLeft="Add" />
                                                            <ButtonSettings AreaHeight="30" Position="Left" HorizontalAlign="Center" AreaWidth="90px" />
                                                        </telerik:RadListBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="form-group">
                                                    <label for="selectdatasource" class="control-label col-sm-12">
                                                        Selected Data Fields</label>
                                                    <div class="col-sm-3">
                                                        <telerik:RadButton ID="RadButtonRemove" Width="100px" runat="server" CssClass="btn btn-info btn-small"
                                                            ForeColor="White" Text="Remove" onclick="RadButtonRemove_Click">
                                                        </telerik:RadButton>
                                                        <br />
                                                        <div>
                                                            &nbsp;</div>
                                                        <telerik:RadButton ID="RadButtonRemoveAll" Width="100px" runat="server" CssClass="btn btn-info btn-small"
                                                            ForeColor="White" Text="Remove All" onclick="RadButtonRemoveAll_Click">
                                                        </telerik:RadButton>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <telerik:RadListBox RenderMode="Lightweight" ID="RadListBox2" CssClass="horizontalListbox"
                                                            runat="server" SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true"
                                                            EnableDragAndDrop="true" ButtonSettings-ShowTransfer="true" Skin="Office2010Blue"
                                                            CheckBoxes="true" AllowTransfer="true" AutoPostBack="true" TransferToID="RadListBox1" OnClientTransferring="clientTransfering" OnClientItemChecked="OnClientItemChecked" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"   > <%--  OnClientDropped="fncsave" OnClientSelectedIndexChanged="itemClicked" OnClientItemChecked="itemClicked"  >--%>
                                                            <EmptyMessageTemplate>
                                                                Drag and drop Data Fields here to create report...</EmptyMessageTemplate>
                                                            <Localization AllToLeft="Remove All" ToLeft="Remove" />
                                                            <ButtonSettings AreaHeight="30" Position="Left" HorizontalAlign="Center" AreaWidth="90px" />
                                                            <ButtonSettings RenderButtonText="true" ShowTransferAll="true" />
                                                        </telerik:RadListBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    &nbsp;</div>
                                                <div>
                                                    &nbsp;</div>
                                                <div class="panel-body">
                                                    <div id="Div1" class="container-fluid" runat="server">
                                                        <div class="row">
                                                            <div class="col-md-12 text-right">
                                                                <telerik:RadButton ID="RadPreview" runat="server" Text="Preview" CssClass="btn btn-info btn-small"
                                                                    ForeColor="White" ValidationGroup="BuildReport"
                                                                     OnClick="RadPreview_Click">
                                                                </telerik:RadButton>
                                                                <telerik:RadButton ID="RadSave" runat="server" Text="Submit" CssClass="btn btn-info btn-small"
                                                                    ForeColor="White" ValidationGroup="BuildReport" OnClick="RadSave_Click">
                                                                </telerik:RadButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                              
                                            </asp:Panel>
                                            <telerik:RadButton ID="RadButton1" Width="100px" runat="server" CssClass="btn btn-info btn-small"
                                                ForeColor="White" Text="Add" OnClick="RadTestButton_Click"  Style="display: none;">
                                            </telerik:RadButton>
                                        </asp:Panel>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>
