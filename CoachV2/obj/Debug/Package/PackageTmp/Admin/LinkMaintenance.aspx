﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LinkMaintenance.aspx.cs" Inherits="CoachV2.LinkMaintenance" %>

<%@ Register Src="~/UserControl/SidebarDashboardUserControl.ascx" TagName="SidebarUserControl1"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/DashboardMyReviews.ascx" TagName="DashboardMyReviewsUserControl"
    TagPrefix="ucdash5" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<script type="text/javascript" src="~/libs/js/JScript1.js"></script>
    <link href="~/libs/css/gridview.css" rel="stylesheet" type="text/css" />
    <link href="~/Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var uploadedFilesCount = 0;
            var isEditMode;
            function validateRadUpload(source, e) {
                // When the RadGrid is in Edit mode the user is not obliged to upload file.
                if (isEditMode == null || isEditMode == undefined) {
                    e.IsValid = false;

                    if (uploadedFilesCount > 0) {
                        e.IsValid = true;
                    }
                }
                isEditMode = null;
            }

            function OnClientFileUploaded(sender, eventArgs) {
                uploadedFilesCount++;
            }
             
        </script>
    </telerik:RadCodeBlock>
   <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
       <AjaxSettings>
           <telerik:AjaxSetting AjaxControlID="RadGrid1">
               <UpdatedControls>
                   <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
               </UpdatedControls>
           </telerik:AjaxSetting>
       </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="conditionalPostback">
        <asp:Panel ID="pnlMain" runat="server">
            <div class="menu-content bg-alt">
                   <div class="panel-group" id="accordion">
                      <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Link Maintenance <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                    </span>
                                </h4>
                            </div>
                        </a>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body" style="padding-bottom: 5px">
                                <telerik:RadGrid runat="server" ID="RadGrid1" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" GridLines="None" OnItemCreated="RadGrid1_ItemCreated"
                                    PageSize="10" OnNeedDataSource="RadGrid1_NeedDataSource"
                                    OnUpdateCommand="RadGrid1_UpdateCommand"
                                    OnItemCommand="RadGrid1_ItemCommand">
                                    <PagerStyle Mode="NumericPages" AlwaysVisible="true"></PagerStyle>
                                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="ID">
                                    <CommandItemSettings ShowAddNewRecordButton="false" />
                                        <Columns>
                                            <telerik:GridEditCommandColumn>
                                                <HeaderStyle Width="36px"></HeaderStyle>
                                            </telerik:GridEditCommandColumn>
                                            <telerik:GridTemplateColumn HeaderText="Description" UniqueName="Description" DataField="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%# TrimDescription(Eval("Description") as string) %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox RenderMode="Lightweight" ID="txbDescription" Width="300px" runat="server"
                                                        TextMode="MultiLine" Text='<%# Eval("Description") %>' Height="150px">
                                                    </telerik:RadTextBox>
                                                </EditItemTemplate>
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="File Name" UniqueName="FileName" SortExpression="Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblFileName" Text='<%# Eval("File_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox RenderMode="Lightweight" runat="server" Width="300px" ID="txbFileName"
                                                        Text='<%# Eval("File_Name") %>' Enabled="false">
                                                    </telerik:RadTextBox>
                                                </EditItemTemplate>
                                                <HeaderStyle Width="30%"></HeaderStyle>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Link" UniqueName="Link" DataField="Link">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Link") %>' runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                        Target="_blank">
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadTextBox RenderMode="Lightweight" ID="txbLink" Width="300px" runat="server"
                                                        TextMode="MultiLine" Text='<%# Eval("Link") %>' Height="150px">
                                                    </telerik:RadTextBox>
                                                </EditItemTemplate>
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="File" HeaderText="File" UniqueName="Upload">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFile" runat="server" Text='<%# TrimDescription(Eval("File_Name") as string) %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadAsyncUpload RenderMode="Lightweight" runat="server" ID="AsyncUpload1"
                                                        OnClientFileUploaded="OnClientFileUploaded" AllowedFileExtensions="pdf" MaxFileSize="1048576"
                                                        OnFileUploaded="AsyncUpload1_FileUploaded" OnClientValidationFailed="OnClientValidationFailed">
                                                        <Localization Select="                             " />
                                                    </telerik:RadAsyncUpload>
                                                </EditItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn ButtonType="FontIconButton">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                   </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>
