﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddMassReview.aspx.cs" Inherits="CoachV2.AddMassReview" %>
<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<%@ Register src="UserControl/MassCoachingAll.ascx" tagname="MassCoachingAll" tagprefix="ucMassAll" %>
<%@ Register src="UserControl/MassCoachingGroups.ascx" tagname="MassCoachingGroups" tagprefix="ucMassGroups" %>
<%@ Register src="UserControl/MassCoachingSelected.ascx" tagname="MassCoachingSelected" tagprefix="ucMassSelected" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <div class="menu-content bg-alt">
        <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
         <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseMassType">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Mass Coaching Type <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                    </h4>
                </div>
            </a>
            <div id="collapseMassType" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Account" class="control-label col-xs-4">
                            Mass Coaching Type</label>
                        <div class="col-xs-8">
                            <telerik:RadComboBox ID="RadMassType" runat="server" class="form-control" RenderMode="Lightweight"
                                DropDownAutoWidth="Enabled" CausesValidation="true" AutoPostBack="true" 
                                onselectedindexchanged="RadMassType_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="Mass Coaching All" Value="1" />
                                </Items>
                                <Items>
                                    <telerik:RadComboBoxItem Text="Mass Coaching Groups" Value="2" />
                                </Items>
                                <Items>
                                    <telerik:RadComboBoxItem Text="Mass Coaching Selected" Value="3" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <ucMassAll:MassCoachingAll ID="MassCoachingAll2" runat="server" />
         <ucMassGroups:MassCoachingGroups ID="MassCoachingGroups1" runat="server" />
         <ucMassSelected:MassCoachingSelected ID="MassCoachingSelected1" runat="server" />
      
       
    </div>
</asp:Content>
