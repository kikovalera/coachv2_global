﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="Dashboard.aspx.cs" Inherits="CoachV2.Dashboard" %>
<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMain.ascx" tagname="SidebarUserControl2" tagprefix="ucdash1" %>
<%@ Register src="UserControl/AgentDashboardMain.ascx" tagname="UserControl3" tagprefix="ucdash2" %>
<%@ Register src="UserControl/TLChartDashboard.ascx" tagname="UserControl4" tagprefix="ucdash4" %>
<%@ Register src="UserControl/DashboardMenuTopMyReview.ascx" tagname="HeadUserControl1" tagprefix="ucdash3" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<%@ Register src="UserControl/OM_DirCharts.ascx" tagname="UserControl7" tagprefix="ucdash7" %>
<%@ Register src="UserControl/QAChartDB.ascx" tagname="UserControl6" tagprefix="ucdash6" %>
<%@ Register src="UserControl/DashboardAgentRosterUserCtrl.ascx" tagname="DashboardAgentRosterUserCtrl" tagprefix="uc2" %>
<%@ Register src="UserControl/TLMainDashboard.ascx" tagname="UserControlTL1" tagprefix="ucdashTL1" %>
<%@ Register src="UserControl/AgentDashboardMyReview.ascx" tagname="UserControlAgent2" tagprefix="ucdashagent2" %>
<%@ Register src="UserControl/OMDashboardMain.ascx" tagname="UserControlOM1" tagprefix="ucdashOM1" %>
<%@ Register src="UserControl/DirDashboardMain.ascx" tagname="UserControlDir1" tagprefix="ucdashDir1" %>
<%@ Register src="UserControl/VPDashboardMain.ascx" tagname="UserControlVP1" tagprefix="ucdashVP1" %>

<%@ Register src="UserControl/MyReviewRecentlyReviewedUserCtrl.ascx" tagname="MyReviewRecentlyReviewedUserCtrl" tagprefix="uc3" %>
<%@ Register src="UserControl/MyReviewOpenForMeUserCtrl.ascx" tagname="MyReviewOpenForMeUserCtrl" tagprefix="uc4" %>


<%@ Register src="UserControl/QADashboard.ascx" tagname="QADashboard" tagprefix="uc5" %>

<%@ Register src="UserControl/QA_DashboardwithTriad.ascx" tagname="QADashboardwithTriad" tagprefix="uc6" %>
<%@ Register src="UserControl/HRDashboard.ascx" tagname="HRDashboard" tagprefix="ucHR" %>
<%@ Register src="UserControl/HR_DashboardwithTriad.ascx" tagname="HRDashboardwithTriad" tagprefix="ucHR1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">

<uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<%--<ucdash3:HeadUserControl1  ID="MyReviewHead" runat="server" Visible="false" />
<uc2:DashboardAgentRosterUserCtrl ID="DashboardAgentRosterUserCtrl1" runat="server" Visible="false" />
<ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" Visible="false" />
<uc5:QADashboard ID="QADashboard1" runat="server" Visible="false" />
<uc6:QADashboardwithTriad ID="QADashboardT" runat="server" Visible="false" />

<ucdash2:UserControl3  ID="AgentMain" runat="server" Visible="false" />
<ucdashagent2:UserControlAgent2 ID="AgentMyReview"  runat="server" Visible="false" />
<uc3:MyReviewRecentlyReviewedUserCtrl ID="MyReviewRecentlyReviewedUserCtrl1" 
        runat="server" Visible="false" />
<uc4:MyReviewOpenForMeUserCtrl ID="MyReviewOpenForMeUserCtrl1" runat="server" Visible="false" />
    
<ucdashTL1:UserControlTL1 ID="TLMainDB" runat ="server" visible = "false" />
<ucdashOM1:UserControlOM1 ID="OMDashboard1" runat="server" Visible="false" />
<ucdash7:UserControl7  ID="OMDirChartsDB" runat="server" Visible="false" />
<ucdash6:UserControl6  ID="QAChartsDB" runat="server" Visible="false" />
<ucdashDir1:UserControlDir1  ID="UControlDir1" runat="server" Visible="false" />
<ucHR:HRDashboard  ID="HRDashboard1" runat="server" Visible="false" />
<ucHR1:HRDashboardwithTriad  ID="HRDashboardwithTriad1" runat="server" Visible="false" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>

    <div class="menu-content bg-alt">
        <div id="DashboardDiv" runat="server">
        </div>
        <div class="panel-body" id="MyTeam" runat="server">
        </div>
    </div>
</asp:Content>