﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateReviewNexidia.aspx.cs" Inherits="CoachV2.UpdateReviewNexidia" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/RevNexidia.ascx" tagname="RevNexidia1" tagprefix="revNex1" %>
<%@ Register src="UserControl/RevOD.ascx" tagname="RevOD" tagprefix="revOD1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
  <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<script type="text/javascript" src="libs/js/JScript1.js"></script>
<link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
 <asp:Panel ID="pn_coacheedetails" Visible="false"  runat="server" >
  <div class="panel menuheadercustom">
    <div>
        &nbsp;<span><asp:Label
            ID="Label3" runat="server" Text=""></asp:Label></span><span><asp:Label
            ID="Label1" runat="server" Text=""></asp:Label></span><span class="breadcrumb2ndlevel"><asp:Label
      ID="Label2" runat="server" Text=""></asp:Label></span>
     </div>
</div>
        <div class="panel-group" id="Div1">
       <div class="panel panel-custom">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                        </span>
                    </h4>
                </div>
            </a>
            <div id="Div2" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="Account" class="control-label col-sm-3">
                            Coaching Ticket</label>
                        <div class="col-sm-3">
                            <telerik:RadTextBox ID="RadReviewID" runat="server" class="form-control" Text="0"
                                ReadOnly="true" Enabled="false" Width="100%">
                            </telerik:RadTextBox>
                        </div>
                        <label for="Account" class="control-label col-sm-3">
                            Coaching Date</label>
                        <div class="col-sm-3">
                            <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                Text="0" ReadOnly="true">
                            </telerik:RadTextBox>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="container-fluid" id="Div6" runat="server">
                        <div class="row">
                            <div class="col-md-4">
                                <asp:HiddenField ID="FakeURLID" runat="server" />
                                <div id="profileimageholder">
                                    <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5>
                                    <b>Name: </b><span>
                                        <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span></h5>
                                <h5>
                                    <b>Position: </b><span>
                                        <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b>Site: </b><span>
                                        <asp:Label ID="LblSite" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b>Reporting Manager: </b><span>
                                        <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b>Region: </b><span>
                                        <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b>Country: </b><span>
                                        <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                <h5 style="display: none;">
                                    <b>Campaign: </b><span>
                                        <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b>Client: </b><span>
                                        <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5> <!-- edited by Charlie -->
                                <h5>
                                    <b>Organizational Unit: </b><span>
                                        <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                <h5>
                                    <b></b><span>
                                        <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                            </div>
                            <div class="col-md-4">
                                <h5>
                                    <b>Session Type: </b><span>
                                        <asp:Label ID="LblSessionType" runat="server" Text="My Session Type" /></span></h5>
                                <h5>
                                    <b>Topic: </b><span>
                                        <asp:Label ID="LblSessionTopic" runat="server" Text="My Session Type" /></span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

               
    

</asp:Panel>

<revNex1:RevNexidia1 ID="RevNex1" runat="server"/>
<revOD1:RevOD ID="RevOD2" runat="server" />
    
<div class="panel-body">
                <div class="container-fluid" id="SignOut" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <telerik:RadTextBox ID="RadCoacheeCIM" runat="server" class="form-control" RenderMode="Lightweight"
                            TabIndex="12" placeholder="Coachee CIM Number" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="SSN"
                            RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview" Visible="false"
                            ForeColor="White" onclick="RadCoacheeSignOff_Click">
                            </telerik:RadButton>
                           <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off"
                            CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                            ForeColor="White" onclick="RadCoacherSignOff_Click" Visible="false">
                            </telerik:RadButton>
                            <telerik:RadButton ID="btn_Audit1" runat="server" Text="Proceed to Audit" Visible="false" ValidationGroup="AddReview"
                                CssClass="btn btn-info btn-small" ForeColor="White" OnClick="btn_Audit1_Click">
                            </telerik:RadButton>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Label ID="lblstatus" runat="server"  ForeColor="Red" Font-Size="Small" ></asp:Label>
    <div class="form-group">
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Width="1000px" Height="500px">
            </telerik:RadWindowManager>
        </div>
</asp:Content>
