﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CoachV2.AppCode;
using System.Data;

namespace CoachV2
{
    public partial class LinkMaintenance : System.Web.UI.Page
    {

        const int MaxTotalBytes = 1048576; // 1 MB
        Int64 totalBytes;

        public bool? IsRadAsyncValid
        {
            get
            {
                if (Session["IsRadAsyncValid"] == null)
                {
                    Session["IsRadAsyncValid"] = true;
                }

                return Convert.ToBoolean(Session["IsRadAsyncValid"].ToString());
            }
            set
            {
                Session["IsRadAsyncValid"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            IsRadAsyncValid = null;
        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                RadAsyncUpload upload = ((GridEditableItem)e.Item)["Upload"].FindControl("AsyncUpload1") as RadAsyncUpload;
                TableCell cell = (TableCell)upload.Parent;

                CustomValidator validator = new CustomValidator();
                validator.ErrorMessage = "Please select file to be uploaded";
                validator.ClientValidationFunction = "validateRadUpload";
                validator.Display = ValidatorDisplay.Dynamic;
                cell.Controls.Add(validator);
            }
        }

        protected string TrimDescription(string description)
        {
            if (!string.IsNullOrEmpty(description) && description.Length > 200)
            {
                return string.Concat(description.Substring(0, 200), "...");
            }
            return description;
        }

        protected void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DataAccess ws = new DataAccess();
            DataSet ds = null;
            ds = ws.GetCoachLinks();
            if (ds.Tables[0].Rows.Count > 0)
            {
                RadGrid1.DataSource = ds;
            }    
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadGrid1_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (!IsRadAsyncValid.Value)
                {
                    e.Canceled = true;
                    RadAjaxManager1.Alert("The length of the uploaded file must be less than 1 MB");
                    return;
                }

                GridEditableItem editedItem = e.Item as GridEditableItem;
                int ID = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ID"].ToString());
                string imageName = (editedItem["FileName"].FindControl("txbFileName") as RadTextBox).Text;
                string description = (editedItem["Description"].FindControl("txbDescription") as RadTextBox).Text;
                string link = (editedItem["Link"].FindControl("txbLink") as RadTextBox).Text;
                RadAsyncUpload radAsyncUpload = editedItem["Upload"].FindControl("AsyncUpload1") as RadAsyncUpload;

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                if (radAsyncUpload.UploadedFiles.Count > 0)
                {

                    foreach (UploadedFile f in radAsyncUpload.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + description + "-" + ID + f.GetExtension());
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.UpdateCoachLinks(ID, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + description + "-" + ID + f.GetExtension(), CIMNumber, f.GetNameWithoutExtension(), description);
                    }
                }
                else
                {
                    DataAccess ws = new DataAccess();
                    ws.UpdateCoachLinks(ID, link, CIMNumber, DBNull.Value.ToString(), description);
    
                }

                string ModalLabel = "Successfully updated data.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);  
  
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadGrid1_UpdateCommand " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);  
  
            }

        }

        protected void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetEditMode", "isEditMode = true;", true);
            }
        }
        protected void AsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            if ((totalBytes < MaxTotalBytes) && (e.File.ContentLength < MaxTotalBytes))
            {
                e.IsValid = true;
                totalBytes += e.File.ContentLength;
                IsRadAsyncValid = true;
            }
            else
            {
                e.IsValid = false;
                IsRadAsyncValid = false;
            }
        }
    }
}