﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using System.Collections;
using System.Globalization;
using System.IO;
using Telerik.Web.UI;


namespace CoachV2
{
    public partial class MassCoachingSignOff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] != null)
                {

                    pn_coacheedetails.Visible = true;
                    //Decrypt
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    int CoachingTicket = Convert.ToInt32(decrypt);
                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    GetMassCoachingDetails(CoachingTicket);
                    loadcoacheedetails(CoachingTicket);
                    LoadDocumentations();


                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

        }


        public void GetMassCoachingDetails(int CoachingTicket)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetMassCoachingDetails(CoachingTicket);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RadReviewID.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                    LblCimNumber.Text = ds.Tables[0].Rows[0]["CoacheeID"].ToString();
                    LblFullName.Text = ds.Tables[0].Rows[0]["CoacheeName"].ToString();
                    RadCoachingDate.Text = ds.Tables[0].Rows[0]["ReviewDate"].ToString();
                    LblSessionType.Text = ds.Tables[0].Rows[0]["SessionName"].ToString();
                    LblSessionTopic.Text = ds.Tables[0].Rows[0]["TopicName"].ToString();
                    RadAgenda.Text = ds.Tables[0].Rows[0]["Agenda"].ToString();
                    RadDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                    RadAgenda.ReadOnly = true;
                    RadDescription.ReadOnly = true;



                    
                    Label3.Text = "My Reviews";
                    Label1.Text = " > Mass Reviews " ;
                    Label2.Text = " > " + ds.Tables[0].Rows[0]["SessionName"].ToString();

                    if ((DBNull.Value.Equals(ds.Tables[0].Rows[0]["CoacheeSignedDate"])))
                    {
                        if (Convert.ToInt32(LblCimNumber.Text) == CIMNumber)
                        {
                            if (IsPH(CIMNumber) == true)
                            {
                                RadSSN.Visible = true;
                                RadCoacheeCIM.Text = Convert.ToString(CIMNumber);
                                RadCoacheeCIM.Enabled = false;
                                RadCoacheeSignOff.Visible = true;
                            }
                            else
                            {
                                RadSSN.Visible = false;
                                RadCoacheeCIM.Text = "";
                                RadCoacheeCIM.Enabled = true;
                                RadCoacheeSignOff.Visible = true;
                            }

                        }
                        else
                        {
                            RadSSN.Visible = false;
                            RadCoacheeCIM.Visible = false;
                            RadCoacheeSignOff.Visible = false;
                        }
                    }
                    else 
                    {
                        RadSSN.Visible = false;
                        RadCoacheeCIM.Visible = false;
                        RadCoacheeSignOff.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);


                string ModalLabel = "GetMassCoachingDetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
       


            }
        
        }
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                if (IsPH(CIMNumber) == true)
                {
                    if (RadSSN.Text != "")
                    {
                        if (CheckSSN() == true)
                        {

                            DataAccess ws = new DataAccess();
                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                            DataHelper.SetTicketStatus(7, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                            RadCoacheeSignOff.Visible = false;
                            //string myStringVariable = "Coachee has signed off from this review.";
                            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            string ModalLabel = "Coachee has signed off from this review.";
                            //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                            string ModalHeader = "Success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                            DisableElements();
                            RadSSN.Enabled = false;

                            //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                            loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));
                        }
                        else
                        {
                            //string myStringVariable = "Incorrect last 3 digits of SSN.";
                            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            string ModalLabel = "Incorrect last 3 digits of SSN.";
                            string ModalHeader = "Error Message";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
       

                            //DisableElements();

                        }
                    }


                    else
                    {
                        //string myStringVariable = "SSN is a required field.";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        string ModalLabel = "SSN is a required field.";
                        //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //DisableElements();
                    }
                }
                else
                {
                    DataAccess ws = new DataAccess();
                    ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                    DataHelper.SetTicketStatus(7, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                    RadCoacheeSignOff.Visible = false;
                    //string myStringVariable = "Coachee has signed off from this review.";
                    //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                    string ModalLabel = "Coachee has signed off from this review.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    DisableElements();
                    RadSSN.Enabled = false;

                    //Added to change status after sign off - UAT result (janelle.velasquez 01242019)
                    loadcoacheedetails(Convert.ToInt32(RadReviewID.Text));

                    //if (RadCoacheeCIM.Text != "" && RadCoacheeCIM.Visible == true)
                    //{
                    //    if (Convert.ToInt32(RadCoacheeCIM.Text) == Convert.ToInt32(CIMNumber))
                    //    {

                    //        DataAccess ws = new DataAccess();
                    //        ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                    //        DataHelper.SetTicketStatus(7, Convert.ToInt32(RadReviewID.Text)); // update status to coachersigned off 
                    //        RadCoacheeSignOff.Visible = false;
                    //        //string myStringVariable = "Coachee has signed off from this review.";
                    //        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                    //        string ModalLabel = "Coachee has signed off from this review.";
                    //        string ModalHeader = "Error Message";
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    //        DisableElements();
                    //        RadSSN.Enabled = false;

                    //    }
                    //    else
                    //    {
                    //        //string myStringVariable = "Coachee CIM is a required field.";
                    //        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                    //        string ModalLabel = "Incorrect CIM Number.";
                    //        //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    //        string ModalHeader = "Error Message";
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //        //DisableElements();
                    //    }
                    //}
                    //else
                    //{
                    //    string ModalLabel = "Coachee CIM is a required field.";
                    //    string ModalHeader = "Error Message";
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                
                    //}
                }
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                string ModalLabel = "RadCoacheeSignOff_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
       

            }
        }

        protected void loadcoacheedetails(int reviewid)
        {

            DataSet ds3 = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
     
            string emp_email = ds3.Tables[0].Rows[0]["Email"].ToString();
            string cim_num = ds3.Tables[0].Rows[0][0].ToString();
            int intcim = Convert.ToInt32(cim_num);

            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

            int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
            DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
            DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
            DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));

            DataSet ds3x = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review.Tables[0].Rows[0]["createdby"]));

            string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString() + "/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString() + ".jpg"));
            string ImgDefault;

            int coacheesigned, coachersigned;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = 1;
            }
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = 1;
            }

            string coachstatus;

            string coachersigneddate = ds_get_review.Tables[0].Rows[0]["CoacherSigneddate"].ToString();
            string coacheesigneddate = ds_get_review.Tables[0].Rows[0]["CoacheeSigneddate"].ToString();
            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
            string rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

            if (rolevalue == "QA")
                saprolefordashboard = "QA";
            else if (rolevalue == "HR")
                saprolefordashboard = "HR";
            else
                saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

            string retlabels = DataHelper.ReturnLabels(saprolefordashboard);
            string retrole = "";
            if (saprolefordashboard == "TL")
            {
                retrole = "TL";
            }
            else if (saprolefordashboard == "OM")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "Dir")
            {
                retrole = "OM";
            }
            else if (saprolefordashboard == "QA")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));
                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {

                    retrole = "";
                }
            }
            else if (saprolefordashboard == "HR")
            {


                string saprole1 = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                if ((saprole1 == "OM") || (saprole1 == "Dir"))
                {
                    retrole = "OM";
                }
                else
                {
                    retrole = "";
                }
            }
            else
            {
                retrole = "";
            }

            //if ((coacheesigned == 1) && (coachersigned == 1) && (retrole == "OM"))
            //{

            //    btn_Audit1.Visible = true;
            //}

            // fixed audit button visibility (francis.valera/07192018)
            if ((int)ds_get_review.Tables[0].Rows[0]["createdby"] == intcim ||
                (int)ds_get_review.Tables[0].Rows[0]["coacheeid"] == intcim || 
                (int)ds_get_review.Tables[0].Rows[0]["reviewtypeid"] != 1)
            {
                btn_Audit1.Visible = false;
            }
            else
            {
                if (retrole == "OM" || retrole == "Dir")
                {
                    btn_Audit1.Visible = true;
                }
                else
                {
                    btn_Audit1.Visible = false;
                }
            }

            if (host.Substring(host.Length - 1, 1) != @"/")
            {
                host += @"\";
            }
            if (!File.Exists(directoryPath.Remove(directoryPath.Length - 1)))
            {
                ImgDefault = host + "Content/images/no-photo.jpg";
                //sdf
            }
            else
            {

                ImgDefault = host + "Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();

            }

            string coachersigneddate1, coacheesigneddate1;
            if (ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"] is DBNull)
            {
                coacheesigneddate1 = "";
            }
            else
            {
                coacheesigneddate1 = ds_get_review.Tables[0].Rows[0]["coacheesigneddate1"].ToString();
            }

            if (ds_get_review.Tables[0].Rows[0]["coachersigneddate1"] is DBNull)
            {
                coachersigneddate1 = "";
            }
            else
            {
                coachersigneddate1 = ds_get_review.Tables[0].Rows[0]["coachersigneddate1"].ToString();
            }

            if (coacheesigned == 1) //if coacheesigned off
            {
                if (coachersigned == 1) // if coachersigned off 
                {
                    lblstatus.Text = "*Coach: " + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["First_Name"].ToString()) + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["Last_Name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                     " Coach Sign off date : " + coachersigneddate1 + " Coachee Sign off date : " + coacheesigneddate1;
                }
                else if (coachersigned == 0) //if no coacher sign off
                {
                    lblstatus.Text = "*Needs sign off from Coach :" + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["First_Name"].ToString()) + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["Last_Name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                     " Coachee Sign off date : " + coacheesigneddate1;
                }

            }
            else if (coachersigned == 1) //if coachersigned off
            {
                if (coacheesigned == 1) //   if no coachee sign off
                {
                    lblstatus.Text = "*Coach: " + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["First_Name"].ToString()) + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["Last_Name"].ToString()) + " and Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + " already signed off." +
                      " Coach Sign off date : " + coachersigneddate1 + " Coachee Sign off date : " + coacheesigneddate1;
                }
                else if (coacheesigned == 0) //   if no coachee sign off
                {
                    lblstatus.Text = "*Needs sign off from Coachee : " + " " + Convert.ToString(ds_get_review.Tables[0].Rows[0]["Name"].ToString()) + ". " + "Coach: " + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["First_Name"].ToString()) + " " + Convert.ToString(ds3x.Tables[0].Rows[0]["Last_Name"].ToString()) + " already signed off." +
                       " Coach Sign off date : " + coachersigneddate1;
                }
            }
          
            

            ProfImage.ImageUrl = ImgDefault;
            LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
            LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
            LblSite.Text = ds.Tables[0].Rows[0]["sitename"].ToString();
            LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
            LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
            LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
            //LblCIMNo.Text = Convert.ToString(coacheeid); //cim_num;
            LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();
            //LblRegion.Text = "English Speaking Region";
            LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
            LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";

           
        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text),2);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.DataBind();
            }
            catch (Exception ex)
            {
                //string myStringVariable = ex.ToString();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);

                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
       

            }

        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        public bool CheckSSN()
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DisableElements()
        {
            RadSSN.Visible = false;
            RadCoacheeCIM.Visible = false;
        }

        protected void btn_Audit1_Click(object sender, EventArgs e)
        {
            btn_Audit1.Attributes.Add("onclick", " CloseWindow();");

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            string coachid = "";
            int reviewid;
            coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(DataHelper.Decrypt(coachid));
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);
            //string val1 = Convert.ToString(reviewid);

            string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

            Response.Redirect("~/AddTriadCoaching.aspx?CoachingTicket=" + coachid + "&ReviewType=" + val2);

        }
    }
}