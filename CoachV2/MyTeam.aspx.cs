﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CoachV2
{
    public partial class MyTeam : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["tab"] == null || Request.QueryString["CIM"] == null)
                    Response.Redirect("~/Default.aspx");

                MyTeamPerfUserCtrl1.Visible = false;
                MyTeamProfileUserCtrl1.Visible = false;
                MyTeamLogsUserCtrl1.Visible = false;

                MyTeamPerfUserCtrl1.Visible = Request.QueryString["tab"] == "perf" && Request.QueryString["CIM"] != null ? true : false;
                MyTeamProfileUserCtrl1.Visible = Request.QueryString["tab"] == "profile" && Request.QueryString["CIM"] != null ? true : false;
                MyTeamLogsUserCtrl1.Visible = Request.QueryString["tab"] == "logs" && Request.QueryString["CIM"] != null ? true : false;
            }
        }
    }
}