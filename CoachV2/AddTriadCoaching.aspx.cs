﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;

namespace CoachV2
{
    public partial class AddTriadCoaching : System.Web.UI.Page
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string review_date = HttpContext.Current.Timestamp.ToShortDateString();
            //dp_reviewdate.SelectedDate = Convert.ToDateTime(review_date);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num); 


            var myreviewtitle = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("myreviewtitle");
            myreviewtitle.Visible = false;
            var myreviewtitle2 = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("Label1");
            myreviewtitle2.Visible = false;
            var myreviewtitle3 = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("Label2");
            myreviewtitle3.Visible = false;

            var hyp_qa = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("HyperLinkQA");
            hyp_qa.Visible = false;
            var hyp_newrev = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("HyperLinkRev");
            hyp_newrev.Visible = false;
            var hyp_newmass = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("HyperLink3");
            hyp_newmass.Visible = false;
            var hyp_newremote = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("HyperLink4");
            hyp_newremote.Visible = false;
            var search = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("HyperLink5");
            search.Visible = false; 
            var searchpanel = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("SearchPane");
            searchpanel.Visible = false;
            var searchtextsmol = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("TxtSearch");
            searchtextsmol.Visible = false;
            var searchbtnsmol = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("BtnSearchQuery");
            searchbtnsmol.Visible = false; 

            var searchp = this.Master.FindControl("ContentPlaceHolderMain").FindControl("SearchAdvUserCtrl1").FindControl("DashboardMyReviewsUserControl1").FindControl("Search");
            searchp.Visible = true;

            if (btn_Save.Visible == true)
            {
                this.Form.DefaultButton = btn_Save.UniqueID;
            }

            if (!Page.IsPostBack)
            {

                if ((Page.Request.QueryString["CoachingTicket"] != null))
                {
                    //DataSet ds_reviewtype = DataHelper.Get_ReviewID(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));
                    //decryption
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    DataSet ds_reviewtype = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
                    if (ds_reviewtype.Tables[0].Rows.Count > 0)
                    {
                        int reviewtype = Convert.ToInt32(ds_reviewtype.Tables[0].Rows[0]["Reviewtypeid"]);
                         
                        pn_reviews.Visible = false;  
                        string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(CIMNumber));
                        string retlabels = DataHelper.ReturnLabels(saprolefordashboard);

                        if ((saprolefordashboard == "OM") || (saprolefordashboard == "Dir"))
                        {
                            pn_coacheeview.Visible = true;
                            pn_OMView.Visible = true;
                            pn_coacherfeedback.Visible = true;
                            pn_signoff.Visible = true;

                            // added francis.valera/08022018
                            int coacheesigned, coachersigned;
                            if (ds_reviewtype.Tables[0].Rows[0]["coachersigned"] is DBNull)
                            {
                                coachersigned = 0;
                            }
                            else
                            {
                                coachersigned = 1;
                            }
                            if (ds_reviewtype.Tables[0].Rows[0]["coacheesigned"] is DBNull)
                            {
                                coacheesigned = 0;
                            }
                            else
                            {
                                coacheesigned = 1;
                            }
                            if (reviewtype == 1 && coacheesigned == 1 && coachersigned == 1)
                            {
                                btn_Audit.Visible = true;
                            }
                            //btn_Audit.Visible = true;
                            
                            //decrypt
                            //DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));
                            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
                            if ((ds_get_review.Tables[0].Rows[0]["coacheesigned"].Equals(true)) )//&& (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true)) && (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull))
                            {
                                RadSSN.Visible = false;
                                RadSSN.Enabled = false;
                                RadCoacheeCIM.Visible = false;
                                RadCoacheeCIM.Enabled = false;
                                RadCoacheeSignOff.Visible = false;
                                RadCoacheeSignOff.Enabled = false;
                            }
                            if (ds_get_review.Tables[0].Rows[0]["coachersigned"].Equals(true))
                            {
                                RadCoacherSignOff.Visible = false;
                                RadCoacherSignOff.Enabled = false;
                            }
                            //RadSSN.Visible = true;
                            //RadSSN.Enabled = false;
                            //RadCoacheeCIM.Visible = true;
                            //RadCoacheeCIM.Enabled = false;
                            //RadCoacheeSignOff.Visible = true;
                            //RadCoacheeSignOff.Enabled = false;
                            txtCoacherFeedback.Enabled = false;
                            //decrypt
                            //int review_id = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);

                            int review_id = Convert.ToInt32(decrypt);
                            get_review(review_id);

                            LoadPreviousCoachingNotes(review_id);
                            LoadDocumentations();
                            GetCommitment(review_id);
                        }
                        //else if (saprolefordashboard == "TL")
                        //{

                        //    pn_coacheeview.Visible = true;
                        //    pn_OMView.Visible = true;
                        //    pn_coacherfeedback.Visible = true;
                        //    pn_signoff.Visible = true;
                        //    RadCoacherSignOff.Visible = true;
                        //    //grd_coachingevaluation.Enabled = false;
                        //    //grd_notesfromcoacher.Enabled = false;
                        //    //grd_Commitment.Enabled = false;
                        //    RadSSN.Visible = true;
                        //    RadSSN.Enabled = false;
                        //    RadCoacheeCIM.Visible = true;
                        //    RadCoacheeCIM.Enabled = false;
                        //    RadCoacheeSignOff.Visible = true;
                        //    RadCoacheeSignOff.Enabled = false;
                        //    txtCoacherFeedback.Enabled = true;
                        //    int review_id = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                        //    get_review(review_id);
                        //    LoadDocumentations();

                        //}
                        else
                        {

                            pn_coacheeview.Visible = true;
                            pn_OMView.Visible = true;
                            pn_coacherfeedback.Visible = true;
                            pn_signoff.Visible = true;
                            RadCoacherSignOff.Visible = true;
                            pn_signoff.Visible = true;
                            RadCoacherSignOff.Visible = true;
                            RadSSN.Visible = true;
                            RadSSN.Enabled = true;
                            txtCoacherFeedback.Enabled = false;
                            int review_id = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                            get_review(review_id);
                            btn_Audit.Visible = false;
                            btn_Save.Visible = false;
                            LoadPreviousCoachingNotes(review_id);
                            LoadDocumentations();
                            grd_coachingevaluation.Enabled = false;
                            grd_notesfromcoacher.Enabled = false;
                            //grd_Commitment.Enabled = false;
                            DisableCommitmentTable(false);
                        }

                        //else
                        //{
                        //    Response.Redirect("~/AddTriadCoaching.aspx");
                        //}
                        //}
                        //else
                        //{
                        //    Response.Redirect("~/AddTriadCoaching.aspx");
                        //}
                    }

                }
            }
          //  grd_coachingevaluation_ItemCreated();
        }//

        protected void grd_tlreviews_Itemdatabound(object sender, GridItemEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["CoachingTicket"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["NameField"].Controls[0];

                string val1 = hLink.Text;
                int reviewid = Convert.ToInt32(val1);
                DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                string val2 = ds_get_review.Tables[0].Rows[0]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;


                hLinkname.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;

            }
        }
        
        protected void get_review(int revid)
        {
            SidebarUserControl.Visible = true;


            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber_userlogged = Convert.ToInt32(cim_num);

            string coachid = "";
            int reviewid;

            //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            coachid = decrypt;
            //coachid = Page.Request.QueryString["Coachingticket"];
            reviewid = Convert.ToInt32(coachid);
            DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

            int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
            DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
            DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
            DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));


            RadCoacheeCIM.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["CoacheeId"].ToString());
            DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadTextBox1.Text));
            RadGrid1.DataSource = ds_KPILIST;
            RadGrid1.DataBind(); //performance 

            DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadTextBox1.Text));
            RadGridPRR.DataSource = ds_coachingnotes;
            //RadGridPRR.DataBind();

            DataSet ds_triadnotes = DataHelper.GetNotesforTriad(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["ID"]));
            grd_notesfromcoacher.DataSource = ds_triadnotes;
            //grd_notesfromcoacher.DataBind();

            DataSet ds_scoring = DataHelper.GetCommentsforTriad(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["ID"]));
            //grd_Commitment.DataSource = ds_scoring;
            //grd_Commitment.DataBind();

            if (ds_scoring.Tables[0].Rows.Count > 0)
            {

                if (ds_scoring.Tables[0].Rows[0]["CoacherFeedback"] is DBNull)
                {
                    txtCoacherFeedback.Text = "NONE"; // added to eliminate placeholder text when feedback is not supplied (francis.valera/071120181130)
                }
                else
                {
                    txtCoacherFeedback.Text = Convert.ToString(ds_scoring.Tables[0].Rows[0]["CoacherFeedback"]);
                }
            }

            //grd_coachingevaluation.DataSource = ds_coachingnotes;
            DataSet ds_coachingevaluation = DataHelper.GetScoreforTriad(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["ID"]));
            grd_coachingevaluation.DataSource = ds_coachingevaluation;
            if (ds_coachingevaluation.Tables[0].Rows.Count > 0)
            {
                txttotalscore.Text = Convert.ToString(ds_coachingevaluation.Tables[0].Rows[4]["totalscore"]);
            }




            DataSet ds_docs = DataHelper.GetDocumentation(reviewid);
            grd_Documentation.DataSource = ds_docs;

            int coachersigned, coacheesigned = 0;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coachersigned"]);
            }
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
            }

            int reviewsaved, reviewaudited = 0;

            if (ds_get_review.Tables[0].Rows[0]["saved"] is DBNull)
            {
                reviewsaved = 0;
            }
            else
            {
                reviewsaved = 1;
            }

            if (ds_get_review.Tables[0].Rows[0]["audit"] is DBNull)
            {
                reviewaudited = 0;
            }
            else
            {
                reviewaudited = 1;
            }

            if (reviewaudited == 1)
            {
                //grd_Commitment.Enabled = false;
                DisableCommitmentTable(false);
                if (reviewsaved != 1) //audited and not saved
                {
                    btn_Save.Visible = true;
                    btn_Audit.Visible = false;
                    pn_save.Visible = true;
                    //RadSSN.Visible = true;
                    //RadSSN.Enabled = true;
                    //RadCoacheeCIM.Visible = true;
                    //RadCoacheeCIM.Enabled = false;
                    //RadCoacheeSignOff.Visible = true;
                    //RadCoacheeSignOff.Enabled = true;
                    //RadCoacherSignOff.Visible = false;


                }
                else  //audited and  saved
                {
                    btn_Save.Visible = false;
                    btn_Audit.Visible = false;
                    pn_save.Visible = true;
                    //grd_Commitment.Enabled = false;
                    DisableCommitmentTable(false);
                    grd_coachingevaluation.Enabled = false;
                    grd_coachingevaluation.Enabled = false;
                    RadCoacheeSignOff.Enabled = true;


                    


                    if (coacheesigned == 0)
                    {

                        pn_save.Visible = true;
                        grd_coachingevaluation.Enabled = false;
                        grd_notesfromcoacher.Enabled = false;

                        //RadSSN.Visible = true;
                        //RadSSN.Enabled = true;
                        //RadCoacheeCIM.Visible = true;
                        RadCoacheeCIM.Enabled = false;
                        //RadCoacheeSignOff.Visible = true;
                        //RadCoacheeSignOff.Enabled = true;
                        RadCoacherSignOff.Visible = false;



                    }
                    else if (coacheesigned == 1) //if not yet signedoff by coacher
                    {
                        RadCoachCim.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["supervisorid"]);
                        //RadCoachCim.Visible = true;
                        //RadCoachSSN.Visible = true;
                        //RadCoachSSN.Enabled = true;
                        //RadCoacherSignOff.Visible = true;
                        //RadCoacherSignOff.Enabled = true;


                        pn_save.Visible = true;
                        grd_coachingevaluation.Enabled = false;
                        grd_notesfromcoacher.Enabled = false;
                        txtCoacherFeedback.Enabled = true;
                        RadSSN.Visible = false;
                        RadSSN.Enabled = false;
                        RadCoacheeCIM.Visible = false;
                        RadCoacheeCIM.Enabled = false;
                        RadCoacheeSignOff.Visible = false;
                        RadCoacheeSignOff.Enabled = false;

                        if (coachersigned == 1)
                        {

                            RadCoacherSignOff.Enabled = false;
                            RadCoachSSN.Enabled = false;
                            RadCoachCim.Visible = false;
                            RadCoachSSN.Visible = false;
                            RadCoacherSignOff.Visible = false;
                            txtCoacherFeedback.Enabled = false;
                        }


                    }
                }



            }

            else if (reviewaudited == 0) //not audited and not saved
            {

                btn_Save.Visible = false;
                //btn_Audit.Visible = true;
                //added francis.valera/08022018
                if (coacheesigned == 1 && coachersigned == 1)
                {
                    btn_Audit.Visible = true;
                }


                //RadSSN.Visible = true;
                //RadSSN.Enabled = true;
                //RadCoacheeCIM.Visible = true;
                //RadCoacheeCIM.Enabled = false;
                //RadCoacheeSignOff.Visible = true;
                //RadCoacheeSignOff.Enabled = true;
                //RadCoacherSignOff.Visible = false;
                //grd_Commitment.Enabled = true;
                DisableCommitmentTable(true);
                //override above method
                //kean
                RadTextBox3.Enabled = RadTextBox5.Enabled = RadTextBox7.Enabled = RadTextBox9.Enabled = false;  
            }



            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
            string ImgDefault;
            string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
            FakeURLID.Value = URL;

            if (!Directory.Exists(directoryPath))
            {
                ImgDefault = URL + "/Content/images/no-photo.jpg";
            }

            else
            {

                ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
            }

            RadTextBox1.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["ID"]);

            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                RadDatePicker1.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }

            ProfImage.ImageUrl = ImgDefault;
            LblFullName.Text = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString();
            LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
            lblSite.Text = ds.Tables[0].Rows[0]["Sitename"].ToString();
            LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
            LblCountry.Text = ds.Tables[0].Rows[0]["CountryName"].ToString();
            LblMySupervisor.Text = ds.Tables[0].Rows[0]["ReportsTo"].ToString();
            LblRegion.Text = ds.Tables[0].Rows[0]["RegionName"].ToString();

            LblClient.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Client"].ToString() : "";
            LblCampaign.Text = ds2.Tables[0].Rows.Count > 0 ? ds2.Tables[0].Rows[0]["Campaign"].ToString() : "";
            if (ds_get_review.Tables[0].Rows[0]["Topicname"] is DBNull) { }
            else
            {
                SessionTopic.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Topicname"]);
            }

            if (ds_get_review.Tables[0].Rows[0]["SessionName"] is DBNull) { }
            else
            {
                SessionType.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["SessionName"]);
            }

            RadTextBox1.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Id"].ToString());


            if (ds_get_review.Tables[0].Rows[0]["FollowDate"] is DBNull) { }
            else
            {
                RadDatePicker1.Text = ds_get_review.Tables[0].Rows[0]["CreatedOn"].ToString();
            }

            if (ds_get_review.Tables[0].Rows[0]["Description"] is DBNull) { }
            else
            {
                RadDescription.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Description"].ToString());
            }

            if (ds_get_review.Tables[0].Rows[0]["Strengths"] is DBNull) { }
            else
            {
                TxtStrenghts.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Strengths"].ToString());
            }

            if (ds_get_review.Tables[0].Rows[0]["Opportunity"] is DBNull) { }
            else { txtOpportunities.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Opportunity"].ToString()); }

            if (ds_get_review.Tables[0].Rows[0]["Commitment"] is DBNull) { }
            else
            {
                txtCommitment.Text = Convert.ToString(ds_get_review.Tables[0].Rows[0]["Commitment"].ToString());
            }


        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            int reviewid = Convert.ToInt32(decrypt);
            //int reviewid = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
            //save_notes(CIMNumber, reviewid);

            // removed unused variables replaced by dropdown score validation (francis.valera/09132018)
            //int c_cmpntid, scoringval = 0;int totalval = 0;
            //string prepstepval, properstepval, poststepval;
            //int e1 = 0;int e2 = 0;int e3 = 0;int e4 = 0;int e5 = 0;

            int intnoscore = 0; string strnoscore = "<p><ul>";
            foreach (GridDataItem itm in grd_coachingevaluation.MasterTableView.Items)
            {
                RadComboBox _drpscore = (RadComboBox)itm.FindControl("drpScore");
                if (_drpscore.Text == "")
                {
                    intnoscore = intnoscore + 1;
                    RadTextBox _radtxtcomponent = (RadTextBox)itm.FindControl("txt_ccomponent");
                    strnoscore = strnoscore + "<li>" + _radtxtcomponent.Text + "</li>";
                }
            }
            if (intnoscore > 0)
            {
                string ModalLabel = "<p><b>Please provide scores for:</b></p>" + strnoscore + "</ul></p>";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModalHtml('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
            else //((e1 == 0) && (e2 == 0) && (e3 == 0) && (e4 == 0) && (e5 == 0))
            {
                save_notes(CIMNumber, reviewid);
                save_coachingevaluation();
                GetCommitment(reviewid);
                DataSet ds_saved_review = DataHelper.SaveReviewforAudit(CIMNumber, reviewid);
                btn_Audit.Visible = false;
                btn_Save.Visible = false;
                RadCoacheeSignOff.Enabled = true;
                DataSet ds_scoring = DataHelper.GetCommentsforTriad(Convert.ToInt32(reviewid));
                //grd_Commitment.DataSource = ds_scoring;
                if (ds_scoring.Tables[0].Rows.Count > 0)
                {

                    if (ds_scoring.Tables[0].Rows[0]["CoacherFeedback"] is DBNull)
                    {
                        txtCoacherFeedback.Text = "NONE"; // added to eliminate placeholder text when feedback is not supplied (francis.valera/071120181130)
                    }
                    else
                    {
                        txtCoacherFeedback.Text = Convert.ToString(ds_scoring.Tables[0].Rows[0]["CoacherFeedback"]);
                    }
                }

                DataSet ds_coachingevaluation = DataHelper.GetScoreforTriad(Convert.ToInt32(reviewid));
                grd_coachingevaluation.DataSource = ds_coachingevaluation;
                if (ds_coachingevaluation.Tables[0].Rows.Count > 0)
                {
                   txttotalscore.Text = Convert.ToString(ds_coachingevaluation.Tables[0].Rows[4]["totalscore"]);
                }
                DataSet ds_triadnotes = DataHelper.GetNotesforTriad(Convert.ToInt32(reviewid));
                grd_notesfromcoacher.DataSource = ds_triadnotes;
                //string myStringVariable = "Coaching Ticket : " + reviewid + " Saved!";
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                string ModalLabel = "Coaching Ticket : " + reviewid + " Saved!";
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                get_review(reviewid);
                pn_save.Visible = true;
                grd_coachingevaluation.Visible = true;
                grd_coachingevaluation.Enabled = false;
                grd_notesfromcoacher.Visible = true;
                grd_notesfromcoacher.Enabled = false;
            }
        }

        protected void btn_Audit_Click(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num); 
            //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            int reviewid = Convert.ToInt32(decrypt);
            //int reviewid = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
            DataSet ds_saved_review = DataHelper.SetReviewforAudit(CIMNumber, reviewid);


            //add / edit commitment only
            save_commitment(CIMNumber, reviewid);
            DataSet ds_scoring = DataHelper.GetCommentsforTriad(Convert.ToInt32(reviewid));
            //grd_Commitment.DataSource = ds_scoring;
            GetCommitment(reviewid);
            if (ds_scoring.Tables[0].Rows.Count > 0)
            {
                if (ds_scoring.Tables[0].Rows[0]["CoacherFeedback"] is DBNull)
                {
                    txtCoacherFeedback.Text = "NONE"; // added to eliminate placeholder text when feedback is not supplied (francis.valera/071120181130)
                }
                else
                {
                    txtCoacherFeedback.Text = Convert.ToString(ds_scoring.Tables[0].Rows[0]["CoacherFeedback"]);
                }
            }

            //grd_Commitment.DataBind();
            DataSet ds_triadnotes = DataHelper.GetNotesforTriad(Convert.ToInt32(reviewid));
            grd_notesfromcoacher.DataSource = ds_triadnotes;
            DataSet ds_coachingevaluation = DataHelper.GetScoreforTriad(Convert.ToInt32(reviewid));
            grd_coachingevaluation.DataSource = ds_coachingevaluation;
            if (ds_coachingevaluation.Tables[0].Rows.Count > 0)
            {
              //  txttotalscore.Text = Convert.ToString(ds_coachingevaluation.Tables[0].Rows[4]["totalscore"]);
            }
            btn_Audit.Visible = false;
            btn_Save.Visible = true;
            DataHelper.SetTicketStatus(3, reviewid);
//            string myStringVariable = "Coaching Ticket : " + reviewid + " For auditing!";
//            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            string ModalLabel = "Coaching Ticket : " + reviewid + " For auditing.";
            string ModalHeader = "Success Message";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                     
            Response.AppendHeader("Refresh", "0.1");

            get_review(reviewid);
            pn_save.Visible = true;
        }
        
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            //DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));
            //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
            int coacheesigned;
            if (ds_get_review.Tables[0].Rows[0]["coacheesigned"] is DBNull)
            {
                coacheesigned = 0;
            }
            else
            {
                coacheesigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheesigned"]);
            }
            if (coacheesigned != 1)
            {

                try
                {

                    if (RadSSN.Text != "")
                    {
                        if (CheckSSN() == true)
                        {

                            CheckSSN();
                            //decryption
                            int ReviewID = Convert.ToInt32(decrypt);
                            //int ReviewID = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                            DataHelper.Get_CoacheeSignOff(ReviewID);
                            RadCoacheeSignOff.Enabled = false;
                            string myStringVariable = "Coachee has signed off from this review.";
                            get_review(ReviewID);
                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            RadCoacheeSignOff.Enabled = false;
                            RadSSN.Enabled = false;
                            //lblSignedoffDateCoachee.Visible = true;


                        }
                        else
                        {
                            string myStringVariable = "Incorrect last 3 digits of SSN.";
                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);


                        }
                    }
                    else
                    {
                        string myStringVariable = "SSN is a required field.";
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                    }
                }
                catch (Exception ex)
                {
                    string myStringVariable = ex.ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                }
            }
            else
            {
                string myStringVariable = "Coaching ticket already signed off by Coachee!";
                //decryption                 
                get_review(Convert.ToInt32(decrypt));  
                //get_review(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadCoacheeSignOff.Enabled = false;

            }
        }

        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        { //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));                  
            //DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
            int coachersigned, coacheesigned;
            if (ds_get_review.Tables[0].Rows[0]["coachersigned"] is DBNull)
            {
                coachersigned = 0;
            }
            else
            {
                coachersigned = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coachersigned"]);
            }

            if (coachersigned != 1)
            {

                try
                {
                    if (RadCoachSSN.Text != "")
                    {
                        if (CheckSSN_Sup() == true)
                        {

                            CheckSSN_Sup();
                            //decrypt
                            int ReviewID = Convert.ToInt32(decrypt);
                            //int ReviewID = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                            DataHelper.Get_CoacherSignOff(ReviewID);
                            RadCoacheeSignOff.Enabled = false;
                            DataHelper.Get_CoacherFeedback(Convert.ToString(txtCoacherFeedback.Text), ReviewID);
                            string myStringVariable = "Coach has signed off from this review.";
                            get_review(ReviewID);
                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                            RadCoacherSignOff.Enabled = false;
                            RadCoachSSN.Enabled = false;
                            RadCoachCim.Visible = false;
                            RadCoachSSN.Visible = false;
                            RadCoacherSignOff.Visible = false;
                            txtCoacherFeedback.Enabled = false;
                            //lblSignedoffDateCoachee.Visible = true;


                        }
                        else
                        {
                            string myStringVariable = "Incorrect last 3 digits of SSN.";
                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);


                        }
                    }
                    else
                    {
                        string myStringVariable = "SSN is a required field.";
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                    }
                }
                catch (Exception ex)
                {
                    string myStringVariable = ex.ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                }
            }
            else
            {
                string myStringVariable = "Coaching ticket already signed off by Coacher!";
                get_review(Convert.ToInt32(decrypt));
                //get_review(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));

                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            }
        }

        public bool CheckSSN()
        {    //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
            int CIMNumber = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheeid"]);//RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["coacheeid"]), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckSSN_Sup()
        {   //decryption
            string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
            DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(decrypt));
            //DataSet ds_get_review = DataHelper.Get_ReviewID(Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]));

            int CIMNumber = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["supervisorid"]);//RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["supervisorid"]), Convert.ToInt32(RadCoachSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void save_commitment(int cimno, int reviewid)
        {
            try
            {
                //RadTextBox Goal , Reality, Options, Way;
                //string Goal, Reality, Options, Way;
                //foreach (GridDataItem itm in grd_Commitment.MasterTableView.Items)
                //{
                //    reviewid = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                //    Goal = (itm.FindControl("txtGoal") as RadTextBox).Text; //(RadTextBox)itm.FindControl("txtGoal"); //itm["Goal_Text"].Text; //
                //    Reality = (itm.FindControl("txtReality") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
                //    Options = (itm.FindControl("txtOptions") as RadTextBox).Text; //(RadTextBox)itm.FindControl("txtOptions");
                //    Way = (itm.FindControl("txtWayForward") as RadTextBox).Text;  //(RadTextBox)itm.FindControl("txtWayForward");
                //    DataHelper.InsertCommitmenthere(Convert.ToInt32(reviewid), Convert.ToString(Goal), Convert.ToString(Reality), Convert.ToString(Options), Convert.ToString(Way));
                //}
                DataHelper.InsertCommitmenthere(Convert.ToInt32(reviewid), Convert.ToString(RadTextBox3.Text), Convert.ToString(RadTextBox5.Text), Convert.ToString(RadTextBox7.Text), Convert.ToString(RadTextBox9.Text));
                                 

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected void save_notes(int cimno, int reviewid)
        {
            try
            {
                //RadTextBox Goal , Reality, Options, Way;
                string didwell, dodifferent;
                foreach (GridDataItem itm in grd_notesfromcoacher.MasterTableView.Items)
                {   //decrypt
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));                   
                    //reviewid = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
                    reviewid = Convert.ToInt32(decrypt);
                    didwell = (itm.FindControl("txt_didwell") as RadTextBox).Text; //(RadTextBox)itm.FindControl("txtGoal"); //itm["Goal_Text"].Text; //
                    dodifferent = (itm.FindControl("txt_DoDifferent") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
                    DataHelper.InsertTriadNotes(Convert.ToInt32(reviewid), Convert.ToString(didwell), Convert.ToString(dodifferent));
                }

            }
            catch (Exception ex)
            {
                string ModalLabel = ex.Message.ToString();//"Coaching Ticket : " + reviewid + " For auditing!";
                string ModalHeader = "Success Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                //string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        protected void save_coachingevaluation()
        {
            int score1, score2, score3, score4, score5 = 0;
            string prepstep1, properstep1, poststep1, prepstep2, properstep2, poststep2,
                       prepstep3, properstep3, poststep3,
                       prepstep4, properstep4, poststep4,
                       prepstep5, properstep5, poststep5 = "";
            double totalscore;

            try
            {
                //RadTextBox Goal , Reality, Options, Way;
                int reviewid, c_cmpntid, scoringval = 0;
                int totalval = 0;

                string prepstepval, properstepval, poststepval;
                foreach (GridDataItem itm in grd_coachingevaluation.MasterTableView.Items)
                {
                    int itmindx = itm.ItemIndex + 1;

                    reviewid = Convert.ToInt32(DataHelper.Decrypt(Page.Request.QueryString["CoachingTicket"]));
                    c_cmpntid = Convert.ToInt32(itmindx); // (itm.FindControl("txt_scoring") as RadTextBox).Text;  //(RadTextBox)itm.FindControl("txtGoal"); //itm["Goal_Text"].Text; //
                    // change source of component scores (dropdown) (francis.valera/09132018)
                    //scoringval = Convert.ToInt32((itm.FindControl("txt_scoring") as RadNumericTextBox).Text); // (RadTextBox)itm.FindControl("txtReality");
                    RadComboBox _drpscore = (RadComboBox)itm.FindControl("drpScore");
                    scoringval = Convert.ToInt32(_drpscore.Text);
                    prepstepval = (itm.FindControl("txt_prestep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
                    properstepval = (itm.FindControl("txt_Properstep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
                    poststepval = (itm.FindControl("txt_Poststep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");

                    totalval = scoringval + totalval;
                    totalscore = totalval;
                    if (c_cmpntid == 1)
                    {
                        if (scoringval < 21)
                        {
                            DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(scoringval), Convert.ToString(prepstepval), Convert.ToString(properstepval), Convert.ToString(poststepval), 0);
                            score1 = Convert.ToInt32(scoringval);
                            prepstep1 = Convert.ToString(prepstepval);
                            properstep1 = Convert.ToString(properstepval);
                            poststep1 = Convert.ToString(poststepval);
                            DataHelper.InsertCoachingEvaluation1(reviewid, score1, prepstep1, properstep1, poststep1);
                        }
                        //else
                        //{
                        //    string myStringVariable = "Please input score less than or equal to 20!";
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                        //}

                    }
                    else if (c_cmpntid == 2)
                    {
                        if (scoringval < 31)
                        {
                            DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(scoringval), Convert.ToString(prepstepval), Convert.ToString(properstepval), Convert.ToString(poststepval), 0);
                            score2 = Convert.ToInt32(scoringval);
                            prepstep2 = Convert.ToString(prepstepval);
                            properstep2 = Convert.ToString(properstepval);
                            poststep2 = Convert.ToString(poststepval);
                            DataHelper.InsertCoachingEvaluation2(reviewid, score2, prepstep2, properstep2, poststep2);
                        }
                        //else
                        //{
                        //    string myStringVariable = "Please input score less than or equal to30!";
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);


                    }
                    else if (c_cmpntid == 3)
                    {
                        if (scoringval < 26)
                        {
                            DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(scoringval), Convert.ToString(prepstepval), Convert.ToString(properstepval), Convert.ToString(poststepval), 0);
                            score3 = Convert.ToInt32(scoringval);
                            prepstep3 = Convert.ToString(prepstepval);
                            properstep3 = Convert.ToString(properstepval);
                            poststep3 = Convert.ToString(poststepval);
                            DataHelper.InsertCoachingEvaluation3(reviewid, score3, prepstep3, properstep3, poststep3);

                        }
                        //else
                        //{
                        //    string myStringVariable = "Please input score less than or equal to 25!";
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                        //}
                    }
                    else if (c_cmpntid == 4)
                    {
                        if (scoringval < 16)
                        {
                            DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(scoringval), Convert.ToString(prepstepval), Convert.ToString(properstepval), Convert.ToString(poststepval), 0);
                            score4 = Convert.ToInt32(scoringval);
                            prepstep4 = Convert.ToString(prepstepval);
                            properstep4 = Convert.ToString(properstepval);
                            poststep4 = Convert.ToString(poststepval);
                            DataHelper.InsertCoachingEvaluation4(reviewid, score4, prepstep4, properstep4, poststep4);

                        }
                        //else
                        //{
                        //    string myStringVariable = "Please input score less than or equal to 15!";
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

                        //}
                    }

                    else if (c_cmpntid == 5)
                    {
                        if (scoringval < 11)
                        {
                            DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(scoringval), Convert.ToString(prepstepval), Convert.ToString(properstepval), Convert.ToString(poststepval), totalval);
                            score5 = Convert.ToInt32(scoringval);
                            prepstep5 = Convert.ToString(prepstepval);
                            properstep5 = Convert.ToString(properstepval);
                            poststep5 = Convert.ToString(poststepval);
                            DataHelper.InsertCoachingEvaluation5(reviewid, score5, prepstep5, properstep5, poststep5, totalscore, 0);

                            //DataHelper.InsertCoachingEvaluation(Convert.ToInt32(reviewid), Convert.ToInt32(c_cmpntid), Convert.ToInt32(totalval), Convert.ToString(""), Convert.ToString(""), Convert.ToString(""), Convert.ToInt32(totalval));

                        }
                    }

                }


            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }
        }

        //protected void scorevalidation(object sender, EventArgs e)
        //{
        //    int reviewid, c_cmpntid, scoringval = 0;
        //    int totalval = 0;
        //    string prepstepval, properstepval, poststepval;
        //    int e1 = 0;
        //    int e2 = 0;
        //    int e3 = 0;
        //    int e4 = 0;
        //    int e5 = 0;
        //    foreach (GridDataItem itm in grd_coachingevaluation.MasterTableView.Items)
        //    {
        //        int itmindx = itm.ItemIndex + 1;
        //        reviewid = Convert.ToInt32(Page.Request.QueryString["CoachingTicket"]);
        //        c_cmpntid = Convert.ToInt32(itmindx); // (itm.FindControl("txt_scoring") as RadTextBox).Text;  //(RadTextBox)itm.FindControl("txtGoal"); //itm["Goal_Text"].Text; //
        //        scoringval = Convert.ToInt32((itm.FindControl("txt_scoring") as RadNumericTextBox).Text); // (RadTextBox)itm.FindControl("txtReality");
        //        prepstepval = (itm.FindControl("txt_prestep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
        //        properstepval = (itm.FindControl("txt_Properstep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
        //        poststepval = (itm.FindControl("txt_Poststep") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
        //        if (c_cmpntid == 1)
        //        {
        //            if ((scoringval != 0) || (scoringval != 20))//(scoringval > 21)
        //            {
        //                string myStringVariable = "Please input score less than or equal to 20!";
        //                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //                e1 = 1;
        //            }
        //        }
        //        else if (c_cmpntid == 2)
        //        {
        //            if ((scoringval != 0) || (scoringval != 30))//(scoringval > 31)
        //            {
        //                string myStringVariable = "Please input score less than or equal to 30!";
        //                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //                e2 = 1;
        //            }
        //        }
        //        else if (c_cmpntid == 3)
        //        {
        //            if ((scoringval != 0) || (scoringval != 25)) //(scoringval > 26)
        //            {
        //                string myStringVariable = "Please input score less than or equal to 25!";
        //                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //                e3 = 1;
        //            }
        //        }
        //        else if (c_cmpntid == 4)
        //        {
        //            if ((scoringval != 0) || (scoringval != 15))//(scoringval > 16) 
        //            {
        //                string myStringVariable = "Please input score less than or equal to 15!";
        //                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //                e4 = 1;
        //            }
        //        }
        //        else if (c_cmpntid == 5)
        //        {
        //            if ((scoringval != 0) || (scoringval != 10)) //(scoringval > 11)
        //            {
        //                string myStringVariable = "Please input score less than or equal to 10!";
        //                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
        //                e5 = 1;
        //            }
        //        }
        //        else
        //        {
        //        }
        //    }
        //}

        public void LoadPreviousCoachingNotes(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetIncHistory(ReviewID, 1);
                RadGridCN.DataSource = ds;
                RadGridCN.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "LoadPreviousCoachingNotes " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadGridCN_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FullName"].Controls[0];
                hLink.ToolTip = "Coaching Ticket : " + hLink.Text;
                string coachingtkt = (item.FindControl("LabelCT") as Label).Text;
                string enctxt1 = DataHelper.Encrypt(Convert.ToInt32(coachingtkt));
                //hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt1 + "&ReviewType="+ Page.Request.QueryString["ReviewType"];

                //added for url assignments for previous coaching notes (francis.valera/08162018)
                int nReviewTypeID = Convert.ToInt32((item.FindControl("LabelReviewTypeID") as Label).Text);
                string enctxt2 = DataHelper.Encrypt(nReviewTypeID);
                string formtype = (item.FindControl("LabelFormType") as Label).Text;
                int nFormType = 0;
                if ((formtype == null) || (formtype == ""))
                {
                    nFormType = 0;
                }
                else
                {
                    nFormType = Convert.ToInt32(formtype);
                }
                if (nReviewTypeID == 1)
                {
                    if (nFormType == 1)
                    {
                        hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                    }
                    else
                    {
                        hLink.NavigateUrl = "~/UpdateReviewNexidia.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                    }
                }
                else if (nReviewTypeID == 2)
                {
                    hLink.NavigateUrl = "~/UpdateQAReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 3)
                {
                    hLink.NavigateUrl = "~/MassCoachingSignOff.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 4)
                {
                    hLink.NavigateUrl = "~/AddRemoteCoaching.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 5)
                {
                    hLink.NavigateUrl = "~/AddTriadCoaching.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 6)
                {
                    hLink.NavigateUrl = "~/CMTPreview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else if (nReviewTypeID == 7)
                {
                    hLink.NavigateUrl = "~/UpdateHRReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
                else
                {
                    hLink.NavigateUrl = "~/UpdateReview.aspx?CoachingTicket=" + enctxt1 + "&ReviewType=" + enctxt2;
                }
            }
        }

        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadTextBox1.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
            }

        }

        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }

        }

        //export to PDF 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {

            if (Convert.ToInt32(RadTextBox1.Text) != 0)
            {
                try
                {
                    DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadTextBox1.Text));
                    DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                    DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                    string ImgDefault;
                    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                    FakeURLID.Value = URL;
                    if (!Directory.Exists(directoryPath))
                    {
                        ImgDefault = URL + "/Content/images/no-photo.jpg";
                    }
                    else
                    {

                        ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                    }

                    Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.NewPage();

                    iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
                    gif.ScaleToFit(10f, 10f);
                    //gif.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                    //gif.IndentationLeft = 15f;
                    //gif.IndentationRight = 15f;
                    //gif.SpacingAfter = 15f;
                    //gif.SpacingBefore = 15f;
                    //gif.Border = 0;



                    string cs = "Coaching Ticket ";
                    PdfPTable specificstable = new PdfPTable(2);
                    string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader));
                    specificscell.Colspan = 2;
                    //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                    specificscell.Border = 0;
                    specificstable.AddCell(specificscell);
                    //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                    //specificscelli00.Border = 0;
                    //                   specificscelli00.Rowspan = 7;


                    PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs)));
                    specificscelli1.Border = 0;
                    PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadTextBox1.Text)));
                    specificscelli2.Border = 0;


                    PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name "));
                    specificscelli3.Border = 0;
                    PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"])));
                    specificscelli4.Border = 0;

                    PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor "));
                    specificscelli5.Border = 0;
                    string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                    PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname));
                    specificscelli6.Border = 0;

                    PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department "));
                    specificscelli7.Border = 0;
                    PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"])));
                    specificscelli8.Border = 0;

                    PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign "));
                    specificscelli9.Border = 0;
                    string campval = "";
                    if (ds2.Tables[0].Rows.Count == 0)
                    {
                    }
                    else
                    {
                        campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                    }
                    PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval)));
                    specificscelli10.Border = 0;


                    PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic "));
                    specificscelli11.Border = 0;
                    PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])));
                    specificscelli12.Border = 0;

                    PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  "));
                    specificscelli13.Border = 0;
                    PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])));
                    specificscelli14.Border = 0;

                    PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  "));
                    specificscelli15.Border = 0;
                    PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"])));
                    specificscelli16.Border = 0;

                    PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  "));
                    specificscelli17.Border = 0;
                    PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"])));
                    specificscelli18.Border = 0;


                    //specificstable.AddCell(gif);
                    specificstable.AddCell(specificscelli1);
                    specificstable.AddCell(specificscelli2);
                    specificstable.AddCell(specificscelli15);
                    specificstable.AddCell(specificscelli16);
                    specificstable.AddCell(specificscelli3);
                    specificstable.AddCell(specificscelli4);
                    specificstable.AddCell(specificscelli5);
                    specificstable.AddCell(specificscelli6);
                    specificstable.AddCell(specificscelli7);
                    specificstable.AddCell(specificscelli8);
                    specificstable.AddCell(specificscelli9);
                    specificstable.AddCell(specificscelli10);
                    specificstable.AddCell(specificscelli11);
                    specificstable.AddCell(specificscelli12);
                    specificstable.AddCell(specificscelli13);
                    specificstable.AddCell(specificscelli14);
                    specificstable.AddCell(specificscelli17);
                    specificstable.AddCell(specificscelli18);

                    specificstable.SpacingAfter = 40;



                    Paragraph p13 = new Paragraph();
                    string line13 = "Documentation " + Environment.NewLine;
                    p13.Alignment = Element.ALIGN_LEFT;
                    p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p13.Add(line13);
                    p13.SpacingBefore = 10;
                    p13.SpacingAfter = 10;

                    Paragraph p14 = new Paragraph();
                    string line14 = "Description " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]);//RadDescription.Text);
                    p14.Alignment = Element.ALIGN_LEFT;
                    p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p14.Add(line14);
                    p14.SpacingBefore = 10;
                    p14.SpacingAfter = 10;


                    Paragraph p15 = new Paragraph();
                    string line15 = "Performance Result " + Environment.NewLine;
                    p15.Alignment = Element.ALIGN_LEFT;
                    p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p15.Add(line15);
                    p15.SpacingBefore = 10;
                    p15.SpacingAfter = 10;


                    PdfPTable perftable = new PdfPTable(5);
                    string perfheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell perfcell = new PdfPCell(new Phrase(perfheader));
                    perfcell.Colspan = 5;
                    perfcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    perftable.AddCell(perfcell);
                    perftable.SpacingBefore = 30;
                    perftable.SpacingAfter = 30;
                    perftable.WidthPercentage = 100;
                    DataSet ds_KPILIST = DataHelper.Get_ReviewIDKPI(Convert.ToInt32(RadTextBox1.Text));
                    if (ds_KPILIST.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_KPILIST.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI"));
                                perfcellh1.Border = 0;
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                                perfcellh2.Border = 0;
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current"));
                                perfcellh3.Border = 0;
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous"));
                                perfcellh4.Border = 0;
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                                perfcellh5.Border = 0;
                                perftable.AddCell(perfcellh1);
                                perftable.AddCell(perfcellh2);
                                perftable.AddCell(perfcellh3);
                                perftable.AddCell(perfcellh4);
                                perftable.AddCell(perfcellh5);
                            }
                            PdfPCell perfcellitem1 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["KPI"])));
                            perfcellitem1.Border = 0;
                            perftable.AddCell(perfcellitem1);

                            PdfPCell perfcellitem2 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Target"])));
                            perfcellitem2.Border = 0;
                            perftable.AddCell(perfcellitem2);

                            PdfPCell perfcellitem3 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Current"])));
                            perfcellitem3.Border = 0;
                            perftable.AddCell(perfcellitem3);

                            PdfPCell perfcellitem4 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Previous"])));
                            perfcellitem4.Border = 0;
                            perftable.AddCell(perfcellitem4);


                            PdfPCell perfcellitem5 = new PdfPCell(new Phrase(Convert.ToString(ds_KPILIST.Tables[0].Rows[ctr]["Description"])));
                            perfcellitem5.Border = 0;
                            perftable.AddCell(perfcellitem5);
                        }
                    }
                    else
                    {
                        //perftable.AddCell("Target");
                        //perftable.AddCell("Current");
                        //perftable.AddCell("Previous");
                        //perftable.AddCell("Description");
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("KPI"));
                        perfcellh1.Border = 0;
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                        perfcellh2.Border = 0;
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Current"));
                        perfcellh3.Border = 0;
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Previous"));
                        perfcellh4.Border = 0;
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                        perfcellh5.Border = 0;
                        perftable.AddCell(perfcellh1);
                        perftable.AddCell(perfcellh2);
                        perftable.AddCell(perfcellh3);
                        perftable.AddCell(perfcellh4);
                        perftable.AddCell(perfcellh5);
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No Performance Summary"));
                        perfcelli1.Border = 0;
                        perftable.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(" "));
                        perfcelli2.Border = 0;
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                        perftable.AddCell(perfcelli2);
                    }


                    Paragraph p16 = new Paragraph();
                    string line16 = "Coaching Notes " + Environment.NewLine;
                    p16.Alignment = Element.ALIGN_LEFT;
                    p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p16.Add(line16);
                    p16.SpacingBefore = 10;
                    p16.SpacingAfter = 10;


                    PdfPTable notestable = new PdfPTable(5);
                    string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell notescell = new PdfPCell(new Phrase(notesheader));
                    notescell.Colspan = 5;
                    notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    notestable.AddCell(notescell);
                    notestable.SpacingBefore = 30;
                    notestable.SpacingAfter = 30;
                    notestable.WidthPercentage = 100;
                    DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadTextBox1.Text));
                    if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                                perfcellh1.Border = 0;
                                notestable.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                                perfcellh2.Border = 0;
                                notestable.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                                perfcellh3.Border = 0;
                                notestable.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                                perfcellh4.Border = 0;
                                notestable.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                                perfcellh5.Border = 0;
                                notestable.AddCell(perfcellh4);

                            }
                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"])));
                            perfcelli1.Border = 0;
                            notestable.AddCell(perfcelli1);

                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"])));
                            perfcelli2.Border = 0;
                            notestable.AddCell(perfcelli2);

                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"])));
                            perfcelli3.Border = 0;
                            notestable.AddCell(perfcelli3);

                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"])));
                            perfcelli4.Border = 0;
                            notestable.AddCell(perfcelli4);

                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"])));
                            perfcelli5.Border = 0;
                            notestable.AddCell(perfcelli5);


                        }

                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                        perfcellh1.Border = 0;
                        notestable.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                        perfcellh2.Border = 0;
                        notestable.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                        perfcellh3.Border = 0;
                        notestable.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                        perfcellh4.Border = 0;
                        notestable.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                        perfcellh5.Border = 0;
                        notestable.AddCell(perfcellh4);

                        PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes."));
                        notescelli1.Border = 0;
                        notestable.AddCell(notescelli1);
                        PdfPCell notescelli2 = new PdfPCell(new Phrase(" "));
                        notescelli2.Border = 0;
                        notestable.AddCell(notescelli2);
                        notestable.AddCell(notescelli2);
                        notestable.AddCell(notescelli2);
                        notestable.AddCell(notescelli2);
                        notestable.AddCell(notescelli2);
                    }



                    DataSet ds_perfhistory = null;
                    DataAccess ws1 = new DataAccess();
                    ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadTextBox1.Text)), 2);

                    //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    Paragraph p1 = new Paragraph();
                    string line1 = "Performance history";
                    p1.Alignment = Element.ALIGN_LEFT;
                    p1.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p1.Add(line1);
                    p1.SpacingBefore = 10;
                    p1.SpacingAfter = 10;

                    PdfPTable tableperfhist = new PdfPTable(9);
                    string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                    PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader));
                    perfhistcell.Colspan = 9;
                    perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    tableperfhist.AddCell(perfhistcell);
                    tableperfhist.SpacingBefore = 30;
                    tableperfhist.SpacingAfter = 30;
                    tableperfhist.WidthPercentage = 100;
                    if (ds_perfhistory.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                                perfcellh1.Border = 0;
                                tableperfhist.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full Name"));
                                perfcellh2.Border = 0;
                                tableperfhist.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number"));
                                perfcellh3.Border = 0;
                                tableperfhist.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target"));
                                perfcellh4.Border = 0;
                                tableperfhist.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current"));
                                perfcellh5.Border = 0;
                                tableperfhist.AddCell(perfcellh5);
                                PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous"));
                                perfcellh6.Border = 0;
                                tableperfhist.AddCell(perfcellh6);
                                PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name"));
                                perfcellh7.Border = 0;
                                tableperfhist.AddCell(perfcellh7);
                                PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date"));
                                perfcellh8.Border = 0;
                                tableperfhist.AddCell(perfcellh8);
                                PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By"));
                                perfcellh9.Border = 0;
                                tableperfhist.AddCell(perfcellh9);

                            }

                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"])));
                            perfcelli1.Border = 0;
                            tableperfhist.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"])));
                            perfcelli2.Border = 0;
                            tableperfhist.AddCell(perfcelli2);
                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"])));
                            perfcelli3.Border = 0;
                            tableperfhist.AddCell(perfcelli3);
                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"])));
                            perfcelli4.Border = 0;
                            tableperfhist.AddCell(perfcelli4);
                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"])));
                            perfcelli5.Border = 0;
                            tableperfhist.AddCell(perfcelli5);
                            PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"])));
                            perfcelli6.Border = 0;
                            tableperfhist.AddCell(perfcelli6);
                            PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"])));
                            perfcelli7.Border = 0;
                            tableperfhist.AddCell(perfcelli7);
                            PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"])));
                            perfcelli8.Border = 0;
                            tableperfhist.AddCell(perfcelli8);
                            PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"])));
                            perfcelli9.Border = 0;
                            tableperfhist.AddCell(perfcelli9);

                        }
                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                        perfcellh1.Border = 0;
                        tableperfhist.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full Name"));
                        perfcellh2.Border = 0;
                        tableperfhist.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number"));
                        perfcellh3.Border = 0;
                        tableperfhist.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target"));
                        perfcellh4.Border = 0;
                        tableperfhist.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current"));
                        perfcellh5.Border = 0;
                        tableperfhist.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous"));
                        perfcellh6.Border = 0;
                        tableperfhist.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name"));
                        perfcellh7.Border = 0;
                        tableperfhist.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date"));
                        perfcellh8.Border = 0;
                        tableperfhist.AddCell(perfcellh8);
                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By"));
                        perfcellh9.Border = 0;
                        tableperfhist.AddCell(perfcellh9);
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History."));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(""));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);
                        tableperfhist.AddCell(perfcelli2);

                    }




                    Paragraph p17 = new Paragraph();
                    string line17 = "Strengths " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Strengths"]);//TxtStrenghts.Text);
                    p17.Alignment = Element.ALIGN_LEFT;
                    p17.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p17.Add(line17);
                    p17.SpacingBefore = 10;
                    p17.SpacingAfter = 10;


                    Paragraph p18 = new Paragraph();
                    string line18 = "Opportunities " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Opportunity"]); //txtOpportunities.Text);
                    p18.Alignment = Element.ALIGN_LEFT;
                    p18.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p18.Add(line18);
                    p18.SpacingBefore = 10;
                    p18.SpacingAfter = 10;


                    Paragraph p20 = new Paragraph();
                    string line20 = "Commitment " + Environment.NewLine;
                    p20.Alignment = Element.ALIGN_LEFT;
                    p20.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p20.Add(line20);
                    p20.SpacingBefore = 10;
                    p20.SpacingAfter = 10;

                    string coachfeedback = "";
                    PdfPTable commitmenttable = new PdfPTable(4);
                    string commitmentheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell commitmentcell = new PdfPCell(new Phrase(commitmentheader));
                    commitmentcell.Colspan = 4;
                    commitmentcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    commitmenttable.AddCell(commitmentcell);
                    commitmenttable.SpacingBefore = 30;
                    commitmenttable.SpacingAfter = 30;
                    commitmenttable.WidthPercentage = 100;
                    DataSet ds_scoring = DataHelper.GetCommentsforTriadpdf(Convert.ToInt32(Convert.ToInt32(RadTextBox1.Text)));
                    if (ds_scoring.Tables[0].Rows.Count > 0)
                    {
                        coachfeedback = (Convert.ToString(ds_scoring.Tables[0].Rows[0]["CoacherFeedback"]));

                        for (int ctr = 0; ctr < ds_scoring.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal"));
                                commitmentcellh1.Border = 0;
                                commitmenttable.AddCell(commitmentcellh1);
                                PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality"));
                                commitmentcellh2.Border = 0;
                                commitmenttable.AddCell(commitmentcellh2);
                                PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options"));
                                commitmentcellh3.Border = 0;
                                commitmenttable.AddCell(commitmentcellh3);
                                PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward"));
                                commitmentcellh4.Border = 0;
                                commitmenttable.AddCell(commitmentcellh4);

                                PdfPCell commitmentcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Goal_Text"])));
                                commitmentcelli1.Border = 0;
                                commitmenttable.AddCell(commitmentcelli1);
                                PdfPCell commitmentcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Reality_Text"])));
                                commitmentcelli2.Border = 0;
                                commitmenttable.AddCell(commitmentcelli2);
                                PdfPCell commitmentcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Options_Text"])));
                                commitmentcelli3.Border = 0;
                                commitmenttable.AddCell(commitmentcelli3);

                                PdfPCell commitmentcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["WayForward_Text"])));
                                commitmentcelli4.Border = 0;
                                commitmenttable.AddCell(commitmentcelli4);
                            }


                        }
                    }
                    else
                    {
                        coachfeedback = "";
                        PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal"));
                        commitmentcellh1.Border = 0;
                        commitmenttable.AddCell(commitmentcellh1);
                        PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality"));
                        commitmentcellh2.Border = 0;
                        commitmenttable.AddCell(commitmentcellh2);
                        PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options"));
                        commitmentcellh3.Border = 0;
                        commitmenttable.AddCell(commitmentcellh3);
                        PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward"));
                        commitmentcellh4.Border = 0;
                        commitmenttable.AddCell(commitmentcellh4);

                        PdfPCell commitmentcelli1 = new PdfPCell(new Phrase("No GROW Comments"));
                        commitmentcelli1.Border = 0;
                        commitmenttable.AddCell(commitmentcelli1);
                        PdfPCell commitmentcelli2 = new PdfPCell(new Phrase(""));
                        commitmentcelli2.Border = 0;
                        commitmenttable.AddCell(commitmentcelli2);

                        commitmenttable.AddCell(commitmentcelli2);
                        commitmenttable.AddCell(commitmentcelli2);
                    }

                    Paragraph p19 = new Paragraph();
                    string line19 = string.Empty;
                    if (string.IsNullOrEmpty(coachfeedback))
                    {
                        line19 = "N/A";
                    }
                    else {
                         line19 = "Coach Feedback " + Environment.NewLine + Convert.ToString(coachfeedback);
                    }
                    
                    p19.Alignment = Element.ALIGN_LEFT;
                    p19.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p19.Add(line19);
                    p19.SpacingBefore = 10;
                    p19.SpacingAfter = 10;


                    PdfPTable table = new PdfPTable(4);
                    string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cell = new PdfPCell(new Phrase(header));
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cell);
                    table.SpacingBefore = 30;
                    table.SpacingAfter = 30;
                    table.WidthPercentage = 100;


                    DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadTextBox1.Text), 3);
                    if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                                notescellh1.Border = 0;
                                table.AddCell(notescellh1);
                                PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                                notescellh2.Border = 0;
                                table.AddCell(notescellh2);
                                PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                                notescellh3.Border = 0;
                                table.AddCell(notescellh3);
                                PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                                notescellh4.Border = 0;
                                table.AddCell(notescellh4);

                            }
                            //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                            //alldocs = documents + Environment.NewLine + alldocs;

                            PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"])));
                            notescelli1.Border = 0;
                            table.AddCell(notescelli1);


                            PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"])));
                            notescelli2.Border = 0;
                            table.AddCell(notescelli2);

                            PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"])));
                            notescelli3.Border = 0;
                            table.AddCell(notescelli3);

                            PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"])));
                            notescelli4.Border = 0;
                            table.AddCell(notescelli4);

                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                            //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));

                        }
                    }
                    else
                    {
                        PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                        notescellh1.Border = 0;
                        table.AddCell(notescellh1);
                        PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                        notescellh2.Border = 0;
                        table.AddCell(notescellh2);
                        PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                        notescellh3.Border = 0;
                        table.AddCell(notescellh3);
                        PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                        notescellh4.Border = 0;
                        table.AddCell(notescellh4);
                        PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded."));
                        notescellh5.Border = 0;
                        table.AddCell(notescellh5);
                        PdfPCell notescellh6 = new PdfPCell(new Phrase(""));
                        notescellh6.Border = 0;
                        table.AddCell(notescellh6);
                        table.AddCell(notescellh6);
                        table.AddCell(notescellh6);
                    }

                    Paragraph p21 = new Paragraph();
                    string line21 = "Evaluation " + Environment.NewLine;
                    p21.Alignment = Element.ALIGN_LEFT;
                    p21.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p21.Add(line21);
                    p21.SpacingBefore = 10;
                    p21.SpacingAfter = 10;

                    PdfPTable evaluationtable = new PdfPTable(5);
                    string evaluationheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell evaluationcell = new PdfPCell(new Phrase(evaluationheader));
                    evaluationcell.Colspan = 5;
                    evaluationcell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    evaluationtable.AddCell(evaluationcell);
                    evaluationcell.Border = 0;
                    evaluationtable.SpacingBefore = 30;
                    evaluationtable.SpacingAfter = 30;
                    evaluationtable.WidthPercentage = 100;
                    DataSet ds_coachingevaluation = DataHelper.GetReviewcommitmentforpdf(Convert.ToInt32(RadTextBox1.Text));
                    //  int tablecount =  Convert.ToInt32(ds_coachingevaluation.Tables[0].Rows.Count);
                    if (ds_coachingevaluation.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_coachingevaluation.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell evaluationcellh1 = new PdfPCell(new Phrase("Coaching Components"));
                                evaluationcellh1.Border = 0;
                                evaluationtable.AddCell(evaluationcellh1);
                                PdfPCell evaluationcellh2 = new PdfPCell(new Phrase("Scoring"));
                                evaluationcellh2.Border = 0;
                                evaluationtable.AddCell(evaluationcellh2);
                                PdfPCell evaluationcellh3 = new PdfPCell(new Phrase("Pre Step"));
                                evaluationcellh3.Border = 0;
                                evaluationtable.AddCell(evaluationcellh3);
                                PdfPCell evaluationcellh4 = new PdfPCell(new Phrase("Proper Step"));
                                evaluationcellh4.Border = 0;
                                evaluationtable.AddCell(evaluationcellh4);
                                PdfPCell evaluationcellh5 = new PdfPCell(new Phrase("Post Step"));
                                evaluationcellh5.Border = 0;
                                evaluationtable.AddCell(evaluationcellh5);
                                //PdfPCell evaluationcellh6 = new PdfPCell(new Phrase("Total Score"));
                                //evaluationcellh6.Border = 0;
                                //evaluationtable.AddCell(evaluationcellh6);
                            }

                            PdfPCell evaluationcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["CoachingComponents"])));
                            evaluationcelli1.Border = 0;
                            evaluationtable.AddCell(evaluationcelli1);
                            PdfPCell evaluationcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["scoring"])));
                            evaluationcelli2.Border = 0;
                            evaluationtable.AddCell(evaluationcelli2);
                            PdfPCell evaluationcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["prepstep"])));
                            evaluationcelli3.Border = 0;
                            evaluationtable.AddCell(evaluationcelli3);
                            PdfPCell evaluationcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["properstep"])));
                            evaluationcelli4.Border = 0;
                            evaluationtable.AddCell(evaluationcelli4);
                            PdfPCell evaluationcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["properstep"])));
                            evaluationcelli5.Border = 0;
                            evaluationtable.AddCell(evaluationcelli5);
                            if (Convert.ToInt32(ds_coachingevaluation.Tables[0].Rows[ctr]["c_cmpnt_id"]) == 5) // && (Convert.ToInt32(ds_coachingevaluation.Tables[0].Rows[ctr]["totalscore"]) != 0))
                            {

                                PdfPCell evaluationcelli7 = new PdfPCell(new Phrase(Convert.ToString("Total Score")));
                                evaluationcelli7.Border = 0;
                                evaluationtable.AddCell(evaluationcelli7);
                                PdfPCell evaluationcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingevaluation.Tables[0].Rows[ctr]["totalscore"])));
                                evaluationcelli6.Border = 0;
                                evaluationtable.AddCell(evaluationcelli6);
                                PdfPCell evaluationcelli8 = new PdfPCell(new Phrase(""));
                                evaluationcelli8.Border = 0;
                                evaluationtable.AddCell(evaluationcelli8);
                                evaluationtable.AddCell(evaluationcelli8);
                                evaluationtable.AddCell(evaluationcelli8);
                            }


                        }


                    }
                    else
                    {
                        PdfPCell evaluationcellh1 = new PdfPCell(new Phrase("Coaching Components"));
                        evaluationcellh1.Border = 0;
                        evaluationtable.AddCell(evaluationcellh1);
                        PdfPCell evaluationcellh2 = new PdfPCell(new Phrase("Scoring"));
                        evaluationcellh2.Border = 0;
                        evaluationtable.AddCell(evaluationcellh2);
                        PdfPCell evaluationcellh3 = new PdfPCell(new Phrase("Pre Step"));
                        evaluationcellh3.Border = 0;
                        evaluationtable.AddCell(evaluationcellh3);
                        PdfPCell evaluationcellh4 = new PdfPCell(new Phrase("Proper Step"));
                        evaluationcellh4.Border = 0;
                        evaluationtable.AddCell(evaluationcellh4);
                        PdfPCell evaluationcellh5 = new PdfPCell(new Phrase("Post Step"));
                        evaluationcellh5.Border = 0;
                        evaluationtable.AddCell(evaluationcellh5);
                        //PdfPCell evaluationcellh6 = new PdfPCell(new Phrase("Total Score"));
                        //evaluationcellh6.Border = 0;
                        //evaluationtable.AddCell(evaluationcellh6);

                        PdfPCell evaluationcelli1 = new PdfPCell(new Phrase("No evaluation created."));
                        evaluationcelli1.Border = 0;
                        evaluationtable.AddCell(evaluationcelli1);
                        PdfPCell evaluationcelli2 = new PdfPCell(new Phrase(""));
                        evaluationcelli2.Border = 0;
                        evaluationtable.AddCell(evaluationcelli2);
                        evaluationtable.AddCell(evaluationcelli2);
                        evaluationtable.AddCell(evaluationcelli2);
                        evaluationtable.AddCell(evaluationcelli2);
                        //evaluationtable.AddCell(evaluationcelli2);
                    }


                    Paragraph p22 = new Paragraph();
                    string line22 = "Notes " + Environment.NewLine;
                    p22.Alignment = Element.ALIGN_LEFT;
                    p22.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p22.Add(line22);
                    p22.SpacingBefore = 10;
                    p22.SpacingAfter = 10;


                    PdfPTable n2table = new PdfPTable(2);
                    string n2header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell n2cell = new PdfPCell(new Phrase(n2header));
                    n2cell.Colspan = 2;
                    n2cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    n2table.AddCell(n2cell);
                    n2table.SpacingBefore = 30;
                    n2table.SpacingAfter = 30;
                    n2table.WidthPercentage = 100;
                    DataSet ds_notes2 = DataHelper.GetNotesforTriadforPDF(Convert.ToInt32(RadTextBox1.Text));
                    if (ds_notes2.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_notes2.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell n2cellh1 = new PdfPCell(new Phrase("Did Well"));
                                n2cellh1.Border = 0;
                                n2table.AddCell(n2cellh1);
                                PdfPCell n2cellh2 = new PdfPCell(new Phrase("Do Different"));
                                n2cellh2.Border = 0;
                                n2table.AddCell(n2cellh2);
                            }

                            PdfPCell n2celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_notes2.Tables[0].Rows[ctr]["didwell"])));
                            n2celli1.Border = 0;
                            n2table.AddCell(n2celli1);
                            PdfPCell evaluationcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_notes2.Tables[0].Rows[ctr]["DoDifferent"])));
                            evaluationcelli2.Border = 0;
                            n2table.AddCell(evaluationcelli2);


                        }


                    }
                    else
                    {

                        PdfPCell n2cellh1 = new PdfPCell(new Phrase("Did Well"));
                        n2cellh1.Border = 0;
                        n2table.AddCell(n2cellh1);
                        PdfPCell n2cellh2 = new PdfPCell(new Phrase("Do Different"));
                        n2cellh2.Border = 0;
                        n2table.AddCell(n2cellh2);
                        PdfPCell n2cellh3 = new PdfPCell(new Phrase("No Notes "));
                        n2cellh3.Border = 0;
                        n2table.AddCell(n2cellh3);

                        PdfPCell n2cellh4 = new PdfPCell(new Phrase(""));
                        n2cellh4.Border = 0;
                        n2table.AddCell(n2cellh4);

                    }

                    //pdfDoc.Add(new Paragraph(p1));
                    pdfDoc.Add(specificstable); //coaching specifics
                    pdfDoc.Add(new Paragraph(p14)); //description
                    pdfDoc.Add(new Paragraph(p15)); //perfsummary
                    pdfDoc.Add(perftable); //table 
                    pdfDoc.Add(new Paragraph(p16)); //notes table
                    pdfDoc.Add(notestable); //notes table 
                    pdfDoc.Add(new Paragraph(p1)); //perf hist
                    pdfDoc.Add(tableperfhist);
                    pdfDoc.Add(new Paragraph(p17)); //strengths
                    pdfDoc.Add(new Paragraph(p18)); //oppportunitiess
                    pdfDoc.Add(new Paragraph(p19)); //coacher feedback
                    pdfDoc.Add(new Paragraph(p20)); //commitment table
                    pdfDoc.Add(commitmenttable);

                    pdfDoc.Add(new Paragraph(p13)); //docu header
                    pdfDoc.Add(table);//documentation
                    pdfDoc.Add(new Paragraph(p21));
                    pdfDoc.Add(evaluationtable); //evaluation
                    pdfDoc.Add(new Paragraph(p22));// notes
                    pdfDoc.Add(n2table);

                    pdfDoc.Close();


                    Response.ContentType = "application/pdf";
                    string filename = "CoachingTicket_" + Convert.ToString(RadTextBox1.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                        + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                    string filenameheader = filename + ".pdf";
                    string filenameheadername = "filename=" + filenameheader;
                    Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message.ToString());

                }
            }
            else
            {
                string script = "alert('Please submit the review before creating a pdf.')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "its working", script, true);

            }

        }        //export to PDF

        public void GetCommitment(int CoachingTicket)
        {
            //DataSet ds_scoring = DataHelper.GetCommentsforTriad(Convert.ToInt32(CoachingTicket));
            //grd_Commitment.DataSource = ds_scoring;
            //grd_Commitment.Enabled = false;
            RadTextBox2.Text = "What do you want to achieve?";
            RadTextBox4.Text = "Where are you now? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox6.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that? (SMART)";
            RadTextBox8.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

            DataSet ds_scoring = DataHelper.GetCommentsforTriad(CoachingTicket);
            if (ds_scoring.Tables[0].Rows.Count > 0)
            {
                RadTextBox3.Text = ds_scoring.Tables[0].Rows[0]["Goal_Text"].ToString();
                RadTextBox5.Text = ds_scoring.Tables[0].Rows[0]["Reality_Text"].ToString();
                RadTextBox7.Text = ds_scoring.Tables[0].Rows[0]["Options_Text"].ToString();
                RadTextBox9.Text = ds_scoring.Tables[0].Rows[0]["WayForward_Text"].ToString();

            }
        }

        public void DisableCommitmentTable(bool state)
        {
            RadTextBox2.Enabled = state;
            RadTextBox3.Enabled = state;
            RadTextBox4.Enabled = state;
            RadTextBox5.Enabled = state;
            RadTextBox6.Enabled = state;
            RadTextBox7.Enabled = state;
            RadTextBox8.Enabled = state;
            RadTextBox9.Enabled = state;
        }

        protected void commitmentautosum(object sender, EventArgs e)
        {
            //   int sum = Convert.ToInt32(RadTextBox3.Text) + Convert.ToInt32(RadTextBox5.Text) + Convert.ToInt32(RadTextBox7.Text) + Convert.ToInt32(RadTextBox9.Text);

            int reviewid = Convert.ToInt32(DataHelper.Decrypt( Page.Request.QueryString["CoachingTicket"]));
            //save_notes(CIMNumber, reviewid);
            int scoringval = 0;
            int c_cmpntid;
            int totalval = 0;
            string prepstepval, properstepval, poststepval;
            int e1 = 0;
            int e2 = 0;
            int e3 = 0;
            int e4 = 0;
            int e5 = 0;
            int sum = 0;

            //GridViewRow row = ((GridViewRow)((RadNumericTextBox)sender).NamingContainer);
            //RadNumericTextBox txt = (RadNumericTextBox)row.FindControl("txt_scoring");

            //scoringval = sum + Convert.ToInt32(txt.Text);
            //txttotalscore.Text = Convert.ToString(scoringval);

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('Score: " + scoringval + "','Warning'); });", true);
            //return;



            foreach (GridDataItem itm in grd_coachingevaluation.MasterTableView.Items)
            {
               


                int itmindx = itm.ItemIndex + 1;

                reviewid = Convert.ToInt32(DataHelper.Decrypt(Page.Request.QueryString["CoachingTicket"]));
                c_cmpntid = Convert.ToInt32(itmindx);
                try
                {
                    scoringval = Convert.ToInt32((itm.FindControl("txt_scoring") as RadNumericTextBox).Text); 
                }
                catch (Exception)
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('Only Accept Whole Number','Warning'); });", true);
                    return;
                }
                prepstepval = (itm.FindControl("txt_prestep") as RadTextBox).Text;
                properstepval = (itm.FindControl("txt_Properstep") as RadTextBox).Text; 
                poststepval = (itm.FindControl("txt_Poststep") as RadTextBox).Text; 
                 
                if (c_cmpntid == 1)
                {
                    
                    if ((scoringval == 0) || (scoringval == 20))//(scoringval > 21)
                    {

                        grd_coachingevaluation.Focus();
                    }
                    else
                    { 
                        string ModalLabel = "C  - Communication and Follow Through  Score Input score must only be 0 or 20! ";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //scoringval = 0;
                        grd_coachingevaluation.Focus();

                    }
                    e1 = scoringval;
                }
                else if (c_cmpntid == 2)
                {
                    if ((scoringval == 0) || (scoringval == 30))//(scoringval > 31)
                    {
                        grd_coachingevaluation.Focus();
                    }
                    else
                    {
                        
                        string ModalLabel = "O - Obtain and gather information to drill behaviours on RCA  Score Input score must only be 0 or 30! ";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //scoringval = 0;
                        grd_coachingevaluation.Focus();
                        
                    }

                    e2 = scoringval;
                }
                else if (c_cmpntid == 3)
                {
                    if ((scoringval == 0) || (scoringval == 25)) //(scoringval > 26)
                    {
                        grd_coachingevaluation.Focus();
                    }
                    else
                    { 
                        //string myStringVariable = "Input score must only be 0 or 25!";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        string ModalLabel = "A - Action Items that lead to Specific, Measurable, Attainable, Relevance and Timebound  Score Input score must only be 0 or 25! ";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //scoringval = 0;
                        grd_coachingevaluation.Focus();
                    }

                    e3 = scoringval;
                }
                else if (c_cmpntid == 4)
                {
                    if ((scoringval == 0) || (scoringval == 15))//(scoringval > 16) 
                    {
                        grd_coachingevaluation.Focus();
                    }
                    else
                    {
                        //string myStringVariable = "Input score must only be 0 or  15!";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        string ModalLabel = "C - Craft and Concrete Documentation Score Input score must only be 0 or 15! ";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //scoringval = 0;
                        grd_coachingevaluation.Focus();
                    

                    }

                    e4 = scoringval;
                }

                else if (c_cmpntid == 5)
                {
                    if ((scoringval == 0) || (scoringval == 10)) //(scoringval > 11
                    {
                        grd_coachingevaluation.Focus();
                    }
                    else
                    { 
                        // string myStringVariable = "Input score must only be 0 or 10!";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                      
                        string ModalLabel = "H - Highly Engaged Conversation Score Input score must only be 0 or 10! ";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                        //scoringval = 0;
                        grd_coachingevaluation.Focus();
                    }
                    e5 = scoringval;
                   
                }
                else
                {
                   
                }

                sum = e1 + e2 + e3 + e4 + e5;
                txttotalscore.Text = Convert.ToString(sum);        

            }

        }


        //protected void grd_coachingevaluation_ItemCreated(object sender, EventArgs e)
        //{

        //    foreach (GridDataItem item in grd_coachingevaluation.MasterTableView.Items)
        //    {
        //        RadNumericTextBox txtBox = (RadNumericTextBox)item.FindControl("txt_scoring");
        //        txtBox.TextChanged += new EventHandler(commitmentautosum);

        //        RadAjaxManager1.AjaxSettings.AddAjaxSetting(txtBox, grd_coachingevaluation);
        //        RadAjaxManager1.AjaxSettings.AddAjaxSetting(txtBox, txttotalscore);
        //    }

        //}

        protected void grd_coachingevaluation_OnItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                var _componentscore = dataItem["ComponentScore"].Text;
                RadComboBox _drpscore = (RadComboBox)e.Item.FindControl("drpScore");
                _drpscore.Items.Add(new RadComboBoxItem("","0"));
                _drpscore.Items.Add(new RadComboBoxItem(_componentscore.ToString(), _componentscore.ToString()));
                _drpscore.Items.Add(new RadComboBoxItem("0","0"));

                RadNumericTextBox txt_scoring = (RadNumericTextBox)e.Item.FindControl("txt_scoring");

                if (txt_scoring.Text == "0")
                {
                    if (grd_coachingevaluation.Enabled == true) //wrong result in enabled...
                        _drpscore.SelectedIndex = 0;
                    else
                        _drpscore.SelectedIndex = 2;
                }
                else
                {
                    _drpscore.SelectedIndex = 1;
                }
            }
        }

        //protected void grd_coachingevaluation_onitemdatabound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        RadNumericTextBox btn = (RadNumericTextBox)e.Item.FindControl("txt_scoring");
        //        btn.TextChanged += commitmentautosum;
        //        AjaxSetting ajaxsetting1 = new AjaxSetting(btn.ID);
        //        ajaxsetting1.UpdatedControls.Add(new AjaxUpdatedControl(txttotalscore.ID, RadAjaxLoadingPanel1.ID));
        //        ajaxsetting1.UpdatedControls.Add(new AjaxUpdatedControl(grd_coachingevaluation.ID, RadAjaxLoadingPanel1.ID));
        //        RadAjaxManager1.AjaxSettings.Add(ajaxsetting1);
        //    }
        //}
     
    }
    
}