﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using CoachV2.AppCode;

namespace CoachV2
{
    public partial class AddMassReview : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCoachingType();
            }
        }


       
        protected void RadMassType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (RadMassType.SelectedValue == "1")
            {
                MassCoachingAll2.Visible = true;
                MassCoachingGroups1.Visible = false;
                MassCoachingSelected1.Visible = false;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = true;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = false;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = false;


            }
            else if (RadMassType.SelectedValue == "2")
            {
                MassCoachingAll2.Visible = false;
                MassCoachingGroups1.Visible = true;
                MassCoachingSelected1.Visible = false;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = false;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = false;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = true;
            }
            else
            {
                MassCoachingAll2.Visible = false;
                MassCoachingGroups1.Visible = false;
                MassCoachingSelected1.Visible = true;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = false;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = true;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = false;
            }
        }

        public void LoadCoachingType()
        {
            if (RadMassType.SelectedValue == "1")
            {
                MassCoachingAll2.Visible = true;
                MassCoachingGroups1.Visible = false;
                MassCoachingSelected1.Visible = false;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = true;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = false;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = false;

            }
            else if (RadMassType.SelectedValue == "2")
            {
                MassCoachingAll2.Visible = false;
                MassCoachingGroups1.Visible = true;
                MassCoachingSelected1.Visible = false;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = false;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = false;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = true;
            }
            else
            {
                MassCoachingAll2.Visible = false;
                MassCoachingGroups1.Visible = false;
                MassCoachingSelected1.Visible = true;
                //Panel ctrlA = (Panel)MassCoachingAll2.FindControl("CoachingAllPanel");
                //ctrlA.Visible = false;
                //Panel ctrlB = (Panel)MassCoachingAll2.FindControl("CoachingSelectedPanel");
                //ctrlB.Visible = true;
                //Panel ctrlC = (Panel)MassCoachingAll2.FindControl("CoachingGroupsPanel");
                //ctrlC.Visible = false;
            }

        }

    }
}