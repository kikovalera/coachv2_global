﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CoachV2
{
    public partial class EditProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet dsUserInfo = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            string cim_num =  dsUserInfo.Tables[0].Rows[0]["CIMNo"].ToString();

            DataSet ds = DataHelper.GetUserRolesAssigned(Convert.ToInt32(cim_num), 16);

            if (ds.Tables[0].Rows.Count == 0)
                Response.Redirect("~/MyProfile.aspx");


        }
    }
}