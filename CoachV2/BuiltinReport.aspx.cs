﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
 
namespace CoachV2
{
    public partial class BuiltinReport : System.Web.UI.Page
    {  
        protected void Page_Load(object sender, EventArgs e)
        { 
            //Label ctrlA1 = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
            //ctrlA1.Visible = true;
            //ctrlA1.Text = "  Built In Report";

            Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
            ctrlReviews.Text = " Coaching Dashboard ";
            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
            ctrlA.Text = " > My Reports";
            Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
            ctrlB.Text = " > Built In Report";
            ctrlB.Visible = true;
            if (!IsPostBack) 
            {
                //users
                DataSet ds_user = DataHelper.getreportbuiltinusers();
                cb_users.DataSource = ds_user;
                cb_users.DataBind();

                //ticket types
                DataSet ds_tickettypes = DataHelper.GetAllTicketTypes();
                cb_dashboards.DataSource = ds_tickettypes;
                cb_dashboards.DataBind();

                //session type
                DataSet ds_sessiontype = null;
                DataAccess ws = new DataAccess();
                ds_sessiontype = ws.GetSessionTypes();                 
                cb_sessiontype.DataSource = ds_sessiontype;
                cb_sessiontype.DataBind();

                //department 
                DataSet ds_dept = DataHelper.GetAllDepartments();
                cb_department.DataSource = ds_dept;
                cb_department.DataBind(); 
            }

        } 
        protected void cb_sessiontype_onselectedindex(object sender, EventArgs e)
        {
            cb_sessiontopic.Items.Clear();
            DataSet ds_sessiontopic = DataHelper.GetAllTopics(Convert.ToInt32(cb_sessiontype.SelectedValue));
            cb_sessiontopic.DataSource = ds_sessiontopic;
            cb_sessiontopic.DataBind();
        }

        //protected void cb_account_onselectedindex(object sender, EventArgs e)
        //{
        //    cb_LOB.Items.Clear();
        //    DataSet ds_lob = DataHelper.GetAllLOB(Convert.ToInt32(cb_account.SelectedValue));
        //    cb_LOB.DataSource = ds_lob;
        //    cb_LOB.DataBind();

        //}

        protected void btnViewReport_onClick(object sender, EventArgs e) 
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNo;
                CIMNo = Convert.ToInt32(cim_num);
                string DateFrom, DateTo;
                if (dp_start.SelectedDate.HasValue)
                {
                    DateFrom = dp_start.SelectedDate.Value.ToString("yyyy-MM-dd");
           
                }
                else
                {
                    DateFrom = dp_start.FocusedDate.ToString("yyyy-MM-dd"); //DateTime.Today.Date; //  DBNull.Value.ToString();
                }

                if (dp_end.SelectedDate.HasValue)
                {
                    DateTo = dp_end.SelectedDate.Value.ToString("yyyy-MM-dd");//dp_end.SelectedDate.ToString();
                }
                else
                {
                    DateTo = dp_end.FocusedDate.ToString("yyyy-MM-dd"); //DBNull.Value.ToString();
                }
                if ((cb_dashboards.SelectedIndex == -1) && (cb_sessiontype.SelectedIndex == -1) &&
                    (cb_sessiontopic.SelectedIndex == -1) && (cb_users.SelectedIndex == -1) &&
                     (cb_department.SelectedIndex == -1) )
                     //&& (cb_account.SelectedIndex == -1) && (cb_LOB.SelectedIndex == -1) )
                {

                    Report rt = new Report();
                    string repname = "Built-In Report " + DateTime.Now ;
                    int ReportID = rt.InsertReportNew(repname, CIMNo.ToString(), DateFrom, DateTo, 0, 0, 2);
                    HiddenReportID.Value = ReportID.ToString();
                    //ReportID=" + ReportID
                    string values_cb_dashboards = "0";

                     
                    if (cb_dashboards.SelectedIndex == -1)
                    {
                        values_cb_dashboards = "0";
                    }
                    else
                    {
                        values_cb_dashboards = cb_dashboards.SelectedValue;

                    }
                    string values_sessiontype = "0";
                    if (cb_sessiontype.SelectedIndex == -1)
                    {
                        values_sessiontype = "0";

                    }
                    else
                    {
                        values_sessiontype = cb_sessiontype.SelectedValue;
                    }
                    string values_sessiontopic = "0";
                    if (cb_sessiontopic.SelectedIndex == -1)
                    {
                        values_sessiontopic = "0";

                    }
                    else
                    {
                        values_sessiontopic = cb_sessiontopic.SelectedValue;
                    }
                    string values_users = "0";
                    if (cb_users.SelectedIndex == -1)
                    {
                        values_users = "0";

                    }
                    else
                    {
                        values_users = cb_users.SelectedValue;
                    }
                    string values_dept = "0";
                    if (cb_department.SelectedIndex == -1)
                    {
                        values_dept = "0";

                    }
                    else
                    {
                        values_dept = cb_department.SelectedValue;
                    }

                    string values_account = "0";
        
                    string values_lob = "0";
               


                    DataHelper.CreateBuiltInReport(ReportID, Convert.ToInt32(values_cb_dashboards),
                                        Convert.ToInt32(values_sessiontype),
                                        Convert.ToInt32(values_sessiontopic),
                                        Convert.ToInt32(values_users),
                                        Convert.ToInt32(values_dept),
                                        Convert.ToInt32(values_account),
                                        Convert.ToInt32(values_lob),
                                        0,
                                        Convert.ToDateTime(DateFrom), Convert.ToDateTime(DateTo));

                    ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ReportID + "&UserID=" + Convert.ToString(CIMNo) + "','Graph','height=400,width=500');", true);
                   }
                else
                {
                    Report rt = new Report(); 
                    string repname = "Built-In Report " + DateTime.Now;
                    int ReportID = rt.InsertReportNew(repname, CIMNo.ToString(), DateFrom, DateTo, 0, 0, 2);

                    HiddenReportID.Value = ReportID.ToString();
                    ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + ReportID + "&UserID=" + Convert.ToString(CIMNo) + "','Graph','height=400,width=500');", true);
                    
                    string values_cb_dashboards = "0";

                    if (dp_start.SelectedDate.HasValue)
                    {
                        DateFrom = dp_start.SelectedDate.Value.ToString("yyyy-MM-dd");//dp_start.SelectedDate.Value.ToString("yyyy-MM-dd"); //dp_start.SelectedDate.ToString();  //dp_start.SelectedDate.ToString("yyyy-MM-dd");//
                    }
                    else
                    {
                        DateFrom = dp_start.FocusedDate.ToString("yyyy-MM-dd"); //DateTime.Today.Date; //  DBNull.Value.ToString();
                    }

                    if (dp_end.SelectedDate.HasValue)
                    {
                        DateTo = dp_end.SelectedDate.Value.ToString("yyyy-MM-dd");//dp_end.SelectedDate.ToString();
                    }
                    else
                    {
                        DateTo = dp_end.FocusedDate.ToString(); //DBNull.Value.ToString();
                    }
                    if (cb_dashboards.SelectedIndex == -1)
                    {
                        values_cb_dashboards = "0";
                    }
                    else
                    {
                        values_cb_dashboards = cb_dashboards.SelectedValue;

                    }
                    string values_sessiontype = "0";
                    if (cb_sessiontype.SelectedIndex == -1)
                    {
                        values_sessiontype = "0";

                    }
                    else
                    {
                        values_sessiontype = cb_sessiontype.SelectedValue;
                    }
                    string values_sessiontopic = "0";
                    if (cb_sessiontopic.SelectedIndex == -1)
                    {
                        values_sessiontopic = "0";

                    }
                    else
                    {
                        values_sessiontopic = cb_sessiontopic.SelectedValue;
                    }
                    string values_users = "0";
                    if (cb_users.SelectedIndex == -1)
                    {
                        values_users = "0";

                    }
                    else
                    {
                        values_users = cb_users.SelectedValue;
                    }
                    string values_dept = "0";
                    if (cb_department.SelectedIndex == -1)
                    {
                        values_dept = "0";

                    }
                    else
                    {
                        values_dept = cb_department.SelectedValue;
                    }

                    string values_account = "0";
            
                    string values_lob = "0";
                  

                    DataHelper.CreateBuiltInReport(ReportID, Convert.ToInt32(values_cb_dashboards),
                                        Convert.ToInt32(values_sessiontype),
                                        Convert.ToInt32(values_sessiontopic),
                                        Convert.ToInt32(values_users),
                                        Convert.ToInt32(values_dept),
                                        Convert.ToInt32(values_account),
                                        Convert.ToInt32(values_lob),
                                        0,
                                        Convert.ToDateTime(DateFrom), Convert.ToDateTime(DateTo));
                }
            }
            catch (Exception ex) { 
            
            }
        }
    }
}