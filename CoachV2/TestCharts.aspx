﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master"  CodeBehind="TestCharts.aspx.cs" Inherits="CoachV2.TestCharts" %>
<%@ Register src="UserControl/TLChartDashboard.ascx" tagname="UserControl4" tagprefix="ucdash4" %>
<%@ Register src="UserControl/OM_DirCharts.ascx" tagname="UserControl7" tagprefix="ucdash7" %>
<%@ Register src="UserControl/QAChartDB.ascx" tagname="UserControl6" tagprefix="ucdash6" %>
<%@ Register src="UserControl/DashboardMenuTopMyReview.ascx" tagname="HeadUserControl1" tagprefix="ucdash3" %>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<ucdash4:UserControl4  ID="TLChartsDB" runat="server" Visible="true" />
<ucdash7:UserControl7  ID="OMDirChartsDB" runat="server" Visible="true" />
<ucdash6:UserControl6  ID="QAChartsDB" runat="server" Visible="true" />

</asp:Content>