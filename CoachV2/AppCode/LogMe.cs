﻿//=============================================================
// LOGME.CS: Class for logging messages or exceptions
// Logs are saved/added to a json/text file based on dates.
// Example of log file: TranscomCoachV2_Logs_20180720.json/log
//
// Created by: 10157011/francis.valera@transcom.com
// Created on: July 20, 2018
//
// Input Parameters:
//    (1) ex - exception (required, null allowed)
//    (2) msg  - custom message (optional)
//
// Usage:
//    LogMe.InJson(Exception, "Custom Message");
//    LogMe.InText(Exception, "Custom Message");
//
// Example:
//    LogMe.InJson(ex, "Dashboard/My Reviews");
//    LogMe.InText(ex, "Dashboard/My Reviews");
//
//=============================================================

using System;
using System.IO;
using context = System.Web.HttpContext;
using CoachV2.AppCode;
using System.Data;
using System.Web;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Linq;

namespace CoachV2.AppCode
{
    public class LogMe
    {
        public static string filepath = "";

        // save logs to json
        public static string InJson(Exception ex = null, String msg = null)
        {
            string strlogid = "";
            try
            {
                filepath = context.Current.Server.MapPath("~/LogFiles/");
                filepath = filepath + "TranscomCoachV2_Logs_" + DateTime.Today.ToString("yyyyMMdd") + ".json";
                if (!File.Exists(filepath))
                {
                    File.Create(filepath).Dispose();
                    using (StreamWriter swcreate = File.AppendText(filepath))
                    {
                        swcreate.Write("[]");
                        swcreate.Flush();
                        swcreate.Close();
                    }
                }
                string strUserCIM = "N/A";
                string strUserEmail = "N/A";
                if (HttpContext.Current.User.Identity.Name != "") // tests if a user is logged in
                {
                    strUserCIM = HttpContext.Current.User.Identity.Name.Split('|')[3].ToString();
                    strUserEmail = HttpContext.Current.User.Identity.Name.Split('|')[0].ToString();
                }
                string strErrorPage = context.Current.Request.Url.PathAndQuery.ToString();
                string strErrorCode = "";
                //string strErrorLocation = "";
                string strErrorLine = "";
                string strErrorMsg = "";
                string strErrorStackTrace = "";
                HttpException httpex = ex as HttpException;
                strErrorCode = httpex.GetHttpCode().ToString();
                switch (httpex.GetHttpCode())
                {
                    case 404:
                        strErrorMsg = httpex.Message;
                        strErrorStackTrace = httpex.StackTrace;
                        break;
                    case 500:
                        Regex r = new Regex(@"at (?<namespace>.*)\.(?<class>.*)\.(?<method>.*(.*)) in (?<file>.*):line (?<line>\d*)");
                        var result = r.Match(@ex.InnerException.StackTrace.ToString().Trim());
                        if (result.Success || result != null)
                        {
                            //strErrorLocation = "/" + result.Groups["namespace"].Value.ToString().Trim() + "/" + result.Groups["class"].Value.ToString().Trim();
                            strErrorLine = "/" + result.Groups["method"].Value.ToString().Trim() + " (Line " + result.Groups["line"].Value.ToString().Trim() + ")";
                        }
                        strErrorMsg = ex.InnerException.Message;
                        strErrorStackTrace = ex.StackTrace;
                        break;
                    default:
                        strErrorMsg = httpex.Message;
                        strErrorStackTrace = httpex.StackTrace;
                        break;
                }
                var _newlogdata = new JObject();
                _newlogdata["LogID"] = Guid.NewGuid();
                _newlogdata["LogDateTime"] = DateTime.Now;
                _newlogdata["LogUserCIM"] = strUserCIM;
                _newlogdata["LogUserEmail"] = strUserEmail;
                _newlogdata["LogCustomMessage"] = msg ?? "null";
                _newlogdata["LogErrorCode"] = strErrorCode;
                _newlogdata["LogErrorUrl"] = strErrorPage;
                //_newlogdata["LogErrorLocation"] = strErrorLocation;
                _newlogdata["LogErrorLine"] = strErrorLine;
                _newlogdata["LogErrorMessage"] = strErrorMsg;
                _newlogdata["LogStackTrace"] = strErrorStackTrace;
                var initialJson = File.ReadAllText(@filepath);
                var array = JArray.Parse(initialJson);
                array.Add(_newlogdata);
                var jsonToOutput = JsonConvert.SerializeObject(array, Formatting.Indented);
                System.IO.File.WriteAllText(filepath, jsonToOutput);
                strlogid = _newlogdata["LogID"].ToString();
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return strlogid;
        }

        // save logs to text
        //public static void InText(String msg, Exception ex = null)
        //{
        //    try
        //    {
        //        filepath = filepath + "TranscomCoachV2_Logs_" + DateTime.Today.ToString("yyyyMMdd") + ".log";
        //        if (!File.Exists(filepath))
        //        {
        //            File.Create(filepath).Dispose();
        //            using (StreamWriter swcreate = File.AppendText(filepath))
        //            {
        //                swcreate.WriteLine("Transcom: CoachV2 Log File for" + " " + DateTime.Now.Date.ToString("d"));
        //                swcreate.WriteLine("========================================" + Environment.NewLine);
        //                swcreate.Flush();
        //                swcreate.Close();
        //            }
        //        }
        //        using (StreamWriter sw = File.AppendText(filepath))
        //        {
        //            string LogString = "";
        //            LogString += "Log ID        : " + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Environment.NewLine;
        //            LogString += "Date and Time : " + DateTime.Now.ToString() + Environment.NewLine;
        //            string strUserLogin = "N/A";
        //            if (HttpContext.Current.User.Identity.Name != "") // tests if a user is logged in
        //            {
        //                strUserLogin = HttpContext.Current.User.Identity.Name.Split('|')[3].ToString() + "/" + HttpContext.Current.User.Identity.Name.Split('|')[0].ToString();
        //            }
        //            LogString += "User Login    : " + strUserLogin + Environment.NewLine;
        //            LogString += "Message       : " + msg + Environment.NewLine;
        //            if (ex != null) // log exception details only when provided
        //            {
        //                LogString += "Page Url      : " + context.Current.Request.Url.PathAndQuery.ToString() + Environment.NewLine;
        //                LogString += "Type/Line No. : " + ex.GetType().ToString().Trim() + " @ " + ex.StackTrace.ToString() + Environment.NewLine;  //ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7) + Environment.NewLine;
        //                LogString += "Exception     : " + ex.Message.ToString() + Environment.NewLine;
        //            }
        //            sw.WriteLine(LogString);
        //            sw.Flush();
        //            sw.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        e.ToString();
        //    }
        //}

        public int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

    }
}