﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2.AppCode
{
    public class QA
    {

        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "cn_CoachV2";


        public QA()
        {
            ConnectionStringSettings dbConns;
            dbConns = ConfigurationManager.ConnectionStrings[CONNSTR];
            strConn = dbConns.ConnectionString;
            conn = new SqlConnection(strConn);
        }

        public DataTable GetQAAccts(int CimNumber)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetQAAccts", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;
        }

        public DataTable GetSupervisorForQA(int CimNumber, int AccountID)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetTLsForQA", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@Account", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;
        }

        public string CheckIfQAOverAll(int CIMNumber)
        {
            string Role;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_CheckifQAOverall", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIMNumber", CIMNumber);
            cmd.Parameters.Add("@Role", SqlDbType.NVarChar, 3).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            Role = cmd.Parameters["@Role"].Value.ToString();

            return Role;

        }

        public int InsertReviewQA(int CoacheeID, int AccountID, int SupervisorID, int TopicID, DateTime? FollowupDate, string Description, string Strengths, string Opportunity, string Commitment, int Signed, int CreatedBy, int ReviewType, int AssignmentType)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_reviewQA", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@FollowupDate", FollowupDate);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            cmd.Parameters.AddWithValue("@Signed", Signed);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@AssignmentType", AssignmentType);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public int InsertReviewNexidiaQA(int CoacheeID, int AccountID, int SupervisorID, int TopicId, string FollowDate, string Description, string Strengths, string Opportunity,
           int SessionFocus, string PositiveBehaviour, string OpportunityBehaviour, string BeginBehaviourComments, string BehaviourEffects, string RootCause, string ReviewResultsComments,
           string AddressOpportunities, string ActionPlanComments, string SmartGoals, string CreatePlanComments, string FollowThrough, int CreatedBy, int ReviewType, int FormType, int AssignmentType)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review_nexidia_qa", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicId);
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);

            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }
            cmd.Parameters.AddWithValue("@PositiveBehaviour", PositiveBehaviour);
            cmd.Parameters.AddWithValue("@OpportunityBehaviour", OpportunityBehaviour);
            cmd.Parameters.AddWithValue("@BeginBehaviourComments", BeginBehaviourComments);

            cmd.Parameters.AddWithValue("@BehaviourEffects", BehaviourEffects);
            cmd.Parameters.AddWithValue("@RootCause", RootCause);
            cmd.Parameters.AddWithValue("@ReviewResultsComments", ReviewResultsComments);
            cmd.Parameters.AddWithValue("@AddressOpportunities", AddressOpportunities);
            cmd.Parameters.AddWithValue("@ActionPlanComments", ActionPlanComments);
            cmd.Parameters.AddWithValue("@SmartGoals", SmartGoals);

            cmd.Parameters.AddWithValue("@CreatePlanComments", CreatePlanComments);
            cmd.Parameters.AddWithValue("@FollowThrough", FollowThrough);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@AssignmentType", AssignmentType);
            cmd.Parameters.AddWithValue("@FormType", FormType);

            conn.Open();
            int NexidiaReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return NexidiaReviewID;
        }

        public int InsertReviewODQA(int CoacheeID, int AccountID, int SupervisorID, int TopicID, string FollowDate, string Description, string Strengths, string Opportunity, int SessionFocus, int CreatedBy, string CoacherFeedback, int CallID, int ReviewType, int AssignmentType, int FormType)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review_od_qa", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }

            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            if (CallID == 0)
            {
                cmd.Parameters.AddWithValue("@CallID", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CallID", CallID);
            }
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@AssignmentType", AssignmentType);
            cmd.Parameters.AddWithValue("@FormType", FormType);

            conn.Open();
            int ODReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ODReviewID;
        }
    }
}