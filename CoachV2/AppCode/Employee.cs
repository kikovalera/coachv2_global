﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.AppCode
{
    [Serializable]
    public class Employee
    {
        public Guid UniqueID { get; set; }
        public int ID { get; set; }
        public int KPIID { get; set; }
        public string Name { get; set; }
        public string Target { get; set; }
        public string Current { get; set; }
        public string Previous { get; set; }
        public int Driver { get; set; }
        public string DriverName { get; set; }


    }
}