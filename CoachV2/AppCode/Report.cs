﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2.AppCode
{
    public class Report
    {

        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "cn_CoachV2";

        public Report()
        {
            ConnectionStringSettings dbConns;
            dbConns = ConfigurationManager.ConnectionStrings[CONNSTR];
            strConn = dbConns.ConnectionString;
            conn = new SqlConnection(strConn);
        }

        public DataSet GetColumns(string Datasource)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("SELECT b.DataSourceID,Column_Name as Fields FROM information_schema.columns a inner join [DataSources] b on a.table_name = b.DataSourceName WHERE table_name = '"+ Datasource+"'", conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Columns");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetColumns2(string Datasource)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Report_GetColumns", conn);
            cmd.Parameters.AddWithValue("@DataSource", Datasource);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Columns");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int InsertReport(string ReportName, string ReportDescription, string CreatedBy, string TableName)
        {
            int ReportID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_coach_insertReport", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportName", ReportName);
            cmd.Parameters.AddWithValue("@ReportDescription", ReportDescription);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@TableName", TableName);
            conn.Open();
            ReportID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportID;
        }

        public void InsertReportSpecific(int ReportID, string ColumnName)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_coach_insertReportSpecifics", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            cmd.Parameters.AddWithValue("@ColumnName", ColumnName);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public int InsertintoReportTable(string ReportName, string CreatedBy)
        {
            int ReportID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportTable", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportName", ReportName);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            conn.Open();
            ReportID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportID;
        }

        public int InsertintoReportDataSource(int ReportID, string DataSource, string DataSourceID)
        {
            int ReportIDDataSource = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportDataSource", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            cmd.Parameters.AddWithValue("@DataSource", DataSource);
            cmd.Parameters.AddWithValue("@DataSourceID", DataSourceID);
            conn.Open();
            ReportIDDataSource = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportIDDataSource;
        }

        public void InsertintoReportColumns(int ReportIDDataSource, string ColumnName)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportColumns", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportIDDataSource", ReportIDDataSource);
            cmd.Parameters.AddWithValue("@ColumnName", ColumnName);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public DataSet GetSavedReports(int CreatedBy)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSavedReports", conn);
            cmd.Parameters.AddWithValue("@Createdby", CreatedBy);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SavedReports");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public DataSet GenerateSavedReport(string ReportID,string User)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GenReport", conn);
            cmd.Parameters.AddWithValue("@ReportID", Convert.ToInt32(ReportID));
            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(User));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "GSavedReports");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public DataSet GetReportColumns(string ReportID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetReportColumns", conn);
            cmd.Parameters.AddWithValue("@ReportID", Convert.ToInt32(ReportID));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "GReportColumns");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataTable GetReportColumnsDataTable(string ReportID)
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetReportColumns", conn);
            cmd.Parameters.AddWithValue("@ReportID", Convert.ToInt32(ReportID));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }
        public void UpdateIntoReportTable(int ReportId, string ReportName)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateintoReportTable", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportName", ReportName);
            cmd.Parameters.AddWithValue("@ReportID", ReportId);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public int UpdateintoReportDataSource(int ReportID, string DataSource, string DataSourceID)
        {
            int ReportIDDataSource = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateintoReportDataSource", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            cmd.Parameters.AddWithValue("@DataSource", DataSource);
            cmd.Parameters.AddWithValue("@DataSourceID", DataSourceID);
            conn.Open();
            ReportIDDataSource = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportIDDataSource;
        }
        public void UpdateintoReportColumns(int ReportIDDataSource, string ColumnName, int ReportID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateintoReportColumns", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportIDDataSource", ReportIDDataSource);
            cmd.Parameters.AddWithValue("@ColumnName", ColumnName);
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public DataSet GetReportName(string ReportID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetReportName", conn);
            cmd.Parameters.AddWithValue("@ReportID", Convert.ToInt32(ReportID));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "GReportName");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public void DeleteintoReportColumns(int ReportID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_DeleteintoReportColumns", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public int InsertReportNew(string ReportName,string CreatedBy,string DateRangeFrom ,string DateRangeTo,int IsExported,int isSaved, int ReportType)
        {
            int ReportID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportTableNew", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportName", ReportName);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@DateRangeFrom", DateRangeFrom);
            cmd.Parameters.AddWithValue("@DateRangeTo", DateRangeTo);
            cmd.Parameters.AddWithValue("@IsExported", IsExported);
            cmd.Parameters.AddWithValue("@isSaved", isSaved);
            cmd.Parameters.AddWithValue("@ReportType", ReportType);
            conn.Open();
            ReportID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportID;

        }

        public int InsertintoReportDataSourceNew(int ReportID, string DataSourceID)
        {
            int ReportIDDataSource = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportDataSourceNew", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            cmd.Parameters.AddWithValue("@DataSourceID", DataSourceID);
            conn.Open();
            ReportIDDataSource = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReportIDDataSource;
        }
        public DataSet GenerateSavedReportNew(string ReportID, string User)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GenReportNew", conn);
            cmd.Parameters.AddWithValue("@ReportID", Convert.ToInt32(ReportID));
            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(User));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.CommandTimeout = 6000;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "GSavedReports");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void UpdateReportStatus(int ReportID, int Status)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateReportStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            cmd.Parameters.AddWithValue("@Status", Status);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public DataSet GenerateRecentlyUsedReports(string User,int Type)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetRecentlyUsedReports", conn);
            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(User));
            cmd.Parameters.AddWithValue("@Type", Convert.ToInt32(Type));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "RUsedReports");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void InsertintoReportColumnsNew(int ReportID, string ColumnName, int DataSourceID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertintoReportColumnsNew", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DataSourceID", DataSourceID);
            cmd.Parameters.AddWithValue("@ColumnName", ColumnName);
            cmd.Parameters.AddWithValue("@ReportID", ReportID);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public DataSet GenerateRecentlyUsedReports2(string User, int Type)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetRecentlyUsedReports2", conn);
            cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(User));
            cmd.Parameters.AddWithValue("@Type", Convert.ToInt32(Type));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "RUsedReports");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet CheckIfReportNameExists(string ReportName, string User, int Type)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetReportNameIfExists", conn);
            cmd.Parameters.AddWithValue("@ReportName", ReportName);
            cmd.Parameters.AddWithValue("@UserID", User);
            cmd.Parameters.AddWithValue("@ReportType", Type);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CheckExists");
            conn.Close();
            conn.Dispose();
            return ds;
        }
    }

}