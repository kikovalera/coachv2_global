﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2.AppCode
{
    public class EmailNotifs
    {
        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "RASConnectionString";

        public EmailNotifs()
        {
            ConnectionStringSettings dbConns;
            dbConns = ConfigurationManager.ConnectionStrings[CONNSTR];
            strConn = dbConns.ConnectionString;
            conn = new SqlConnection(strConn);
        }

        public bool sendNotification(string subject, string toName, string toEmail, string fromName, string fromEmail, string htmlBody, string body)
        {
            bool retVal = true;
            SqlCommand cmd = null;
            cmd = new SqlCommand("susl3psqldb05.postoffice.dbo.pr_PostOffice_Ins_Email", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 1800;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ApplicationID", SqlDbType.Int);
            cmd.Parameters.Add("@Subject", SqlDbType.VarChar);
            cmd.Parameters.Add("@ToName", SqlDbType.VarChar);
            cmd.Parameters.Add("@ToEmail", SqlDbType.VarChar);
            cmd.Parameters.Add("@FromName", SqlDbType.VarChar);
            cmd.Parameters.Add("@FromEmail", SqlDbType.VarChar);
            cmd.Parameters.Add("@Body", SqlDbType.VarChar);
            cmd.Parameters.Add("@HTMLBody", SqlDbType.VarChar);
            cmd.Parameters["@ApplicationID"].Value = 2;
            cmd.Parameters["@Subject"].Value = subject;
            cmd.Parameters["@ToName"].Value = toName;
            cmd.Parameters["@ToEmail"].Value = toEmail;
            cmd.Parameters["@FromName"].Value = fromName;
            cmd.Parameters["@FromEmail"].Value = fromEmail;
            cmd.Parameters["@Body"].Value = body;
            cmd.Parameters["@HTMLBody"].Value = htmlBody;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return retVal;
        }

       
    }
}