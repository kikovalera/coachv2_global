﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2.AppCode
{
    public class DataAccessGenericWeb
    {
        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "GENERICWEBConnectionString";

        public DataAccessGenericWeb()
         {
            ConnectionStringSettings dbConns;
            dbConns = ConfigurationManager.ConnectionStrings[CONNSTR];
            strConn = dbConns.ConnectionString;
            conn = new SqlConnection(strConn);
        }

    

        public DataSet GetCompanySites(int CompanyGroup)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_rst_CompanySite", conn);
            cmd.Parameters.AddWithValue("@CompanyGroupID", CompanyGroup);
            cmd.Parameters.AddWithValue("@CountryIDs", string.Empty);
            cmd.Parameters.AddWithValue("@GetAll", 2);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CompanySites");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetCampaigns(int CompanyGroup, int CimNumber, int ClientID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_lkp_campaignMassTicket", conn);
            cmd.Parameters.AddWithValue("@CompanyGroupID", CompanyGroup);
            cmd.Parameters.AddWithValue("@CIMNumber", CimNumber);
            cmd.Parameters.AddWithValue("@FilterBy", 1);
            cmd.Parameters.AddWithValue("@ClientID", ClientID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Campaigns");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetClients()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_lkp_client", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Clients");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetTalktalkCampaigns(int? CompanyGroup, int? FilterBy, int? ClientID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_lkp_campaign", conn);
            cmd.Parameters.AddWithValue("@CompanyGroupID", CompanyGroup);
            cmd.Parameters.AddWithValue("@FilterBy", FilterBy);
            cmd.Parameters.AddWithValue("@ClientID", ClientID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Campaigns");
            conn.Close();
            conn.Dispose();
            return ds;
        }
    }
}