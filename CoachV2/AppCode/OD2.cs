﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.AppCode
{
    [Serializable]
    public class OD2
    {
        public Guid ODID { get; set; }
        public int ID { get; set; }
        public int KPIID { get; set; }
        public string Name { get; set; }
        public string DataViewValue { get; set; }
        public string Target { get; set; }
        public string Current { get; set; }
        public string Previous { get; set; }
        public string Change { get; set; }
        public string Driver { get; set; }
        public string Behavior { get; set; }
        public string RootCause { get; set; }
    }
}