﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CoachV2.AppCode
{

    public class DataAccess
    {
        string strConn = "";
        SqlConnection conn = null;
        const string CONNSTR = "cn_CoachV2";

        public DataAccess()
        {
            ConnectionStringSettings dbConns;
            dbConns = ConfigurationManager.ConnectionStrings[CONNSTR];
            strConn = dbConns.ConnectionString;
            conn = new SqlConnection(strConn);
        }

        public DataSet GetSessionTypes()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_sessiontype", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SessionTypes");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetSessionTopics(int SessionID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_session_topic", conn);
            cmd.Parameters.AddWithValue("@SessionID", SessionID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SessionTopics");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetSessionTopics2(int SessionID,int CimNumber,Boolean isremote=false)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_session_topic2", conn);
            cmd.Parameters.AddWithValue("@SessionID", SessionID);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            if (isremote) {
                cmd.Parameters.AddWithValue("@isremote", 1);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SessionTopics");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetSessionTopics3(int SessionID,int CimNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_session_topic3", conn);
            cmd.Parameters.AddWithValue("@SessionID", SessionID);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SessionTopics");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPIList()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_KPIList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIList");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPIListPerUser(int Account)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_CoachGetKPIforuser", conn);
            cmd.Parameters.AddWithValue("@acct", Account);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIListPerUser");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPIListPerAcct(string Account)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetDriversforKPIS_PER_ACCT", conn);
            cmd.Parameters.AddWithValue("@accountid", Account);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIListPerUser");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPITarget(int KPIID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_KPIData_perKPI", conn);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIData");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPITargets(int KPIID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_KPIData_perKPI_acctbased", conn);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIData");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPIDrivers(int KPIID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_KPIDrivers_perKPI", conn);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIDrivers_perKPI");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetKPIDriversfromlist(int KPIID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_KPIDrivers_foreachKPI", conn);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIDrivers_perKPI");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataTable GetSubordinates(int CimNumber)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinates", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataTable GetSubordinatesWithAccounts(int CimNumber,int AccountID)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesUsingAccountID", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataTable GetSubordinatesWithAccounts3(int CimNumber, int AccountID)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesUsingAccountID2", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataSet GetSubordinatesWithAccounts4(int CimNumber, int AccountID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesUsingAccountID2", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "EmpInf");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetSubordinatesWithTeam(int CimNumber, int Account)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesWithTeam", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@AccountID", Account);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SubordinatesWithTeam");
            conn.Close();
            conn.Dispose();
            return ds;
         }

        public DataSet GetEmployeeInfo(int CIMNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetCimNumberDetails", conn);
            cmd.Parameters.AddWithValue("@CimNum", CIMNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "EmpInf");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetCoachingNotes(int CIMNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_CoachingNotesPerCim", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingNotes");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetPerformanceResults(int CIMNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_PerformanceResultsPerCim", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "PerformanceResults");
            conn.Close();
            conn.Dispose();
            return ds;
        }


        public int InsertReview(int CoacheeID,int AccountID,int SupervisorID,int TopicID,DateTime? FollowupDate,string Description,string Strengths,string Opportunity,string Commitment,int Signed,int CreatedBy,int ReviewType,int Status)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@FollowupDate", FollowupDate);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            cmd.Parameters.AddWithValue("@Signed", Signed);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@Status", Status);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public void InsertReviewKPI(int ReviewID, int KPIID, string Target, string Current, string Previous, int DriverID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_reviewkpi", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.Parameters.AddWithValue("@Target", Target);
            cmd.Parameters.AddWithValue("@Current", Current);
            cmd.Parameters.AddWithValue("@Previous", Previous);
            cmd.Parameters.AddWithValue("@DriverID", DriverID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public void InsertReviewInc(int ReviewID,int ReviewHistoryID, int InclusionType)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_inc", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@ReviewHistoryID", ReviewHistoryID);
            cmd.Parameters.AddWithValue("@InclusionType", InclusionType);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public void UpdateReview(int ReviewID, int Signed)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_update_review", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@Signed", Signed);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }


        public int CheckSSN(int CIMNumber, int SSN)
        {
            int retval;
            SqlCommand cmd = null;
            cmd = new SqlCommand("CheckSSN", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIMNumber", CIMNumber);
            cmd.Parameters.AddWithValue("@SSN", SSN);
            cmd.Parameters.Add("@RetVal", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            retval = Convert.ToInt32(cmd.Parameters["@RetVal"].Value);
            return retval;
        }


        public int InsertReviewStaging(int CoacheeID, int AccountID, int SupervisorID, int TopicID, DateTime? FollowupDate, string Description, string Strengths, string Opportunity, string Commitment, int Signed, int CreatedBy, int ReviewType, int Status)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@FollowupDate", FollowupDate);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            cmd.Parameters.AddWithValue("@Signed", Signed);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@Status", Status);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public DataSet GetTeamLeaderPerAccount(int AccountID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetTeamLeaderPerAccount", conn);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "TLPerAccount");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetTeamLeaderPerAccount2(int AccountID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetTeamLeaderPerAccount2", conn);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "TLPerAccount");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int InsertReviewQA(int CoacheeID,int AccountID,int SupervisorID,int TopicID,string Description,string Strengths,string Opportunity,int CreatedBy,int Status, int AssignmentType)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_qareview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@AssignmentType", AssignmentType);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public DataSet GetKPIReview(int ReviewID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_review_kpi", conn);
            cmd.Parameters.AddWithValue("@ID", ReviewID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ReviewKPI");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int InsertRemoteCoaching(int CoacheeID,int AccountID,int SupervisorID,int TopicID,string CoacherFeedback,int CreatedBy, bool RequireSignOff)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_remotereview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@CoacherFeedback", CoacherFeedback);
            //cmd.Parameters.AddWithValue("@CoacheeFeedback", CoacheeFeedback);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@RequiredSignOff", RequireSignOff == false ? 0 : 1);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public DataSet GetAssignmentType()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAssignmentType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "AssignmentType");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetAssignedReviewsData(int ReviewID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAssignedReviewsData", conn);
            cmd.Parameters.AddWithValue("@Id", ReviewID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "AssignedReviewsData");
            conn.Close();
            conn.Dispose();
            return ds;
        }


        public void UpdateTLCommitment(int Id, string Commitment)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateTLCommitment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id",Id);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public void UpdateRemoteCoachFeedback(int ReviewID, int CimNumber, string Feedback)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateRemoteCoachFeedback", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewId", ReviewID);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@Feedback", Feedback);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        //public DataSet GetRemoteCoachFeedback(int ReviewId, int FeedbackRole)
        //{
        //    DataSet ds = new DataSet();
        //    SqlDataAdapter da = new SqlDataAdapter();
        //    SqlCommand cmd = null;
        //    cmd = new SqlCommand("pr_Coach_GetRemoteCoachFeedback", conn);
        //    cmd.Parameters.AddWithValue("@ReviewId", ReviewId);
        //    cmd.Parameters.AddWithValue("@FeedbackRole", FeedbackRole);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    conn.Open();
        //    da.SelectCommand = cmd;
        //    cmd.ExecuteNonQuery();
        //    da.Fill(ds, "RemoteCoachFeedback");
        //    conn.Close();
        //    conn.Dispose();
        //    return ds;
        //}

        public DataTable GetRemoteCoachFeedback(int ReviewId, int FeedbackRole)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetRemoteCoachFeedback", conn);
            cmd.Parameters.AddWithValue("@ReviewId", ReviewId);
            cmd.Parameters.AddWithValue("@FeedbackRole", FeedbackRole);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataSet GetActiveAccounts()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_getActiveAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ActiveAccounts");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void UpdateReviewForRemote(int ReviewID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_signoff_remotereview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public int GetCompanyGroup(int CIMNumber, int CIMNumber2)
        {
            DataSet ds = new DataSet();
            int retval;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAndCompareCompanyGroup", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIMNumber1", CIMNumber);
            cmd.Parameters.AddWithValue("@CIMNumber2", CIMNumber2);
            cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            retval = Convert.ToInt32(cmd.Parameters["@Result"].Value);
            return retval;
        }

        public int InsertMassCoachingAll(int MassCoachingType,string Description,string Agenda,int TopicID,int CreatedBy, bool IsRequired)
         {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_massreview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MassCoachingType", MassCoachingType);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Agenda", Agenda);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@RequiredSignOff", IsRequired == true ? 1 : 0);
            conn.Open();
            int MassReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return MassReviewID;
        }

        public int InsertMassCoachingGroups(int MassCoachingType, string Description, string Agenda, int TopicID, int CreatedBy, int? SiteID, int? ClientID, int? CampaignID, bool IsRequired)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_massreview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MassCoachingType", MassCoachingType);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Agenda", Agenda);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@ClientID",ClientID);
            cmd.Parameters.AddWithValue("@CampaignID",CampaignID);
            if(HttpContext.Current.Session["Role"].ToString()=="TL")
                cmd.Parameters.AddWithValue("@TeamLeaderID", CreatedBy);
            cmd.Parameters.AddWithValue("@RequiredSignOff", IsRequired == true ? 1 : 0);
            conn.Open();
            int MassReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return MassReviewID;
        }

        public int InsertMassCoachingSelected(int MassCoachingType, string Description, string Agenda, int TopicID, int CreatedBy, int CoacheeID, bool IsRequired)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_massreview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MassCoachingType", MassCoachingType);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Agenda", Agenda);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@RequiredSignOff", IsRequired == true ? 1 : 0);
            conn.Open();
            int MassReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return MassReviewID;
        }


        public void InsertMassCoachingSelectedPerCoachee(int MassCoachingType, string Description, string Agenda, int TopicID, int CreatedBy, int CoacheeID,int MassReviewID, bool isRequired)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_massreviewselected", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MassCoachingType", MassCoachingType);
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Agenda", Agenda);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@new_identity", MassReviewID);
            cmd.Parameters.AddWithValue("@RequiredSignOff", isRequired == true ? 1 : 0);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public DataTable CheckIfSubordinate(int CimNumber)
        {
            //DataTable ds = new DataTable();
            //SqlDataAdapter da = new SqlDataAdapter();
            //SqlCommand cmd = null;
            //cmd = new SqlCommand("pr_Coach_GetAllSubordinates", conn);
            //cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            //cmd.CommandType = CommandType.StoredProcedure;
            //conn.Open();
            //da.SelectCommand = cmd;
            //cmd.ExecuteNonQuery();
            //da.Fill(ds, "Subordinates");
            //conn.Close();
            //conn.Dispose();
            //return ds;

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAllSubordinates", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataTable GetIncHistoryDataTable(int ReviewID, int Type)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_INC_History", conn);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        public DataSet GetMassCoachingDetails(int CoachingTicket)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetMassCoachingDetails", conn);
            cmd.Parameters.AddWithValue("@CoachingID", CoachingTicket);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "MassCoachingTicket");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void InsertUploadedDocumentation(int ReviewID, string DocumentName, int UploadedBy, string FilePath)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("InsertUploadedDocumentation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@DocumentName", DocumentName);
            cmd.Parameters.AddWithValue("@UploadedBy", UploadedBy);
            cmd.Parameters.AddWithValue("@FilePath", FilePath);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public DataSet GetUploadedDocuments(int ReviewID,int Type)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetUploadedDocuments", conn);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Uploaded");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetCMTDropdowns(int DropdownType, int MajorID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_coach_getCMTdropdowns", conn);
            cmd.Parameters.AddWithValue("@DropdownType", DropdownType);
            cmd.Parameters.AddWithValue("@MajorID", MajorID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CMTDropdowns");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int InsertCMTReview(int CoacheeID,int AccountID,int SupervisorID,int TopicID,int CreatedBy,int SearchBlackout,string HRComments
        ,int? HRCIM,
        string ReferenceNumber, int? CaseLevel, int? MajorCategory, int? InfractionClass, int? Status, DateTime? NTESubmittedDate, DateTime? NTEApprovalDate, DateTime? NTEIssueDate,
        int? HoldCase, DateTime? HoldStartDate, DateTime? HoldEndDate, int? HoldCaseType, string HRSuperiorNotes, int? EmployeeAccepted, DateTime? EmployeeSignDate, int? Witness1,
        DateTime? Witness1SignDate, int? Witness2, DateTime? Witness2SignDate, int? NonAcceptanceReason, string AcceptanceNotes, DateTime? RTNTEReceiveDate
        , int? PWithSuspension,DateTime? PSchedule1, DateTime? PSchedule2, DateTime? PAdminHearingDate
        , int CheckBoxChoice, DateTime? NODSubmissionDate, DateTime? NODApprovalDate, DateTime? NODIssueDate,
        int? NODHoldCase, DateTime? NODHoldStartDate, DateTime? NODHoldEndDate, int? NODHoldCaseType, string NODHRSuperiorNotes, int? NODEmployeeAccepted, DateTime? NODEmployeeSignDate,
        int? NODWitness1, DateTime? NODWitness1SignDate, int? NODWitness2, DateTime? NODWitness2SignDate, int? NODNonAcceptanceReason, string NODAcceptanceNotes, DateTime? NODReturnDate,
        int? NODEscalatedBy, int? NODCaseDecision, string NODCaseDecisionNotes, string NODEOWNotes, int? EoWAction)
        {

            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_InsertCMTReview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID",CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID",AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID",SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID",TopicID);
	        cmd.Parameters.AddWithValue("@CreatedBy",CreatedBy);
            cmd.Parameters.AddWithValue("@SearchBlackout",SearchBlackout);
            if (HRComments.Length == 0)
            {
                cmd.Parameters.AddWithValue("@HRComments", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRComments", HRComments);
            }

            if (HRCIM == 0)
            {
                cmd.Parameters.AddWithValue("@HRCIM", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRCIM", HRCIM);
            }

            if (ReferenceNumber.Length == 0)
            {

                cmd.Parameters.AddWithValue("@ReferenceNumber", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ReferenceNumber", ReferenceNumber);
            }

            if (CaseLevel == 0)
            {
                cmd.Parameters.AddWithValue("@CaseLevel", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CaseLevel", CaseLevel);            
            }

            if (MajorCategory == 0)
            {
                cmd.Parameters.AddWithValue("@MajorCategory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@MajorCategory", MajorCategory);
            }

            if (InfractionClass == 0)
            {
                cmd.Parameters.AddWithValue("@InfractionClass", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@InfractionClass", InfractionClass);
            }

            if (Status == 0)
            {
                cmd.Parameters.AddWithValue("@Status", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Status", Status);
            }
            
            cmd.Parameters.AddWithValue("@NTESubmittedDate", NTESubmittedDate);
            cmd.Parameters.AddWithValue("@NTEApprovalDate", NTEApprovalDate);
            cmd.Parameters.AddWithValue("@NTEIssueDate", NTEIssueDate);

            if (HoldCase == 0)
            {
                cmd.Parameters.AddWithValue("@HoldCase", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HoldCase", HoldCase);
            }

            cmd.Parameters.AddWithValue("@HoldStartDate", HoldStartDate);
            cmd.Parameters.AddWithValue("@HoldEndDate", HoldEndDate);

            if (HoldCaseType == 0)
            {
                cmd.Parameters.AddWithValue("@HoldCaseType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HoldCaseType", HoldCaseType);
            }

            if (HRSuperiorNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@HRSuperiorNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRSuperiorNotes", HRSuperiorNotes);
            }

            if (EmployeeAccepted == 0)
            {
                cmd.Parameters.AddWithValue("@EmployeeAccepted", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@EmployeeAccepted", EmployeeAccepted);
            }

            cmd.Parameters.AddWithValue("@EmployeeSignDate", EmployeeSignDate);

            if (Witness1 == 0)
            {
                cmd.Parameters.AddWithValue("@Witness1", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Witness1", Witness1);
            }

            cmd.Parameters.AddWithValue("@Witness1SignDate", Witness1SignDate);

            if (Witness2 == 0)
            {
                cmd.Parameters.AddWithValue("@Witness2", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Witness2", Witness2);
            }

            cmd.Parameters.AddWithValue("@Witness2SignDate", Witness2SignDate);

            if (NonAcceptanceReason == 0)
            {
                cmd.Parameters.AddWithValue("@NonAcceptanceReason", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NonAcceptanceReason", NonAcceptanceReason);
            }

            if (AcceptanceNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@AcceptanceNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@AcceptanceNotes", AcceptanceNotes);
            }
          
            cmd.Parameters.AddWithValue("@RTNTEReceiveDate", RTNTEReceiveDate);

	        //Preventive
            if (PWithSuspension == 0)
            {
                cmd.Parameters.AddWithValue("@PWithSuspension", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PWithSuspension", PWithSuspension);
            }
            cmd.Parameters.AddWithValue("@PSchedule1", PSchedule1);
            cmd.Parameters.AddWithValue("@PSchedule2", PSchedule2);
            cmd.Parameters.AddWithValue("@PAdminHearingDate", PAdminHearingDate);
	
	        //NOD Details
            if (CheckBoxChoice == 0)
            {
                cmd.Parameters.AddWithValue("@CheckBox", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CheckBox", CheckBoxChoice);
            }
            cmd.Parameters.AddWithValue("@NODSubmissionDate", NODSubmissionDate);
            cmd.Parameters.AddWithValue("@NODApprovalDate", NODApprovalDate);
            cmd.Parameters.AddWithValue("@NODIssueDate", NODIssueDate);

            if (NODHoldCase == 0)
            {
                cmd.Parameters.AddWithValue("@NODHoldCase", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHoldCase", NODHoldCase);
            }

            cmd.Parameters.AddWithValue("@NODHoldStartDate", NODHoldStartDate);
            cmd.Parameters.AddWithValue("@NODHoldEndDate", NODHoldEndDate);

            if (NODHoldCaseType == 0)
            {
                cmd.Parameters.AddWithValue("@NODHoldCaseType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHoldCaseType", NODHoldCaseType);
            }

            if (NODHRSuperiorNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODHRSuperiorNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHRSuperiorNotes", NODHRSuperiorNotes);
            }

            if (NODEmployeeAccepted == 0)
            {
                cmd.Parameters.AddWithValue("@NODEmployeeAccepted", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEmployeeAccepted", NODEmployeeAccepted);
            }

            cmd.Parameters.AddWithValue("@NODEmployeeSignDate", NODEmployeeSignDate);

            if (NODWitness1 == 0)
            {
                cmd.Parameters.AddWithValue("@NODWitness1", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODWitness1", NODWitness1);
            }

            cmd.Parameters.AddWithValue("@NODWitness1SignDate", NODWitness1SignDate);

            if (NODWitness2 == 0)
            {
                cmd.Parameters.AddWithValue("@NODWitness2", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODWitness2", NODWitness2);
            }

            cmd.Parameters.AddWithValue("@NODWitness2SignDate", NODWitness2SignDate);

            if (NonAcceptanceReason == 0)
            {
                cmd.Parameters.AddWithValue("@NODNonAcceptanceReason", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODNonAcceptanceReason", NODNonAcceptanceReason);
            }

            if (NODAcceptanceNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODAcceptanceNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODAcceptanceNotes", NODAcceptanceNotes);
            }

            cmd.Parameters.AddWithValue("@NODReturnDate", NODReturnDate);

            if (NODEscalatedBy == 0)
            {
                cmd.Parameters.AddWithValue("@NODEscalatedBy", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEscalatedBy", NODEscalatedBy);
            }

            if (NODCaseDecision == 0)
            {
                cmd.Parameters.AddWithValue("@NODCaseDecision", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODCaseDecision", NODCaseDecision);
            }

            if (NODCaseDecisionNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODCaseDecisionNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODCaseDecisionNotes", NODCaseDecisionNotes);
            }

            if (NODEOWNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODEOWNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEOWNotes", NODEOWNotes);
            }
            if (EoWAction == 0)
            {
                cmd.Parameters.AddWithValue("@EoWAction", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@EoWAction", EoWAction);
            }
            conn.Open();
            int CMTReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return CMTReviewID;
        }


      public void UpdateCMTReview(int ReviewID,int SearchBlackout,string HRComments , int? HRCIM,string ReferenceNumber, int? CaseLevel, int? MajorCategory, int? InfractionClass, int? Status, DateTime? NTESubmittedDate, DateTime? NTEApprovalDate, DateTime? NTEIssueDate,
      int? HoldCase, DateTime? HoldStartDate, DateTime? HoldEndDate, int? HoldCaseType, string HRSuperiorNotes, int? EmployeeAccepted, DateTime? EmployeeSignDate, int? Witness1,
      DateTime? Witness1SignDate, int? Witness2, DateTime? Witness2SignDate, string NonAcceptanceReason, string AcceptanceNotes, DateTime? RTNTEReceiveDate
      , int? PWithSuspension, DateTime? PSchedule1, DateTime? PSchedule2, DateTime? PAdminHearingDate
      , int CheckBoxChoice, DateTime? NODSubmissionDate, DateTime? NODApprovalDate, DateTime? NODIssueDate,
      int? NODHoldCase, DateTime? NODHoldStartDate, DateTime? NODHoldEndDate, int? NODHoldCaseType, string NODHRSuperiorNotes, int? NODEmployeeAccepted, DateTime? NODEmployeeSignDate,
      int? NODWitness1, DateTime? NODWitness1SignDate, int? NODWitness2, DateTime? NODWitness2SignDate, string NODNonAcceptanceReason, string NODAcceptanceNotes, DateTime? NODReturnDate,
      int? NODEscalatedBy, int? NODCaseDecision, string NODCaseDecisionNotes, string NODEOWNotes, int? EoWAction)
        {

            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateCMTReview", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SearchBlackout", SearchBlackout);
            if (HRComments.Length == 0)
            {
                cmd.Parameters.AddWithValue("@HRComments", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRComments", HRComments);
            }

            if (HRCIM == 0)
            {
                cmd.Parameters.AddWithValue("@HRCIM", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRCIM", HRCIM);
            }

            if (ReferenceNumber.Length == 0)
            {

                cmd.Parameters.AddWithValue("@ReferenceNumber", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ReferenceNumber", ReferenceNumber);
            }

            if (CaseLevel == 0)
            {
                cmd.Parameters.AddWithValue("@CaseLevel", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CaseLevel", CaseLevel);
            }

            if (MajorCategory == 0)
            {
                cmd.Parameters.AddWithValue("@MajorCategory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@MajorCategory", MajorCategory);
            }

            if (InfractionClass == 0)
            {
                cmd.Parameters.AddWithValue("@InfractionClass", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@InfractionClass", InfractionClass);
            }

            if (Status == 0)
            {
                cmd.Parameters.AddWithValue("@Status", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Status", Status);
            }

            cmd.Parameters.AddWithValue("@NTESubmittedDate", NTESubmittedDate);
            cmd.Parameters.AddWithValue("@NTEApprovalDate", NTEApprovalDate);
            cmd.Parameters.AddWithValue("@NTEIssueDate", NTEIssueDate);

            if (HoldCase == 0)
            {
                cmd.Parameters.AddWithValue("@HoldCase", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HoldCase", HoldCase);
            }

            cmd.Parameters.AddWithValue("@HoldStartDate", HoldStartDate);
            cmd.Parameters.AddWithValue("@HoldEndDate", HoldEndDate);

            if (HoldCaseType == 0)
            {
                cmd.Parameters.AddWithValue("@HoldCaseType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HoldCaseType", HoldCaseType);
            }

            if (HRSuperiorNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@HRSuperiorNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@HRSuperiorNotes", HRSuperiorNotes);
            }

            if (EmployeeAccepted == 0)
            {
                cmd.Parameters.AddWithValue("@EmployeeAccepted", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@EmployeeAccepted", EmployeeAccepted);
            }

            cmd.Parameters.AddWithValue("@EmployeeSignDate", EmployeeSignDate);

            if (Witness1 == 0)
            {
                cmd.Parameters.AddWithValue("@Witness1", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Witness1", Witness1);
            }

            cmd.Parameters.AddWithValue("@Witness1SignDate", Witness1SignDate);

            if (Witness2 == 0)
            {
                cmd.Parameters.AddWithValue("@Witness2", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Witness2", Witness2);
            }

            cmd.Parameters.AddWithValue("@Witness2SignDate", Witness2SignDate);

            if (NonAcceptanceReason.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NonAcceptanceReason", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NonAcceptanceReason", NonAcceptanceReason);
            }

            if (AcceptanceNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@AcceptanceNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@AcceptanceNotes", AcceptanceNotes);
            }

            cmd.Parameters.AddWithValue("@RTNTEReceiveDate", RTNTEReceiveDate);

            //Preventive
            if (PWithSuspension == 0)
            {
                cmd.Parameters.AddWithValue("@PWithSuspension", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PWithSuspension", PWithSuspension);
            }
            cmd.Parameters.AddWithValue("@PSchedule1", PSchedule1);
            cmd.Parameters.AddWithValue("@PSchedule2", PSchedule2);
            cmd.Parameters.AddWithValue("@PAdminHearingDate", PAdminHearingDate);

            //NOD Details
            if (CheckBoxChoice == 0)
            {
                cmd.Parameters.AddWithValue("@CheckBox", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CheckBox", CheckBoxChoice);
            }
            cmd.Parameters.AddWithValue("@NODSubmissionDate", NODSubmissionDate);
            cmd.Parameters.AddWithValue("@NODApprovalDate", NODApprovalDate);
            cmd.Parameters.AddWithValue("@NODIssueDate", NODIssueDate);

            if (NODHoldCase == 0)
            {
                cmd.Parameters.AddWithValue("@NODHoldCase", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHoldCase", NODHoldCase);
            }

            cmd.Parameters.AddWithValue("@NODHoldStartDate", NODHoldStartDate);
            cmd.Parameters.AddWithValue("@NODHoldEndDate", NODHoldEndDate);

            if (NODHoldCaseType == 0)
            {
                cmd.Parameters.AddWithValue("@NODHoldCaseType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHoldCaseType", NODHoldCaseType);
            }

            if (NODHRSuperiorNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODHRSuperiorNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODHRSuperiorNotes", NODHRSuperiorNotes);
            }

            if (NODEmployeeAccepted == 0)
            {
                cmd.Parameters.AddWithValue("@NODEmployeeAccepted", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEmployeeAccepted", NODEmployeeAccepted);
            }

            cmd.Parameters.AddWithValue("@NODEmployeeSignDate", NODEmployeeSignDate);

            if (NODWitness1 == 0)
            {
                cmd.Parameters.AddWithValue("@NODWitness1", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODWitness1", NODWitness1);
            }

            cmd.Parameters.AddWithValue("@NODWitness1SignDate", NODWitness1SignDate);

            if (NODWitness2 == 0)
            {
                cmd.Parameters.AddWithValue("@NODWitness2", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODWitness2", NODWitness2);
            }

            cmd.Parameters.AddWithValue("@NODWitness2SignDate", NODWitness2SignDate);

            if (NonAcceptanceReason.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODNonAcceptanceReason", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODNonAcceptanceReason", NODNonAcceptanceReason);
            }

            if (NODAcceptanceNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODAcceptanceNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODAcceptanceNotes", NODAcceptanceNotes);
            }

            cmd.Parameters.AddWithValue("@NODReturnDate", NODReturnDate);

            if (NODEscalatedBy == 0)
            {
                cmd.Parameters.AddWithValue("@NODEscalatedBy", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEscalatedBy", NODEscalatedBy);
            }

            if (NODCaseDecision == 0)
            {
                cmd.Parameters.AddWithValue("@NODCaseDecision", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODCaseDecision", NODCaseDecision);
            }

            if (NODCaseDecisionNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODCaseDecisionNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODCaseDecisionNotes", NODCaseDecisionNotes);
            }

            if (NODEOWNotes.Length == 0)
            {
                cmd.Parameters.AddWithValue("@NODEOWNotes", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEOWNotes", NODEOWNotes);
            }
            if (EoWAction == 0)
            {
                cmd.Parameters.AddWithValue("@NODEoWAction", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NODEoWAction", EoWAction);
            }
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

      public DataSet GetCMTReview(int ReviewID)
      {
          DataSet ds = new DataSet();
          SqlDataAdapter da = new SqlDataAdapter();
          SqlCommand cmd = null;
          cmd = new SqlCommand("pr_Coach_GetCMTReview", conn);
          cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
          cmd.CommandType = CommandType.StoredProcedure;
          conn.Open();
          da.SelectCommand = cmd;
          cmd.ExecuteNonQuery();
          da.Fill(ds, "Uploaded");
          conn.Close();
          conn.Dispose();
          return ds;

      }

      public string CheckIfQAHR(int CIMNumber)
      {
          string Role;
          SqlCommand cmd = null;
          cmd = new SqlCommand("pr_Coach_CheckifQAHR", conn);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@CIMNumber", CIMNumber);
          cmd.Parameters.Add("@Role", SqlDbType.NVarChar,3).Direction = ParameterDirection.Output;
          conn.Open();
          cmd.ExecuteNonQuery();
          conn.Close();
          conn.Dispose();
          cmd.Dispose();
          Role = cmd.Parameters["@Role"].Value.ToString();

          return Role;
      
      }

      public DataSet GetSessionFocus(string Account)
      {
          DataSet ds = new DataSet();
          SqlDataAdapter da = new SqlDataAdapter();
          SqlCommand cmd = null;
          cmd = new SqlCommand("pr_Coach_GetSessionFocus", conn);
          cmd.Parameters.AddWithValue("@Account", Account);
          cmd.CommandType = CommandType.StoredProcedure;
          conn.Open();
          da.SelectCommand = cmd;
          cmd.ExecuteNonQuery();
          da.Fill(ds, "SessionFocus");
          conn.Close();
          conn.Dispose();
          return ds;

      }


        public int InsertReviewNexidia(int CoacheeID,int AccountID,int SupervisorID,int TopicId,string  FollowDate,string Description,string Strengths,string Opportunity,
        int SessionFocus,string PositiveBehaviour,string OpportunityBehaviour,string BeginBehaviourComments,string  BehaviourEffects, string RootCause,string ReviewResultsComments,
        string AddressOpportunities,string ActionPlanComments,string SmartGoals,string CreatePlanComments,string FollowThrough,int CreatedBy)
         {
          SqlCommand cmd = null;
          cmd = new SqlCommand("pr_Coach_insert_review_nexidia", conn);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
          cmd.Parameters.AddWithValue("@AccountID", AccountID);
          cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
          cmd.Parameters.AddWithValue("@TopicID", TopicId);
          if (FollowDate != null)
          {
              cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
          }
          else
          {
              cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
          }
          cmd.Parameters.AddWithValue("@Description", Description);

          cmd.Parameters.AddWithValue("@Strengths", Strengths);
          cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
          if (SessionFocus == 0)
          {
              cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
          }
          else
          {
              cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
          }
          cmd.Parameters.AddWithValue("@PositiveBehaviour", PositiveBehaviour);
          cmd.Parameters.AddWithValue("@OpportunityBehaviour", OpportunityBehaviour);
          cmd.Parameters.AddWithValue("@BeginBehaviourComments", BeginBehaviourComments);

          cmd.Parameters.AddWithValue("@BehaviourEffects", BehaviourEffects);
          cmd.Parameters.AddWithValue("@RootCause", RootCause);
          cmd.Parameters.AddWithValue("@ReviewResultsComments", ReviewResultsComments);
          cmd.Parameters.AddWithValue("@AddressOpportunities", AddressOpportunities);
          cmd.Parameters.AddWithValue("@ActionPlanComments", ActionPlanComments);
          cmd.Parameters.AddWithValue("@SmartGoals", SmartGoals);

          cmd.Parameters.AddWithValue("@CreatePlanComments", CreatePlanComments);
          cmd.Parameters.AddWithValue("@FollowThrough", FollowThrough);
          cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);

          
          conn.Open();
          int NexidiaReviewID = (int)cmd.ExecuteScalar();
          conn.Close();
          conn.Dispose();
          cmd.Dispose();
          return NexidiaReviewID;
        }

        public DataSet GetIncHistory(int ReviewID,int Type)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_INC_History", conn);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "IncHistory");
            conn.Close();
            conn.Dispose();
            return ds;

        }

        public int InsertReviewOD(int CoacheeID, int AccountID, int SupervisorID, int TopicID, string FollowDate, string Description, string Strengths, string Opportunity, int SessionFocus,int CreatedBy, string CoacherFeedback, int CallID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review_od", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }
            
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            if (CallID == 0)
            {
                cmd.Parameters.AddWithValue("@CallID", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CallID", CallID);
            }


            conn.Open();
            int ODReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ODReviewID;
        }
        public void InsertReviewKPIOD(int ReviewID, int KPIID, string Target, string Current, string Previous, int DriverID,string Change,string Behaviour, string RootCause)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_reviewkpi", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.Parameters.AddWithValue("@Target", Target);
            cmd.Parameters.AddWithValue("@Current", Current);
            cmd.Parameters.AddWithValue("@Previous", Previous);
            cmd.Parameters.AddWithValue("@DriverID", DriverID);
            cmd.Parameters.AddWithValue("@Change", Change);
            cmd.Parameters.AddWithValue("@Behaviour", Behaviour);
            cmd.Parameters.AddWithValue("@RootCause", RootCause);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public DataSet GetKPIReviewOD(int ReviewID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_review_kpi_OD", conn);
            cmd.Parameters.AddWithValue("@ID", ReviewID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ReviewKPIOD");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int CheckIfOperations(int CIMNumber)
        {
            int ops;
            SqlCommand cmd = null;
            cmd = new SqlCommand("CheckIfOperations", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIMNumber", CIMNumber);
            cmd.Parameters.Add("@IsOperations", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            ops = Convert.ToInt32(cmd.Parameters["@IsOperations"].Value);

            return ops;

        }


        public int CheckIfOperationsviaAcct(int AcctId)
        {
            int ops;
            SqlCommand cmd = null;
            cmd = new SqlCommand("CheckIfOperationsviaaccountid", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@acctid", AcctId);
            cmd.Parameters.Add("@IsOperations", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            ops = Convert.ToInt32(cmd.Parameters["@IsOperations"].Value);

            return ops;

        }
        public void UpdateReviewRemote(int ReviewID, int Role)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_signoff_remote", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@Role", Role);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public DataSet CheckIfCanSignOff(int ReviewID, int FeedbackRole)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_CheckIfRemoteCanSignOff", conn);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@FeedbackRole", FeedbackRole);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CheckSignOff");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public void UpdateTLHRCommitment(int Id, string Commitment, int Cim)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_UpdateTLHRCommitment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            cmd.Parameters.AddWithValue("@UpdatedBy", Cim);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public int GetAccountType(int AccountID)
        {
            DataSet ds = new DataSet();
            int retval;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAccountType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.Add("@AccountType", SqlDbType.Int).Direction = ParameterDirection.Output;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            retval = Convert.ToInt32(cmd.Parameters["@AccountType"].Value);

            return retval;
        }
       
        public DataSet GetCoachingNotesViaCimAndST(int CIMNumber, int SessionType)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_CoachingNotesPerCimandSession", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.Parameters.AddWithValue("@SessionID", SessionType);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingNotes");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public DataSet GetCoachingNotesViaCimAndST1(int CIMNumber, int SessionType)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_CoachingNotesPerCimandSessionandreviewtype", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.Parameters.AddWithValue("@SessionID", SessionType);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingNotes");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public int getroleofloggedin_user(int cim)
        {
            int hierarchy;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetRoleofLoggedin", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cim", cim);
            conn.Open();
            cmd.ExecuteNonQuery();
            hierarchy = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return hierarchy;
        }

        public string getuserdept(int cim)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetUserDept", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cim", cim);
            conn.Open();
            cmd.ExecuteNonQuery();
            string dept = (string)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return dept;
        }
        public DataSet GetCoachLinks()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetCoachLinks", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingLinks");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void UpdateCoachLinks(int Id, string Link, int Cim, string FileName, string Description)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("UpdateCoachLinks", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", Id);
            cmd.Parameters.AddWithValue("@Link", Link);
            cmd.Parameters.AddWithValue("@UpdatedBy", Cim);
            cmd.Parameters.AddWithValue("@FileName", FileName);
            cmd.Parameters.AddWithValue("@Description", Description);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        //insert HR review
        public int InsertHRReview(int CoacheeID, int AccountID, int SupervisorID, int TopicID, DateTime? FollowupDate, string HRComments, int Assignmenttype, int CreatedBy, int ReviewType, int Status )
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_hr_review", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@FollowupDate", FollowupDate);
            cmd.Parameters.AddWithValue("@HRComments", HRComments);
            cmd.Parameters.AddWithValue("@Assignmenttype", Assignmenttype);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@Status", Status);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public void UploadAgentPerformance(string CIM, string NAME, string LOB, string DATE, string KPI, string TARGET, string SCORE, string SUPERVISORCIM)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("UploadAgentPerformance", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIM", CIM);
            cmd.Parameters.AddWithValue("@NAME", NAME);
            cmd.Parameters.AddWithValue("@LOB", LOB);
            cmd.Parameters.AddWithValue("@DATE", Convert.ToDateTime(DATE));
            cmd.Parameters.AddWithValue("@KPI", KPI);

            if (TARGET == "")
            {
                cmd.Parameters.AddWithValue("@TARGET", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@TARGET", Convert.ToDecimal(TARGET));
            }
            if (SCORE == "")
            {
                cmd.Parameters.AddWithValue("@SCORE", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SCORE", Convert.ToDecimal(SCORE));
            }
            cmd.Parameters.AddWithValue("@SUPERVISORCIM", SUPERVISORCIM);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public int InsertUploadedAgentPerformance(string DocumentName, int UploadedBy, string FilePath)
        {
            SqlCommand cmd = null;
            int UploadID = 0;
            cmd = new SqlCommand("InsertUploadedAgentPerformance", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DocumentName", DocumentName);
            cmd.Parameters.AddWithValue("@UploadedBy", UploadedBy);
            cmd.Parameters.AddWithValue("@FilePath", FilePath);
            //conn.Open();
            //cmd.ExecuteNonQuery();
            //conn.Close();
            //conn.Dispose();
            //cmd.Dispose();
            conn.Open();
            UploadID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return UploadID;
        }

        public DataSet GetCoachTypes(int CimNumber,string role)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetMassCoachingType", conn);
            cmd.Parameters.AddWithValue("@CIMNumber", CimNumber);
            cmd.Parameters.AddWithValue("@role",role);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingTypes");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void UploadAgentPerformancev2(string CIM, string NAME, string LOB, string DATE, string KPI, string TARGET, string SCORE, string SUPERVISORCIM, string NUMERATOR, string DENOMINATOR, string SUPERVISORNAME)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("UploadAgentPerformancev2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CIM", CIM);
            cmd.Parameters.AddWithValue("@NAME", NAME);
            cmd.Parameters.AddWithValue("@LOB", LOB);
            cmd.Parameters.AddWithValue("@DATE", Convert.ToDateTime(DATE));
            cmd.Parameters.AddWithValue("@KPI", KPI);

            if (TARGET == "")
            {
                cmd.Parameters.AddWithValue("@TARGET", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@TARGET", Convert.ToDecimal(TARGET));
            }
            if (SCORE == "")
            {
                cmd.Parameters.AddWithValue("@SCORE", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SCORE", Convert.ToDecimal(SCORE));
            }
            if (NUMERATOR == "")
            {
                cmd.Parameters.AddWithValue("@NUMERATOR", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@NUMERATOR", Convert.ToDecimal(NUMERATOR));
            }
            if (DENOMINATOR == "")
            {
                cmd.Parameters.AddWithValue("@DENOMINATOR", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@DENOMINATOR", Convert.ToDecimal(DENOMINATOR));
            }
            cmd.Parameters.AddWithValue("@SUPERVISORNAME", SUPERVISORNAME);
            cmd.Parameters.AddWithValue("@SUPERVISORCIM", SUPERVISORCIM);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public void UpdateKPIAdminv2(int id, int valuetype, int linkto, string createdby, int accountid, DateTime createdon, int hierarchy)
        {

            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_updateKPIv2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("id", id);
            cmd.Parameters.AddWithValue("valuetype", valuetype);
            cmd.Parameters.AddWithValue("linkto", linkto);
            cmd.Parameters.AddWithValue("createdby", createdby);
            cmd.Parameters.AddWithValue("accountid", accountid);
            cmd.Parameters.AddWithValue("createdon", createdon);
            cmd.Parameters.AddWithValue("@hierarchy", hierarchy);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();

        }

        public DataSet GetLKPKPIAccounts()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetLKPKPIAccounts", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "LKPAccounts");
            conn.Close();
            conn.Dispose();
            return ds;
        }


        public DataSet GetEmployeeInfo2(int CIMNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetCimNumberDetailsForReviews", conn);
            cmd.Parameters.AddWithValue("@CimNum", CIMNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "EmpInf");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataTable GetSubordinates2(int CimNumber)
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesForReviews", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }

        public DataTable GetSubordinatesWithAccounts2(int CimNumber, int AccountID)
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSubordinatesUsingAccountIDForReviews", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }

        public DataSet GetProfileDetails(int UserID, string Data)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetProfileDetails", conn);
            cmd.Parameters.AddWithValue("@UserInfoID", UserID);
            cmd.Parameters.AddWithValue("@Data", Data);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ProfDetails");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetPerformanceResults2(int CIMNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_PerformanceResultsPerCim2", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "PerformanceResults");
            conn.Close();
            conn.Dispose();
            return ds;
        }
        public void UpdateFormType(int ReviewID, int FormType)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("UpdateFormType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@FormType", FormType);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public DataSet GetActiveAccounts2()
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_getActiveAccount2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ActiveAccounts");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public DataSet GetSubordinatesSSO(int SupCIM, int CimNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetCoacheeDetailsViaSSO", conn);
            cmd.Parameters.AddWithValue("@SupervisorID", SupCIM);
            cmd.Parameters.AddWithValue("@CoacheeID", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SSO");
            conn.Close();
            conn.Dispose();
            return ds;

        }

        public int InsertHRReview1(int CoacheeID, int AccountID, int SupervisorID, int TopicID, DateTime? FollowupDate, string HRComments, int Assignmenttype, int CreatedBy, int ReviewType, int Status, int FormType)
        {
            int ReviewID = 0;
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_hr_review1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            cmd.Parameters.AddWithValue("@FollowupDate", FollowupDate);
            cmd.Parameters.AddWithValue("@HRComments", HRComments);
            cmd.Parameters.AddWithValue("@Assignmenttype", Assignmenttype);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ReviewType", ReviewType);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@FormType", FormType);
            conn.Open();
            ReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ReviewID;
        }

        public DataSet GetCoachingNotesViaCimAndST2(int CIMNumber, int SessionType, int ReviewID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_Get_CoachingNotesPerCimandSession3", conn);
            cmd.Parameters.AddWithValue("@CimNumber", CIMNumber);
            cmd.Parameters.AddWithValue("@SessionID", SessionType);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "CoachingNotes");
            conn.Close();
            conn.Dispose();
            return ds;
        }

        public void UpdateHRReviewNonProd(string FollowupDate, string Description, string Strengths, string Opportunity, string Commitment, string SupervisorComments, int ReviewID, int UpdatedBy)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_update_hr_reviewNonProd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (FollowupDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowupDate", Convert.ToDateTime(FollowupDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            cmd.Parameters.AddWithValue("@Commitment", Commitment);
            cmd.Parameters.AddWithValue("@SupervisorComments", SupervisorComments);
            cmd.Parameters.AddWithValue("@ReviewId", ReviewID);
            cmd.Parameters.AddWithValue("@UpdatedBy", UpdatedBy);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }

        public void UpdateHRReviewTT(string FollowDate, string Description, string Strengths, string Opportunity,
           int SessionFocus, string PositiveBehaviour, string OpportunityBehaviour, string BeginBehaviourComments, string BehaviourEffects, string RootCause, string ReviewResultsComments,
           string AddressOpportunities, string ActionPlanComments, string SmartGoals, string CreatePlanComments, string FollowThrough, int UpdatedBy, string SupervisorComments, int ReviewID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_update_review_nexidia_hr", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }
            cmd.Parameters.AddWithValue("@PositiveBehaviour", PositiveBehaviour);
            cmd.Parameters.AddWithValue("@OpportunityBehaviour", OpportunityBehaviour);
            cmd.Parameters.AddWithValue("@BeginBehaviourComments", BeginBehaviourComments);

            cmd.Parameters.AddWithValue("@BehaviourEffects", BehaviourEffects);
            cmd.Parameters.AddWithValue("@RootCause", RootCause);
            cmd.Parameters.AddWithValue("@ReviewResultsComments", ReviewResultsComments);
            cmd.Parameters.AddWithValue("@AddressOpportunities", AddressOpportunities);
            cmd.Parameters.AddWithValue("@ActionPlanComments", ActionPlanComments);
            cmd.Parameters.AddWithValue("@SmartGoals", SmartGoals);

            cmd.Parameters.AddWithValue("@CreatePlanComments", CreatePlanComments);
            cmd.Parameters.AddWithValue("@FollowThrough", FollowThrough);
            cmd.Parameters.AddWithValue("@UpdatedBy", UpdatedBy);
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@SupervisorComments", SupervisorComments);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }


        public void UpdateHRReviewNT(string FollowDate, string Description, string Strengths, string Opportunity, int SessionFocus, int UpdatedBy, string CoacherFeedback, int CallID, string SupervisorComments, int ReviewID)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_update_review_od_hr", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }
            if (CallID == 0)
            {
                cmd.Parameters.AddWithValue("@CallID", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CallID", CallID);
            }
            cmd.Parameters.AddWithValue("@UpdatedBy", UpdatedBy);
            cmd.Parameters.AddWithValue("@SupervisorComments", SupervisorComments);
            cmd.Parameters.AddWithValue("@CoacherFeedback", CoacherFeedback);
            cmd.Parameters.AddWithValue("@ReviewId", ReviewID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();

        }
        public DataSet GetPerformanceDetails(int CIMNumber, string KPI, string DataView, int AccountID)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetPerformanceResults", conn);
            cmd.Parameters.AddWithValue("@CIM", CIMNumber);
            cmd.Parameters.AddWithValue("@KPI", KPI);
            cmd.Parameters.AddWithValue("@DataView", DataView);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "KPIData");
            conn.Close();
            conn.Dispose();
            return ds;

        }
        public DataSet GetSubordinatesSSOv2(int SupCIM, int CimNumber)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("GetCoacheeDetailsViaSSOv2", conn);
            cmd.Parameters.AddWithValue("@SupervisorID", SupCIM);
            cmd.Parameters.AddWithValue("@CoacheeID", CimNumber);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "SSO");
            conn.Close();
            conn.Dispose();
            return ds;

        }
        public DataSet GetLastDate(int CimNumber, string KPI)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetLastDate", conn);
            cmd.Parameters.AddWithValue("@Cim", CimNumber);
            cmd.Parameters.AddWithValue("@KPI", KPI);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "LastDate");
            conn.Close();
            conn.Dispose();
            return ds;

        }
        //public int InsertReviewODv2(int CoacheeID, int AccountID, int SupervisorID, int TopicID, string FollowDate, string Description, string Strengths, string Opportunity, int SessionFocus, int CreatedBy, string CoacherFeedback, int CallID, int SiteID, string Division, int LOBID)
        // changed datatype of callid from int to string (francis.valera/07272018)
        public int InsertReviewODv2(int CoacheeID, int AccountID, int SupervisorID, int TopicID, string FollowDate, string Description, string Strengths, string Opportunity, int SessionFocus, int CreatedBy, string CoacherFeedback, string CallID, int SiteID, string Division, int LOBID, bool RequireSignOff)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_review_odv2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CoacheeID", CoacheeID);
            cmd.Parameters.AddWithValue("@AccountID", AccountID);
            cmd.Parameters.AddWithValue("@SupervisorID", SupervisorID);
            cmd.Parameters.AddWithValue("@TopicID", TopicID);
            if (FollowDate != null)
            {
                cmd.Parameters.AddWithValue("@FollowDate", Convert.ToDateTime(FollowDate));
            }
            else
            {
                cmd.Parameters.AddWithValue("@FollowDate", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@Description", Description);
            cmd.Parameters.AddWithValue("@Strengths", Strengths);
            cmd.Parameters.AddWithValue("@Opportunity", Opportunity);
            if (SessionFocus == 0)
            {
                cmd.Parameters.AddWithValue("@SessionFocus", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@SessionFocus", SessionFocus);
            }

            cmd.Parameters.AddWithValue("@CoacherFeedback", CoacherFeedback);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            
            if (CallID == string.Empty)
            {
                cmd.Parameters.AddWithValue("@CallID", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@CallID", CallID);
            }
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@Division", Division);
            cmd.Parameters.AddWithValue("@LOBID", LOBID);
            cmd.Parameters.AddWithValue("@RequiredSignOff", RequireSignOff == true ? 1 : 0);
            conn.Open();
            int ODReviewID = (int)cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
            return ODReviewID;
        }
        public void InsertReviewKPIODv2(int ReviewID, int KPIID, string Target, string Current, string Previous, int DriverID, string Change, string Behaviour, string RootCause, string DriversID, string DriverNames, string DataView)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_insert_reviewkpiOD", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReviewID", ReviewID);
            cmd.Parameters.AddWithValue("@KPIID", KPIID);
            cmd.Parameters.AddWithValue("@Target", Target);
            cmd.Parameters.AddWithValue("@Current", Current);
            cmd.Parameters.AddWithValue("@Previous", Previous);
            cmd.Parameters.AddWithValue("@DriverID", DriverID);
            cmd.Parameters.AddWithValue("@Change", Change);
            cmd.Parameters.AddWithValue("@Behaviour", Behaviour);
            cmd.Parameters.AddWithValue("@RootCause", RootCause);
            cmd.Parameters.AddWithValue("@DriversID", DriversID);
            cmd.Parameters.AddWithValue("@DriversName", DriverNames);
            cmd.Parameters.AddWithValue("@DataView", DataView);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        public DataSet GetKPIReviewODv2(int ReviewID)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_get_review_kpi_ODv2", conn);
            cmd.Parameters.AddWithValue("@ID", ReviewID);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            da.SelectCommand = cmd;
            cmd.ExecuteNonQuery();
            da.Fill(ds, "ReviewKPIOD");
            conn.Close();
            conn.Dispose();
            return ds;
        }


        public DataTable GetAllSites()
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetSite", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }

        public DataTable GetAllCampaigns()
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAllCampaign", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }


        public DataTable GetAllDivs()
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_getAllDivisions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }

        public DataTable GetAllLOB()
        {

            DataSet ds = new DataSet();
            SqlCommand cmd = null;
            cmd = new SqlCommand("pr_Coach_GetAllLOBs", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda1.Fill(dt);

            conn.Close();
            conn.Dispose();

            return dt;

        }

    }
 }