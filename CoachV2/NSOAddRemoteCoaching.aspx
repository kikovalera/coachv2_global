﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NSOAddRemoteCoaching.aspx.cs" Inherits="CoachV2.NSOAddRemoteCoaching" %>
<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
<link href="libs/css/gridview.css" rel="stylesheet" type="text/css" />
<%-- <script type="text/javascript">
     function onRequestStart(sender, args) {
        if (args.get_eventTarget().indexOf("btn_ExporttoPDF") >= 0) {
            args.set_enableAjax(false);
        if (args.get_eventTarget().
        }indexOf("btn_ExporttoPDF1") >= 0) {
            args.set_enableAjax(false);
        }
    }
  </script>
  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rptCoacherFeedback">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rptCoacherFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rptCoacheeFeedback">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rptCoacheeFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadAccount">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CoachingSpecifics" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSupervisor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CoachingSpecifics" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeSignOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pn_coacher"  />
                    <telerik:AjaxUpdatedControl ControlID="CoacherFeedback"  />
                    <telerik:AjaxUpdatedControl ControlID="CoacheeFeedback"  />
                    <telerik:AjaxUpdatedControl ControlID="CoacherFeedbackHistory"  />
                    <telerik:AjaxUpdatedControl ControlID="CoacheeFeedbackHistory"  />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDocumentation" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
          </AjaxSettings>
    </telerik:RadAjaxManager>--%>
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
  <script type="text/javascript" src="libs/js/JScript1.js"></script>
      <telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btn_ExporttoPDF.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= btn_ExporttoPDF1.UniqueID %>") {
                    args.set_enableAjax(false);
                }
            }

            function ShowConfirmation(sender, args) {
                return confirm("By confirming, this will end your Remote Coaching session with your Coachee\nDo you wish to proceed?");
            }

            function RadSaveButtons_OnClientClicked(source, args) {

                if (Page_IsValid) {
                    var RadSaveBtn = $find("<%=RadSave.ClientID%>");
                    RadSaveBtn.set_enabled(false);

                }

                else
                    document.getElementById(target).disabled = false;


            }
        </script>       
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="rptCoacherFeedback">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rptCoacherFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rptCoacheeFeedback">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rptCoacheeFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadAccount">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CoachingSpecifics" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSupervisor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CoachingSpecifics" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeName">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCoacheeSignOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadEndSession">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="RadSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pn_coacher" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CoacherFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CoacheeFeedback" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CoacherFeedbackHistory" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CoacheeFeedbackHistory" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridDocumentation" LoadingPanelID="RadAjaxLoadingPanel1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="PanelDocumentation" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="SignOut" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%-- ajaxify PanelDocumentation and Signout to prevent multiple saving (francis.valera/07302018) --%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadSessionType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSessionTopic" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="conditionalPostback">
        <div class="menu-content bg-alt">
            <ucdash5:DashboardMyReviewsUserControl ID="DashboardMyReviewsUserControl1" runat="server" />
            <asp:Panel ID="pn_coacheedetails" Visible="false" runat="server">
                <div class="panel panel-custom">
                    <a data-toggle="collapse" data-parent="#accordion" href="#coachingspecs">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                                </span>
                            </h4>
                        </div>
                    </a>
                    <div id="coachingspecs" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="Account" class="control-label col-sm-2">
                                    Coaching Ticket</label>
                                <div class="col-sm-3">
                                    <telerik:RadTextBox ID="RadTextBox1" runat="server" class="form-control" Text="0"
                                        ReadOnly="true" Enabled="false" Width="100%">
                                    </telerik:RadTextBox>
                                </div>
                                <label for="Account" class="control-label col-sm-3">
                                    Coaching Date</label>
                                <div class="col-sm-3">
                                    <telerik:RadTextBox ID="RadFollowup" Width="100%" runat="server" class="form-control"
                                        Text="0" ReadOnly="true">
                                    </telerik:RadTextBox>
                                </div>
                                <div class="col-sm-1 text-right">
                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                    <asp:HiddenField ID="HiddenField3" runat="server" />
                                    <telerik:RadButton ID="btn_ExporttoPDF1" runat="server" Width="45px" Height="45px"
                                        Visible="true" OnClick="btn_ExporttoPDF_Click">
                                        <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                    </telerik:RadButton>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="container-fluid" id="Div6" runat="server">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:HiddenField ID="FakeURLID" runat="server" />
                                        <div id="profileimageholder">
                                            <asp:Image ID="ProfImage" runat="server" CssClass="img-responsive" Width="200" Height="200" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Name: </b><span>
                                                <asp:Label ID="LblFullName" runat="server" Text="My Full Name" /></span></h5>
                                        <h5>
                                            <b>Position: </b><span>
                                                <asp:Label ID="LblRole" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Site: </b><span>
                                                <asp:Label ID="lblSite" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Reporting Manager: </b><span>
                                                <asp:Label ID="LblMySupervisor" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Region: </b><span>
                                                <asp:Label ID="LblRegion" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Country: </b><span>
                                                <asp:Label ID="LblCountry" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Department: </b><span>
                                                <asp:Label ID="LblDept" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Client: </b><span>
                                                <asp:Label ID="LblClient" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b>Campaign: </b><span>
                                                <asp:Label ID="LblCampaign" runat="server" Text="My region" /></span></h5>
                                        <h5>
                                            <b></b><span>
                                                <asp:Label ID="LblCimNumber" runat="server" Text="My region" Visible="false" /></span></h5>
                                        <h5>
                                            <b></b><span>
                                                <asp:Label ID="LblCimCoacher" runat="server" Text="My region" Visible="false" /></span></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>
                                            <b>Session Type: </b><span>
                                                <asp:Label ID="SessionType" runat="server" Text="My Session Type" /></span></h5>
                                        <h5>
                                            <b>Topic: </b><span>
                                                <asp:Label ID="SessionTopic" runat="server" Text="My Session Type" /></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pn_coacher" runat="server" Visible="true">
                <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Coaching Specifics <span class="glyphicon glyphicon-triangle-top triangletop">
                                </span>
                            </h4>
                        </div>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <%--  <div class="panel-body">
                <div class="form-group">
                    <label for="Account" class="control-label col-sm-2">
                        Coaching Ticket</label>
                    <div class="col-sm-3">
                        <telerik:RadTextBox ID="RadReviewID" runat="server" Width="100%" class="form-control"
                            Text="0" ReadOnly="true">
                        </telerik:RadTextBox>
                    </div>
                    <label for="Account" class="control-label col-sm-3">
                        Coaching Date</label>
                    <div class="col-sm-2">
                        <telerik:RadTextBox ID="RadCoachingDate" Width="80%" runat="server" class="form-control"
                            Text="0" ReadOnly="true">
                        </telerik:RadTextBox>
                    </div>
                    <div class="col-sm-2 text-right">
                        <asp:HiddenField ID="HiddenField4" runat="server" />
                        <asp:HiddenField ID="HiddenField5" runat="server" />
                        <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                            Visible="true" OnClick="btn_ExporttoPDF_Click">
                            <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                        </telerik:RadButton>
                        <asp:HiddenField ID="hfGridHtml" runat="server" />
                    </div>
                </div>
            </div>--%>
                        <div class="panel-body" style="padding-bottom: 5px">
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2 col-md-push-10">
                                    <div class="col-sm-2">
                                        <asp:HiddenField ID="HiddenField4" runat="server" />
                                        <asp:HiddenField ID="HiddenField5" runat="server" />
                                        <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                            Visible="true" OnClick="btn_ExporttoPDF_Click">
                                            <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                        </telerik:RadButton>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-10 col-md-pull-2">
                                    <label for="Account" class="control-label col-sm-3">
                                        Coaching Ticket</label>
                                    <div class="col-sm-3">
                                        <telerik:RadTextBox ID="RadReviewID" Width="100%" runat="server" class="form-control"
                                            Text="0" ReadOnly="true">
                                        </telerik:RadTextBox>
                                    </div>
                                    <label for="Account" class="control-label col-sm-3">
                                        Coaching Date</label>
                                    <div class="col-sm-3">
                                        <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                            Text="0" ReadOnly="true">
                                        </telerik:RadTextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="panel-body">
                            <div class="form-group col-sm-6" id="SelectCoacheeGroup" runat="server">
                                <form class="form-horizontal">
                                <label for="name2" class="control-label">
                                    Select Coachee</label>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">
                                        Account:</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadAccount" runat="server" class="form-control" AutoPostBack="true"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="1" EmptyMessage="-Select Account-"
                                            Width="100%" OnSelectedIndexChanged="RadAccount_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                            ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                            CssClass="validator"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Supervisor">
                                        Supervisor:</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadSupervisor" runat="server" class="form-control" AutoPostBack="true"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="-Select Supervisor-"
                                            TabIndex="2" Width="100%" OnSelectedIndexChanged="RadSupervisor_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                            ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                            ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="CoacheeName" class="control-label col-sm-4">
                                        Coachee Name</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadCoacheeName" runat="server" class="form-control" AutoPostBack="true"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" EmptyMessage="-Select Coachee-"
                                            TabIndex="3" Width="100%">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                            ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                            ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                                </form>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="name2" class="control-label">
                                    Coaching Specifics</label>
                                <div class="form-group">
                                    <label for="SessionType" class="control-label col-sm-4">
                                        Session Type</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true"
                                            RenderMode="Lightweight" DropDownAutoWidth="Enabled" TabIndex="4" Width="100%"
                                            OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged" EmptyMessage="-Select Session Type-">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="AddReview"
                                            ControlToValidate="RadSessionType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                            CssClass="validator"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="Topic" class="control-label col-sm-4">
                                        Topic</label>
                                    <div class="col-sm-8">
                                        <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                            TabIndex="5" AutoPostBack="true" Width="100%" EmptyMessage="-Select Session Topic-">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="AddReview"
                                            ControlToValidate="RadSessionTopic" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                            CssClass="validator"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div>
                                    &nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="panel panel-custom" id="CoacherFeedback" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coach Feedback <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadCoacherFeedback" runat="server" class="form-control" placeholder="Description"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RadCoacherFeedbackValidator" ValidationGroup="AddReview"
                            ControlToValidate="RadCoacherFeedback" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a required field." CssClass="validator">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom" id="CoacheeFeedback" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coachee Feedback <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadTextBox ID="RadCoacheeFeedback" runat="server" class="form-control" placeholder="Description"
                            TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7" onkeydown="setHeight(this,event)" style="overflow:hidden !important;">
                            <ClientEvents OnLoad="RadTextBoxLoad" />
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RadCoacheeFeedbackValidator" ValidationGroup="AddReview"
                            ControlToValidate="RadCoacheeFeedback" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a required field." CssClass="validator">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom" id="CoacherFeedbackHistory" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#Div2">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coach Feedback History <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="Div2" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <h4 style="text-decoration: underline;">
                            Comments:</h4>
                        <telerik:RadListView ID="rptCoacherFeedback" runat="server">
                            <ItemTemplate>
                                <div class="commentbox">
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>'>'></asp:Label></b>&nbsp;(<asp:Label
                                            ID="Label2" runat="server" Text='<%#Eval("DateTimeCreated") %>'>'></asp:Label>):<br />
                                    <%--<asp:Label ID="Label3" runat="server" Text='<%#Eval("Feedback") %>'></asp:Label><br />--%>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("Feedback").ToString().Replace("\n", "<br />") %>'></asp:Label><br />
                                </div>
                            </ItemTemplate>
                        </telerik:RadListView>
                        <div style="overflow: hidden;">
                            <telerik:RadListView ID="rptPaging2" runat="server" OnItemCommand="rptPaging2_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnPage" Style="padding: 8px; margin: 2px; background: #007acc;
                                        border: solid 1px blue; font: 8px;" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                        runat="server" ForeColor="White" Font-Bold="True" CausesValidation="false"><%# Container.DataItem %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </telerik:RadListView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom" id="CoacheeFeedbackHistory" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#Div1">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Coachee Feedback History <span class="glyphicon glyphicon-triangle-top triangletop">
                            </span>
                        </h4>
                    </div>
                </a>
                <div id="Div1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <h4 style="text-decoration: underline;">
                            Comments:</h4>
                        <telerik:RadListView ID="rptCoacheeFeedback" runat="server">
                            <ItemTemplate>
                                <div class="commentbox">
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>'>'></asp:Label></b>&nbsp;(<asp:Label
                                            ID="Label2" runat="server" Text='<%#Eval("DateTimeCreated") %>'>'></asp:Label>):<br />
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("Feedback").ToString().Replace("\n", "<br />") %>'></asp:Label><br />
                                </div>
                            </ItemTemplate>
                        </telerik:RadListView>
                        <div style="overflow: hidden;">
                            <telerik:RadListView ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnPage" Style="padding: 8px; margin: 2px; background: #007acc;
                                        border: solid 1px blue; font: 8px;" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                        runat="server" ForeColor="White" Font-Bold="True" CausesValidation="false"><%# Container.DataItem %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </telerik:RadListView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-custom" id="PanelDocumentation" runat="server">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseQADocumentation">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Documentation <span class="glyphicon glyphicon-triangle-top triangletop"></span>
                        </h4>
                    </div>
                </a>
                <div id="collapseQADocumentation" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                            AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                            OnItemDataBound="RadGridDocumentation_onItemDatabound" CssClass="RemoveBorders"
                            GridLines="None" BorderStyle="None">
                            <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Item Number">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                        FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="Document Name"
                                        UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                        AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Scrolling AllowScroll="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div>
                            &nbsp;
                        </div>
                        <div class="col-sm-12">
                            <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" MultipleFileSelection="Automatic"
                                AllowedFileExtensions=".pdf,.png,.xls,.xlsx" MaxFileSize="1000000" Width="40%" OnClientValidationFailed="OnClientValidationFailed">
                                <Localization Select="                             " />
                            </telerik:RadAsyncUpload>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="SignOut" class="container-fluid" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <telerik:RadButton ID="RadSave" runat="server" Text="Save" CssClass="btn btn-info btn-small"
                                ForeColor="White" ValidationGroup="AddReview" OnClick="RadSave_Click">
                            </telerik:RadButton>
                            <telerik:RadButton ID="RadEndSession" runat="server" Text="End Session" CssClass="btn btn-info btn-small"
                                ForeColor="White" Visible="false" OnClick="RadEndSession_Click" OnClientClicked="ShowConfirmation">
                            </telerik:RadButton>
                            <telerik:RadTextBox ID="RadCoacheeCIMNum" runat="server" class="form-control" RenderMode="Lightweight"
                                TabIndex="12" placeholder="Coachee CIM Number" Visible="false">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox ID="RadSSN" runat="server" class="form-control" placeholder="Last 3 SSID"
                                RenderMode="Lightweight" TabIndex="13" TextMode="Password" MaxLength="3">
                            </telerik:RadTextBox>
                            <telerik:RadButton ID="RadCoacheeSignOff" runat="server" Text="Coachee Sign Off"
                                CssClass="btn btn-info btn-small" ValidationGroup="AddReview" OnClick="RadCoacheeSignOff_Click"
                                ForeColor="White">
                            </telerik:RadButton>
                            <telerik:RadButton ID="RadCoacherSignOff" runat="server" Text="Coach Sign Off"
                                CssClass="btn btn-info btn-small" OnClick="RadCoacherSignOff_Click" ForeColor="White">
                            </telerik:RadButton>
                            <telerik:RadButton ID="btn_Audit1" runat="server" Text="Audit" Visible="false" ValidationGroup="AddReview"
                                CssClass="btn btn-info btn-small" ForeColor="White" OnClick="btn_Audit1_Click">
                            </telerik:RadButton>
                        </div>
                    </div>

                    <asp:Label ID="lblstatus" runat="server"  ForeColor="Red" ></asp:Label>
                </div>
            </div>
                   

        </div>
    </telerik:RadAjaxPanel>
</asp:Content>