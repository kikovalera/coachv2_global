﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoachV2.Controllers
{
    public class HomeController : Controller
    {
        // so default URL should go to index.html
        public RedirectResult Index()
        {
            Response.Redirect("~/Default.aspx");
            return Redirect("~/Default.aspx");
        }
    }
}
