﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using Telerik.Web.UI.GridExcelBuilder;
using System.Web.Configuration;
using System.Data.SqlClient;
using CoachV2.AppCode;


namespace CoachV2
{
    public partial class SaveBuiltInReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
        }
        protected void RadBack_Click(object sender, EventArgs e)
        {
            string jScript = "<script>window.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
        }
        protected void RadSave_Click(object sender, EventArgs e)
        {
            DataSet ds_reportype = DataHelper.getReportType(Convert.ToInt32(Page.Request.QueryString["ReportID"]));
            if (RadReportName.Text != string.Empty)
            {
                if (ds_reportype.Tables[0].Rows.Count > 0)
                {

                    DataHelper.SaveBuiltIn(Convert.ToInt32(Page.Request.QueryString["ReportID"]), 1, Convert.ToString(RadReportName.Text));
                    string ModalLabel = "Successfully saved report.";
                    string ModalHeader = "Success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    RadReportName.Enabled = false;
                    RadSave.Enabled = false;
                    //string jScript = "<script>window.close();</script>";
                    //ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
                    //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('GeneratedReport.aspx?ReportID=" + Page.Request.QueryString["ReportID"] + "&UserID=" + Convert.ToString(Page.Request.QueryString["UserID"]) + "','Graph');", true);
                };
            }
            else
            {
                string ModalLabel = "Fill up Report Name";
                string ModalHeader = "Error";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);               
                RadReportName.Focus();
            }
        }
    }
}