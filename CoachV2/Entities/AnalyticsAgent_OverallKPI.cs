﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.Entities
{
    public class AnalyticsAgent_OverallKPI
    {
        //coach count
        public int coached { get; set; }
        //date range
        public string ADate { get; set; }
     }
}