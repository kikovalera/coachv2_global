﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.Entities
{
    public class AnalyticsX
    {
        public string ADate { get; set; }
        public int CoachCount { get; set; }
        public decimal TargetCount { get; set; }
        public decimal CurrentCount { get; set; }
        public int? ChangeFiscal { get; set; }
    }
}