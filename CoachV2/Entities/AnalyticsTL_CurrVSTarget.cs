﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.Entities
{
    public class AnalyticsTL_CurrVsTarget
    {
        //coach count , target score, date range, current
        public int coachedkpis { get; set; }
        public int target { get; set; }
        public string ADate { get; set; }
        public int AvgCurr { get; set; }
     }
}