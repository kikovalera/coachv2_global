﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachV2.Entities
{
    public class Analytics
    {
        public string ADate { get; set; }
        public int CoachCount { get; set; }
        public  int TargetCount { get; set; }
        public decimal CurrentCount { get; set; }
    }
}