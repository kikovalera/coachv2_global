﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
namespace CoachV2
{
    /// <summary>
    /// Summary description for setup_role
    /// </summary>
    public class setup_role : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
             string category = string.Empty;
            string val=string.Empty;
            try
            {
               category=context.Request["cat"].ToString();
                val = context.Request["val"].ToString();
            }
            catch (NullReferenceException)
            {

                context.Response.ContentType = "text/plain";
                context.Response.Write("Query strings are empty/invalid");
                return;
            }
                 string xml_path = "roles_set.xml";
                     XmlDocument doc = new XmlDocument();
                    doc.Load(xml_path);
                    XmlNode appinfo = doc["Roles"][category].AppendChild(doc.CreateElement("Role"));
                    appinfo.AppendChild(doc.CreateTextNode(val));
                    doc.Save(xml_path);


            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}