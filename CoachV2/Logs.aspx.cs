﻿using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using Telerik.Web.UI;

namespace CoachV2
{
    public partial class Logs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dpLogDate.MaxDate = DateTime.Now;
                dpLogDate.SelectedDate = DateTime.Now;
                DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/LogFiles/"));
                FileInfo[] Files = d.GetFiles("*.json");
                foreach (FileInfo file in Files)
                {
                    DateTime lastmodified = file.CreationTime;
                    Telerik.Web.UI.RadCalendarDay radDay = new RadCalendarDay();
                    radDay.Date = lastmodified;
                    radDay.IsToday = false;
                    radDay.ItemStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                    dpLogDate.Calendar.SpecialDays.Add(radDay);
                }
            }
        }

        protected void gridLogs_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            string filepath = HttpContext.Current.Server.MapPath("~/LogFiles/");
            filepath = filepath + "TranscomCoachV2_Logs_" + dpLogDate.SelectedDate.Value.ToString("yyyyMMdd") + ".json";
            DataTable dtTopics = new DataTable();
            if (File.Exists(filepath))
            {
                var initialJson = File.ReadAllText(@filepath);
                dtTopics = JsonConvert.DeserializeObject<DataTable>(initialJson);
                dtTopics.DefaultView.Sort = "LogDateTime desc";
            }
            gridLogs.DataSource = dtTopics;
        }

        protected void dpLogDate_OnSelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            gridLogs.Rebind();
        }

    }
}