﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Telerik.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI;
/// <summary>
/// Author      : Shanu
/// Create date : 2019-09-04
/// Description : telerikGridHelper
/// Latest
/// Modifier    : Shanu
/// Modify date : 2019-09-04
/// </summary>
public class TelerikGridHelper
{
	public TelerikGridHelper()
	{	}
    //Set all the telerik Grid layout
    #region Layout
    public static void Layouts(RadGrid grid, int height, int width, Boolean widthtype, Boolean multirowselect, Boolean allowsorting, Boolean showstatusbar, Boolean allowfilteringbycolumns, Boolean showgrouppannel, Boolean ShowHeader, Boolean ShowFooter)
    {
        grid.AutoGenerateColumns = false; // set the auto genrated columns as false
        grid.Skin = "Office2007";// set the Skin
        grid.AllowMultiRowSelection = multirowselect;//set the multirow selection as true or false     
        grid.AllowSorting = allowsorting; // Set Sorting for a grid
        // set grid lines as none
        grid.GridLines = GridLines.Both;
        grid.ShowStatusBar = showstatusbar; // set true or false to display the status bar
        grid.AllowFilteringByColumn = allowfilteringbycolumns; //Set the Filtering for a individual columns
       
        grid.Height = height; //Set the height of the grid  in % or in pixcel
        if (width > 0)
        {
            if (widthtype == false)
            {
                grid.Width = width; // set the Width of the grid  in % or in pixcel
            }
            else
            {
                grid.Width = Unit.Percentage(width);
            }
        }       
        grid.ShowGroupPanel = showgrouppannel;//show group panel for header
        grid.ShowHeader = ShowHeader; // show header of the grid true or false
        grid.ShowFooter = ShowFooter; // show header of the grid true or false
    }

    #endregion

    //Set all the telerik Grid Page
    #region LayoutPage
    public static void LayoutPage(RadGrid grid, int pagesize, Boolean allowpaging)
    {
        grid.PageSize = pagesize;//Set the Grid Page default page size
        grid.AllowPaging = allowpaging;//Set Paging for a grid as true or false
        grid.PagerStyle.PageSizeControlType = PagerDropDownControlType.None; 

        grid.PagerStyle.Mode = GridPagerMode.NextPrevAndNumeric;
       

    }
    #endregion

    //Client Settings like columns reorder, scrolling and resize
    #region ClientSetting
    public static void ClientSetting(RadGrid grid, Boolean ColumnReorder, Boolean ReorderColumnsOnClient, Boolean AllowColumnResize, Boolean EnableRealTimeResize, Boolean AllowRowSelect, Boolean EnableRowHoverStyle, int FrozenColumnsCount)
    {
        grid.ClientSettings.AllowColumnsReorder = ColumnReorder;
        grid.ClientSettings.ReorderColumnsOnClient = ReorderColumnsOnClient;
        grid.ClientSettings.EnableRowHoverStyle = EnableRowHoverStyle;
        grid.ClientSettings.Scrolling.AllowScroll = true;
        grid.ClientSettings.Scrolling.SaveScrollPosition = true;
        grid.ClientSettings.Scrolling.UseStaticHeaders = true;
        grid.ClientSettings.Scrolling.ScrollHeight = 125;
        grid.ClientSettings.Scrolling.ScrollBarWidth = 100;
        grid.ClientSettings.Scrolling.FrozenColumnsCount = FrozenColumnsCount;
        grid.ClientSettings.Resizing.EnableRealTimeResize = EnableRealTimeResize;
        grid.ClientSettings.Resizing.AllowColumnResize = AllowColumnResize;
        grid.ClientSettings.Selecting.AllowRowSelect = AllowRowSelect;

        //grid.ClientSettings.Scrolling.EnableVirtualScrollPaging = true;        

    }
    #endregion

    //Client Settings ForKeyboardNavigation
    #region ClientSetting
    public static void ClientSettingKEYBOARDNavi(RadGrid grid, Boolean AllowKeyboardNavigation, Boolean EnableKeyboardShortcuts, Boolean AllowActiveRowCycle)
    {
        
        grid.ClientSettings.Selecting.CellSelectionMode = GridCellSelectionMode.MultiCell;
        grid.ClientSettings.Selecting.EnableDragToSelectRows = false;
        grid.ClientSettings.AllowKeyboardNavigation = AllowKeyboardNavigation;
        grid.ClientSettings.KeyboardNavigationSettings.EnableKeyboardShortcuts = EnableKeyboardShortcuts;
        grid.ClientSettings.KeyboardNavigationSettings.AllowActiveRowCycle = AllowActiveRowCycle;

        //grid.ClientSettings.Resizing.AllowColumnResize = true;
        //grid.ClientSettings.Resizing.EnableRealTimeResize = true;

    }
    #endregion

    //Client Events .this function used to decalre the client side events
    #region ClientEvents
    public static void ClientEvents(RadGrid grid, clientEventType clienteventtype, String clientfunctionname)
    {
        switch (clienteventtype.ToString())
        {
            case "gridCreated":
                grid.ClientSettings.ClientEvents.OnGridCreated = clientfunctionname;//"GetGridObject";
                break;
            case "rowClicked":
                grid.ClientSettings.ClientEvents.OnRowClick = clientfunctionname;
                break;
            case "rowDblClick":
                grid.ClientSettings.ClientEvents.OnRowDblClick = clientfunctionname;
                break;
            case "ColumnClick":
                grid.ClientSettings.ClientEvents.OnColumnClick = clientfunctionname;
                break;
            case "OnRowSelected":
                grid.ClientSettings.ClientEvents.OnRowSelected = clientfunctionname;
                break;
            case "OnKeyPress":
                grid.ClientSettings.ClientEvents.OnKeyPress = clientfunctionname;
                break;

        }


    }
    #endregion

    #region ExporttoExcel
    public static void ExporttoExcel(RadGrid grid, Boolean ExportOnlyData, Boolean IgnorePaging, Boolean OpenInNewWindow, Boolean UseItemStyles)
    {


        grid.ExportSettings.ExportOnlyData = ExportOnlyData;
        grid.ExportSettings.IgnorePaging = IgnorePaging;
        grid.ExportSettings.OpenInNewWindow = OpenInNewWindow;
        grid.ExportSettings.UseItemStyles = UseItemStyles;

        grid.MasterTableView.ExportToExcel();
    }
    #endregion
    // bind the Datatable to  grid
    #region DataBind
    public static void DataBinds(RadGrid grid, DataTable dataTable, Boolean needdatasource)
    {


        grid.DataSource = dataTable;
        if (!needdatasource)
        {
            grid.DataBind();
        }
    }
    public static void DataBinds(RadGrid grid, DataSet dataSet, Boolean needdatasource)
    {
        DataBinds(grid, dataSet.Tables[0], needdatasource);
    }
    #endregion

    //In this Mastertbaleview we define the datakey for the grid
    #region GridMasterTableSetting
    public static void Mastertableview(RadGrid grid, String[] keyvalue, Boolean allowfilteringbycolumn)
    {
        if (keyvalue[0] != String.Empty)
        {
            grid.MasterTableView.DataKeyNames = keyvalue; // set the grid Datakeyname use ,(comma) for more then one key
            grid.MasterTableView.ClientDataKeyNames = keyvalue; //set the Client Datakey names
        }
        // grid.MasterTableView.TableLayout = "Auto";
        grid.MasterTableView.EnableHeaderContextMenu = true;
        grid.MasterTableView.AllowFilteringByColumn = allowfilteringbycolumn;
        grid.MasterTableView.Font.Size = 9;
        grid.MasterTableView.TableLayout = GridTableLayout.Fixed;


        //grid.MasterTableView.ExpandCollapseColumn.Visible = false;
        //grid.MasterTableView.ExpandCollapseColumn.Visible = false;
        //grid.MasterTableView.ExpandCollapseColumn.Display = false;
      //  grid.MasterTableView.EnableGroupsExpandAll = false;
       
        grid.MasterTableView.ShowGroupFooter = true;
        grid.MasterTableView.ShowFooter = true;
       
    }
    #endregion

    //here we define each column type and value and text
    #region gridColumnType

    //To set the Grid header column groups
    #region ColumnGroups
    public static void ColumnGroups(RadGrid grid, String groupHeaderText, String groupName, String ParentgroupName, HorizontalAlign Alignment, int Width)
    {
        if (groupHeaderText == String.Empty)
        {
            return;
        }
        GridColumnGroup columnGroup = new GridColumnGroup();
        columnGroup.HeaderText = groupHeaderText;
        columnGroup.Name = groupName;
        columnGroup.HeaderStyle.HorizontalAlign = Alignment;
        columnGroup.HeaderStyle.Width = Width;
        if (ParentgroupName != String.Empty)
        {
            columnGroup.ParentGroupName = ParentgroupName;
        }
        grid.MasterTableView.ColumnGroups.Add(columnGroup);

    }
    #endregion


    //Bound column is used to display the data 
    #region BoundColumn
    public static void BoundColumn(RadGrid grid, String HeaderText, String datafield, String UniqueName, String groupName, HorizontalAlign Alignment, int Width, String Aggregate, Boolean AllowFiltering, Boolean colDisplay, VerticalAlign verticalAlignment, HorizontalAlign itemAlignment)
    {
        GridBoundColumn boundColumn;
        boundColumn = new GridBoundColumn();
        boundColumn.DataField = datafield;
        boundColumn.HeaderText = HeaderText;
        boundColumn.UniqueName = UniqueName;
        if (groupName != String.Empty)
        {
            boundColumn.ColumnGroupName = groupName;
        }

        boundColumn.HeaderStyle.HorizontalAlign = Alignment;
        boundColumn.HeaderStyle.VerticalAlign = verticalAlignment;
        boundColumn.ItemStyle.HorizontalAlign = itemAlignment;
        boundColumn.HeaderStyle.Width = Width;
        boundColumn.Aggregate = GridAggregateFunction.None;
        boundColumn.Display = colDisplay;
        boundColumn.AllowFiltering = AllowFiltering;
        boundColumn.DataFormatString = "{0:n0}";
        if (Aggregate != String.Empty)
        {
            // boundColumn.FooterText = Footertext;
          
            switch (Aggregate)
            {
                case "Sum":
                    boundColumn.Aggregate = GridAggregateFunction.Sum;
                    boundColumn.FooterAggregateFormatString = " {0:n0}";
                    boundColumn.FooterStyle.Font.Bold = true;
                    break;
                case "Avg":
                    boundColumn.Aggregate = GridAggregateFunction.Avg;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
                case "Count":
                    boundColumn.Aggregate = GridAggregateFunction.Count;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0}";
                    break;
                case "Max":
                    boundColumn.Aggregate = GridAggregateFunction.Max;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
                case "Min":
                    boundColumn.Aggregate = GridAggregateFunction.Min;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
            }
            //boundColumn.FooterText = Footertext;
            //  boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";

        }

        grid.MasterTableView.Columns.Add(boundColumn);
    }
    #endregion


    //Bound column is used to display the data 
    #region BoundColumn
    public static void BoundColumnnoFormat(RadGrid grid, String HeaderText, String datafield, String UniqueName, String groupName, HorizontalAlign Alignment, int Width, String Aggregate,String FooterText, Boolean AllowFiltering, Boolean colDisplay, VerticalAlign verticalAlignment, HorizontalAlign itemAlignment)
    {
        GridBoundColumn boundColumn;
        boundColumn = new GridBoundColumn();
        boundColumn.DataField = datafield;
        boundColumn.HeaderText = HeaderText;
        boundColumn.UniqueName = UniqueName;
        if (groupName != String.Empty)
        {
            boundColumn.ColumnGroupName = groupName;
        }

        boundColumn.HeaderStyle.HorizontalAlign = Alignment;
        boundColumn.HeaderStyle.VerticalAlign = verticalAlignment;
        boundColumn.ItemStyle.HorizontalAlign = itemAlignment;
        boundColumn.HeaderStyle.Width = Width;
        boundColumn.Aggregate = GridAggregateFunction.None;
        boundColumn.Display = colDisplay;
        boundColumn.AllowFiltering = AllowFiltering;
      //  boundColumn.DataFormatString = "{0:n0}";
        if (Aggregate != String.Empty)
        {
            // boundColumn.FooterText = Footertext;

            switch (Aggregate)
            {
                case "Sum":
                    boundColumn.Aggregate = GridAggregateFunction.Sum;
                    boundColumn.FooterAggregateFormatString = FooterText + "{0:n}";
                    boundColumn.FooterStyle.Font.Bold = true;
                   // boundColumn.FooterText = "합계";
                    break;
                case "Avg":
                    boundColumn.Aggregate = GridAggregateFunction.Avg;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
                case "Count":
                    boundColumn.Aggregate = GridAggregateFunction.Count;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0}";
                    break;
                case "Max":
                    boundColumn.Aggregate = GridAggregateFunction.Max;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
                case "Min":
                    boundColumn.Aggregate = GridAggregateFunction.Min;
                    boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";
                    break;
                case "None":
                  boundColumn.Aggregate = GridAggregateFunction.None;
                    boundColumn.FooterAggregateFormatString = FooterText ;
                    boundColumn.FooterText = FooterText;
                    boundColumn.FooterStyle.Font.Bold = true;
                    break;
                // boundColumn.FooterText = "합계";
            }
            //boundColumn.FooterText = Footertext;
            //  boundColumn.FooterAggregateFormatString = boundColumn.Aggregate.ToString() + ": {0:n}";

        }

        grid.MasterTableView.Columns.Add(boundColumn);
    }
    #endregion

    //Image column is used to add the image to the column
    #region ImageColumn
    public static void ImageColumn(RadGrid grid, String HeaderText, String[] datafield, String imageURL, String AlternateText, String groupName, ImageAlign imgAlign, int ImageHeight, int ImageWidth, int ColWidth)
    {
        GridImageColumn imageColumn;
        imageColumn = new GridImageColumn();
        imageColumn.HeaderText = HeaderText;
        imageColumn.DataImageUrlFields = datafield;
        if (groupName != String.Empty)
        {
            imageColumn.ColumnGroupName = groupName;
        }
        imageColumn.DataImageUrlFormatString = imageURL;
        imageColumn.AlternateText = AlternateText;
        imageColumn.HeaderStyle.Width = ColWidth;
        imageColumn.ImageAlign = imgAlign;
        imageColumn.ItemStyle.Width = ColWidth;
        imageColumn.AllowFiltering = false;
        if (ImageHeight > 0)
        {
            imageColumn.ImageHeight = ImageHeight;
        }
        if (ImageWidth > 0)
        {
            imageColumn.ImageWidth = ImageWidth;
        }

        grid.MasterTableView.Columns.Add(imageColumn);
    }
    #endregion

    //Template Column In this column we can add Textbox,Lable,Check Box,Dropdown box,LinkButton,button,Image Button,numeric textbox and etc
    #region Templatecolumn
    //public static void Templatecolumn(RadGrid grid, String HeaderText, String datafield, String UniqueName, String groupName, HorizontalAlign Alignment, int Width, String Aggregate, String Footertext, Boolean AllowFiltering,String Columntype)
    public static void Templatecolumn(RadGrid grid, String HeaderText, String datafield, String UniqueName, String groupName, HorizontalAlign Alignment, int Width, Boolean AllowFiltering, TelerikControlType Columntype, String contolID, String CommandName, Boolean chklableVisible, HorizontalAlign ItemAlignment, Boolean ColumnDisplay)
    {
        if (Width <= 10)
        {
            Width = 20;
        }
        GridTemplateColumn templateColumn;
        templateColumn = new GridTemplateColumn();
        templateColumn.ItemTemplate = new MyTemplate(datafield, Columntype.ToString(), Width - 2, contolID, CommandName, chklableVisible);
        templateColumn.HeaderText = HeaderText;
        templateColumn.DataField = "datafield";
        templateColumn.UniqueName = UniqueName;
        templateColumn.Display = ColumnDisplay;
        if (groupName != String.Empty)
        {
            templateColumn.ColumnGroupName = groupName;
        }
        templateColumn.HeaderStyle.HorizontalAlign = Alignment;
        templateColumn.ItemStyle.HorizontalAlign = ItemAlignment;
        templateColumn.HeaderStyle.Width = Width;
       


        templateColumn.Aggregate = GridAggregateFunction.None;

        templateColumn.AllowFiltering = AllowFiltering;

        //if (Aggregate != String.Empty)
        //{
        //    templateColumn.FooterAggregateFormatString = templateColumn.Aggregate.ToString() + ": {0:n}";
        //    switch (Aggregate)
        //    {
        //        case "Sum":
        //            templateColumn.Aggregate = GridAggregateFunction.Sum;
        //            break;
        //        case "Avg":
        //            templateColumn.Aggregate = GridAggregateFunction.Avg;
        //            break;
        //        case "Count":
        //            templateColumn.Aggregate = GridAggregateFunction.Count;
        //            templateColumn.FooterAggregateFormatString = templateColumn.Aggregate.ToString() + ": {0}";
        //            break;
        //        case "Max":
        //            templateColumn.Aggregate = GridAggregateFunction.Max;
        //            break;
        //        case "Min":
        //            templateColumn.Aggregate = GridAggregateFunction.Min;
        //            break;
        //    }
        //}
        //templateColumn.FooterText = Footertext;


        grid.MasterTableView.Columns.Add(templateColumn);
    }
    #endregion

    //Add a Button Column to a teleik grid
    #region ButtonColumn
    public static void ButtonColumn(RadGrid grid, String HeaderText, String UniqueName, String groupName, HorizontalAlign Alignment, int Width, String buttonText, GridButtonColumnType buttontype, String commandName, String imageURL, Boolean colDisplay)
    {
        GridButtonColumn buttonColum = new GridButtonColumn();
        buttonColum.HeaderText = HeaderText;
        buttonColum.UniqueName = UniqueName;
        if (groupName != String.Empty)
        {
            buttonColum.ColumnGroupName = groupName;
        }

        buttonColum.HeaderStyle.HorizontalAlign = Alignment;
        buttonColum.HeaderStyle.Width = Width;
        buttonColum.Display = colDisplay;
        buttonColum.Text = buttonText;
        buttonColum.ButtonType = buttontype;
        buttonColum.CommandName = commandName;
        buttonColum.ImageUrl = imageURL;


        grid.MasterTableView.Columns.Add(buttonColum);
    }
    #endregion

    #endregion

    # region TemplateColumnClass
    public class MyTemplate : ITemplate
    {
        #region Variables

        // controls
        protected RequiredFieldValidator validatorTextBox;
        protected TextBox textBox;
        protected CheckBox boolValue;
        protected LinkButton linkbutton;
        protected DropDownList combobox;
        protected RadTextBox searchTextBox;
        protected RadTextBox searchTextBoxNOBRDR;
        protected RadNumericTextBox numericTextBox;
        protected RadDatePicker radDate;
        protected Label label;
        protected System.Web.UI.WebControls.Image searchImg;

        //loacl variable
        private String colname;
        private String Columntype;
        private int cntrlwidth;
        private String cntrlId;
        private String CmdName;
        private Boolean chkLblVisible;
        # endregion

        #region bindControls

        public MyTemplate(string cName, String Ctype, int controlWidth, String ControlID, String CommandName, Boolean chklableVisible)
        {
            colname = cName;
            Columntype = Ctype;
            cntrlwidth = controlWidth;
            cntrlId = ControlID;
            CmdName = CommandName;
            chkLblVisible = chklableVisible;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (Columntype)
            {
                case "TextBox":

                    textBox = new TextBox();
                    textBox.ID = cntrlId;
                    textBox.Width = cntrlwidth-6;
                    textBox.BorderWidth = 0;
                    textBox.DataBinding += new EventHandler(textBox_DataBinding);
                    //validatorTextBox = new RequiredFieldValidator();
                    //validatorTextBox.ControlToValidate = cntrlId;
                    //validatorTextBox.ErrorMessage = "*";

                    container.Controls.Add(textBox);
                    ///container.Controls.Add(validatorTextBox);
                    break;

                case "SearchTextBox":

                    searchTextBox = new RadTextBox();
                    searchTextBox.ID = cntrlId;
                    searchTextBox.Width = cntrlwidth - 20;
                    searchTextBox.ShowButton = false;
                    searchTextBox.DataBinding += new EventHandler(searchtextBox_DataBinding);
                    searchTextBox.BorderWidth = 0;

                    searchImg = new System.Web.UI.WebControls.Image();
                    searchImg.ID = "img" + cntrlId;
                    searchImg.ImageUrl = "~/Images/icFind.gif";
                    searchImg.Attributes["style"] = "cursor:hand";

                    container.Controls.Add(searchTextBox);
                    container.Controls.Add(searchImg);
                    //container.Controls.Add(validatorTextBox);
                    break;
                case "SearchTextBoxNOBORDER":

                    searchTextBoxNOBRDR = new RadTextBox();
                    searchTextBoxNOBRDR.ID = cntrlId;
                    searchTextBoxNOBRDR.Width = cntrlwidth - 4;
                    searchTextBoxNOBRDR.ShowButton = false;
                    searchTextBoxNOBRDR.BorderWidth = 0;
                    searchTextBoxNOBRDR.ReadOnly = true;
                    searchTextBoxNOBRDR.DataBinding += new EventHandler(searchtextBoxNoBORDER_DataBinding);
                    //validatorTextBox = new RequiredFieldValidator();
                    //validatorTextBox.ControlToValidate = cntrlId;
                    //validatorTextBox.ErrorMessage = "*";
                    container.Controls.Add(searchTextBoxNOBRDR);
                    //container.Controls.Add(validatorTextBox);
                    break;
                case "NumericTextBox":

                    numericTextBox = new RadNumericTextBox();
                    numericTextBox.ID = cntrlId;
                    numericTextBox.Width = cntrlwidth-6;
                    numericTextBox.AutoCompleteType = AutoCompleteType.None;
                    numericTextBox.Type = NumericType.Number;
                    numericTextBox.ShowSpinButtons = false;
                    numericTextBox.AllowOutOfRangeAutoCorrect = false;
                    numericTextBox.BorderWidth = 0;
                    // numericTextBox.InvalidStyle.Font = false;
                    numericTextBox.NumberFormat.AllowRounding = false;
                    numericTextBox.NumberFormat.DecimalDigits = 3;
                    numericTextBox.NumberFormat.KeepNotRoundedValue = true;

                    numericTextBox.DataBinding += new EventHandler(numerictextBox_DataBinding);

                    container.Controls.Add(numericTextBox);

                    break;

                case "LinkButton":

                    linkbutton = new LinkButton();
                    linkbutton.ID = cntrlId;
                    linkbutton.Width = cntrlwidth-6;
                    linkbutton.CommandName = CmdName;
                    linkbutton.DataBinding += new EventHandler(linkbutton_DataBinding);
                    container.Controls.Add(linkbutton);
                    break;

                case "CheckBox":


                    boolValue = new CheckBox();
                    boolValue.ID = cntrlId;
                    boolValue.DataBinding += new EventHandler(boolValue_DataBinding);

                    if (chkLblVisible == true)
                    {
                        label = new Label();
                        label.ID = "lbl_" + cntrlId;
                        label.Width = cntrlwidth-6;
                       
                        label.DataBinding += new EventHandler(label_DataBinding);

                        container.Controls.Add(label);
                    }

                    container.Controls.Add(boolValue);
                    break;

                case "ComboBox":
                    combobox = new DropDownList();
                    combobox.ID = cntrlId;
                    combobox.Width = cntrlwidth-8;
                    combobox.AutoPostBack = false;
                    container.Controls.Add(combobox);
                    break;

                case "RadDatePicker":

                    radDate = new RadDatePicker();
                    radDate.ID = cntrlId;
                    radDate.Width = cntrlwidth - 6;
                    radDate.EnableScreenBoundaryDetection = false;
                    radDate.DateInput.DateFormat = "yyyy-MM-dd";

                    radDate.DataBinding += new EventHandler(radDate_DataBinding);
                    container.Controls.Add(radDate);
                    break;

                case "Label":

                    label = new Label();
                    label.ID = cntrlId;
                    label.Width = cntrlwidth-6;
                    label.DataBinding += new EventHandler(label_DataBinding);

                    container.Controls.Add(label);
                    break;
            }
        }
        void boolValue_DataBinding(object sender, EventArgs e)
        {
            //  CheckBox cBox = (CheckBox)sender;
            //  GridDataItem container = (GridDataItem)cBox.NamingContainer;
            ////  cBox.Checked = (bool)((DataRowView)container.DataItem)["id"];
        }
        public void label_DataBinding(object sender, EventArgs e)
        {
            Label lbltxt = (Label)sender;
            GridDataItem container = (GridDataItem)lbltxt.NamingContainer;
            lbltxt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }

        public void textBox_DataBinding(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridDataItem container = (GridDataItem)txt.NamingContainer;
            txt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }
        public void searchtextBoxNoBORDER_DataBinding(object sender, EventArgs e)
        {
            RadTextBox txt = (RadTextBox)sender;
            GridDataItem container = (GridDataItem)txt.NamingContainer;
            txt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }

        public void searchtextBox_DataBinding(object sender, EventArgs e)
        {
            RadTextBox txt = (RadTextBox)sender;
            GridDataItem container = (GridDataItem)txt.NamingContainer;
            txt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }

        public void numerictextBox_DataBinding(object sender, EventArgs e)
        {
            RadNumericTextBox txt = (RadNumericTextBox)sender;
            GridDataItem container = (GridDataItem)txt.NamingContainer;
            txt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }

        public void linkbutton_DataBinding(object sender, EventArgs e)
        {
            LinkButton txt = (LinkButton)sender;
            GridDataItem container = (GridDataItem)txt.NamingContainer;
            txt.Text = ((DataRowView)container.DataItem)[colname].ToString();
        }

        public void radDate_DataBinding(object sender, EventArgs e)
        {
            RadDatePicker dtepicket = (RadDatePicker)sender;
            GridDataItem container = (GridDataItem)dtepicket.NamingContainer;
            if (((DataRowView)container.DataItem)[colname].ToString() != String.Empty)
            {
                dtepicket.SelectedDate = Convert.ToDateTime(((DataRowView)container.DataItem)[colname].ToString());
            }
        }

        # endregion

    }
    # endregion
}
//Enam decalaration for teleric Column Type ex like Textbox Column ,LinkButton Column
public enum TelerikControlType { TextBox, ComboBox, CheckBox, LinkButton, SearchTextBox, SearchTextBoxNOBORDER, NumericTextBox, RadDatePicker, Label, None, DIV,Image }

//Enum declaration for TelerikGrid Client Events
public enum clientEventType { gridCreated, rowClicked, rowDblClick, SearcButtonClick, ColumnClick, OnRowSelected, OnKeyPress, CellClicked }