﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;


namespace CoachV2
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private int cim_num;
        private string rolevalue;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.ClearHeaders();
            //Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            //Response.AddHeader("Pragma", "no-cache");
            //Response.AddHeader("Expires", "0");

            var mark1 = 0;
            var mark2 = 0;
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
 
            string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            string cim_num =   ds.Tables[0].Rows[0][0].ToString();
            int intcim = Convert.ToInt32(cim_num);

            // DataSet ds_getrolefromsap = DataHelper.GetEmployeeRoleFromSapDetails(emp_email); // for actual roles 
            DataSet ds_getrolefromsap = DataHelper.getuserrolefromsap(intcim);
            DataSet ds_userselect = DataHelper.GetAllUserRoles(intcim);
            if (ds_userselect.Tables[0].Rows.Count > 0)
            {
                DataSet ds_userselect1 = DataHelper.GetUserRolesAssigned(intcim, 1);
                mark1 = 1;

                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(intcim, 2);
                mark2 = 1;

            }
            rolevalue = Convert.ToString(ds_getrolefromsap.Tables[0].Rows[0]["Role"].ToString());

            string saprolefordashboard = DataHelper.MyRoleInSAP(Convert.ToInt32(intcim));

                get_hierarchy();         

                if (Request.QueryString["tab"] == "team")
                {

                    LoadMyTeam();
                    //AgentMain.Visible = false;
                    //TLMainDB.Visible = false;
                    //OMDashboard1.Visible = false;
                    //UControlDir1.Visible = false;
                    //QADashboard1.Visible = false;
                    //HRDashboard1.Visible = false;
                    //QADashboardT.Visible = false;
                    //HRDashboardwithTriad1.Visible = false;
                }
                else if (Request.QueryString["tab"] == "myreviews")
                {

                    DataSet ds_subordinates = DataHelper.Get_Subordinates(intcim); //check if user has subordinates
                    if (ds_subordinates.Tables[0].Rows.Count > 0)
                    { //enable my reviews view for users with subordinates
                        //DashboardMyReviewsUserControl1.Visible = true;
                        //MyReviewRecentlyReviewedUserCtrl1.Visible = true;
                        //MyReviewOpenForMeUserCtrl1.Visible = true;
                        //AgentMain.Visible = false;
                        //TLMainDB.Visible = false;
                        //OMDashboard1.Visible = false;
                        //UControlDir1.Visible = false;
                        //QADashboard1.Visible = false;
                        //HRDashboard1.Visible = false;
                        //QADashboardT.Visible = false;
                        //HRDashboardwithTriad1.Visible = false;
                        DashboardDiv.Controls.Clear();
                        DashboardMyReviews DashboardMyReviews = (DashboardMyReviews)LoadControl("UserControl/DashboardMyReviews.ascx");
                        DashboardDiv.Controls.Add(DashboardMyReviews);

                        // inherited agentdashboardmain for non-agent review list (francis.valera/07172018)
                        if (rolevalue.IndexOf("VP") < 0)
                        {
                            AgentDashboardMyReview AgentDashboardMain = (AgentDashboardMyReview)LoadControl("UserControl/AgentDashboardMyReview.ascx");
                            DashboardDiv.Controls.Add(AgentDashboardMain);
                        }

                        MyReviewRecentlyReviewedUserCtrl MyReviewRecentlyReviewedUserCtrl = (MyReviewRecentlyReviewedUserCtrl)LoadControl("UserControl/MyReviewRecentlyReviewedUserCtrl.ascx");
                        DashboardDiv.Controls.Add(MyReviewRecentlyReviewedUserCtrl);

                        MyReviewOpenForMeUserCtrl MyReviewOpenForMeUserCtrl = (MyReviewOpenForMeUserCtrl)LoadControl("UserControl/MyReviewOpenForMeUserCtrl.ascx");
                        DashboardDiv.Controls.Add(MyReviewOpenForMeUserCtrl);
                    }
                    else
                    {
                        //my reviews agent view
                        //AgentMyReview.Visible = true;
                        //AgentMain.Visible = false;
                        //TLMainDB.Visible = false;
                        //OMDashboard1.Visible = false;
                        //UControlDir1.Visible = false;
                        //QADashboard1.Visible = false;
                        //HRDashboard1.Visible = false;
                        //QADashboardT.Visible = false;
                        //HRDashboardwithTriad1.Visible = false;
                        DashboardDiv.Controls.Clear();
                        AgentDashboardMyReview AgentDashboardMain = (AgentDashboardMyReview)LoadControl("UserControl/AgentDashboardMyReview.ascx");
                        DashboardDiv.Controls.Add(AgentDashboardMain);
                    }
                }
                else
                {
                    //DashboardAgentRosterUserCtrl1.Visible = false;
                    //AgentMain.Visible = false;
                    ////TLChartsDB.Visible = false;
                    //TLMainDB.Visible = false;
                    //UControlDir1.Visible = false;
                    //QAChartsDB.Visible = false;
                    //OMDirChartsDB.Visible = false;
                    //QADashboard1.Visible = false;
                    //HRDashboard1.Visible = false;
                }
            }
       
        public void LoadMyTeam()
        {
            //asd

            DataSet dsUserInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

            cim_num = Convert.ToInt32(dsUserInfo.Tables[0].Rows[0]["CIM_Number"]);
            string Role = DataHelper.MyRoleInSAP(cim_num);
            DataAccess ws2 = new DataAccess();
            string department = "";
            department = ws2.getuserdept(cim_num);

            if (rolevalue.ToUpper().Contains("VP") == true)
            {
                DashboardDiv.Controls.Clear();
                DashboardDirRosterUserCtrl dirxx = (DashboardDirRosterUserCtrl)LoadControl("UserControl/DashboardDirRosterUserCtrl.ascx");
                DashboardDiv.Controls.Add(dirxx);
            }
            else
            {

                if (Role == "TL")
                {
                    //DashboardAgentRosterUserCtrl1.Visible = true;
                    DashboardDiv.Controls.Clear();
                    DashboardAgentRosterUserCtrl DashboardAgentRosterUserCtrl = (DashboardAgentRosterUserCtrl)LoadControl("UserControl/DashboardAgentRosterUserCtrl.ascx");
                    DashboardDiv.Controls.Add(DashboardAgentRosterUserCtrl);
                }

                if (Role == "OM")
                {
                    DashboardDiv.Controls.Clear();
                    //DashboardAgentRosterUserCtrl1.Visible = false;
                    DashboardTLRosterUserCtrl Test2 = (DashboardTLRosterUserCtrl)LoadControl("UserControl/DashboardTLRosterUserCtrl.ascx");
                    MyTeam.Controls.Add(Test2);
                }

                if (Role == "Dir")
                {
                    DashboardDiv.Controls.Clear();
                    //DashboardAgentRosterUserCtrl1.Visible = false;
                    DashboardOMRosterUserCtrl Test2 = (DashboardOMRosterUserCtrl)LoadControl("UserControl/DashboardOMRosterUserCtrl.ascx");
                    MyTeam.Controls.Add(Test2);
                }

                if (Role == "QA")
                {
                    //DashboardAgentRosterUserCtrl1.Visible = true;
                    DashboardAgentRosterUserCtrl DashboardAgentRosterUserCtrl = (DashboardAgentRosterUserCtrl)LoadControl("UserControl/DashboardAgentRosterUserCtrl.ascx");
                    DashboardDiv.Controls.Add(DashboardAgentRosterUserCtrl);
                }
            }
             

            //DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            ////string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            ////string cim_num = ds.Tables[0].Rows[0][0].ToString();
            ////int intcim = Convert.ToInt32(cim_num);
            //////testing purposes for sir bart's access 
            //string emp_email = "ROMEL.BARATETA@TRANSCOM.COM";//"TRACEY.RUSTIA@TRANSCOM.COM";//"ROMEL.BARATETA@TRANSCOM.COM";//ds.Tables[0].Rows[0]["Email"].ToString(); //"MARIA.CASTANEDA@TRANSCOM.COM";//"MARVIN.REGALADO@TRANSCOM.COM"; //
            //string cim_num = "10113944";//"95759"; //"10113944";  // ds.Tables[0].Rows[0][0].ToString();
            //int intcim = Convert.ToInt32(cim_num);//95759;//10113944;  //Convert.ToInt32(cim_num);
            ////testing purposes for sir bart's access 

            //DataSet ds_hassubs = DataHelper.CheckforSubs(Convert.ToInt32(intcim));
            //DataSet ds1 = null;
            //DataAccess ws1 = new DataAccess();
            //int userhierarchy;
            //userhierarchy = ws1.getroleofloggedin_user(intcim);

            //DataAccess ws2 = new DataAccess();
            //string department = "";
            //department = ws2.getuserdept(intcim);

            //int deptid, roleid;
            //DataSet ds_userdept = DataHelper.get_adminusername(emp_email);
            //if (ds_userdept.Tables[0].Rows.Count > 0)
            //{
            //    deptid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["accountid"].ToString());
            //    roleid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["roleid"].ToString());
            //}
            //else
            //{
            //    deptid = 0;
            //    roleid = 0;
            //}
            //DataSet ds_checkhierarchy = DataHelper.checkuserhierarchy(intcim);
            //if (ds_checkhierarchy.Tables[0].Rows.Count > 0)
            //{
            //    if (deptid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["dept_id"].ToString()) && (roleid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["role_id"].ToString())))
            //    { }
            //    else
            //    {
            //        //update user hierarchy
            //        DataHelper.Edituserhierarchy(intcim, deptid, roleid, userhierarchy);
            //    }
            //}

            //else
            //{
            //    DataHelper.Insertuserhierarchy(intcim, deptid, roleid, userhierarchy);

            //}
            //if (department == "HR")
            //{
            //    DashboardDiv.Controls.Clear();
            //    //DashboardAgentRosterUserCtrl1.Visible = false;
            //    DashboardOMRosterUserCtrl Test2 = (DashboardOMRosterUserCtrl)LoadControl("UserControl/DashboardOMRosterUserCtrl.ascx");
            //    MyTeam.Controls.Add(Test2);
            //}
            //else if (department == "QA")
            //{
            //    DashboardDiv.Controls.Clear();
            //    //DashboardAgentRosterUserCtrl1.Visible = true;
            //    DashboardAgentRosterUserCtrl DashboardAgentRosterUserCtrl = (DashboardAgentRosterUserCtrl)LoadControl("UserControl/DashboardAgentRosterUserCtrl.ascx");
            //    DashboardDiv.Controls.Add(DashboardAgentRosterUserCtrl);
            //}
            //else
            //{
            //    if (userhierarchy == 2)
            //    { //tl
            //        DashboardDiv.Controls.Clear();
            //        DashboardAgentRosterUserCtrl DashboardAgentRosterUserCtrl = (DashboardAgentRosterUserCtrl)LoadControl("UserControl/DashboardAgentRosterUserCtrl.ascx");
            //        DashboardDiv.Controls.Add(DashboardAgentRosterUserCtrl);

            //    }
            //    else if (userhierarchy == 3)
            //    {
            //        //om
            //        DashboardDiv.Controls.Clear();
            //        //DashboardAgentRosterUserCtrl1.Visible = false;
            //        DashboardTLRosterUserCtrl Test2 = (DashboardTLRosterUserCtrl)LoadControl("UserControl/DashboardTLRosterUserCtrl.ascx");
            //        MyTeam.Controls.Add(Test2);
            //    }
            //    else if (userhierarchy == 4)
            //    {   //dir
            //        DashboardDiv.Controls.Clear();
            //        //DashboardAgentRosterUserCtrl1.Visible = false;
            //        DashboardOMRosterUserCtrl Test2 = (DashboardOMRosterUserCtrl)LoadControl("UserControl/DashboardOMRosterUserCtrl.ascx");
            //        MyTeam.Controls.Add(Test2);
            //    }
               
            //}

        }

        protected void get_hierarchy()
        {
            DataSet ds = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string emp_email = ds.Tables[0].Rows[0]["Email"].ToString();
            string cim_num =  ds.Tables[0].Rows[0][0].ToString();
            int intcim = Convert.ToInt32(cim_num);

            DataSet ds_hassubs = DataHelper.CheckforSubs(Convert.ToInt32(intcim));
            DataSet ds1 = null;
            DataAccess ws1 = new DataAccess();
            int userhierarchy;
            userhierarchy = ws1.getroleofloggedin_user(intcim);

            DataAccess ws2 = new DataAccess();
            string department = "";
            department = ws2.getuserdept(intcim);

            int deptid, roleid;
            DataSet ds_userdept = DataHelper.get_adminusername(emp_email);
            if (ds_userdept.Tables[0].Rows.Count > 0)
            {
                deptid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["accountid"].ToString());
                roleid = Convert.ToInt32(ds_userdept.Tables[0].Rows[0]["roleid"].ToString());
            }
            else
            {
                deptid = 0;
                roleid = 0;
            }
            DataSet ds_checkhierarchy = DataHelper.checkuserhierarchy(intcim);
            if (ds_checkhierarchy.Tables[0].Rows.Count > 0)
            {
                if (deptid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["dept_id"].ToString()) && (roleid == Convert.ToInt32(ds_checkhierarchy.Tables[0].Rows[0]["role_id"].ToString())))
                { }
                else
                {
                    //update user hierarchy
                    DataHelper.Edituserhierarchy(intcim, deptid, roleid, userhierarchy);
                }
            }

            else
            {
                DataHelper.Insertuserhierarchy(intcim, deptid, roleid, userhierarchy);

            }

            if (department == "HR")
            {
                if ((userhierarchy == 1) || (userhierarchy == 2))
                { //w/o triad

                    DashboardDiv.Controls.Clear();
                    HRDashboard HRDashboard = (HRDashboard)LoadControl("UserControl/HRDashboard.ascx");
                    DashboardDiv.Controls.Add(HRDashboard);
                }
                else if ((userhierarchy == 3) || (userhierarchy == 4))
                {//w/ triad
                    DashboardDiv.Controls.Clear();
                    HR_DashboardwithTriad HR_DashboardwithTriad = (HR_DashboardwithTriad)LoadControl("UserControl/HR_DashboardwithTriad.ascx");
                    DashboardDiv.Controls.Add(HR_DashboardwithTriad);
                }
                else
                {
                    DashboardDiv.Controls.Clear();
                    HRDashboard HRDashboard = (HRDashboard)LoadControl("UserControl/HRDashboard.ascx");
                    DashboardDiv.Controls.Add(HRDashboard);
                }
            }
            else if (department == "QA")
            {
                if ((userhierarchy == 1) || (userhierarchy == 2))
                { //w/o triad
                    DashboardDiv.Controls.Clear();
                    QADashboard QADashboard = (QADashboard)LoadControl("UserControl/QADashboard.ascx");
                    DashboardDiv.Controls.Add(QADashboard);
                }
                else if ((userhierarchy == 3) || (userhierarchy == 4))
                {//w/ triad
                    DashboardDiv.Controls.Clear();
                    QA_DashboardwithTriad QA_DashboardwithTriad = (QA_DashboardwithTriad)LoadControl("UserControl/QA_DashboardwithTriad.ascx");
                    DashboardDiv.Controls.Add(QA_DashboardwithTriad);
                }
                else
                { //w/o triad
                    DashboardDiv.Controls.Clear();
                    QADashboard QADashboard = (QADashboard)LoadControl("UserControl/QADashboard.ascx");
                    DashboardDiv.Controls.Add(QADashboard);


                }
            }
            else
            {
                if (rolevalue.ToUpper().Contains("VP") == true)
                {
                    DashboardDiv.Controls.Clear();
                    VPDashboardMain vpx = (VPDashboardMain)LoadControl("UserControl/VPDashboardMain.ascx");
                    DashboardDiv.Controls.Add(vpx);
                } else {

                    if (userhierarchy == 2)
                    { //tl
                        DashboardDiv.Controls.Clear();
                        TLReportsDashboard TLReportsDashboard = (TLReportsDashboard)LoadControl("UserControl/TLMainDashboard.ascx");
                        DashboardDiv.Controls.Add(TLReportsDashboard);

                    }
                    else if (userhierarchy == 3)
                    {
                        //om
                        // added for special access (francis.valera/08032018)
                        DashboardDiv.Controls.Clear();
                        if (DataHelper.UserHasSpecialAccess(intcim))
                        {
                            //dir
                            DirDashboardMain DirDashboardMain = (DirDashboardMain)LoadControl("UserControl/DirDashboardMain.ascx");
                            DashboardDiv.Controls.Add(DirDashboardMain);
                        }
                        else
                        {
                            OMDashboardMain OMDashboardMain = (OMDashboardMain)LoadControl("UserControl/OMDashboardMain.ascx");
                            DashboardDiv.Controls.Add(OMDashboardMain);
                        }
                        //DashboardDiv.Controls.Clear();
                        //OMDashboardMain OMDashboardMain = (OMDashboardMain)LoadControl("UserControl/OMDashboardMain.ascx");
                        //DashboardDiv.Controls.Add(OMDashboardMain);
                    }
                    else if (userhierarchy == 4)
                    {   //dir
                        DashboardDiv.Controls.Clear();
                        DirDashboardMain DirDashboardMain = (DirDashboardMain)LoadControl("UserControl/DirDashboardMain.ascx");
                        DashboardDiv.Controls.Add(DirDashboardMain);
                    }
                    else
                    {  //agent         
                        DashboardDiv.Controls.Clear();
                        AgentDashboardMain AgentDashboardMain = (AgentDashboardMain)LoadControl("UserControl/AgentDashboardMain.ascx");
                        DashboardDiv.Controls.Add(AgentDashboardMain);
                    }
                }
            }

            //testing purposes
            //if (intcim == 10107032)
            //{
            //    DashboardDiv.Controls.Clear();
            //    QADashboard QADashboard = (QADashboard)LoadControl("UserControl/QADashboard.ascx");
            //    DashboardDiv.Controls.Add(QADashboard);
            //}
          
            
        }
    }

}