﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace CoachV2
{
    public partial class AddReviewHR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);

            if (!IsPostBack)
            {

                if (Convert.ToInt32(RadReviewID.Text) != 0)
                {
                    // ((Page.Request.QueryString["CoachingTicket"] != null)) {
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadFollowup.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadHRComments.Enabled = false;
                    UploadDummy.Enabled = false;
                    DataSet ds_review_details = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                    RadAccount.Text = Convert.ToString(ds_review_details.Tables[0].Rows[0]["account"].ToString());
                    RadSupervisor.Text = Convert.ToString(ds_review_details.Tables[0].Rows[0]["Supervisor name"].ToString());
                    RadCoachingDate.Text = Convert.ToString(ds_review_details.Tables[0].Rows[0]["CreatedOn"].ToString());
                    RadFollowup.SelectedDate = Convert.ToDateTime(ds_review_details.Tables[0].Rows[0]["FollowDate"].ToString());
                    RadSessionType.Text = Convert.ToString(ds_review_details.Tables[0].Rows[0]["SessionName"]);
                    RadSessionTopic.Text = Convert.ToString(ds_review_details.Tables[0].Rows[0]["topicname"]);
                    RadHRComments.Text = ds_review_details.Tables[0].Rows[0][""].ToString();
                    RadGridDocumentation.Visible = true;
                    LoadDocumentations();

                }
                else
                {
                    DataSet ds1 = null;
                    DataAccess ws1 = new DataAccess();
                    ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                    GetAccounts(CIMNumber);
                    RadAccount.SelectedValue = Convert.ToString(ds1.Tables[0].Rows[0]["Dept Id"].ToString());
                    GetDropDownSessions();
                    Session["Account"] = Convert.ToString(ds1.Tables[0].Rows[0]["Account"].ToString());
                }
            }
            else
            {

                //DataSet ds2 = null;
                //DataAccess ws2 = new DataAccess();
                //ds2 = ws2.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                //GetAccounts(CIMNumber);
                //RadAccount.SelectedValue = Convert.ToString(ds2.Tables[0].Rows[0]["Dept Id"].ToString());
                //GetDropDownSessions();
                //Session["Account"] = Convert.ToString(ds2.Tables[0].Rows[0]["Account"].ToString());

            }

        }

        public void GetTLsPerAccount(int AccountID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount(AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetTLsPerAccount " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetTLsPerAccount " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void GetSubordinates(int CimNumber)
        {
            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinates(CimNumber);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("GetSubordinates " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
      
        protected void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownSessions " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetDropDownSessions " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void PopulateTopics()
        {
            try
            {
                int SessionID;
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                GetDropDownTopics(SessionID);
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("PopulateTopics " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "PopulateTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void GetDropDownTopics(int SessionID)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                SessionID = Convert.ToInt32(RadSessionType.SelectedValue);
                ds = ws.GetSessionTopics2(SessionID, CIMNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownTopics " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void GetAccounts(int CimNumber)
        {
            try
            {
            //    DataTable dt1 = null;
            //    DataAccess ws1 = new DataAccess();
            //    dt1 = ws1.GetSubordinates(CimNumber);

            //    var distinctRows = (from DataRow dRow in dt1.Rows
            //                        select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

            //    RadAccount.Items.Clear();

            //    foreach (var row in distinctRows)
            //    {
            //        RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
            //    }
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "Account";
                RadAccount.DataValueField = "AccountID";
                RadAccount.DataBind();
            }
            catch (Exception ex)
            {
               // RadWindowManager1.RadAlert("GetAccounts " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetAccounts " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
            }

        }
        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
           
            try
            {
                
                LoadTTSupervisors(Convert.ToInt32(RadAccount.SelectedValue));
                RadCoacheeName.Text = string.Empty;
                RadCoacheeName.ClearSelection();
                RadCoacheeName.SelectedIndex = -1;
                RadSupervisor.Text = string.Empty;
                RadSupervisor.ClearSelection();
                RadSupervisor.SelectedIndex = -1;
                RadSessionType.Text = string.Empty;
                RadSessionType.ClearSelection();
                RadSessionType.SelectedIndex = -1;
                RadSessionTopic.Text = string.Empty;
                RadSessionTopic.ClearSelection();
                RadSessionTopic.SelectedIndex = -1;
                GetDropDownSessions();


            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadAccount_SelectedIndexChanged " + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadAccount_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void UploadDummy_Click(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
            //}

        }

        public void UploadDocumentationReview(int MassCoachingType, int ReviewID)
        {
            try
            {

                foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentations();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
           
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("UploadDocumentationReview" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "UploadDocumentationReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
        }
       
        public void LoadTTSubordinates(int CimNumber, int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinatesWithAccounts(CimNumber, AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadTTSubordinates" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadTTSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadCoacheeName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()));


                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    HiddenCoacheeSSS.Value = ds.Tables[0].Rows[0]["SSN"].ToString();
                    //}
                    //RadCoacheeCIM.Text = RadCoacheeName.SelectedValue.ToString();
                    ////HiddenCoacheeCIM.Value = RadCoacheeName.SelectedValue.ToString();
                    //RadCoacheeCIM.Enabled = false;
                    //RadSSN.Visible = true;
                    //RadGridCN.DataSource = null;
                    //RadGridCN.Rebind();
                    //RadGridPR.DataSource = null;
                    //RadGridPR.Rebind();
                    //RadGrid6.DataSource = null;
                    //RadGrid6.Rebind();
                    //RadGrid11.DataSource = null;
                    //RadGrid11.Rebind();
                    //RadGrid7.DataSource = null;
                    //RadGrid7.Rebind();
                    //if (CheckBox1.Visible == true)
                    //{
                    //    if (CheckBox1.Checked == true)
                    //    {
                    //        CheckBox1.Checked = false;
                    //    }
                    //}
                    //if (CheckBox2.Visible == true)
                    //{
                    //    if (CheckBox2.Checked == true)
                    //    {
                    //        CheckBox2.Checked = false;
                    //    }
                    //}
                }
                else
                {
                    //RadCoacheeCIM.Text = "";
                    //RadCoacheeCIM.Enabled = true;
                    //RadSSN.Visible = false;
                    //RadGridCN.DataSource = null;
                    //RadGridCN.Rebind();
                    //RadGridPR.DataSource = null;
                    //RadGridPR.Rebind();
                    //RadGrid6.DataSource = null;
                    //RadGrid6.Rebind();
                    //RadGrid11.DataSource = null;
                    //RadGrid11.Rebind();
                    //RadGrid7.DataSource = null;
                    //RadGrid7.Rebind();
                    //if (CheckBox1.Visible == true)
                    //{
                    //    if (CheckBox1.Checked == true)
                    //    {
                    //        CheckBox1.Checked = false;
                    //    }
                    //}
                    //if (CheckBox2.Visible == true)
                    //{
                    //    if (CheckBox2.Checked == true)
                    //    {
                    //        CheckBox2.Checked = false;
                    //    }
                    //}

                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadCoacheeName_SelectedIndexChanged" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadCoacheeName_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
      
        private bool IsPH(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }
        }

        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            GetDropDownTopics(Convert.ToInt32(RadSessionType.SelectedValue), CIMNumber);
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;

            if (Request["CIM"] != null)
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                if (ds.Tables[0].Rows.Count > 0)
                {
                  //  GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));
                }
            }

        }

        protected void GetDropDownTopics(int SessionID, int CimNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID, CimNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        public void LoadTTSupervisors(int AccountID)
        {
            try
            {
                //DataSet ds = null;
                //DataAccess ws = new DataAccess();
                //ds = ws.GetTeamLeaderPerAccount(AccountID);

                //RadSupervisor.DataSource = ds;
                //RadSupervisor.DataTextField = "Name";
                //RadSupervisor.DataValueField = "CimNumber";
                //RadSupervisor.DataBind();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

                int CIMNumber = Convert.ToInt32(cim_num);

                string Role;
                Role = CheckIfHRQA(CIMNumber);

                if (Role == "HR")
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetTeamLeaderPerAccount(AccountID);

                    RadSupervisor.DataSource = ds;
                    RadSupervisor.DataTextField = "Name";
                    RadSupervisor.DataValueField = "CimNumber";
                    RadSupervisor.DataBind();
                }
                else
                {
                    GetSubordinatesWithTeam(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadAccount.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadTTSupervisors" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadTTSupervisors " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected string CheckIfHRQA(int CimNumber)
        {

            DataAccess ws = new DataAccess();
            string Role = ws.CheckIfQAHR(CimNumber);
            if (Role != "")
            {
                return Role;
            }
            else
            {
                return Role;
            }
        }

        protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        {
            try
            {
                string Role;
                Role = CheckIfHRQA(CimNumber);

                if (Role == "HR")
                {
                    GetTLsPerAccount(AccountID);
                }
                else
                {

                    RadSupervisor.Items.Clear();
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetSubordinatesWithTeam(CimNumber, AccountID);
                    RadSupervisor.DataSource = ds;
                    RadSupervisor.DataTextField = "Name";
                    RadSupervisor.DataValueField = "CimNumber";
                    RadSupervisor.DataBind();

                    DataSet dsInfo = null;
                    DataAccess wsInfo = new DataAccess();
                    dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
                    if (dsInfo.Tables[0].Rows.Count > 0)
                    {
                        string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                        string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                        string FullName = FirstName + " " + LastName;
                        RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSubordinatesWithTeam" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinatesWithTeam " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        protected void btn_SendtoTl_OnClick(object sender, EventArgs e)
        {   DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            int ReviewID;
            DataAccess ws = new DataAccess();
            ReviewID = ws.InsertHRReview(   Convert.ToInt32(RadCoacheeName.SelectedValue),
                                            Convert.ToInt32(RadAccount.SelectedValue), 
                                            Convert.ToInt32(RadSupervisor.SelectedValue), 
                                            Convert.ToInt32(RadSessionTopic.SelectedValue), 
                                            RadFollowup.SelectedDate, 
                                            RadHRComments.Text,
                                            2,
                                            CIMNumber,
                                            6,
                                            4);
            RadReviewID.Text = Convert.ToString(ReviewID);
            UploadDocumentation(3,  Convert.ToInt32(RadReviewID.Text));
            btn_SendtoTl.Enabled = false;
            string ModalLabel = "Coachee has signed off from this review.";
            string ModalHeader = "Success";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        }

        public void LoadDocumentations()
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 1);
                 
                    RadGridDocumentation.DataSource = ds;
                    RadGridDocumentation.Rebind();
              
                
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("LoadDocumentationsReview" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }

        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload1.ClientID), true);
        }

        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }

        protected void btn_ExporttoPDF_Click(object sender, EventArgs e) {
            if (Convert.ToInt32(RadReviewID.Text) != 0) {
                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));

                Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                specificstable.AddCell(specificscell);
                //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                //specificscelli00.Border = 0;
                //                   specificscelli00.Rowspan = 7;


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs)));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text)));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name "));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"])));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor "));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department "));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"])));
                specificscelli8.Border = 0;

                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign "));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval)));
                specificscelli10.Border = 0;


                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic "));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  "));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])));
                specificscelli14.Border = 0;


                Paragraph p1 = new Paragraph();
                string line1 = "HR Comments " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HRComments"]);
                p1.Alignment = Element.ALIGN_LEFT;
                p1.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                p1.Add(line1);
                p1.SpacingBefore = 10;
                p1.SpacingAfter = 10;

                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 10;
                p13.SpacingAfter = 10;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;


                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                            notescellh1.Border = 0;
                            table.AddCell(notescellh1);
                            PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                            notescellh2.Border = 0;
                            table.AddCell(notescellh2);
                            PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                            notescellh3.Border = 0;
                            table.AddCell(notescellh3);
                            PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                            notescellh4.Border = 0;
                            table.AddCell(notescellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"])));
                        notescelli1.Border = 0;
                        table.AddCell(notescelli1);


                        PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"])));
                        notescelli2.Border = 0;
                        table.AddCell(notescelli2);

                        PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"])));
                        notescelli3.Border = 0;
                        table.AddCell(notescelli3);

                        PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"])));
                        notescelli4.Border = 0;
                        table.AddCell(notescelli4);


                    }
                }
                else
                {
                    PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                    notescellh1.Border = 0;
                    table.AddCell(notescellh1);
                    PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                    notescellh2.Border = 0;
                    table.AddCell(notescellh2);
                    PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                    notescellh3.Border = 0;
                    table.AddCell(notescellh3);
                    PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                    notescellh4.Border = 0;
                    table.AddCell(notescellh4);
                    PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded."));
                    notescellh5.Border = 0;
                    table.AddCell(notescellh5);
                    PdfPCell notescellh6 = new PdfPCell(new Phrase(""));
                    notescellh6.Border = 0;
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                }
                pdfDoc.Add(specificstable);
                pdfDoc.Add(new Paragraph(p1)); //HRComments
               
                pdfDoc.Close();

                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();

            }
            else
            {
                //string script = "alert('Please submit the review before creating a pdf.')";
                string ModalLabel = "Please submit the review before creating a pdf.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);    
         
            
            }
        
        }
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            VisibleCheckBox();
        }


        public void VisibleCheckBox()
        {
            try
            {
                if (Session["Account"] != null)
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    string Account = Session["Account"].ToString();
                    if (RadSessionTopic.SelectedItem.Text != "Awareness" && RadSessionTopic.SelectedItem.Text != "1st Warning")
                    {

                        if (RadSessionTopic.SelectedItem.Text == "Termination")
                        {
                            CheckBox1.Visible = true;
                            CheckBox2.Visible = true;
                            RadSessionTopic.Enabled = false;
                            RadSessionType.Enabled = false;

                            SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-5";
                            SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-5";
                            SelectCoacheeGroup.Attributes["style"] = "padding:0px";
                            SelectCoachingSpecifics.Attributes["style"] = "padding:0px";

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMT(); });", true);
                        }

                      
                    }
                }
                else
                {
                    RadSessionTopic.Text = string.Empty;
                    RadSessionTopic.ClearSelection();
                    RadSessionTopic.SelectedIndex = -1;
                    string myStringVariable = "Please select the account first.";
                    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("VisibleControls" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "VisibleCheckBox " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["Account"] != null)
            {
                string Account = Session["Account"].ToString();
                if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
                {

                    if (RadSessionType.SelectedIndex != -1 || RadSessionType.AllowCustomText == true)
                    {
                        //if (Account == "Default")
                        //{
                            if (CheckBox1.Checked == true)
                            {
                                CoachingNotes();
                                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                                DataSet ds = null;
                                DataAccess ws = new DataAccess();
                                //ds = ws.GetCoachingNotes(CoachingCIM);
                                int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                                ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    RadGrid2.DataSource = ds;
                                    RadGrid2.DataBind();
                                    RadGrid2.Visible = true;
                                    RadGrid3.Visible = false;
                                    RadPRLink.Visible = false;
                                    RadPRCancel.Visible = false;
                                    RadCNLink.Visible = true;
                                    RadCNCancel.Visible = true;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                    RadGridCN.Visible = true;

                                }
                                else
                                {
                                    // RadWindowManager1.RadAlert("CheckBox1_CheckedChanged" + myStringVariable + "", 500, 200, "Error Message", "", "");
                                    string ModalLabel = "No data for previous coaching notes.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }

                            }

                            else
                            {
                                //ViewState["CN"] = null;
                                RadGridCN.DataSource = null;
                                RadGridCN.DataBind();
                                RadGridCN.Visible = false;

                            }
                        //}
                        //else if (Account == "TalkTalk")
                        //{

                        //    if (CheckBox1.Checked == true)
                        //    {
                        //        CoachingNotes();
                        //        int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                        //        //int CoachingCIM = 10107032;
                        //        DataSet ds = null;
                        //        DataAccess ws = new DataAccess();
                        //        //ds = ws.GetCoachingNotes(CoachingCIM);
                        //        int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                        //        ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                        //        if (ds.Tables[0].Rows.Count > 0)
                        //        {
                        //            RadGrid2.DataSource = ds;
                        //            RadGrid2.DataBind();
                        //            RadGrid2.Visible = true;
                        //            RadGrid3.Visible = false;
                        //            //Window1.VisibleOnPageLoad = true;
                        //            RadPRLink.Visible = false;
                        //            RadPRCancel.Visible = false;
                        //            RadCNLink.Visible = true;
                        //            RadCNCancel.Visible = true;

                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                        //            RadGrid6.Visible = true;


                        //        }
                        //        else
                        //        {

                        //            string ModalLabel = "No data for previous performance results.";
                        //            //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        //            string ModalHeader = "Error Message";
                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        //        }
                        //    }

                        //    else
                        //    {
                        //        //ViewState["CN"] = null;
                        //        RadGrid6.DataSource = null;
                        //        RadGrid6.DataBind();
                        //        RadGrid6.Visible = false;


                        //    }
                        //}
                        //else
                        //{
                        //    if (CheckBox1.Checked == true)
                        //    {
                        //        CoachingNotes();
                        //        int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                        //        //int CoachingCIM = 10107032;
                        //        DataSet ds = null;
                        //        DataAccess ws = new DataAccess();
                        //        //ds = ws.GetCoachingNotes(CoachingCIM);
                        //        int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                        //        ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                        //        if (ds.Tables[0].Rows.Count > 0)
                        //        {
                        //            RadGrid2.DataSource = ds;
                        //            RadGrid2.DataBind();
                        //            RadGrid2.Visible = true;
                        //            RadGrid3.Visible = false;
                        //            //Window1.VisibleOnPageLoad = true;
                        //            RadPRLink.Visible = false;
                        //            RadPRCancel.Visible = false;
                        //            RadCNLink.Visible = true;
                        //            RadCNCancel.Visible = true;

                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                        //            RadGrid11.Visible = true;

                        //        }
                        //        else
                        //        {

                        //            string ModalLabel = "No data for previous performance results.";
                        //            //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        //            string ModalHeader = "Error Message";
                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        //        }
                        //    }

                        //    else
                        //    {
                        //        //ViewState["CN"] = null;
                        //        RadGrid11.DataSource = null;
                        //        RadGrid11.DataBind();
                        //        RadGrid11.Visible = false;

                        //        Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                        //        PreviousCommitment.Controls.Remove(Test2);

                        //    }
                        //}
                    }
                    else
                    {
                        CheckBox1.Checked = false;
                        string ModalLabel = "Please select the session type.";
                        //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                }
                else
                {
                    CheckBox1.Checked = false;
                    string ModalLabel = "Please select the employee first.";
                    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }
            }
            else
            {
                CheckBox1.Checked = false;
                string ModalLabel = "Please select the account first.";
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }

        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
            BindListViewCNCMT();
        }
        private void BindListViewCN()
        {
            string Account = Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (Account == "Default")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridCN.DataSource = dt;
                    RadGridCN.DataBind();
                }
                else
                {
                    RadGridCN.DataSource = null;
                    RadGridCN.DataBind();
                }
            }
            else if (Account == "TalkTalk")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    //RadGrid6.DataSource = dt;
                    //RadGrid6.DataBind();
                }
                else
                {
                    //RadGrid6.DataSource = null;
                    //RadGrid6.DataBind();
                }
            }
            else
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    //RadGrid11.DataSource = dt;
                    //RadGrid11.DataBind();
                }
                else
                {
                    //RadGrid11.DataSource = null;
                    //RadGrid11.DataBind();
                }
            }

        }
        private void BindListViewCNCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            Session.Add("CMTCN", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                //RadGridCNCMT.DataSource = dt;
                //RadGridCNCMT.DataBind();
            }
            else
            {
                //RadGridCNCMT.DataSource = null;
                //RadGridCNCMT.DataBind();
            }
        }
        protected void RadCNLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedIndex != -1)
            {
                if (RadSessionTopic.SelectedItem.Text != "Termination")
                {
                    string Account = Session["Account"].ToString();
                    if (Account == "Default")
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["ReviewID"].Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();
                        }
                    }
                    else if (Account == "TalkTalk")
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["ReviewID"].Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();
                        }

                    }
                    else
                    {
                        string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid2.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["ReviewID"].Text;
                            chec = chk.Checked;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            session = item["Session"].Text;
                            topic = item["Topic"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["CN"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewID"] = id;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Session"] = session;
                            dr["Topic"] = topic;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["CN"] = dt;
                            }
                            this.BindListViewCN();

                        }
                        int count = RadGrid2.SelectedItems.Count;
                        //LoadPreviousCommitment(count);

                    }
                }

                else
                {

                    string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid2.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        id = item["ReviewID"].Text;
                        chec = chk.Checked;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        session = item["Session"].Text;
                        topic = item["Topic"].Text;
                        reviewdate = item["ReviewDate"].Text;
                        assignedby = item["AssignedBy"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["CN"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["ReviewID"] = id;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Session"] = session;
                        dr["Topic"] = topic;
                        dr["ReviewDate"] = reviewdate;
                        dr["AssignedBy"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["CN"] = dt;
                        }
                        this.BindListViewCNCMT();
                    }

                }
                ViewState["CN"] = null;
                //Window1.VisibleOnPageLoad = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeModal(); });", true);
            }
            else
            {
                // RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

        protected void RadCNCancel_Click(object sender, EventArgs e)
        {
            string Account = Session["Account"].ToString();
            CheckBox1.Checked = false;
            //if (Account == "Default")
            //{
                RadGridCN.DataSource = null;
                RadGridCN.DataBind();
            //}
            //else if (Account == "TalkTalk")
            //{
            //    RadGrid6.DataSource = null;
            //    RadGrid6.DataBind();
            //}
            //else
            //{
            //    RadGrid11.DataSource = null;
            //    RadGrid11.DataBind();
            //}
        }

        protected void RadPRLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedIndex != -1)
            {

                if (RadSessionTopic.SelectedItem.Text != "Termination")
                {
                    string Account = Session["Account"].ToString();
                    if (Account == "Default")
                    {
                        string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid3.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["ReviewKPIID"].Text;
                            chec = chk.Checked;
                            coachingid = item["ID"].Text;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            name = item["Name"].Text;
                            target = item["Target"].Text;
                            current = item["Current"].Text;
                            previous = item["Previous"].Text;
                            drivername = item["Driver_Name"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["PR"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewKPIID"] = id;
                            dr["ReviewID"] = coachingid;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Name"] = name;
                            dr["Target"] = target;
                            dr["Current"] = current;
                            dr["Previous"] = previous;
                            dr["Driver_Name"] = drivername;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["PR"] = dt;
                            }
                            this.BindListViewPR();
                        }
                    }
                    else if (Account == "TalkTalk")
                    {
                        string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                        bool chec;
                        foreach (GridDataItem item in RadGrid3.SelectedItems)
                        {
                            CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                            id = item["ReviewKPIID"].Text;
                            chec = chk.Checked;
                            coachingid = item["ID"].Text;
                            fullname = item["FullName"].Text;
                            cimnumber = item["CIMNumber"].Text;
                            name = item["Name"].Text;
                            target = item["Target"].Text;
                            current = item["Current"].Text;
                            previous = item["Previous"].Text;
                            drivername = item["Driver_Name"].Text;
                            reviewdate = item["ReviewDate"].Text;
                            assignedby = item["AssignedBy"].Text;

                            DataTable dt = new DataTable();
                            DataRow dr;
                            //assigning ViewState value in Data Table  
                            dt = (DataTable)ViewState["PR"];
                            //Adding value in datatable  
                            dr = dt.NewRow();
                            dr["ReviewKPIID"] = id;
                            dr["ReviewID"] = coachingid;
                            dr["FullName"] = fullname;
                            dr["CIMNumber"] = cimnumber;
                            dr["Name"] = name;
                            dr["Target"] = target;
                            dr["Current"] = current;
                            dr["Previous"] = previous;
                            dr["Driver_Name"] = drivername;
                            dr["ReviewDate"] = reviewdate;
                            dr["AssignedBy"] = assignedby;
                            dt.Rows.Add(dr);

                            if (dt != null)
                            {
                                ViewState["PR"] = dt;
                            }
                            this.BindListViewPR();
                        }
                    }

                }
                else
                {
                    string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid3.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        id = item["ReviewKPIID"].Text;
                        chec = chk.Checked;
                        coachingid = item["ID"].Text;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        name = item["Name"].Text;
                        target = item["Target"].Text;
                        current = item["Current"].Text;
                        previous = item["Previous"].Text;
                        drivername = item["Driver_Name"].Text;
                        reviewdate = item["ReviewDate"].Text;
                        assignedby = item["AssignedBy"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["PR"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["ReviewKPIID"] = id;
                        dr["ReviewID"] = coachingid;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Name"] = name;
                        dr["Target"] = target;
                        dr["Current"] = current;
                        dr["Previous"] = previous;
                        dr["Driver_Name"] = drivername;
                        dr["ReviewDate"] = reviewdate;
                        dr["AssignedBy"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["PR"] = dt;
                        }
                        this.BindListViewPRCMT();
                    }



                }
                ViewState["PR"] = null;
            }
            else
            {
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        protected void RadPRCancel_Click(object sender, EventArgs e)
        {
            string Account = Session["Account"].ToString();
            CheckBox2.Checked = false;
            //if (Account == "Default")
            //{
                RadGridPR.DataSource = null;
                RadGridPR.DataBind();
            //}
            //else if (Account == "TalkTalk")
            //{
            //    RadGrid7.DataSource = null;
            //    RadGrid7.DataBind();
            //}

        }

        private void BindListViewPRCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            Session.Add("CMTPR", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridPRCMT.DataSource = dt;
                RadGridPRCMT.DataBind();
            }
            else
            {
                RadGridPRCMT.DataSource = null;
                RadGridPRCMT.DataBind();
            }


        }

        private void BindListViewPR()
        {
            string Account = Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            //if (Account == "Default")
            //{
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridPR.DataSource = dt;
                    RadGridPR.DataBind();
                }
                else
                {
                    RadGridPR.DataSource = null;
                    RadGridPR.DataBind();
                }
            //}
            //else if (Account == "TalkTalk")
            //{
            //    if (dt.Rows.Count > 0 && dt != null)
            //    {

            //        RadGrid7.DataSource = dt;
            //        RadGrid7.DataBind();
            //    }
            //    else
            //    {
            //        RadGrid7.DataSource = null;
            //        RadGrid7.DataBind();
            //    }
            //}

        }

        public void DisableElements()
        {
            string Account = Session["Account"].ToString();
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                //if (Account == "Default")
                //{
                    //RadGrid1.DataSource = null;
                    //RadGrid1.DataBind();
                    //RadGrid1.Visible = false;
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    //RadDescription.Enabled = false;
                    //RadStrengths.Enabled = false;
                    //RadOpportunities.Enabled = false;
                    //RadCommitment.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    //RadAsyncUpload2.Visible = false;
                    //RadButton1.Visible = false;
             
            }

        }
       
    }

}