﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="AddReviewHR.aspx.cs" Inherits="CoachV2.AddReviewHR" %>

<%@ Register src="UserControl/SidebarDashboardUserControl.ascx" tagname="SidebarUserControl1" tagprefix="uc1" %>
<%@ Register src="UserControl/DashboardMyReviews.ascx" tagname="DashboardMyReviewsUserControl" tagprefix="ucdash5" %>
<%@ Register src="UserControl/DashboardTop.ascx" tagname="DashboardTop" tagprefix="ucdashtop" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderSidebar" runat="server">
    <uc1:SidebarUserControl1 ID="SidebarUserControl" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
   <script type="text/javascript" src="libs/js/JScript1.js"></script>
    <link href="libs/css/gridview.css" rel="stylesheet" type="text/css" />
    <link href="Content/dist/css/StyleSheet1.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">


         var objChkd;

         function HandleOnCheck() {

             var chkLst = document.getElementById('CBList');
             if (objChkd && objChkd.checked)
                 objChkd.checked = false;
             objChkd = event.srcElement;
         }

         var objChkd2;

         function HandleOnCheck2() {

             var chkLst2 = document.getElementById('CheckBoxList1');


             if (objChkd2 && objChkd2.checked)
                 objChkd2.checked = false;

             objChkd2 = event.srcElement;


         }


    </script>
  <telerik:RadScriptBlock ID="Sc" runat="server">
        <script type="text/javascript">
        
            function conditionalPostback(sender, args) {
                if (args.get_eventTarget() == "<%= btn_ExporttoPDF.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= CheckBox1.UniqueID %>") {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget() == "<%= CheckBox2.UniqueID %>") {
                    args.set_enableAjax(false);
                } 
            }

            function BtnDummy(sender, args) {
                var clickButton = document.getElementById("<%= UploadDummy.ClientID %>");
                clickButton.click();
            }
             
        </script>
        <script type="text/javascript">

            function openInc() {
                $('#myInc').modal('show');
            }
            function closeModal() {
                $('#myInc').modal('hide');
            }
//            function openCMT() {
//                $('#myCMTPop').modal('show');
//            }
//            function closeCMT() {
//                $('#myCMTPop').modal('hide');
//            }
//            function openCMTv2() {
//                $('#myCMTPopv2').modal('show');
//            }
//            function closeCMTv2() {
//                $('#myCMTPopv2').modal('hide');
//            }
//            function openCMTv3() {
//                $('#myCMTPopv3').modal('show');
//            }
//            function closeCMTv3() {
//                $('#myCMTPopv3').modal('hide');
//            }
            
        </script> 
    </telerik:RadScriptBlock>
 <div class="menu-content bg-alt">
 <ucdash5:DashboardMyReviewsUserControl  ID="DashboardMyReviewUC1" runat="server" />
    <div class="panel-group" id="accordion">
                    <div class="panel panel-custom" id="CoachingSpecifics" runat="server">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Coaching Specifics <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                    </span>
                                </h4>
                            </div>
                        </a>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body" style="padding-bottom: 5px">
                                <div class="form-group">
                                    <div class="col-sm-4 col-md-2 col-md-push-10">
                                        <div class="col-sm-2">
                                            <asp:HiddenField ID="FakeURLID" runat="server" />
                                            <asp:HiddenField ID="hfGridHtml" runat="server" />
                                            <telerik:RadButton ID="btn_ExporttoPDF" runat="server" Width="45px" Height="45px"
                                                Visible="true" > <%-- OnClick="btn_ExporttoPDF_Click"--%>
                                                <Image ImageUrl="~/Content/images/pdficon.jpg" DisabledImageUrl="~/Content/images/pdficon.jpg" />
                                            </telerik:RadButton>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-md-pull-2">
                                        <label for="Account" class="control-label col-sm-3">
                                            Coaching Ticket</label>
                                        <div class="col-sm-3">
                                            <telerik:RadTextBox ID="RadReviewID" Width="100%" runat="server" class="form-control"
                                                Text="0" ReadOnly="true">
                                            </telerik:RadTextBox>
                                        </div>
                                        <label for="Account" class="control-label col-sm-3">
                                            Coaching Date</label>
                                        <div class="col-sm-3">
                                            <telerik:RadTextBox ID="RadCoachingDate" Width="100%" runat="server" class="form-control"
                                                Text="0" ReadOnly="true">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="panel-body" style="padding-top: 5px">
                                <div class="form-group col-sm-5" id="SelectCoacheeGroup" runat="server">
                                    <form class="form-horizontal">
                                    <label for="name2" class="control-label">
                                        Select Coachee</label>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="email">
                                            Account:</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadAccount" runat="server"  AutoPostBack="true" RenderMode="Lightweight"
                                                DropDownAutoWidth="Disabled" TabIndex="1" Width="100%" EmptyMessage="-Select Account-"
                                             CausesValidation="false"  OnSelectedIndexChanged="RadAccount_SelectedIndexChanged" >
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="AddReview"
                                                ControlToValidate="RadAccount" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="Supervisor">
                                            Supervisor:</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSupervisor" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                DropDownAutoWidth="Disabled" TabIndex="2" Width="100%" EmptyMessage="-Select Supervisor-"
                                               CausesValidation="True" OnSelectedIndexChanged="RadSupervisor_SelectedIndexChanged"> 
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="AddReview"
                                                ControlToValidate="RadSupervisor" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="CoacheeName" class="control-label col-sm-4">
                                            Coachee Name</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadCoacheeName" runat="server" AutoPostBack="true" RenderMode="Lightweight"
                                                DropDownAutoWidth="Disabled" TabIndex="3" Width="100%" EmptyMessage="-Select Coachee-"
                                                CausesValidation="True"  OnSelectedIndexChanged="RadCoacheeName_SelectedIndexChanged" >  
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="AddReview"
                                                ControlToValidate="RadCoacheeName" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                    </form>
                                </div>
                                <div class="form-group col-sm-5" id="SelectCoachingSpecifics" runat="server">
                                    <label for="name2" class="control-label">
                                        Coaching Specifics</label>
                                      <div class="form-group">
                                        <label for="SessionType" class="control-label col-sm-4">
                                            Session Type</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSessionType" runat="server" class="form-control" AutoPostBack="true" CausesValidation="false"
                                                RenderMode="Lightweight" TabIndex="4" Width="100%" EmptyMessage="-Select Session Type-" OnSelectedIndexChanged="RadSessionType_SelectedIndexChanged"  > 
                                            </telerik:RadComboBox>
                                             <asp:RequiredFieldValidator runat="server" ID="RadSessionTypeRequiredFieldValidator13" ValidationGroup="AddReview"
                                                ControlToValidate="RadSessionType" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="Topic" class="control-label col-sm-4">
                                            Topic</label>
                                        <div class="col-sm-8">
                                            <telerik:RadComboBox ID="RadSessionTopic" runat="server" class="form-control" DropDownAutoWidth="Enabled"
                                                TabIndex="5" AutoPostBack="true" Width="100%" EmptyMessage="-Select Session Topic-" OnSelectedIndexChanged="RadSessionTopic_SelectedIndexChanged" >

                                            </telerik:RadComboBox>
                                              <asp:RequiredFieldValidator runat="server" ID="RadSessionTopicRequiredFieldValidator14" ValidationGroup="AddReview"
                                                ControlToValidate="RadSessionTopic" ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field."
                                                CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div> 
                                    <div>
                                        &nbsp;
                                    </div>
       <%--                             <div class="form-group" id="SessionFocusGroup" runat="server">
                                        <label for="Topic" class="control-label col-sm-6">
                                            Session Focus</label>
                                        <div class="col-sm-6">
                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatLayout="Table" RepeatDirection="Vertical"
                                                Onclick="return HandleOnCheck2()" >
                                            </asp:CheckBoxList>
                                            <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList"
                                                ForeColor="Red" Display="Dynamic" ErrorMessage="*This is a Required field." ValidationGroup="AddReview"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>--%>
                                  <div class="form-group" runat="server" id="CheckBoxes">
                                        <div class="col-sm-12">
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="Include Previous Coaching Notes"
                                                AutoPostBack="true"  OnCheckedChanged="CheckBox1_CheckedChanged" CausesValidation="false"  Visible="false" /><br /> 
                                            <asp:CheckBox ID="CheckBox2" runat="server" Text="Include Previous Performance Results"
                                                AutoPostBack="true"  Visible="false" /><br /><%-- OnCheckedChanged="CheckBox2_CheckedChanged"--%>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;
                                    </div>
                                    <div class="form-group" runat="server" id="FollowUp">
                                        <label for="SessionType" class="control-label col-sm-4">
                                            Follow up Date</label>
                                        <div class="col-sm-8">
                                            <telerik:RadDatePicker ID="RadFollowup" runat="server" placeholder="Date" class="RadCalendarPopupShadows"
                                                TabIndex="6" Width="75%" > <%-- OnLoad="RadFollowup_Load" --%>
                                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" EnableWeekends="True"
                                                    FastNavigationNextText="&amp;lt;&amp;lt;">
                                                </Calendar>
                                                <DateInput DisplayDateFormat="yyyy/MM/dd" DateFormat="yyyy/MM/dd" LabelWidth="40%"
                                                    TabIndex="6">
                                                    <EmptyMessageStyle Resize="None"></EmptyMessageStyle>
                                                    <ReadOnlyStyle Resize="None"></ReadOnlyStyle>
                                                    <FocusedStyle Resize="None"></FocusedStyle>
                                                    <DisabledStyle Resize="None"></DisabledStyle>
                                                    <InvalidStyle Resize="None"></InvalidStyle>
                                                    <HoveredStyle Resize="None"></HoveredStyle>
                                                    <EnabledStyle Resize="None"></EnabledStyle>
                                                </DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl="" TabIndex="6"></DatePopupButton>
                                            </telerik:RadDatePicker>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="AddReview"
                                                ControlToValidate="RadFollowup" Display="Dynamic" ErrorMessage="*This is a required field."
                                                ForeColor="Red" CssClass="validator"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div>
                                        &nbsp;</div>
                                </div>
                              <%--  <div class="form-group col-sm-2" id="SelectSearchBlackout" runat="server">
                                    <telerik:RadButton ID="RadSearchBlackout" runat="server" Text="Search Blackout" CssClass="btn btn-info btn-small"
                                        ForeColor="White" Visible="false" OnClick="RadSearchBlackout_Click" Font-Size="Smaller">
                                    </telerik:RadButton>
                                    <asp:HiddenField ID="HiddenSearchBlackout" Value="0" runat="server" />
                                </div>--%>
                            </div>
                        </div>
                    </div>
           <%--         <div id="DefaultForm" runat="server">
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel1" runat="server">
                            <div id="AddReviewDefault" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Description <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadTextBox ID="RadDescription" runat="server" class="form-control" placeholder="Description"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel3" runat="server">
                            <div id="Div1" runat="server">
                                <div class="panel panel-custom" id="PerfResult" runat="server">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Performance Result <span class="glyphicon glyphicon-triangle-bottom trianglebottom">
                                                </span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource"
                                                AllowPaging="true" CssClass="RemoveBorders" OnItemCommand="RadGrid1_ItemCommand"
                                                OnItemCreated="OnItemDataBoundHandler" OnItemDataBound="OnItemDataBound" OnPreRender="RadGrid1_PreRender"
                                                ClientIDMode="AutoID" RenderMode="Lightweight">
                                                <MasterTableView DataKeyNames="UniqueID" CommandItemDisplay="Bottom" CellSpacing="0">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="KPIID" UniqueName="KPIID" HeaderText="KPIID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="KPI">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadKPI" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                    DropDownAutoWidth="Enabled" OnSelectedIndexChanged="RadKPI_SelectedIndexChanged">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadKPIRadDriverRadTargetRequiredFieldValidator3"
                                                                    runat="server" ErrorMessage="*" ControlToValidate="RadKPI" Display="Dynamic"
                                                                    ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Target">
                                                            <ItemTemplate>
                                                                <telerik:RadTextBox ID="RadTarget" runat="server" Text='<%# Eval("Target") %>' ReadOnly="true">
                                                                </telerik:RadTextBox>
                                                                <asp:RequiredFieldValidator ID="RadTargetRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadTarget" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Current">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadCurrent" runat="server" Text='<%# Eval("Current") %>'>
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadCurrent" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Previous">
                                                            <ItemTemplate>
                                                                <telerik:RadNumericTextBox ID="RadPrevious" runat="server" Text='<%# Eval("Previous") %>'>
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                    ControlToValidate="RadPrevious" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridBoundColumn DataField="Driver" UniqueName="DriverID" HeaderText="DriverID"
                                                            Visible="false">
                                                        </telerik:GridBoundColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Driver">
                                                            <ItemTemplate>
                                                                <telerik:RadComboBox ID="RadDriver" runat="server" DropDownAutoWidth="Enabled">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ID="RadDriverRequiredFieldValidator3" runat="server"
                                                                    ErrorMessage="*" ControlToValidate="RadDriver" Display="Dynamic" ValidationGroup="Perf"></asp:RequiredFieldValidator>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <telerik:RadGrid ID="RadGridPRR" runat="server" Visible="true" RenderMode="LightWeight">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <div>
                                                &nbsp;</div>
                                            <div class="col-md-10">
                                            </div>
                                            <div class="col-md-2">
                                                <telerik:RadButton ID="RadButton1" runat="server" Text="Add More" CssClass="btn btn-info btn-small"
                                                    ForeColor="White" ValidationGroup="Perf" OnClick="Button1_Click">
                                                </telerik:RadButton>
                                            </div>
                                            <asp:Panel ID="CNPRPanel" runat="server">
                                                <div id="CNPR" runat="server">
                                                    <div id="collapseCNPR" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>
                        <asp:Panel ID="Panel2" runat="server">
                            <div id="AddReviewDefault2" runat="server">
                                <asp:Panel ID="DefaultPane" runat="server">
                                    <asp:Panel ID="DefaultPane1" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Strengths <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseFour" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadStrengths" runat="server" class="form-control" placeholder="Strengths"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="9">
                                                    </telerik:RadTextBox> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Opportunities <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseFive" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadOpportunities" runat="server" class="form-control" placeholder="Opportunities"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="10">
                                                    </telerik:RadTextBox>
 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Commitment <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseSix" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <telerik:RadTextBox ID="RadCommitment" runat="server" class="form-control" placeholder="Commitment"
                                                        TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="11">
                                                    </telerik:RadTextBox> 
                                                </div>
                                            </div>
                                        </div>
                        <%--            </asp:Panel>
                                </asp:Panel> 
                                <asp:Panel ID="DefaultPane2" runat="server">
                                    <asp:Panel ID="DefaultPane3" runat="server">
                                        <div class="panel panel-custom">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentationCMT">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                                    </h4>
                                                </div>
                                            </a>
                                            <div id="collapseDocumentationCMT" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:Panel ID="DefaultPane4" runat="server">
                                                        <asp:Panel ID="DefaultPane5" runat="server">
                                                            <telerik:RadGrid ID="RadDocumentationReview" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                                CssClass="RemoveBorders" 
                                                                BorderStyle="None" GridLines="None">  
                                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                                        </telerik:GridHyperLinkColumn>
                                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                                        </telerik:GridHyperLinkColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings>
                                                                    <Scrolling AllowScroll="True" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                    <div>
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <telerik:RadAsyncUpload ID="RadAsyncUpload2" runat="server" MultipleFileSelection="Automatic"
                                                            OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf" PostbackTriggers="UploadDummy"
                                                            MaxFileSize="1000000" Width="40%" >
                                                            <Localization Select="                             " />
                                                        </telerik:RadAsyncUpload>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                         </div>
                        </asp:Panel>
                        <div style="height: 5px">
                        </div>--%>
                        <asp:Panel ID="Panel4" runat="server">
                            <asp:Panel ID="CMTView" runat="server">
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#HRComments">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                HR Comments <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="HRComments" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <label for="Previous Perfomance Results">
                                                Comments Section</label><br />
                                            <div style="height: 5px">
                                            </div>
                                            <telerik:RadTextBox ID="RadHRComments" runat="server" class="form-control" placeholder="HR Comments"
                                                TextMode="MultiLine" Width="100%" RenderMode="Lightweight" Rows="5" TabIndex="7">
                                            </telerik:RadTextBox>
                                   <%--         <asp:Panel ID="Panel5" runat="server">
                                                <div id="Div2" runat="server">
                                                    <div id="Div3" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCNCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPRCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>--%>
                                             <asp:Panel ID="CNPRPanel" runat="server">
                                                <div id="CNPR" runat="server">
                                                    <div id="collapseCNPR" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCN" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 5px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPR" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
    <div id="myInc" class="modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="ModalHeader" class="manager">
                    </h4>
                </div>
                <div class="modal-body">
                    <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                        Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic" RenderMode="Lightweight"
                        CssClass="RemoveBorders">
                        <MasterTableView DataKeyNames="ReviewID">
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                </telerik:GridClientSelectColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="true">
                            <Scrolling AllowScroll="True" />
                            <Selecting AllowRowSelect="True"></Selecting>
                            <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="RadGrid3" runat="server" AutoGenerateColumns="true" AllowMultiRowSelection="True"
                        Font-Size="Smaller" GroupPanelPosition="Top" ResolvedRenderMode="Classic" RenderMode="Lightweight"
                        CssClass="RemoveBorders">
                        <MasterTableView DataKeyNames="ReviewKPIID">
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="Select All">
                                </telerik:GridClientSelectColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="true">
                            <Scrolling AllowScroll="True" />
                            <Selecting AllowRowSelect="True"></Selecting>
                            <ClientEvents OnRowMouseOver="demo.RowMouseOver" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <div class="modal-footer">
                    <span class="addreviewbuttonholder">
                        <telerik:RadButton ID="RadCNLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadCNLink_Click">
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton ID="RadCNCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadCNCancel_Click">
                        </telerik:RadButton>
                        <telerik:RadButton ID="RadPRLink" runat="server" Text="Link" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadPRLink_Click">
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton ID="RadPRCancel" runat="server" Text="Cancel" class="btn btn-info btn-larger"
                            Height="30px" OnClick="RadPRCancel_Click">
                        </telerik:RadButton>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="Panel5" runat="server">
                                                <div id="Div2" runat="server">
                                                    <div id="Div3" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Coaching Notes</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridCNCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("Session") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Topic">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="AssignedBy">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <label for="Previous Perfomance Results">
                                                                Previous Perfomance Results</label><br />
                                                            <div style="height: 10px">
                                                            </div>
                                                            <div>
                                                                <div class="panel-body">
                                                                    <telerik:RadGrid ID="RadGridPRCMT" RenderMode="Lightweight" runat="server" AutoGenerateColumns="false">
                                                                        <MasterTableView>
                                                                            <Columns>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching KPI ID">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCT" Text='<%# Eval("ReviewKPIID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Ticket">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCN" Text='<%# Eval("ReviewID") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Employee Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCC" Text='<%# Eval("FullName") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="CIM Number">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelSS" Text='<%# Eval("CIMNumber") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Session">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelTC" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCD" Text='<%# Eval("Target") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Current">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelAD" Text='<%# Eval("Current") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Previous">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelPR" Text='<%# Eval("Previous") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Driver">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelDN" Text='<%# Eval("Driver_Name") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Coaching Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelCDate" Text='<%# Eval("ReviewDate") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                                <telerik:GridTemplateColumn HeaderText="Assigned By">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LabelABy" Text='<%# Eval("AssignedBy") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </telerik:GridTemplateColumn>
                                                                            </Columns>
                                                                        </MasterTableView>
                                                                        <ClientSettings>
                                                                            <Scrolling AllowScroll="True" />
                                                                        </ClientSettings>
                                                                    </telerik:RadGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentation">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                Documentation <span class="glyphicon glyphicon-triangle-bottom trianglebottom"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapseDocumentation" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <telerik:RadGrid ID="RadGridDocumentation" runat="server" AllowPaging="True" AllowFilteringByColumn="false"
                                                AllowSorting="true" GroupPanelPosition="Top" ResolvedRenderMode="Classic" AutoGenerateColumns="false"
                                                CssClass="RemoveBorders" 
                                                BorderStyle="None" GridLines="None"> <%--OnItemDataBound="RadGridDocumentation_onItemDatabound"--%>
                                                <MasterTableView DataKeyNames="Id" NoMasterRecordsText="">
                                                    <Columns>
                                                        <telerik:GridTemplateColumn HeaderText="Item Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCT" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="FilePath" HeaderText="File Path" UniqueName="FilePath"
                                                            FilterControlToolTip="FilePath" DataNavigateUrlFields="FilePath" Display="false">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridHyperLinkColumn DataTextField="DocumentName" HeaderText="DocumentName"
                                                            UniqueName="DocumentName" FilterControlToolTip="DocumentName" DataNavigateUrlFields="DocumentName"
                                                            AllowSorting="true" Target="_blank" ShowSortIcon="true">
                                                        </telerik:GridHyperLinkColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Uploaded By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelCC" runat="server" Text='<%# Eval("UploadedBy") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridTemplateColumn HeaderText="Date Uploaded">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LabelSS" runat="server" Text='<%# Eval("DateUploaded") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <div>
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-12">
                                                <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" MultipleFileSelection="Automatic"
                                                    OnClientValidationFailed="OnClientValidationFailed" AllowedFileExtensions=".pdf"
                                                    MaxFileSize="1000000" Width="40%" PostbackTriggers="RadCMTPreview,CyborgCMT">
                                                    <Localization Select="                             " />
                                                </telerik:RadAsyncUpload>
                                            </div>
                                        </div>
                                    </div>
                                </div>
         
                            </asp:Panel>
                        </asp:Panel>
                        </div>
             
                    <div class="panel-body">
                        <div id="SignOut" class="container-fluid" runat="server">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    
                                    <telerik:RadButton ID="btn_SendtoTl" runat="server" Text="Send to Supervisor"
                                        CssClass="btn btn-info btn-small" ValidationGroup="AddReview"
                                        ForeColor="White"  OnClick="btn_SendtoTl_OnClick" > 
                                    </telerik:RadButton> 
                                    <telerik:RadButton ID="UploadDummy" runat="server" Text="UploadDummy" 
                                    OnClick="UploadDummy_Click"  Style="display: none;">   
                                    </telerik:RadButton>
                                </div>
                            </div>
                        </div>
                    </div> 
 </div>
</asp:Content>
