﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;
namespace CoachV2
{
    public partial class TestReportExtraction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void topdf_onclick(object sender, EventArgs e)
        {

            DataSet ds_get_userstable_PDF = DataHelper.getuserstable_pdf();

            Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            pdfDoc.NewPage();
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());

            //iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
            //gif.ScaleToFit(10f, 10f);

            Paragraph p_usertable = new Paragraph();
            string line_usertable = "User Table " + Environment.NewLine;
            p_usertable.Alignment = Element.ALIGN_LEFT;
            p_usertable.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
            p_usertable.Add(line_usertable);            
            p_usertable.SpacingBefore = 10;
            p_usertable.SpacingAfter = 10;

            if (ds_get_userstable_PDF.Tables[0].Rows.Count > 0)
            {
                int col = ds_get_userstable_PDF.Tables[0].Columns.Count;

                PdfPTable usertable = new PdfPTable(col);
                string perfheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell usercell = new PdfPCell(new Phrase(perfheader));
                usercell.Colspan = col;
                usercell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                usertable.AddCell(usercell);
                usertable.WidthPercentage = 100;
                usertable.SpacingBefore = 30;
                usertable.SpacingAfter = 30;
                int ctrc=0;

                string val = "";
                for (int ctr = 0; ctr < ds_get_userstable_PDF.Tables[0].Rows.Count; ctr++ )
                {
                 if (ctr == 0)
                {
                    for ( ctrc = 0; ctrc < ds_get_userstable_PDF.Tables[0].Columns.Count; ctrc++)
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase(Convert.ToString(ds_get_userstable_PDF.Tables[0].Columns[ctrc])));
                        perfcellh1.Border = 0;
                        perfcellh1.NoWrap = false;
                        usertable.AddCell(perfcellh1);
                    }
                }
                 else if (ctr != 0)
                 {
                     for (ctrc = 0; ctrc < ds_get_userstable_PDF.Tables[0].Columns.Count; ctrc++)
                     {
                         if (ds_get_userstable_PDF.Tables[0].Rows[ctr][ctrc] is DBNull)
                         {
                             val = "";

                         }
                         else { val = ds_get_userstable_PDF.Tables[0].Rows[ctr][ctrc].ToString(); }
                         PdfPCell perfcellh1 = new PdfPCell(new Phrase(val));
                         perfcellh1.Border = 0;
                         perfcellh1.NoWrap = false;
                         usertable.AddCell(perfcellh1);
                     }
                 }
                }

               pdfDoc.Add(new Paragraph(p_usertable));
               pdfDoc.Add(usertable);//usettableitems



                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "usertable_" + DateTime.Today; 
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();

            } 
            

        }
    }
}