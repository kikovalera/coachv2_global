﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using DotNetOpenAuth.AspNet;
using System.Web.Security;
//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Auth.OAuth2.Flows;
using Newtonsoft.Json;

using System.Data;
using System.Globalization;
using System.Net;
using System.IO;

namespace CoachV2
{
    public partial class ExternalLandingPage : System.Web.UI.Page
    {
        public string ReturnUrl { get; set; }

        string ProviderName
        {
            get { return (string)ViewState["ProviderName"] ?? String.Empty; }
            set { ViewState["ProviderName"] = value; }
        }

        string ProviderDisplayName
        {
            get { return (string)ViewState["ProviderDisplayName"] ?? String.Empty; }
            set { ViewState["ProviderDisplayName"] = value; }
        }

        string ProviderUserId
        {
            get { return (string)ViewState["ProviderUserId"] ?? String.Empty; }
            set { ViewState["ProviderUserId"] = value; }
        }

        string ProviderUserName
        {
            get { return (string)ViewState["ProviderUserName"] ?? String.Empty; }
            set { ViewState["ProviderUserName"] = value; }
        }

        protected void Page_Load()
        {
            if (!IsPostBack)
            {
                ProcessProviderResult();
            }
            else {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            /*if (!IsPostBack)
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetNoStore();
            }*/
        }


        protected void logIn_Click(object sender, EventArgs e)
        {
            CreateAndLoginUser();
        }

        protected void cancel_Click(object sender, EventArgs e)
        {
            RedirectToReturnUrl();
        }

        private void ProcessProviderResult()
        {

           
            DotNetOpenAuth.GoogleOAuth2.GoogleOAuth2Client.RewriteRequest();
            // Process the result from an auth provider in the request
            ProviderName = OpenAuth.GetProviderNameFromCurrentRequest();

            if (String.IsNullOrEmpty(ProviderName))
            {
                //Response.Redirect(FormsAuthentication.LoginUrl);
                Response.Redirect("Default.aspx");
            }

            // Build the redirect url for OpenAuth verification
            var redirectUrl = "~/ExternalLandingPage.aspx";
            var returnUrl = Request.QueryString["ReturnUrl"];
            if (!String.IsNullOrEmpty(returnUrl))
            {
                redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl);
            }

            // Verify the OpenAuth payload
            AuthenticationResult authResult = OpenAuth.VerifyAuthentication(redirectUrl);
        
            
            ProviderDisplayName = OpenAuth.GetProviderDisplayName(ProviderName);
            unauthenticated_msg.Visible = false;

            if (!authResult.IsSuccessful)
            {
                Title = "External login failed";
                //ModelState.AddModelError("Provider", String.Format("Exteranal login {0} failed.", ProviderDisplayName));

                // To view this error, enable page tracing in web.config (<system.web><trace enabled="true"/></system.web>) and visit ~/Trace.axd
                Trace.Warn("OpenAuth", String.Format("There was an error verifying authentication with {0})", ProviderDisplayName), authResult.Error);
                original_message.Visible = false;

                unauthenticated_msg.Visible = true;
                //unauthenticated_msg.InnerText = "You haven't signed in yet" + String.Format("There was an error verifying authentication with {0})", ProviderDisplayName, authResult.Error);
                
                return;
            }

            // Strip the query string from action
            Form.Action = ResolveUrl(redirectUrl);
            var extraData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(authResult.ExtraData));
            
            // Store the provider details in ViewState
           
            //replace authResult.Provider and extraData.email  with email of user to impersonate account
            //latest
            try
            {
                string bot = Session["bot"].ToString();
                ProviderUserName=ProviderName = bot;
            }
            catch (NullReferenceException)
            {

                ProviderName = authResult.Provider;
                ProviderUserName = extraData.email;
            }
            
           

            //ProviderName = "maria.castaneda@TRANSCOM.COM";
            //ProviderUserName = "maria.castaneda@TRANSCOM.COM";
            //Session["GoogleName"] = authResult.UserName;
            lblUsername.Text = ProviderUserName;
            lblEmail.Text = ProviderUserName;
            ProviderUserId = authResult.ProviderUserId;
            lblGoogleName.Text ="Register with your Google account " + authResult.UserName;
            googleaccountHiddenField.Value = authResult.UserName;
            lblProfileImage.Text = extraData.picture;

            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(lblEmail.Text);
            if (dsSAPInfo.Tables[0].Rows.Count == 1)
            {
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                Session["CIMKOPO"] = cim_num;
                Session["Role"] = dsSAPInfo.Tables[0].Rows[0]["Role"].ToString();
            }
            if (dsSAPInfo.Tables[0].Rows.Count <= 0) {
                original_message.Visible = false;
                lblEmail.Text = "Your Transcom Google ID or Email is not configured in SAP. Please contact HR. Click <a href='Default.aspx'>here</a> to return home.";
                Session.Abandon();
                Session.RemoveAll();
                Session.Clear();
                return;
            }

            if (dsSAPInfo.Tables[0].Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                    "alert('Your Transcom Google ID or Email is not configured in SAP. Please contact HR. );window.location='Default.aspx';", true);
                return;
            }

            if (extraData.hd != null && extraData.hd.ToString() == "transcom.com")
            {
                original_message.Visible = true;

                if (OpenAuth.Login(authResult.Provider, authResult.ProviderUserId, createPersistentCookie: true))
                {
                    RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text + "|" + Session["CIMKOPO"].ToString());
                }

                // Check if user is already registered locally
                if (User.Identity.IsAuthenticated)
                {
                    // User is already authenticated, add the external login and redirect to return url
                    OpenAuth.AddAccountToExistingUser(ProviderName, ProviderUserId, ProviderUserName, User.Identity.Name);
                    RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text + "|" + Session["CIMKOPO"].ToString());
                }
            }
            else
            {
                //OpenAuth.DeleteAccount(ProviderUserName, ProviderName, ProviderUserId);
                //https://mail.google.com/mail/u/0/?logout&hl=en
                //string strRetMsg ="<script>window.location.href = 'https://www.google.com/accounts/Logout?continue='" + Request.Url.Host + ";</script>";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                //"window.location='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                //HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath ) + "/Default.aspx';", true);

                

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                "alert('Your email is not authorized to use this application. Redirecting you back to homepage.');window.location='Default.aspx';", true);

                FormsAuthentication.SignOut();
                Session.Abandon();

                //headingx.Visible = false;



                AdditionalCaveat.Visible = false;
                Button1.Visible = false;
                Button2.Visible = false;
                //ModelErrorMessage1.Text = "Your domain is not allowed";

                //Response.Redirect("Default.aspx");
                
            }
        }

        private void CreateAndLoginUser()
        {
            if (!IsValid)
            {
                return;
            }

            var createResult = OpenAuth.CreateUser(ProviderName, ProviderUserId, ProviderUserName, lblUsername.Text);
            if (!createResult.IsSuccessful)
            {
                //ModelState.AddModelError("UserName", createResult.ErrorMessage);
            }
            else
            {
               /* HttpCookie ck = new HttpCookie("UserInfo");
                //Session["MyUserID"] = null;

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(ProviderUserName);
                DataSet dsUserInfo = DataHelper.GetUserInfo(ProviderUserName);

                if (dsUserInfo.Tables[0].Rows.Count > 0)
                {
                    //Session["MyUserID"] = dsUserInfo.Tables[0].Rows[0]["UserInfoId"].ToString();
                    ck.Values["MyUserID"] = dsUserInfo.Tables[0].Rows[0]["UserInfoId"].ToString();
                }
                else
                {
                    string CIMNo = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    string FirstName = dsSAPInfo.Tables[0].Rows[0]["First_Name"].ToString();
                    string LastName = dsSAPInfo.Tables[0].Rows[0]["Last_Name"].ToString();
                    string ReportingTo = dsSAPInfo.Tables[0].Rows[0]["ReportsTo"].ToString();
                    int Gender = Convert.ToInt32(dsSAPInfo.Tables[0].Rows[0]["Gender"]);

                    string MobileNo = dsSAPInfo.Tables[0].Rows[0]["Mobile_Number"].ToString();
                    string LandLineNo = dsSAPInfo.Tables[0].Rows[0]["Phone_Number"].ToString();
                    string TranscomEmail = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                    string Role = dsSAPInfo.Tables[0].Rows[0]["Role"].ToString();
                    string Site2 = dsSAPInfo.Tables[0].Rows[0]["Site"].ToString();
                    string Region = "";
                    string Country = dsSAPInfo.Tables[0].Rows[0]["Country"].ToString();
                    string Department = dsSAPInfo.Tables[0].Rows[0]["Department"].ToString();

                    using (WebClient wc = new WebClient())
                    {
                        string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + CIMNo));

                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                            wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + CIMNo + ".jpg");
                        }
                        else
                        {
                            wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2], directoryPath + "\\" + CIMNo + ".jpg");
                        }

                        // wc.DownloadFile(HttpContext.Current.User.Identity.Name.Split('|')[2],  (!System.IO.Directory.Exists(@"Content\\uploads\\"+CIMNo)) ? System.IO.Directory.CreateDirectory(@"Content\\uploads\\"+CIMNo : "");


                    }





                    DateTime DateOfBirth = DateTime.ParseExact(dsSAPInfo.Tables[0].Rows[0]["Birthday"].ToString(),
                                        "yyyyMMdd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);

                    DateTime DateStartTranscom = DateTime.ParseExact(dsSAPInfo.Tables[0].Rows[0]["Start_Date"].ToString(),
                                        "yyyyMMdd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);

                    //string UserId = System.Web.HttpContext.Current.User.Identity.;

                    MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string userId = mu.ProviderUserKey.ToString();

                    DataHelper.InsertUserInfo(userId,
                        CIMNo,
                        FirstName,
                        LastName,
                        "",
                        ReportingTo,
                        Gender,
                        DateOfBirth,
                        DateStartTranscom,
                        "",
                        MobileNo,
                        LandLineNo,
                        TranscomEmail,
                        "",
                        Role,
                        Site2,
                        Region,
                        Country,
                        Department,
                        "",
                        "",
                        (CIMNo + ".jpg"),
                        DateTime.Now,
                        DateTime.Now, false);

                    DataSet dsUserInfo2 = DataHelper.GetUserInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    ck.Values["MyUserID"] = dsUserInfo2.Tables[0].Rows[0]["UserInfoId"].ToString();

                    Request.Cookies.Add(ck);

                }*/

                // User created & associated OK

                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(lblEmail.Text);
                if (dsSAPInfo.Tables[0].Rows.Count == 1)
                {
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    Session["CIMKOPO"] = cim_num;
                    Session["Role"] = dsSAPInfo.Tables[0].Rows[0]["Role"].ToString();
                    if (OpenAuth.Login(ProviderName, ProviderUserId, createPersistentCookie: false))
                    {
                        RedirectToReturnUrl(lblUsername.Text + "|" + googleaccountHiddenField.Value + "|" + lblProfileImage.Text + "|" + Session["CIMKOPO"]);
                    }
                }

            }
            

        }

        public IEnumerable<ProviderDetails> GetProviderNames()
        {
            return OpenAuth.AuthenticationClients.GetAll();
        }


        protected void BtnLoginGoogle_Click(object sender, EventArgs e)
        {
            var redirectUrl = "~/ExternalLandingPage.aspx";

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                var resolvedReturnUrl = ResolveUrl(ReturnUrl);
                redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
            }

            OpenAuth.RequestAuthentication("google", redirectUrl);
        }
        

        private void RedirectToReturnUrl()
        {
            var returnUrl = Request.QueryString["ReturnUrl"];

            if (!String.IsNullOrEmpty(returnUrl) && OpenAuth.IsLocalUrl(returnUrl))
            {
                Response.Redirect(returnUrl);
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        private void RedirectToReturnUrl(string userID)
        {
            var returnUrl = Request.QueryString["ReturnUrl"];
            FormsAuthentication.SetAuthCookie(userID, true);

            if (!String.IsNullOrEmpty(returnUrl))
            {
                Response.Redirect(returnUrl);
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}