﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using System.IO;
using CoachV2.UserControl;

namespace CoachV2
{
    public partial class UpdateQAReview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //int CIMNumber =10107032;

                if (Page.Request.QueryString["Coachingticket"] == null)
                {

                    Response.Redirect("~/Default.aspx");
                 }
                else
                {
                    pn_coacheedetails.Visible = true;
                    //decryption
                    string decrypt = DataHelper.Decrypt(Convert.ToString(Page.Request.QueryString["Coachingticket"]));
                    int CoachingTicket = Convert.ToInt32(decrypt);

                    //int CoachingTicket = Convert.ToInt32(Page.Request.QueryString["Coachingticket"]);
                    //int CoachingTicket = 2037;
                    //loadcoacheedetails(Convert.ToInt32(Page.Request.QueryString["Coachingticket"]));//(CoachingTicket);
                    loadcoacheedetails(CoachingTicket);
                    //if ((Page.Request.QueryString["Account"] == "TalkTalk") || (Page.Request.QueryString["Account"] == "Talk - Talk"))
                    //{
                    //    RevNex1.Visible = true;
                    //    RevOD2.Visible = false;
                    //}
                    //else
                    //{
                    //    RevOD2.Visible = true;
                    //    RevNex1.Visible = false;
                    //}
                    //Response.Redirect("~/Default.aspx");
                }            
        }
        protected void loadcoacheedetails(int reviewid)
        {
            try
            {
                DataSet ds3 = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string emp_email = ds3.Tables[0].Rows[0]["Email"].ToString();
                string cim_num = ds3.Tables[0].Rows[0][0].ToString();
                int intcim = Convert.ToInt32(cim_num);

                DataSet ds_get_review = DataHelper.Get_ReviewID(reviewid);

                int coacheeid = Convert.ToInt32(ds_get_review.Tables[0].Rows[0]["CoacheeID"]);
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(coacheeid));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
                //DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(coacheeid));
                DataSet ds = DataHelper.GetUserDetails(Convert.ToInt32(coacheeid));

                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;


                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = host + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = host + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                ProfImage.ImageUrl = ImgDefault;
                LblFullName.Text = ds.Tables[0].Rows[0]["E First Name"].ToString() + " " + ds.Tables[0].Rows[0]["E Last Name"].ToString();
                LblRole.Text = ds.Tables[0].Rows[0]["Role"].ToString();
                LblSite.Text = ds.Tables[0].Rows[0]["Site"].ToString();
                LblDept.Text = ds.Tables[0].Rows[0]["Department"].ToString();
                LblCountry.Text = ds.Tables[0].Rows[0]["Country"].ToString();
                LblMySupervisor.Text = ds.Tables[0].Rows[0]["SupFirst Name"].ToString() + " " + ds.Tables[0].Rows[0]["SupLast Name"].ToString();
                LblRegion.Text = "English Speaking Region";
                LblClient.Text = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0]["Client"].ToString() : "";
                LblCampaign.Text = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0]["Account"].ToString() : "";


                if (ds_get_review.Tables[0].Rows[0]["ReviewTypeID"].ToString() == "2")
                {
                    string Acct = ds_get_review.Tables[0].Rows[0]["FormType"].ToString();
                    if (Acct == "1")
                    {
                        PanelQARev.Controls.Clear();
                        RevDefault RevDefault = (RevDefault)LoadControl("UserControl/RevDefault.ascx");
                        PanelQARev.Controls.Add(RevDefault);

                    }
                    else if (Acct == "2")
                    {
                        PanelQARev.Controls.Clear();
                        RevNexidiaQA RevNexidiaQA = (RevNexidiaQA)LoadControl("UserControl/RevNexidiaQA.ascx");
                        PanelQARev.Controls.Add(RevNexidiaQA);
                    }
                    else
                    {
                        PanelQARev.Controls.Clear();
                        RevODQA RevODQA = (RevODQA)LoadControl("UserControl/RevODQA.ascx");
                        PanelQARev.Controls.Add(RevODQA);
                    }
                }
            }
            catch (Exception ex)
            {
                string ModalLabel = "loadcoacheedetails " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }


        }

        protected void RadSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);


                DataAccess ws = new DataAccess();
                ws.UpdateTLHRCommitment(Convert.ToInt32(RadReviewID.Text), RadCommitment.Text, CIMNumber);
                string ModalLabel = "Commitment was successfully saved.";
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                RadSave.Visible = false;
                RadCommitment.Enabled = false;


                string stats = "*Supervisor Sign Off: " + HttpContext.Current.User.Identity.Name.Split('|')[1].ToString() + " " + DateTime.Now.ToString("M/d/yyyy");
                lblstatus.Text = stats;

            }
            catch (Exception ex)
            {
                string ModalLabel = "RadSave_Click " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }

    }
}