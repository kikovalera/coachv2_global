﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.Drawing;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.IO;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace CoachV2
{
    public partial class AddReviewHR : System.Web.UI.Page
    {

        string strFilename;
        protected string CIM;
        protected int requestID;
        protected string ccmCIM;
        protected string reviewID;
        protected string reportsTo;
        protected string userIsAgent;

        string toName = "";
        string target = "";
        string subject = "";
        string toEmail = "";
        string htmlBody = "";
        string fromName = "";
        string fromEmail = "";

        public List<Employee> lstEmployee
        {
            get
            {
                if (Session["lstEmployee"] != null)
                {
                    return (List<Employee>)Session["lstEmployee"];
                }
                else
                {
                    return new List<Employee>();
                }
            }
            set
            {
                Session["lstEmployee"] = value;
            }
        }
        public List<Nexidia> lstNexidia
        {
            get
            {
                if (Session["lstNexidia"] != null)
                {
                    return (List<Nexidia>)Session["lstNexidia"];
                }
                else
                {
                    return new List<Nexidia>();
                }
            }
            set
            {
                Session["lstNexidia"] = value;
            }
        }
        public DataTable SearchBlackout
        {
            get
            {
                return ViewState["SearchBlackout"] as DataTable;
            }
            set
            {
                ViewState["SearchBlackout"] = value;
            }
        }
        public DataTable CN
        {
            get
            {
                return ViewState["CN"] as DataTable;
            }
            set
            {
                ViewState["CN"] = value;
            }
        }
        public DataTable PR
        {
            get
            {
                return ViewState["PR"] as DataTable;
            }
            set
            {
                ViewState["PR"] = value;
            }
        }
        public DataTable DataEntry
        {
            get
            {
                return ViewState["DataEntry"] as DataTable;
            }
            set
            {
                ViewState["DataEntry"] = value;
            }
        }
        protected void PerfResults()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns["ID"].AutoIncrement = true;
            dt.Columns["ID"].AutoIncrementSeed = 1;
            dt.Columns["ID"].AutoIncrementStep = 1;
            dt.Columns.Add("KPIID", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("DriverName", typeof(string));
            dt.Columns.Add("Target", typeof(string));
            dt.Columns.Add("Current", typeof(string));
            dt.Columns.Add("Previous", typeof(string));
            dt.Columns.Add("Change", typeof(string));
            dt.Columns.Add("Behaviour", typeof(string));
            dt.Columns.Add("RootCause", typeof(string));
            ViewState["DataEntry"] = dt;
            BindListView();
        }
        private void BindListView()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["DataEntry"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGrid9.DataSource = dt;
                RadGrid9.DataBind();
            }
            else
            {
                RadGrid9.DataSource = null;
                RadGrid9.DataBind();
            }
        }
        protected void RadGrid9_OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if ((e.Item is GridEditFormInsertItem) && (e.Item.OwnerTableView.IsItemInserted))
                {
                    GridEditFormInsertItem insertitem = (GridEditFormInsertItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI9");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    //ds = ws.GetKPIList();
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsTarget = null;
                            DataAccess wsTarget = new DataAccess();
                            dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                            RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }
                    }


                }

                if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;

                    Label radID = (Label)edititem.FindControl("RadID");
                    radID.Text = ((DataRowView)e.Item.DataItem)["ID"].ToString();

                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI9");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPIID"].ToString();
                    selectedItem.Value = ((DataRowView)e.Item.DataItem)["Name"].ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI"] = selectedItem.Value;
                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI9");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    //ds = ws.GetKPIList();
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    comboBrand.SelectedValue = selectedItem.Value;

                    RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                    target.Text = ((DataRowView)e.Item.DataItem)["Target"].ToString();

                    RadTextBox behaviour = (RadTextBox)edititem.FindControl("RadBehaviour");
                    behaviour.Text = ((DataRowView)e.Item.DataItem)["Behaviour"].ToString();

                    RadTextBox rootcause = (RadTextBox)edititem.FindControl("RadRootCause");
                    rootcause.Text = ((DataRowView)e.Item.DataItem)["RootCause"].ToString();

                    RadNumericTextBox current = (RadNumericTextBox)edititem.FindControl("RadCurrent");
                    current.Text = ((DataRowView)e.Item.DataItem)["Current"].ToString();

                    RadNumericTextBox previous = (RadNumericTextBox)edititem.FindControl("RadPrevious");
                    previous.Text = ((DataRowView)e.Item.DataItem)["previous"].ToString();

                    RadNumericTextBox change = (RadNumericTextBox)edititem.FindControl("RadChange");
                    change.Text = ((DataRowView)e.Item.DataItem)["Change"].ToString();

                    RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                    RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                    selectedItemdriver.Text = ((DataRowView)e.Item.DataItem)["Driver"].ToString();
                    selectedItemdriver.Value = ((DataRowView)e.Item.DataItem)["DriverName"].ToString();
                    combodriver.Items.Add(selectedItemdriver);
                    selectedItemdriver.DataBind();
                    Session["Driver"] = selectedItemdriver.Value;
                    RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                    DataSet dsdriver = new DataSet();
                    DataAccess wsdriver = new DataAccess();
                    dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(comboBrand.SelectedValue));
                    comboBranddriver.DataSource = dsdriver;
                    comboBranddriver.DataTextField = "Description";
                    comboBranddriver.DataValueField = "Driver_ID";
                    comboBranddriver.DataBind();
                    comboBranddriver.SelectedValue = selectedItemdriver.Value;

                }
            }
            else
            {
                if ((e.Item is GridEditFormInsertItem) && (e.Item.OwnerTableView.IsItemInserted))
                {

                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridEditFormInsertItem insertitem = (GridEditFormInsertItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI9");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        //ds = ws.GetKPIList();
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsTarget = null;
                                DataAccess wsTarget = new DataAccess();
                                dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                                RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }
                        }
                    }


                }

                if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;

                    Label radID = (Label)edititem.FindControl("RadID");
                    radID.Text = ((DataRowView)e.Item.DataItem)["ID"].ToString();

                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI9");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPIID"].ToString();
                    selectedItem.Value = ((DataRowView)e.Item.DataItem)["Name"].ToString();
                    combo.Items.Add(selectedItem);
                    selectedItem.DataBind();
                    Session["KPI"] = selectedItem.Value;
                    RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI9");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    ds = ws.GetKPIList();
                    comboBrand.DataSource = ds;
                    comboBrand.DataBind();
                    comboBrand.SelectedValue = selectedItem.Value;

                    RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                    target.Text = ((DataRowView)e.Item.DataItem)["Target"].ToString();

                    RadTextBox behaviour = (RadTextBox)edititem.FindControl("RadBehaviour");
                    behaviour.Text = ((DataRowView)e.Item.DataItem)["Behaviour"].ToString();

                    RadTextBox rootcause = (RadTextBox)edititem.FindControl("RadRootCause");
                    rootcause.Text = ((DataRowView)e.Item.DataItem)["RootCause"].ToString();

                    RadNumericTextBox current = (RadNumericTextBox)edititem.FindControl("RadCurrent");
                    current.Text = ((DataRowView)e.Item.DataItem)["Current"].ToString();

                    RadNumericTextBox previous = (RadNumericTextBox)edititem.FindControl("RadPrevious");
                    previous.Text = ((DataRowView)e.Item.DataItem)["previous"].ToString();

                    RadNumericTextBox change = (RadNumericTextBox)edititem.FindControl("RadChange");
                    change.Text = ((DataRowView)e.Item.DataItem)["Change"].ToString();

                    RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                    RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                    selectedItemdriver.Text = ((DataRowView)e.Item.DataItem)["Driver"].ToString();
                    selectedItemdriver.Value = ((DataRowView)e.Item.DataItem)["DriverName"].ToString();
                    combodriver.Items.Add(selectedItemdriver);
                    selectedItemdriver.DataBind();
                    Session["Driver"] = selectedItemdriver.Value;
                    RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                    DataSet dsdriver = new DataSet();
                    DataAccess wsdriver = new DataAccess();
                    dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(comboBrand.SelectedValue));
                    comboBranddriver.DataSource = dsdriver;
                    comboBranddriver.DataTextField = "Description";
                    comboBranddriver.DataValueField = "Driver_ID";
                    comboBranddriver.DataBind();
                    comboBranddriver.SelectedValue = selectedItemdriver.Value;

                }
            }
        }
        protected void RadGrid9_InsertCommand(object sender, GridCommandEventArgs e)
        {

            if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
            {

                GridEditableItem item = (GridEditableItem)e.Item;

                RadComboBox RadKPI = (RadComboBox)item.FindControl("RadKPI9");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadNumericTextBox RadCurrent = (RadNumericTextBox)item.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)item.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)item.FindControl("RadChange");
                RadTextBox RadBehaviour = (RadTextBox)item.FindControl("RadBehaviour");
                RadTextBox RadRootCause = (RadTextBox)item.FindControl("RadRootCause");
                DataTable dt = new DataTable();
                DataRow dr;
                //assigning ViewState value in Data Table  
                dt = (DataTable)ViewState["DataEntry"];
                //Adding value in datatable  
                if (RadKPI.SelectedIndex != -1 || RadDriver.SelectedIndex != -1 || RadTarget.Text != null || RadPrevious.Text != null || RadCurrent.Text != null || RadChange.Text != null || RadBehaviour.Text != null || RadRootCause.Text != null)
                {
                    dr = dt.NewRow();
                    dr["KPIID"] = RadKPI.SelectedItem.Text;
                    dr["Name"] = RadKPI.SelectedValue;
                    dr["Driver"] = RadDriver.SelectedItem.Text;
                    dr["DriverName"] = RadDriver.SelectedValue;
                    dr["Target"] = RadTarget.Text;
                    dr["Current"] = RadCurrent.Text;
                    dr["Previous"] = RadPrevious.Text;
                    dr["Change"] = RadChange.Text;
                    dr["Behaviour"] = RadBehaviour.Text;
                    dr["RootCause"] = RadRootCause.Text;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["DataEntry"] = dt;
                    }
                }
                else
                {
                    // RadWindowManager1.RadAlert("Cannot accept blank fields." + "", 500, 200, "Error Message", "", "");
                    string ModalLabel = "Cannot accept blank fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }


            //here bind Listview after data save in ViewState  
            this.BindListView();


        }
        protected void RadGrid9_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
            {

                GridEditFormItem updateItem = (GridEditFormItem)e.Item;
                int rowindex = updateItem.ItemIndex;
                RadComboBox RadKPI = (RadComboBox)updateItem.FindControl("RadKPI9");
                RadComboBox RadDriver = (RadComboBox)updateItem.FindControl("RadDriver");
                RadTextBox RadTarget = (RadTextBox)updateItem.FindControl("RadTarget");
                RadNumericTextBox RadCurrent = (RadNumericTextBox)updateItem.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)updateItem.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)updateItem.FindControl("RadChange");
                RadTextBox RadBehaviour = (RadTextBox)updateItem.FindControl("RadBehaviour");
                RadTextBox RadRootCause = (RadTextBox)updateItem.FindControl("RadRootCause");
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["DataEntry"];
                if (RadPrevious.Text != null || RadCurrent.Text != null || RadChange.Text != null || RadBehaviour.Text != null || RadRootCause.Text != null)
                {

                    dt.Rows[rowindex]["KPIID"] = RadKPI.SelectedItem.Text;
                    dt.Rows[rowindex]["Name"] = RadKPI.SelectedValue;
                    dt.Rows[rowindex]["Driver"] = RadDriver.SelectedItem.Text;
                    dt.Rows[rowindex]["DriverName"] = RadDriver.SelectedValue;
                    dt.Rows[rowindex]["Target"] = RadTarget.Text;
                    dt.Rows[rowindex]["Current"] = RadCurrent.Text;
                    dt.Rows[rowindex]["Previous"] = RadPrevious.Text;
                    dt.Rows[rowindex]["Change"] = RadChange.Text;
                    dt.Rows[rowindex]["Behaviour"] = RadBehaviour.Text;
                    dt.Rows[rowindex]["RootCause"] = RadRootCause.Text;
                    ViewState["DataEntry"] = dt;
                    RadGrid9.MasterTableView.ClearEditItems();
                    //RadGrid1.Rebind();
                }
                else
                {
                    // RadWindowManager1.RadAlert("Cannot accept blank fields." + "", 500, 200, "Error Message", "", "");
                    string ModalLabel = "Cannot accept blank fields.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }

            }

            //here bind Listview after data save in ViewState  
            this.BindListView();
        }
        protected void RadGrid9_DeleteCommand(object sender, GridCommandEventArgs e)
        {

            GridDataItem deleteitem = (GridDataItem)e.Item;
            Label LblID = (Label)deleteitem.FindControl("LblID");

            string ID = LblID.Text;
            DataTable dt = (DataTable)ViewState["DataEntry"];

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                if (dr["ID"].ToString() == ID)
                {
                    dr.Delete();
                }
            }


            this.BindListView();

            if (RadGrid9.Items.Count == 0)
            {
                RadGrid9.MasterTableView.IsItemInserted = true;
                RadGrid9.MasterTableView.Rebind();
            }
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            RadGrid9.MasterTableView.IsItemInserted = true;
            RadGrid9.MasterTableView.Rebind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
             
            if (!IsPostBack)
            {


                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                    RadReviewID.Text = "0";
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    NexidiaForm.Visible = false;
                    ODForm.Visible = false;
                    DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CIMNumber, 2);
                    if (ds_userselect2.Tables[0].Rows.Count > 0)
                    {
                        bool Supervisor;
                        Supervisor = CheckIfHasSubordinates(CIMNumber);
                        
                        if (Supervisor == true)
                        {
                            string CIM = "";
                            if (Request["CIM"] != null) //Add Review Thru My Team
                            {
                                CIM = Request.QueryString["CIM"];
                                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                                ctrlA.Text = "> My Team > Add HR Review >";
                                SetLabels(Convert.ToInt32(CIM));
                              

                            }
                            else //Add Review Thru My Reviews
                            {
                                //Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                                //ctrlA.Text = " > Add HR Review";
                                //Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                                //ctrlB.Text = "";

                                Label ctrlReviews = (Label)DashboardMyReviewsUserControl1.FindControl("LblMyReviews");
                                ctrlReviews.Text = " Coaching Dashboard ";
                                Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                                ctrlA.Text = " > My Reviews";
                                Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                                ctrlB.Text = " > Add HR Review";
                                ctrlB.Visible = true;

                                RadAccount.Enabled = true;
                                RadSupervisor.Enabled = true;
                                RadCoacheeName.Enabled = true;
                                GetAccounts(CIMNumber);


                            }

                            GetDropDownSessions();
                  
                            RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                            RadDocumentationReview.DataSource = string.Empty;
                            RadDocumentationReview.Rebind();
                            RadGridDocumentation.DataSource = string.Empty;
                            RadGridDocumentation.Rebind();
                            RadGrid8.DataSource = string.Empty;
                            RadGrid8.Rebind();
                            RadGrid12.DataSource = string.Empty;
                            RadGrid12.Rebind();
                            PerfResults(); 
                            RadGrid4.Rebind();
                            RadGrid4.MasterTableView.Rebind(); 
                            LoadCommitmentQuestions();
                            string Role;
                            Role = CheckIfHRQA(CIMNumber);

                            SelectSearchBlackout.Visible = false;
                            SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-6";
                            SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-6";
                             
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                            Session["DefaultUserAcct"] = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                            Panel1.Visible = false;
                            Panel2.Visible = false;
                            Panel3.Visible = false;
                            Panel4.Visible = true;
                            //}
                            RadGrid9.MasterTableView.IsItemInserted = true;
                            //RadGrid1.Rebind();
                            RadGrid9.Rebind();
                            RadGrid9.MasterTableView.Rebind();

                            if (Request["CIM"] != null) //Add Review Thru My Team
                            {
                                DataSet ds1 = null;
                                DataAccess ws1 = new DataAccess();
                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                               // GetForm(Convert.ToInt32(ds1.Tables[0].Rows[0]["CampaignID"].ToString()));
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
               
           
            }

        }
        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }
        private bool IsPH(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }
        }
        public void GetDropDownSessions()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownSessions" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetDropDownSessions " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void GetAccounts(int CimNumber)
        {
            try
            {
                string Role;
                Role = CheckIfHRQA(CimNumber);

               
                    GetAccount(CimNumber);
                //    if (Role == "HR")
                //    { }
                //else
                //{
                //    DataTable dt = null;
                //    DataAccess ws = new DataAccess();
                //    dt = ws.GetSubordinates(CimNumber);

                //    var distinctRows = (from DataRow dRow in dt.Rows
                //                        select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                //    RadAccount.Items.Clear();

                //    foreach (var row in distinctRows)
                //    {
                //        RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                //    }
                //}
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetAccounts" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetAccounts " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected string CheckIfHRQA(int CimNumber)
        {

            DataAccess ws = new DataAccess();
            string Role = ws.CheckIfQAHR(CimNumber);
            if (Role != "")
            {
                return Role;
            }
            else
            {
                return Role;
            }
        }
        public void GetAccount(int CIMNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "Account";
                RadAccount.DataValueField = "AccountID";
                RadAccount.DataBind();
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("GetAccount" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetAccount " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected void GetSubordinatesWithTeam(int CimNumber, int AccountID)
        {
            try
            {
                //string Role;
                //Role = CheckIfHRQA(CimNumber);

                //if (Role == "HR")
                //{
                //    GetTLsPerAccount(AccountID);
                //}
                //else
                //{

                    RadSupervisor.Items.Clear();
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    //ds = ws.GetSubordinatesWithTeam(CimNumber, AccountID);
                    ds = ws.GetSubordinatesWithAccounts4(CimNumber, AccountID); 
                    RadSupervisor.DataSource = ds;
                    RadSupervisor.DataTextField = "Name";
                    RadSupervisor.DataValueField = "CimNumber";
                    RadSupervisor.DataBind();

                    DataSet dsInfo = null;
                    DataAccess wsInfo = new DataAccess();
                    dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CimNumber));
                    if (dsInfo.Tables[0].Rows.Count > 0)
                    {
                        string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                        string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                        string FullName = FirstName + " " + LastName;
                        RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CimNumber.ToString()));
                    }
                //}
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSubordinatesWithTeam" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSubordinatesWithTeam " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        public void GetTLsPerAccount(int AccountID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount(AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("GetTLsPerAccount" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetTLsPerAccount " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSupervisors(Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
            RadSupervisor.Text = string.Empty;
            RadSupervisor.ClearSelection();
            RadSupervisor.SelectedIndex = -1;
            RadSessionType.Text = string.Empty;
            RadSessionType.ClearSelection();
            RadSessionType.SelectedIndex = -1;
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;
            GetDropDownSessions();
         //   GetForm(Convert.ToInt32(RadAccount.SelectedValue));
        }
        public void LoadTTSupervisors(int AccountID)
        {
            try
            {
                //DataSet ds = null;
                //DataAccess ws = new DataAccess();
                //ds = ws.GetTeamLeaderPerAccount(AccountID);

                //RadSupervisor.DataSource = ds;
                //RadSupervisor.DataTextField = "Name";
                //RadSupervisor.DataValueField = "CimNumber";
                //RadSupervisor.DataBind();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();

                int CIMNumber = Convert.ToInt32(cim_num);

                //string Role;
                //Role = CheckIfHRQA(CIMNumber);

                //if (Role == "HR")
                //{
                //    DataSet ds = null;
                //    DataAccess ws = new DataAccess();
                //    ds = ws.GetTeamLeaderPerAccount2(AccountID);

                //    RadSupervisor.DataSource = ds;
                //    RadSupervisor.DataTextField = "Name";
                //    RadSupervisor.DataValueField = "CimNumber";
                //    RadSupervisor.DataBind();
                //}
                //else
                //{
                    //GetSubordinatesWithTeam(Convert.ToInt32(CIMNumber), Convert.ToInt32(RadAccount.SelectedValue));
                //}


                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetTeamLeaderPerAccount2(AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadTTSupervisors" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadTTSupervisors " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
        }
        public void LoadTTSubordinates(int CimNumber, int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
//                dt = ws.GetSubordinatesWithAccounts(CimNumber, AccountID);
                dt = ws.GetSubordinatesWithAccounts3(CimNumber, AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadTTSubordinates" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadTTSubordinates " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadCoacheeName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                {
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()));


                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    HiddenCoacheeSSS.Value = ds.Tables[0].Rows[0]["SSN"].ToString();
                    //}
                     
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();
                    RadGridPR.DataSource = null;
                    RadGridPR.Rebind();
                    RadGrid6.DataSource = null;
                    RadGrid6.Rebind();
                    RadGrid11.DataSource = null;
                    RadGrid11.Rebind();
                    RadGrid7.DataSource = null;
                    RadGrid7.Rebind();
                    if (CheckBox1.Visible == true)
                    {
                        if (CheckBox1.Checked == true)
                        {
                            CheckBox1.Checked = false;
                        }
                    }
                    if (CheckBox2.Visible == true)
                    {
                        if (CheckBox2.Checked == true)
                        {
                            CheckBox2.Checked = false;
                        }
                    }
                }
                else
                {
                     
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();
                    RadGridPR.DataSource = null;
                    RadGridPR.Rebind();
                    RadGrid6.DataSource = null;
                    RadGrid6.Rebind();
                    RadGrid11.DataSource = null;
                    RadGrid11.Rebind();
                    RadGrid7.DataSource = null;
                    RadGrid7.Rebind();
                    if (CheckBox1.Visible == true)
                    {
                        if (CheckBox1.Checked == true)
                        {
                            CheckBox1.Checked = false;
                        }
                    }
                    if (CheckBox2.Visible == true)
                    {
                        if (CheckBox2.Checked == true)
                        {
                            CheckBox2.Checked = false;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadCoacheeName_SelectedIndexChanged" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "RadCoacheeName_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void SetLabels(int CIMNumber)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));


                if (ds.Tables[0].Rows.Count > 0)
                {
                    //GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));

                    string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();

                    string AccountID = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                    string Account = ds.Tables[0].Rows[0]["Account"].ToString();

                    string SupervisorID = ds.Tables[0].Rows[0]["Reports_To"].ToString();
                    string SupervisorFirstName = ds.Tables[0].Rows[0]["SupFirst Name"].ToString();
                    string SupervisorLastName = ds.Tables[0].Rows[0]["SupLast Name"].ToString();

                    string FullName = FirstName + " " + LastName;
                    string SupFullName = SupervisorFirstName + " " + SupervisorLastName;
                    //HiddenCoacheeSSS.Value = ds.Tables[0].Rows[0]["SSN"].ToString();
                    //HiddenCoacheeCIM.Value = CIMNumber.ToString();

                    RadAccount.AllowCustomText = true;
                    RadAccount.Text = Account;
                    RadAccount.SelectedValue = AccountID;
                    RadCoacheeName.AllowCustomText = true;
                    RadCoacheeName.SelectedValue = CIMNumber.ToString();
                    RadCoacheeName.Text = FullName;
                    RadSupervisor.AllowCustomText = true;
                    RadSupervisor.SelectedValue = SupervisorID;
                    RadSupervisor.Text = SupFullName;
                    RadAccount.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSupervisor.Enabled = false;
                     
                    //LblSelected.Text = FullName;
                    Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                    ctrlA.Text = FullName;
                    ctrlA.Visible = true;


                    SelectCoacheeGroup.Visible = false;
                    //CoacheeInfoGroup.Visible = false;
                    //RadSessionType.Width = new Unit("50%");
                    //RadSessionTopic.Width = new Unit("50%");
                    //RadFollowup.Width = new Unit("35%");
                    //if (RadCallID.Visible == true)
                    //{
                    //    RadCallID.Width = new Unit("50%");
                    //}
                }

                 
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("SetLabels" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "SetLabels " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }


        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = lstEmployee;
        }
        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllData();
                lstEmployee.Insert(0, new Employee() { UniqueID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid1.Rebind();
            }

            if (e.CommandName == "Delete")
            {
                Guid itemID = (Guid)(((GridDataItem)e.Item).GetDataKeyValue("UniqueID"));
                foreach (var n in lstEmployee.Where(p => p.UniqueID == itemID).ToArray()) lstEmployee.Remove(n);
                RadGrid1.Rebind();


                string ModalLabel = "The selected performance result has been removed";
                string ModalHeader = "Removing performance result";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (lstEmployee.Count == 0)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid1.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid1.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            commandItem.FireCommandEvent("InitInsert", null);
                        }
                    }
                    else
                    {
                        string ModalLabel = "Please do not leave any field blank in Performance Results";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }

        }
        protected void saveAllData()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
            {
                Guid UniqueID = new Guid(item.GetDataKeyValue("UniqueID").ToString());
                Employee emp = lstEmployee.Where(i => i.UniqueID == UniqueID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI") as RadComboBox).SelectedItem.Value); //.SelectedValue);
                emp.Name = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadKPI") as RadComboBox).SelectedItem.Text;
            }
        }
        protected void OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem insertitem = (GridDataItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    //ds = ws.GetKPIList();
                    //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsTarget = new DataSet();
                            DataAccess wsTarget = new DataAccess();
                            dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                            RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }
                    }
                }
            }
            else
            {
                if (e.Item is GridDataItem)
                {
                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridDataItem insertitem = (GridDataItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        //ds = ws.GetKPIList();
                        //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");
                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsTarget = new DataSet();
                                DataAccess wsTarget = new DataAccess();
                                dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                                RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }
                        }
                    }
                }
            }

        }
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {

            if (lstEmployee.Count == 0)
            {
                if (!IsPostBack)
                {
                    GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    commandItem.FireCommandEvent("InitInsert", null);

                }
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid1.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid1.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            if (!IsPostBack)
                            {
                                GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                                commandItem.FireCommandEvent("InitInsert", null);

                            }
                        }

                    }
                }


            }

        }
        protected void OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    if (a != "0")
                    {
                        selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                        combo.Items.Add(selectedItem);
                        selectedItem.DataBind();
                        Session["KPI"] = selectedItem.Value;
                        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIList();
                        comboBrand.DataSource = ds;
                        comboBrand.DataBind();
                        comboBrand.SelectedValue = selectedItem.Value;
                    }
                    if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                    {

                        RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                        DataSet dsTarget = null;
                        DataAccess wsTarget = new DataAccess();
                        dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                        target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                        RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                        RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                        string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                        if (b != "0")
                        {
                            selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                            combodriver.Items.Add(selectedItemdriver);
                            selectedItemdriver.DataBind();
                            Session["Driver"] = selectedItemdriver.Value;
                            RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                            DataSet dsdriver = new DataSet();
                            DataAccess wsdriver = new DataAccess();
                            dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                            comboBranddriver.DataSource = dsdriver;
                            comboBranddriver.DataTextField = "Description";
                            comboBranddriver.DataValueField = "Driver_ID";
                            comboBranddriver.DataBind();
                            comboBranddriver.SelectedValue = selectedItemdriver.Value;
                        }
                    }
                }
            }

        }
        public void RadKPI_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPI_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void RadKPINexidia_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPINexidia_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void RadKPIOD_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            try
            {
                RadComboBox combo = sender as RadComboBox;
                GridEditableItem item = (GridEditableItem)combo.NamingContainer;
                int index = item.ItemIndex;
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "RadKPIOD_SelectedIndexChanged " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void RadGridDocumentation_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void RadGrid4_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid4.DataSource = lstNexidia;
        }
        protected void RadGrid4_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                saveAllDataNexidia();
                lstNexidia.Insert(0, new Nexidia() { NexidiaID = Guid.NewGuid() });
                e.Canceled = true;
                RadGrid4.Rebind();
            }
        }
        protected void saveAllDataNexidia()
        {
            //Update Session
            foreach (GridDataItem item in RadGrid4.MasterTableView.Items)
            {
                Guid NexidiaID = new Guid(item.GetDataKeyValue("NexidiaID").ToString());
                Nexidia emp = lstNexidia.Where(i => i.NexidiaID == NexidiaID).First();
                emp.KPIID = Convert.ToInt32((item.FindControl("RadKPI4") as RadComboBox).SelectedValue);
                emp.Name = (item.FindControl("RadKPI4") as RadComboBox).SelectedItem.Text;
                emp.Target = (item.FindControl("RadTarget") as RadTextBox).Text;
                emp.Current = (item.FindControl("RadCurrent") as RadNumericTextBox).Text;
                emp.Previous = (item.FindControl("RadPrevious") as RadNumericTextBox).Text;
                emp.Driver = Convert.ToInt32((item.FindControl("RadDriver") as RadComboBox).SelectedValue);
                emp.DriverName = (item.FindControl("RadDriver") as RadComboBox).SelectedItem.Text;

            }
        }
        protected void RadGrid4_OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem insertitem = (GridDataItem)e.Item;
                    RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI4");
                    DataSet ds = new DataSet();
                    DataAccess ws = new DataAccess();
                    //ds = ws.GetKPIList();
                    //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                    ds = ws.GetKPIListPerAcct(RadAccount.SelectedValue);
                    if (ds.Tables.Count > 0)
                    {
                        combo.DataSource = ds;
                        combo.DataTextField = "Name";
                        combo.DataValueField = "KPIID";
                        combo.DataBind();
                        combo.ClearSelection();

                        RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                        RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                        if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                        {
                            DataSet dsTarget = null;
                            DataAccess wsTarget = new DataAccess();
                            dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                            RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                            DataSet dsdrivers = null;
                            DataAccess ws2 = new DataAccess();
                            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                            RadDriver.DataSource = dsdrivers;
                            RadDriver.DataTextField = "Description";
                            RadDriver.DataValueField = "Driver_ID";
                            RadDriver.DataBind();
                        }
                    }

                }
            }
            else
            {
                if (e.Item is GridDataItem)
                {
                    if (Session["DefaultUserAcct"] != null)
                    {
                        string DefaultUserAcct = Session["DefaultUserAcct"].ToString();
                        GridDataItem insertitem = (GridDataItem)e.Item;
                        RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI4");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        //ds = ws.GetKPIList();
                        //ds = ws.GetKPIListPerUser(Convert.ToInt32(RadAccount.SelectedValue));
                        ds = ws.GetKPIListPerAcct(DefaultUserAcct);
                        if (ds.Tables.Count > 0)
                        {
                            combo.DataSource = ds;
                            combo.DataTextField = "Name";
                            combo.DataValueField = "KPIID";
                            combo.DataBind();
                            combo.ClearSelection();

                            RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                            RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                            if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                            {
                                DataSet dsTarget = null;
                                DataAccess wsTarget = new DataAccess();
                                dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                                RadTarget.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();

                                DataSet dsdrivers = null;
                                DataAccess ws2 = new DataAccess();
                                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                                RadDriver.DataSource = dsdrivers;
                                RadDriver.DataTextField = "Description";
                                RadDriver.DataValueField = "Driver_ID";
                                RadDriver.DataBind();
                            }
                        }
                    }

                }
            }
        }
        protected void RadGrid4_PreRender(object sender, EventArgs e)
        {
            if (lstNexidia.Count == 0)
            {
                if (!IsPostBack)
                {
                    GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    commandItem.FireCommandEvent("InitInsert", null);
                }
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid4.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid4.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI4") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            if (!IsPostBack)
                            {
                                GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                                commandItem.FireCommandEvent("InitInsert", null);
                            }
                        }

                    }
                }


            }
        }
        protected void RadGrid4_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (RadAccount.SelectedIndex != -1 || RadAccount.AllowCustomText == true)
            {
                if (e.Item is GridDataItem)
                {
                    GridEditableItem edititem = (GridEditableItem)e.Item;
                    RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI4");
                    RadComboBoxItem selectedItem = new RadComboBoxItem();
                    string a = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                    if (a != "0")
                    {
                        selectedItem.Value = (string)DataBinder.Eval(e.Item.DataItem, "KPIID").ToString();
                        combo.Items.Add(selectedItem);
                        selectedItem.DataBind();
                        Session["KPI"] = selectedItem.Value;
                        RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI4");
                        DataSet ds = new DataSet();
                        DataAccess ws = new DataAccess();
                        ds = ws.GetKPIList();
                        comboBrand.DataSource = ds;
                        comboBrand.DataBind();
                        comboBrand.SelectedValue = selectedItem.Value;
                    }

                    if (combo.SelectedIndex != -1 || combo.AllowCustomText == true)
                    {
                        RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                        DataSet dsTarget = null;
                        DataAccess wsTarget = new DataAccess();
                        dsTarget = wsTarget.GetKPITargets(Convert.ToInt32(combo.SelectedValue));
                        target.Text = dsTarget.Tables[0].Rows[0]["KPITarget"].ToString();


                        RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                        RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                        string b = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                        if (b != "0")
                        {
                            selectedItemdriver.Value = (string)DataBinder.Eval(e.Item.DataItem, "Driver").ToString();
                            combodriver.Items.Add(selectedItemdriver);
                            selectedItemdriver.DataBind();
                            Session["Driver"] = selectedItemdriver.Value;
                            RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                            DataSet dsdriver = new DataSet();
                            DataAccess wsdriver = new DataAccess();
                            dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(Session["KPI"]));
                            comboBranddriver.DataSource = dsdriver;
                            comboBranddriver.DataTextField = "Description";
                            comboBranddriver.DataValueField = "Driver_ID";
                            comboBranddriver.DataBind();
                            comboBranddriver.SelectedValue = selectedItemdriver.Value;
                        }
                    }
                }
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            if (lstNexidia.Count == 0)
            {
                GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                commandItem.FireCommandEvent("InitInsert", null);
            }
            else
            {

                int itemcount = 0;
                foreach (GridDataItem item in RadGrid4.Items)
                {
                    GridDataItem dataItem = item as GridDataItem;
                    int radcount = RadGrid4.Items.Count;
                    string current = (dataItem.FindControl("RadCurrent") as RadNumericTextBox).Text;
                    string previous = (dataItem.FindControl("RadPrevious") as RadNumericTextBox).Text;
                    string kpi = (dataItem.FindControl("RadKPI4") as RadComboBox).SelectedValue;
                    string target = (dataItem.FindControl("RadTarget") as RadTextBox).Text;
                    string driver = (dataItem.FindControl("RadDriver") as RadComboBox).SelectedValue;
                    if (current != "" && previous != "")
                    {
                        itemcount++;

                        if (itemcount == radcount)
                        {
                            GridCommandItem commandItem = (GridCommandItem)RadGrid4.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                            commandItem.FireCommandEvent("InitInsert", null);
                        }
                    }
                    else
                    {
                        string ModalLabel = "Please do not leave any field blank in Performance Results";
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    }
                }


            }
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
            int CIMNumber = Convert.ToInt32(cim_num);
            GetDropDownTopics(Convert.ToInt32(RadSessionType.SelectedValue), CIMNumber);
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;

            //if (Request["CIM"] != null)
            //{
            //    DataSet ds = null;
            //    DataAccess ws = new DataAccess();
            //    ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));
            //    }
            //}

        }
        protected void GetDropDownTopics(int SessionID, int CimNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics3(SessionID, CimNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
            }
            catch (Exception ex)
            {
                string ModalLabel = "GetDropDownTopics " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void VisibleCheckBox()
        {
            try
            {
                if (RadAccount.SelectedIndex != -1) //(Session["Account"] != null)
                {
                    DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                    string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                    int CIMNumber = Convert.ToInt32(cim_num);
                    string Account = RadAccount.SelectedValue.ToString();// Session["Account"].ToString();
                    if (RadSessionTopic.SelectedItem.Text != "Awareness" ) // && RadSessionTopic.SelectedItem.Text != "1st Warning")
                    {

                        //CNPR.Visible = false;
                        CheckBox1.Visible = true;
                        CheckBox2.Visible = true;
                        //AddReviewDefault.Visible = true;
                        ////AddReviewDefault2.Visible = true;
                        //RadSearchBlackout.Visible = false;
                        //PerfResult.Visible = true;
                        //CMTView.Visible = false;
                        //FollowUp.Visible = true;
                        //SignOut.Visible = true;
                        ////CMTButtons.Visible = false;
                        ////RadCMTPreview.Visible = false;
                        ////RadCMTSaveSubmit.Visible = false;
                        //ODForm.Visible = false;
                        //Div23.Visible = false;
                        //Div27.Visible = false;
                        //Div21.Visible = false;
                        //CheckBox2.Visible = false;
                        //DefaultPane1.Visible = true;
                        //DefaultPane3.Visible = true;
                        //DefaultPane5.Visible = true;
                    }
                }
                else
                {
                    RadSessionTopic.Text = string.Empty;
                    RadSessionTopic.ClearSelection();
                    RadSessionTopic.SelectedIndex = -1;
                    string myStringVariable = "Please select the account first.";
                    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + myStringVariable + "'); });", true);

                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("VisibleControls" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "VisibleCheckBox " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadGrid8_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void RadGrid9_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid9.DataSource = DataEntry;
        }
        //public void LoadCMTDropdowns()
        //{
        //    DataSet ds = null;
        //    DataAccess ws = new DataAccess();
        //    ds = ws.GetCMTDropdowns(1, 0);

        //    RadCaseLevel.DataSource = ds;
        //    RadCaseLevel.DataTextField = "Case_Name";
        //    RadCaseLevel.DataValueField = "Case_ID";
        //    RadCaseLevel.DataBind();

        //    DataSet ds2 = null;
        //    DataAccess ws2 = new DataAccess();
        //    ds2 = ws2.GetCMTDropdowns(2, 0);

        //    RadMajorCategory.DataSource = ds2;
        //    RadMajorCategory.DataTextField = "MJ_Name";
        //    RadMajorCategory.DataValueField = "MJ_ID";
        //    RadMajorCategory.DataBind();

        //    DataSet ds3 = null;
        //    DataAccess ws3 = new DataAccess();
        //    ds3 = ws3.GetCMTDropdowns(4, 0);

        //    RadStatus.DataSource = ds3;
        //    RadStatus.DataTextField = "Status";
        //    RadStatus.DataValueField = "ID";
        //    RadStatus.DataBind();

        //    DataSet ds4 = null;
        //    DataAccess ws4 = new DataAccess();
        //    ds4 = ws4.GetCMTDropdowns(5, 0);

        //    //RadCaseDecision.DataSource = ds4;
        //    //RadCaseDecision.DataTextField = "Decision_Name";
        //    //RadCaseDecision.DataValueField = "ID";
        //    //RadCaseDecision.DataBind();

        //    DataSet ds5 = null;
        //    DataAccess ws5 = new DataAccess();
        //    ds5 = ws5.GetCMTDropdowns(6, 0);

        //    RadHoldCaseType.DataSource = ds5;
        //    RadHoldCaseType.DataTextField = "Hold_Case_Type";
        //    RadHoldCaseType.DataValueField = "ID";
        //    RadHoldCaseType.DataBind();

        //    //RadNODHoldCaseType.DataSource = ds5;
        //    //RadNODHoldCaseType.DataTextField = "Hold_Case_Type";
        //    //RadNODHoldCaseType.DataValueField = "ID";
        //    //RadNODHoldCaseType.DataBind();

        //    DataSet ds6 = null;
        //    DataAccess ws6 = new DataAccess();
        //    ds6 = ws6.GetCMTDropdowns(7, 0);


        //    //RadNODNonAcceptance.DataSource = ds6;
        //    //RadNODNonAcceptance.DataTextField = "Non_Acceptance_Reason";
        //    //RadNODNonAcceptance.DataValueField = "ID";
        //    //RadNODNonAcceptance.DataBind();


        //    RadNonAcceptance.DataSource = ds6;
        //    RadNonAcceptance.DataTextField = "Non_Acceptance_Reason";
        //    RadNonAcceptance.DataValueField = "ID";
        //    RadNonAcceptance.DataBind();

        //    DataSet ds7 = null;
        //    DataAccess ws7 = new DataAccess();
        //    ds7 = ws7.GetCMTDropdowns(8, 0);


        //    //RadEoW.DataSource = ds7;
        //    //RadEoW.DataTextField = "Act_Rqrd";
        //    //RadEoW.DataValueField = "ID";
        //    //RadEoW.DataBind();
        //}
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
           // VisibleCheckBox();
        }
        public void GetForm(int AccountID)
        {

            int Account = 0;
            DataAccess ws = new DataAccess();
            Account = ws.GetAccountType(Convert.ToInt32(AccountID));
            SelectSearchBlackout.Visible = false;
            SelectCoacheeGroup.Attributes["class"] = "form-group col-sm-6";
            SelectCoachingSpecifics.Attributes["class"] = "form-group col-sm-6";
            if (Account == 2)
            {
                //HyperLinkRev.NavigateUrl = string.Format("~/AddNexidiaReview.aspx");
                DefaultForm.Visible = false;
                NexidiaForm.Visible = true;
                ODForm.Visible = false;
                CMTView.Visible = false;
                GetSessionFocus("Nexidia");
                //CMTButtons.Visible = false;
                //RadCMTPreview.Visible = false;
                //RadCMTSaveSubmit.Visible = false;
                CallID.Visible = false;
                CallIDDiv.Visible = false;
                CheckBox2.Visible = true;
                Session["Account"] = "TalkTalk";
                SessionFocusGroup.Visible = true;
                RadGrid4.Enabled = true;
                RadGrid4.Rebind();
                RadGrid4.MasterTableView.Rebind();

            }

            else if (Account == 3)
            {
                DefaultForm.Visible = false;
                NexidiaForm.Visible = false;
                ODForm.Visible = true;
                CMTView.Visible = false;
                CallID.Visible = true;
                CallIDDiv.Visible = true;
                GetSessionFocus("OD");
                //CMTButtons.Visible = false;
                //RadCMTPreview.Visible = false;
                //RadCMTSaveSubmit.Visible = false;
                //DataSet ds_scoring = DataHelper.GetCommentsforTriad(0);
                //grd_Commitment.DataSource = ds_scoring;
                LoadCommitmentQuestions();
                CheckBox2.Visible = false;
                Session["Account"] = "OD";
                SessionFocusGroup.Visible = true;
                RadGrid9.Enabled = true;
                RadGrid9.Rebind();
                RadGrid9.MasterTableView.Rebind();


            }
            else
            {
                //HyperLinkRev.NavigateUrl = string.Format("~/AddReview.aspx");
                DefaultForm.Visible = true;
                NexidiaForm.Visible = false;
                ODForm.Visible = false;
                CMTView.Visible = false;
                CallID.Visible = false;
                //CMTButtons.Visible = false;
                //RadCMTPreview.Visible = false;
                //RadCMTSaveSubmit.Visible = false;
                CallIDDiv.Visible = false;
                SessionFocusGroup.Visible = false;
                CheckBox2.Visible = true;
                Session["Account"] = "Default";
                RadGrid1.Enabled = true;
                RadGrid1.Rebind();
                RadGrid1.MasterTableView.Rebind();


            }

        }
        protected bool CheckIfOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public void GetSessionFocus(string Account)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionFocus(Account);
                CheckBoxList1.DataSource = ds;
                CheckBoxList1.DataTextField = "SessionFocusName";
                CheckBoxList1.DataValueField = "ID";
                CheckBoxList1.DataBind();
            }

            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetSessionFocus" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "GetSessionFocus " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        protected void RadGrid12_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;
            }
        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (RadAccount.SelectedIndex != -1)  //(Session["Account"] != null)
            {
                string Account = RadAccount.SelectedValue.ToString(); //Session["Account"].ToString();
                if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
                {

                    if (RadSessionType.SelectedIndex != -1 || RadSessionType.AllowCustomText == true)
                    {
                        //if (Account == "Default")
                        //{
                            if (CheckBox1.Checked == true)
                            {
                                CoachingNotes();
                                int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                                DataSet ds = null;
                                DataAccess ws = new DataAccess();
                                //ds = ws.GetCoachingNotes(CoachingCIM);
                                int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                                ds = ws.GetCoachingNotesViaCimAndST1(CoachingCIM, SessionType);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    RadGrid2.DataSource = ds;
                                    RadGrid2.DataBind();
                                    RadGrid2.Visible = true;
                                    RadGrid3.Visible = false;
                                    //Window1.VisibleOnPageLoad = true;
                                    RadPRLink.Visible = false;
                                    RadPRCancel.Visible = false;
                                    RadCNLink.Visible = true;
                                    RadCNCancel.Visible = true;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                    RadGridCN.Visible = true;

                                }
                                else
                                {
                                    // RadWindowManager1.RadAlert("CheckBox1_CheckedChanged" + myStringVariable + "", 500, 200, "Error Message", "", "");
                                    string ModalLabel = "No data for previous coaching notes.";
                                    string ModalHeader = "Error Message";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                                }

                            }

                            else
                            {
                                //ViewState["CN"] = null;
                                RadGridCN.DataSource = null;
                                RadGridCN.DataBind();
                                RadGridCN.Visible = false;

                            }
                        //}
                        //else if (Account == "TalkTalk")
                        //{

                        //    if (CheckBox1.Checked == true)
                        //    {
                        //        CoachingNotes();
                        //        int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                        //        //int CoachingCIM = 10107032;
                        //        DataSet ds = null;
                        //        DataAccess ws = new DataAccess();
                        //        //ds = ws.GetCoachingNotes(CoachingCIM);
                        //        int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                        //        ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                        //        if (ds.Tables[0].Rows.Count > 0)
                        //        {
                        //            RadGrid2.DataSource = ds;
                        //            RadGrid2.DataBind();
                        //            RadGrid2.Visible = true;
                        //            RadGrid3.Visible = false;
                        //            //Window1.VisibleOnPageLoad = true;
                        //            RadPRLink.Visible = false;
                        //            RadPRCancel.Visible = false;
                        //            RadCNLink.Visible = true;
                        //            RadCNCancel.Visible = true;

                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                        //            RadGrid6.Visible = true;


                        //        }
                        //        else
                        //        {

                        //            string ModalLabel = "No data for previous performance results.";
                        //            //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        //            string ModalHeader = "Error Message";
                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        //        }
                        //    }

                        //    else
                        //    {
                        //        //ViewState["CN"] = null;
                        //        RadGrid6.DataSource = null;
                        //        RadGrid6.DataBind();
                        //        RadGrid6.Visible = false;


                        //    }
                        //}
                        //else
                        //{
                        //    if (CheckBox1.Checked == true)
                        //    {
                        //        CoachingNotes();
                        //        int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                        //        //int CoachingCIM = 10107032;
                        //        DataSet ds = null;
                        //        DataAccess ws = new DataAccess();
                        //        //ds = ws.GetCoachingNotes(CoachingCIM);
                        //        int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
                        //        ds = ws.GetCoachingNotesViaCimAndST(CoachingCIM, SessionType);
                        //        if (ds.Tables[0].Rows.Count > 0)
                        //        {
                        //            RadGrid2.DataSource = ds;
                        //            RadGrid2.DataBind();
                        //            RadGrid2.Visible = true;
                        //            RadGrid3.Visible = false;
                        //            //Window1.VisibleOnPageLoad = true;
                        //            RadPRLink.Visible = false;
                        //            RadPRCancel.Visible = false;
                        //            RadCNLink.Visible = true;
                        //            RadCNCancel.Visible = true;

                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                        //            RadGrid11.Visible = true;

                        //        }
                        //        else
                        //        {

                        //            string ModalLabel = "No data for previous performance results.";
                        //            //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        //            string ModalHeader = "Error Message";
                        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                        //        }
                        //    }

                        //    else
                        //    {
                        //        //ViewState["CN"] = null;
                        //        RadGrid11.DataSource = null;
                        //        RadGrid11.DataBind();
                        //        RadGrid11.Visible = false;

                        //        Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                        //        PreviousCommitment.Controls.Remove(Test2);

                        //    }
                        //}
                    }
                    else
                    {
                        CheckBox1.Checked = false;
                        string ModalLabel = "Please select the session type.";
                        //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        string ModalHeader = "Error Message";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    }
                }
                else
                {
                    CheckBox1.Checked = false;
                    string ModalLabel = "Please select the employee first.";
                    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                }
            }
            else
            {
                CheckBox1.Checked = false;
                string ModalLabel = "Please select the account first.";
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
            BindListViewCNCMT();
        }
        private void BindListViewCN()
        {
            string Account = RadAccount.SelectedValue.ToString(); //Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (Account == "Default")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridCN.DataSource = dt;
                    RadGridCN.DataBind();
                }
                else
                {
                    RadGridCN.DataSource = null;
                    RadGridCN.DataBind();
                }
            }
            else if (Account == "TalkTalk")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid6.DataSource = dt;
                    RadGrid6.DataBind();
                }
                else
                {
                    RadGrid6.DataSource = null;
                    RadGrid6.DataBind();
                }
            }
            else
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid11.DataSource = dt;
                    RadGrid11.DataBind();
                }
                else
                {
                    RadGrid11.DataSource = null;
                    RadGrid11.DataBind();
                }
            }

        }
        private void BindListViewCNCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            Session.Add("CMTCN", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridCNCMT.DataSource = dt;
                RadGridCNCMT.DataBind();
            }
            else
            {
                RadGridCNCMT.DataSource = null;
                RadGridCNCMT.DataBind();
            }
        }
        protected void RadCNLink_Click(object sender, EventArgs e)
        { 
            if ( RadAccount.SelectedValue != null ) //Session["Account"] != null)
            {
              
                //if (RadSessionTopic.SelectedItem.Text != "Termination")
                //{
                    string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid2.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        id =  item.GetDataKeyValue("ReviewID").ToString(); //item["ReviewID"].Text;
                        chec = chk.Checked;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        session = item["Session"].Text;
                        topic = item["Topic"].Text;
                        reviewdate = item["ReviewDate"].Text;
                        assignedby = item["AssignedBy"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["CN"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["ReviewID"] = id;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Session"] = session;
                        dr["Topic"] = topic;
                        dr["ReviewDate"] = reviewdate;
                        dr["AssignedBy"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["CN"] = dt;
                        }
                        this.BindListViewCNCMT();
                    //}


                }
                ViewState["CN"] = null;
                //Window1.VisibleOnPageLoad = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeModal(); });", true);
            }
            else
            {
                // RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadPreviousCommitment(int count)
        {

            DataTable dt = (DataTable)ViewState["CN"];
            foreach (DataRow row in dt.Rows)
            {
                string id = row.Field<string>("ReviewID");
                Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                Test2.ReviewIDSelected = Convert.ToInt32(id);
                PreviousCommitment.Controls.Add(Test2);
            }
        }
        protected void RadCNCancel_Click(object sender, EventArgs e)
        {
            string Account = Convert.ToString(RadAccount.SelectedValue); //Session["Account"].ToString();
            CheckBox1.Checked = false;
            if (Account == "Default")
            {
                RadGridCN.DataSource = null;
                RadGridCN.DataBind();
            }
            else if (Account == "TalkTalk")
            {
                RadGrid6.DataSource = null;
                RadGrid6.DataBind();
            }
            else
            {
                RadGrid11.DataSource = null;
                RadGrid11.DataBind();
            }
        }
        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if ( RadAccount.SelectedValue != null ) //Session["Account"] != null)
            {
                string Account = RadAccount.SelectedValue.ToString(); //Session["Account"].ToString();
                if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
                {
                    //if (Account == "Default")
                    //{

                        if (CheckBox2.Checked == true)
                        {
                            PerformanceResults();
                            int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                            DataSet ds = null;
                            DataAccess ws = new DataAccess();
                            ds = ws.GetPerformanceResults(CoachingCIM);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                RadGrid3.DataSource = ds;
                                RadGrid3.DataBind();
                                RadGrid3.Visible = true;
                                RadGrid2.Visible = false;
                                //Window2.VisibleOnPageLoad = true;
                                RadPRLink.Visible = true;
                                RadPRCancel.Visible = true;
                                RadCNLink.Visible = false;
                                RadCNCancel.Visible = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                                RadGridPR.Visible = true;

                            }
                            else
                            {
                                // RadWindowManager1.RadAlert("CheckBox2_CheckedChanged" + myStringVariable + "", 500, 200, "Error Message", "", "");
                                string ModalLabel = "No data for previous performance results.";
                                string ModalHeader = "Error Message";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                            }
                        }

                        else
                        {
                            RadGridPR.DataSource = null;
                            RadGridPR.DataBind();
                            RadGridPR.Visible = false;

                        }
                    //}
                    //else if (Account == "TalkTalk")
                    //{
                    //    if (CheckBox2.Checked == true)
                    //    {
                    //        PerformanceResults();
                    //        int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                    //        DataSet ds = null;
                    //        DataAccess ws = new DataAccess();
                    //        ds = ws.GetPerformanceResults(CoachingCIM);
                    //        if (ds.Tables[0].Rows.Count > 0)
                    //        {
                    //            RadGrid3.DataSource = ds;
                    //            RadGrid3.DataBind();
                    //            RadGrid3.Visible = true;
                    //            RadGrid2.Visible = false;
                    //            //Window2.VisibleOnPageLoad = true;
                    //            RadPRLink.Visible = true;
                    //            RadPRCancel.Visible = true;
                    //            RadCNLink.Visible = false;
                    //            RadCNCancel.Visible = false;
                    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openInc(); });", true);
                    //            RadGridPR.Visible = true;
                    //        }
                    //        else
                    //        {
                    //            // RadWindowManager1.RadAlert("CheckBox2_CheckedChanged" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    //            string ModalLabel = "No data for previous performance results.";
                    //            string ModalHeader = "Error Message";
                    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                    //        }
                    //    }

                    //    else
                    //    {
                    //        RadGridPR.DataSource = null;
                    //        RadGridPR.DataBind();
                    //        RadGridPR.Visible = false;

                    //    }
                    //}

                    //else
                    //{
                    //    CheckBox2.Checked = false;
                    //    string ModalLabel = "Please select the employee first.";
                    //    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    //    string ModalHeader = "Error Message";
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
                    //}
                }
                else
                {
                    CheckBox2.Checked = false;
                    string ModalLabel = "Please select the account first.";
                    //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }

            }
        }
        public void PerformanceResults()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewKPIID", typeof(string));
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Name", typeof(string));
            dt2.Columns.Add("Target", typeof(string));
            dt2.Columns.Add("Current", typeof(string));
            dt2.Columns.Add("Previous", typeof(string));
            dt2.Columns.Add("Driver_Name", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["PR"] = dt2;

            BindListViewPR();
            BindListViewPRCMT();
        }
        private void BindListViewPR()
        {
            string Account = RadAccount.SelectedValue.ToString();// Session["Account"].ToString();
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            if (Account == "Default")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGridPR.DataSource = dt;
                    RadGridPR.DataBind();
                }
                else
                {
                    RadGridPR.DataSource = null;
                    RadGridPR.DataBind();
                }
            }
            else if (Account == "TalkTalk")
            {
                if (dt.Rows.Count > 0 && dt != null)
                {

                    RadGrid7.DataSource = dt;
                    RadGrid7.DataBind();
                }
                else
                {
                    RadGrid7.DataSource = null;
                    RadGrid7.DataBind();
                }
            }

        }
        private void BindListViewPRCMT()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["PR"];
            Session.Add("CMTPR", dt);
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridPRCMT.DataSource = dt;
                RadGridPRCMT.DataBind();
            }
            else
            {
                RadGridPRCMT.DataSource = null;
                RadGridPRCMT.DataBind();
            }


        }
        protected void RadPRLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedIndex != -1)
            {   string Account = RadAccount.SelectedValue.ToString(); //Session["Account"].ToString();
                  
                    string id, coachingid, fullname, cimnumber, name, target, current, previous, drivername, reviewdate, assignedby;
                    bool chec;
                    foreach (GridDataItem item in RadGrid3.SelectedItems)
                    {
                        CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                        id = item["ReviewKPIID"].Text;
                        chec = chk.Checked;
                        coachingid = item["ID"].Text;
                        fullname = item["FullName"].Text;
                        cimnumber = item["CIMNumber"].Text;
                        name = item["Name"].Text;
                        target = item["Target"].Text;
                        current = item["Current"].Text;
                        previous = item["Previous"].Text;
                        drivername = item["Driver_Name"].Text;
                        reviewdate = item["ReviewDate"].Text;
                        assignedby = item["AssignedBy"].Text;

                        DataTable dt = new DataTable();
                        DataRow dr;
                        //assigning ViewState value in Data Table  
                        dt = (DataTable)ViewState["PR"];
                        //Adding value in datatable  
                        dr = dt.NewRow();
                        dr["ReviewKPIID"] = id;
                        dr["ReviewID"] = coachingid;
                        dr["FullName"] = fullname;
                        dr["CIMNumber"] = cimnumber;
                        dr["Name"] = name;
                        dr["Target"] = target;
                        dr["Current"] = current;
                        dr["Previous"] = previous;
                        dr["Driver_Name"] = drivername;
                        dr["ReviewDate"] = reviewdate;
                        dr["AssignedBy"] = assignedby;
                        dt.Rows.Add(dr);

                        if (dt != null)
                        {
                            ViewState["PR"] = dt;
                        }
                        this.BindListViewPRCMT();
                    }


            }
            else
            {
                //RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "Please select topic.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        protected void RadPRCancel_Click(object sender, EventArgs e)
        {
            string Account = RadAccount.SelectedValue.ToString();//Session["Account"].ToString();
            CheckBox2.Checked = false;
            if (Account == "Default")
            {
                RadGridPR.DataSource = null;
                RadGridPR.DataBind();
            }
            else if (Account == "TalkTalk")
            {
                RadGrid7.DataSource = null;
                RadGrid7.DataBind();
            }

        }
        protected void RadSearchBlackout_Click(object sender, EventArgs e)
        {
            //Window3.VisibleOnPageLoad = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openCMTv2(); });", true);

        }
        protected void RadSearchBlackoutYes_Click(object sender, EventArgs e)
        {
            HiddenSearchBlackout.Value = "1";
            RadSearchBlackout.Enabled = false;
            RadSearchBlackout.ForeColor = Color.Black;
            //RadCMTSaveSubmit.Text = "Save";
            // Window3.VisibleOnPageLoad = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);

        }
        protected void RadSearchBlackoutNo_Click(object sender, EventArgs e)
        {
            //Window3.VisibleOnPageLoad = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMT(); });", true);

        }
        protected void UploadDummy_Click(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
            //}

        }
        //protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string Account = Session["Account"].ToString();
        //        string emailAddressCCM, emailCoachee;
        //        if (ValidatePage() == true)
        //        {
        //            if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
        //            {
        //                if (RadSSN.Text != "")
        //                {
        //                    if (CheckSSN() == true)
        //                    {
        //                        if (Convert.ToInt32(RadReviewID.Text) == 0)
        //                        {
        //                            if (Account == "Default")
        //                            {

        //                                //RadAsyncUpload2.PostbackTriggers = new string[] { "UploadDummy" };
        //                                DataSet ds1 = null;
        //                                DataAccess ws1 = new DataAccess();
        //                                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
        //                                emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                emailAddressCCM = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
        //                                int ReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
        //                                RadReviewID.Text = Convert.ToString(ReviewID);
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(ReviewID);
        //                                //UploadDocumentationReview(0, ReviewID);
        //                                //UploadDummy_Click(sender, e);
        //                                LoadDocumentationsReview();
        //                                CIM = RadCoacheeCIM.Text;
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                //SendEmail(1, ReviewID, emailCoachee);
        //                                //SendEmail(2, ReviewID, emailAddressCCM);
        //                                //RadWindowManager1.RadAlert(script + "", 500, 200, "Success", "", "");
        //                                string ModalLabel = "Coachee has signed off from this review.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
                                        


        //                            }
        //                            else if (Account == "TalkTalk")
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionNexidia.Text != null)
        //                                {
        //                                    Description = RadDescriptionNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsNexidia.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesNexidia.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

        //                                string PositiveBehaviour;
        //                                if (RadPositiveBehaviour.Text != null)
        //                                {
        //                                    PositiveBehaviour = RadPositiveBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    PositiveBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string OpportunityBehaviour;
        //                                if (RadOpportunityBehaviour.Text != null)
        //                                {
        //                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    OpportunityBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string BeginBehaviourComments;
        //                                if (RadBeginBehaviourComments.Text != null)
        //                                {
        //                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    BeginBehaviourComments = DBNull.Value.ToString();
        //                                }

        //                                string BehaviourEffects;
        //                                if (RadBehaviourEffect.Text != null)
        //                                {
        //                                    BehaviourEffects = RadBehaviourEffect.Text;
        //                                }
        //                                else
        //                                {
        //                                    BehaviourEffects = DBNull.Value.ToString();
        //                                }

        //                                string RootCause;
        //                                if (RadRootCause.Text != null)
        //                                {
        //                                    RootCause = RadRootCause.Text;
        //                                }
        //                                else
        //                                {
        //                                    RootCause = DBNull.Value.ToString();
        //                                }

        //                                string ReviewResultsComments;
        //                                if (RadReviewResultsComments.Text != null)
        //                                {
        //                                    ReviewResultsComments = RadReviewResultsComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ReviewResultsComments = DBNull.Value.ToString();
        //                                }

        //                                string AddressOpportunities;
        //                                if (RadAddressOpportunities.Text != null)
        //                                {
        //                                    AddressOpportunities = RadAddressOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    AddressOpportunities = DBNull.Value.ToString();
        //                                }

        //                                string ActionPlanComments;
        //                                if (RadActionPlanComments.Text != null)
        //                                {
        //                                    ActionPlanComments = RadActionPlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ActionPlanComments = DBNull.Value.ToString();
        //                                }

        //                                string SmartGoals;
        //                                if (RadSmartGoals.Text != null)
        //                                {
        //                                    SmartGoals = RadSmartGoals.Text;
        //                                }
        //                                else
        //                                {
        //                                    SmartGoals = DBNull.Value.ToString();
        //                                }

        //                                string CreatePlanComments;
        //                                if (RadCreatePlanComments.Text != null)
        //                                {
        //                                    CreatePlanComments = RadCreatePlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    CreatePlanComments = DBNull.Value.ToString();
        //                                }

        //                                string FollowThrough;
        //                                if (RadFollowThrough.Text != null)
        //                                {
        //                                    FollowThrough = RadFollowThrough.Text;
        //                                }
        //                                else
        //                                {
        //                                    FollowThrough = DBNull.Value.ToString();
        //                                }



        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int NexidiaReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
        //                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(NexidiaReviewID);
        //                                //UploadDocumentationReview(0, NexidiaReviewID);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                //RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
        //                                string ModalLabel = "Coachee has signed off from this review.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
                                         
        //                                RadCoacheeCIM.Visible = false;
        //                            }
        //                            else
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionOD.Text != null)
        //                                {
        //                                    Description = RadDescriptionOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsOD.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesOD.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesOD.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                string CoacherFeedback;
        //                                if (RadCoacherFeedback.Text != null)
        //                                {
        //                                    CoacherFeedback = RadCoacherFeedback.Text;
        //                                }
        //                                else
        //                                {
        //                                    CoacherFeedback = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
        //                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ODReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID);
        //                                RadReviewID.Text = Convert.ToString(ODReviewID);
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPIOD();
        //                                //save_commitment(CIMNumber, ODReviewID);
        //                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
        //                                InsertReviewHistory(1);
        //                                //InsertReviewHistory(2);
        //                                LoadKPIReview(ODReviewID);
        //                                //UploadDocumentationReview(0, ODReviewID);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                //RadWindowManager1.RadAlert("" + script + "", 500, 200, "Success", "", "");
        //                                string ModalLabel = "Coachee has signed off from this review.";
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

        //                                RadSSN.Visible = false;
        //                                //RadCoacherSignOff.Visible = true;
        //                                RadCoacheeCIM.Visible = false;



        //                            }
        //                        }
        //                        else
        //                        {
        //                            DataAccess ws = new DataAccess();
        //                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
        //                            //UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                            LoadDocumentationsReview();
        //                            RadCoacheeSignOff.Visible = false;
        //                            string ModalLabel = "Coachee has signed off from this review.";
        //                            string ModalHeader = "Success";
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
        //                            DisableElements();
        //                            RadSSN.Visible = false;

        //                        }
        //                    }
        //                    else
        //                    {

        //                        string script = "Incorrect last 3 digits of SSN.";
        //                        string ModalHeader = "Error Message";
        //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                        DisableElements();

        //                    }
        //                }


        //                else
        //                {

        //                    string script = "SSN is a required field.";
        //                    //RadWindowManager1.RadAlert(script + "", 500, 200, "Error Message", "", "");
        //                    string ModalHeader = "Success";
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                    DisableElements();
        //                }
        //            }
        //            else
        //            {
        //                if (RadCoacheeCIM.Text != "")
        //                {
        //                    if (Convert.ToInt32(RadCoacheeCIM.Text) == Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()))
        //                    {
        //                        if (Convert.ToInt32(RadReviewID.Text) == 0)
        //                        {
        //                            if (Account == "Default")
        //                            {
        //                                //int CIMNumber = Convert.ToInt32(RadCoacheeName.SelectedValue);
        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ReviewID = ws.InsertReview(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), RadFollowup.SelectedDate, RadDescription.Text, RadStrengths.Text, RadOpportunities.Text, RadCommitment.Text, 1, CIMNumber, 1, 2);
        //                                RadReviewID.Text = Convert.ToString(ReviewID);

        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(ReviewID);
        //                                //UploadDocumentationReview(0, ReviewID);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Coachee has signed off from this review.";
        //                                //RadWindowManager1.RadAlert(script + "", 500, 200, "Success", "", "");
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
        //                                RadCoacheeCIM.Visible = false;
                                        
        //                            }
        //                            else if (Account == "TalkTalk")
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescriptionNexidia.Text != null)
        //                                {
        //                                    Description = RadDescriptionNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengthsNexidia.Text != null)
        //                                {
        //                                    Strengths = RadStrengthsNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunitiesNexidia.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunitiesNexidia.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);

        //                                string PositiveBehaviour;
        //                                if (RadPositiveBehaviour.Text != null)
        //                                {
        //                                    PositiveBehaviour = RadPositiveBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    PositiveBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string OpportunityBehaviour;
        //                                if (RadOpportunityBehaviour.Text != null)
        //                                {
        //                                    OpportunityBehaviour = RadOpportunityBehaviour.Text;
        //                                }
        //                                else
        //                                {
        //                                    OpportunityBehaviour = DBNull.Value.ToString();
        //                                }

        //                                string BeginBehaviourComments;
        //                                if (RadBeginBehaviourComments.Text != null)
        //                                {
        //                                    BeginBehaviourComments = RadBeginBehaviourComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    BeginBehaviourComments = DBNull.Value.ToString();
        //                                }

        //                                string BehaviourEffects;
        //                                if (RadBehaviourEffect.Text != null)
        //                                {
        //                                    BehaviourEffects = RadBehaviourEffect.Text;
        //                                }
        //                                else
        //                                {
        //                                    BehaviourEffects = DBNull.Value.ToString();
        //                                }

        //                                string RootCause;
        //                                if (RadRootCause.Text != null)
        //                                {
        //                                    RootCause = RadRootCause.Text;
        //                                }
        //                                else
        //                                {
        //                                    RootCause = DBNull.Value.ToString();
        //                                }

        //                                string ReviewResultsComments;
        //                                if (RadReviewResultsComments.Text != null)
        //                                {
        //                                    ReviewResultsComments = RadReviewResultsComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ReviewResultsComments = DBNull.Value.ToString();
        //                                }

        //                                string AddressOpportunities;
        //                                if (RadAddressOpportunities.Text != null)
        //                                {
        //                                    AddressOpportunities = RadAddressOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    AddressOpportunities = DBNull.Value.ToString();
        //                                }

        //                                string ActionPlanComments;
        //                                if (RadActionPlanComments.Text != null)
        //                                {
        //                                    ActionPlanComments = RadActionPlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    ActionPlanComments = DBNull.Value.ToString();
        //                                }

        //                                string SmartGoals;
        //                                if (RadSmartGoals.Text != null)
        //                                {
        //                                    SmartGoals = RadSmartGoals.Text;
        //                                }
        //                                else
        //                                {
        //                                    SmartGoals = DBNull.Value.ToString();
        //                                }

        //                                string CreatePlanComments;
        //                                if (RadCreatePlanComments.Text != null)
        //                                {
        //                                    CreatePlanComments = RadCreatePlanComments.Text;
        //                                }
        //                                else
        //                                {
        //                                    CreatePlanComments = DBNull.Value.ToString();
        //                                }

        //                                string FollowThrough;
        //                                if (RadFollowThrough.Text != null)
        //                                {
        //                                    FollowThrough = RadFollowThrough.Text;
        //                                }
        //                                else
        //                                {
        //                                    FollowThrough = DBNull.Value.ToString();
        //                                }



        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int NexidiaReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                NexidiaReviewID = ws.InsertReviewNexidia(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, PositiveBehaviour, OpportunityBehaviour, BeginBehaviourComments, BehaviourEffects, RootCause, ReviewResultsComments, AddressOpportunities, ActionPlanComments, SmartGoals, CreatePlanComments, FollowThrough, CIMNumber);
        //                                RadReviewID.Text = Convert.ToString(NexidiaReviewID);
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPI();
        //                                InsertReviewHistory(1);
        //                                InsertReviewHistory(2);
        //                                LoadKPIReview(NexidiaReviewID);
        //                                //UploadDocumentationReview(0, NexidiaReviewID);
        //                                LoadDocumentationsReview();
        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Coachee has signed off from this review.";
        //                                //RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
                                        
        //                                RadCoacheeCIM.Visible = false;

        //                            }
        //                            else
        //                            {
        //                                string FollowDate;
        //                                if (RadFollowup.SelectedDate.HasValue)
        //                                {
        //                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
        //                                }
        //                                else
        //                                {
        //                                    FollowDate = DBNull.Value.ToString();
        //                                }

        //                                string Description;
        //                                if (RadDescription.Text != null)
        //                                {
        //                                    Description = RadDescription.Text;
        //                                }
        //                                else
        //                                {
        //                                    Description = DBNull.Value.ToString();
        //                                }

        //                                string Strengths;
        //                                if (RadStrengths.Text != null)
        //                                {
        //                                    Strengths = RadStrengths.Text;
        //                                }
        //                                else
        //                                {
        //                                    Strengths = DBNull.Value.ToString();
        //                                }

        //                                string Opportunity;
        //                                if (RadOpportunities.Text != null)
        //                                {
        //                                    Opportunity = RadOpportunities.Text;
        //                                }
        //                                else
        //                                {
        //                                    Opportunity = DBNull.Value.ToString();
        //                                }

        //                                string CoacherFeedback;
        //                                if (RadCoacherFeedback.Text != null)
        //                                {
        //                                    CoacherFeedback = RadCoacherFeedback.Text;
        //                                }
        //                                else
        //                                {
        //                                    CoacherFeedback = DBNull.Value.ToString();
        //                                }

        //                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
        //                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

        //                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
        //                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
        //                                int CIMNumber = Convert.ToInt32(cim_num);
        //                                int ODReviewID;
        //                                DataAccess ws = new DataAccess();
        //                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID);
        //                                RadReviewID.Text = Convert.ToString(ODReviewID);
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);


        //                                InsertReviewKPIOD();
        //                                //save_commitment(CIMNumber, ODReviewID);
        //                                DataHelper.InsertCommitmenthere(Convert.ToInt32(ODReviewID), Convert.ToString(RadTextBox2.Text), Convert.ToString(RadTextBox4.Text), Convert.ToString(RadTextBox6.Text), Convert.ToString(RadTextBox8.Text));
        //                                InsertReviewHistory(1);
        //                                //InsertReviewHistory(2);
        //                                LoadKPIReview(ODReviewID);
        //                                //UploadDocumentationReview(0, ODReviewID);
        //                                LoadDocumentationsReview();

        //                                DisableElements();
        //                                RadCoacheeSignOff.Visible = false;
        //                                string script = "Coachee has signed off from this review.";
        //                                //RadWindowManager1.RadAlert("" + script + "", 500, 200, "Success", "", "");
        //                                string ModalHeader = "Success";
        //                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                                RadSSN.Visible = false;
        //                                RadCoacheeCIM.Visible = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            DataAccess ws = new DataAccess();
        //                            ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
        //                            // UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { BtnDummy(); });", true);

        //                            LoadDocumentationsReview();
        //                            RadCoacheeSignOff.Visible = false;
        //                            string script = "Coachee has signed off from this review.";
        //                            //RadWindowManager1.RadAlert(script + "", 500, 200, "Success", "", "");
        //                            string ModalHeader = "Success";
        //                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                            DisableElements();
        //                            RadSSN.Visible = false;

        //                        }
        //                    }
        //                    else
        //                    {

        //                        string script = "Incorrect CIM Number.";
        //                        //RadWindowManager1.RadAlert(script + "", 500, 200, "Error Message", "", "");
        //                        string ModalHeader = "Error Message";
        //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                        DisableElements();

        //                    }
        //                }


        //                else
        //                {

        //                    string script = "Coachee CIM is a required field.";
        //                    //RadWindowManager1.RadAlert(myStringVariable + "", 500, 200, "Error Message", "", "");
        //                    string ModalHeader = "Error Message";
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true);
        //                    DisableElements();
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string myStringVariable = ex.ToString();
        //        //RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");
        //        //ModalLabel.Text = "RadCoacheeSignOff_Click " + myStringVariable;
        //        //ModalHeader.InnerHtml = "Error Message";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal(); });", true);
        //    }
        //}
        //public bool CheckSSN()
        //{
        //    int CIMNumber = Convert.ToInt32(RadCoacheeName.SelectedValue);
        //    int SSN;
        //    DataAccess ws = new DataAccess();
        //    SSN = ws.CheckSSN(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadSSN.Text));

        //    if (SSN == 1)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        private void InsertReviewKPI()
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (Account == "Default")
                {
                    int ReviewID;
                    //Label Target,Current,Previous,KPIID,DriverID;
                    RadComboBox KPI, Driver;
                    RadTextBox Target;
                    RadNumericTextBox Current, Previous;

                    foreach (GridDataItem itm in RadGrid1.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        KPI = (RadComboBox)itm.FindControl("RadKPI");
                        Driver = (RadComboBox)itm.FindControl("RadDriver");
                        Target = (RadTextBox)itm.FindControl("RadTarget");
                        Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                        Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");

                        if (Current.Text != "" && Previous.Text != "")
                        {
                            DataAccess ws = new DataAccess();
                            ws.InsertReviewKPI(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue));

                        }
                    }
                }
                else if (Account == "TalkTalk")
                {
                    int ReviewID;
                    //Label Target,Current,Previous,KPIID,DriverID;
                    RadComboBox KPI, Driver;
                    RadTextBox Target;
                    RadNumericTextBox Current, Previous;

                    foreach (GridDataItem itm in RadGrid4.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        KPI = (RadComboBox)itm.FindControl("RadKPI4");
                        Driver = (RadComboBox)itm.FindControl("RadDriver");
                        Target = (RadTextBox)itm.FindControl("RadTarget");
                        Current = (RadNumericTextBox)itm.FindControl("RadCurrent");
                        Previous = (RadNumericTextBox)itm.FindControl("RadPrevious");

                        if (Current.Text != "" && Previous.Text != "")
                        {
                            DataAccess ws = new DataAccess();
                            ws.InsertReviewKPI(ReviewID, Convert.ToInt32(KPI.SelectedValue), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.SelectedValue));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("InsertReviewKPI" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "InsertReviewKPI " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
   
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReview(ReviewID);
                if (Account == "Default")
                {
                    RadGridPRR.DataSource = ds;
                    RadGridPRR.DataBind();
                }
                else if (Account == "TalkTalk")
                {
                    RadGrid5.DataSource = ds;
                    RadGrid5.DataBind();
                }
                else
                {
                    RadGrid10.DataSource = ds;
                    RadGrid10.DataBind();
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadKPIReview" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadKPIReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void UploadDocumentationReview(int MassCoachingType, int ReviewID)
        {
            try
            {
                string Account = Session["Account"].ToString();
                if (Account == "Default")
                {
                    foreach (UploadedFile f in RadAsyncUpload2.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
                }
                else if (Account == "TalkTalk")
                {
                    foreach (UploadedFile f in RadAsyncUpload3.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload3.ClientID), true);

                }
                else
                {
                    foreach (UploadedFile f in RadAsyncUpload4.UploadedFiles)
                    {
                        string targetFolder = Server.MapPath("~/Documentation/");
                        //string targetFolder = "C:\\Documentation\\";
                        string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                        DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                        string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                        int CIMNumber = Convert.ToInt32(cim_num);
                        DataAccess ws = new DataAccess();
                        string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                        ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                    }
                    LoadDocumentationsReview();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload4.ClientID), true);


                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("UploadDocumentationReview" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "UploadDocumentationReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                string Account = Session["Account"].ToString();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                if (Account == "Default")
                {
                    RadDocumentationReview.DataSource = ds;
                    RadDocumentationReview.Rebind();
                }
                else if (Account == "TalkTalk")
                {
                    RadGrid8.DataSource = ds;
                    RadGrid8.Rebind();
                }
                else
                {
                    RadGrid12.DataSource = ds;
                    RadGrid12.Rebind();
                }
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("LoadDocumentationsReview" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadDocumentationsReview " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        public void DisableElements()
        {
            string Account = Session["Account"].ToString();
            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                if (Account == "Default")
                {
                    RadGrid1.DataSource = null;
                    RadGrid1.DataBind();
                    RadGrid1.Visible = false;
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    RadDescription.Enabled = false;
                    RadStrengths.Enabled = false;
                    RadOpportunities.Enabled = false;
                    RadCommitment.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    //RadAsyncUpload2.Visible = false;
                    RadButton1.Visible = false;
                }
                else if (Account == "TalkTalk")
                {
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    CheckBoxList1.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    RadDescriptionNexidia.Enabled = false;
                    RadGrid4.Visible = false;
                    RadButton2.Visible = false;
                    RadStrengthsNexidia.Enabled = false;
                    RadOpportunitiesNexidia.Enabled = false;
                    RadPositiveBehaviour.Enabled = false;
                    RadOpportunityBehaviour.Enabled = false;
                    RadBeginBehaviourComments.Enabled = false;
                    RadBehaviourEffect.Enabled = false;
                    RadRootCause.Enabled = false;
                    RadReviewResultsComments.Enabled = false;
                    RadAddressOpportunities.Enabled = false;
                    RadActionPlanComments.Enabled = false;
                    RadSmartGoals.Enabled = false;
                    RadCreatePlanComments.Enabled = false;
                    RadFollowThrough.Enabled = false;
                    //RadAsyncUpload3.Visible = false;
                }
                else
                {
                    RadAccount.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadCallID.Enabled = false;
                    RadSessionType.Enabled = false;
                    RadSessionTopic.Enabled = false;
                    RadFollowup.Enabled = false;
                    CheckBoxList1.Enabled = false;
                    if (CheckBox1.Visible == true)
                    {
                        CheckBox1.Enabled = false;
                    }
                    if (CheckBox2.Visible == true)
                    {
                        CheckBox2.Enabled = false;
                    }
                    RadDescriptionOD.Enabled = false;
                    RadGrid9.Visible = false;
                    RadButton3.Visible = false;
                    RadStrengthsOD.Enabled = false;
                    RadOpportunitiesOD.Enabled = false;
                    RadCoacherFeedback.Enabled = false;
                    //RadAsyncUpload3.Visible = false;
                    //grd_Commitment.Enabled = false;
                }
            }

        }
        private void InsertReviewKPIOD()
        {
            try
            {
                int ReviewID;
                //Label Target,Current,Previous,KPIID,DriverID;
                Label KPI, Driver;
                Label Target, Current, Previous, Change; ;
                RadTextBox Behaviour, RootCause;


                foreach (GridDataItem itm in RadGrid9.Items)
                {
                    ReviewID = Convert.ToInt32(RadReviewID.Text);
                    KPI = (Label)itm.FindControl("LblKPI");
                    Driver = (Label)itm.FindControl("LblDriver");
                    Target = (Label)itm.FindControl("LblTarget");
                    Current = (Label)itm.FindControl("LblCurrent");
                    Previous = (Label)itm.FindControl("LblPrevious");
                    Change = (Label)itm.FindControl("LblChange");
                    Behaviour = (RadTextBox)itm.FindControl("RadTextBoxBehaviour");
                    RootCause = (RadTextBox)itm.FindControl("RadTextBoxRootCause");

                    if (Current.Text != "" && Previous.Text != "")
                    {
                        DataAccess ws = new DataAccess();
                        ws.InsertReviewKPIOD(ReviewID, Convert.ToInt32(KPI.Text), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.Text), Change.Text, Behaviour.Text, RootCause.Text);

                    }
                }
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "InsertReviewKPIOD " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }
        protected void RadCMTPreview_Click(object sender, EventArgs e)
        {
            Session["CMTCIM"] = RadCoacheeName.SelectedValue;
            Session["CMTSupName"] = RadSupervisor.SelectedItem.Text;
            Session["CMTAccount"] = RadAccount.SelectedItem.Text;
            Session["CMTSessionType"] = RadSessionType.SelectedItem.Text;
            Session["CMTSessionTopic"] = RadSessionTopic.SelectedItem.Text;
            Session["CMTHRComment"] = RadHRComments.Text;
            Session["CMTName"] = RadCoacheeName.Text;
            Session["PreviewType"] = 1;
            LoadDocs();
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "var Mleft = (screen.width/2)-(760/2);var Mtop = (screen.height/2)-(700/2);window.open( 'CMT.aspx', null, 'height=700,width=760,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );", true);

        }
        public void LoadDocs()
        {
            Docspo();
            DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
            string Name = dsSAPInfo.Tables[0].Rows[0]["First_Name"].ToString() + " " + dsSAPInfo.Tables[0].Rows[0]["Last_Name"].ToString();
            string DateToday = DateTime.Now.ToShortDateString();

            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string filename = f.GetNameWithoutExtension() + f.GetExtension();

                DataTable dt = new DataTable();
                DataRow dr;
                //assigning ViewState value in Data Table  
                dt = (DataTable)ViewState["Docs"];
                //Adding value in datatable  
                dr = dt.NewRow();
                dr["FileName"] = filename;
                dr["UploadedBy"] = Name;
                dr["DateUploaded"] = DateToday;
                dt.Rows.Add(dr);

                if (dt != null)
                {
                    ViewState["Docs"] = dt;
                }
                this.BindListViewDocs();

            }


        }
        public DataTable Docs
        {
            get
            {
                return ViewState["Docs"] as DataTable;
            }
            set
            {
                ViewState["Docs"] = value;
            }
        }
        public void Docspo()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("FileName", typeof(string));
            dt2.Columns.Add("UploadedBy", typeof(string));
            dt2.Columns.Add("DateUploaded", typeof(string));
            ViewState["Docs"] = dt2;

            BindListViewDocs();
        }
        private void BindListViewDocs()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["Docs"];

            if (dt.Rows.Count > 0 && dt != null)
            {

                Session["Docs"] = dt;
            }
        } 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {

            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                string Account = RadAccount.SelectedValue.ToString();//Session["Account"].ToString();

                //if (RadSessionTopic.Text != "Termination")
                //{
                //    if (Account == "Default")
                //    {
                        createreviewtype1();
                //    }
                //    else if (Account == "TalkTalk")
                //    {
                //        PrintNexidia();
                //    }
                //    else
                //    {
                     
                //    }
                //}
                //else
                //{
                //    //createreviewtypecmt();
                //}
            }
            else
            {
                //string script = "alert('Please submit the review before creating a pdf.')";
                string ModalLabel = "Please submit the review before creating a pdf.";
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        protected void createreviewtype1()
        {

            try
            {

                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                FakeURLID.Value = URL;
                if (!Directory.Exists(directoryPath))
                {
                    ImgDefault = URL + "/Content/images/no-photo.jpg";
                }
                else
                {

                    ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                }

                //Document pdfDoc = new Document(PageSize.A4, 28f, 28f, 28f, 28f);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //pdfDoc.NewPage();

                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImgDefault);
                gif.ScaleToFit(10f, 10f);
                //gif.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                //gif.IndentationLeft = 15f;
                //gif.IndentationRight = 15f;
                //gif.SpacingAfter = 15f;
                //gif.SpacingBefore = 15f;
                //gif.Border = 0;



                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader, font));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                specificstable.AddCell(specificscell);
                //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                //specificscelli00.Border = 0;
                //                   specificscelli00.Rowspan = 7;


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs), font));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text), font));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name ", font));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"]), font));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor ", font));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname, font));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department ", font));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"]), font));
                specificscelli8.Border = 0;

                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign ", font));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval), font));
                specificscelli10.Border = 0;


                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic ", font));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]), font));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  ", font));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]), font));
                specificscelli14.Border = 0;


                PdfPCell specificscelli15 = new PdfPCell(new Phrase("Coaching Date  ", font));
                specificscelli15.Border = 0;
                PdfPCell specificscelli16 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CreatedOn"]), font));
                specificscelli16.Border = 0;

                PdfPCell specificscelli17 = new PdfPCell(new Phrase("Follow Date  ", font));
                specificscelli17.Border = 0;
                PdfPCell specificscelli18 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Followdate1"]), font));
                specificscelli18.Border = 0;


                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli15);
                specificstable.AddCell(specificscelli16);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);
                specificstable.AddCell(specificscelli17);
                specificstable.AddCell(specificscelli18);

                specificstable.SpacingAfter = 40;

                Paragraph pHRComments = new Paragraph();
                string lineHRC = "HR Comments : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HRComments"]); //RadStrengths.Text);
                pHRComments.Alignment = Element.ALIGN_LEFT;
                pHRComments.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                pHRComments.Add(lineHRC);
                pHRComments.SpacingBefore = 10;
                pHRComments.SpacingAfter = 10;


                PdfPTable tablenotes = new PdfPTable(7);
                string headernotes = "";
                PdfPCell cellnotes = new PdfPCell(new Phrase(headernotes, font));
                cellnotes.Colspan = 7;
                cellnotes.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tablenotes.AddCell(cellnotes);
                tablenotes.SpacingBefore = 30;
                tablenotes.SpacingAfter = 30;
                tablenotes.WidthPercentage = 100;



                Paragraph p12 = new Paragraph();
                string line12 = "Coaching Notes " + Environment.NewLine;
                p12.Alignment = Element.ALIGN_LEFT;
                p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p12.Add(line12);
                p12.SpacingBefore = 10;
                p12.SpacingAfter = 10;


                PdfPTable notestable = new PdfPTable(5);
                string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell notescell = new PdfPCell(new Phrase(notesheader, font));
                notescell.Colspan = 5;
                notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                notestable.AddCell(notescell);
                notestable.SpacingBefore = 30;
                notestable.SpacingAfter = 30;
                notestable.WidthPercentage = 100;


                notestable.HorizontalAlignment = 0;
                notestable.TotalWidth = 500f;
                notestable.LockedWidth = true;
                float[] widths = new float[] { 100f, 100f, 100f, 100f, 100f };
                notestable.SetWidths(widths);


                DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            notestable.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                            perfcellh2.Border = 0;
                            notestable.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                            perfcellh3.Border = 0;
                            notestable.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                            perfcellh4.Border = 0;
                            notestable.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh5.Border = 0;
                            notestable.AddCell(perfcellh4);

                        }
                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"]), font));
                        perfcelli1.Border = 0;
                        notestable.AddCell(perfcelli1);

                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"]), font));
                        perfcelli2.Border = 0;
                        notestable.AddCell(perfcelli2);

                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"]), font));
                        perfcelli3.Border = 0;
                        notestable.AddCell(perfcelli3);

                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"]), font));
                        perfcelli4.Border = 0;
                        notestable.AddCell(perfcelli4);

                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"]), font));
                        perfcelli5.Border = 0;
                        notestable.AddCell(perfcelli5);


                    }

                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    notestable.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name", font));
                    perfcellh2.Border = 0;
                    notestable.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name", font));
                    perfcellh3.Border = 0;
                    notestable.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by", font));
                    perfcellh4.Border = 0;
                    notestable.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh5.Border = 0;
                    notestable.AddCell(perfcellh4);

                    PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes.", font));
                    notescelli1.Border = 0;
                    notestable.AddCell(notescelli1);
                    PdfPCell notescelli2 = new PdfPCell(new Phrase(" ", font));
                    notescelli2.Border = 0;
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                    notestable.AddCell(notescelli2);
                }

                DataSet ds_perfhistory = null;
                DataAccess ws1 = new DataAccess();
                ds_perfhistory = ws1.GetIncHistory((Convert.ToInt32(RadReviewID.Text)), 2);

                //DataSet ds_perfhistory = DataHelper.GetKPIPerfHistory(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                Paragraph p16 = new Paragraph();
                string line16 = "Performance history";
                p16.Alignment = Element.ALIGN_LEFT;
                p16.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p16.Add(line16);
                p16.SpacingBefore = 10;
                p16.SpacingAfter = 10;

                PdfPTable tableperfhist = new PdfPTable(9);
                string perfhistheader = "";// "Goal    Reality     Options     Way Forward";
                PdfPCell perfhistcell = new PdfPCell(new Phrase(perfhistheader, font));
                perfhistcell.Colspan = 9;
                perfhistcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableperfhist.AddCell(perfhistcell);
                tableperfhist.SpacingBefore = 30;
                tableperfhist.SpacingAfter = 30;
                tableperfhist.WidthPercentage = 100;

                tableperfhist.HorizontalAlignment = 0;
                tableperfhist.TotalWidth = 500f;
                tableperfhist.LockedWidth = true;
                float[] widthstableperfhist = new float[] { 50f, 50f, 45f, 45f, 45f, 45f, 120f, 50f, 50f };
                tableperfhist.SetWidths(widthstableperfhist);


                if (ds_perfhistory.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_perfhistory.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                            perfcellh1.Border = 0;
                            tableperfhist.AddCell(perfcellh1);
                            PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full Name", font));
                            perfcellh2.Border = 0;
                            tableperfhist.AddCell(perfcellh2);
                            PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number", font));
                            perfcellh3.Border = 0;
                            tableperfhist.AddCell(perfcellh3);
                            PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                            perfcellh4.Border = 0;
                            tableperfhist.AddCell(perfcellh4);
                            PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                            perfcellh5.Border = 0;
                            tableperfhist.AddCell(perfcellh5);
                            PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                            perfcellh6.Border = 0;
                            tableperfhist.AddCell(perfcellh6);
                            PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name", font));
                            perfcellh7.Border = 0;
                            tableperfhist.AddCell(perfcellh7);
                            PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date", font));
                            perfcellh8.Border = 0;
                            tableperfhist.AddCell(perfcellh8);
                            PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By", font));
                            perfcellh9.Border = 0;
                            tableperfhist.AddCell(perfcellh9);

                        }

                        PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewID"]), font));
                        perfcelli1.Border = 0;
                        tableperfhist.AddCell(perfcelli1);
                        PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["FullName"]), font));
                        perfcelli2.Border = 0;
                        tableperfhist.AddCell(perfcelli2);
                        PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["CIMNumber"]), font));
                        perfcelli3.Border = 0;
                        tableperfhist.AddCell(perfcelli3);
                        PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Target"]), font));
                        perfcelli4.Border = 0;
                        tableperfhist.AddCell(perfcelli4);
                        PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Current"]), font));
                        perfcelli5.Border = 0;
                        tableperfhist.AddCell(perfcelli5);
                        PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Previous"]), font));
                        perfcelli6.Border = 0;
                        tableperfhist.AddCell(perfcelli6);
                        PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["Driver_Name"]), font));
                        perfcelli7.Border = 0;
                        tableperfhist.AddCell(perfcelli7);
                        PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["ReviewDate"]), font));
                        perfcelli8.Border = 0;
                        tableperfhist.AddCell(perfcelli8);
                        PdfPCell perfcelli9 = new PdfPCell(new Phrase(Convert.ToString(ds_perfhistory.Tables[0].Rows[ctr]["AssignedBy"]), font));
                        perfcelli9.Border = 0;
                        tableperfhist.AddCell(perfcelli9);

                    }
                }
                else
                {
                    PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket", font));
                    perfcellh1.Border = 0;
                    tableperfhist.AddCell(perfcellh1);
                    PdfPCell perfcellh2 = new PdfPCell(new Phrase("Full Name", font));
                    perfcellh2.Border = 0;
                    tableperfhist.AddCell(perfcellh2);
                    PdfPCell perfcellh3 = new PdfPCell(new Phrase("CIM Number", font));
                    perfcellh3.Border = 0;
                    tableperfhist.AddCell(perfcellh3);
                    PdfPCell perfcellh4 = new PdfPCell(new Phrase("Target", font));
                    perfcellh4.Border = 0;
                    tableperfhist.AddCell(perfcellh4);
                    PdfPCell perfcellh5 = new PdfPCell(new Phrase("Current", font));
                    perfcellh5.Border = 0;
                    tableperfhist.AddCell(perfcellh5);
                    PdfPCell perfcellh6 = new PdfPCell(new Phrase("Previous", font));
                    perfcellh6.Border = 0;
                    tableperfhist.AddCell(perfcellh6);
                    PdfPCell perfcellh7 = new PdfPCell(new Phrase("Driver Name", font));
                    perfcellh7.Border = 0;
                    tableperfhist.AddCell(perfcellh7);
                    PdfPCell perfcellh8 = new PdfPCell(new Phrase("Review Date", font));
                    perfcellh8.Border = 0;
                    tableperfhist.AddCell(perfcellh8);
                    PdfPCell perfcellh9 = new PdfPCell(new Phrase("Assigned By", font));
                    perfcellh9.Border = 0;
                    tableperfhist.AddCell(perfcellh9);
                    PdfPCell perfcelli1 = new PdfPCell(new Phrase("No History.", font));
                    perfcelli1.Border = 0;
                    tableperfhist.AddCell(perfcelli1);
                    PdfPCell perfcelli2 = new PdfPCell(new Phrase("", font));
                    perfcelli2.Border = 0;
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);
                    tableperfhist.AddCell(perfcelli2);

                }




                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header, font));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;
                table.WidthPercentage = 100;

                table.HorizontalAlignment = 0;
                table.TotalWidth = 600f;
                table.LockedWidth = true;
                float[] widthstable = new float[] { 150f, 150f, 150f, 150f };
                table.SetWidths(widthstable);



                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                            notescellh1.Border = 0;
                            table.AddCell(notescellh1);
                            PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                            notescellh2.Border = 0;
                            table.AddCell(notescellh2);
                            PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                            notescellh3.Border = 0;
                            table.AddCell(notescellh3);
                            PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                            notescellh4.Border = 0;
                            table.AddCell(notescellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]), font));
                        notescelli1.Border = 0;
                        table.AddCell(notescelli1);


                        PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]), font));
                        notescelli2.Border = 0;
                        table.AddCell(notescelli2);

                        PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]), font));
                        notescelli3.Border = 0;
                        table.AddCell(notescelli3);

                        PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]), font));
                        notescelli4.Border = 0;
                        table.AddCell(notescelli4);

                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"]));
                        //table.AddCell(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"]));

                    }
                }
                else
                {
                    PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number", font));
                    notescellh1.Border = 0;
                    table.AddCell(notescellh1);
                    PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name", font));
                    notescellh2.Border = 0;
                    table.AddCell(notescellh2);
                    PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By", font));
                    notescellh3.Border = 0;
                    table.AddCell(notescellh3);
                    PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded", font));
                    notescellh4.Border = 0;
                    table.AddCell(notescellh4);
                    PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded.", font));
                    notescellh5.Border = 0;
                    table.AddCell(notescellh5);
                    PdfPCell notescellh6 = new PdfPCell(new Phrase("", font));
                    notescellh6.Border = 0;
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                }

                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 9f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 40;
                p13.SpacingAfter = 40;

                pdfDoc.Add(specificstable);
                pdfDoc.Add(pHRComments);
                //pdfDoc.Add(p12);                // previous coaching notes
                //pdfDoc.Add(notestable);
                //pdfDoc.Add(new Paragraph(p16));
                //pdfDoc.Add(tableperfhist);
                pdfDoc.Add(new Paragraph(p13)); //documentation
                pdfDoc.Add(table);//documentation table
                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }
        }
  
        protected void PrintNexidia()
        {
            try
            {
                DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                string ImgDefault;
                string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());
                 
                Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                pdfDoc.NewPage();

                string cs = "Coaching Ticket ";
                PdfPTable specificstable = new PdfPTable(2);
                string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader));
                specificscell.Colspan = 2;
                //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                specificscell.Border = 0;
                specificstable.AddCell(specificscell);
                //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                //specificscelli00.Border = 0;
                //                   specificscelli00.Rowspan = 7;


                PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs)));
                specificscelli1.Border = 0;
                PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text)));
                specificscelli2.Border = 0;


                PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name "));
                specificscelli3.Border = 0;
                PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"])));
                specificscelli4.Border = 0;

                PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor "));
                specificscelli5.Border = 0;
                string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname));
                specificscelli6.Border = 0;

                PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department "));
                specificscelli7.Border = 0;
                PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"])));
                specificscelli8.Border = 0;


                PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign "));
                specificscelli9.Border = 0;
                string campval = "";
                if (ds2.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                }
                PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval)));
                specificscelli10.Border = 0;

                PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic "));
                specificscelli11.Border = 0;
                PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])));
                specificscelli12.Border = 0;

                PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  "));
                specificscelli13.Border = 0;
                PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])));
                specificscelli14.Border = 0;

                //specificstable.AddCell(gif);
                specificstable.AddCell(specificscelli1);
                specificstable.AddCell(specificscelli2);
                specificstable.AddCell(specificscelli3);
                specificstable.AddCell(specificscelli4);
                specificstable.AddCell(specificscelli5);
                specificstable.AddCell(specificscelli6);
                specificstable.AddCell(specificscelli7);
                specificstable.AddCell(specificscelli8);
                specificstable.AddCell(specificscelli9);
                specificstable.AddCell(specificscelli10);
                specificstable.AddCell(specificscelli11);
                specificstable.AddCell(specificscelli12);
                specificstable.AddCell(specificscelli13);
                specificstable.AddCell(specificscelli14);
                specificstable.SpacingAfter = 40;


                Paragraph p1 = new Paragraph();
                string line1 = "HR Comments " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["HRComments"]);
                p1.Alignment = Element.ALIGN_LEFT;
                p1.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                p1.Add(line1);
                p1.SpacingBefore = 10;
                p1.SpacingAfter = 10;

                Paragraph p13 = new Paragraph();
                string line13 = "Documentation " + Environment.NewLine;
                p13.Alignment = Element.ALIGN_LEFT;
                p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                p13.Add(line13);
                p13.SpacingBefore = 10;
                p13.SpacingAfter = 10;

                PdfPTable table = new PdfPTable(4);
                string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                PdfPCell cell = new PdfPCell(new Phrase(header));
                cell.Colspan = 4;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cell);
                table.SpacingBefore = 30;
                table.SpacingAfter = 30;


                DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                {
                    for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                    {
                        if (ctr == 0)
                        {
                            PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                            notescellh1.Border = 0;
                            table.AddCell(notescellh1);
                            PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                            notescellh2.Border = 0;
                            table.AddCell(notescellh2);
                            PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                            notescellh3.Border = 0;
                            table.AddCell(notescellh3);
                            PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                            notescellh4.Border = 0;
                            table.AddCell(notescellh4);

                        }
                        //documents = Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"]);
                        //alldocs = documents + Environment.NewLine + alldocs;

                        PdfPCell notescelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"])));
                        notescelli1.Border = 0;
                        table.AddCell(notescelli1);


                        PdfPCell notescelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"])));
                        notescelli2.Border = 0;
                        table.AddCell(notescelli2);

                        PdfPCell notescelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"])));
                        notescelli3.Border = 0;
                        table.AddCell(notescelli3);

                        PdfPCell notescelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"])));
                        notescelli4.Border = 0;
                        table.AddCell(notescelli4);


                    }
                }
                else
                {
                    PdfPCell notescellh1 = new PdfPCell(new Phrase("Item Number"));
                    notescellh1.Border = 0;
                    table.AddCell(notescellh1);
                    PdfPCell notescellh2 = new PdfPCell(new Phrase("Document Name"));
                    notescellh2.Border = 0;
                    table.AddCell(notescellh2);
                    PdfPCell notescellh3 = new PdfPCell(new Phrase("Uploaded By"));
                    notescellh3.Border = 0;
                    table.AddCell(notescellh3);
                    PdfPCell notescellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                    notescellh4.Border = 0;
                    table.AddCell(notescellh4);
                    PdfPCell notescellh5 = new PdfPCell(new Phrase("No documents uploaded."));
                    notescellh5.Border = 0;
                    table.AddCell(notescellh5);
                    PdfPCell notescellh6 = new PdfPCell(new Phrase(""));
                    notescellh6.Border = 0;
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                    table.AddCell(notescellh6);
                }
                pdfDoc.Add(specificstable);
                pdfDoc.Add(new Paragraph(p1)); //HRComments
                pdfDoc.Add(table); // documentation

                pdfDoc.Close();


                Response.ContentType = "application/pdf";
                string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                    + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                string filenameheader = filename + ".pdf";
                string filenameheadername = "filename=" + filenameheader;
                Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Write(pdfDoc);
                Response.End();



            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                string ModalLabel = ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }
        }
     
        protected void RadFollowup_Load(object sender, EventArgs e)
        {
            (sender as RadDatePicker).MinDate = DateTime.Today;
        }
        private void LoadCommitmentQuestions()
        {
            RadTextBox1.Text = "What do you want to achieve?";
            RadTextBox3.Text = "Where are you know? What is your current impact? What are the future implications? Did Well on current Week? Do Differently?";
            RadTextBox5.Text = "What can you do to bridge the gap / make your goal happen?What else can you try? What might get in the way? How might you overcome that?";
            RadTextBox7.Text = "What option do you think will work the best? What will you do and when? What support and resources do you need?";

        }
        private void GetDefaultForm(int CIMNumber)
        {
            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
            GetForm(Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString()));
            Session["DefaultUserAcct"] = Convert.ToInt32(ds.Tables[0].Rows[0]["CampaignID"].ToString());
            //Session["DefaultUserAcct"]="22946";
            //GetForm(22946);
        }
     
        private static T FindControl<T>(ControlCollection controls, string controlId)
        {
            T ctrl = default(T);
            foreach (Control ctl in controls)
            {
                if (ctl.GetType() != typeof(Telerik.Web.UI.GridTableRow))
                {
                    if (ctl.ClientID.Length <= controlId.Length)
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                    else
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                }


                if (ctl.Controls.Count > 0 && ctrl == null)
                    ctrl = FindControl<T>(ctl.Controls, controlId);
            }
            return ctrl;
        }
        private void InsertReviewHistoryCMT(int InclusionType)
        {
            try
            {
                


                if (InclusionType == 1)
                {
                    int ReviewID;
                    // string ReviewHistoryID;

                    foreach (GridDataItem itm in RadGridCNCMT.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");


                        //ReviewHistoryID = itm["ReviewID"].Text;

                        DataAccess ws = new DataAccess();
                        ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                    }
                }
                else
                {
                    int ReviewID;
                    //string ReviewHistoryID;

                    foreach (GridDataItem itm in RadGridPRCMT.Items)
                    {
                        ReviewID = Convert.ToInt32(RadReviewID.Text);
                        //ReviewHistoryID = itm["ReviewKPIID"].Text;
                        Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                        DataAccess ws = new DataAccess();
                        ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                    }

                } 
            }
            catch (Exception ex)
            {
                // RadWindowManager1.RadAlert("InsertReviewHistoryCMT" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "InsertReviewHistoryCMT " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        public void LoadDocumentations()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadGridDocumentation.DataSource = ds;
                RadGridDocumentation.Rebind();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("LoadDocumentations" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "LoadDocumentations " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

            }

        }
        public void UploadDocumentation(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload1.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                string datename = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + datename + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload1.ClientID), true);
        }
        //protected void RadMajorCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    DataSet ds = null;
        //    DataAccess ws = new DataAccess();
        //    ds = ws.GetCMTDropdowns(3, Convert.ToInt32(RadMajorCategory.SelectedValue));

        //    RadInfractionClass.AllowCustomText = true;
        //    RadInfractionClass.Text = "";
        //    RadInfractionClass.ClearSelection();
        //    RadInfractionClass.AllowCustomText = false;
        //    RadInfractionClass.DataSource = ds;
        //    RadInfractionClass.DataTextField = "Infraction_Name";
        //    RadInfractionClass.DataValueField = "id";
        //    RadInfractionClass.DataBind();
        //}
        //protected void RadSSN_TextChanged(object sender, EventArgs e)
        //{
        //    if (RadSSN.Text == HiddenCoacheeSSS.Value)
        //    {
        //        RadCoacheeSignOff.Enabled = true;
        //        RadSSN.Text = HiddenCoacheeSSS.Value.ToString();
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { LoadSSN(); });", true); 

        //    }
        //    else
        //    {
        //        RadCoacheeSignOff.Enabled = false;
        //        string script = "Incorrect last 3 digits of SSN.";
        //        string ModalHeader = "Error Message";
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + script + "'); });", true); 

        //    }

        //}
        //private void SendEmail(int CoachType,int ReviewID,string emailAddressCCM)
        //{

        //    if (CoachType == 1)//Coachee
        //    {
        //        toName = "To Approve";
        //        fromName = "Request Approval System";
        //        fromEmail = "ras@nucomm.net";

        //        //string CCMEmail = emailAddressCCM;
        //        //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
        //        target = "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
        //        htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + CIM + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\".</font></p>";
        //        htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
        //        subject = "Coach Request with ReviewID" + ReviewID.ToString() + " ";
        //        toEmail = emailAddressCCM;
        //        EmailNotifs ws = new EmailNotifs();
        //        ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
        //    }
        //    else {

        //        toName = "To Approve";
        //        fromName = "Request Approval System";
        //        fromEmail = "ras@nucomm.net";

        //        //string CCMEmail = emailAddressCCM;
        //        //string str1 = CCMEmail.Substring(CCMEmail.IndexOf("@") + 1);
        //        target = "http://home.nucomm.net/Applications/Coach/userlogin.aspx";
        //        htmlBody = "<p><font face=arial size=-1>A coaching session was filed for agent #" + CIM + " with the session \"" + RadSessionType.SelectedItem.Text + "\" and topic \"" + RadSessionTopic.SelectedItem.Text + "\". The review has been signed off by the agent.</font></p>";
        //        htmlBody = htmlBody + "<p><font face=arial size=-1><a target='blank' href='" + target + "'>Click here to go to the Coach Application</a></font></p>";
        //        subject = "Coach Request Sign Off with ReviewID" + ReviewID.ToString() + " ";
        //        toEmail = emailAddressCCM;
        //        EmailNotifs ws = new EmailNotifs();
        //        ws.sendNotification(subject, toName, toEmail, fromName, fromEmail, htmlBody, htmlBody);
        //    }//Coacher
        //}
        private bool ValidatePage()
        {
            bool stat = true;
            if (RadAccount.Visible == true)
            {
                if (RadAccount.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select an account.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSupervisor.Visible == true)
            {
                if (RadSupervisor.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select supervisor.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadCoacheeName.Visible == true)
            {
                if (RadCoacheeName.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select coachee.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSessionType.Visible == true)
            {
                if (RadSessionType.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session type.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadSessionTopic.Visible == true)
            {
                if (RadSessionTopic.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session topic.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadCallID.Visible == true)
            {
                if (RadCallID.Text == "")
                {
                    stat = false;
                    string ModalLabel = "Please enter call id.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (CheckBoxList1.Visible == true)
            {
                if (CheckBoxList1.SelectedIndex == -1)
                {
                    stat = false;
                    string ModalLabel = "Please select session focus.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
            if (RadFollowup.Visible == true)
            {
                if (RadFollowup.SelectedDate.HasValue == false)
                {
                    stat = false;
                    string ModalLabel = "Please select enter follow up date.";
                    string ModalHeader = "Error Message";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);

                }
            }
             
            return stat;
        }
        protected void RadSearchBlackoutYesv2_Click(object sender, EventArgs e)
        {
            HiddenSearchBlackout.Value = "1";
            RadSearchBlackout.Enabled = false;
            RadSearchBlackout.ForeColor = Color.Black;
            //RadCMTSaveSubmit.Text = "Save";
            // Window3.VisibleOnPageLoad = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv2(); });", true);

        }
        protected void RadSearchBlackoutNov2_Click(object sender, EventArgs e)
        {
            //Window3.VisibleOnPageLoad = false;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv2(); });", true);

        }
        protected void RadCMTShareYes_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { CyborgCMT(); });", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv3(); });", true);

        }
        protected void RadCMTShareNo_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { closeCMTv3(); });", true);

        }


        protected void btn_SendtoTl_OnClick(object sender, EventArgs e) {
            //RadAsyncUpload2.PostbackTriggers = new string[] { "UploadDummy" };

            if (RadAccount.SelectedValue != null)
            { 
                string Account = RadAccount.SelectedValue.ToString();//Session["Account"].ToString();
                string emailAddressCCM, emailCoachee;
                DataSet ds1 = null;
                DataAccess ws1 = new DataAccess();
                ds1 = ws1.GetEmployeeInfo(Convert.ToInt32(RadCoacheeName.SelectedValue));
                emailCoachee = ds1.Tables[0].Rows[0]["Email"].ToString();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                emailAddressCCM = dsSAPInfo.Tables[0].Rows[0]["Email"].ToString();
                int ReviewID;

                //ReviewID = ws.InsertHRReview(Convert.ToInt32(RadCoacheeName.SelectedValue),
                //                                Convert.ToInt32(RadAccount.SelectedValue),
                //                                Convert.ToInt32(RadSupervisor.SelectedValue),
                //                                Convert.ToInt32(RadSessionTopic.SelectedValue),
                //                                RadFollowup.SelectedDate,
                //                                RadHRComments.Text,
                //                                2,
                //                                CIMNumber,
                //                                7,
                //                                4); 
                DataAccess ws3 = new DataAccess();
                int formtype = ws3.GetAccountType(Convert.ToInt32(RadAccount.SelectedValue));

                DataAccess ws = new DataAccess();
                ReviewID = ws.InsertHRReview1(Convert.ToInt32(RadCoacheeName.SelectedValue),
                                                Convert.ToInt32(RadAccount.SelectedValue),
                                                Convert.ToInt32(RadSupervisor.SelectedValue),
                                                Convert.ToInt32(RadSessionTopic.SelectedValue),
                                                RadFollowup.SelectedDate,
                                                RadHRComments.Text,
                                                2,
                                                CIMNumber,
                                                7,
                                                4,
                                                formtype); 
                RadReviewID.Text = Convert.ToString(ReviewID);
                DataAccess ws2 = new DataAccess();
                ws2.UpdateReview(Convert.ToInt32(RadReviewID.Text), 1);
                LoadDocs();
                UploadDocumentation(0, Convert.ToInt32(RadReviewID.Text)); // upload
                InsertReviewHistory(1);
                InsertReviewHistory(2);
                RadReviewID.Text = Convert.ToString(ReviewID);
                LoadDocumentations();
                RadAccount.Enabled = false;
                RadSupervisor.Enabled = false;
                RadCoacheeName.Enabled = false;
                RadSessionType.Enabled = false;
                RadSessionTopic.Enabled = false;
                RadHRComments.Enabled = false;
                RadFollowup.Enabled = false;
                UploadDummy.Enabled = false;
                RadAsyncUpload1.Enabled = false;
                CheckBox1.Enabled = false;
                CheckBox2.Enabled = false;
                btn_SendtoTl.Enabled = false;
                
                string ModalLabel = "Review : " + Convert.ToString(RadReviewID.Text) + " Sent To TL : " + Convert.ToString(RadSupervisor.Text);
                string ModalHeader = "Success";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
               
            }
        }
        
        private void InsertReviewHistory(int InclusionType)
        {
            try
            {
                string Account = RadAccount.SelectedValue.ToString(); //Session["Account"].ToString();
              
                        if (InclusionType == 1)
                        {
                            int ReviewID;
                            foreach (GridDataItem itm in RadGridCNCMT.Items) //RadGridCN.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                //ReviewHistoryID = itm["ReviewID"].Text;
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);
                            }
                        }
                        else
                        {
                            int ReviewID;
                            foreach (GridDataItem itm in RadGridPRCMT.Items)// RadGridPR.Items)
                            {
                                ReviewID = Convert.ToInt32(RadReviewID.Text);
                                //ReviewHistoryID = itm["ReviewKPIID"].Text;
                                Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                                DataAccess ws = new DataAccess();
                                ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);
                            }

                        }
                  
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("InsertReviewHistory" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
                string ModalLabel = "InsertReviewHistory " + ex.Message.ToString();
                string ModalHeader = "Error Message";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", "$(function() { openModal('" + ModalHeader + "','" + ModalLabel + "'); });", true);
            }

        }



        protected void RadGrid2_OnItemDatabound(object sender, GridItemEventArgs e)
        {
            int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
            DataSet ds1 = null;
            DataAccess ws1 = new DataAccess();
            //ds = ws.GetCoachingNotes(CoachingCIM);
            int SessionType = Convert.ToInt32(RadSessionType.SelectedValue);
            ds1 = ws1.GetCoachingNotesViaCimAndST1(CoachingCIM, SessionType);
            string Account = null;
            string itemValue = Convert.ToString(CoachingCIM);

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(itemValue));
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["ReviewID"].Controls[0];
                string val1 = hLink.Text;
                //for (int ctr = 1; ctr < ds1.Tables[0].Rows.Count; ctr++)
                //{
                string hlinknavigate = "";
                string val2 = ds1.Tables[0].Rows[e.Item.ItemIndex]["reviewtypeid"].ToString(); //item["reviewtypeid"].Text;

                if (ds.Tables[0].Rows.Count > 0)
                {


                    if (ds.Tables[0].Rows[0]["Client"].ToString() == "TalkTalk")
                    {
                        Account = "TalkTalk";
                    }
                    else
                    {
                        if (CheckOperations(Convert.ToInt32(itemValue)) == true)
                        {
                            Account = "Operations";
                        }
                        else
                        {
                            Account = "NT";
                        }
                    }
                }
                else
                {
                    Account = "NT";
                }

                if (Convert.ToInt32(val2) == 4)
                {
                    hLink.NavigateUrl = "AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2; ;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false");//"~/AddRemoteCoaching.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;


                }

                else if (Convert.ToInt32(val2) == 2)
                {
                    hLink.NavigateUrl = "AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + ",null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false"); //.NavigateUrl =  "~/AddReviewQA.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;


                }


                else if (Convert.ToInt32(val2) == 3)
                {
                    hLink.NavigateUrl = "MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                    hlinknavigate = hLink.NavigateUrl;
                    hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + "',null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false"); //.NavigateUrl =  "~/MassCoachingSignOff.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;


                }
                else
                {
                    //added for talkt talk
                    if (Account == "TalkTalk")
                    {
                        hLink.NavigateUrl = "UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + "',null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false"); //.NavigateUrl =  "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2 + "&Account=" + Account;

                    }
                    else if (Account == "Operations")
                    {
                        hLink.NavigateUrl = "UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + "',null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false"); //.NavigateUrl =  "~/UpdateReviewNexidia.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2 + "&Account=" + Account;

                    }
                    else
                    {
                        hLink.NavigateUrl = "UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;
                        hlinknavigate = hLink.NavigateUrl;
                        hLink.Attributes.Add("onclick", "javascript:window.open('" + hlinknavigate + "',null,'resizable=no,toolbar=no,scrollbars=no,menubar=no,status=no,width=400,height=400'); return false"); //.NavigateUrl =  "~/UpdateReview.aspx?CoachingTicket=" + val1 + "&ReviewType=" + val2;

                    }
                    //added for talkt talk
                }

                //}
            }
        }



        public bool CheckOperations(int CimNumber)
        {
            int ops;
            DataAccess ws = new DataAccess();
            ops = ws.CheckIfOperations(Convert.ToInt32(CimNumber));
            if (ops == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }


}
