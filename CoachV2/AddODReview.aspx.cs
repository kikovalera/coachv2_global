﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CoachV2.AppCode;
using Telerik.Web.UI;
using CoachV2.UserControl;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net;
using iTextSharp.text.html;
using System.Web.UI.HtmlControls;



namespace CoachV2
{
    public partial class AddODReview : System.Web.UI.Page
    {
        public DataTable DataEntry
        {
            get
            {
                return ViewState["DataEntry"] as DataTable;
            }
            set
            {
                ViewState["DataEntry"] = value;
            }
        }
        protected void PerfResults()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns["ID"].AutoIncrement = true;
            dt.Columns["ID"].AutoIncrementSeed = 1;
            dt.Columns["ID"].AutoIncrementStep = 1;
            dt.Columns.Add("KPIID", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("DriverName", typeof(string));
            dt.Columns.Add("Target", typeof(string));
            dt.Columns.Add("Current", typeof(string));
            dt.Columns.Add("Previous", typeof(string));
            dt.Columns.Add("Change", typeof(string));
            dt.Columns.Add("Behaviour", typeof(string));
            dt.Columns.Add("RootCause", typeof(string));
            ViewState["DataEntry"] = dt;
            BindListView();
        }
        private void BindListView()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["DataEntry"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGrid1.DataSource = dt;
                RadGrid1.DataBind();
            }
            else
            {
                RadGrid1.DataSource = null;
                RadGrid1.DataBind();
            }
        }
        protected void OnItemDataBoundHandler(object sender, GridItemEventArgs e)
        {

            if ((e.Item is GridEditFormInsertItem) && (e.Item.OwnerTableView.IsItemInserted))
            {


                GridEditFormInsertItem insertitem = (GridEditFormInsertItem)e.Item;
                RadComboBox combo = (RadComboBox)insertitem.FindControl("RadKPI");
                DataSet ds = new DataSet();
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIList();
                combo.DataSource = ds;
                combo.DataTextField = "Name";
                combo.DataValueField = "KPIID";
                combo.DataBind();
                combo.ClearSelection();

                RadTextBox RadTarget = (RadTextBox)insertitem.FindControl("RadTarget");
                RadComboBox RadDriver = (RadComboBox)insertitem.FindControl("RadDriver");

                DataSet dsTarget = null;
                DataAccess wsTarget = new DataAccess();
                dsTarget = wsTarget.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
                RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

                DataSet dsdrivers = null;
                DataAccess ws2 = new DataAccess();
                dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
                RadDriver.DataSource = dsdrivers;
                RadDriver.DataTextField = "Description";
                RadDriver.DataValueField = "Driver_ID";
                RadDriver.DataBind();


            }

            if ((e.Item is GridEditableItem) && (e.Item.IsInEditMode) && (!(e.Item is IGridInsertItem)))
            {
                GridEditableItem edititem = (GridEditableItem)e.Item;

                Label radID = (Label)edititem.FindControl("RadID");
                radID.Text = ((DataRowView)e.Item.DataItem)["ID"].ToString();

                RadComboBox combo = (RadComboBox)edititem.FindControl("RadKPI");
                RadComboBoxItem selectedItem = new RadComboBoxItem();
                selectedItem.Text = ((DataRowView)e.Item.DataItem)["KPIID"].ToString();
                selectedItem.Value = ((DataRowView)e.Item.DataItem)["Name"].ToString();
                combo.Items.Add(selectedItem);
                selectedItem.DataBind();
                Session["KPI"] = selectedItem.Value;
                RadComboBox comboBrand = (RadComboBox)edititem.FindControl("RadKPI");
                DataSet ds = new DataSet();
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIList();
                comboBrand.DataSource = ds;
                comboBrand.DataBind();
                comboBrand.SelectedValue = selectedItem.Value;

                RadTextBox target = (RadTextBox)edititem.FindControl("RadTarget");
                target.Text = ((DataRowView)e.Item.DataItem)["Target"].ToString();

                RadTextBox behaviour = (RadTextBox)edititem.FindControl("RadBehaviour");
                behaviour.Text = ((DataRowView)e.Item.DataItem)["Behaviour"].ToString();

                RadTextBox rootcause = (RadTextBox)edititem.FindControl("RadRootCause");
                rootcause.Text = ((DataRowView)e.Item.DataItem)["RootCause"].ToString();

                RadNumericTextBox current = (RadNumericTextBox)edititem.FindControl("RadCurrent");
                current.Text = ((DataRowView)e.Item.DataItem)["Current"].ToString();

                RadNumericTextBox previous = (RadNumericTextBox)edititem.FindControl("RadPrevious");
                previous.Text = ((DataRowView)e.Item.DataItem)["previous"].ToString();

                RadNumericTextBox change = (RadNumericTextBox)edititem.FindControl("RadChange");
                change.Text = ((DataRowView)e.Item.DataItem)["Change"].ToString();

                RadComboBox combodriver = (RadComboBox)edititem.FindControl("RadDriver");
                RadComboBoxItem selectedItemdriver = new RadComboBoxItem();
                selectedItemdriver.Text = ((DataRowView)e.Item.DataItem)["Driver"].ToString();
                selectedItemdriver.Value = ((DataRowView)e.Item.DataItem)["DriverName"].ToString();
                combodriver.Items.Add(selectedItemdriver);
                selectedItemdriver.DataBind();
                Session["Driver"] = selectedItemdriver.Value;
                RadComboBox comboBranddriver = (RadComboBox)edititem.FindControl("RadDriver");
                DataSet dsdriver = new DataSet();
                DataAccess wsdriver = new DataAccess();
                dsdriver = wsdriver.GetKPIDrivers(Convert.ToInt32(comboBrand.SelectedValue));
                comboBranddriver.DataSource = dsdriver;
                comboBranddriver.DataTextField = "Description";
                comboBranddriver.DataValueField = "Driver_ID";
                comboBranddriver.DataBind();
                comboBranddriver.SelectedValue = selectedItemdriver.Value;

            }

        }
        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {


            if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
            {

                GridEditableItem item = (GridEditableItem)e.Item;
                RadComboBox RadKPI = (RadComboBox)item.FindControl("RadKPI");
                RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");
                RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
                RadNumericTextBox RadCurrent = (RadNumericTextBox)item.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)item.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)item.FindControl("RadChange");
                RadTextBox RadBehaviour = (RadTextBox)item.FindControl("RadBehaviour");
                RadTextBox RadRootCause = (RadTextBox)item.FindControl("RadRootCause");
                DataTable dt = new DataTable();
                DataRow dr;
                //assigning ViewState value in Data Table  
                dt = (DataTable)ViewState["DataEntry"];
                //Adding value in datatable  
                if (RadPrevious.Text != null || RadCurrent.Text != null || RadChange.Text != null || RadBehaviour.Text != null || RadRootCause.Text != null)
                {
                    dr = dt.NewRow();
                    dr["KPIID"] = RadKPI.SelectedItem.Text;
                    dr["Name"] = RadKPI.SelectedValue;
                    dr["Driver"] = RadDriver.SelectedItem.Text;
                    dr["DriverName"] = RadDriver.SelectedValue;
                    dr["Target"] = RadTarget.Text;
                    dr["Current"] = RadCurrent.Text;
                    dr["Previous"] = RadPrevious.Text;
                    dr["Change"] = RadChange.Text;
                    dr["Behaviour"] = RadBehaviour.Text;
                    dr["RootCause"] = RadRootCause.Text;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["DataEntry"] = dt;
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("Cannot accept blank fields." + "", 500, 200, "Error Message", "", "");
                }
            }


            //here bind Listview after data save in ViewState  
            this.BindListView();


        }
        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if ((e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.EditFormItem))
            {

                GridEditFormItem updateItem = (GridEditFormItem)e.Item;
                int rowindex = updateItem.ItemIndex;
                RadComboBox RadKPI = (RadComboBox)updateItem.FindControl("RadKPI");
                RadComboBox RadDriver = (RadComboBox)updateItem.FindControl("RadDriver");
                RadTextBox RadTarget = (RadTextBox)updateItem.FindControl("RadTarget");
                RadNumericTextBox RadCurrent = (RadNumericTextBox)updateItem.FindControl("RadCurrent");
                RadNumericTextBox RadPrevious = (RadNumericTextBox)updateItem.FindControl("RadPrevious");
                RadNumericTextBox RadChange = (RadNumericTextBox)updateItem.FindControl("RadChange");
                RadTextBox RadBehaviour = (RadTextBox)updateItem.FindControl("RadBehaviour");
                RadTextBox RadRootCause = (RadTextBox)updateItem.FindControl("RadRootCause");
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["DataEntry"];
                if (RadPrevious.Text != null || RadCurrent.Text != null || RadChange.Text != null || RadBehaviour.Text != null || RadRootCause.Text != null)
                {
            
                    dt.Rows[rowindex]["KPIID"] = RadKPI.SelectedItem.Text;
                    dt.Rows[rowindex]["Name"] = RadKPI.SelectedValue;
                    dt.Rows[rowindex]["Driver"] = RadDriver.SelectedItem.Text;
                    dt.Rows[rowindex]["DriverName"] = RadDriver.SelectedValue;
                    dt.Rows[rowindex]["Target"] = RadTarget.Text;
                    dt.Rows[rowindex]["Current"] = RadCurrent.Text;
                    dt.Rows[rowindex]["Previous"] = RadPrevious.Text;
                    dt.Rows[rowindex]["Change"] = RadChange.Text;
                    dt.Rows[rowindex]["Behaviour"] = RadBehaviour.Text;
                    dt.Rows[rowindex]["RootCause"] = RadRootCause.Text;
                    ViewState["DataEntry"] = dt;
                    RadGrid1.MasterTableView.ClearEditItems();
                    //RadGrid1.Rebind();
                }
                else
                {
                    RadWindowManager1.RadAlert("Cannot accept blank fields." + "", 500, 200, "Error Message", "", "");
                }

            }

            //here bind Listview after data save in ViewState  
            this.BindListView();
        }
        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {

            GridDataItem deleteitem = (GridDataItem)e.Item;
            Label LblID = (Label)deleteitem.FindControl("LblID");

            string ID = LblID.Text;
            DataTable dt = (DataTable)ViewState["DataEntry"];

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                if (dr["ID"].ToString() == ID)
                {
                    dr.Delete();
                }
            }
            
          
            this.BindListView();

            if (RadGrid1.Items.Count == 0)
            {
                RadGrid1.MasterTableView.IsItemInserted = true;
                RadGrid1.MasterTableView.Rebind();
            }
        }
        public void RadKPI_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            GridEditFormItem item = (GridEditFormItem)combo.NamingContainer;
            int index = item.ItemIndex;
            RadTextBox RadTarget = (RadTextBox)item.FindControl("RadTarget");
            RadComboBox RadDriver = (RadComboBox)item.FindControl("RadDriver");

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetKPITarget(Convert.ToInt32(combo.SelectedValue));
            RadTarget.Text = ds.Tables[0].Rows[0]["KPITarget"].ToString();

            DataSet dsdrivers = null;
            DataAccess ws2 = new DataAccess();
            dsdrivers = ws2.GetKPIDrivers(Convert.ToInt32(combo.SelectedValue));
            RadDriver.DataSource = dsdrivers;
            RadDriver.DataTextField = "Description";
            RadDriver.DataValueField = "Driver_ID";
            RadDriver.DataBind();

        }
        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = DataEntry;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //GridCommandItem commandItem = (GridCommandItem)RadGrid1.MasterTableView.GetItems(GridItemType.CommandItem)[0];
            //commandItem.FireCommandEvent("InitInsert", null);
            RadGrid1.MasterTableView.IsItemInserted = true;
            RadGrid1.MasterTableView.Rebind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                //ForTesting
                //int CIMNumber = Convert.ToInt32(CIMTEST);
                DataSet ds_userselect2 = DataHelper.GetUserRolesAssigned(CIMNumber, 2);

                if (ds_userselect2.Tables[0].Rows.Count > 0)
                {
                    bool Supervisor;
                    Supervisor = CheckIfHasSubordinates(CIMNumber);

                    if (Supervisor == true)
                    {
                        string CIM = "";

                        if (Request["CIM"] != null) //Add Review Thru My Team
                        {
                            CIM = Request.QueryString["CIM"];
                            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                            ctrlA.Text = "> My Team > Add Review >";
                            SetLabels(Convert.ToInt32(CIM));


                        }
                        else //Add Review Thru My Reviews
                        {
                            Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label1");
                            ctrlA.Text = " > Add Review";
                            Label ctrlB = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                            ctrlB.Text = "";
                            GetAccounts(CIMNumber);


                        }
                        RadCoachingDate.Text = DateTime.Today.ToShortDateString();
                        GetSessionFocus("OD");
                        DataSet ds_scoring = DataHelper.GetCommentsforTriad(0);
                        grd_Commitment.DataSource = ds_scoring;
                        //grdPrevCom.DataSource = ds_scoring;
                        RadDocumentationReview.DataSource = string.Empty;
                        RadDocumentationReview.Rebind();
                        //LoadTTCampaigns();
                        GetDropDownSessions();
                        //List<Nexidia> list = new List<Nexidia>();
                        //lstNexidia = list;
                        PerfResults();
                        RadGrid1.MasterTableView.IsItemInserted = true;
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }


            }
            
        }
        protected bool CheckIfHasSubordinates(int CimNumber)
        {
            DataTable dt = null;
            DataAccess ws = new DataAccess();
            dt = ws.GetSubordinates(CimNumber);
            bool Sup;
            if (dt.Rows.Count > 0)
            {
                Sup = true;
            }
            else
            {
                Sup = false;
            }
            return Sup;
        }
        public void GetSessionFocus(string Account)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionFocus(Account);
                CBList.DataSource = ds;
                CBList.DataTextField = "SessionFocusName";
                CBList.DataValueField = "ID";
                CBList.DataBind();
            }

            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("GetSessionFocus" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
            }
        }
        protected void RadDocumentationReview_onItemDatabound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hLink = (HyperLink)item["FilePath"].Controls[0];
                HyperLink hLinkname = (HyperLink)item["DocumentName"].Controls[0];
                string val1 = hLink.Text;

                hLink.NavigateUrl = val1;
                hLinkname.NavigateUrl = val1;

            }
        }
        public void GetDropDownSessions()
        {
            try
            {

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTypes();
                RadSessionType.DataSource = ds;
                RadSessionType.DataTextField = "SessionName";
                RadSessionType.DataValueField = "SessionID";
                RadSessionType.DataBind();
                //VisibleCheckBox();
            }
            catch (Exception ex)
            {
                //RadWindowManager1.RadAlert("GetDropDownSessions" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void RadSessionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GetDropDownTopics(Convert.ToInt32(RadSessionType.SelectedValue), 10115015);
            RadSessionTopic.Text = string.Empty;
            RadSessionTopic.ClearSelection();
            RadSessionTopic.SelectedIndex = -1;
            VisibleCheckBox();
        }
        protected void GetDropDownTopics(int SessionID, int CimNumber)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSessionTopics2(SessionID, CimNumber);
                RadSessionTopic.DataSource = ds;
                RadSessionTopic.DataTextField = "TopicName";
                RadSessionTopic.DataValueField = "TopicID";
                RadSessionTopic.DataBind();
                //VisibleCheckBox();
            }
            catch (Exception ex)
            {
               RadWindowManager1.RadAlert("GetDropDownTopics" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void RadSessionTopic_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            VisibleCheckBox();
        }
        public void CoachingNotes()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("ID", typeof(int));
            dt2.Columns["ID"].AutoIncrement = true;
            dt2.Columns["ID"].AutoIncrementSeed = 1;
            dt2.Columns["ID"].AutoIncrementStep = 1;
            dt2.Columns.Add("ReviewID", typeof(string));
            dt2.Columns.Add("FullName", typeof(string));
            dt2.Columns.Add("CIMNumber", typeof(string));
            dt2.Columns.Add("Session", typeof(string));
            dt2.Columns.Add("Topic", typeof(string));
            dt2.Columns.Add("ReviewDate", typeof(string));
            dt2.Columns.Add("AssignedBy", typeof(string));
            ViewState["CN"] = dt2;

            BindListViewCN();
        }
        private void BindListViewCN()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["CN"];
            if (dt.Rows.Count > 0 && dt != null)
            {

                RadGridCN.DataSource = dt;
                RadGridCN.DataBind();
            }
            else
            {
                RadGridCN.DataSource = null;
                RadGridCN.DataBind();
            }
        }
        private void VisibleCheckBox()
        {
            if (RadSessionTopic.SelectedIndex == -1)
            {
                CNPR.Visible = false;
                CheckBox1.Visible = false;
                //CheckBox2.Visible = false;
                FollowUp.Visible = true;
                //PerfResult.Visible = false;
                //Div1.Visible = false;
                //Div3.Visible = false;
                //Div5.Visible = false;
            }
            else if (RadSessionTopic.SelectedItem.Text != "Awareness" && RadSessionTopic.SelectedItem.Text != "1st Warning")
            {
                CNPR.Visible = true;
                CheckBox1.Visible = true;
                //CheckBox2.Visible = true;
                FollowUp.Visible = true;
                Div2.Visible = true;
                Div1.Visible = true;
                //DataSet ds_scoring = DataHelper.GetCommentsforTriad(0);
                //grdPrevCom.DataSource = ds_scoring;

            }
            else
            {
                CNPR.Visible = false;
                CheckBox1.Visible = false;
                //CheckBox2.Visible = false;
                FollowUp.Visible = true;
                Div1.Visible = false;
                Div2.Visible = false;


            }
        }
        protected void RadAccount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSupervisors(Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
            RadSupervisor.Text = string.Empty;
            RadSupervisor.ClearSelection();
            RadSupervisor.SelectedIndex = -1;
        }
        public void LoadTTSupervisors(int AccountID)
        {

            try
            {
                //DataSet ds = null;
                //DataAccess ws = new DataAccess();
                //ds = ws.GetTeamLeaderPerAccount(AccountID);

                //RadSupervisor.DataSource = ds;
                //RadSupervisor.DataTextField = "Name";
                //RadSupervisor.DataValueField = "CimNumber";
                //RadSupervisor.DataBind();
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);

                RadSupervisor.Items.Clear();
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetSubordinatesWithTeam(CIMNumber, AccountID);
                RadSupervisor.DataSource = ds;
                RadSupervisor.DataTextField = "Name";
                RadSupervisor.DataValueField = "CimNumber";
                RadSupervisor.DataBind();

                DataSet dsInfo = null;
                DataAccess wsInfo = new DataAccess();
                dsInfo = wsInfo.GetEmployeeInfo(Convert.ToInt32(CIMNumber));
                if (dsInfo.Tables[0].Rows.Count > 0)
                {
                    string FirstName = dsInfo.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = dsInfo.Tables[0].Rows[0]["E Last Name"].ToString();
                    string FullName = FirstName + " " + LastName;
                    RadSupervisor.Items.Add(new Telerik.Web.UI.RadComboBoxItem(FullName.ToString(), CIMNumber.ToString()));
                }

            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("LoadTTSupervisors" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");
            }
        }
        protected void RadSupervisor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadTTSubordinates(Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue));
            RadCoacheeName.Text = string.Empty;
            RadCoacheeName.ClearSelection();
            RadCoacheeName.SelectedIndex = -1;
        }
        public void LoadTTSubordinates(int CimNumber, int AccountID)
        {

            try
            {
                DataTable dt = null;
                DataAccess ws = new DataAccess();
                dt = ws.GetSubordinatesWithAccounts(CimNumber, AccountID);

                var distinctRows = (from DataRow dRow in dt.Rows
                                    select new { col1 = dRow["CimNumber"], col2 = dRow["Name"] }).Distinct();

                RadCoacheeName.Items.Clear();

                foreach (var row in distinctRows)
                {
                    RadCoacheeName.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                }
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert("LoadTTSubordinates" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void RadCoacheeName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                {
                    RadCoacheeCIM.Text = RadCoacheeName.SelectedValue.ToString();
                    RadCoacheeCIM.Enabled = false;
                    RadSSN.Visible = true;
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();



                }
                else
                {
                    RadCoacheeCIM.Text = "";
                    RadCoacheeCIM.Enabled = true;
                    RadSSN.Visible = false;
                    RadGridCN.DataSource = null;
                    RadGridCN.Rebind();

                }
            }
            catch (Exception ex)
            {
                
               // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadWindowManager1.RadAlert("RadCoacheeName_SelectedIndexChanged" + ex.Message.ToString() + "", 500, 200, "Error Message", "", "");

            }
        }
        private bool IsPH(int CIMNumber)
        {

            DataSet ds = null;
            DataAccess ws = new DataAccess();
            ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));

            if (ds.Tables[0].Rows.Count > 0)
            {
                string Country = ds.Tables[0].Rows[0]["Country"].ToString();
                {
                    if (Country == "PH")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {

                return false;
            }

        }
        protected void RadCoacherSignOff_Click(object sender, EventArgs e)
        {
            try
            {


                DataAccess ws = new DataAccess();
                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 1);
                RadCoacherSignOff.Visible = false;
                string myStringVariable = "Coach has signed off from this review.";
                //DisableElements();
                RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "", "", "");


                
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("CoacherSignOff" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void RadCoacheeSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                //    if (CBList.SelectedIndex != -1)
                //    {
                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue.ToString())))
                {
                    if (RadSSN.Text != "")
                    {
                        if (CheckSSN() == true)
                        {
                            if (Convert.ToInt32(RadReviewID.Text) == 0)
                            {
                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescription.Text != null)
                                {
                                    Description = RadDescription.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengths.Text != null)
                                {
                                    Strengths = RadStrengths.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunities.Text != null)
                                {
                                    Opportunity = RadOpportunities.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                string CoacherFeedback;
                                if (RadCoacherFeedback.Text != null)
                                {
                                    CoacherFeedback = RadCoacherFeedback.Text;
                                }
                                else
                                {
                                    CoacherFeedback = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int ODReviewID;
                                DataAccess ws = new DataAccess();
                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus, CIMNumber, CoacherFeedback, CallID);
                                RadReviewID.Text = Convert.ToString(ODReviewID);

                                InsertReviewKPIOD();
                                save_commitment(CIMNumber, ODReviewID);
                                InsertReviewHistory(1);
                                //InsertReviewHistory(2);
                                LoadKPIReview(ODReviewID);
                                UploadDocumentationReview(0, ODReviewID);
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                RadAsyncUpload2.Visible = false;
                                string script = "Coachee has signed off from this review.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "Success", "", "");
                                RadSSN.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, pnlMain, null);
                                RadCoacheeCIM.Visible = false;

                            }
                            else
                            {
                                DataAccess ws = new DataAccess();
                                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                                UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
                                LoadDocumentationsReview();
                                RadCoacheeSignOff.Visible = false;
                                string script = "Coachee has signed off from this review.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "Success", "", "");
                                DisableElements();
                                RadSSN.Visible = false;
                                RadCoacheeCIM.Visible = false;
                                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, pnlMain, null);
                                RadAsyncUpload2.Visible = false;

                            }
                        }
                        else
                        {

                            string script = "Incorrect last 3 digits of SSN.";
                            RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                            //DisableElements();

                        }
                    }


                    else
                    {

                        string script = "SSN is a required field.";
                        RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                        //DisableElements();
                    }
                }
                else
                {
                    if (RadCoacheeCIM.Text != "")
                    {
                        if (Convert.ToInt32(RadCoacheeCIM.Text) == Convert.ToInt32(RadCoacheeName.SelectedValue.ToString()))
                        {
                            if (Convert.ToInt32(RadReviewID.Text) == 0)
                            {

                                
                                string FollowDate;
                                if (RadFollowup.SelectedDate.HasValue)
                                {
                                    FollowDate = RadFollowup.SelectedDate.Value.ToString();
                                }
                                else
                                {
                                    FollowDate = DBNull.Value.ToString();
                                }

                                string Description;
                                if (RadDescription.Text != null)
                                {
                                    Description = RadDescription.Text;
                                }
                                else
                                {
                                    Description = DBNull.Value.ToString();
                                }

                                string Strengths;
                                if (RadStrengths.Text != null)
                                {
                                    Strengths = RadStrengths.Text;
                                }
                                else
                                {
                                    Strengths = DBNull.Value.ToString();
                                }

                                string Opportunity;
                                if (RadOpportunities.Text != null)
                                {
                                    Opportunity = RadOpportunities.Text;
                                }
                                else
                                {
                                    Opportunity = DBNull.Value.ToString();
                                }

                                string CoacherFeedback;
                                if (RadCoacherFeedback.Text != null)
                                {
                                    CoacherFeedback = RadCoacherFeedback.Text;
                                }
                                else
                                {
                                    CoacherFeedback = DBNull.Value.ToString();
                                }

                                int sessionfocus = string.IsNullOrEmpty(CBList.SelectedValue) ? 0 : int.Parse(CBList.SelectedValue);
                                int CallID = string.IsNullOrEmpty(RadCallID.Text) ? 0 : int.Parse(RadCallID.Text);

                                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                                int CIMNumber = Convert.ToInt32(cim_num);
                                int ODReviewID;
                                DataAccess ws = new DataAccess();
                                ODReviewID = ws.InsertReviewOD(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadAccount.SelectedValue), Convert.ToInt32(RadSupervisor.SelectedValue), Convert.ToInt32(RadSessionTopic.SelectedValue), FollowDate, Description, Strengths, Opportunity, sessionfocus,CIMNumber,CoacherFeedback,CallID);
                                RadReviewID.Text = Convert.ToString(ODReviewID);



                                InsertReviewKPIOD();
                                save_commitment(CIMNumber, ODReviewID);
                                InsertReviewHistory(1);
                                //InsertReviewHistory(2);
                                LoadKPIReview(ODReviewID);
                                UploadDocumentationReview(0, ODReviewID);
                                LoadDocumentationsReview();
                                DisableElements();
                                RadCoacheeSignOff.Visible = false;
                                string script = "Coachee has signed off from this review.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "", "", "");
                                RadSSN.Visible = false;
                                RadCoacheeCIM.Visible = false;
                                RadCoacherSignOff.Visible = true;
                                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, pnlMain, null);
                                RadAsyncUpload2.Visible = false;

                            }
                            else
                            {
                                DataAccess ws = new DataAccess();
                                ws.UpdateReview(Convert.ToInt32(RadReviewID.Text), 0);
                                UploadDocumentationReview(0, Convert.ToInt32(RadReviewID.Text));
                                LoadDocumentationsReview();
                                RadCoacheeSignOff.Visible = false;
                                string script = "Coachee has signed off from this review.";
                                RadWindowManager1.RadAlert("" + script + "", 500, 200, "Success", "", "");
                                DisableElements();
                                RadSSN.Visible = false;
                                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, pnlMain, null);
                                RadAsyncUpload2.Visible = false;
                            }
                        }
                        else
                        {

                            string script = "Incorrect CIM Number.";
                            RadWindowManager1.RadAlert("" + script + "", 500, 200, "Error Message", "", "");
                           

                        }
                    }


                    else
                    {
                        string myStringVariable = "Coachee CIM is a required field.";
                        RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                        //DisableElements();
                    }

                }
                //}
                //else
                //{
                //    string myStringVariable = "Please select from the items in Session Focus.";
                //    RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");
                //}
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }

        }
        public bool CheckSSN()
        {
            int CIMNumber = Convert.ToInt32(RadCoacheeName.SelectedValue);
            int SSN;
            DataAccess ws = new DataAccess();
            SSN = ws.CheckSSN(Convert.ToInt32(RadCoacheeName.SelectedValue), Convert.ToInt32(RadSSN.Text));

            if (SSN == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void UploadDocumentationReview(int MassCoachingType, int ReviewID)
        {
            foreach (UploadedFile f in RadAsyncUpload2.UploadedFiles)
            {
                string targetFolder = Server.MapPath("~/Documentation/");
                //string targetFolder = "C:\\Documentation\\";
                f.SaveAs(targetFolder + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
                DataSet dsSAPInfo = DataHelper.GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                string cim_num = dsSAPInfo.Tables[0].Rows[0]["CIM_Number"].ToString();
                int CIMNumber = Convert.ToInt32(cim_num);
                DataAccess ws = new DataAccess();
                string host = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                ws.InsertUploadedDocumentation(ReviewID, f.GetName(), CIMNumber, host + "/Documentation/" + f.GetNameWithoutExtension() + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + ReviewID + "-" + MassCoachingType + f.GetExtension());
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "clearUpload", String.Format("$find('{0}').deleteAllFileInputs()", RadAsyncUpload2.ClientID), true);
        }
        public void LoadDocumentationsReview()
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetUploadedDocuments(Convert.ToInt32(RadReviewID.Text), 3);
                //ds = ws.GetUploadedDocuments(79);

                RadDocumentationReview.DataSource = ds;
                RadDocumentationReview.Rebind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertscript", myStringVariable, true);
            }

        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (RadCoacheeName.SelectedIndex != -1 || RadCoacheeName.AllowCustomText == true)
            {

                if (CheckBox1.Checked == true)
                {
                    CoachingNotes();
                    int CoachingCIM = Convert.ToInt32(RadCoacheeName.SelectedValue);
                    //int CoachingCIM = 10107032;
                    DataSet ds = null;
                    DataAccess ws = new DataAccess();
                    ds = ws.GetCoachingNotes(CoachingCIM);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        RadGrid2.DataSource = ds;
                        RadGrid2.DataBind();
                        Window1.VisibleOnPageLoad = true;
                        RadGridCN.Visible = true;
                    }
                    else
                    {

                        string myStringVariable = "No data for previous performance results.";
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                        RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");

                    }
                }

                else
                {
                    ViewState["CN"] = null;
                    RadGridCN.DataSource = null;
                    RadGridCN.DataBind();

                    Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                    PreviousCommitment.Controls.Remove(Test2);

                }
            }
            else
            {
                CheckBox1.Checked = false;
                string myStringVariable = "Please select the employee first.";
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadWindowManager1.RadAlert("" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }
        }
        private void InsertReviewKPIOD()
        {
            try
            {
                int ReviewID;
                //Label Target,Current,Previous,KPIID,DriverID;
                Label KPI, Driver;
                Label Target,Current, Previous, Change;;
                RadTextBox Behaviour, RootCause;
           

                foreach (GridDataItem itm in RadGrid1.Items)
                {
                    ReviewID = Convert.ToInt32(RadReviewID.Text);
                    KPI = (Label)itm.FindControl("LblKPI");
                    Driver = (Label)itm.FindControl("LblDriver");
                    Target = (Label)itm.FindControl("LblTarget");
                    Current = (Label)itm.FindControl("LblCurrent");
                    Previous = (Label)itm.FindControl("LblPrevious");
                    Change = (Label)itm.FindControl("LblChange");
                    Behaviour = (RadTextBox)itm.FindControl("RadTextBoxBehaviour");
                    RootCause = (RadTextBox)itm.FindControl("RadTextBoxRootCause");

                    if (Current.Text != "" && Previous.Text != "")
                    {
                        DataAccess ws = new DataAccess();
                        ws.InsertReviewKPIOD(ReviewID, Convert.ToInt32(KPI.Text), Target.Text, Current.Text, Previous.Text, Convert.ToInt32(Driver.Text), Change.Text, Behaviour.Text, RootCause.Text);

                    }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");
            }

        }
        protected void save_commitment(int cimno, int reviewid)
        {
            try
            {
                //RadTextBox Goal , Reality, Options, Way;
                string Goal, Reality, Options, Way;
                foreach (GridDataItem itm in grd_Commitment.MasterTableView.Items)
                {
                    reviewid = Convert.ToInt32(RadReviewID.Text);
                    Goal = (itm.FindControl("txtGoal") as RadTextBox).Text; //(RadTextBox)itm.FindControl("txtGoal"); //itm["Goal_Text"].Text; //
                    Reality = (itm.FindControl("txtReality") as RadTextBox).Text; // (RadTextBox)itm.FindControl("txtReality");
                    Options = (itm.FindControl("txtOptions") as RadTextBox).Text; //(RadTextBox)itm.FindControl("txtOptions");
                    Way = (itm.FindControl("txtWayForward") as RadTextBox).Text;  //(RadTextBox)itm.FindControl("txtWayForward");
                    DataHelper.InsertCommitmenthere(Convert.ToInt32(reviewid), Convert.ToString(Goal), Convert.ToString(Reality), Convert.ToString(Options), Convert.ToString(Way));
                }

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }
        }
        protected void GetAccounts(int CimNumber)
        {
            try
            {
                string Role;
                Role = CheckIfHRQA(CimNumber);

                if (Role == "HR")
                {
                    GetAccount(CimNumber);
                }
                else
                {

                    DataTable dt = null;
                    DataAccess ws = new DataAccess();
                    dt = ws.GetSubordinates(CimNumber);

                    var distinctRows = (from DataRow dRow in dt.Rows
                                        select new { col1 = dRow["AccountID"], col2 = dRow["account"] }).Distinct();

                    RadAccount.Items.Clear();

                    foreach (var row in distinctRows)
                    {
                        RadAccount.Items.Add(new Telerik.Web.UI.RadComboBoxItem(row.col2.ToString(), row.col1.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }

        }
        protected string CheckIfHRQA(int CimNumber)
        {

            DataAccess ws = new DataAccess();
            string Role = ws.CheckIfQAHR(CimNumber);
            if (Role != "")
            {
                return Role;
            }
            else
            {
                return Role;
            }


        }
        public void GetAccount(int CIMNumber)
        {
            try
            {

                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetActiveAccounts();
                RadAccount.DataSource = ds;
                RadAccount.DataTextField = "Account";
                RadAccount.DataValueField = "AccountID";
                RadAccount.DataBind();

            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);

            }

        }
        protected void RadCNLink_Click(object sender, EventArgs e)
        {
            if (RadSessionTopic.SelectedItem.Text != "Termination")
            {

                string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                bool chec;
                foreach (GridDataItem item in RadGrid2.SelectedItems)
                {
                    CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                    id = item["ReviewID"].Text;
                    chec = chk.Checked;
                    fullname = item["FullName"].Text;
                    cimnumber = item["CIMNumber"].Text;
                    session = item["Session"].Text;
                    topic = item["Topic"].Text;
                    reviewdate = item["ReviewDate"].Text;
                    assignedby = item["AssignedBy"].Text;

                    DataTable dt = new DataTable();
                    DataRow dr;
                    //assigning ViewState value in Data Table  
                    dt = (DataTable)ViewState["CN"];
                    //Adding value in datatable  
                    dr = dt.NewRow();
                    dr["ReviewID"] = id;
                    dr["FullName"] = fullname;
                    dr["CIMNumber"] = cimnumber;
                    dr["Session"] = session;
                    dr["Topic"] = topic;
                    dr["ReviewDate"] = reviewdate;
                    dr["AssignedBy"] = assignedby;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["CN"] = dt;
                    }
                    this.BindListViewCN();
                   
                }
                int count = RadGrid2.SelectedItems.Count;
                LoadPreviousCommitment(count);
            }

            else
            {
                string id, fullname, cimnumber, session, topic, reviewdate, assignedby;
                bool chec;
                foreach (GridDataItem item in RadGrid2.SelectedItems)
                {
                    CheckBox chk = (CheckBox)item["ClientSelectColumn"].Controls[0];
                    id = item["ReviewID"].Text;
                    chec = chk.Checked;
                    fullname = item["FullName"].Text;
                    cimnumber = item["CIMNumber"].Text;
                    session = item["Session"].Text;
                    topic = item["Topic"].Text;
                    reviewdate = item["ReviewDate"].Text;
                    assignedby = item["AssignedBy"].Text;

                    DataTable dt = new DataTable();
                    DataRow dr;
                    //assigning ViewState value in Data Table  
                    dt = (DataTable)ViewState["CN"];
                    //Adding value in datatable  
                    dr = dt.NewRow();
                    dr["ReviewID"] = id;
                    dr["FullName"] = fullname;
                    dr["CIMNumber"] = cimnumber;
                    dr["Session"] = session;
                    dr["Topic"] = topic;
                    dr["ReviewDate"] = reviewdate;
                    dr["AssignedBy"] = assignedby;
                    dt.Rows.Add(dr);

                    if (dt != null)
                    {
                        ViewState["CN"] = dt;
                    }
                    //this.BindListViewCNCMT();
                }

            }
            ViewState["CN"] = null;
            Window1.VisibleOnPageLoad = false;
        }
        protected void RadCNCancel_Click(object sender, EventArgs e)
        {
            Window1.VisibleOnPageLoad = false;
            CheckBox1.Checked = false;
            RadGridCN.DataSource = null;
            RadGridCN.DataBind();
        }
        public void LoadPreviousCommitment(int count)
        {
            
            DataTable dt = (DataTable)ViewState["CN"];
            foreach (DataRow row in dt.Rows)
            {
                string id = row.Field<string>("ReviewID");
                Test Test2 = (Test)LoadControl("UserControl/Test.ascx");
                Test2.ReviewIDSelected = Convert.ToInt32(id);
                PreviousCommitment.Controls.Add(Test2);
            }

                //int num = count;
                //for (int i = 0; i <= num; i++)
                //{


                   

                   
                //}
                
            //}
        }
        private void InsertReviewHistory(int InclusionType)
        {
            try
            {
                if (CNPR.Visible == true && CheckBoxes.Visible == true)
                {


                    if (InclusionType == 1)
                    {
                        int ReviewID;
                        //string ReviewHistoryID;

                        foreach (GridDataItem itm in RadGridCN.Items)
                        {
                            ReviewID = Convert.ToInt32(RadReviewID.Text);
                            //ReviewHistoryID = itm["ReviewID"].Text;
                            Label ReviewHistoryID = (Label)itm.FindControl("LabelCT");

                            DataAccess ws = new DataAccess();
                            ws.InsertReviewInc(ReviewID, Convert.ToInt32(ReviewHistoryID.Text), InclusionType);


                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }

        }
        public void LoadKPIReview(int ReviewID)
        {
            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetKPIReviewOD(ReviewID);
                RadGridPRR.DataSource = ds;
                RadGridPRR.DataBind();
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                RadWindowManager1.RadAlert("RadCoacheeSignOff_Click" + myStringVariable + "", 500, 200, "Error Message", "", "");

            }

        }
        public void DisableElements()
        {
            RadAccount.Enabled = false;
            RadSupervisor.Enabled = false;
            RadCoacheeName.Enabled = false;
            RadCallID.Enabled = false;
            RadSessionType.Enabled = false;
            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadCoacheeSignOff, RadAsyncUpload2);
            RadSessionTopic.Enabled = false;
            CBList.Enabled = false;
            RadGrid1.Visible = false;
            RadButton1.Visible = false;
            RadDescription.Enabled = false;
            if (Div1.Visible == true)
            {
                RadStrengths.Enabled = false;
                RadOpportunities.Enabled = false;
            }
            RadCoacherFeedback.Enabled = false;
            RadFollowup.Enabled = false;
            grd_Commitment.Enabled = false;
        }
        protected void SetLabels(int CIMNumber)
        {

            try
            {
                DataSet ds = null;
                DataAccess ws = new DataAccess();
                ds = ws.GetEmployeeInfo(Convert.ToInt32(CIMNumber));


                if (ds.Tables[0].Rows.Count > 0)
                {
                    string FirstName = ds.Tables[0].Rows[0]["E First Name"].ToString();
                    string LastName = ds.Tables[0].Rows[0]["E Last Name"].ToString();

                    string AccountID = ds.Tables[0].Rows[0]["CampaignID"].ToString();
                    string Account = ds.Tables[0].Rows[0]["Account"].ToString();

                    string SupervisorID = ds.Tables[0].Rows[0]["Reports_To"].ToString();
                    string SupervisorFirstName = ds.Tables[0].Rows[0]["SupFirst Name"].ToString();
                    string SupervisorLastName = ds.Tables[0].Rows[0]["SupLast Name"].ToString();

                    string FullName = FirstName + " " + LastName;
                    string SupFullName = SupervisorFirstName + " " + SupervisorLastName;

                    RadAccount.AllowCustomText = true;
                    RadAccount.Text = Account;
                    RadAccount.SelectedValue = AccountID;
                    RadCoacheeName.AllowCustomText = true;
                    RadCoacheeName.SelectedValue = CIMNumber.ToString();
                    RadCoacheeName.Text = FullName;
                    RadSupervisor.AllowCustomText = true;
                    RadSupervisor.SelectedValue = SupervisorID;
                    RadSupervisor.Text = SupFullName;
                    RadAccount.Enabled = false;
                    RadCoacheeName.Enabled = false;
                    RadSupervisor.Enabled = false;
                    RadCoacheeCIM.Text = CIMNumber.ToString();
                    //LblSelected.Text = FullName;
                    Label ctrlA = (Label)DashboardMyReviewsUserControl1.FindControl("Label2");
                    ctrlA.Text = FullName;
                    ctrlA.Visible = true;

                    SelectCoacheeGroup.Visible = false;
                    CoacheeInfoGroup.Visible = false;
                    RadCallID.Width = new Unit("35%");
                    RadSessionType.Width = new Unit("50%");
                    RadSessionTopic.Width = new Unit("50%");
                    RadFollowup.Width = new Unit("35%");
                }

                if (IsPH(Convert.ToInt32(RadCoacheeName.SelectedValue)))
                {
                    RadCoacheeCIM.Text = RadCoacheeName.SelectedValue;
                    RadCoacheeCIM.Enabled = false;
                    RadSSN.Visible = true;
                }
                else
                {
                    RadCoacheeCIM.Text = "";
                    RadSSN.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string myStringVariable = ex.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
                RadWindowManager1.RadAlert("SetLabels" + myStringVariable + "", 500, 200, "Error Message", "", "");
            }


        }
        protected void RadFollowup_Load(object sender, EventArgs e)
        {
            (sender as RadDatePicker).MinDate = DateTime.Today;
        }
        //export to PDF 
        protected void btn_ExporttoPDF_Click(object sender, EventArgs e)
        {

            if (Convert.ToInt32(RadReviewID.Text) != 0)
            {
                try
                {
                    DataSet ds_get_review_forPDF = DataHelper.Get_ReviewID(Convert.ToInt32(RadReviewID.Text));
                    DataSet dscoacheeInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Coacheeid"]));
                    DataSet dscoacherInfo = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Createdby"])); //GetEmployeeInfo(HttpContext.Current.User.Identity.Name.Split('|')[0]);

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Content/uploads/" + ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"].ToString()));
                    string ImgDefault;
                    string URL = "http://" + HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath);
                    DataSet ds = DataHelper.GetUserInfoViaCIMNo(Convert.ToString(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])));
                    DataSet ds1 = DataHelper.getuserrolefromsap(Convert.ToInt32(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"]));
                    DataSet ds2 = DataHelper.GetUserInfo(Convert.ToString(ds1.Tables[0].Rows[0]["email"].ToString())); //ds.Tables[0].Rows[0]["Email"].ToString());

                    FakeURLID.Value = URL;
                    if (!Directory.Exists(directoryPath))
                    {
                        ImgDefault = URL + "/Content/images/no-photo.jpg";
                    }
                    else
                    {

                        ImgDefault = URL + "/Content/uploads/" + ds.Tables[0].Rows[0]["CIM_Number"].ToString() + "/" + ds2.Tables[0].Rows[0]["Photo"].ToString();
                    }

                    Document pdfDoc = new Document(PageSize.A4, 35f, 35f, 35f, 35f);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.NewPage();



                    string cs = "Coaching Ticket ";
                    PdfPTable specificstable = new PdfPTable(2);
                    string specificsheader = "Coaching Specifics";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell specificscell = new PdfPCell(new Phrase(specificsheader));
                    specificscell.Colspan = 2;
                    //specificscell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                    specificscell.Border = 0;
                    specificstable.AddCell(specificscell);
                    //PdfPCell specificscelli00 = new PdfPCell(gif);  //gif
                    //specificscelli00.Border = 0;
                    //                   specificscelli00.Rowspan = 7;


                    PdfPCell specificscelli1 = new PdfPCell(new Phrase(Convert.ToString(cs)));
                    specificscelli1.Border = 0;
                    PdfPCell specificscelli2 = new PdfPCell(new Phrase(Convert.ToString(RadReviewID.Text)));
                    specificscelli2.Border = 0;


                    PdfPCell specificscelli3 = new PdfPCell(new Phrase("Name "));
                    specificscelli3.Border = 0;
                    PdfPCell specificscelli4 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Last_Name"])));
                    specificscelli4.Border = 0;

                    PdfPCell specificscelli5 = new PdfPCell(new Phrase("Supervisor "));
                    specificscelli5.Border = 0;
                    string supname = Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Supervisor name"]);//dscoacherInfo.Tables[0].Rows[0]["First_Name"]) + " " + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["Last_Name"]);
                    PdfPCell specificscelli6 = new PdfPCell(new Phrase(supname));
                    specificscelli6.Border = 0;

                    PdfPCell specificscelli7 = new PdfPCell(new Phrase("Department "));
                    specificscelli7.Border = 0;
                    PdfPCell specificscelli8 = new PdfPCell(new Phrase(Convert.ToString(dscoacheeInfo.Tables[0].Rows[0]["Department"])));
                    specificscelli8.Border = 0;

                    PdfPCell specificscelli9 = new PdfPCell(new Phrase("Campaign "));
                    specificscelli9.Border = 0;
                    string campval = "";
                    if (ds2.Tables[0].Rows.Count == 0)
                    {
                    }
                    else
                    {
                        campval = Convert.ToString(ds2.Tables[0].Rows[0]["Campaign"]);
                    }
                    PdfPCell specificscelli10 = new PdfPCell(new Phrase(Convert.ToString(campval)));
                    specificscelli10.Border = 0;

                    PdfPCell specificscelli11 = new PdfPCell(new Phrase("Topic "));
                    specificscelli11.Border = 0;
                    PdfPCell specificscelli12 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"])));
                    specificscelli12.Border = 0;

                    PdfPCell specificscelli13 = new PdfPCell(new Phrase("Session Type  "));
                    specificscelli13.Border = 0;
                    PdfPCell specificscelli14 = new PdfPCell(new Phrase(Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"])));
                    specificscelli14.Border = 0;

                    //specificstable.AddCell(gif);
                    specificstable.AddCell(specificscelli1);
                    specificstable.AddCell(specificscelli2);
                    specificstable.AddCell(specificscelli3);
                    specificstable.AddCell(specificscelli4);
                    specificstable.AddCell(specificscelli5);
                    specificstable.AddCell(specificscelli6);
                    specificstable.AddCell(specificscelli7);
                    specificstable.AddCell(specificscelli8);
                    specificstable.AddCell(specificscelli9);
                    specificstable.AddCell(specificscelli10);
                    specificstable.AddCell(specificscelli11);
                    specificstable.AddCell(specificscelli12);
                    specificstable.AddCell(specificscelli13);
                    specificstable.AddCell(specificscelli14);
                    specificstable.SpacingAfter = 40;


                    Paragraph p5 = new Paragraph();
                    DataSet ds_focusname = DataHelper.getsessionfocus(CBList.SelectedValue);
                    string line5 = "";
                    string line5a1 = "Call ID : " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CallID"]);

                    string line5a = Environment.NewLine + "Session Type : " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["SessionName"]);//RadSessionType.Text);
                    string line5a2 = Environment.NewLine + "Topic: " + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["topicname"]);//RadSessionTopic.Text);

                    if (ds_focusname.Tables[0].Rows.Count > 0)
                    {
                        line5 = Environment.NewLine + "Session Focus : " + Convert.ToString(ds_focusname.Tables[0].Rows[0]["SessionFocusName"]);
                    }
                    else
                    {

                        line5 = Environment.NewLine + "Session Focus : ";
                    }
                    p5.Alignment = Element.ALIGN_LEFT;
                    p5.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p5.Add(line5a1);
                    p5.Add(line5a2);
                    p5.Add(line5a);
                    p5.Add(line5);


                    Paragraph p9 = new Paragraph();
                    string line9 = "Description : " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Description"]);
                    p9.Alignment = Element.ALIGN_LEFT;
                    p9.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p9.Add(line9);
                    p9.SpacingBefore = 40;

                    Paragraph p10 = new Paragraph();
                    string line10 = "Performance Result ";
                    p10.Alignment = Element.ALIGN_LEFT;
                    p10.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p10.Add(line10);
                    p10.SpacingBefore = 10;
                    p10.SpacingAfter = 10;

                    PdfPTable tableperf = new PdfPTable(8);
                    string headerperf = "";// "Name    Target      Current     Previous     Change     Driver      Behaviour       Root Cause";
                    PdfPCell cellperf = new PdfPCell(new Phrase(headerperf));
                    cellperf.Colspan = 8;
                    cellperf.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    tableperf.AddCell(cellperf);
                    tableperf.SpacingBefore = 30;
                    tableperf.SpacingAfter = 30;

                    DataSet ds_kpiperf = DataHelper.GetKPIPerfOD(Convert.ToInt32(RadReviewID.Text));
                    if (ds_kpiperf.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_kpiperf.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {

                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Name"));
                                perfcellh1.Border = 0;
                                tableperf.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                                perfcellh2.Border = 0;
                                tableperf.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Previous"));
                                perfcellh3.Border = 0;
                                tableperf.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Current"));
                                perfcellh4.Border = 0;
                                tableperf.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                                perfcellh5.Border = 0;
                                tableperf.AddCell(perfcellh5);
                                PdfPCell perfcellh6 = new PdfPCell(new Phrase("Change"));
                                perfcellh6.Border = 0;
                                tableperf.AddCell(perfcellh6);
                                PdfPCell perfcellh7 = new PdfPCell(new Phrase("Behaviour"));
                                perfcellh7.Border = 0;
                                tableperf.AddCell(perfcellh7);
                                PdfPCell perfcellh8 = new PdfPCell(new Phrase("Root Cause"));
                                perfcellh8.Border = 0;
                                tableperf.AddCell(perfcellh8);

                            }

                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Name"])));
                            perfcelli1.Border = 0;
                            tableperf.AddCell(perfcelli1);
                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Target"])));
                            perfcelli2.Border = 0;
                            tableperf.AddCell(perfcelli2);
                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Previous"])));
                            perfcelli3.Border = 0;
                            tableperf.AddCell(perfcelli3);
                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Current"])));
                            perfcelli4.Border = 0;
                            tableperf.AddCell(perfcelli4);
                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Description"])));
                            perfcelli5.Border = 0;
                            tableperf.AddCell(perfcelli5);
                            PdfPCell perfcelli6 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Change"])));
                            perfcelli6.Border = 0;
                            tableperf.AddCell(perfcelli6);
                            PdfPCell perfcelli7 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["Behaviour"])));
                            perfcelli7.Border = 0;
                            tableperf.AddCell(perfcelli7);
                            PdfPCell perfcelli8 = new PdfPCell(new Phrase(Convert.ToString(ds_kpiperf.Tables[0].Rows[ctr]["RootCause"])));
                            perfcelli8.Border = 0;
                            tableperf.AddCell(perfcelli8);
                        }
                    }
                    else
                    {


                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Name"));
                        perfcellh1.Border = 0;
                        tableperf.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Target"));
                        perfcellh2.Border = 0;
                        tableperf.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Previous"));
                        perfcellh3.Border = 0;
                        tableperf.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Current"));
                        perfcellh4.Border = 0;
                        tableperf.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Description"));
                        perfcellh5.Border = 0;
                        tableperf.AddCell(perfcellh5);
                        PdfPCell perfcellh6 = new PdfPCell(new Phrase("Change"));
                        perfcellh6.Border = 0;
                        tableperf.AddCell(perfcellh6);
                        PdfPCell perfcellh7 = new PdfPCell(new Phrase("Behaviour"));
                        perfcellh7.Border = 0;
                        tableperf.AddCell(perfcellh7);
                        PdfPCell perfcellh8 = new PdfPCell(new Phrase("Root Cause"));
                        perfcellh8.Border = 0;
                        tableperf.AddCell(perfcellh8);

                        PdfPCell perfcellh9 = new PdfPCell(new Phrase("No Performance Result."));
                        perfcellh9.Border = 0;
                        tableperf.AddCell(perfcellh9);
                        PdfPCell perfcellh10 = new PdfPCell(new Phrase(""));
                        perfcellh10.Border = 0;
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        tableperf.AddCell(perfcellh10);
                        //  alldocs = "No documents uploaded.";
                    }


                    Paragraph p11 = new Paragraph();
                    string line11 = "Coaching Notes " + Environment.NewLine;
                    p11.Alignment = Element.ALIGN_LEFT;
                    p11.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p11.Add(line11);
                    p11.SpacingBefore = 10;
                    p11.SpacingAfter = 10;


                    PdfPTable tablenotes = new PdfPTable(5);
                    string notesheader = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell notescell = new PdfPCell(new Phrase(notesheader));
                    notescell.Colspan = 5;
                    notescell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    tablenotes.AddCell(notescell);
                    tablenotes.SpacingBefore = 30;
                    tablenotes.SpacingAfter = 30;
                    DataSet ds_coachingnotes = DataHelper.Get_ReviewIDCoachingNotes(Convert.ToInt32(RadReviewID.Text));
                    if (ds_coachingnotes.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_coachingnotes.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                                perfcellh1.Border = 0;
                                tablenotes.AddCell(perfcellh1);
                                PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                                perfcellh2.Border = 0;
                                tablenotes.AddCell(perfcellh2);
                                PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                                perfcellh3.Border = 0;
                                tablenotes.AddCell(perfcellh3);
                                PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                                perfcellh4.Border = 0;
                                tablenotes.AddCell(perfcellh4);
                                PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                                perfcellh5.Border = 0;
                                tablenotes.AddCell(perfcellh4);

                            }
                            PdfPCell perfcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Coaching Ticket"])));
                            perfcelli1.Border = 0;
                            tablenotes.AddCell(perfcelli1);

                            PdfPCell perfcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Topic Name"])));
                            perfcelli2.Border = 0;
                            tablenotes.AddCell(perfcelli2);

                            PdfPCell perfcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Session Name"])));
                            perfcelli3.Border = 0;
                            tablenotes.AddCell(perfcelli3);

                            PdfPCell perfcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Assigned by"])));
                            perfcelli4.Border = 0;
                            tablenotes.AddCell(perfcelli4);

                            PdfPCell perfcelli5 = new PdfPCell(new Phrase(Convert.ToString(ds_coachingnotes.Tables[0].Rows[ctr]["Review Date"])));
                            perfcelli5.Border = 0;
                            tablenotes.AddCell(perfcelli5);


                        }

                    }
                    else
                    {
                        PdfPCell perfcellh1 = new PdfPCell(new Phrase("Coaching Ticket"));
                        perfcellh1.Border = 0;
                        tablenotes.AddCell(perfcellh1);
                        PdfPCell perfcellh2 = new PdfPCell(new Phrase("Topic Name"));
                        perfcellh2.Border = 0;
                        tablenotes.AddCell(perfcellh2);
                        PdfPCell perfcellh3 = new PdfPCell(new Phrase("Session Name"));
                        perfcellh3.Border = 0;
                        tablenotes.AddCell(perfcellh3);
                        PdfPCell perfcellh4 = new PdfPCell(new Phrase("Assigned by"));
                        perfcellh4.Border = 0;
                        tablenotes.AddCell(perfcellh4);
                        PdfPCell perfcellh5 = new PdfPCell(new Phrase("Review Date"));
                        perfcellh5.Border = 0;
                        tablenotes.AddCell(perfcellh4);

                        PdfPCell notescelli1 = new PdfPCell(new Phrase("No Coaching Notes."));
                        notescelli1.Border = 0;
                        tablenotes.AddCell(notescelli1);
                        PdfPCell notescelli2 = new PdfPCell(new Phrase(" "));
                        notescelli2.Border = 0;
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                        tablenotes.AddCell(notescelli2);
                    }


                    Paragraph p12 = new Paragraph();
                    string line12 = "Strengths " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Strengths"]); //RadStrengths.Text);
                    p12.Alignment = Element.ALIGN_LEFT;
                    p12.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p12.Add(line12);
                    p12.SpacingBefore = 10;
                    p12.SpacingAfter = 10;

                    Paragraph p14 = new Paragraph();
                    string line14 = "Opportunities " + Environment.NewLine + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["Opportunity"]);//RadOpportunities.Text);
                    p14.Alignment = Element.ALIGN_LEFT;
                    p14.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p14.Add(line14);
                    p14.SpacingBefore = 10;
                    p14.SpacingAfter = 10;


                    Paragraph p15 = new Paragraph();
                    string line15 = "Commitment";
                    p15.Alignment = Element.ALIGN_LEFT;
                    p15.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p15.Add(line15);
                    p15.SpacingBefore = 10;
                    p15.SpacingAfter = 10;

                    PdfPTable commitment = new PdfPTable(4);
                    string commheader = "";// "Goal    Reality     Options     Way Forward";
                    PdfPCell commcell = new PdfPCell(new Phrase(commheader));
                    commcell.Colspan = 4;
                    commcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    commitment.AddCell(commcell);
                    commitment.SpacingBefore = 30;
                    commitment.SpacingAfter = 30;

                    DataSet ds_scoring = DataHelper.GetCommentsforTriadpdf(Convert.ToInt32(RadReviewID.Text)); //GetCommentsforTriad(Convert.ToInt32(RadReviewID.Text));
                    if (ds_scoring.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_scoring.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal"));
                                commitmentcellh1.Border = 0;
                                commitment.AddCell(commitmentcellh1);
                                PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality"));
                                commitmentcellh2.Border = 0;
                                commitment.AddCell(commitmentcellh2);
                                PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options"));
                                commitmentcellh3.Border = 0;
                                commitment.AddCell(commitmentcellh3);
                                PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward"));
                                commitmentcellh4.Border = 0;
                                commitment.AddCell(commitmentcellh4);


                            }

                            PdfPCell commitmentcelli1 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Goal_Text"])));
                            commitmentcelli1.Border = 0;
                            commitment.AddCell(commitmentcelli1);
                            PdfPCell commitmentcelli2 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Reality_Text"])));
                            commitmentcelli2.Border = 0;
                            commitment.AddCell(commitmentcelli2);
                            PdfPCell commitmentcelli3 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["Options_Text"])));
                            commitmentcelli3.Border = 0;
                            commitment.AddCell(commitmentcelli3);
                            PdfPCell commitmentcelli4 = new PdfPCell(new Phrase(Convert.ToString(ds_scoring.Tables[0].Rows[ctr]["WayForward_Text"])));
                            commitmentcelli4.Border = 0;
                            commitment.AddCell(commitmentcelli4);
                        }
                    }
                    else
                    {
                        PdfPCell commitmentcellh1 = new PdfPCell(new Phrase("Goal"));
                        commitmentcellh1.Border = 0;
                        commitment.AddCell(commitmentcellh1);
                        PdfPCell commitmentcellh2 = new PdfPCell(new Phrase("Reality"));
                        commitmentcellh2.Border = 0;
                        commitment.AddCell(commitmentcellh2);
                        PdfPCell commitmentcellh3 = new PdfPCell(new Phrase("Options"));
                        commitmentcellh3.Border = 0;
                        commitment.AddCell(commitmentcellh3);
                        PdfPCell commitmentcellh4 = new PdfPCell(new Phrase("Way Forward"));
                        commitmentcellh4.Border = 0;
                        commitment.AddCell(commitmentcellh4);
                        PdfPCell commitmentcellh5 = new PdfPCell(new Phrase("No GROW Comments"));
                        commitmentcellh5.Border = 0;
                        commitment.AddCell(commitmentcellh5);
                        PdfPCell commitmentcellh6 = new PdfPCell(new Phrase(""));
                        commitmentcellh6.Border = 0;
                        commitment.AddCell(commitmentcellh6);
                        commitment.AddCell(commitmentcellh6);
                        commitment.AddCell(commitmentcellh6);


                    }



                    Paragraph p13 = new Paragraph();
                    string line13 = "Documentation " + Environment.NewLine;
                    p13.Alignment = Element.ALIGN_LEFT;
                    p13.Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK);
                    p13.Add(line13);
                    p13.SpacingBefore = 10;
                    p13.SpacingAfter = 10;

                    PdfPTable table = new PdfPTable(4);
                    string header = "";//"Item Number        Document Name        Uploaded By       Date Uploaded";
                    PdfPCell cell = new PdfPCell(new Phrase(header));
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cell);
                    table.SpacingBefore = 30;
                    table.SpacingAfter = 30;

                    DataSet ds_remotedocumentation1 = DataHelper.GetDocumentationpdf(Convert.ToInt32(RadReviewID.Text), 3);
                    if (ds_remotedocumentation1.Tables[0].Rows.Count > 0)
                    {
                        for (int ctr = 0; ctr < ds_remotedocumentation1.Tables[0].Rows.Count; ctr++)
                        {
                            if (ctr == 0)
                            {
                                PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number"));
                                cellh1.Border = 0;
                                table.AddCell(cellh1);
                                PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name"));
                                cellh2.Border = 0;
                                table.AddCell(cellh2);
                                PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By"));
                                cellh3.Border = 0;
                                table.AddCell(cellh3);
                                PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                                cellh4.Border = 0;
                                table.AddCell(cellh4);


                            }
                            PdfPCell celli1 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["Id"])));
                            celli1.Border = 0;
                            table.AddCell(celli1);


                            PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DocumentName"])));
                            celli2.Border = 0;
                            table.AddCell(celli2);

                            PdfPCell celli3 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["UploadedBy"])));
                            celli3.Border = 0;
                            table.AddCell(celli3);

                            PdfPCell celli4 = new PdfPCell(new Phrase(Convert.ToString(ds_remotedocumentation1.Tables[0].Rows[ctr]["DateUploaded"])));
                            celli4.Border = 0;
                            table.AddCell(celli4);
                        }
                    }
                    else
                    {
                        PdfPCell cellh1 = new PdfPCell(new Phrase("Item Number"));
                        cellh1.Border = 0;
                        table.AddCell(cellh1);
                        PdfPCell cellh2 = new PdfPCell(new Phrase("Document Name"));
                        cellh2.Border = 0;
                        table.AddCell(cellh2);
                        PdfPCell cellh3 = new PdfPCell(new Phrase("Uploaded By"));
                        cellh3.Border = 0;
                        table.AddCell(cellh3);
                        PdfPCell cellh4 = new PdfPCell(new Phrase("Date Uploaded"));
                        cellh4.Border = 0;
                        table.AddCell(cellh4);

                        PdfPCell celli1 = new PdfPCell(new Phrase("No documents uploaded."));
                        celli1.Border = 0;
                        table.AddCell(celli1);
                        PdfPCell celli2 = new PdfPCell(new Phrase(Convert.ToString("")));
                        celli2.Border = 0;
                        table.AddCell(celli2);
                        table.AddCell(celli2);
                        table.AddCell(celli2);
                    }





                    pdfDoc.Add(specificstable); //coaching specifics
                    pdfDoc.Add(new Paragraph(p9)); //description
                    pdfDoc.Add(new Paragraph(p10)); //positive description
                    pdfDoc.Add(tableperf); //performance table
                    pdfDoc.Add(new Paragraph(p11)); //positive description
                    pdfDoc.Add(tablenotes);
                    pdfDoc.Add(new Paragraph(p12)); // description
                    pdfDoc.Add(new Paragraph(p14)); //strenght
                    pdfDoc.Add(new Paragraph(p15)); //opportunities
                    pdfDoc.Add(commitment); //commitment table 
                    pdfDoc.Add(new Paragraph(p13)); //documentation
                    pdfDoc.Add(table);//documentation



                    pdfDoc.Close();


                    Response.ContentType = "application/pdf";
                    string filename = "CoachingTicket_" + Convert.ToString(RadReviewID.Text) + "_" + "Coachee_" + Convert.ToString(ds_get_review_forPDF.Tables[0].Rows[0]["CoacheeID"])
                        + "_Coacher_" + Convert.ToString(dscoacherInfo.Tables[0].Rows[0]["CIM_Number"]);
                    string filenameheader = filename + ".pdf";
                    string filenameheadername = "filename=" + filenameheader;
                    Response.AddHeader("content-disposition", "attachment;" + filenameheadername);// "filename=sample.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message.ToString());

                }
            }
            else
            {
                string script = "alert('Please submit the review before creating a pdf.')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "its working", script, true);

            }

        }        //export to PDF
     
    }
}